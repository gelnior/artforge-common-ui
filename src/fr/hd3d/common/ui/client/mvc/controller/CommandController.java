package fr.hd3d.common.ui.client.mvc.controller;

import java.util.HashMap;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;


/**
 * Command controller is a maskable controller that handles a list of commands. That list of commands is store in a map
 * associating an event to a command. So each time one of registered events is called, it executes associated command.
 * 
 * @author HD3D
 */
public class CommandController extends MaskableController
{
    /** Map that contains event <=> command association. */
    protected HashMap<EventType, BaseCommand> commandMap = new HashMap<EventType, BaseCommand>();

    /**
     * Works as usually plus the fact that it checks if command map contains the event to know if it should be executed.
     * 
     * @param event
     *            The event to check.
     */
    @Override
    public boolean canHandle(AppEvent event)
    {
        if (!this.isMasked)
        {
            if (commandMap.get(event.getType()) != null)
                return true;
            else
                return super.canHandle(event);
        }
        else
        {
            return false;
        }
    }

    /**
     * Gets command corresponding to event, then executes it.
     * 
     * @param event
     *            The event to handle.
     */
    @Override
    public void handleEvent(AppEvent event)
    {
        BaseCommand command = commandMap.get(event.getType());
        if (command != null)
            command.execute(event);
    }

}
