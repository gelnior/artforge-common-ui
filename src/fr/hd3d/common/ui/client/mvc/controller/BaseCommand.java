package fr.hd3d.common.ui.client.mvc.controller;

import com.extjs.gxt.ui.client.mvc.AppEvent;


/**
 * Interface for command of base controller. Force to implement execute method with application event as parameter. This
 * method is called by controller when an event occured.
 * 
 * @author HD3D
 */
public interface BaseCommand
{

    void execute(AppEvent event);

}
