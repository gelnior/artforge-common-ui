package fr.hd3d.common.ui.client.mvc.controller;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.mvc.Controller;


/**
 * Maskable controller got an additional field that can be used to mask the controller to EventDispatcher. It gives the
 * availability to disable/enable controller.
 * 
 * @author HD3D
 */
public abstract class MaskableController extends Controller
{
    /** Tell if controller is actually masked or not. */
    protected boolean isMasked = false;

    /**
     * @return True when controller is actually masked, false either.
     */
    public boolean isMasked()
    {
        return isMasked;
    }

    /**
     * Set controller as masked.
     */
    public void mask()
    {
        this.isMasked = true;
    }

    /**
     * Set controller as unmasked.
     */
    public void unMask()
    {
        this.isMasked = false;
    }

    public void handleEvent(EventType type)
    {
        this.handleEvent(new AppEvent(type));
    }

    /**
     * Maskable controller can handle events only if it is not masked and if event is registered.
     * 
     * @param event
     *            The event to analyse.
     */
    @Override
    public boolean canHandle(AppEvent event)
    {
        if (!this.isMasked)
        {
            return super.canHandle(event);
        }
        else
        {
            return false;
        }
    }

    /**
     * Force controller children to handle event, even they are masked.
     * 
     * @param event
     *            The event to forward.
     */
    public void forwardToChildForced(AppEvent event)
    {
        if (children != null)
        {
            for (Controller c : children)
            {
                c.handleEvent(event);
            }
        }
    }

    @Deprecated
    public void handleLocalEvent(AppEvent event)
    {

        this.handleEvent(event);
    }

    @Deprecated
    public void handleLocalEvent(EventType event)
    {
        this.handleLocalEvent(new AppEvent(event));
    }
}
