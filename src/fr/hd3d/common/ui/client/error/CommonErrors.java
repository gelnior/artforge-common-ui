package fr.hd3d.common.ui.client.error;

/**
 * Error Code for most common errors.
 * 
 * @author HD3D
 */
public class CommonErrors
{
    public static final int NO_CONFIG_FILE = 1;
    public static final int BAD_CONFIG_FILE = 2;
    public static final int NO_SERVICE_CONNECTION = 3;
    public static final int SERVER_ERROR = 4;
    public static final int RECORD_NOT_ON_SERVER = 5;
    public static final int NOT_AUTHENTIFIED = 6;
    public static final int NO_PERMISSION = 7;
    public static final int NO_DATA_TYPE_CONFIG_FILE = 8;
    public static final int SCRIPT_EXECUTION_FAILED = 9;
    public static final int TASK_TYPE_WITHOUT_ENTITY = 10;
}
