package fr.hd3d.common.ui.client.error;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.config.CommonConfig;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;


/**
 * ErrorDispatcher send error events with error code to the controllers.
 * 
 * @author HD3D
 */
public class ErrorDispatcher
{
    /**
     * Send error events with error code to the controllers.
     * 
     * @param errorType
     *            The error code.
     */
    public static void sendError(int errorType)
    {
        AppEvent errorEvent = new AppEvent(CommonEvents.ERROR);
        errorEvent.setData(CommonConfig.ERROR_EVENT_VAR_NAME, Integer.valueOf(errorType));

        EventDispatcher.forwardEvent(errorEvent);
    }

    /**
     * Send error events with error code and message to the controllers.
     * 
     * @param errorType
     *            The error code.
     * @param msg
     *            Message to display
     */
    public static void sendError(int errorType, String msg)
    {
        AppEvent errorEvent = new AppEvent(CommonEvents.ERROR);
        errorEvent.setData(CommonConfig.ERROR_MSG_EVENT_VAR_NAME, msg);
        errorEvent.setData(CommonConfig.ERROR_EVENT_VAR_NAME, Integer.valueOf(errorType));

        EventDispatcher.forwardEvent(errorEvent);
    }

    /**
     * Send error events with error code and messages to the controllers.
     * 
     * @param errorType
     *            The error code.
     * @param msg
     *            Message to display
     * @param technicalMsg
     *            Message to display
     */
    public static void sendError(int errorType, String msg, String technicalMsg)
    {
        AppEvent errorEvent = new AppEvent(CommonEvents.ERROR);
        errorEvent.setData(CommonConfig.ERROR_MSG_EVENT_VAR_NAME, msg);
        errorEvent.setData(CommonConfig.ERROR_TECH_MSG_EVENT_VAR_NAME, technicalMsg);
        errorEvent.setData(CommonConfig.ERROR_EVENT_VAR_NAME, Integer.valueOf(errorType));

        EventDispatcher.forwardEvent(errorEvent);
    }
}
