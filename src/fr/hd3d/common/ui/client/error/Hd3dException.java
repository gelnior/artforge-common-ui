package fr.hd3d.common.ui.client.error;

import com.extjs.gxt.ui.client.widget.MessageBox;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.logs.Logger;


/**
 * Exception which print a message box error.
 * 
 * @author HD3D
 * 
 */
public class Hd3dException extends Exception
{

    private static final long serialVersionUID = -7401249139535308535L;

    /** Message to print. */
    private final String message;

    /** Constant strings to display : dialog messages, button label... */
    // public static final CommonConstants CONSTANTS = GWT.create(CommonConstants.class);
    /**
     * Default Constructor.
     * 
     * @param message
     *            the message to print.
     */
    public Hd3dException(String message)
    {
        this.message = message;
    }

    /**
     * Display the message.
     */
    public void print()
    {
        if (GWT.isClient())
        {
            MessageBox.alert("Error", message, null);
        }
        else
        {
            Logger.log(message);
        }
    }
}
