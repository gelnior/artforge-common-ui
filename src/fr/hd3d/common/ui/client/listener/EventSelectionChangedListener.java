package fr.hd3d.common.ui.client.listener;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.SelectionChangedEvent;
import com.extjs.gxt.ui.client.event.SelectionChangedListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.EventDispatcher;


/**
 * A simple selection listener that forwards to event dispatcher a widget selection changed has occurred.
 * 
 * @author HD3D
 */
public class EventSelectionChangedListener<M extends ModelData> extends SelectionChangedListener<M>
{
    /** The event to forward. */
    protected AppEvent event;

    /**
     * Default constructor.
     * 
     * @param eventType
     *            The event to forward.
     */
    public EventSelectionChangedListener(EventType eventType)
    {
        this.event = new AppEvent(eventType);
    }

    /**
     * Constructor with AppEvent type.
     * 
     * @param event
     *            The event to forward.
     */
    public EventSelectionChangedListener(AppEvent event)
    {
        this.event = event;
    }

    /**
     * When selection changed event <i>se</i> occurs, this listener forwards the event type <i>event</i> to the event
     * dispatcher.
     */
    @Override
    public void selectionChanged(SelectionChangedEvent<M> se)
    {
        M model = se.getSelectedItem();
        this.event.setData(model);

        EventDispatcher.forwardEvent(event);
    }
}
