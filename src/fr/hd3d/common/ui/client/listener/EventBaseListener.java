package fr.hd3d.common.ui.client.listener;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Listener;

import fr.hd3d.common.ui.client.event.EventDispatcher;


/**
 * A simple event listener that forwards to event dispatcher the given event is triggered.
 * 
 * @author HD3D
 */
public class EventBaseListener implements Listener<BaseEvent>
{
    /** The event to forward. */
    protected EventType event;

    /**
     * Default constructor.
     * 
     * @param event
     *            The event to forward.
     */
    public EventBaseListener(EventType event)
    {
        this.event = event;
    }

    /**
     * When base event <i>be</i> occurs it forward the event type <i>event</i> to the event dispatcher.
     */
    public void handleEvent(BaseEvent be)
    {
        EventDispatcher.forwardEvent(event);
    }
}
