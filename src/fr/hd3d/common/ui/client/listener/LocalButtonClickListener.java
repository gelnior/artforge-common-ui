package fr.hd3d.common.ui.client.listener;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.mvc.controller.CommandController;


/**
 * Listener that forwards a given event to a given controller.
 * 
 * @author HD3D
 */
public class LocalButtonClickListener extends SelectionListener<ButtonEvent>
{

    /** The event to send. */
    protected final EventType event;
    /** The controller that handles events. */
    protected final CommandController controller;

    /**
     * Default constructor.
     * 
     * @param event
     *            The type of event to send.
     * @param controller
     *            The controller that will handle the event.
     */
    public LocalButtonClickListener(EventType event, CommandController controller)
    {
        this.event = event;
        this.controller = controller;
    }

    /**
     * When button event <i>be</i> occurs it sends the event type <i>event</i> to <i>controller</i>.
     */
    @Override
    public void componentSelected(ButtonEvent ce)
    {
        controller.handleEvent(new AppEvent(event));
    }
}
