package fr.hd3d.common.ui.client.listener;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.EventDispatcher;


/**
 * A simple selection listener that forwards to event dispatcher a button selection is triggered.
 * 
 * @author HD3D
 */
public class ButtonClickListener extends SelectionListener<ButtonEvent>
{
    /** The event to forward. */
    protected AppEvent event;

    /**
     * Default constructor.
     * 
     * @param eventType
     *            The event to forward.
     */
    public ButtonClickListener(EventType eventType)
    {
        this.event = new AppEvent(eventType);
    }

    /**
     * Constructor with AppEvent type.
     * 
     * @param event
     *            The event to forward.
     */
    public ButtonClickListener(AppEvent event)
    {
        this.event = event;
    }

    /**
     * When button event <i>be</i> occurs it forward the event type <i>event</i> to the event dispatcher.
     */
    @Override
    public void componentSelected(ButtonEvent ce)
    {
        EventDispatcher.forwardEvent(event);
    }
}
