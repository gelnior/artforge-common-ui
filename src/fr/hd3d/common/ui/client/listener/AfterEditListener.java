package fr.hd3d.common.ui.client.listener;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.EventDispatcher;


/**
 * Listener that raises a MVC event after a cell grid edition.
 * 
 * @author HD3D
 * 
 * @param <M>
 *            Type of data displayed in the grid.
 */
public class AfterEditListener<M extends ModelData> implements Listener<GridEvent<M>>
{
    /** The event to forward. */
    protected AppEvent event;

    /**
     * Default constructor.
     * 
     * @param eventType
     *            The event to forward.
     */
    public AfterEditListener(EventType eventType)
    {
        this.event = new AppEvent(eventType);
    }

    /**
     * Constructor with AppEvent type.
     * 
     * @param event
     *            The event to forward.
     */
    public AfterEditListener(AppEvent event)
    {
        this.event = event;
    }

    /**
     * When double click event <i>be</i> occurs it forward the event type <i>event</i> to the event dispatcher.
     */
    public void handleEvent(GridEvent<M> ge)
    {
        event.setData(ge.getModel());
        EventDispatcher.forwardEvent(event);
    }
}
