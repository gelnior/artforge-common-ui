package fr.hd3d.common.ui.client.listener;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.MenuEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.mvc.controller.CommandController;


/**
 * A simple selection listener that forwards to registered controller a menu button selection is triggered.
 * 
 * @author HD3D
 */
public class MenuButtonLocalClickListener extends SelectionListener<MenuEvent>
{

    /** The event to forward. */
    protected AppEvent event;

    /** The controller that will handle the event. */
    protected CommandController controller;

    /**
     * Default constructor.
     * 
     * @param controller
     *            The controller that will handle the event.
     * @param eventType
     *            The event to forward.
     */
    public MenuButtonLocalClickListener(CommandController controller, EventType eventType)
    {
        this.event = new AppEvent(eventType);
        this.controller = controller;
    }

    /**
     * Constructor with AppEvent type.
     * 
     * @param controller
     *            The controller that will handle the event.
     * @param event
     *            The event to forward.
     */
    public MenuButtonLocalClickListener(CommandController controller, AppEvent event)
    {
        this.event = event;
        this.controller = controller;
    }

    /**
     * When button event <i>be</i> occurs it forward the event type <i>event</i> to the registered controller.
     */
    @Override
    public void componentSelected(MenuEvent ce)
    {
        controller.handleEvent(event);
    }
}
