package fr.hd3d.common.ui.client.listener;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.EventDispatcher;


/**
 * A basic listener that can relay one event containing a data object.
 * 
 * @author HD3D
 * 
 */
public class EventRelay implements Listener<BaseEvent>
{

    /** the relayed event type */
    protected EventType type;
    /** the relayed object data */
    protected Object data;

    public EventRelay(EventType type, Object data)
    {
        this.type = type;
        this.data = data;
    }

    public void handleEvent(BaseEvent be)
    {
        AppEvent event = new AppEvent(type);
        if (data != null)
        {
            event.setData(data);
        }
        EventDispatcher.forwardEvent(event);
    }

}
