package fr.hd3d.common.ui.client.listener;

import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.mvc.controller.CommandController;
import fr.hd3d.common.ui.client.util.CollectionUtils;


public class LocalCellDoubleClickListener<M extends Hd3dModelData> implements Listener<GridEvent<M>>
{

    private final EventType event;
    private final CommandController controller;

    public LocalCellDoubleClickListener(EventType event, CommandController controller)
    {
        this.event = event;
        this.controller = controller;
    }

    public void handleEvent(GridEvent<M> ge)
    {
        List<M> selection = ge.getGrid().getSelectionModel().getSelectedItems();
        if (CollectionUtils.isNotEmpty(selection))
        {
            controller.handleEvent(new AppEvent(event, selection.get(0)));
        }
    }
}
