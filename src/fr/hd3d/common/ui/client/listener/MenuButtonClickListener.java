package fr.hd3d.common.ui.client.listener;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.MenuEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.EventDispatcher;


/**
 * A simple selection listener that forwards to event dispatcher a menu button selection is triggered.
 * 
 * @author HD3D
 */
public class MenuButtonClickListener extends SelectionListener<MenuEvent>
{
    /** The event to forward. */
    protected AppEvent event;

    /**
     * Default constructor.
     * 
     * @param eventType
     *            The event to forward.
     */
    public MenuButtonClickListener(EventType eventType)
    {
        this.event = new AppEvent(eventType);
    }

    /**
     * Constructor with AppEvent type.
     * 
     * @param event
     *            The event to forward.
     */
    public MenuButtonClickListener(AppEvent event)
    {
        this.event = event;
    }

    /**
     * When button event <i>be</i> occurs it forward the event type <i>event</i> to the event dispatcher.
     */
    @Override
    public void componentSelected(MenuEvent ce)
    {
        EventDispatcher.forwardEvent(event);
    }
}
