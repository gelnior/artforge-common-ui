package fr.hd3d.common.ui.client.listener;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.SelectionChangedEvent;
import com.extjs.gxt.ui.client.event.SelectionChangedListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.mvc.controller.CommandController;


/**
 * Listener that forwards a given event to a given controller.
 * 
 * @author HD3D
 */
public class LocalSelectionChangedListener<M extends Hd3dModelData> extends SelectionChangedListener<M>
{
    /** The event to send. */
    protected EventType event;
    /** The controller that handles events. */
    protected CommandController controller;

    /**
     * Default constructor.
     * 
     * @param event
     *            The type of event to send.
     * @param controller
     *            The controller that will handle the event.
     */
    public LocalSelectionChangedListener(EventType event, CommandController controller)
    {
        this.controller = controller;
        this.event = event;
    }

    /**
     * When selection changed event <i>be</i> occurs it sends the event type <i>event</i> to <i>controller</i>.
     */
    @Override
    public void selectionChanged(SelectionChangedEvent<M> se)
    {
        this.controller.handleEvent(new AppEvent(event, se.getSelection()));
    }
}
