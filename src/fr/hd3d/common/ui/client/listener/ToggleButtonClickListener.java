package fr.hd3d.common.ui.client.listener;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.button.ToggleButton;

import fr.hd3d.common.ui.client.event.EventDispatcher;


/**
 * A simple selection listener that forwards to event dispatcher a button selection is triggered.
 * 
 * @author HD3D
 */
public class ToggleButtonClickListener extends SelectionListener<ButtonEvent>
{
    /** The event to forward. */
    protected AppEvent event;

    /** The pressed toggle button */
    protected ToggleButton button;

    /**
     * Default constructor.
     * 
     * @param eventType
     *            The event to forward.
     */
    public ToggleButtonClickListener(EventType eventType, ToggleButton button)
    {
        this(new AppEvent(eventType), button);
    }

    /**
     * Constructor with AppEvent type.
     * 
     * @param event
     *            The event to forward.
     */
    public ToggleButtonClickListener(AppEvent event, ToggleButton button)
    {
        this.event = event;
        this.button = button;
    }

    /**
     * When button event <i>be</i> occurs it forward the event type <i>event</i> to the event dispatcher.
     */
    @Override
    public void componentSelected(ButtonEvent ce)
    {
        event.setData(Boolean.valueOf(button.isPressed()));
        EventDispatcher.forwardEvent(event);
    }
}
