package fr.hd3d.common.ui.client.listener;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.service.store.BaseStore;


/**
 * Listener connected to a store. When listener is activated, it reloads the store.
 * 
 * @author HD3D
 * 
 * @param <M>
 */
public class ReloadListener<M extends Hd3dModelData> extends SelectionListener<ButtonEvent>
{
    /** The store to reload. */
    protected final BaseStore<M> store;

    /**
     * Constructor : register store.
     * 
     * @param store
     *            The store to reload when listener is activated.
     */
    public ReloadListener(BaseStore<M> store)
    {
        this.store = store;
    }

    /**
     * When listener is activated, the store is reloaded.
     * 
     * @param ce
     *            The event that activates the listener.
     */
    @Override
    public void componentSelected(ButtonEvent ce)
    {
        this.store.reload();
    }
}
