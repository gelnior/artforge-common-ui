package fr.hd3d.common.ui.client.listener;

import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.KeyListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.google.gwt.event.dom.client.KeyCodes;

import fr.hd3d.common.ui.client.event.EventDispatcher;


/**
 * Listener to raise MVC event when enter key is up. It is useful to handle key up on text field.
 * 
 * @author HD3D
 */
public class EnterKeyListener extends KeyListener
{
    /** The event to forward to controllers. */
    private final AppEvent appEvent;

    /**
     * Default constructor.
     * 
     * @param appEvent
     *            The event to forward to controllers.
     */
    public EnterKeyListener(AppEvent appEvent)
    {
        this.appEvent = appEvent;
    }

    /**
     * Default constructor.
     * 
     * @param eventType
     *            The event type to forward to controllers.
     */
    public EnterKeyListener(EventType eventType)
    {
        this.appEvent = new AppEvent(eventType);
    }

    /**
     * When enter key is pressed, the setted event is forwarded to controllers.
     */
    @Override
    public void componentKeyUp(ComponentEvent event)
    {
        if (event.getKeyCode() == KeyCodes.KEY_ENTER)
        {
            EventDispatcher.forwardEvent(appEvent);
        }
    }
}
