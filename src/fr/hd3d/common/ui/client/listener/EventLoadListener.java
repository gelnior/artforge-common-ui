package fr.hd3d.common.ui.client.listener;

import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.LoadListener;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.service.store.BaseStore;


/**
 * A simple load listener that forwards to event dispatcher the given event when data are loaded.
 * 
 * @author HD3D
 */
public class EventLoadListener extends LoadListener
{
    /** The event to forward. */
    protected EventType event;
    private BaseStore<?> store;

    /**
     * Default constructor.
     * 
     * @param event
     *            The event to forward.
     */
    public EventLoadListener(EventType event)
    {
        this.event = event;
    }

    /**
     * Default constructor.
     * 
     * @param event
     *            The event to forward.
     */
    public EventLoadListener(EventType event, BaseStore<?> store)
    {
        this(event);
        this.store = store;
    }

    /**
     * When data are loaded, <i>event</i> is forwarded to the event dispatcher.
     * 
     * @see com.extjs.gxt.ui.client.event.LoadListener#loaderLoad(com.extjs.gxt.ui.client.data.LoadEvent)
     */
    @Override
    public void loaderLoad(LoadEvent le)
    {
        EventDispatcher.forwardEvent(event, store);
    }
}
