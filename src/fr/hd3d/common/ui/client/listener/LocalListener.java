package fr.hd3d.common.ui.client.listener;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.mvc.controller.CommandController;


public class LocalListener implements Listener<BaseEvent>
{

    private final EventType event;
    private final CommandController controller;

    public LocalListener(EventType event, CommandController controller)
    {
        this.event = event;
        this.controller = controller;
    }

    public void handleEvent(BaseEvent be)
    {
        controller.handleEvent(new AppEvent(event));
    }

}
