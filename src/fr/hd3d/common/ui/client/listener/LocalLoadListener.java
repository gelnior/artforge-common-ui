package fr.hd3d.common.ui.client.listener;

import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.LoadListener;

import fr.hd3d.common.ui.client.mvc.controller.CommandController;


/**
 * Listener that forwards a given event to a given controller.
 * 
 * @author HD3D
 */
public class LocalLoadListener extends LoadListener
{
    /** The event to send. */
    private final EventType event;
    /** The controller that handles events. */
    private final CommandController controller;

    /**
     * Default constructor.
     * 
     * @param event
     *            The type of event to send.
     * @param controller
     *            The controller that will handle the event.
     */
    public LocalLoadListener(EventType event, CommandController controller)
    {
        this.event = event;
        this.controller = controller;
    }

    /**
     * When event <i>be</i> occurs it sends the event type <i>event</i> to <i>controller</i>.
     */
    @Override
    public void loaderLoad(LoadEvent le)
    {
        this.controller.handleEvent(event);
    }
}
