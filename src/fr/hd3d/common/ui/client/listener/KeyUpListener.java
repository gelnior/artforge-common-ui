package fr.hd3d.common.ui.client.listener;

import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.KeyListener;
import com.extjs.gxt.ui.client.widget.form.Field;

import fr.hd3d.common.ui.client.event.EventDispatcher;


/**
 * A simple key up listener that forwards to event dispatcher the given event when a key is up.
 * 
 * @author HD3D
 */
public class KeyUpListener extends KeyListener
{
    /** The event to forward. */
    protected EventType eventType;

    /**
     * Default constructor.
     * 
     * @param eventType
     *            The event to forward.
     */
    public KeyUpListener(EventType eventType)
    {
        this.eventType = eventType;
    }

    /**
     * When key is up, <i>event</i> is forwarded to the event dispatcher.
     * 
     * @see com.extjs.gxt.ui.client.event.LoadListener#loaderLoad(com.extjs.gxt.ui.client.data.LoadEvent)
     */
    @Override
    public void componentKeyUp(ComponentEvent event)
    {
        Object value = ((Field<?>) event.getSource()).getValue();
        EventDispatcher.forwardEvent(eventType, value);
    }
}
