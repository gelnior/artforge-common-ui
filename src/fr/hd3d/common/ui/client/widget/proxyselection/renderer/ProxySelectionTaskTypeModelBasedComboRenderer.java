package fr.hd3d.common.ui.client.widget.proxyselection.renderer;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.store.Store;
import com.extjs.gxt.ui.client.store.StoreFilter;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.widget.proxyselection.ProxySelectionGridModelData;
import fr.hd3d.common.ui.client.widget.proxyselection.event.ProxySelectionEvents;
import fr.hd3d.common.ui.client.widget.proxyselection.widget.ProxySelectionTaskTypeComboBox;


public class ProxySelectionTaskTypeModelBasedComboRenderer implements GridCellRenderer<ProxySelectionGridModelData>
{
    // final
    ProxySelectionTaskTypeComboBox combo;

    public Object render(final ProxySelectionGridModelData model, String property, ColumnData config, int rowIndex,
            int colIndex, ListStore<ProxySelectionGridModelData> store, Grid<ProxySelectionGridModelData> grid)
    {
        combo = new ProxySelectionTaskTypeComboBox() {
            @Override
            public void setValue(TaskTypeModelData value)
            {
                this.disableEvents(true);
                super.setValue(value);
                this.disableEvents(false);
            }
        };
        combo.getStore().addFilter(new StoreFilter<TaskTypeModelData>() {

            public boolean select(Store<TaskTypeModelData> store, TaskTypeModelData parent, TaskTypeModelData item,
                    String property)
            {
                return model.isTaskTypeAvailable(item.getId());
            }
        });
        combo.getStore().applyFilters(TaskTypeModelData.NAME_FIELD);

        setvalue: for (TaskTypeModelData ttmd : combo.getStore().getModels())
            if (ttmd != null && ttmd.getId().equals(model.getTaskType()))
            {
                // combo.disableEvents(true);
                combo.setValue(ttmd);
                // combo.disableEvents(false);
                System.out.println("Value set to " + ttmd.getName());
                break setvalue;
            }

        combo.removeAllListeners();
        combo.addListener(Events.SelectionChange, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                if (combo.getValue() != null && combo.getValue().getId() != null
                        && combo.getValue().getId().equals(model.getTaskType()))
                    return;
                AppEvent event = new AppEvent(ProxySelectionEvents.TASK_TYPE_SELECTED);
                event.setData("source", model);
                event.setData("data", combo.getValue());
                EventDispatcher.forwardEvent(event);
            }
        });
        // combo.setValueById(model.getTaskType());
        return combo;
    }

}
