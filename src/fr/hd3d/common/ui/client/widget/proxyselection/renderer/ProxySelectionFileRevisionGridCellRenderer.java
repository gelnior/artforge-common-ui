package fr.hd3d.common.ui.client.widget.proxyselection.renderer;

import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;

import fr.hd3d.common.ui.client.widget.proxyselection.ProxySelectionGridModelData;
import fr.hd3d.common.ui.client.widget.proxyselection.widget.ProxySelectionFileRevisionComboBox;


public class ProxySelectionFileRevisionGridCellRenderer implements GridCellRenderer<ProxySelectionGridModelData>
{
    public Object render(ProxySelectionGridModelData model, String property,
            com.extjs.gxt.ui.client.widget.grid.ColumnData config, int rowIndex, int colIndex,
            com.extjs.gxt.ui.client.store.ListStore<ProxySelectionGridModelData> store,
            com.extjs.gxt.ui.client.widget.grid.Grid<ProxySelectionGridModelData> grid)
    {

        ProxySelectionFileRevisionComboBox combo = new ProxySelectionFileRevisionComboBox();
        combo.setStore(model.getFileRevisionStore());
        return combo;
    };
}
