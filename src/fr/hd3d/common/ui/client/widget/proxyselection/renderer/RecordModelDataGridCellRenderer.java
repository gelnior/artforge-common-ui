package fr.hd3d.common.ui.client.widget.proxyselection.renderer;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.asset.AssetRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.asset.FileRevisionModelData;
import fr.hd3d.common.ui.client.widget.proxyselection.ProxySelectionGridModelData;


public class RecordModelDataGridCellRenderer implements GridCellRenderer<ProxySelectionGridModelData>
{

    private String getTooltipText(ProxySelectionGridModelData model)
    {
        StringBuilder builder = new StringBuilder();

        builder.append("Asset [");
        AssetRevisionModelData actualAssetRevisionModelData = model.getActualAssetRevisionModelData();
        if (actualAssetRevisionModelData != null)
            builder.append(actualAssetRevisionModelData.getName());
        else
            builder.append("NULL");

        builder.append("] File [");
        FileRevisionModelData actualFileRevisionModelData = model.getActualFileRevisionModelData();
        if (actualFileRevisionModelData != null)
            builder.append(actualFileRevisionModelData.getKey());
        else
            builder.append("NULL");

        builder.append("] ProxyType [");
        builder.append(model.getProxyTypeId());

        builder.append("] Proxy [");
        builder.append(model.getProxyModelData());
        builder.append("] ");

        return builder.toString();
    }

    public Object render(ProxySelectionGridModelData model, String property, ColumnData config, int rowIndex,
            int colIndex, ListStore<ProxySelectionGridModelData> store, Grid<ProxySelectionGridModelData> grid)
    {
        RecordModelData object = model.get(property);
        if (object == null)
            return "?";

        String formattedName = model.getName();
        Long proxyModelData = model.getProxyModelData();

        if (proxyModelData == null || proxyModelData == -1L)
        {
            return "<span style=\"color:#D11;\" title=\""
                    + getTooltipText(model)
                    + "\"><img style=\"float:right;position:relative;margin-right:0;\"  src=\"images/explorateur/delete.png\" />"
                    + formattedName + "</span>";
        }
        else
        {
            return "<span style=\"font-weight:bold;\" title=\""
                    + getTooltipText(model)
                    + "\"> <img style=\"float:right;position:relative;margin-right:0;\" src=\"images/explorateur/validation_edit.png\" />"
                    + formattedName + "</span>";
        }

    }
}
