package fr.hd3d.common.ui.client.widget.proxyselection.widget;

import java.util.List;

import fr.hd3d.common.ui.client.modeldata.asset.ProxyTypeModelData;
import fr.hd3d.common.ui.client.modeldata.reader.ProxyTypeReader;
import fr.hd3d.common.ui.client.service.parameter.InConstraint;
import fr.hd3d.common.ui.client.widget.ModelDataComboBox;
import fr.hd3d.common.ui.client.widget.proxyplayer.util.ProxyMimeType;
import fr.hd3d.common.ui.client.widget.proxyselection.event.ProxySelectionEvents;


/**
 * 
 * @author guillaume-maucomble l
 */
public class ProxySelectionProxyTypeComboBox extends ModelDataComboBox<ProxyTypeModelData>
{

    public ProxySelectionProxyTypeComboBox()
    {
        super(new ProxyTypeReader());
        initListeners();
        List<String> openTypeValues = ProxyMimeType.getAllowedTypes();
        store.addParameter(new InConstraint(ProxyTypeModelData.OPENTYPE_FIELD, openTypeValues));
    }

    protected void initListeners()
    {
        setSelectionChangedEvent(ProxySelectionEvents.ALL_PROXY_TYPE_SELECTED);
    }

    @Override
    protected void setStyles()
    {
        super.setStyles();
        this.setSelectOnFocus(false);
        this.setTypeAhead(false);
        this.setForceSelection(false);
        this.setHideTrigger(false);
        this.setEditable(true);
        this.setEmptyText("Select Proxy Type...");
        this.setDisplayField(ProxyTypeModelData.NAME_FIELD);

    }

}
