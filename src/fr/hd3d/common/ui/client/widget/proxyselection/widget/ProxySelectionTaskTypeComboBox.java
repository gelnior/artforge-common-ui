package fr.hd3d.common.ui.client.widget.proxyselection.widget;

import java.util.List;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;

import fr.hd3d.common.ui.client.modeldata.reader.TaskTypeReader;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.widget.ModelDataComboBox;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;


public class ProxySelectionTaskTypeComboBox extends ModelDataComboBox<TaskTypeModelData>
{

    private List<Long> ids;

    public ProxySelectionTaskTypeComboBox(List<Long> taskTypesIds)
    {
        super(new TaskTypeReader());
        this.ids = (taskTypesIds == null || taskTypesIds.isEmpty()) ? MainModel.getCurrentProject().getTaskTypeIDs()
                : taskTypesIds;
        initListeners();
    }

    public ProxySelectionTaskTypeComboBox()
    {
        this(null);
    }

    private void initListeners()
    {
        final Listener<BaseEvent> loadingListener = new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                getServiceStore().clearParameters();
                getServiceStore().addInConstraint(
                        TaskTypeModelData.ID_FIELD,
                        (ProxySelectionTaskTypeComboBox.this.ids == null ? MainModel.getCurrentProject()
                                .getTaskTypeIDs() : ProxySelectionTaskTypeComboBox.this.ids));
                getServiceStore().getLoader().load();
                ProxySelectionTaskTypeComboBox.this.removeListener(Events.Expand, this);
            }
        };

        // this.addListener(Events.OnLoad, loadingListener);
        this.addListener(Events.Expand, loadingListener);
        // this.setSelectionChangedEvent(ProxySelectionEvents.ALL_TASK_TYPE_SELECTED);
    }

    @Override
    protected void setStyles()
    {
        super.setStyles();
        this.setSelectOnFocus(false);
        this.setTypeAhead(false);
        this.setForceSelection(false);
        this.setHideTrigger(false);
        this.setEditable(true);
        this.setEmptyText("Select Task Type...");
        this.setDisplayField(TaskTypeModelData.NAME_FIELD);
        this.setTemplate(getXTemplate());
    }

    /**
     * @return Combo box values display style.
     */
    public native String getXTemplate() /*-{
                 return  [ 
                 '<tpl for=".">', 
                 '<div class="x-combo-list-item"> <span style="background-color:{color}; font-size: 13px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;<span style="font-weight: bold; font-size : 12px;">&nbsp;{name}&nbsp;&nbsp;</span> </div>', 
                 '</tpl>' 
                 ].join("");
                 }-*/;

    public List<Long> getIds()
    {
        return ids;
    }

    public void setIds(List<Long> ids)
    {
        this.ids = ids;
    }
}
