package fr.hd3d.common.ui.client.widget.proxyselection;

import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.MessageBox;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.asset.FileRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.asset.ProxyTypeModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.widget.proxyselection.event.ProxySelectionEvents;


/**

 * 
 */
public class ProxySelectionDialogController extends MaskableController
{

    ProxySelectionDialogModel model;
    ProxySelectionDialog view;

    public ProxySelectionDialogController(ProxySelectionDialog view, ProxySelectionDialogModel model)
    {
        this.model = model;
        this.view = view;
        this.registerEvents();
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();

        if (type == ProxySelectionEvents.ALL_TASK_TYPE_SELECTED)
        {
            // System.out.println("onAllTaskTypeSelected");
            onAllTaskTypeSelected(event);
        }
        else if (type == ProxySelectionEvents.ALL_PROXY_TYPE_SELECTED)
        {
            // System.out.println("onAllProxyTypeSelected");
            onAllProxyTypeSelected(event);
        }
        else if (type == ProxySelectionEvents.TASK_TYPE_SELECTED)
        {
            // System.out.println("onTaskTypeSelected");
            onTaskTypeSelected(event);
        }
        else if (type == ProxySelectionEvents.FILE_REVISION_SELECTED)
        {
            // System.out.println("onFileRevisionSelected");
            onFileRevisionSelected(event);
        }
        else if (type == ProxySelectionEvents.MODEL_CHANGED_EVENT)
        {
            // System.out.println("onModelChanged");
            onModelChanged(event);
        }
        else if (type == ProxySelectionEvents.MODEL_LOADING_STARTED)
        {
            // System.out.println("onModelLoadingStarted");
            onModelLoadingStarted(event);
        }
        else if (type == ProxySelectionEvents.MODEL_LOADING_ENDED)
        {
            // System.out.println("onModelLoadingEnded");
            onModelLoadingEnded(event);
        }
        else if (type == ProxySelectionEvents.OK_BUTTON_CLICKED)
        {
            // System.out.println("onOkButtonClicked");
            onOkButtonClicked(event);
        }

    }

    /**
     * Event : applies TaskTypeModelData to all models.
     * 
     * @param event
     */
    private void onAllTaskTypeSelected(AppEvent event)
    {
        TaskTypeModelData value = event.getData(ProxySelectionGridModelData.TASK_TYPE_MODEL_DATA_FIELD);
        if (value == null)
            value = event.getData();

        if (value == null)
            return;
        model.applyAllTaskTypes(value);
    }

    /**
     * Ask model for proxy type selection
     * 
     * @param event
     */
    private void onAllProxyTypeSelected(AppEvent event)
    {
        ProxyTypeModelData value = event.getData(ProxySelectionGridModelData.PROXY_TYPE_MODEL_DATA_FIELD);
        if (value == null)
            value = event.getData();
        if (value == null)
            return;
        model.applyAllProxyType(value);
    }

    private void onTaskTypeSelected(AppEvent event)
    {
        ProxySelectionGridModelData model = event.getData("source");
        if (model == null)
            return;
        TaskTypeModelData taskTypeValue = event.getData("data");
        if (taskTypeValue == null)
            return;
        model.setTaskTypeModelDataId(taskTypeValue.getId());
    }

    private void onFileRevisionSelected(AppEvent event)
    {
        ProxySelectionGridModelData model = event.getData("source");
        if (model == null)
            return;
        FileRevisionModelData fileRevisionValue = event.getData("data");
        model.setFileRevisionModelData(fileRevisionValue.getId());
    }

    private void onModelChanged(AppEvent event)
    {
        ProxySelectionGridModelData data = event.getData();
        if (data == null)
            data = event.getData("source");
        if (data == null)
            return;
        view.getGrid().getView().refresh(false);
    }

    private void onModelLoadingStarted(AppEvent event)
    {
        view.setLoadingState(true);
    }

    private void onModelLoadingEnded(AppEvent event)
    {
        view.setLoadingState(false);
    }

    protected void onOkButtonClicked(AppEvent event)
    {
        final MessageBox newPlaylistPrompt = MessageBox.prompt("Add new playlist", "Enter new playlist name");

        newPlaylistPrompt.addCallback(new Listener<MessageBoxEvent>() {
            public void handleEvent(MessageBoxEvent be)
            {
                String buttonClicked = be.getButtonClicked().getText().toLowerCase();
                if (MessageBox.OK.toLowerCase().equals(buttonClicked) == false)
                {
                    return;
                }
                final String newName = be.getValue();
                if (newName == null || "".equals(newName))
                {
                    onOkButtonClicked(null);
                    return;
                }

                List<ProxySelectionGridModelData> collection = ProxySelectionDialogController.this.model
                        .getActualStore();
                AppEvent playlistCreationEvent = new AppEvent(CommonEvents.PLAYLIST_CREATION_REQUIRED);
                playlistCreationEvent.setData("name", newName);
                playlistCreationEvent.setData("collection", collection);
                EventDispatcher.forwardEvent(playlistCreationEvent);
            };
        });

    }

    private void registerEvents()
    {
        registerEventTypes(ProxySelectionEvents.ALL_TASK_TYPE_SELECTED, //
                ProxySelectionEvents.ALL_PROXY_TYPE_SELECTED, //
                ProxySelectionEvents.TASK_TYPE_SELECTED,//
                ProxySelectionEvents.FILE_REVISION_SELECTED,//
                ProxySelectionEvents.MODEL_CHANGED_EVENT, //
                ProxySelectionEvents.MODEL_LOADING_STARTED, //
                ProxySelectionEvents.MODEL_LOADING_ENDED, //
                ProxySelectionEvents.OK_BUTTON_CLICKED);
    }

}
