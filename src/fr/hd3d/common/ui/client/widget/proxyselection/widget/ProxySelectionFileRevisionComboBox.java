package fr.hd3d.common.ui.client.widget.proxyselection.widget;

import java.util.List;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;

import fr.hd3d.common.ui.client.modeldata.asset.FileRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.reader.FileRevisionReader;
import fr.hd3d.common.ui.client.service.parameter.OrderBy;
import fr.hd3d.common.ui.client.widget.ModelDataComboBox;


public class ProxySelectionFileRevisionComboBox extends ModelDataComboBox<FileRevisionModelData>
{

    private List<Long> ids;

    public ProxySelectionFileRevisionComboBox(List<Long> fileRevisionIds)
    {
        super(new FileRevisionReader());
        orderBy = new OrderBy(FileRevisionModelData.LAST_OPERATION_DATE_FIELD);
        this.ids = (fileRevisionIds == null || fileRevisionIds.isEmpty()) ? null : fileRevisionIds;
        initListeners();
    }

    public ProxySelectionFileRevisionComboBox()
    {
        this(null);
    }

    private void initListeners()
    {
        final Listener<BaseEvent> loadingListener = new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                // getServiceStore().clearParameters();
                // getServiceStore().addInConstraint(
                // TaskTypeModelData.ID_FIELD,
                // (ProxySelectionFileRevisionComboBox.this.ids == null ? MainModel.getCurrentProject()
                // .getTaskTypeIDs() : ProxySelectionTaskTypeComboBox.this.ids));
                getServiceStore().getLoader().load();
                ProxySelectionFileRevisionComboBox.this.removeListener(Events.Expand, this);
            }
        };

        // this.addListener(Events.OnLoad, loadingListener);
        this.addListener(Events.Expand, loadingListener);
        // this.setSelectionChangedEvent(ProxySelectionEvents.ALL_TASK_TYPE_SELECTED);
    }

    @Override
    protected void setStyles()
    {
        super.setStyles();
        this.setSelectOnFocus(false);
        this.setTypeAhead(false);
        this.setForceSelection(false);
        this.setHideTrigger(false);
        this.setEditable(true);
        this.setEmptyText("Select File Revision...");
        this.setDisplayField(FileRevisionModelData.KEY_FIELD);
        this.setTemplate(getXTemplate());
    }

    /**
     * @return Combo box values display style.
     */
    public native String getXTemplate() /*-{
              return [
              '<tpl for=".">',
              '<div class="x-combo-list-item"> {key} (<span style="font-weight: bold;">{state}</span>) </div>',
              '</tpl>'
              ].join("");
              }-*/;

    public List<Long> getIds()
    {
        return ids;
    }

    public void setIds(List<Long> ids)
    {
        this.ids = ids;
    }

}
