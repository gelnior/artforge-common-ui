package fr.hd3d.common.ui.client.widget.proxyselection;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.EditorGrid;


/**
 * 
 */
public class ProxySelectionGrid extends EditorGrid<ProxySelectionGridModelData>
{

    public ProxySelectionGrid(ListStore<ProxySelectionGridModelData> listStore, ColumnModel cm)
    {
        super(listStore, cm);
        this.initUi();
    }

    private void initUi()
    {
        setColumnReordering(false);
        // setColumnResize(false);
        setStripeRows(true);
        setBorders(false);
        setHeight(300);
        setWidth(630);
        getView().setShowInvalidCells(true);
        getView().setShowDirtyCells(true);
        setAutoExpandColumn(ProxySelectionGridModelData.WORK_OBJECT_MODEL_DATA_FIELD);
    }

}
