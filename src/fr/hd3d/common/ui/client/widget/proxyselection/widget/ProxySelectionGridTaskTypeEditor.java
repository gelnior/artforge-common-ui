package fr.hd3d.common.ui.client.widget.proxyselection.widget;

import com.extjs.gxt.ui.client.widget.grid.CellEditor;


public class ProxySelectionGridTaskTypeEditor extends CellEditor
{

    private final ProxySelectionTaskTypeComboBox combo;

    public ProxySelectionGridTaskTypeEditor(ProxySelectionTaskTypeComboBox combo)
    {
        super(combo);
        this.combo = combo;
    }

    public ProxySelectionTaskTypeComboBox getCombo()
    {
        return combo;
    }

}
