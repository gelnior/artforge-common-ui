package fr.hd3d.common.ui.client.widget.proxyselection;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.BaseListLoadResult;
import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.LoadListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.ListStore;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.WorkObjectModelData;
import fr.hd3d.common.ui.client.modeldata.asset.AssetRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.asset.FileRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.asset.ProxyModelData;
import fr.hd3d.common.ui.client.modeldata.asset.ProxyTypeModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.modeldata.reader.AssetRevisionReader;
import fr.hd3d.common.ui.client.modeldata.reader.FileRevisionReader;
import fr.hd3d.common.ui.client.modeldata.reader.ProxyReader;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.InConstraint;
import fr.hd3d.common.ui.client.service.parameter.OrderBy;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.widget.proxyselection.event.ProxySelectionEvents;


/**
 * This enhanced RecordModelData gathers all the objects from a {@link WorkObjectModelData} to a {@link ProxyModelData}.
 * 
 * @author guillaume-maucomble
 * 
 */
@SuppressWarnings("serial")
public class ProxySelectionGridModelData extends RecordModelData
{
    /**
     * {@link RecordModelData} on which this object depends. This should be only {@link ShotModelData} or
     * {@link ConstituentModelData}.
     */
    public static final String WORK_OBJECT_MODEL_DATA_FIELD = "recordModelData";

    /** {@link TaskTypeModelData} id on which {@link AssetRevisionModelData} depends. */
    public static final String TASK_TYPE_MODEL_DATA_FIELD = "taskTypeModelData";

    /** {@link AssetRevisionModelData} object id on which {@link FileRevisionModelData} depends. */
    public static final String ASSET_REVISION_MODEL_DATA_FIELD = "assetRevisionModelData";

    /** {@link FileRevisionModelData} object id on which {@link ProxyModelData} depends. */
    public static final String FILE_REVISION_MODEL_DATA_FIELD = "fileRevisionModelData";

    /** {@link ProxyModelData} Long Id for {@link ProxySelectionGridModelData} */
    public static final String PROXY_MODEL_DATA_FIELD = "proxyModelData";

    /** The map contains a Long id for {@link ProxyTypeModelData} */
    public static final String PROXY_TYPE_MODEL_DATA_FIELD = "proxyTypeModelData";
    /** TODO doc */
    private static List<String> allowedClasses;
    /** TODO doc */
    private final ServiceStore<FileRevisionModelData> fileRevisionStore = new ServiceStore<FileRevisionModelData>(
            new FileRevisionReader());
    /** TODO doc */
    private final ServiceStore<ProxyModelData> proxyStore = new ServiceStore<ProxyModelData>(new ProxyReader());
    /** List of available distinct task types for this work object. */
    private final List<Long> availableTaskTypesIds = new ArrayList<Long>();
    /** List of available asset revisions. */
    private List<AssetRevisionModelData> availableAssetRevisions = new ArrayList<AssetRevisionModelData>();
    /** TODO doc */
    // private Long currentFileRevisionId = -1L;
    /** TODO doc */
    private final Long tmpTaskTypeId;
    private boolean tmpTaskTypeIdApplied = false;
    /** TODO doc */
    private final Long tmpFileRevisionId;
    private boolean tmpFileRevisionIdApplied = false;

    static
    {
        if (allowedClasses == null)
        {
            allowedClasses = new ArrayList<String>();
            allowedClasses.add(ShotModelData.SIMPLE_CLASS_NAME);
            allowedClasses.add(ConstituentModelData.SIMPLE_CLASS_NAME);
        }
    }

    @Override
    public String getName()
    {
        if (ShotModelData.SIMPLE_CLASS_NAME.equals(getWorkObjectModelData().getSimpleClassName()))
        {
            ShotModelData shotModelData = (ShotModelData) getWorkObjectModelData();
            return shotModelData.getSequencesNames() + shotModelData.getName();
        }
        else
            return getWorkObjectModelData().getName();
    }

    /**
     * Builds a new ProxySelectionGridModelData object based on a WorkObjectModelData. All the fields except
     * {@link WorkObjectModelData} are set to null.
     * 
     * Depending on the nature of
     * 
     * @param workObjectModelData
     *            WorkObjectModelData object
     */
    public ProxySelectionGridModelData(RecordModelData workObjectModelData)
    {
        this(workObjectModelData, null, null);
        tmpTaskTypeIdApplied = true;
        tmpFileRevisionIdApplied = true;
    }

    public ProxySelectionGridModelData(RecordModelData workObjectModelData, Long defaultTaskTypeId,
            Long defaultFileRevisionId)
    {
        if (!allowedClasses.contains(workObjectModelData.getSimpleClassName()))
            throw new IllegalArgumentException("This object is not allowed in a Playlist.");

        startEditing();

        tmpTaskTypeId = defaultTaskTypeId;
        tmpFileRevisionId = defaultFileRevisionId;

        set(WORK_OBJECT_MODEL_DATA_FIELD, workObjectModelData);
        loadServiceAssetRevision();
    }

    public RecordModelData getWorkObjectModelData()
    {
        return get(WORK_OBJECT_MODEL_DATA_FIELD);
    }

    /**
     * Loads WorkObject from following web service address :
     * <ul>
     * <li>Shot : /projects/{projectId}/sequences/{sequenceId}/shots/{shotId}/assets</li>
     * <li>Constituent : /projects/{projectId}/categories/{categoryId}/constituents/{constituentId}/assets/</li>
     * </ul>
     * 
     * The loading is asynchronous. Thus loadShot() launches {@link ProxySelectionEvents#ALL_ASSET_REVISION_LOADED} when
     * data is loaded. The event' data is compounded of two parts :
     * <ul>
     * <li>"source" : this {@link ProxySelectionGridModelData}</li>
     * <li>"list" : a List of AssetRevisionModelData.</li>
     * </ul>
     * 
     * This does nothing if {@link RecordModelData} in parameter is not a {@link ShotModelData} instance.
     * 
     * @param workObjectModelData
     *            ShotModelData
     */
    private void loadServiceAssetRevision()
    {
        ServiceStore<AssetRevisionModelData> assetRevisionStore = new ServiceStore<AssetRevisionModelData>(
                new AssetRevisionReader());

        assetRevisionStore.setPath(getWorkObjectModelData().getDefaultPath() + ServicesPath.ASSETS);
        assetRevisionStore.getLoader().addLoadListener(new LoadListener() {
            @SuppressWarnings("unchecked")
            @Override
            public void loaderLoad(LoadEvent le)
            {
                super.loaderLoad(le);
                BaseListLoadResult<AssetRevisionModelData> baseListLoadResult = (BaseListLoadResult<AssetRevisionModelData>) (le
                        .getData());
                ProxySelectionGridModelData.this.setAvailableAssetRevisions(baseListLoadResult.getData());
            }
        });
        assetRevisionStore.getLoader().load();
    }

    private void updateFileRevisionStore()
    {
        if (!(exists(availableAssetRevisions) && exists(availableTaskTypesIds)))
            return;

        Long assetRevisionModelData = getAssetRevisionModelData();

        if (!exists(assetRevisionModelData))
        {
            // Do something here ?
        }
        else
        {
            setFileRevisionModelData(-1L); // remove current file
            fileRevisionStore.removeAll(); // empty file store
            setProxyModelData(-1L); // remove proxy
            loadServiceFileRevision(assetRevisionModelData);
        }
    }

    private void loadServiceFileRevision(Long assetRevisionId)
    {
        fileRevisionStore.setPath(ServicesPath.ASSET_REVISIONS + assetRevisionId + "/" + ServicesPath.FILE_REVISIONS);
        fileRevisionStore.clearParameters();
        fileRevisionStore.addEqConstraint(FileRevisionModelData.ASSET_REVISION_FIELD, getAssetRevisionModelData());
        fileRevisionStore.addParameter(new OrderBy(FileRevisionModelData.LAST_OPERATION_DATE_FIELD));
        fileRevisionStore.getLoader().addLoadListener(new LoadListener() {
            @Override
            public void loaderLoad(LoadEvent le)
            {
                super.loaderLoad(le);
                if (!tmpFileRevisionIdApplied)
                {
                    setFileRevisionModelData(tmpFileRevisionId);
                    tmpFileRevisionIdApplied = true;
                }
                else
                {
                    if (fileRevisionStore.getCount() > 0)
                        setFileRevisionModelData(fileRevisionStore.getAt(0).getId());
                }
                if (exists(getFileRevisionModelDataId()))
                    ProxySelectionGridModelData.this.loadProxies();
            }
        });

        fileRevisionStore.getLoader().load();
    }

    /**
     * Returns the Long id of the current fileRevisionModleData.
     * 
     * @return the Long id of the current fileRevisionModleData
     */
    public Long getFileRevisionModelDataId()
    {
        return get(FILE_REVISION_MODEL_DATA_FIELD);
    }

    /**
     * Returns the current FileRevisionModelData selected object.
     * 
     * @return the current FileRevisionModelData selected object, or null otherwise
     */
    public FileRevisionModelData getActualFileRevisionModelData()
    {
        if (fileRevisionStore == null || fileRevisionStore.getCount() == 0 || !exists(getFileRevisionModelDataId()))
            return null;
        return fileRevisionStore.findModel(FileRevisionModelData.ID_FIELD, getFileRevisionModelDataId());
    }

    @SuppressWarnings("unchecked")
    private void loadProxies()
    {
        if (!exists(fileRevisionStore) || !exists(getFileRevisionModelDataId()))
            return;

        proxyStore.setPath(ServicesPath.PROXIES);
        proxyStore.clearParameters();
        proxyStore.addParameter(new InConstraint("id", getActualFileRevisionModelData().getProxies()));
        proxyStore.getLoader().addLoadListener(new LoadListener() {
            @Override
            public void loaderLoad(LoadEvent le)
            {
                super.loaderLoad(le);
                // System.out.println(getName() + "::" + "LOAD PROXIES \t (" + proxyStore.getCount() + " element(s))");
                setProxyModelData();
            }
        });
        proxyStore.getLoader().load();
    }

    private void setProxyModelData()
    {
        boolean proxyFound = false;
        search: for (ProxyModelData pmd : proxyStore.getModels())
            if (pmd != null && pmd.getProxyTypeId() != null && pmd.getProxyTypeId().equals(getProxyTypeId()))
            {
                proxyFound = true;
                setProxyModelData(pmd);
                break search;
            }
        if (proxyFound == false)
            setProxyModelData(-1L);
    }

    /**
     * Sets TaskType Id value.
     * 
     * @param taskTypeModelDataId
     *            id of the taskType
     */
    public void setTaskTypeModelDataId(Long taskTypeModelDataId)
    {
        if (taskTypeModelDataId == null)
            taskTypeModelDataId = -1L;
        if (getTaskType() != null && getTaskType().equals(taskTypeModelDataId)
                || !isTaskTypeAvailable(taskTypeModelDataId))
            return;

        // System.out.println(getName() + "::" + "SET TASK_TYPE_MODEL_DATA_FIELD \t  " + taskTypeModelDataId);
        set(TASK_TYPE_MODEL_DATA_FIELD, taskTypeModelDataId);

        // We suppose we only have one asset per task type. The first we find is thus the correct one.
        for (AssetRevisionModelData object : availableAssetRevisions)
        {
            if (object.getTaskTypeId() != null && taskTypeModelDataId != null
                    && object.getTaskTypeId().longValue() == taskTypeModelDataId.longValue())
            {
                setAssetRevisionModelData(object);
                return;
            }
        }

        // No assetRevisionModelData was found - this case should never happen
        setAssetRevisionModelData(-1L);

    }

    public Long getAssetRevisionModelData()
    {
        return get(ASSET_REVISION_MODEL_DATA_FIELD);
    }

    public AssetRevisionModelData getActualAssetRevisionModelData()
    {
        if (availableAssetRevisions == null || availableAssetRevisions.isEmpty() == true
                || getAssetRevisionModelData() == null || getAssetRevisionModelData() < 0)
            return null;

        for (AssetRevisionModelData armd : availableAssetRevisions)
        {
            if (armd != null && armd.getId().equals(getAssetRevisionModelData()))
                return armd;
        }

        return null;
    }

    public void setAssetRevisionModelData(AssetRevisionModelData assetRevisionModelData)
    {
        setAssetRevisionModelData(assetRevisionModelData.getId());
    }

    public void setAssetRevisionModelData(Long assetRevisionModelDataId)
    {
        if (getAssetRevisionModelData() != null && getAssetRevisionModelData().equals(assetRevisionModelDataId))
            return;

        // System.out.println(getName() + "::" + "SET ASSET_REVISION_MODEL_DATA_FIELD \t " + assetRevisionModelDataId);
        set(ASSET_REVISION_MODEL_DATA_FIELD, assetRevisionModelDataId);
        updateFileRevisionStore();
    }

    public ServiceStore<FileRevisionModelData> getFileRevisionStore()
    {
        return fileRevisionStore;
    }

    private boolean isFileRevisionAvailable(Long fileRevisionModelDataId)
    {
        if (fileRevisionStore == null || fileRevisionStore.getCount() == 0)
            return false;
        return (fileRevisionStore.findModel(FileRevisionModelData.ID_FIELD, fileRevisionModelDataId) != null);
    }

    /**
     * Sets if of the current fileRevisionModelData.
     * 
     * @param fileRevisionModelDataId
     *            id of the FileRevisionModelData in this object
     */
    public void setFileRevisionModelData(Long fileRevisionModelDataId)
    {
        if (fileRevisionModelDataId == null)
            return;
        if (getFileRevisionModelDataId() != null && getFileRevisionModelDataId().equals(fileRevisionModelDataId))
            return;
        if (isFileRevisionAvailable(fileRevisionModelDataId))
        {
            // System.out.println(getName() + "::" + "SET FILE_REVISION_MODEL_DATA_FIELD \t " +
            // fileRevisionModelDataId);
            set(FILE_REVISION_MODEL_DATA_FIELD, fileRevisionModelDataId);
        }
    }

    public Long getProxyModelData()
    {
        return get(PROXY_MODEL_DATA_FIELD);
    }

    public void setProxyTypeId(Long proxyTypeId)
    {
        set(PROXY_TYPE_MODEL_DATA_FIELD, proxyTypeId);
    }

    public Long getProxyTypeId()
    {
        return get(PROXY_TYPE_MODEL_DATA_FIELD);
    }

    public void setProxyModelData(ProxyModelData proxyModelData)
    {
        setProxyModelData(proxyModelData.getId());
    }

    public void setProxyModelData(Long proxyModelDataId)
    {
        if (getProxyTypeId() != null && getProxyTypeId().equals(proxyModelDataId))
            return;
        // System.out.println(getName() + "::" + "SET PROXY_MODEL_DATA_FIELD \t " + proxyModelDataId);
        set(PROXY_MODEL_DATA_FIELD, proxyModelDataId);
    }

    private void setAvailableAssetRevisions(List<AssetRevisionModelData> availableAssetRevisions)
    {
        // System.out.println(getName() + "::" + "SET availableAssetRevisions  \t (" + availableAssetRevisions.size()
        // + " elements)");
        this.availableAssetRevisions = availableAssetRevisions;

        if (this.availableAssetRevisions == null || availableAssetRevisions.isEmpty())
        {
            endEditing();
            return;
        }

        for (AssetRevisionModelData asset : availableAssetRevisions)
        {
            if (!availableTaskTypesIds.contains(asset.getTaskTypeId()))
                availableTaskTypesIds.add(asset.getTaskTypeId());
        }

        if (!tmpTaskTypeIdApplied)
        {
            setTaskTypeModelDataId(tmpTaskTypeId);
            tmpTaskTypeIdApplied = true; // ensure that tmpTaskType is used only once
        }
        endEditing();
    }

    public Long getTaskType()
    {
        return get(TASK_TYPE_MODEL_DATA_FIELD);
    }

    public List<AssetRevisionModelData> getAvailableAssetRevisions()
    {
        return availableAssetRevisions;
    }

    public List<AssetRevisionModelData> getAvailableAssetRevisionsWithTaskType(TaskTypeModelData taskType)
    {
        List<AssetRevisionModelData> collection = new ArrayList<AssetRevisionModelData>();

        if (availableAssetRevisions == null)
            return collection;
        // FIXME reload via loadShot(this.getWorkObjectModelData());

        for (AssetRevisionModelData armd : availableAssetRevisions)
        {
            if (armd != null && armd.getTaskTypeId() != null && armd.getTaskTypeId().equals(taskType.getId()))
            {
                collection.add(armd);
            }
        }

        return collection;
    }

    public List<Long> getAvailableTaskTypesIds()
    {
        return availableTaskTypesIds;
    }

    public boolean isTaskTypeAvailable(Long taskTypeModelData)
    {
        if (availableTaskTypesIds != null)
            return availableTaskTypesIds.contains(taskTypeModelData);
        return false;
    }

    public boolean isTaskTypeAvailableById(Long taskTypeModelDataId)
    {
        if (availableTaskTypesIds != null)
            return availableTaskTypesIds.contains(taskTypeModelDataId);
        return false;
    }

    public void setProxyTypeModelData(ProxyTypeModelData value)
    {
        // System.out.println(getName() + "::" + "SET PROXY_TYPE_MODEL_DATA_FIELD  \t OK ");
        set(PROXY_TYPE_MODEL_DATA_FIELD, value.getId());
        setProxyModelData();
    }

    // /**
    // * Sets a temporary value for taskTypeId, that is going to be set when the asset data is loaded.
    // * <p>
    // * This should be called only once.
    // * </p>
    // *
    // * @param initialTaskTypeId
    // * TaskTypeModelData id
    // */
    // public void applyLaterTaskTypeModelDataId(Long initialTaskTypeId)
    // {
    // this.tmpTaskTypeId = initialTaskTypeId;
    // }
    //
    // /**
    // * Sets a temporary value for fileRevisionModelDataId, that is going to be set when the asset data is loaded.
    // * <p>
    // * This should be called only once.
    // * </p>
    // *
    // * @param initialFileRevisionModelDataId
    // * FileRevisionModelData id
    // */
    // public void applyLaterFileRevisionModelData(Long initialFileRevisionModelDataId)
    // {
    // this.tmpFileRevisionId = initialFileRevisionModelDataId;
    // }

    private void startEditing()
    {
        EventDispatcher.forwardEvent(new AppEvent(ProxySelectionEvents.MODEL_LOADING_STARTED));
    }

    private void endEditing()
    {
        EventDispatcher.forwardEvent(new AppEvent(ProxySelectionEvents.MODEL_LOADING_ENDED));
    }

    /**
     * Sets up a new Event that contain useful information on current model.
     */
    private static class ValueChangedEvent extends AppEvent
    {
        public ValueChangedEvent(ProxySelectionGridModelData source, String property, Object value)
        {
            super(ProxySelectionEvents.MODEL_CHANGED_EVENT);
            setData("source", source);
            setData("property", property);
            setData("value", value);
        }
    }

    /**
     * Set method is overriden to tell the controlle which field has been modified, and what's its new value.
     */
    @Override
    public <X extends Object> X set(String property, X value)
    {
        if (get(property) == value)
            return value;
        super.set(property, value);
        EventDispatcher.forwardEvent(new ValueChangedEvent(this, property, value));
        return value;
    };

    // /**
    // * Sends a new {@link ProxySelectionEvents#MODEL_CHANGED_EVENT} if called.
    // */
    // private void applyChange()
    // {
    // AppEvent modelChangedEvent = new AppEvent(ProxySelectionEvents.MODEL_CHANGED_EVENT);
    // modelChangedEvent.setData(this);
    // EventDispatcher.forwardEvent(modelChangedEvent);
    // }

    public static boolean exists(Long... longIds)
    {
        for (Long longId : longIds)
            if (!(longId != null && longId > -1))
                return false;
        return true;
    }

    public static boolean exists(ListStore<? extends ModelData>... stores)
    {
        for (ListStore<? extends ModelData> store : stores)
            if (!(store != null && store.getCount() > 0))
                return false;
        return true;
    }

    public static boolean exists(List<?>... stores)
    {
        for (List<?> store : stores)
            if (!(store != null && store.size() > 0))
                return false;
        return true;
    }

    // private Long getAssetRevisionIdFromTaskTypeId(Long taskTypeId)
    // {
    // if (this.availableAssetRevisions == null || this.availableAssetRevisions.isEmpty())
    // return new Long(-1L);
    //
    // for (AssetRevisionModelData assetRevision : availableAssetRevisions)
    // if (assetRevision.getTaskTypeId() == taskTypeId)
    // return assetRevision.getId();
    //
    // return new Long(-1L);
    // }

    // public void setAvailableAssetRevisions(List<AssetRevisionModelData> availableAssetRevisions, Long
    // defaultTaskTypeId)
    // {
    // this.setAvailableAssetRevisions(availableAssetRevisions);
    // if (this.availableAssetRevisions != null && !this.availableAssetRevisions.isEmpty())
    // {
    // Long assetRevisionIdFromTaskTypeId = getAssetRevisionIdFromTaskTypeId(defaultTaskTypeId);
    // if (assetRevisionIdFromTaskTypeId > -1)
    // loadServiceFileRevision(assetRevisionIdFromTaskTypeId);
    //
    // else
    // loadServiceFileRevision(availableAssetRevisions.get(0).getId());
    // }
    // }

}
