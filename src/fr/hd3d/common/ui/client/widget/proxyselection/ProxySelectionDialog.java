package fr.hd3d.common.ui.client.widget.proxyselection;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.Component;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.RowNumberer;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.toolbar.LabelToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.ToolBar;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.widget.proxyselection.event.ProxySelectionEvents;
import fr.hd3d.common.ui.client.widget.proxyselection.renderer.ProxySelectionFileRevisionComboBoxRenderer;
import fr.hd3d.common.ui.client.widget.proxyselection.renderer.ProxySelectionTaskTypeModelBasedComboRenderer;
import fr.hd3d.common.ui.client.widget.proxyselection.renderer.RecordModelDataGridCellRenderer;
import fr.hd3d.common.ui.client.widget.proxyselection.widget.ProxySelectionProxyTypeComboBox;
import fr.hd3d.common.ui.client.widget.proxyselection.widget.ProxySelectionTaskTypeComboBox;


/**
 * View Dialog window for selecting proxies via following selection process :
 * 
 * <pre>
 * WorkObject &gt; TaskType &gt; AssetRevision &gt; FileRevision &gt; ProxyType &gt; Proxy
 * </pre>
 * 
 * The process is made thanks to an interactive grid.
 * 
 */
public class ProxySelectionDialog extends Dialog
{
    private static ProxySelectionDialog dialog;
    /** Model handling data */
    private final ProxySelectionDialogModel model;
    /** Controller handling events */
    private final ProxySelectionDialogController controller;
    /** Actual grid displaying ProxySelectionGridModelData */
    private final ProxySelectionGrid grid;
    /** ToolBar for ProxySelectionDialog */
    private final ToolBar toolBar = new ToolBar();
    private final LabelToolItem toolBarEmptySpace = new LabelToolItem();
    private final ProxySelectionTaskTypeComboBox toolBarTaskTypeComboBox = new ProxySelectionTaskTypeComboBox();
    // The following code has been commented for preview purpose. Do not remove.
    // private SimpleComboBox<String> toolBarFileRevisionComboBox = new SimpleComboBox<String>();

    private final ProxySelectionProxyTypeComboBox proxyTypeComboBox = new ProxySelectionProxyTypeComboBox();

    private ProxySelectionDialog()
    {
        this(null);
    }

    protected ProxySelectionDialog(List<RecordModelData> initialList)
    {
        this.model = new ProxySelectionDialogModel();
        this.controller = new ProxySelectionDialogController(this, model);
        this.grid = new ProxySelectionGrid(model.getStore(initialList), createColumnModel());
        this.initUi();
        this.initEvents();
    }

    private void initUi()
    {
        this.setMinWidth(650);
        this.setMinHeight(300);
        this.setModal(true);
        this.setButtons(Dialog.OKCANCEL);
        this.setHideOnButtonClick(true);
        this.setLayout(new FitLayout());
        this.add(grid);
        this.initToolBar();
        this.initButtonBar();
    }

    public static ProxySelectionDialog getInstance()
    {
        if (dialog == null)
        {
            dialog = new ProxySelectionDialog();
        }
        return dialog;
    }

    private void reset()
    {
        this.toolBarTaskTypeComboBox.clearSelections();
        this.proxyTypeComboBox.clearSelections();
    }

    public ProxySelectionDialog reset(List<RecordModelData> initialList)
    {
        model.getStore(initialList);
        reset();
        return this;
    }

    public void show(List<RecordModelData> initialList)
    {
        super.show();
        reset();
        model.getStore(initialList);
        EventDispatcher.forwardEvent(Events.Resize);
    }

    public void show(List<RecordModelData> initialList, Long defaultTaskTypeId, FastMap<Long> fileRevisionCollection)
    {
        super.show();
        reset();
        model.getStore(initialList, defaultTaskTypeId, fileRevisionCollection);
        EventDispatcher.forwardEvent(Events.Resize);
    }

    private void initButtonBar()
    {
        List<Component> items = this.getButtonBar().getItems();

        proxyTypeComboBox.setWidth(150);

        LabelToolItem emptySpace = new LabelToolItem();
        emptySpace.setWidth(170);

        items.add(0, emptySpace);
        items.add(0, proxyTypeComboBox);
        items.add(0, new LabelToolItem("Select proxy type: "));
    }

    /**
     * Sets up the toolbar
     */
    private void initToolBar()
    {
        ColumnModel columnModel = this.grid.getColumnModel();
        this.toolBarEmptySpace.setWidth(columnModel.getColumnWidth(0) + columnModel.getColumnWidth(1));
        this.toolBar.add(this.toolBarEmptySpace);

        toolBarTaskTypeComboBox.setWidth(columnModel.getColumnWidth(2));
        toolBarTaskTypeComboBox.setSelectionChangedEvent(ProxySelectionEvents.ALL_TASK_TYPE_SELECTED);
        toolBarTaskTypeComboBox.setToolTip("Select Task Type...");
        this.toolBar.add(toolBarTaskTypeComboBox);

        // The following code has been commented for preview purpose. Do not remove.

        // toolBarFileRevisionComboBox = createSimpleRevisionComboBox("File Revision...");
        // toolBarFileRevisionComboBox.addListener(Events.SelectionChange, new Listener<BaseEvent>() {
        // public void handleEvent(BaseEvent be)
        // {
        // String value = toolBarFileRevisionComboBox.getValue().getValue();
        // if (value == null || value.isEmpty())
        // return;
        // AppEvent evt = new AppEvent(ProxySelectionEvents.ALL_FILE_REVISION_SELECTED);
        // evt.setData("filter", value);
        // EventDispatcher.forwardEvent(evt);
        // };
        // });
        // toolBarFileRevisionComboBox.setWidth(columnModel.getColumnWidth(3));
        // toolBarFileRevisionComboBox.setToolTip("Select File Revision...");
        // this.toolBar.add(toolBarFileRevisionComboBox);

        this.setTopComponent(this.toolBar);
    }

    // The following code has been commented for preview purpose. Do not remove.
    // /**
    // * Creates a ComboBox widget initialized for *Revision objects. It contains
    // *
    // * @param emptyText
    // * Text to appear when no selection
    // *
    // * @return SimpleComboBox widget
    // */
    // private SimpleComboBox<String> createSimpleRevisionComboBox(String emptyText, String... choices)
    // {
    // final SimpleComboBox<String> revisionComboBox = new SimpleComboBox<String>();
    // revisionComboBox.setForceSelection(true);
    // revisionComboBox.setTriggerAction(TriggerAction.ALL);
    // revisionComboBox.setEmptyText(emptyText);
    // revisionComboBox.add("Last revision");
    // revisionComboBox.add("Last validated");
    // if (choices.length > 0)
    // for (String choice : choices)
    // revisionComboBox.add(choice);
    //
    // return revisionComboBox;
    // }

    /**
     * Sets following events :
     * <ul>
     * <li>{@link Events#ColumnResize} on {@link #grid}</li>
     * </ul>
     */
    private void initEvents()
    {
        if (!EventDispatcher.get().getControllers().contains(controller))
            EventDispatcher.get().addController(controller);

        // Dialog resized Event
        Listener<GridEvent<RecordModelData>> resizeListener = new Listener<GridEvent<RecordModelData>>() {

            public void handleEvent(GridEvent<RecordModelData> be)
            {
                ColumnModel columnModel = be.getGrid().getColumnModel();

                if (be.getColIndex() == 2)
                    ProxySelectionDialog.this.toolBarTaskTypeComboBox.setWidth(columnModel.getColumnWidth(2));
                // The following code has been commented for preview purpose. Do not remove.
                // else if (be.getColIndex() == 3)
                // ProxySelectionDialog.this.toolBarFileRevisionComboBox.setWidth(columnModel.getColumn(3).getWidth());
                ProxySelectionDialog.this.toolBarEmptySpace.setWidth(columnModel.getColumnWidth(0)
                        + columnModel.getColumnWidth(1));
            }
        };
        this.grid.addListener(Events.ColumnResize, resizeListener);
        this.grid.addListener(Events.Resize, resizeListener);
        this.grid.getColumnModel().addListener(Events.WidthChange, resizeListener);
        this.grid.getView().addListener(Events.BeforeShow, resizeListener);

        // Ok button event
        Button okButton = (Button) getButtonBar().getItemByItemId(Dialog.OK);
        okButton.addListener(Events.Select, new Listener<BaseEvent>() {

            public void handleEvent(BaseEvent be)
            {
                EventDispatcher.forwardEvent(new AppEvent(ProxySelectionEvents.OK_BUTTON_CLICKED));
            }
        });
    }

    public ProxySelectionGrid getGrid()
    {
        return grid;
    }

    public void setLoadingState(boolean state)
    {
        if (state && !isMasked())
            this.mask("Loading");
        else if (isMasked())
            this.unmask();
    }

    private ColumnModel createColumnModel()
    {
        List<ColumnConfig> configs = new ArrayList<ColumnConfig>();

        /* Column 0 : Line Number */
        RowNumberer rowNumber = new RowNumberer();
        rowNumber.setWidth(20);
        configs.add(rowNumber);

        /* Column 1 : Work Object : Label */
        ColumnConfig column = new ColumnConfig();
        column.setId(ProxySelectionGridModelData.WORK_OBJECT_MODEL_DATA_FIELD);
        column.setHeader("Work Object");
        column.setWidth(150);
        column.setRenderer(new RecordModelDataGridCellRenderer());
        configs.add(column);

        /* Column 2 : Proxy Type : decorated ComboBox */
        column = new ColumnConfig();
        column.setId(ProxySelectionGridModelData.TASK_TYPE_MODEL_DATA_FIELD);
        column.setHeader("Task Type");
        column.setWidth(160);
        // FIXME set up a CellEditor + a renderer to display only valid taskTypes ?
        // In our case, everything is done via the renderer.
        column.setRenderer(new ProxySelectionTaskTypeModelBasedComboRenderer());
        configs.add(column);

        /* Column 3 : File Revision : ComboBox */
        column = new ColumnConfig();
        column.setId(ProxySelectionGridModelData.FILE_REVISION_MODEL_DATA_FIELD);
        column.setHeader("File Revision");
        column.setWidth(160);
        column.setRenderer(new ProxySelectionFileRevisionComboBoxRenderer());
        configs.add(column);

        return new ColumnModel(configs);
    }
}
