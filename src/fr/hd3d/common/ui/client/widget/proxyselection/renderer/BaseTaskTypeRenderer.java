package fr.hd3d.common.ui.client.widget.proxyselection.renderer;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.util.Util;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;


public abstract class BaseTaskTypeRenderer<T extends Hd3dModelData> implements GridCellRenderer<T>
{
    public Object render(T model, String property, ColumnData config, int rowIndex, int colIndex, ListStore<T> store,
            Grid<T> grid)
    {
        String obj = this.getTaskTypeName(model);
        String color = this.getTaskTypeColor(model);

        return getTypeRendered(obj, color);
    }

    protected abstract String getTaskTypeColor(T model);

    protected abstract String getTaskTypeName(T model);

    public String getTypeRendered(String type, String color)
    {
        String cellCode = "";
        if (!Util.isEmptyString(type))
        {
            cellCode = type;
            if (!Util.isEmptyString(color))
            {
                cellCode = "<div style='padding: 3px; font-weight: bold; text-align:center; background-color:" + color
                        + ";'>" + type + "</div>";
            }
            else
            {
                cellCode = "<div style='padding: 3px; font-weight: bold; text-align:center; background-color:white;'>"
                        + type + "</div>";
            }
        }
        else
        {
            cellCode += "";
        }
        return cellCode;
    }

}
