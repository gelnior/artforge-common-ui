package fr.hd3d.common.ui.client.widget.proxyselection;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.ListStore;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.asset.ProxyTypeModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.widget.proxyselection.event.ProxySelectionEvents;


/**
 * Model for ProxySelectionDialog.
 * 
 * @author guillaume-maucomble
 * 
 */
public class ProxySelectionDialogModel
{
    private ListStore<ProxySelectionGridModelData> proxySelectionGridModelDataStore;

    public ProxySelectionDialogModel()
    {}

    public List<ProxySelectionGridModelData> getActualStore()
    {
        List<ProxySelectionGridModelData> collection = new ArrayList<ProxySelectionGridModelData>();
        for (ProxySelectionGridModelData psgmd : proxySelectionGridModelDataStore.getModels())
        {
            Long proxyModelData = psgmd.getProxyModelData();
            if (proxyModelData != null && proxyModelData > -1L)
            {
                collection.add(psgmd);
            }
        }
        return collection;
    }

    /**
     * Returns an empty store. If the store is not empty, is empties it.
     * 
     * @return actual ListStore<ProxySelectionGridModelData>
     */
    public ListStore<ProxySelectionGridModelData> getStore()
    {
        return getStore(null);
    }

    /**
     * Returns a store fileld with converted initialList.
     * 
     * @param initialList
     *            List of RecordModelData objects to convert to ProxySelectionGridModelData
     * @return actual ListStore<ProxySelectionGridModelData>
     */
    public ListStore<ProxySelectionGridModelData> getStore(List<RecordModelData> initialList)
    {
        initStore(initialList, null, null);
        return proxySelectionGridModelDataStore;
    }

    /**
     * Returns a store fileld with converted initialList.
     * 
     * @param initialList
     *            List of RecordModelData objects to convert to ProxySelectionGridModelData
     * @return actual ListStore<ProxySelectionGridModelData>
     */
    public ListStore<ProxySelectionGridModelData> getStore(List<RecordModelData> initialList, Long defaultTaskTypeId,
            FastMap<Long> fileRevisionIds)
    {
        initStore(initialList, defaultTaskTypeId, fileRevisionIds);
        return proxySelectionGridModelDataStore;
    }

    /**
     * Creates an empty store for {@link #proxySelectionGridModelDataStore} and insert data with specified arguments.
     * 
     * @param initialList
     *            List of WorkObjects
     * @param defaultTaskTypeId
     *            TaskTypeModelData id to be applied to all the elements on initialList
     * @param fileRevisionIds
     *            Map with key : String value of the id for an AssetRevisionModelData
     */
    private void initStore(List<RecordModelData> initialList, Long defaultTaskTypeId, FastMap<Long> fileRevisionIds)
    {
        EventDispatcher.forwardEvent(new AppEvent(ProxySelectionEvents.MODEL_LOADING_STARTED));

        proxySelectionGridModelDataStore = reset();

        if (initialList == null || initialList.isEmpty())
        {
            EventDispatcher.forwardEvent(new AppEvent(ProxySelectionEvents.MODEL_LOADING_ENDED));
            return;
        }

        Long ttId = null, frId = null;
        for (RecordModelData modelData : initialList)
        {
            if (defaultTaskTypeId != null && defaultTaskTypeId > -1)
            {
                ttId = defaultTaskTypeId;
            }
            if (fileRevisionIds != null && !fileRevisionIds.isEmpty()
                    && fileRevisionIds.containsKey(modelData.getId().toString()))
            {
                frId = fileRevisionIds.get(modelData.getId().toString());
                // model.setFileRevisionModelData(fileRevisionIds.get(modelData.getId().toString()));
                // model.applyLaterFileRevisionModelData();
            }
            ProxySelectionGridModelData model = new ProxySelectionGridModelData(modelData, ttId, frId);
            proxySelectionGridModelDataStore.add(model);
        }

        EventDispatcher.forwardEvent(new AppEvent(ProxySelectionEvents.MODEL_LOADING_ENDED));
    }

    /**
     * Resets the store (or creates an empty store)
     * 
     * @return empty ListStore<ProxySelectionGridModelData>
     */
    private ListStore<ProxySelectionGridModelData> reset()
    {
        if (proxySelectionGridModelDataStore != null)
            proxySelectionGridModelDataStore.removeAll();

        else
            proxySelectionGridModelDataStore = new ListStore<ProxySelectionGridModelData>();

        return proxySelectionGridModelDataStore;

    }

    /**
     * Sets the parameter TaskTypeModelData to each entity in store.
     * 
     * @param taskTypeModelData
     */
    public void applyAllTaskTypes(TaskTypeModelData taskTypeModelData)
    {
        for (RecordModelData rmd : proxySelectionGridModelDataStore.getModels())
        {
            ((ProxySelectionGridModelData) rmd).setTaskTypeModelDataId(taskTypeModelData.getId());
            // this.proxySelectionGridModelDataStore.update((ProxySelectionGridModelData) rmd);
        }
    }

    /**
     * Sets the proxy type for each entity in store.
     * 
     * @param value
     */
    public void applyAllProxyType(ProxyTypeModelData value)
    {
        for (RecordModelData rmd : proxySelectionGridModelDataStore.getModels())
            ((ProxySelectionGridModelData) rmd).setProxyTypeModelData(value);
    }
}
