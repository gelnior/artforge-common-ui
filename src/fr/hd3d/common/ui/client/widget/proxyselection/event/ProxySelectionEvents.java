package fr.hd3d.common.ui.client.widget.proxyselection.event;

import com.extjs.gxt.ui.client.event.EventType;


/**
 * General class gathering all proxyselection {@link EventType} objects.
 * 
 * @author guillaume-maucomble
 * 
 */
public class ProxySelectionEvents
{

    private static int PROXYSELECTIONEVENTS_START_INTEGER = 7800;

    public static EventType ALL_TASK_TYPE_SELECTED = new EventType(PROXYSELECTIONEVENTS_START_INTEGER);
    // public static EventType ALL_ASSET_REVISION_SELECTED = new EventType(PROXYSELECTIONEVENTS_START_INTEGER + 1);
    public static EventType ALL_FILE_REVISION_SELECTED = new EventType(PROXYSELECTIONEVENTS_START_INTEGER + 2);
    public static EventType ALL_PROXY_TYPE_SELECTED = new EventType(PROXYSELECTIONEVENTS_START_INTEGER + 3);
    public static EventType ALL_ASSET_REVISION_LOADED = new EventType(PROXYSELECTIONEVENTS_START_INTEGER + 100);

    public static EventType VALUE_CHANGED = new EventType(PROXYSELECTIONEVENTS_START_INTEGER + 4);

    public static EventType TASK_TYPE_SELECTED = new EventType(PROXYSELECTIONEVENTS_START_INTEGER + 5);
    public static EventType ASSET_REVISION_SELECTED = new EventType(PROXYSELECTIONEVENTS_START_INTEGER + 6);
    public static EventType FILE_REVISION_SELECTED = new EventType(PROXYSELECTIONEVENTS_START_INTEGER + 7);
    public static EventType PROXY_TYPE_SELECTED = new EventType(PROXYSELECTIONEVENTS_START_INTEGER + 8);

    public static EventType MODEL_LOADING_STARTED = new EventType(PROXYSELECTIONEVENTS_START_INTEGER + 9);
    public static EventType MODEL_LOADING_ENDED = new EventType(PROXYSELECTIONEVENTS_START_INTEGER + 10);

    public static EventType MODEL_CHANGED_EVENT = new EventType(PROXYSELECTIONEVENTS_START_INTEGER + 11);

    public static EventType OK_BUTTON_CLICKED = new EventType(PROXYSELECTIONEVENTS_START_INTEGER + 100);

    private ProxySelectionEvents()
    {}

}
