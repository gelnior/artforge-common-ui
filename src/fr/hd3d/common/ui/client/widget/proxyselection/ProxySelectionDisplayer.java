package fr.hd3d.common.ui.client.widget.proxyselection;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.data.BaseListLoadResult;
import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.event.LoadListener;

import fr.hd3d.common.ui.client.error.Hd3dException;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.modeldata.reader.ShotReader;
import fr.hd3d.common.ui.client.service.store.ServiceStore;


public class ProxySelectionDisplayer
{

    private ProxySelectionDisplayer()
    {}

    public static void show(List<RecordModelData> models)
    {
        ProxySelectionDialog.getInstance().show(models);
    }

    public static void show(List<RecordModelData> models, final Long defaultTaskTypeId,
            FastMap<Long> fileRevisionCollection)
    {
        ProxySelectionDialog.getInstance().show(models, defaultTaskTypeId, fileRevisionCollection);
    }

    private static void loadShots(List<Long> ids, final Long defaultTaskTypeId,
            final FastMap<Long> fileRevisionCollection)
    {
        ServiceStore<ShotModelData> shotStore = new ServiceStore<ShotModelData>(new ShotReader());
        shotStore.clearParameters();
        shotStore.addInConstraint("id", ids);
        shotStore.addLoadListener(new LoadListener() {
            @SuppressWarnings("unchecked")
            @Override
            public void loaderLoad(LoadEvent le)
            {
                List<RecordModelData> models = new ArrayList<RecordModelData>();
                BaseListLoadResult data = le.getData();

                for (Object modelData : data.getData())
                {
                    if (modelData instanceof RecordModelData)
                        models.add((RecordModelData) modelData);
                }
                ProxySelectionDisplayer.show(models, defaultTaskTypeId, fileRevisionCollection);
            }
        });
        shotStore.getLoader().load();
    }

    public static void show(List<Long> ids, String simpleClassName, Long defaultTaskTypeId,
            FastMap<Long> fileRevisionCollection)
    {
        if (ShotModelData.SIMPLE_CLASS_NAME.equals(simpleClassName))
        {
            loadShots(ids, defaultTaskTypeId, fileRevisionCollection);
        }
        else if (ConstituentModelData.SIMPLE_CLASS_NAME.equals(simpleClassName))
        {}
    }

    public static void showTest(Long projectId) throws Hd3dException
    {
        createDemoModels();
    }

    private static void createDemoModels()
    {
        ServiceStore<ShotModelData> store = new ServiceStore<ShotModelData>(
                new ShotReader(ShotModelData.getModelType()));
        store.getLoader().addLoadListener(new LoadListener() {
            @SuppressWarnings("unchecked")
            @Override
            public void loaderLoad(LoadEvent le)
            {
                super.loaderLoad(le);
                List<RecordModelData> models = new ArrayList<RecordModelData>();

                BaseListLoadResult data = le.getData();

                for (Object modelData : data.getData())
                {
                    if (modelData instanceof RecordModelData)
                        models.add((RecordModelData) modelData);
                }
                ProxySelectionDisplayer.show(models);
            }

            @Override
            public void loaderLoadException(LoadEvent le)
            {
                super.loaderLoadException(le);
            }

        });
        store.addEqConstraint(ShotModelData.SHOT_SEQUENCE, 3);
        store.getLoader().load();

    }

}
