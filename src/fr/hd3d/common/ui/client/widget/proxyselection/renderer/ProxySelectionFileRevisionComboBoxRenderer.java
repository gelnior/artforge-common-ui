package fr.hd3d.common.ui.client.widget.proxyselection.renderer;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.form.ComboBox;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.asset.FileRevisionModelData;
import fr.hd3d.common.ui.client.widget.proxyselection.ProxySelectionGridModelData;
import fr.hd3d.common.ui.client.widget.proxyselection.event.ProxySelectionEvents;


public class ProxySelectionFileRevisionComboBoxRenderer implements GridCellRenderer<ProxySelectionGridModelData>
{
    public Object render(final ProxySelectionGridModelData model, String property, ColumnData config, int rowIndex,
            int colIndex, ListStore<ProxySelectionGridModelData> store, Grid<ProxySelectionGridModelData> grid)
    {
        final ComboBox<FileRevisionModelData> combo = new ComboBox<FileRevisionModelData>();
        combo.setStore(model.getFileRevisionStore());
        combo.setDisplayField(FileRevisionModelData.RELATIVE_PATH_FIELD);

        if (model.getActualAssetRevisionModelData() != null)
        {
            combo.setTemplate(getXTemplate());
            combo.setValue(model.getActualFileRevisionModelData());
        }
        else
        {
            combo.reset();
        }

        combo.addListener(Events.SelectionChange, new Listener<BaseEvent>() {

            public void handleEvent(BaseEvent be)
            {
                AppEvent event = new AppEvent(ProxySelectionEvents.FILE_REVISION_SELECTED);
                event.setData("source", model);
                event.setData("data", combo.getValue());
                EventDispatcher.forwardEvent(event);
            }
        });
        return combo;
    }

    /**
     * @return Combo box values display style.
     */
    public native String getXTemplate() /*-{
                                                          return [
                                                          '<tpl for=".">',
                                                          '<div class="x-combo-list-item" title="{state} : {key}"> {key} (<span style="font-weight: bold;">{state}</span>) </div>',
                                                          '</tpl>'
                                                          ].join("");
                                                          }-*/;

}
