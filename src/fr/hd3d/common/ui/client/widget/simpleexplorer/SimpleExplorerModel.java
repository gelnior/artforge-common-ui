package fr.hd3d.common.ui.client.widget.simpleexplorer;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.data.BaseListLoadConfig;
import com.extjs.gxt.ui.client.data.ListLoadConfig;
import com.extjs.gxt.ui.client.store.Record;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.service.BulkRequests;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.IUrlParameter;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.widget.sorter.SimpleExplorerModelSorter;


public class SimpleExplorerModel<C extends Hd3dModelData>
{
    /** Store containing all data. */
    protected final ServiceStore<C> modelStore;

    /** Reader used to parse data. */
    protected final IReader<C> reader;

    protected final ListLoadConfig config = new BaseListLoadConfig();

    /** The "Record to create in database" list. */
    private final ArrayList<C> createdRecords = new ArrayList<C>();
    /** The "Record to delete in database" list. */
    protected final ArrayList<C> deletedRecords = new ArrayList<C>();

    private C defaultInstance;

    private ServiceStore<TaskTypeModelData> taskTypeStore;

    public SimpleExplorerModel(IReader<C> reader)
    {
        this.reader = reader;
        this.modelStore = new ServiceStore<C>(reader);

        this.config.setSortDir(SortDir.ASC);
        this.config.setSortField(RecordModelData.NAME_FIELD);

        this.modelStore.sort(RecordModelData.NAME_FIELD, SortDir.ASC);
        this.modelStore.setStoreSorter(new SimpleExplorerModelSorter<C>());
        this.modelStore.setPath(ServicesPath.getPath(reader.newModelInstance().getSimpleClassName()));
    }

    public ServiceStore<C> getModelStore()
    {
        return this.modelStore;
    }

    public C getNewModelInstance()
    {
        if (defaultInstance != null)
        {
            C instance = reader.newModelInstance();
            Hd3dModelData.copy(defaultInstance, instance);

            return instance;
        }
        return reader.newModelInstance();
    }

    public void addDeletedRecord(C model)
    {
        this.deletedRecords.add(model);
    }

    /**
     * @return "Records to create" list.
     */
    public List<C> getCreatedRecords()
    {
        return this.createdRecords;
    }

    /**
     * Adds a record to the "Records to create" list.
     * 
     * @param record
     */
    public void addCreatedRecord(C record)
    {
        this.createdRecords.add(record);
    }

    /**
     * Clear deleted records list.
     */
    public void clearDeletedRecords()
    {
        this.deletedRecords.clear();
    }

    /**
     * Clear the "Records to create" list.
     */
    public void clearCreatedRecords()
    {
        this.createdRecords.clear();
    }

    public void refresh()
    {
        this.modelStore.reload(config);
        if (config.getSortField() != null && config.getSortDir() != null)
        {
            this.modelStore.sort(config.getSortField(), config.getSortDir());
        }
    }

    @SuppressWarnings("unchecked")
    public void saveUpdatedRecords()
    {
        List<Hd3dModelData> recordsToCreate = new ArrayList<Hd3dModelData>();
        List<Hd3dModelData> recordsToUpdate = new ArrayList<Hd3dModelData>();

        for (Record record : this.modelStore.getModifiedRecords())
        {
            C model = (C) record.getModel();
            model.setDefaultPath(this.getModelPath(model));
            if (model.getId() == null)
            {
                recordsToCreate.add(model);
            }
            else
            {
                recordsToUpdate.add(model);
            }
        }

        if (recordsToCreate.size() > 0)
        {
            BulkRequests.bulkPost(recordsToCreate, SimpleExplorerEvents.CREATE_SUCCESS);
        }
        else
        {
            EventDispatcher.forwardEvent(SimpleExplorerEvents.CREATE_SUCCESS, recordsToCreate);
        }
        if (recordsToUpdate.size() > 0)
        {
            BulkRequests.bulkPut(recordsToUpdate, SimpleExplorerEvents.UPDATE_SUCCESS);
        }
        else
        {
            EventDispatcher.forwardEvent(SimpleExplorerEvents.UPDATE_SUCCESS, recordsToUpdate);
        }
    }

    protected String getModelPath(C model)
    {
        String path = this.modelStore.getPath();
        if (path != null)
        {
            model.setDefaultPath(this.modelStore.getPath());
            if (model.getId() != null)
            {
                path += model.getId().toString();
            }
        }
        return path;
    }

    public void saveDeletedRecords()
    {
        for (C model : this.deletedRecords)
        {
            model.delete(SimpleExplorerEvents.DELETE_SUCCESS);
        }
        this.deletedRecords.clear();
    }

    public List<C> getDeletedRecords()
    {
        return this.deletedRecords;
    }

    public void saveAll()
    {
        this.saveUpdatedRecords();
        this.saveDeletedRecords();
    }

    public void clearParameters()
    {
        this.modelStore.clearParameters();
    }

    public void addParameter(IUrlParameter parameter)
    {
        this.modelStore.addParameter(parameter);
    }

    public void setProxyPath(String path)
    {
        this.modelStore.setPath(path);
    }

    public void setSortField(String column)
    {
        this.config.setSortField(column);
    }

    public void setSortDir(SortDir sortDir)
    {
        this.config.setSortDir(sortDir);
    }

    public void setDefaultInstance(C instance)
    {
        this.defaultInstance = instance;
    }

    /** Commit changes to store (to remove dirty markers). */
    public void commitChanges()
    {
        this.modelStore.commitChanges();
    }

    public void setTaskTypeStore(ServiceStore<TaskTypeModelData> taskTypeStore)
    {
        this.taskTypeStore = taskTypeStore;
    }

    public ServiceStore<TaskTypeModelData> getTaskTypeStore()
    {
        return taskTypeStore;
    }
}
