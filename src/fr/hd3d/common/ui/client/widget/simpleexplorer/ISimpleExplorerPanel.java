package fr.hd3d.common.ui.client.widget.simpleexplorer;

import java.util.List;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.widget.tabdialog.IMaskableView;


/**
 * Simple explorer interface.
 * 
 * @author HD3D
 * 
 * @param <C>
 *            Type of data managed by the simple explorer.
 */
public interface ISimpleExplorerPanel<C extends Hd3dModelData> extends IMaskableView
{
    public void hideSaving();

    public void disableButtons();

    public void showSaving();

    public void enableButtons();

    public List<C> getSelection();

    public void enableCreateToolItem(boolean enabled);

    public void enableDeleteToolItem(boolean enabled);

    public void enableSaveToolItem(boolean enabled);

    public String getNewItemString();

    public void displaySaveConfirmBox();

    public void resetSelection();
}
