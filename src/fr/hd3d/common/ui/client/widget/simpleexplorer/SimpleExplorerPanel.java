package fr.hd3d.common.ui.client.widget.simpleexplorer;

import java.util.List;

import com.extjs.gxt.ui.client.Style.ButtonScale;
import com.extjs.gxt.ui.client.Style.SelectionMode;
import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.Component;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.Status;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.toolbar.FillToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.ToolBar;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.service.parameter.IUrlParameter;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.userrights.PermissionGridViewConfig;
import fr.hd3d.common.ui.client.widget.EditableNameGrid;
import fr.hd3d.common.ui.client.widget.FilterStoreField;
import fr.hd3d.common.ui.client.widget.ToolBarButton;
import fr.hd3d.common.ui.client.widget.dialog.ConfirmationDisplayer;


/**
 * The simple explorer panel is a grid that manages service objects with a name (record model data) of the same type. It
 * allows to create, delete and update the managed data.
 * 
 * @author HD3D
 * 
 * @param <C>
 *            The data type.
 */
public class SimpleExplorerPanel<C extends RecordModelData> extends ContentPanel implements ISimpleExplorerPanel<C>
{
    /** Constant strings to display : dialog messages, button label... */
    public static final CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    /** Model that handles grid data. */
    protected SimpleExplorerModel<C> model;
    /** Controller that handles relation dialog events. */
    protected SimpleExplorerController<C> controller;

    /** Grid showing data. */
    protected EditableNameGrid<C> nameGrid;
    /** Tool bar with create, delete, refresh and save buttons. */
    protected ToolBar toolBar = new ToolBar();

    /** Add tool item raise the ADD_CLICKED event when clicked. */
    protected ToolBarButton addToolItem = new ToolBarButton(Hd3dImages.getMediumAddIcon(), CONSTANTS.AddRow(),
            SimpleExplorerEvents.ADD_CLICKED);
    /** Delete tool item raise the DELETE_CLICKED event when clicked. */
    protected ToolBarButton deleteToolItem = new ToolBarButton(Hd3dImages.getDeleteIcon(), CONSTANTS.DeleteRow(),
            SimpleExplorerEvents.DELETE_CLICKED);
    /** Refresh tool item raise the REFRESH_CLICKED event when clicked. */
    protected ToolBarButton refreshToolItem = new ToolBarButton(Hd3dImages.getRefreshIcon(), CONSTANTS.Refresh(),
            SimpleExplorerEvents.REFRESH_CLICKED);
    /** Save tool item raise the SAVE_CLICKED event when clicked. */
    protected ToolBarButton saveToolItem = new ToolBarButton(Hd3dImages.getMediumSaveIcon(), CONSTANTS.Save(),
            SimpleExplorerEvents.SAVE_CLICKED);

    protected FilterStoreField<C> filterField;

    /** Displays an animated GIF showing that application is saving data. */
    protected final Status savingToolItem = new Status();

    /** Do not use this constructor, it is here for overriding purposes. */
    public SimpleExplorerPanel()
    {
        super();
    }

    /**
     * Default constructor, build tool bar, grid and set dialog styles.
     * 
     * @param reader
     *            The data reader needed by the grid.
     */
    public SimpleExplorerPanel(IReader<C> reader)
    {
        this(reader, null);
    }

    /**
     * Default constructor, build tool bar, grid and set dialog styles. If a column model is given, the grid use this
     * column model. Else the grid will only have one column for name.
     * 
     * @param reader
     *            The data reader needed by the grid.
     * @param cm
     *            The column model to set specific columns to the grid.
     */
    public SimpleExplorerPanel(IReader<C> reader, ColumnModel cm)
    {
        super();

        this.model = new SimpleExplorerModel<C>(reader);
        this.controller = new SimpleExplorerController<C>(model, this);
        this.filterField = new FilterStoreField<C>(this.model.getModelStore());

        this.setStyle();
        this.setToolbar();
        this.setGrid(cm);
    }

    /**
     * @return dialog controller (MVC).
     */
    public MaskableController getController()
    {
        return this.controller;
    }

    /**
     * Hide saving indicator.
     */
    public void hideSaving()
    {
        savingToolItem.hide();
    }

    /**
     * Refresh grid data.
     */
    public void refresh()
    {
        this.controller.onRefreshClicked();
    }

    /**
     * Mask dialog controller.
     */
    public void idle()
    {
        this.controller.mask();
    }

    /**
     * Unmask dialog controller.
     */
    public void unIdle()
    {
        this.controller.unMask();
    }

    /**
     * Disable save and refresh buttons.
     */
    public void disableButtons()
    {
        this.saveToolItem.disable();
        this.refreshToolItem.disable();
    }

    /**
     * Enable save and refresh buttons.
     */
    public void enableButtons()
    {
        this.saveToolItem.enable();
        this.refreshToolItem.enable();
    }

    /**
     * Show saving indicator.
     */
    public void showSaving()
    {
        savingToolItem.show();
    }

    /**
     * @return Selected items.
     */
    public List<C> getSelection()
    {
        return this.nameGrid.getSelectionModel().getSelectedItems();
    }

    /**
     * Enables the create tool item if <i>enabled</i> is true else it disables it.
     * 
     * @param enabled
     */
    public void enableCreateToolItem(boolean enabled)
    {
        this.addToolItem.setEnabled(enabled);
    }

    /**
     * Enables the delete tool item if <i>enabled</i> is true else it disables it.
     * 
     * @param enabled
     */
    public void enableDeleteToolItem(boolean enabled)
    {
        this.deleteToolItem.setEnabled(enabled);
    }

    /**
     * Enables the save tool item if <i>enabled</i> is true else it disables it.
     * 
     * @param enabled
     */
    public void enableSaveToolItem(boolean enabled)
    {
        this.saveToolItem.setEnabled(enabled);
    }

    /**
     * Hides the delete tool item.
     */
    public void hideDeleteToolItem()
    {
        this.deleteToolItem.setVisible(false);
    }

    /**
     * @return "New item" translation.
     */
    public String getNewItemString()
    {
        return CONSTANTS.NewItem();
    }

    /**
     * Reconfigure simple explorer grid with column model given in parameter.
     * 
     * @param cm
     *            Column model needed to build the simple explorer grid.
     */
    public void reconfigureGrid(ColumnModel cm)
    {
        this.nameGrid.reconfigure(this.model.getModelStore(), cm);
        this.nameGrid.setHideHeaders(false);
    }

    /**
     * Clear parameters set on the grid proxy.
     */
    public void clearParameters()
    {
        this.model.clearParameters();
    }

    /**
     * Add URL parameter to the grid proxy.
     * 
     * @param parameter
     *            The parameter to add.
     */
    public void addParameter(IUrlParameter parameter)
    {
        this.model.addParameter(parameter);
    }

    /**
     * Set the proxy path in place of the default one.
     * 
     * @param path
     *            Data path to set on the grid proxy.
     */
    public void setPath(String path)
    {
        this.model.setProxyPath(path);
    }

    /**
     * Set field to be used for grid sorting if different of name.
     * 
     * @param field
     *            The sort field.
     */
    public void setSortField(String field)
    {
        this.model.setSortField(field);
    }

    /**
     * Set sort direction for grid.
     * 
     * @param sortDir
     *            The sort direction.
     */
    public void setSortDir(SortDir sortDir)
    {
        this.model.setSortDir(sortDir);
    }

    /**
     * @return Grid displaying data.
     */
    public Grid<C> getGrid()
    {
        return this.nameGrid;
    }

    /**
     * Initialize permissions.
     */
    public void initPermissions()
    {
        this.controller.handleEvent(new AppEvent(CommonEvents.PERMISSION_INITIALIZED));
        this.nameGrid.getView().setViewConfig(new PermissionGridViewConfig());
        this.nameGrid.addListener(Events.BeforeEdit, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                if (!nameGrid.getSelectionModel().getSelectedItem().getUserCanUpdate())
                {
                    be.setCancelled(true);
                }
            }
        });
    }

    /**
     * @return Return data displayed as a java list.
     */
    public List<C> getDataAsList()
    {
        return this.model.getModelStore().getModels();
    }

    /**
     * Display a save confirmation dialog box.
     */
    public void displaySaveConfirmBox()
    {
        ConfirmationDisplayer.display(CONSTANTS.Confirm(), CONSTANTS.YouAreGoingToDeleteData(),
                SimpleExplorerEvents.SAVE_CONFIRMED);
    }

    /**
     * Add the grid and set its store. If a column model is given, the grid use this column model. Else the grid will
     * only have one column for name.
     * 
     * @param cm
     *            The column model grid to use for the grid.
     */
    protected void setGrid(ColumnModel cm)
    {
        this.nameGrid = new EditableNameGrid<C>(this.model.getModelStore());

        if (cm != null)
        {
            nameGrid.reconfigure(this.nameGrid.getStore(), cm);
        }

        this.add(nameGrid);
    }

    /**
     * Set dialog style (size, no border and layout).
     */
    protected void setStyle()
    {
        this.setHeaderVisible(false);
        this.setLayout(new FitLayout());
        this.setBorders(false);

        this.setSize(300, 307);
    }

    /**
     * Set tool bar tool items : add, delete, refresh, save and saving status.
     */
    protected void setToolbar()
    {
        this.setTopComponent(toolBar);

        this.addToolItem.setScale(ButtonScale.MEDIUM);
        this.saveToolItem.setScale(ButtonScale.MEDIUM);

        this.toolBar.add(addToolItem);
        this.toolBar.add(deleteToolItem);
        this.toolBar.add(refreshToolItem);
        this.toolBar.add(saveToolItem);
        this.toolBar.add(savingToolItem);
        this.toolBar.add(new FillToolItem());
        this.toolBar.add(filterField);

        this.toolBar.setStyleAttribute("padding-right", "5px");
        this.savingToolItem.setBusy(CONSTANTS.Saving());
        this.savingToolItem.hide();
    }

    /** Call this method to allow only single selection inside explorer. */
    public void setSingleSelection()
    {
        this.nameGrid.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
    }

    /**
     * Add a filter field to the right of the explorer toolbar (before the text filter field).
     * 
     * @param field
     *            The field to add.
     */
    public void addMoreFilterField(Component field)
    {
        this.toolBar.insert(field, 6);
    }

    /**
     * Reconfigure grid explorer grid with column configuration list given in parameter.
     * 
     * @param columnConfigs
     *            The column configuration used to reconfigure explorer grid.
     */
    public void reconfigureGrid(List<ColumnConfig> columnConfigs)
    {
        this.reconfigureGrid(new ColumnModel(columnConfigs));
    }

    /**
     * Set instance created when a new row is added. It is useful if created instance needs default value.
     * 
     * @param instance
     *            The instance to set as default instance for row addition.
     */
    public void setDefaultInstance(C instance)
    {
        this.model.setDefaultInstance(instance);
    }

    /**
     * @return Simple explorer data store.
     */
    public ServiceStore<C> getStore()
    {
        return this.model.getModelStore();
    }

    /** Update selection data to ensure display will be correctly cleaned, then clear selection. */
    public void resetSelection()
    {
        this.nameGrid.resetSelection();
    }
}
