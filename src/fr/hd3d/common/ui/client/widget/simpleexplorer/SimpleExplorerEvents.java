package fr.hd3d.common.ui.client.widget.simpleexplorer;

import com.extjs.gxt.ui.client.event.EventType;


/**
 * Events for simple explorer controller.
 * 
 * @author HD3D
 */
public class SimpleExplorerEvents
{
    public static final EventType ADD_CLICKED = new EventType();
    public static final EventType DELETE_CLICKED = new EventType(300);
    public static final EventType REFRESH_CLICKED = new EventType();
    public static final EventType SAVE_CLICKED = new EventType();
    public static final EventType CREATE_SUCCESS = new EventType();
    public static final EventType UPDATE_SUCCESS = new EventType();
    public static final EventType DELETE_SUCCESS = new EventType();
    public static final EventType SAVE_SUCCESS = new EventType();
    public static final EventType SAVE_CONFIRMED = new EventType();
}
