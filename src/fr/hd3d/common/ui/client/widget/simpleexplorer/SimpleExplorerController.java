package fr.hd3d.common.ui.client.widget.simpleexplorer;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.userrights.PermissionUtil;


/**
 * Controller that handles event for the simple explorer.
 * 
 * @author HD3D
 */
public class SimpleExplorerController<C extends Hd3dModelData> extends MaskableController
{
    /** Simple explorer panel managed by the controller. */
    final protected ISimpleExplorerPanel<C> view;
    /** Model handling data. */
    final protected SimpleExplorerModel<C> model;

    /** Number of update query transmitted to the server. */
    protected int nbUpdate = 0;
    /** Number of delete query transmitted to the server. */
    protected int nbDelete = 0;
    /** Is saving the data or not. */
    private boolean isSaving = false;

    /**
     * Default constructor.
     * 
     * @param model
     *            Model handling data.
     * @param view
     *            Simple explorer panel managed by the controller.
     */
    public SimpleExplorerController(SimpleExplorerModel<C> model, ISimpleExplorerPanel<C> view)
    {
        this.model = model;
        this.view = view;

        this.registerEvents();
    }

    public int getNbUpdate()
    {
        return nbUpdate;
    }

    public void setNbUpdate(int nbUpdate)
    {
        this.nbUpdate = nbUpdate;
    }

    public int getNbDelete()
    {
        return nbDelete;
    }

    public void setNbDelete(int nbDelete)
    {
        this.nbDelete = nbDelete;
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();

        if (this.isMasked)
        {
            this.forwardToChild(event);
        }
        else if (type == CommonEvents.PERMISSION_INITIALIZED)
        {
            this.onPermissionInitialized();
            this.forwardToChild(event);
        }
        else if (type == SimpleExplorerEvents.ADD_CLICKED)
        {
            this.onAddClicked();
        }
        else if (type == SimpleExplorerEvents.DELETE_CLICKED)
        {
            this.onDeleteClicked();
        }
        else if (type == SimpleExplorerEvents.REFRESH_CLICKED)
        {
            this.onRefreshClicked();
        }
        else if (type == SimpleExplorerEvents.SAVE_CLICKED)
        {
            this.onSaveClicked();
        }
        else if (type == SimpleExplorerEvents.SAVE_CONFIRMED)
        {
            this.onSaveConfirmed();
        }
        else if (type == SimpleExplorerEvents.CREATE_SUCCESS)
        {
            this.onEndModelSaved();
        }
        else if (type == SimpleExplorerEvents.UPDATE_SUCCESS)
        {
            this.onEndModelSaved();
        }
        else if (type == SimpleExplorerEvents.DELETE_SUCCESS)
        {
            this.onEndSaveDelete();
        }
        else if (type == CommonEvents.ERROR)
        {
            this.onError(event);
        }
        else
        {
            this.forwardToChild(event);
        }
    }

    /**
     * When permissions are initialized, it hides not allowed widgets.
     */
    protected void onPermissionInitialized()
    {
        String path = this.model.getNewModelInstance().getPermissionPath();

        this.view.enableCreateToolItem(PermissionUtil.hasCreateRightsOnPath(path)
                && PermissionUtil.hasUpdateRightsOnPath(path));
        this.view.enableDeleteToolItem(PermissionUtil.hasDeleteRightsOnPath(path));
        this.view.enableSaveToolItem(PermissionUtil.hasUpdateRightsOnPath(path));
    }

    /**
     * Insert a new row in the simple explorer grid.
     */
    protected void onAddClicked()
    {
        C model = this.model.getNewModelInstance();
        if (this.model.getModelStore() != null)
        {
            this.view.resetSelection();
            this.model.getModelStore().insert(model, 0);
        }
    }

    /**
     * When delete is clicked, it removes selection from the data grid (from its list store). And add it to the deleted
     * record list (list to commit to the service when save is clicked).
     */
    protected void onDeleteClicked()
    {
        for (C model : this.view.getSelection())
        {
            if (model.getId() != null)
            {
                this.model.addDeletedRecord(model);
            }
            this.model.getModelStore().remove(model);
        }

        this.view.resetSelection();
    }

    /**
     * When delete is complete it decrements the number of delete requests transmitted. Then it checks if there are
     * another responses to wait.
     */
    protected void onEndSaveDelete()
    {
        if (nbDelete > 0)
            this.nbDelete--;
        this.checkEndSave();
    }

    /**
     * When delete is complete it decrements the number of delete request transmitted. Then it checks if there is other
     * are responses to wait.
     */
    protected void onEndModelSaved()
    {
        if (nbUpdate > 0)
            this.nbUpdate--;
        this.checkEndSave();
    }

    /**
     * Check if there are another responses to wait. If not, it forwards SAVE_SUCCESS event and enables button stuff.
     */
    protected void checkEndSave()
    {
        if (nbUpdate == 0 && nbDelete == 0)
        {
            EventDispatcher.forwardEvent(SimpleExplorerEvents.SAVE_SUCCESS);

            this.model.commitChanges();
            this.model.clearCreatedRecords();
            this.model.clearDeletedRecords();
            this.isSaving = false;
            this.view.hideSaving();
            this.view.enableButtons();
            EventDispatcher.forwardEvent(SimpleExplorerEvents.REFRESH_CLICKED);
        }
    }

    /**
     * Refresh the data grid with data from server.
     */
    public void onRefreshClicked()
    {
        this.model.clearCreatedRecords();
        this.model.clearDeletedRecords();
        this.model.refresh();
    }

    /**
     * When save is clicked, it checks if some row has been deleted and ask for confirmation before doing save actions.
     */
    protected void onSaveClicked()
    {
        if (this.model.getDeletedRecords().size() > 0)
        {
            this.view.displaySaveConfirmBox();
        }
        else
        {
            EventDispatcher.forwardEvent(SimpleExplorerEvents.SAVE_CONFIRMED);
        }
    }

    /**
     * When save is confirmed, it disabled all button stuff and send requests to update the service data.
     */
    public void onSaveConfirmed()
    {
        nbUpdate = 2;
        nbDelete = this.model.getDeletedRecords().size();

        if (nbUpdate > 0 || nbDelete > 0)
        {
            this.isSaving = true;
            this.view.showSaving();
            this.view.disableButtons();
            this.model.saveAll();
        }
        else
        {
            EventDispatcher.forwardEvent(SimpleExplorerEvents.SAVE_SUCCESS);
        }
    }

    /**
     * When error occurs, it considers that saving is done.
     */
    private void onError(AppEvent event)
    {

        this.view.hideSaving();
        this.view.enableButtons();
        nbUpdate = 0;
        nbDelete = 0;

        if (isSaving)
        {
            this.model.refresh();
        }

        this.isSaving = false;
    }

    /**
     * Register events that controller can handle.
     */
    protected void registerEvents()
    {
        this.registerEventTypes(CommonEvents.ERROR);
        this.registerEventTypes(CommonEvents.PERMISSION_INITIALIZED);
        this.registerEventTypes(SimpleExplorerEvents.ADD_CLICKED);
        this.registerEventTypes(SimpleExplorerEvents.DELETE_CLICKED);
        this.registerEventTypes(SimpleExplorerEvents.REFRESH_CLICKED);
        this.registerEventTypes(SimpleExplorerEvents.SAVE_CLICKED);
        this.registerEventTypes(SimpleExplorerEvents.SAVE_CONFIRMED);
        this.registerEventTypes(SimpleExplorerEvents.CREATE_SUCCESS);
        this.registerEventTypes(SimpleExplorerEvents.UPDATE_SUCCESS);
        this.registerEventTypes(SimpleExplorerEvents.DELETE_SUCCESS);
    }
}
