package fr.hd3d.common.ui.client.widget.simpleexplorer;

import java.util.List;

import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.mvc.Controller;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.util.CollectionUtils;


/**
 * 
 * 
 * @author HD3D
 * 
 * @param <M>
 *            Data managed by simple explorer contained inside dialog.
 */
public class SimpleExplorerDialog<M extends RecordModelData> extends Dialog
{
    /** Default dialog width. */
    public static final int DEFAULT_WIDTH = 400;

    /** Simple explorer needed to handle CRUD operations on M data. */
    protected SimpleExplorerPanel<M> explorer;

    /** Do not use this constructor, it is here for overriding purposes. */
    public SimpleExplorerDialog()
    {
        super();
    }

    /**
     * Default constructor : set styles and configure explorer with one column for name.
     * 
     * @param reader
     *            Reader used by simple explorer to parse data coming from server.
     * @param title
     *            Dialog title.
     */
    public SimpleExplorerDialog(IReader<M> reader, String title)
    {
        this.explorer = new SimpleExplorerPanel<M>(reader);
        this.setStyles();
        this.setExplorer(null);

        this.setHeading(title);
    }

    /**
     * Constructor : set styles and configure explorer with <i>columns</i>.
     * 
     * @param reader
     *            Reader used by simple explorer to parse data coming from server.
     * @param title
     *            Dialog title.
     * @param columns
     *            The column configuration list for simple explorer.
     * 
     */
    public SimpleExplorerDialog(IReader<M> reader, String title, List<ColumnConfig> columns)
    {
        this.explorer = new SimpleExplorerPanel<M>(reader);
        this.setStyles();
        this.setExplorer(columns);

        this.setHeading(title);
    }

    /**
     * @return Simple explorer controller.
     */
    public Controller getController()
    {
        return this.explorer.getController();
    }

    /** When dialog is shown, the simple explorer is refreshed and simple explorer controller is unmasked. */
    @Override
    public void show()
    {
        this.explorer.refresh();
        super.show();
        this.explorer.unIdle();
    }

    /** When dialog is hidden, the simple explorer controller is masked. */
    @Override
    public void hide()
    {
        super.hide();
        this.explorer.idle();
    }

    /** @return Dialog explorer service store. */
    public ServiceStore<M> getStore()
    {
        return this.explorer.getStore();
    }

    /**
     * Reconfigure simple explorer grid with <i>columns</i>.
     * 
     * @param columns
     *            Column configuration list.
     */
    protected void setExplorer(List<ColumnConfig> columns)
    {
        if (CollectionUtils.isNotEmpty(columns))
            this.explorer.reconfigureGrid(columns);

        this.explorer.idle();
        this.add(explorer);
    }

    /** Set dialog styles and make column "name" as sorted column by default. */
    protected void setStyles()
    {
        this.setButtons("");
        this.setLayout(new FitLayout());
        this.setWidth(DEFAULT_WIDTH);
        this.setModal(true);
        this.setResizable(true);

        this.explorer.setSortField(RecordModelData.NAME_FIELD);
        this.explorer.setSortDir(SortDir.ASC);
    }
}
