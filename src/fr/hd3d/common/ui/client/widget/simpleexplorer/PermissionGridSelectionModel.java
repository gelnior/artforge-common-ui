package fr.hd3d.common.ui.client.widget.simpleexplorer;

import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.widget.grid.CellSelectionModel;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;


public class PermissionGridSelectionModel<C extends Hd3dModelData> extends CellSelectionModel<C>
{
    @Override
    protected void handleMouseDown(GridEvent<C> e)
    {
        C m = listStore.getAt(e.getRowIndex());
        m.setUserCanUpdate(false);

        if (m.getUserCanUpdate())
            super.handleMouseDown(e);
    }
}
