package fr.hd3d.common.ui.client.widget.tageditor;

import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.KeyListener;


public class TagKeyListener extends KeyListener
{
    protected TagEditorController controller;

    public TagKeyListener(TagEditorController controller)
    {
        this.controller = controller;
    }

    @Override
    public void componentKeyUp(ComponentEvent event)
    {
        this.controller.handleKeyUp(event.getKeyCode());
    }
}
