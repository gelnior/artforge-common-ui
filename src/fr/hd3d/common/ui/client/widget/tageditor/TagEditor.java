package fr.hd3d.common.ui.client.widget.tageditor;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.mvc.Controller;
import com.extjs.gxt.ui.client.util.Margins;
import com.extjs.gxt.ui.client.util.Padding;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.Status;
import com.extjs.gxt.ui.client.widget.layout.HBoxLayout;
import com.extjs.gxt.ui.client.widget.layout.HBoxLayout.HBoxLayoutAlign;
import com.extjs.gxt.ui.client.widget.layout.HBoxLayoutData;

import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.technical.TagModelData;
import fr.hd3d.common.ui.client.widget.ToolBarButton;


/**
 * Widget allowing user to manage tags on a selection of Hd3dModelData given in parameter.
 * 
 * @author HD3D
 */
public class TagEditor extends LayoutContainer implements ITagEditor
{
    /** CSS class for the "add tag" icon. */
    public static final String TAG_ADD_ICON = "tag-add-icon";
    /** CSS class for the "remove tag" icon. */
    public static final String TAG_REMOVE_ICON = "tag-remove-icon";

    /** Combo box used for adding new tag. */
    protected TagComboBox combo = new TagComboBox();
    /** List of tags label represented. */
    protected List<TagLabel> tags = new ArrayList<TagLabel>();
    /** Button needed for adding */
    protected ToolBarButton addButton = new ToolBarButton(Hd3dImages.getAddTagIcon(), "Add tags", null);

    /** Model that manages editor stores. */
    protected TagEditorModel model = new TagEditorModel(combo.getStore());
    /** Controller that manages editor events. */
    protected TagEditorController controller = new TagEditorController(model, this);

    /** Add button width. */
    protected int buttonWidth;
    /** Saving indicator. */
    protected Status status = new Status();

    /** True if the combo box is visible, false either. */
    protected boolean isComboVisible = false;
    /** True if data are loading, false either. */
    protected boolean isLoading = false;

    /**
     * Default constructor.
     */
    public TagEditor()
    {
        this.setAddButton();
        this.setStyle();
        this.setStores();
        this.setListeners();
    }

    /**
     * @return Tag editor controller.
     */
    public Controller getController()
    {
        return controller;
    }

    /**
     * Refresh selection and displayed tags.
     */
    public void refreshSelection(List<Hd3dModelData> selection)
    {
        if (selection == null || selection.size() == 0)
        {
            this.addButton.disable();
            this.model.getEditorStore().removeAll();
        }
        else
        {
            this.addButton.enable();
            this.controller.onRefreshSelection(selection);
        }
    }

    /**
     * Resize the editor depending on the widget it contains.
     */
    public void reSize()
    {
        this.layout();
        int width = 0;
        width = allLabelWidth();
        if (isComboVisible)
        {
            width += 160;
        }
        else
        {
            width += 30;
        }
        if (isLoading)
        {
            width += 30;
        }

        this.setWidth(width);
    }

    /**
     * Return sum of all the tag label width.
     */
    public int allLabelWidth()
    {
        int width = 0;
        for (TagLabel tag : tags)
        {
            width += tag.getWidth();
        }

        return width;
    }

    /**
     * Remove the tag label corresponding to name from the editor view.
     * 
     * @param name
     *            The tag name corresponding to the tag label.
     */
    public void removeTagLabel(String name)
    {
        TagLabel tagToDelete = this.findTagLabel(name);
        this.remove(tagToDelete);
        this.tags.remove(tagToDelete);
        this.reSize();
    }

    /**
     * @param name
     *            The tag name corresponding to the tag label.
     * @return The tag label whose tag name is equal to name.
     */
    protected TagLabel findTagLabel(String name)
    {
        for (TagLabel tag : tags)
        {
            if (tag.getTagName().equals(name))
            {
                return tag;
            }
        }
        return null;
    }

    /**
     * Add a tag label to the component.
     * 
     * @param tag
     *            The tag that need a new tag label.
     */
    public void addTagLabel(TagModelData tag)
    {
        TagLabel tagLabel = new TagLabel(tag);

        this.insert(tagLabel, tags.size(), new HBoxLayoutData(new Margins(0, 0, 0, 0)));
        this.tags.add(tagLabel);
        this.reSize();
    }

    /**
     * @return The combo box field value.
     */
    public String getComboRawValue()
    {
        return this.combo.getRawValue();
    }

    /**
     * @return The combo box selected value.
     */
    public TagModelData getValue()
    {
        return this.combo.getValue();
    }

    /**
     * Reload tag combo box data.
     */
    public void reloadCombo()
    {
        this.combo.reLoad();
    }

    /**
     * Set combo value to <i>tag</i>.
     * 
     * @param tag
     *            The tag to select.
     */
    public void setComboValue(TagModelData tag)
    {
        this.combo.setValue(tag);
    }

    /**
     * Expand tag combo box.
     */
    public void expandCombo()
    {
        this.combo.expand();
    }

    /**
     * Collapse tag combo box.
     */
    public void collapseCombo()
    {
        this.combo.collapse();
    }

    /**
     * True if tag combo box is expanded.
     */
    public boolean isComboExpanded()
    {
        return this.combo.isExpanded();
    }

    /**
     * Add the add button and register its width.
     */
    protected void setAddButton()
    {
        this.add(addButton, new HBoxLayoutData(new Margins(0, 0, 0, 0)));
        buttonWidth = addButton.getWidth();
    }

    /**
     * Set styles : layout mainly (HBox layout).
     */
    protected void setStyle()
    {
        HBoxLayout layout = new HBoxLayout();
        layout.setPadding(new Padding(0, 10, 0, 10));
        layout.setHBoxLayoutAlign(HBoxLayoutAlign.MIDDLE);
        this.status.setBusy("");

        this.setLayout(layout);
        this.setHeight(24);
        // this.addButton.disable();
        this.reSize();
    }

    /**
     * Set combo constraint and bind editor store to this view.
     */
    protected void setStores()
    {
        this.combo.addConstraint(this.model.getConstraint());
        TagEditorStoreBinder binder = new TagEditorStoreBinder(this.model.getEditorStore(), this);
        binder.init();
    }

    /**
     * Sets :
     * <ul>
     * <li>two small listeners for combo box and add button hiding/showing.</li>
     * <li>key listener on combo box.</li>
     * </ul>
     */
    protected void setListeners()
    {
        this.combo.addKeyListener(new TagKeyListener(this.controller));

        addButton.addListener(Events.OnMouseOver, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                if (model.getSelection().size() > 0)
                {
                    insert(combo, tags.size(), new HBoxLayoutData(new Margins(0, 0, 0, 0)));
                    remove(addButton);
                    isComboVisible = true;
                    reSize();
                }
            }
        });

        combo.addListener(Events.OnMouseOut, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                insert(addButton, tags.size(), new HBoxLayoutData(new Margins(0, 0, 0, 0)));
                remove(combo);
                isComboVisible = false;
                reSize();
            }
        });
    }

    /**
     * Remove all tags label from the tag editor view.
     */
    public void removeAllTagLabels()
    {
        for (TagLabel tagLabel : tags)
        {
            this.remove(tagLabel);
        }
        this.tags.clear();
        this.reSize();
    }

    public void showLoading()
    {
        // insert(status, model.getEditorCount(), new HBoxLayoutData(new Margins(0, 0, 0, 0)));
        // isLoading = true;
        // this.reSize();
    }

    public void hideLoading()
    {
        // this.remove(this.status);
        // isLoading = false;
        // this.reSize();
    }
}
