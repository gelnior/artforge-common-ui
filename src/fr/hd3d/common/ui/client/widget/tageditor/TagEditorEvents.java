package fr.hd3d.common.ui.client.widget.tageditor;

import com.extjs.gxt.ui.client.event.EventType;


public class TagEditorEvents
{

    public static final EventType REMOVE_TAG_CLICKED = new EventType();
    public static final EventType SELECTION_RETRIEVED = new EventType();
    public static final EventType TAG_SAVED = new EventType();
    public static final EventType TAG_REFRESHED = new EventType();

}
