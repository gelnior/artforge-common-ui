package fr.hd3d.common.ui.client.widget.tageditor;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.store.ListStore;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.technical.TagModelData;
import fr.hd3d.common.ui.client.service.parameter.Constraint;


/**
 * Model containing tag data for tag combo box.
 * 
 * @author HD3D
 */
public class TagEditorModel
{
    /** The tag combo list store. */
    protected ListStore<TagModelData> comboStore;
    /** The tag combo list store. */
    protected ListStore<TagModelData> editorStore = new ListStore<TagModelData>();

    /** Constraint needed to filter data in the combo box. */
    protected Constraint constraint = new Constraint(EConstraintOperator.like, RecordModelData.NAME_FIELD, "%", "");

    /** Selection whom tags are managed by the tag editor. */
    protected List<Hd3dModelData> selection = new ArrayList<Hd3dModelData>();
    private Hd3dModelData objectToHandle;

    /**
     * Default constructor.
     * 
     * @param store
     *            The store of the combo box.
     */
    public TagEditorModel(ListStore<TagModelData> store)
    {
        this.comboStore = store;
    }

    /**
     * @return The combo tag list store.
     */
    public ListStore<TagModelData> getComboStore()
    {
        return this.comboStore;
    }

    /**
     * @return The widget list store.
     */
    public ListStore<TagModelData> getEditorStore()
    {
        return this.editorStore;
    }

    /**
     * @return The number of tag contained by the tag editor.
     */
    public int getEditorCount()
    {
        return this.editorStore.getCount();
    }

    /**
     * @return The number of tags contains in the store.
     */
    public int getComboCount()
    {
        return this.comboStore.getCount();
    }

    /**
     * @return Return the constraint used to filter combo data.
     */
    public Constraint getConstraint()
    {
        return constraint;
    }

    public List<Hd3dModelData> getSelection()
    {
        return selection;
    }

    public Hd3dModelData getObjectToHandle()
    {
        return this.objectToHandle;
    }

    public void setObjectToHandle(Hd3dModelData object)
    {
        this.objectToHandle = object;
    }
}
