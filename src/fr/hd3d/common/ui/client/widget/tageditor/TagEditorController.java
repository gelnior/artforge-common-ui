package fr.hd3d.common.ui.client.widget.tageditor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.mvc.Controller;
import com.google.gwt.event.dom.client.KeyCodes;

import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.technical.TagModelData;
import fr.hd3d.common.ui.client.service.parameter.IUrlParameter;
import fr.hd3d.common.ui.client.service.parameter.TagsMode;


/**
 * Controller handling tag editor events.
 * 
 * @author HD3D
 */
public class TagEditorController extends Controller
{
    /** Tag editor view. */
    protected final ITagEditor view;
    /** Tag editor model. */
    protected final TagEditorModel model;

    /**
     * Default constructor.
     * 
     * @param model
     *            Tag editor model.
     * @param view
     *            Tag editor view.
     */
    public TagEditorController(TagEditorModel model, ITagEditor view)
    {
        this.view = view;
        this.model = model;

        this.registerEvents();
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();

        if (type == TagEditorEvents.REMOVE_TAG_CLICKED)
        {
            this.onRemoveTag(event);
        }
        else if (type == TagEditorEvents.SELECTION_RETRIEVED)
        {
            this.onSelectionRetrieved();
        }
        else if (type == TagEditorEvents.TAG_REFRESHED)
        {
            this.onTagRefreshed(event);
        }
        else if (type == TagEditorEvents.TAG_SAVED)
        {
            this.onTagSaved(event);
        }
    }

    /**
     * When tag is refreshed, a new tag is added to the tag list store. If tag does not exist in database, it is saved
     * to database. Then it is added to tag store after TAG_SAVED event handling.
     * 
     * @param event
     */
    private void onTagRefreshed(AppEvent event)
    {
        TagModelData tag = event.getData();

        if (tag.getId() == null)
        {
            AppEvent eventToForward = new AppEvent(TagEditorEvents.TAG_SAVED, tag);
            tag.save(eventToForward);
        }
        else
        {
            this.addTag(tag);
        }
    }

    /**
     * Evaluates which key is typed and adopt corresponding behavior.
     * <ul>
     * <li>Enter key cause the tag to be added to selection. If the tag does not exist, it is saved to database.</li>
     * <li>Text key causes combo reloading and expanding, if the combo was not previously empty.</li>
     * <li>Down key causes combo reloading too.</li>
     * </ul>
     * 
     * @param keyCode
     *            The key code on which to react.
     */
    public void handleKeyUp(int keyCode)
    {
        String name = this.view.getComboRawValue();

        if (keyCode == KeyCodes.KEY_ENTER && (name.length() > 0 || this.view.getValue() != null))
        {
            this.onEnterKeyUp(name);
        }
        else if (name.length() > 1 && isTextKey(keyCode))
        {
            this.onCharacterTyped(name);
        }
        else if (name.length() == 1 && isTextKey(keyCode))
        {
            this.onFirstCharacterTyped(name);
        }
        else if (keyCode == KeyCodes.KEY_DOWN)
        {
            this.onComboDownKeyisUp(name);
        }
        else
        {
            this.view.collapseCombo();
        }
    }

    /**
     * When enter key is up and tag field not empty, it add the tag to selection. If tag does not already exist, it
     * creates it.
     * 
     * @param name
     */
    private void onEnterKeyUp(String name)
    {
        TagModelData value = this.view.getValue();
        TagModelData tag;

        if (value == null || this.model.getComboCount() > 0)
        {
            tag = this.model.getComboStore().findModel(RecordModelData.NAME_FIELD, name);

            if (tag == null)
            {
                tag = new TagModelData(name);
                tag.refreshByName();
            }
            else
            {
                this.addTag(value);
            }
        }
        else
        {
            this.addTag(value);
        }

        this.view.setComboValue(null);
    }

    /**
     * Add a tag to the tag list store (for displaying) and it to the tag list inside handled object tag list. Object is
     * saved afterwards.
     * 
     * @param tag
     *            The tag to add
     */
    public void addTag(TagModelData tag)
    {
        if (this.model.getEditorStore().findModel(TagModelData.NAME_FIELD, tag.getName()) == null)
        {
            this.model.getEditorStore().add(tag);
            Hd3dModelData object = this.model.getObjectToHandle();
            if (object.getTagIDs() == null)
            {
                object.setTagIDs(new ArrayList<Long>());
            }
            object.getTagIDs().add(tag.getId());

            object.save();
        }
    }

    /**
     * When tag is saved, it is added to current object and to tag store to be displayed.
     * 
     * @param event
     *            The tag saved event.
     */
    private void onTagSaved(AppEvent event)
    {
        this.addTag((TagModelData) event.getData());
    }

    /**
     * When a character is typed, the combo box is reloaded with an updated filter. If the combo store was previously
     * empty, nothing is reloaded.
     * 
     * @param name
     *            the name of the tag currently typed.
     */
    private void onCharacterTyped(String name)
    {
        this.model.getConstraint().setLeftMember(name + "%");
        if (this.model.getComboStore().getCount() > 0)
        {
            this.view.reloadCombo();
            this.view.expandCombo();
        }
    }

    /**
     * When first character is typed, the combo store filter is updated and combo is reloaded.
     * 
     * @param name
     *            the name of the tag currently typed.
     */
    private void onFirstCharacterTyped(String name)
    {
        this.model.getConstraint().setLeftMember(name + "%");
        this.view.reloadCombo();
        this.view.expandCombo();
    }

    /**
     * When down key is up, if name is empty, it reloads the combo box with no constraint.
     * 
     * @param name
     *            the name of the tag currently typed.
     */
    private void onComboDownKeyisUp(String name)
    {
        if (name.length() == 0 && !this.view.isComboExpanded())
        {
            this.model.getConstraint().setLeftMember("%");
            this.view.reloadCombo();
        }
    }

    /**
     * When a tag is ask for removing, it is removed from editor store and from view.
     * 
     * 
     * @param event
     *            The remove tag clicked event.
     */
    protected void onRemoveTag(AppEvent event)
    {
        TagModelData tag = (TagModelData) event.getData();
        this.model.getEditorStore().remove(tag);
        this.model.getObjectToHandle().getTagIDs().remove(tag.getId());
        Logger.log(this.model.getObjectToHandle().toPUTJson());
        this.model.getObjectToHandle().save();
    }

    /**
     * @param code
     *            The key to test.
     * @return True if the key is not a navigator key (up, left, home, right...)
     */
    protected boolean isTextKey(int code)
    {
        boolean isTestKey = code != KeyCodes.KEY_DOWN;
        isTestKey &= code != KeyCodes.KEY_UP;
        isTestKey &= code != KeyCodes.KEY_LEFT;
        isTestKey &= code != KeyCodes.KEY_HOME;
        isTestKey &= code != KeyCodes.KEY_RIGHT;
        isTestKey &= code != KeyCodes.KEY_PAGEDOWN;
        isTestKey &= code != KeyCodes.KEY_PAGEUP;

        return isTestKey;
    }

    /**
     * Refresh tag editor depending on selection.
     * 
     * @param selection
     *            The explorer selection.
     */
    public void onRefreshSelection(List<Hd3dModelData> selection)
    {
        this.view.showLoading();
        this.model.getEditorStore().removeAll();

        List<IUrlParameter> parameters = new ArrayList<IUrlParameter>();
        parameters.add(new TagsMode());

        if (selection.size() == 1)
        {
            this.model.getSelection().addAll(selection);

            Hd3dModelData object = new Hd3dModelData();
            Hd3dModelData firstSelected = selection.get(0);

            object.setId(firstSelected.getId());
            object.setSimpleClassName(firstSelected.getSimpleClassName());
            object.setClassName(firstSelected.getClassName());
            object.setDefaultPath(firstSelected.getDefaultPath());

            this.model.setObjectToHandle(object);
            this.model.getObjectToHandle().refreshWithParams(new AppEvent(TagEditorEvents.SELECTION_RETRIEVED),
                    parameters);
        }
    }

    /**
     * When selection is retrieved, it updates the displayed tag.
     */
    public void onSelectionRetrieved()
    {
        List<Long> ids = this.model.getObjectToHandle().getTagIDs();
        List<String> names = this.model.getObjectToHandle().getTagNames();
        List<TagModelData> tags = new ArrayList<TagModelData>();

        if (ids != null)
        {
            for (int i = 0; i < ids.size(); i++)
            {
                TagModelData tag = new TagModelData(ids.get(i), names.get(i));
                tags.add(tag);
            }
        }

        Collections.sort(tags, new Comparator<TagModelData>() {
            public int compare(TagModelData arg0, TagModelData arg1)
            {
                return arg0.getName().compareTo(arg1.getName());
            }
        });

        for (TagModelData tag : tags)
        {
            this.model.getEditorStore().add(tag);
        }
        this.view.hideLoading();
    }

    /**
     * Register Events supported by tag controller.
     */
    protected void registerEvents()
    {
        this.registerEventTypes(TagEditorEvents.REMOVE_TAG_CLICKED);
        this.registerEventTypes(TagEditorEvents.SELECTION_RETRIEVED);
        this.registerEventTypes(TagEditorEvents.TAG_SAVED);
        this.registerEventTypes(TagEditorEvents.TAG_REFRESHED);
    }
}
