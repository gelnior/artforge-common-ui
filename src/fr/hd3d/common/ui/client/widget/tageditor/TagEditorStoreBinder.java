package fr.hd3d.common.ui.client.widget.tageditor;

import java.util.List;

import com.extjs.gxt.ui.client.binder.StoreBinder;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.store.StoreEvent;
import com.extjs.gxt.ui.client.widget.Component;
import com.extjs.gxt.ui.client.widget.LayoutContainer;

import fr.hd3d.common.ui.client.modeldata.technical.TagModelData;


/**
 * Binder that binds tag editor store and tag editor view. For each tag added or removed a tag label is added or
 * removed.
 * 
 * @author HD3D
 */
public class TagEditorStoreBinder extends StoreBinder<ListStore<TagModelData>, LayoutContainer, TagModelData>
{
    protected final TagEditor editor;

    public TagEditorStoreBinder(ListStore<TagModelData> store, TagEditor editor)
    {
        super(store, editor);
        this.editor = editor;
    }

    @Override
    protected void createAll()
    {}

    @Override
    public Component findItem(TagModelData model)
    {
        return null;
    }

    @Override
    protected List<TagModelData> getSelectionFromComponent()
    {
        return null;
    }

    @Override
    protected void onAdd(StoreEvent<TagModelData> se)
    {

        this.editor.addTagLabel(se.getModels().get(0));
    }

    @Override
    protected void onRemove(StoreEvent<TagModelData> se)
    {
        this.editor.removeTagLabel(se.getModel().getName());
    }

    @Override
    protected void onUpdate(StoreEvent<TagModelData> se)
    {

    }

    @Override
    protected void removeAll()
    {
        this.editor.removeAllTagLabels();
    }

    @Override
    protected void setSelectionFromProvider(List<TagModelData> selection)
    {

    }

    @Override
    protected void update(TagModelData model)
    {

    }

};
