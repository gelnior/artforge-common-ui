package fr.hd3d.common.ui.client.widget.tageditor;

import fr.hd3d.common.ui.client.modeldata.reader.TagReader;
import fr.hd3d.common.ui.client.modeldata.technical.TagModelData;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.Pagination;
import fr.hd3d.common.ui.client.widget.ModelDataComboBox;


/**
 * Combo box used for tag selection in the tag editor.
 * 
 * @author HD3D
 */
public class TagComboBox extends ModelDataComboBox<TagModelData>
{
    public TagComboBox()
    {
        super(new TagReader());

        this.setHideTrigger(true);
        this.setForceSelection(false);
        this.setTypeAhead(false);
        this.store.addParameter(new Pagination(0L, 10L));
    }

    public void addConstraint(Constraint constraint)
    {
        this.store.addParameter(constraint);
    }

    public void reLoad()
    {
        this.store.reload();

    }
}
