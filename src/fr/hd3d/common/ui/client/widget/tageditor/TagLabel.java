package fr.hd3d.common.ui.client.widget.tageditor;

import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.util.TextMetrics;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.form.LabelField;
import com.extjs.gxt.ui.client.widget.layout.HBoxLayout;

import fr.hd3d.common.ui.client.listener.ButtonClickListener;
import fr.hd3d.common.ui.client.modeldata.technical.TagModelData;
import fr.hd3d.common.ui.client.widget.ToolBarButton;


/**
 * Component use to display a tag : it containing a label of the tag name and a remove button. Remove button fires
 * REMOVE_TAG event to all controllers (event contains TAG firing the event).
 * 
 * @author HD3D
 */
public class TagLabel extends LayoutContainer
{
    /** Tag represented by the tag label. */
    protected final TagModelData tag;
    /** Button needed to delete the tag. */
    protected ToolBarButton removeButton = new ToolBarButton(TagEditor.TAG_REMOVE_ICON, "Remove tag", null);

    /**
     * Default constructor, set widget and attributes.
     * 
     * @param tag
     *            The tag is represented by the widget.
     */
    public TagLabel(TagModelData tag)
    {
        this.tag = tag;

        this.setStyles();
        this.setLabel();
        this.setDeleteButton();
        this.setWidth(TextMetrics.get().getWidth(tag.getName()) + removeButton.getWidth() + 25);
        this.layout();
    }

    /**
     * Set styles : layout and width.
     */
    private void setStyles()
    {
        HBoxLayout layout = new HBoxLayout();
        this.setLayout(layout);
    }

    /**
     * Set label.
     */
    private void setLabel()
    {
        LabelField label = new LabelField(tag.getName());
        label.setStyleAttribute("padding-top", "6px");
        this.add(label);
    }

    /**
     * Set delete button.
     */
    private void setDeleteButton()
    {
        AppEvent event = new AppEvent(TagEditorEvents.REMOVE_TAG_CLICKED, tag);
        removeButton.addSelectionListener(new ButtonClickListener(event));

        this.add(removeButton);
    }

    /**
     * @return tag represented by the tag label.
     */
    public TagModelData getTag()
    {
        return tag;
    }

    /**
     * @return tag name represented by the tag label.
     */
    public String getTagName()
    {
        return tag.getName();
    }
}
