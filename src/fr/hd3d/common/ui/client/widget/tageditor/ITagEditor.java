package fr.hd3d.common.ui.client.widget.tageditor;

import fr.hd3d.common.ui.client.modeldata.technical.TagModelData;


public interface ITagEditor
{

    void removeTagLabel(String name);

    int allLabelWidth();

    String getComboRawValue();

    TagModelData getValue();

    void addTagLabel(TagModelData tag);

    void setComboValue(TagModelData tag);

    void reloadCombo();

    void expandCombo();

    boolean isComboExpanded();

    void collapseCombo();

    void showLoading();

    void hideLoading();

}
