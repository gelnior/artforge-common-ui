package fr.hd3d.common.ui.client.widget;

import java.util.List;

import com.extjs.gxt.ui.client.event.LoadListener;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.client.enums.EConstraintLogicalOperator;
import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.reader.TaskTypeReader;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.LogicConstraint;


public class AutoMultiTaskTypeComboBox extends AutoMultiModelCombo<TaskTypeModelData>
{
    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    protected LogicConstraint projectFilterGroup;

    public AutoMultiTaskTypeComboBox()
    {
        this(false);
    }

    public AutoMultiTaskTypeComboBox(boolean emptyField)
    {
        super(new TaskTypeReader());

        this.setStyles();
        this.store.setPath(ServicesPath.getPath(TaskTypeModelData.SIMPLE_CLASS_NAME));
        this.store.addParameter(this.getProjectFilter());
    }

    public void setProject(Long projectId)
    {
        LogicConstraint fg = this.getProjectFilter();
        if (projectId != null)
        {
            fg.setRightMember(new Constraint(EConstraintOperator.eq, "project.id", projectId));
        }
        this.setData();
    }

    private LogicConstraint getProjectFilter()
    {
        if (projectFilterGroup == null)
        {
            projectFilterGroup = new LogicConstraint(EConstraintLogicalOperator.OR);
            projectFilterGroup.setLeftMember(new Constraint(EConstraintOperator.isnull, "project.id", null));
        }

        return this.projectFilterGroup;
    }

    /**
     * Set combo styles.
     */
    @Override
    protected void setStyles()
    {
        super.setStyles();

        this.setEmptyText("Select Task Type...");
        this.setDisplayField(RecordModelData.NAME_FIELD);
        this.setTemplate(getXTemplate());
    }

    @Override
    public void setValueByIds(final List<Long> ids)
    {
        this.store.removeParameter(this.projectFilterGroup);
        super.setValueByIds(ids);
    }

    @Override
    protected void onIdsLoaded(LoadListener listener)
    {
        this.getStore().getLoader().removeLoadListener(listener);

        this.values.clear();
        this.values.addAll(this.store.getModels());
        this.updateRawValue();

        this.store.addParameter(likeConstraint);
        this.store.addParameter(this.projectFilterGroup);
        this.store.removeParameter(idConstraint);
    }

    /**
     * @return Combo box values display style.
     */
    public native String getXTemplate() /*-{
                                                                return  [ 
                                                                '<tpl for=".">', 
                                                                '<div class="x-combo-list-item"> <span style="background-color:{color}; font-size: 13px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;<span style="font-weight: bold; font-size : 12px;">&nbsp;{name}&nbsp;&nbsp;</span> </div>', 
                                                                '</tpl>' 
                                                                ].join("");
                                                                }-*/;

}
