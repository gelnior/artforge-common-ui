package fr.hd3d.common.ui.client.widget;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.toolbar.ToolBar;


/**
 * Little widget that allows a to use a list of toggle buttons together : when one is button is toggled, other buttons
 * are untoggled.
 * 
 * @author HD3D
 */
public class ToggleButtonList
{
    /** Toggle buttons to manage. */
    protected List<ToolBarToggleButton> buttons = new ArrayList<ToolBarToggleButton>();
    /** Button currently toggled. */
    protected ToolBarToggleButton pressedButton;

    /**
     * Add all buttons contains in the list to <i>toolbar</i>.
     * 
     * @param toolbar
     *            The toolbar on which buttons must be added.
     */
    public void addListToToolBar(ToolBar toolbar)
    {
        for (ToolBarToggleButton button : buttons)
        {
            toolbar.add(button);
        }
    }

    /**
     * Add <i>button</i> to the button list. A selection listener is added to handle
     * 
     * @param button
     *            The button to add.
     */
    public void addButton(final ToolBarToggleButton button)
    {
        buttons.add(button);
        button.addSelectionListener(new SelectionListener<ButtonEvent>() {
            @Override
            public void componentSelected(ButtonEvent ce)
            {
                onButtonPressed(button, ce);
            }
        });
    }

    /**
     * Untoggle previously toggled button and toggle <i>button</i>.
     * 
     * @param button
     *            The button to toggle.
     */
    public void toggle(ToolBarToggleButton button)
    {
        if (this.pressedButton != null)
        {
            this.pressedButton.toggle();
        }
        this.pressedButton = button;
        button.toggle();
    }

    /**
     * When a button is pressed, its display is toggled.
     * 
     * @param button
     *            The button to toggle.
     * @param ce
     *            The button event (pressing).
     */
    protected void onButtonPressed(ToolBarToggleButton button, ButtonEvent ce)
    {
        if (!button.isPressed())
        {
            button.toggle();
        }
        else if (this.pressedButton == button)
        {
            button.toggle();
        }
        else
        {
            if (this.pressedButton != null)
            {
                this.pressedButton.toggle();
            }
            this.pressedButton = button;
        }
    }

}
