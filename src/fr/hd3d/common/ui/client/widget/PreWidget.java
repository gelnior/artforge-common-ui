package fr.hd3d.common.ui.client.widget;

import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.ComplexPanel;


public class PreWidget extends ComplexPanel
{
    private final boolean hasHtml;

    public PreWidget(String text, boolean hasHtml)
    {
        super();
        this.hasHtml = hasHtml;
        setElement(DOM.createElement("pre"));
        if (hasHtml)
        {
            getElement().setInnerHTML(text);
        }
        else
        {
            getElement().setInnerText(text);
        }
        setStyleName("gwt-Pre");
    }

    public void setText(String text)
    {
        if (hasHtml)
        {
            this.getElement().setInnerHTML(text);
        }
        else
        {
            this.getElement().setInnerText(text);
        }
    }
    
    public String getText() {
        if (hasHtml)
        {
            return this.getElement().getInnerHTML();
        }
        else
        {
            return this.getElement().getInnerText();
        }
    }
}
