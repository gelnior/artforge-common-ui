package fr.hd3d.common.ui.client.widget.basetree;

import com.extjs.gxt.ui.client.store.TreeStore;
import com.extjs.gxt.ui.client.store.TreeStoreEvent;
import com.extjs.gxt.ui.client.widget.treepanel.TreePanel;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;


public class BaseTreePanel<M extends RecordModelData> extends TreePanel<M>
{

    public BaseTreePanel(TreeStore<M> store)
    {
        super(store);
    }

    @Override
    protected void onFilter(TreeStoreEvent<M> se)
    {
        // clear();
        renderChildren(null);

        update();
    }
}
