package fr.hd3d.common.ui.client.widget.basetree;

import com.extjs.gxt.ui.client.event.LoadListener;
import com.extjs.gxt.ui.client.store.TreeStore;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.modeldata.reader.RecordReader;
import fr.hd3d.common.ui.client.service.proxy.TreeServicesProxy;


/**
 * Base tree data model handle tree store, loader and proxy for the base tree.
 * 
 * @author HD3D
 * 
 * @param <L>
 *            The leaves class.
 * @param <P>
 *            The parent class.
 */
public class BaseTreeDataModel<L extends RecordModelData, P extends RecordModelData>
{
    /** Loader that load data from proxy to store. */
    protected BaseTreeDataLoader<L, P> loader;
    /** The store containing data */
    protected BaseTreeStore<RecordModelData> store;
    /** Name displayed as root folder on the tree. */
    protected String rootName;

    /**
     * Default constructor.
     * 
     * @param leafReader
     *            The JSON reader that translates leave data from JSON to GXT bean.
     * @param parentReader
     *            The JSON reader that translates parent data from JSON to GXT bean.
     */
    public BaseTreeDataModel(IReader<L> leafReader, IReader<P> parentReader, L leafInstance, P parentInstance)
    {
        TreeServicesProxy<RecordModelData> proxy = new TreeServicesProxy<RecordModelData>(new RecordReader());
        loader = new BaseTreeDataLoader<L, P>(proxy, leafReader, parentReader, leafInstance, parentInstance);
        store = new BaseTreeStore<RecordModelData>(loader);
    }

    /**
     * A controller that set directly the loader given in parameter as loader and build the store from it.
     * 
     * @param loader
     *            The loader that is charged to retrieve data.
     */
    public BaseTreeDataModel(BaseTreeDataLoader<L, P> loader)
    {
        this.loader = loader;
        store = new BaseTreeStore<RecordModelData>(loader);
    }

    /**
     * @return The tree data loader.
     */
    public BaseTreeDataLoader<L, P> getLoader()
    {
        return loader;
    }

    /**
     * Set the tree data loader.
     * 
     * @param loader
     *            The tree data loader.
     */
    public void setLoader(BaseTreeDataLoader<L, P> loader)
    {
        this.loader = loader;
        this.store = new BaseTreeStore<RecordModelData>(loader);
    }

    /**
     * @return The tree data store.
     */
    public TreeStore<RecordModelData> getStore()
    {
        return this.store;
    }

    /**
     * Set the tree root name that is displayed for the user.
     * 
     * @param rootName
     *            The tree root name that is displayed for the user.
     */
    public void setRootName(String rootName)
    {
        this.rootName = rootName;
    }

    /**
     * Update proxy path with project id given in parameter then reload data from an empty tree.
     * 
     * @param project
     *            The current project. Project can be null for displaying everything without filtering on project.
     */
    public void setCurrentProject(ProjectModelData project)
    {
        this.store.removeAll();
        if (project != null)
            this.loader.setCurrentProject(project.getId());

        P record = loader.getParentInstance();
        record.setName(rootName);
        record.setId(-1L);
        this.store.add(record, false);

        this.loader.loadChildren(record);
    }

    /**
     * Add a load listener to the tree store.
     * 
     * @param loadListener
     */
    public void addLoadListener(LoadListener loadListener)
    {
        this.loader.addLoadListener(loadListener);
    }

    /**
     * Remove <i>loadListener</i> listener from the tree store load listener list.
     * 
     * @param loadListener
     *            The listener to remove.
     */
    public void removeLoadListener(LoadListener loadListener)
    {
        this.loader.removeLoadListener(loadListener);
    }
}
