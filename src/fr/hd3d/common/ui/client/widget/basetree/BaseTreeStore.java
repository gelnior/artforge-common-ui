package fr.hd3d.common.ui.client.widget.basetree;

import com.extjs.gxt.ui.client.data.TreeLoader;
import com.extjs.gxt.ui.client.store.TreeStore;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;


public class BaseTreeStore<M extends RecordModelData> extends TreeStore<M>
{
    /**
     * Creates a new tree store.
     * 
     * @param loader
     *            the tree loader
     */
    public BaseTreeStore(TreeLoader<M> loader)
    {
        super(loader);
    }
}
