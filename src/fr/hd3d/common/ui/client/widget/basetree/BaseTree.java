package fr.hd3d.common.ui.client.widget.basetree;

import java.util.List;

import com.extjs.gxt.ui.client.Style.SelectionMode;
import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.data.SortInfo;
import com.extjs.gxt.ui.client.data.TreeLoadEvent;
import com.extjs.gxt.ui.client.event.LoadListener;
import com.extjs.gxt.ui.client.event.MenuEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.store.TreeStore;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.menu.MenuItem;
import com.extjs.gxt.ui.client.widget.treepanel.TreePanel;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.widget.BaseView;
import fr.hd3d.common.ui.client.widget.EasyMenu;


/**
 * Basic tree showing Hd3d objects as nodes and leaves.
 * 
 * @author HD3D
 * 
 * @param <L>
 *            The leaves class.
 * @param <P>
 *            The node class.
 */
public abstract class BaseTree<L extends RecordModelData, P extends RecordModelData> extends BaseView
{
    /** Constant strings to display : dialog messages, button label... */
    public static final CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    /** Model handling tree data. */
    protected BaseTreeDataModel<L, P> model;
    /** Tree panel materializing the tree. */
    protected BaseTreePanel<RecordModelData> tree;

    /**
     * Default constructor.
     * 
     * @param rootName
     *            Name that will be displayed for the root node.
     * @param leafReader
     *            The JSON reader needed to read leaves data.
     * @param parentReader
     *            The JSON reader needed to read the node data.
     */
    public BaseTree(String rootName, IReader<L> leafReader, IReader<P> parentReader)
    {
        this.setModel(rootName, leafReader, parentReader);
        this.setTreeWidget();
        this.setContextMenu();
    }

    /**
     * @return Tree wrapped in this panel.
     */
    public TreePanel<RecordModelData> getTree()
    {
        return tree;
    }

    /**
     * @return Store used by tree to display data.
     */
    public TreeStore<RecordModelData> getStore()
    {
        return tree.getStore();
    }

    /**
     * @return Model of the MVC pattern used for this widget.
     */
    public BaseTreeDataModel<L, P> getTreeModel()
    {
        return this.model;
    }

    /**
     * Set the model stuff (proxy, loader and reader).
     * 
     * @param rootName
     *            The name displayed for the tree root.
     * @param leafReader
     *            JSON reader needed to translates leaves data.
     * @param parentReader
     *            JSON reader needed to translates parents data.
     */
    protected abstract void setModel(String rootName, IReader<L> leafReader, IReader<P> parentReader);

    /**
     * This set the tree style and add a listener that expand root node when root data are loaded.
     */
    protected void setTreeWidget()
    {
        this.tree = new BaseTreePanel<RecordModelData>(model.getStore());

        SortInfo sortInfo = new SortInfo(RecordModelData.NAME_FIELD, SortDir.ASC);
        this.tree.getStore().setSortInfo(sortInfo);

        this.setStyles();
        this.setTree();
        this.setListeners();
    }

    /**
     * Set selected project to the tree data proxy. It makes it looking for data corresponding to the project id.
     * 
     * @param project
     *            The project from which the tree retrieve its informations.
     */
    public void setCurrentProject(ProjectModelData project)
    {
        if (project != null)
        {
            this.model.setCurrentProject(project);
        }
        else
        {
            this.model.setCurrentProject(null);
        }
    }

    /**
     * Set styles : fit layout and no headers.
     */
    protected void setStyles()
    {
        this.setLayout(new FitLayout());
        this.setHeaderVisible(false);
    }

    /**
     * Set the tree, default display field is "name".
     */
    protected void setTree()
    {
        this.tree.setStateful(true);
        this.tree.setDisplayProperty(RecordModelData.NAME_FIELD);
        this.add(tree);
    }

    /**
     * Set the load listener that force root to expand when root data are loaded.
     */
    protected void setListeners()
    {
        this.model.addLoadListener(new LoadListener() {
            @Override
            public void loaderLoad(LoadEvent le)
            {
                TreeLoadEvent event = (TreeLoadEvent) le;
                RecordModelData parent = (RecordModelData) event.parent;

                expand(parent);
            }
        });
    }

    /**
     * Expand node corresponding to record.
     * 
     * @param record
     *            The record to expand.
     */
    public void expand(RecordModelData record)
    {
        this.tree.setExpanded(record, true);
    }

    /**
     * @return Selected nodes and selected leaves.
     */
    public List<RecordModelData> getSelection()
    {
        return tree.getSelectionModel().getSelection();
    }

    /**
     * @return The data loader used by tree to populate its store.
     */
    public BaseTreeDataLoader<L, P> getLoader()
    {
        return this.model.getLoader();
    }

    /**
     * Reload data for first selected element. If this element is a parent it refreshe its children. If it is a leaf, it
     * refreshes the parent children.
     */
    public void refreshSelection()
    {
        List<RecordModelData> selection = this.getSelection();

        if (CollectionUtils.isNotEmpty(selection))
        {
            if (this.model.getLoader().hasChildren(selection.get(0)))
            {
                this.model.getLoader().loadChildren(selection.get(0));
            }
            else
            {
                RecordModelData parent = this.model.getStore().getParent(selection.get(0));
                if (parent != null)
                {
                    this.model.getLoader().loadChildren(parent);
                }
            }
        }
    }

    /**
     * Deselect all selected elements.
     */
    public void clearSelection()
    {
        this.getTree().getSelectionModel().deselectAll();
    }

    /**
     * @return First selected record.
     */
    protected RecordModelData getFirstSelected()
    {
        List<RecordModelData> records = this.getSelection();
        if (CollectionUtils.isNotEmpty(records))
        {
            return records.get(0);
        }
        else
        {
            return null;
        }
    }

    /**
     * @param selectionMode
     *            Set a new selection mode for the wrapped by this panel.
     */
    public void setSelectionMode(SelectionMode selectionMode)
    {
        this.getTree().getSelectionModel().setSelectionMode(selectionMode);
    }

    /**
     * Set a context menu with an item allowing to refresh nodes.
     */
    protected void setContextMenu()
    {
        EasyMenu menu = new EasyMenu();

        MenuItem item = new MenuItem();
        item.setText(CONSTANTS.Refresh());
        item.setIcon(Hd3dImages.getRefreshIcon());
        item.addSelectionListener(new SelectionListener<MenuEvent>() {
            @Override
            public void componentSelected(MenuEvent ce)
            {
                refreshSelection();
            }
        });
        menu.add(item);

        this.setContextMenu(menu);
    }

}
