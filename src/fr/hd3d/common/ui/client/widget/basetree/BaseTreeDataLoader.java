package fr.hd3d.common.ui.client.widget.basetree;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.BaseLoader;
import com.extjs.gxt.ui.client.data.TreeLoadEvent;
import com.extjs.gxt.ui.client.data.TreeLoader;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.OrderBy;
import fr.hd3d.common.ui.client.service.proxy.TreeServicesProxy;


/**
 * A tree that loads HD3D hierarchical data. Nodes (folders) are represented by P class, and leaves are represented by L
 * class. P class must have a "/children" service.
 * 
 * @author HD3D
 * 
 * @param <L>
 *            Node class
 * @param <P>
 *            Leave class
 */
public class BaseTreeDataLoader<L extends RecordModelData, P extends RecordModelData> extends
        BaseLoader<List<RecordModelData>> implements TreeLoader<RecordModelData>
{
    /** Data proxy needed by the loader. */
    protected final TreeServicesProxy<RecordModelData> servicesProxy;
    /** Weird list needed by GXT. */
    protected final List<RecordModelData> children = new ArrayList<RecordModelData>();

    /** The ID of the project currently selected. */
    protected Long currentProjectId;
    /** Temporary list buffer to needed to keep loaded while same level leaves are loaded. */
    protected final List<RecordModelData> temporaryResult = new ArrayList<RecordModelData>();
    /** True if loader waits for leaves data. */
    protected boolean isLoadingLeaves = false;

    /** Data reader needed to translate JSON for leaves. */
    protected IReader<L> leafReader;
    /** Data reader needed to translate JSON for nodes. */
    protected IReader<P> parentReader;

    protected L leafInstance;
    protected P parentInstance;

    /**
     * Default constructor.
     * 
     * @param proxy
     *            Data proxy needed to retrieve data from server.
     * @param leafReader
     *            The reader to read leaves data at JSON format.
     * @param parentReader
     *            The reader to read nodes data at JSON format.
     * @param leafInstance
     *            A neutral instance of leaf to get leaf entity informations such as class name.
     * @param parentInstance
     *            A neutral instance of parent to get parent entity informations such as class name.
     */
    public BaseTreeDataLoader(TreeServicesProxy<RecordModelData> proxy, IReader<L> leafReader, IReader<P> parentReader,
            L leafInstance, P parentInstance)
    {
        super(proxy);
        this.servicesProxy = proxy;

        this.leafReader = leafReader;
        this.leafInstance = leafInstance;
        this.parentReader = parentReader;
        this.parentInstance = parentInstance;
    }

    /**
     * Set the ID of the currently selected project.
     * 
     * @param id
     *            ID of the currently selected project.
     */
    public void setCurrentProject(Long id)
    {
        this.currentProjectId = id;
    }

    /**
     * True if <i>parent</i> class is the node class.
     * 
     * @see com.extjs.gxt.ui.client.data.TreeLoader#hasChildren(com.extjs.gxt.ui.client.data.ModelData)
     */
    public boolean hasChildren(RecordModelData parent)
    {
        P instance = getParentInstance();
        return instance.getClassName().equals(parent.getClassName());
    }

    @Override
    public boolean load()
    {
        return true;
    }

    /**
     * @return A new instance of the parent class.
     */
    public P getParentInstance()
    {
        return parentInstance;
    }

    /**
     * @return A new instance of the leaf class.
     */
    public L getLeafInstance()
    {
        return leafInstance;
    }

    /**
     * When load failed, retrieved data are removed from the tree and loading process is stopped.
     */
    @Override
    protected void onLoadFailure(Object loadConfig, Throwable t)
    {
        TreeLoadEvent evt = new TreeLoadEvent(this, loadConfig, t);
        if (loadConfig != null && children.contains(loadConfig))
        {
            evt.parent = (RecordModelData) loadConfig;
            children.remove(loadConfig);
        }
        this.isLoadingLeaves = false;
        fireEvent(LoadException, evt);
    }

    @SuppressWarnings("unchecked")
    protected void initReader()
    {
        servicesProxy.setReader((IReader<RecordModelData>) parentReader);
    }

    /**
     * Load children depending on parent : if the parent is null it loads all root data to the root (for the current
     * project). If parent id is -1 it loads root data as children of this parent. If parent class is the same as P
     * class, it loads children to the parent node. If parent class is the same as L class, it does nothing.
     * 
     * @see com.extjs.gxt.ui.client.data.TreeLoader#loadChildren(com.extjs.gxt.ui.client.data.ModelData)
     */
    @SuppressWarnings("unchecked")
    public boolean loadChildren(RecordModelData parent)
    {
        P instance = getParentInstance();
        servicesProxy.clearParameters();
        servicesProxy.addParameter(new OrderBy(RecordModelData.NAME_FIELD));
        servicesProxy.setReader((IReader<RecordModelData>) parentReader);

        if (parent == null)
        {
            return this.loadNullRoot(instance.getSimpleClassName());
        }
        else if (parent.getId().longValue() == -1L)
        {
            return this.loadRoot(parent, instance.getSimpleClassName());
        }
        else if (instance.getClassName().equals(parent.getClassName()))
        {
            return this.loadParent(parent);
        }
        else
        {
            return false;
        }
    }

    /**
     * Load children for a tree node (parent). If parent has no ID, it considers that it has no children, and add
     * nothing to the tree but considers that loading succeeds.
     * 
     * @param parent
     *            The node of which children to load.
     * @return true if loading succeeds.
     */
    protected boolean loadParent(RecordModelData parent)
    {
        if (parent.getId() != null)
        {
            String path = parent.getPath() + ServicesPath.CHILDREN;
            servicesProxy.setPath(path);

            this.children.add(parent);
        }
        else
        {
            this.onLoadSuccess(null, new ArrayList<RecordModelData>());

        }
        return load(parent);
    }

    /**
     * Load tree nodes which has no parent (root nodes). If a project is set it retrieves only nodes of this project.<br>
     * It loads only parents, not leaves.
     * 
     * @param simpleClassName
     *            Class name of data to retrieve.
     * @return True if succeeds.
     */
    protected boolean loadNullRoot(String simpleClassName)
    {
        String path = "";
        if (currentProjectId != null)
        {
            path = ServicesPath.getPath(ProjectModelData.SIMPLE_CLASS_NAME);
            path += currentProjectId + "/";
        }
        path += ServicesPath.getPath(simpleClassName);
        servicesProxy.setPath(path);

        return load(null);
    }

    /**
     * Load tree nodes which has for parent the root node. If a project is set it retrieves only nodes of this project..<br>
     * It loads only parents, not leaves.
     * 
     * @param parent
     *            The parent root.
     * @param simpleClassName
     *            Class name of data to retrieve.
     * @return True if succeeds.
     */
    protected boolean loadRoot(RecordModelData parent, String simpleClassName)
    {
        String path = "";
        if (currentProjectId != null)
        {
            path = ServicesPath.getPath(ProjectModelData.SIMPLE_CLASS_NAME);
            path += currentProjectId + "/";
        }
        path += ServicesPath.getPath(simpleClassName);
        servicesProxy.setPath(path);

        this.children.add(parent);

        return load(parent);
    }

    /**
     * If load succeeds data are transmitted to tree via tree load event to be adding under parent node. If a parent
     * data are loaded, leaves data are loaded before firing tree load event.
     * 
     * @see com.extjs.gxt.ui.client.data.BaseLoader#onLoadSuccess(java.lang.Object, java.lang.Object)
     */
    @Override
    protected void onLoadSuccess(Object loadConfig, List<RecordModelData> result)
    {
        P parentInstance = getParentInstance();

        if (loadConfig != null && children.contains(loadConfig))
        {
            RecordModelData parent = (RecordModelData) loadConfig;

            if (parent.getId() == -1)
            {
                TreeLoadEvent evt = new TreeLoadEvent(this, loadConfig, result);
                evt.parent = parent;
                fireEvent(Load, evt);
                children.remove(loadConfig);
            }
            else if (parentInstance.getClassName().equals(parent.getClassName()))
            {
                this.onNonRootLoadSuccess(parent, result);
            }
        }
        else
        {
            this.onLoadSuccessWithNullParent(loadConfig, result);
        }
    }

    /**
     * When a parent which is not root has been loaded it loads its leaves.
     * 
     * @param parent
     *            The parent node.
     * @param result
     *            List of data already loaded.
     */
    protected void onNonRootLoadSuccess(RecordModelData parent, List<RecordModelData> result)
    {
        if (isLoadingLeaves)
        {
            this.onLeavesLoadedSuccess(parent, result);
        }
        else
        {
            this.isLoadingLeaves = true;
            this.temporaryResult.addAll(result);
            this.loadLeaves(parent);
        }
    }

    /**
     * When leaves are successfully loaded, parent nodes and children leaves are transmitted to the tree.
     * 
     * @param parent
     *            Parent causing loading.
     * @param result
     *            Result list to send to tree.
     */
    protected void onLeavesLoadedSuccess(RecordModelData parent, List<RecordModelData> result)
    {
        List<RecordModelData> results = new ArrayList<RecordModelData>();
        results.addAll(temporaryResult);
        results.addAll(result);

        TreeLoadEvent evt = new TreeLoadEvent(this, parent, results);
        this.isLoadingLeaves = false;
        this.children.remove(parent);

        evt.parent = parent;
        fireEvent(Load, evt);
        this.temporaryResult.clear();
        this.servicesProxy.clearParameters();
        this.servicesProxy.addParameter(new OrderBy(RecordModelData.NAME_FIELD));
    }

    /**
     * Fires rightly configured event when data are loaded from a null parent (when tree is empty).
     * 
     * @param loadConfig
     *            Loading configuration.
     * @param result
     *            Result as a list of HD3D objects.
     */
    protected void onLoadSuccessWithNullParent(Object loadConfig, List<RecordModelData> result)
    {
        if (result != null)
        {
            TreeLoadEvent evt = new TreeLoadEvent(this, loadConfig, result);
            evt.parent = null;
            fireEvent(Load, evt);
            children.remove(loadConfig);
        }
    }

    /**
     * Set path for leaves loading base on parent path. <br>
     * <code>children_path = parent_path + children_class_path</code>
     * 
     * @param parent
     */
    @SuppressWarnings("unchecked")
    protected void loadLeaves(RecordModelData parent)
    {
    	if(leafReader != null) 
    	{
	        L instance = getLeafInstance();
	
	        String path = "";
	        if (currentProjectId != null)
	        {
	            path = ServicesPath.getPath(ProjectModelData.SIMPLE_CLASS_NAME);
	            path += currentProjectId + "/";
	        }
	        path += ServicesPath.getPath(parent.getSimpleClassName()) + parent.getId();
	
	        path += "/" + ServicesPath.getPath(instance.getSimpleClassName());
	        servicesProxy.setPath(path);
	        servicesProxy.setReader((IReader<RecordModelData>) leafReader);
	
	        load(parent);
        } 
    	else 
        {
        	this.onLeavesLoadedSuccess(parent, new ArrayList<RecordModelData>());
        }
    }

    @SuppressWarnings("unchecked")
    protected void setLeafReader()
    {
        servicesProxy.setReader((IReader<RecordModelData>) leafReader);
    }

}
