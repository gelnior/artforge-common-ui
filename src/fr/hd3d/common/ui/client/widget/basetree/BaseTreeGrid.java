package fr.hd3d.common.ui.client.widget.basetree;

import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.data.TreeLoadEvent;
import com.extjs.gxt.ui.client.event.LoadListener;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.treegrid.TreeGrid;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;


public abstract class BaseTreeGrid<L extends RecordModelData, P extends RecordModelData> extends ContentPanel
{
    protected BaseTreeDataModel<L, P> model;
    protected TreeGrid<RecordModelData> tree;

    public BaseTreeGrid(String rootName, IReader<L> leafReader, IReader<P> parentReader, ColumnModel cm)
    {
        this.setModel(rootName, leafReader, parentReader);
        this.setTreeWidget(cm);
    }

    protected abstract void setModel(String rootName, IReader<L> leafReader, IReader<P> parentReader);

    protected void setTreeWidget(ColumnModel cm)
    {
        this.tree = new TreeGrid<RecordModelData>(model.getStore(), cm);
        this.setStyles();
        this.setTree();
        this.setListeners();
    }

    public void setCurrentProject(ProjectModelData project)
    {
        this.model.setCurrentProject(project);
    }

    protected void setStyles()
    {
        this.setLayout(new FitLayout());
        this.setHeaderVisible(false);
    }

    protected void setTree()
    {
        this.tree.setStateful(true);
        // this.tree.setDisplayProperty(RecordModelData.NAME_FIELD);
        this.add(tree);
    }

    protected void setListeners()
    {
        this.model.addLoadListener(new LoadListener() {
            @Override
            public void loaderLoad(LoadEvent le)
            {
                TreeLoadEvent event = (TreeLoadEvent) le;
                RecordModelData parent = (RecordModelData) event.parent;
                if (parent.getId() == -1)
                {
                    tree.setExpanded(parent, true);
                }
            }
        });
    }
}
