package fr.hd3d.common.ui.client.widget;

import java.util.List;

import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.event.LoadListener;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.reader.TaskTypeReader;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.AndConstraint;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.EqConstraint;
import fr.hd3d.common.ui.client.service.parameter.IsNullConstraint;
import fr.hd3d.common.ui.client.service.parameter.LogicConstraint;
import fr.hd3d.common.ui.client.service.parameter.OrConstraint;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;


/**
 * Auto complete combobox used to select a task type from server.
 * 
 * @author HD3D
 */
public class AutoTaskTypeComboBox extends AutoCompleteModelCombo<TaskTypeModelData>
{
    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    /** Constraint to retrieve any non specific task types or specific project task type. */
    protected OrConstraint projectFilterGroup;
    /** Root constraint used to cumulate constraints. */
    protected AndConstraint rootConstraint = new AndConstraint();

    /**
     * Default constructor.
     */
    public AutoTaskTypeComboBox()
    {
        this(false);
    }

    public AutoTaskTypeComboBox(boolean emptyField)
    {
        super(new TaskTypeReader(), emptyField);

        this.setStyles();
        this.store.setPath(ServicesPath.getPath(TaskTypeModelData.SIMPLE_CLASS_NAME));
        this.setContextMenu();
        this.setTypeAhead(false);

        this.rootConstraint.setLeftMember(likeConstraint);
        this.rootConstraint.setRightMember(getProjectFilter());
        this.store.removeParameter(likeConstraint);
        this.store.addParameter(rootConstraint);
    }

    @Override
    protected void onBeforeLoad(LoadEvent le)
    {
        if (MainModel.getCurrentProject() != null)
            this.setProject(MainModel.getCurrentProject().getId());
        else
            this.setProject(null);
    }

    public AutoTaskTypeComboBox(boolean emptyField, String emptyFieldName)
    {
        super(new TaskTypeReader(), emptyField, emptyFieldName);

        this.setStyles();
        this.store.setPath(ServicesPath.getPath(TaskTypeModelData.SIMPLE_CLASS_NAME));
        this.store.addParameter(this.getProjectFilter());
        this.setContextMenu();
        this.setTypeAhead(false);
    }

    public void setProject(Long projectId)
    {
        LogicConstraint fg = this.getProjectFilter();
        if (projectId != null)
        {
            if (fg.getRightMember() == null)
                fg.setRightMember(new EqConstraint("project.id", projectId));
            else
                ((EqConstraint) fg.getRightMember()).setLeftMember(projectId);
        }
        else
        {
            fg.setRightMember(null);
        }
    }

    private LogicConstraint getProjectFilter()
    {
        if (projectFilterGroup == null)
        {
            projectFilterGroup = new OrConstraint();
            projectFilterGroup.setLeftMember(new IsNullConstraint("project.id"));
        }

        return this.projectFilterGroup;
    }

    @Override
    protected String getLike()
    {
        return "%" + getRawValue() + "%";
    }

    /**
     * Initialize a value with the model data of ID equal to <i>id</i> by loading the model data from services.
     * 
     * @param id
     *            The id of the model data to set as value.
     */
    @Override
    public void setValueById(final Long id)
    {
        this.idConstraint.setLeftMember(id);
        this.store.removeParameter(rootConstraint);
        this.store.removeParameter(pagination);
        this.store.addParameter(idConstraint);

        super.setValueById(id);
    }

    protected AndConstraint banAndConstraint = new AndConstraint();

    public void setBanTaskTypes(List<Long> taskTypeIds)
    {
        if (CollectionUtils.isNotEmpty(taskTypeIds))
        {
            this.banAndConstraint.setLeftMember(rootConstraint);
            this.banAndConstraint.setRightMember(new Constraint(EConstraintOperator.notin, TaskTypeModelData.ID_FIELD,
                    taskTypeIds));
            this.store.removeParameter(rootConstraint);
            this.store.addParameter(banAndConstraint);
        }
        else
        {
            this.store.removeParameter(banAndConstraint);
            this.store.addParameter(rootConstraint);
        }
    }

    /**
     * After value initialization, when model data is loaded, constraint used to filter on id is removed and name
     * constraint is restored.
     */
    @Override
    protected void onIdLoad(LoadListener listener, Long id)
    {
        super.onIdLoad(listener, id);
        this.store.addParameter(rootConstraint);
        this.store.addParameter(pagination);
        this.store.removeParameter(idConstraint);
    }

    /**
     * Set combo styles.
     */
    @Override
    protected void setStyles()
    {
        super.setStyles();
        this.setEmptyText("Select Task Type...");
        this.setDisplayField(RecordModelData.NAME_FIELD);
        this.setTemplate(getXTemplate());
    }

    /**
     * @return Combo box values display style.
     */
    public native String getXTemplate() /*-{
                                                                      return  [ 
                                                                      '<tpl for=".">', 
                                                                      '<div class="x-combo-list-item"> <span style="background-color:{color}; font-size: 13px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;<span style="font-weight: bold; font-size : 12px;">&nbsp;{name}&nbsp;&nbsp;</span> </div>', 
                                                                      '</tpl>' 
                                                                      ].join("");
                                                                      }-*/;

}
