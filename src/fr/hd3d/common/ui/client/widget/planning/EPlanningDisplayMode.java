package fr.hd3d.common.ui.client.widget.planning;

/**
 * Available modes available for data displayed inside planning widget.
 * 
 * @author HD3D
 */
public enum EPlanningDisplayMode
{
    TASK_TYPE, TASK_TYPE_SHOT, CONSTITUENT, SHOT, ALL;
}
