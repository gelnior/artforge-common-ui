package fr.hd3d.common.ui.client.widget.planning.user.model;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.extjs.gxt.ui.client.util.DateWrapper;
import com.extjs.gxt.ui.client.widget.LayoutContainer;

import fr.hd3d.common.ui.client.modeldata.DurationModelData;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.widget.planning.util.Hd3dDateUtil;
import fr.hd3d.common.ui.client.widget.planning.widget.bar.PlanningBar;


public class PlanningWidgetModel
{

    public static int NEW_ID = -2;
    public static Long RESET_VALUE = Long.valueOf(-1);
    private final Map<Hd3dModelData, Map<String, List<PlanningBar>>> unSavedTaskMap = new HashMap<Hd3dModelData, Map<String, List<PlanningBar>>>();
    private final Map<Long, LayoutContainer> objectReperes = new HashMap<Long, LayoutContainer>();

    public int getTaskPanelLine(Hd3dModelData hd3dModelData, DurationModelData baseTaskModelData)
    {
        int nbLigne = 0;
        Map<String, List<PlanningBar>> map = this.unSavedTaskMap.get(hd3dModelData);
        if (map != null)
        {
            DateWrapper startDate = new DateWrapper(baseTaskModelData.getStartDate()).clearTime();
            DateWrapper endDate = new DateWrapper(baseTaskModelData.getEndDate()).clearTime().addDays(1);
            while (startDate.before(endDate))
            {
                String dateString = Hd3dDateUtil.dateToString(startDate.asDate());
                List<PlanningBar> list = map.get(dateString);
                if (list != null)
                {
                    int index = 0;
                    for (Iterator<PlanningBar> iterator2 = list.iterator(); iterator2.hasNext();)
                    {
                        PlanningBar taskPanel = iterator2.next();
                    }
                    if (index > nbLigne)
                    {
                        nbLigne = index;
                    }
                }
                startDate = startDate.addDays(1);
            }
        }
        return nbLigne;
    }

    public Map<Long, LayoutContainer> getObjectReperes()
    {
        return objectReperes;
    }

    public Map<Hd3dModelData, Map<String, List<PlanningBar>>> getUnSavedTaskMap()
    {
        return unSavedTaskMap;
    }
}
