package fr.hd3d.common.ui.client.widget.planning;

/**
 * Enumeration to define which zoom mode is currently active.
 * 
 * @author HD3D
 */
public enum EZoomMode
{
    BIG, NORMAL, SMALL
}
