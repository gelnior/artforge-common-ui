package fr.hd3d.common.ui.client.widget.planning.widget.treeitem;

import com.google.gwt.event.dom.client.HasMouseOutHandlers;
import com.google.gwt.event.dom.client.HasMouseOverHandlers;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.FlexTable;


public class TaskGroupItemContent extends FlexTable implements HasMouseOutHandlers, HasMouseOverHandlers
{

    public HandlerRegistration addMouseOutHandler(MouseOutHandler handler)
    {
        return addDomHandler(handler, MouseOutEvent.getType());
    }

    public HandlerRegistration addMouseOverHandler(MouseOverHandler handler)
    {
        return addDomHandler(handler, MouseOverEvent.getType());
    }
}
