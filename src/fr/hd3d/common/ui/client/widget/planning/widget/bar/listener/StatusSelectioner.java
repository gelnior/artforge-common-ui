package fr.hd3d.common.ui.client.widget.planning.widget.bar.listener;

import com.extjs.gxt.ui.client.event.MenuEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;

import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.widget.planning.ETaskDisplayMode;
import fr.hd3d.common.ui.client.widget.planning.events.PlanningCommonEvents;
import fr.hd3d.common.ui.client.widget.planning.widget.bar.TaskBar;


public class StatusSelectioner extends SelectionListener<MenuEvent>
{
    private final TaskBar taskBar;
    private final ETaskStatus status;

    public StatusSelectioner(TaskBar taskBar2, ETaskStatus status)
    {
        this.taskBar = taskBar2;
        this.status = status;
    }

    @Override
    public void componentSelected(MenuEvent ce)
    {
        TaskModelData task = this.taskBar.getTask();
        task.setStatus(status.toString());
        this.taskBar.setBackgroundColor(ETaskDisplayMode.STATUS);
        this.taskBar.refreshToolTip();

        EventDispatcher.forwardEvent(PlanningCommonEvents.TASK_MOVED, task);
    }
}
