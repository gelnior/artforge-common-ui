package fr.hd3d.common.ui.client.widget.planning.widget.treeitem;

import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.hd3d.common.ui.client.widget.planning.user.widget.SpanWidget;


/**
 * Label to display inside planning tree.
 * 
 * @author HD3D
 */
public class TreeLabelWidget extends ComplexPanel
{
    public TreeLabelWidget()
    {
        setElement(DOM.createDiv());
    }

    @Override
    public void add(Widget w)
    {
        super.add(w, getElement());
    }

    public void addText(String string)
    {
        SpanWidget spanWidget = new SpanWidget();
        spanWidget.setText(string);
        add(spanWidget);
    }

    public String getText()
    {
        return DOM.getInnerHTML(getElement());
    }
}
