package fr.hd3d.common.ui.client.widget.planning.widget.treeitem;

import com.extjs.gxt.ui.client.widget.menu.Menu;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Image;

import fr.hd3d.common.ui.client.modeldata.task.ExtraLineModelData;
import fr.hd3d.common.ui.client.widget.planning.user.view.BasePlanningWidget;
import fr.hd3d.common.ui.client.widget.planning.user.widget.SpanWidget;


public class ExtraLineItem extends PlanningTreeItem
{
    private final ExtraLineModelData extraLine;

    private FlexTable horizontalPanel;
    private final SpanWidget durationLabel = new SpanWidget();
    private final SpanWidget label = new SpanWidget();
    private Image icon;

    private static Menu contextMenu = null;

    public ExtraLineItem(ExtraLineModelData extraLine)
    {
        super();

        this.extraLine = extraLine;

        this.add(createWidget());
        this.setStyleName(TaskGroupItem.TREE_ITEM_TEXT);
        this.setHeight(BasePlanningWidget.CELL_HEIGHT + "px");
        DOM.setStyleAttribute(this.getElement(), "borderTop", "2px solid #AAA");
        DOM.setStyleAttribute(this.getElement(), "marginLeft", "4px");
        DOM.setStyleAttribute(durationLabel.getElement(), "fontSize", "8px");
        DOM.setStyleAttribute(durationLabel.getElement(), "maginTop", "-15px");
        DOM.setStyleAttribute(durationLabel.getElement(), "lineHeight", "8px");
    }

    public ExtraLineModelData getExtraLine()
    {
        return extraLine;
    }

    @Override
    public void setLabel(String label)
    {
        this.label.setText("<i>" + label + "</i>");
    }

    public void setIcon(String iconPath)
    {
        this.icon.setUrl(iconPath);
    }

    @Override
    public void setSelected(boolean selected)
    {
        if (selected)
        {
            horizontalPanel.addStyleName("x-ftree2-selected");
        }
        else
        {
            horizontalPanel.removeStyleName("x-ftree2-selected");
        }
        enableIcons();
    }

    public void setDurationLabel(Long elapsed, Long duration)
    {
        String durationString = "---";
        float nbDay = duration / 28800;
        int day = (int) Math.floor(nbDay);

        if (day > 0)
        {
            durationString = day + "";
        }

        String elapsedString = "---";
        nbDay = elapsed / 28800;
        day = (int) Math.floor(nbDay);

        if (day > 0)
        {
            elapsedString = day + "";
        }

        this.durationLabel.setText(elapsedString + "<br/>" + durationString);
    }

    private FlexTable createWidget()
    {
        horizontalPanel = new FlexTable();
        horizontalPanel.setCellPadding(0);
        horizontalPanel.setCellSpacing(0);

        icon = new Image();
        icon.addStyleName("extra-line-item-icon");

        String userName = this.extraLine.getName();

        durationLabel.setText("");
        durationLabel.setWidth("10px");
        horizontalPanel.setWidget(0, 0, durationLabel);

        if (extraLine.getPersonID() != null)
        {
            label.setText(userName);
            horizontalPanel.setWidget(0, 1, icon);
            horizontalPanel.setWidget(0, 2, label);
        }
        horizontalPanel.setHeight(BasePlanningWidget.CELL_HEIGHT + "px");

        return horizontalPanel;
    }

    private void enableIcons()
    {

    }

    @Override
    public Menu getContextMenu()
    {
        if (contextMenu == null)
        {
            createContextMenu();
        }
        enableIcons();
        return contextMenu;
    }

    private static void createContextMenu()
    {
        contextMenu = new Menu();
    }

}
