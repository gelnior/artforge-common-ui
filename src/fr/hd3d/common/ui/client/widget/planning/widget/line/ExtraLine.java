package fr.hd3d.common.ui.client.widget.planning.widget.line;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.util.DateWrapper;
import com.extjs.gxt.ui.client.widget.Component;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Event;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.task.ExtraLineModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskGroupModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.widget.planning.EPlanningDisplayMode;
import fr.hd3d.common.ui.client.widget.planning.ETaskDisplayMode;
import fr.hd3d.common.ui.client.widget.planning.events.PlanningCommonEvents;
import fr.hd3d.common.ui.client.widget.planning.user.view.BasePlanningWidget;
import fr.hd3d.common.ui.client.widget.planning.user.widget.TimeLine;
import fr.hd3d.common.ui.client.widget.planning.util.BarUtils;
import fr.hd3d.common.ui.client.widget.planning.widget.bar.BaseTaskBar;
import fr.hd3d.common.ui.client.widget.planning.widget.bar.TaskBar;
import fr.hd3d.common.ui.client.widget.planning.widget.treeitem.ExtraLineItem;


/**
 * Extra line is the displayed object that represents a planning line.
 * 
 * @author HD3D
 */
public class ExtraLine extends LayoutContainer
{
    public static final String EXTRA_LINE_MOUSE_OVER_EVENT = "extraLineMouseOverEvent";

    /** Link with the planning time line (needed to know the bounds of the extra line. */
    protected TimeLine timeLine;
    /** Corresponding extra line item inside the planning tree. */
    protected ExtraLineItem extraLineItem;

    /** Task group of which the line belongs. */
    protected TaskGroupModelData taskGroup;

    // protected FastMap<TaskBar> taskBarCoordinates = new FastMap<TaskBar>();
    // protected HashMap<TaskBar, String> invertTaskBarCoordinates = new HashMap<TaskBar, String>();

    /** Task bar displayed in extra line. */
    protected FastMap<TaskBar> taskBars = new FastMap<TaskBar>();

    protected FastMap<Boolean> occupation = new FastMap<Boolean>();

    /** Number of line displayed for this extra line. */
    private int lineHeight = 1;

    public ExtraLine(TimeLine timeLine)
    {
        this.adjustSize = false;
        this.timeLine = timeLine;

        this.setStyleAttribute("position", "relative");
        this.setStyleAttribute("border-top", "2px solid #AAA");
        this.setStyleAttribute("cursor", "default");
        this.setStyleAttribute("zIndex", "1");
        this.setStyleAttribute("fontSize", "1px");

        this.setHeight(BasePlanningWidget.CELL_HEIGHT);
        this.setStyleName(EXTRA_LINE_MOUSE_OVER_EVENT);

        this.setLayoutOnChange(false);
    }

    public TaskGroupModelData getTaskGroup()
    {
        return this.taskGroup;
    }

    public void setTaskGroup(TaskGroupModelData taskGroup)
    {
        this.taskGroup = taskGroup;
    }

    public ExtraLineItem getExtraLineItem()
    {
        return extraLineItem;
    }

    public void setExtraLineItem(ExtraLineItem extraLineItem)
    {
        this.extraLineItem = extraLineItem;
    }

    public Long getPersonId()
    {
        ExtraLineModelData extraLine = extraLineItem.getExtraLine();
        Long personId = extraLine.getPersonID();
        return personId;
    }

    public void addTask(TaskBar panel)
    {
        ExtraLine initialLine = panel.getExtraLine();
        if (initialLine != this)
        {
            panel.setExtraLine(this);
        }
        taskBars.put(panel.getTask().getId().toString(), panel);
        this.insert(panel, 0);
        panel.render(this.getElement());
    }

    @Override
    protected boolean insert(Component item, int index)
    {
        getItems().add(index, item);
        adopt(item);
        layoutNeeded = true;
        return true;
    }

    @Override
    public boolean layout()
    {
        return super.layout();
    }

    @Override
    public void onBrowserEvent(Event event)
    {}

    public void updateTaskColors(ETaskDisplayMode mode)
    {
        for (TaskBar taskPanel : taskBars.values())
        {
            taskPanel.setBackgroundColor(mode);
        }
    }

    /**
     * When something is removed, it checks if last line should be removed.
     */
    public void removeTask(TaskBar taskPanel)
    {
        int height = taskPanel.el().getBounds(true).y;

        taskBars.remove(taskPanel.getTask().getId().toString());
        super.remove(taskPanel);

        if (lineHeight == this.getLineFromHeight(height) && isLineEmpty(height))
        {
            this.removeLine(height);
        }
    }

    /**
     * Remove line inside the extra line (resize height).
     * 
     * @param lineTop
     *            The top position of the line to remove.
     */
    private void removeLine(int lineTop)
    {
        for (TaskBar taskPanel : taskBars.values())
        {
            int top = taskPanel.getTop();
            if (top > lineTop)
            {
                taskPanel.setTop(top - (BasePlanningWidget.CELL_HEIGHT + 1));
            }
        }
        this.removeLastLine();
    }

    /**
     * Resize extra line by adding PLANNING_CELL_HEIGHT to its size.
     */
    private void addLine()
    {
        this.lineHeight++;
        this.rebuildHeight();
    }

    /**
     * Resize extra line by removing PLANNING_CELL_HEIGHT to its size.
     */
    private void removeLastLine()
    {
        this.lineHeight--;
        this.rebuildHeight();
    }

    /**
     * Layout extra line depending on its number of line.
     */
    private void rebuildHeight()
    {
        int height = this.getHeightFromLine(lineHeight);
        height -= 2;
        // height--;// Height minus one to remove pixel added by bottom border.
        if (height < BasePlanningWidget.CELL_HEIGHT)
            height = BasePlanningWidget.CELL_HEIGHT;

        this.setHeight(height);
        this.extraLineItem.setHeight(height + "px");
    }

    /**
     * @param lineNumber
     *            The line of which height will be calculated.
     * @return Size in pixel for given line number.
     */
    public int getHeightFromLine(int lineNumber)
    {
        if (lineNumber > 0)
            return lineNumber * (BasePlanningWidget.CELL_HEIGHT + 2);
        else
            return 0;
    }

    /**
     * @param height
     *            The height to convert.
     * @return From height size in pixel return the number of lines inside the extra line.
     */
    public int getLineFromHeight(int height)
    {
        return height / (BasePlanningWidget.CELL_HEIGHT + 2) + 1;
    }

    /**
     * Checks if there is any item of which y coordinate is equal to <i>height</i>.
     * 
     * @param height
     *            Height of the line to check.
     * @return True if line is empty.
     */
    private boolean isLineEmpty(int height)
    {
        int line = this.getLineFromHeight(height);
        for (int i = 0; i < timeLine.getWrappers().length; i++)
        {
            Boolean isOccupied = occupation.get(line + "-" + i);
            if (isOccupied != null && isOccupied)
            {
                return false;
            }
        }

        // for (Component item : this.getItems())
        // {
        // int itemY = item.el().getBounds(true).y;
        // if (itemY == height)
        // {
        // return false;
        // }
        // }
        return true;
    }

    /**
     * Change the bar Y position depending on line occupation. It checks every line from line 1. If all lines are full,
     * a new line is added. It also update the position map by filling newly occupied person/day by true value.
     * 
     * @param xStart
     *            Starting X coordinate where the task should be placed.
     * @param xEnd
     *            Ending X coordinate where the task should be placed.
     * @param taskPanel
     *            Task panel to set in extra line.
     * @param isPopping
     */
    public void moveToRightLine(int xStart, int xEnd, TaskBar taskPanel, Boolean isPopping)
    {
        if (!GWT.isScript())
        {
            System.out.println("move to Right line " + xStart + " " + xEnd);
            this.printOccupation(1);
            this.printOccupation(2);
        }
        int currentLine = 1;

        int dayXStart = BarUtils.getStartDayOccupation(xStart);
        int dayXEnd = BarUtils.getEndDayOccupation(xEnd);

        // TaskBar previousTaskBar = this.taskBarCoordinates.get(dayXStart + "-" + dayXEnd);
        // if (previousTaskBar != null
        // && previousTaskBar != taskPanel
        // && previousTaskBar.getTask().getTaskTypeId().longValue() == taskPanel.getTask().getTaskTypeId()
        // .longValue() && !isPopping)
        // {
        // previousTaskBar.addAdditionalBar(taskPanel);
        // previousTaskBar.refreshToolTip();
        // if (this.getItems().contains(taskPanel))
        // this.remove(taskPanel);
        // }
        // else
        // {
        while (isLineFullForElement(currentLine, dayXStart, dayXEnd, taskPanel))
        {
            currentLine++;
        }

        if (currentLine > this.lineHeight)
        {
            this.addLine();
            layoutNeeded = true;
        }

        this.fillOccupation(currentLine, dayXStart, dayXEnd);

        // String key = this.invertTaskBarCoordinates.get(taskPanel);
        // if (key != null)
        // {
        // this.taskBarCoordinates.remove(key);
        // }

        // this.taskBarCoordinates.put(dayXStart + "-" + dayXEnd, taskPanel);
        // this.invertTaskBarCoordinates.put(taskPanel, dayXStart + "-" + dayXEnd);

        int top = this.getHeightFromLine(currentLine - 1);
        taskPanel.setTop(top);
        if (!GWT.isScript())
        {
            System.out.println("After move to Right line ");
            this.printOccupation(1);
            this.printOccupation(2);
        }
        // }
    }

    /**
     * @param line
     *            The line to check.
     * @param xStart
     *            The starting X coordinate for element to set on line.
     * @param xEnd
     *            The ending X coordinate for element to set on line.
     * @param taskPanel
     *            The panel to set on line.
     * @return True if there is an element at same place of element, false either.
     */
    private boolean isLineFullForElement(int line, int xStart, int xEnd, BaseTaskBar taskPanel)
    {
        for (int x = xStart; x <= xEnd; x++)
        {
            String key = line + "-" + x;
            if (occupation.get(key) != null && occupation.get(key))
                return true;
        }
        return false;
    }

    /**
     * Fill occupation map in a simple way. Each extra line square (day for a person) is represented by its coordinates
     * (line-dayNumber). When a day is occupied, it is marke as true inside the map.
     * 
     * @param line
     *            The line to fill.
     * @param dayXStart
     *            X coordinate from where filling start.
     * @param dayXEnd
     *            X coordinate to where filling stop.
     */
    private void fillOccupation(int line, int dayXStart, int dayXEnd)
    {
        for (int x = dayXStart; x <= dayXEnd; x++)
        {
            String key = line + "-" + x;
            occupation.put(key, Boolean.TRUE);
        }
    }

    private void removeOccupation(int line, int dayXStart, int dayXEnd)
    {
        for (int x = dayXStart; x <= dayXEnd; x++)
        {
            String key = line + "-" + x;
            occupation.put(key, Boolean.FALSE);
        }
    }

    /**
     * Set occupation for taskBar given in parameter by filling occupation map for each planning coordinate occupied by
     * task bar.
     * 
     * @param taskBar
     *            The occupation
     */
    public void fillOccupation(TaskBar taskBar)
    {
        int line = this.getLineFromHeight(taskBar.getTop());

        int dayXStart = BarUtils.getStartDayOccupation(taskBar.getLeft());
        int dayXEnd = BarUtils.getEndDayOccupation(taskBar.getLeft() + taskBar.getWidth());

        this.fillOccupation(line, dayXStart, dayXEnd);
    }

    /**
     * Remove occupation for task bar given in parameter by filling occupation map for each planning coordinate occupied
     * by task bar.
     * 
     * @param taskBar
     *            The occupation
     */
    public void removeOccupation(BaseTaskBar taskBar)
    {
        int line = this.getLineFromHeight(taskBar.getTop());

        int dayXStart = BarUtils.getStartDayOccupation(taskBar.getLeft());
        int dayXEnd = BarUtils.getEndDayOccupation(taskBar.getLeft() + taskBar.getWidth());

        this.removeOccupation(line, dayXStart, dayXEnd);
    }

    /**
     * Print in console a text representation of the planning line requested with its occupation.
     * 
     * @param line
     */
    public void printOccupation(int line)
    {
        System.out.print("|");
        for (int i = 0; i < timeLine.getWrappers().length; i++)
        {
            Boolean isOccupied = occupation.get(line + "-" + i);
            if (isOccupied != null && isOccupied)
            {
                System.out.print("o|");
            }
            else
            {
                System.out.print("x|");
            }
        }
        System.out.println("");
    }

    public void moveAllTasks(int nbDays)
    {
        for (TaskBar taskBar : taskBars.values())
        {
            DateWrapper startDate = new DateWrapper(taskBar.getStartDate());
            DateWrapper endDate = new DateWrapper(taskBar.getEndDate());

            this.removeOccupation(taskBar);
            taskBar.setPosition(startDate.addDays(nbDays).asDate(), endDate.addDays(nbDays).asDate());
            taskBar.setDirty();
            this.fillOccupation(taskBar);
            AppEvent event = new AppEvent(PlanningCommonEvents.TASK_MOVED, taskBar.getTask());
            event.setData("planning-type", EPlanningDisplayMode.TASK_TYPE);
            EventDispatcher.forwardEvent(event);
        }
    }

    public TaskBar getTaskBar(TaskModelData task)
    {
        return this.taskBars.get(task.getId().toString());
    }

    public Boolean hasTask(TaskBar taskBar)
    {
        return this.taskBars.get(taskBar.getId().toString()) != null;
    }

    public List<TaskModelData> getTasks()
    {
        List<TaskModelData> tasks = new ArrayList<TaskModelData>();

        for (TaskBar taskBar : taskBars.values())
        {
            tasks.add(taskBar.getTask());
        }
        return tasks;
    }

    public void displayDuration()
    {
        Long duration = 0L;
        Long elapsed = 0L;
        for (TaskBar taskBar : taskBars.values())
        {
            Long taskDuration = taskBar.getTask().getDuration();
            if (taskDuration == null)
                taskDuration = 0L;
            duration += taskDuration;
            elapsed += taskBar.getTask().getTotalActivityDuration();
        }

        this.getExtraLineItem().setDurationLabel(elapsed, duration);
    }
}
