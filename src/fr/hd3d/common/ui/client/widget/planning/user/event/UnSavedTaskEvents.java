package fr.hd3d.common.ui.client.widget.planning.user.event;

import com.extjs.gxt.ui.client.event.EventType;


public class UnSavedTaskEvents
{
    private static int START_INDEX = -1;
    public static final String HD3D_MODEL_DATA = "hd3dModelData";
    public static final EventType ADD_EVENT = new EventType(PlanningEvents.UNSAVED_TASK_EVENT_BASE + ++START_INDEX);
    public static final EventType CLEAR_EVENT = new EventType(PlanningEvents.UNSAVED_TASK_EVENT_BASE + ++START_INDEX);
    public static final EventType REMOVE_EVENT = new EventType(PlanningEvents.UNSAVED_TASK_EVENT_BASE + ++START_INDEX);
}
