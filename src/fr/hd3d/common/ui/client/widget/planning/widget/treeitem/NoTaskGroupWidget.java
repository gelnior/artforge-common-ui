package fr.hd3d.common.ui.client.widget.planning.widget.treeitem;



/**
 * Simple widget to display inside tree that no group has been found for this tree.
 * 
 * @author HD3D
 */
public class NoTaskGroupWidget extends TreeLabelWidget
{
    /**
     * Constructor.
     */
    public NoTaskGroupWidget()
    {
        super();

        this.addText("No task group found");
        // this.addText(Planning.MESSAGES.noTaskGroupFound());
        // this.addText(Planning.MESSAGES.noTaskGroupFoundEnd());
        this.setStyleName("NoTaskGroupWidget");
    }
}
