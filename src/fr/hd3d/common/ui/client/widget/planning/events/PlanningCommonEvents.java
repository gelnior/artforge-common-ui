package fr.hd3d.common.ui.client.widget.planning.events;

import com.extjs.gxt.ui.client.event.EventType;


/**
 * Events raised by planning widgets.
 * 
 * @author HD3D
 */
public class PlanningCommonEvents
{
    public static final String FIRST_EXPAND_EVENT_VAR_NAME = "firstExpand";

    public static final EventType TASK_MOVED = new EventType();
    public static final EventType TASK_GROUP_OPENED = new EventType();
    public static final EventType TASK_GROUP_CLOSED = new EventType();
    public static final EventType TASK_GROUP_SYNCHRONIZE_BOUNDS_CLICKED = new EventType();
    public static final EventType TASK_GROUP_LOAD_WORKED_DAYS_CLICKED = new EventType();
    public static final EventType TASK_GROUP_REFRESH_CLICKED = new EventType();
    public static final EventType TASK_GROUP_ADD_WORKERS_CLICKED = new EventType();
    public static final EventType TASK_GROUP_SELECTED = new EventType();

    public static final EventType TASK_STATUS_CHANGED = new EventType();
    public static final EventType TASK_SELECTED = new EventType();
    public static final EventType TASK_DESELECTED = new EventType();

    public static final EventType ADD_MILESTONE_CLICKED = new EventType();
    public static final EventType UPDATE_MILESTONE_CLICKED = new EventType();
    public static final EventType DELETE_MILESTONE_CLICKED = new EventType();
    public static final EventType MILESTONE_SAVED = new EventType();
    public static final EventType DELETE_MILESTONE_CONFIRMED = new EventType();

}
