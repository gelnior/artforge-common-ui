package fr.hd3d.common.ui.client.widget.planning.user.event;

import com.extjs.gxt.ui.client.event.EventType;


public class PlanningEvents
{
    private static int START_INDEX = -1;
    public static int BASE_EVENT = 18700;
    public static int UNSAVED_TASK_EVENT_BASE = BASE_EVENT + 50;

    public static final EventType ADD_DAY_REPERE_EVENT = new EventType(BASE_EVENT + ++START_INDEX);
    public static final EventType ADD_USERS_EVENT = new EventType(BASE_EVENT + ++START_INDEX);
    public static final EventType ADD_PLANNING_EVENTS_EVENT = new EventType(BASE_EVENT + ++START_INDEX);
    public static final EventType ADD_SELECTED_USER_EVENT = new EventType(BASE_EVENT + ++START_INDEX);
    public static final EventType PLANNING_EVENTS_EVENT = new EventType(BASE_EVENT + ++START_INDEX);
    public static final EventType REMOVE_DAYS_REPERE_EVENT = new EventType(BASE_EVENT + ++START_INDEX);
    public static final EventType REMOVE_SELECTED_USER_EVENT = new EventType(BASE_EVENT + ++START_INDEX);
    public static final EventType REMOVE_USER_EVENT = new EventType(BASE_EVENT + ++START_INDEX);
    public static final EventType RESIZE_REPERES_EVENT = new EventType(BASE_EVENT + ++START_INDEX);
    public static final EventType SELECT_USERS_EVENT = new EventType(BASE_EVENT + ++START_INDEX);
    public static final EventType USER_TASKS_EVENT = new EventType(BASE_EVENT + ++START_INDEX);
    public static final EventType USER_ABSCENCES_EVENT = new EventType(BASE_EVENT + ++START_INDEX);

    public static final EventType ADD_MILESTONE_CLICKED = new EventType(BASE_EVENT + ++START_INDEX);
    public static final EventType UPDATE_MILESTONE_CLICKED = new EventType(BASE_EVENT + ++START_INDEX);
    public static final EventType DELETE_MILESTONE_CLICKED = new EventType(BASE_EVENT + ++START_INDEX);
    public static final EventType MILESTONE_SAVED = new EventType(BASE_EVENT + ++START_INDEX);
    public static final EventType DELETE_MILESTONE_CONFIRMED = new EventType(BASE_EVENT + ++START_INDEX);
    public static final EventType TASK_STATUS_CHANGED = new EventType(BASE_EVENT + ++START_INDEX);
    public static final EventType TASK_SELECTED = new EventType(BASE_EVENT + ++START_INDEX);
    public static final EventType TASK_DESELECTED = new EventType(BASE_EVENT + ++START_INDEX);
}
