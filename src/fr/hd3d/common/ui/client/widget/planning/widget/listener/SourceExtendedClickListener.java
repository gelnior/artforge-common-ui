package fr.hd3d.common.ui.client.widget.planning.widget.listener;

public interface SourceExtendedClickListener
{
    public void addClickListener(ExtendedClickListener listener);

    public void removeClickListener(ExtendedClickListener listener);
}
