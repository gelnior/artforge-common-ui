package fr.hd3d.common.ui.client.widget.planning.util;

import com.extjs.gxt.ui.client.util.DateWrapper;

import fr.hd3d.common.ui.client.calendar.DatetimeUtil;
import fr.hd3d.common.ui.client.widget.planning.user.view.BasePlanningWidget;
import fr.hd3d.common.ui.client.widget.planning.widget.bar.TaskBar;


/**
 * Utilities to manage planning bars easily.
 * 
 * @author HD3D
 */
public class BarUtils
{
    public static int getWidthinDays(int xStart, int xEnd)
    {
        return (xEnd - xStart) / BasePlanningWidget.getCellWidth();
    }

    public static int getStartDayOccupation(int xStart)
    {
        return xStart / BasePlanningWidget.getCellWidth();
    }

    public static int getEndDayOccupation(int xEnd)
    {
        return (xEnd / BasePlanningWidget.getCellWidth()) - 1;
    }

    public static int getTaskBarRight(TaskBar taskBar)
    {
        return taskBar.getLeft() + taskBar.getWidth() + 1;
    }

    public static DateWrapper getDate(int x)
    {
        DateWrapper firstDate = BasePlanningWidget.timeLine.getWrappers()[0];
        int nbDaysFromTimeLineBegginingToStart = BarUtils.getStartDayOccupation(x);
        DateWrapper startDate = firstDate.addDays(nbDaysFromTimeLineBegginingToStart);

        return startDate;
    }

    public static int getDays(DateWrapper date)
    {
        DateWrapper firstDate = BasePlanningWidget.timeLine.getWrappers()[0];

        int nbDays = (int) ((firstDate.getTime() - date.getTime()) / (DatetimeUtil.DAY_SECONDS * 1000));
        // int nbDaysFromTimeLineBegginingToStart = BarUtils.getStartDayOccupation(x);
        // DateWrapper startDate = firstDate.addDays(nbDaysFromTimeLineBegginingToStart);

        return nbDays;
    }

    public static int skipMoveWeekend(int x)
    {
        DateWrapper startDate = getDate(x);

        int newX = x;
        if (DatetimeUtil.isSaturday(startDate))
            newX -= BasePlanningWidget.getCellWidth();
        else if (DatetimeUtil.isSunday(startDate))
            newX += BasePlanningWidget.getCellWidth();
        return newX;
    }

    public static int skipResizeLeftWeekend(DateWrapper firstDate, int x)
    {
        int nbDaysFromTimeLineBegginingToStart = BarUtils.getStartDayOccupation(x);
        DateWrapper startDate = firstDate.addDays(nbDaysFromTimeLineBegginingToStart - 1);

        int newX = x;
        if (DatetimeUtil.isSaturday(startDate))
            newX -= BasePlanningWidget.getCellWidth();
        else if (DatetimeUtil.isSunday(startDate))
            newX -= 2 * BasePlanningWidget.getCellWidth();

        return newX;
    }

    public static boolean isSaturday(int x)
    {
        return DatetimeUtil.isSaturday(getDate(x));
    }

    public static boolean isSunday(int x)
    {

        return DatetimeUtil.isSunday(getDate(x));
    }

    public static int getNbWeekEndDays(int x, int panelWidth)
    {
        DateWrapper startDate = getDate(x);
        DateWrapper endDate = getDate(x + panelWidth);

        return DatetimeUtil.getNbWeekEndDayBetween(startDate, endDate);
    }

    public static int getOpenDayWidth(int x, int durationDays)
    {
        int newDurationDays = durationDays;
        while (durationDays + BarUtils.getNbWeekEndDays(x, newDurationDays * BasePlanningWidget.getCellWidth()) > newDurationDays)
        {
            newDurationDays = BarUtils.getNbWeekEndDays(x, newDurationDays * BasePlanningWidget.getCellWidth())
                    + durationDays;
        }

        int panelWidth = newDurationDays * BasePlanningWidget.getCellWidth();

        if (DatetimeUtil.isSaturday(BarUtils.getDate(x + panelWidth - 5)) && panelWidth > 0)
            panelWidth += 2 * BasePlanningWidget.getCellWidth();
        else if (DatetimeUtil.isSunday(BarUtils.getDate(x + panelWidth - 5)) && panelWidth > 0)
            panelWidth += 2 * BasePlanningWidget.getCellWidth();

        if (panelWidth < BasePlanningWidget.getCellWidth())
            return BasePlanningWidget.getCellWidth();
        else
            return panelWidth;
    }

}
