package fr.hd3d.common.ui.client.widget.planning.widget.tree;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.extjs.gxt.ui.client.GXT;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.hd3d.common.ui.client.modeldata.task.ExtraLineModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskGroupModelData;
import fr.hd3d.common.ui.client.util.NativeUtil;
import fr.hd3d.common.ui.client.widget.planning.widget.treeitem.ExtraLineItem;
import fr.hd3d.common.ui.client.widget.planning.widget.treeitem.PlanningTreeItem;
import fr.hd3d.common.ui.client.widget.planning.widget.treeitem.TaskGroupItem;


public class BasePlanningTree extends PlanningTreeItem
{
    private PlanningTreeItem selectedItem;
    private final Map<Long, TaskGroupItem> planningTreeMap = new HashMap<Long, TaskGroupItem>();
    private int openedChild = 0;

    public BasePlanningTree()
    {
        this.setStyles();
        NativeUtil.disableContextMenuInternal(RootPanel.get().getElement(), true);
    }

    @Override
    public int getOffsetHeight()
    {
        int offsetHeight = 0;
        for (Iterator<Widget> iterator = getChildren().iterator(); iterator.hasNext();)
        {
            Widget widget = iterator.next();
            String heightString = DOM.getStyleAttribute(widget.getElement(), "height").replaceAll("px", "");
            if (!"".equals(heightString))
            {
                offsetHeight += Integer.valueOf(heightString).intValue();
            }
            else
            {
                offsetHeight += Integer.valueOf(widget.getOffsetHeight()).intValue();
            }
        }
        return offsetHeight;
    }

    public void removeTaskGroup()
    {
        if (selectedItem.getParentWidget() != null)
        {
            selectedItem.getParentWidget().removeItem(selectedItem);
        }
        else
        {
            this.remove(selectedItem);
        }
    }

    public void addTaskGroupItem(TaskGroupItem taskGroupItem)
    {
        taskGroupItem.setTree(this);
        taskGroupItem.setRoot(true);
        add(taskGroupItem);
    }

    public PlanningTreeItem getSelectedItem()
    {
        return selectedItem;
    }

    public void setSelectedItem(PlanningTreeItem selectedItem)
    {
        this.selectedItem = selectedItem;
    }

    public void buildTaskGroupTree(List<TaskGroupModelData> data)
    {

    }

    public TaskGroupItem addTaskGroupItem(TaskGroupModelData newTaskGroupModelData)
    {
        TaskGroupItem taskGroupItem = new TaskGroupItem(newTaskGroupModelData);
        addTaskGroupItem(taskGroupItem);
        return taskGroupItem;
    }

    public ExtraLineItem addExtraLine(TaskGroupItem taskGroupItem, ExtraLineModelData extraLineModelData)
    {
        ExtraLineItem extraLineItem = new ExtraLineItem(extraLineModelData);
        extraLineItem.getExtraLine().setTaskGroupID(taskGroupItem.getTaskGroup().getId());
        taskGroupItem.addItem(extraLineItem);
        return extraLineItem;
    }

    public void clearSelection()
    {
        if (this.selectedItem != null)
        {
            this.selectedItem.setSelected(false);
            this.selectedItem = null;
        }
    }

    private int getRecursiveChildren(List<PlanningTreeItem> list, int count)
    {
        for (Iterator<PlanningTreeItem> iterator = list.iterator(); iterator.hasNext();)
        {
            PlanningTreeItem widget = iterator.next();
            if (widget instanceof TaskGroupItem)
            {
                TaskGroupItem ulWidget = (TaskGroupItem) widget;
                if (ulWidget.getChildItems().size() != 0 && ulWidget.isOpen())
                {
                    count += ulWidget.getChildItems().size();
                    if (!GXT.isIE)
                    {
                        ++openedChild;
                    }
                    getRecursiveChildren(ulWidget.getChildItems(), count);
                }
            }
        }
        return count;
    }

    public int getChildCount()
    {
        int count = this.getChildren().size();
        for (Iterator<Widget> iterator = this.getChildren().iterator(); iterator.hasNext();)
        {
            Widget widget = iterator.next();
            if (widget instanceof TaskGroupItem)
            {
                TaskGroupItem groupItem = (TaskGroupItem) widget;
                if (groupItem.getChildItems().size() != 0 && groupItem.isOpen())
                {
                    count += groupItem.getChildItems().size();
                    if (!GXT.isIE)
                    {
                        ++openedChild;
                    }
                    getRecursiveChildren(groupItem.getChildItems(), count);
                }
            }
        }
        return count;
    }

    public Map<Long, TaskGroupItem> getPlanningTreeMap()
    {
        return planningTreeMap;
    }

    public int getOpenedChild()
    {
        return openedChild;
    }

    private void setStyles()
    {
        this.setStyleName("CustomTree");
        this.setHeight("100%");
        DOM.setStyleAttribute(this.getElement(), "borderBottom", "1px solid black");
        // HD3DNativeUtil.disableContextMenuInternal(RootPanel.get().getElement(), true);
    }

    @Override
    public void addItem(TreeItemWidget taskGroupItem)
    {}

    @Override
    public void removeItem(TreeItemWidget widget)
    {}

    @Override
    public void select()
    {}

    @Override
    public void setSelected(boolean selected)
    {}
}
