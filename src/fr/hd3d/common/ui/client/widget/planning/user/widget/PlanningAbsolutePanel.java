package fr.hd3d.common.ui.client.widget.planning.user.widget;

import com.extjs.gxt.ui.client.Style.Scroll;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;

import fr.hd3d.common.ui.client.util.CSSUtils;
import fr.hd3d.common.ui.client.widget.planning.widget.line.TaskGroupLine;


public class PlanningAbsolutePanel extends LayoutContainer
{
    private TaskGroupLine lastTaskGroupLine = null;
    private TaskGroupLine firstTaskGroupLine = null;

    public PlanningAbsolutePanel()
    {
        adjustSize = false;
        this.setLayout(new FlowLayout());
        this.setStyleAttribute("position", "relative");
        this.setHeight(2);
        this.setScrollMode(Scroll.NONE);
    }

    /**
     * When a task group line is added, its border styles are set depending on its addition order.
     * 
     * @param taskGroupLine
     *            Task group line to add.
     */
    public void add(TaskGroupLine taskGroupLine)
    {
        super.add(taskGroupLine);

        if (lastTaskGroupLine != null)
        {
            CSSUtils.set2pxBorderTop(taskGroupLine, CSSUtils.COLOR_BLACK);
            CSSUtils.set2pxBorderBottom(taskGroupLine, CSSUtils.COLOR_BLACK);

            lastTaskGroupLine.setStyleAttribute("border-bottom", "none");
        }
        else
        {
            if (firstTaskGroupLine == null)
                CSSUtils.set2pxBorderBottom(taskGroupLine, CSSUtils.COLOR_BLACK);

            firstTaskGroupLine = taskGroupLine;
        }
        lastTaskGroupLine = taskGroupLine;
    }

    /** Set to null variables used to determine border on added task group line. */
    public void resetBorders()
    {
        lastTaskGroupLine = null;
        firstTaskGroupLine = null;
    }
}
