package fr.hd3d.common.ui.client.widget.planning.user.view;

import com.extjs.gxt.ui.client.Style.Orientation;
import com.extjs.gxt.ui.client.Style.Scroll;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.layout.BorderLayoutData;
import com.extjs.gxt.ui.client.widget.layout.RowData;
import com.extjs.gxt.ui.client.widget.layout.RowLayout;
import com.extjs.gxt.ui.client.widget.toolbar.ToolBar;
import com.google.gwt.event.dom.client.ScrollEvent;
import com.google.gwt.event.dom.client.ScrollHandler;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.DeferredCommand;
import com.google.gwt.user.client.ui.ScrollPanel;

import fr.hd3d.common.ui.client.widget.BorderedPanel;
import fr.hd3d.common.ui.client.widget.planning.EZoomMode;
import fr.hd3d.common.ui.client.widget.planning.user.widget.PlanningAbsolutePanel;
import fr.hd3d.common.ui.client.widget.planning.user.widget.TimeLine;
import fr.hd3d.common.ui.client.widget.planning.util.PlanningSettings;
import fr.hd3d.common.ui.client.widget.planning.widget.tree.PlanningTreePanel;


/**
 * Base class for displaying planning widgets (user planning, task type planning, work objects planning...).
 * 
 * @author HD3D
 */
public class BasePlanningWidget
{
    /** Default west panel (navigation tree) width */
    public static final int WEST_PANEL_WIDTH = 200;

    /** Height of planning cells. */
    public static int CELL_HEIGHT = 20;
    /** Width of planning cells. */
    private static final int DEFAULT_CELL_WIDTH = 15;

    private static int cellWidth = DEFAULT_CELL_WIDTH;
    private static EZoomMode mode = EZoomMode.NORMAL;

    public static final int DEFAULT_SCROLL_HEIGHT = 16;

    public static Boolean previewMode = Boolean.FALSE;

    /** Path to planning images. */
    public static final String IMAGES_PATH = "images/planning/";

    /** Planning time line displaying month, weeks and days. */
    public static TimeLine timeLine;

    public static int getCellWidth()
    {
        return cellWidth;
    }

    public static void setCellWidth(int thisCellWidth)
    {
        cellWidth = thisCellWidth;
    }

    public static EZoomMode getZoomMode()
    {
        return mode;
    }

    public static void setZoomMode(EZoomMode newMode)
    {
        mode = newMode;
        if (newMode == EZoomMode.BIG)
            BasePlanningWidget.setCellWidth(90);
        else if (newMode == EZoomMode.NORMAL)
            BasePlanningWidget.setCellWidth(15);
        else if (newMode == EZoomMode.SMALL)
            BasePlanningWidget.setCellWidth(5);
    }

    /**
     * @return Actual display mode if it shows planned tasks or if it shows real start date and end date for tasks.
     */
    public static Boolean isPreviewMode()
    {
        return previewMode;
    }

    /**
     * Set actual display mode, to true if it shows planned tasks or if it shows real start date and end date for tasks.
     */
    public static void setPreviewMode(Boolean previewMode)
    {
        BasePlanningWidget.previewMode = previewMode;
    }

    /** Main panel that contains navigation tree and planning. */
    protected final BorderedPanel mainPanel = new BorderedPanel();

    /** Panel that contains planning. */
    protected PlanningAbsolutePanel boundaryPanel;
    /** Panel that contains planning with no time line. */
    protected ScrollPanel planningPanel;
    /** Panel that contains time line planning. */
    protected ScrollPanel timeLinePanel;

    /** Toolbar containing planning widget tools. */
    protected ToolBar planningToolBar;
    /** Tree needed to navigate inside planning. */
    protected final PlanningTreePanel planningTree;

    /** Panel used to wrap GWT panel that contains planning. */
    protected ContentPanel center;

    /** True if time line should be displayed. */
    protected final boolean withTimeLine;
    /** True if other elements like milestones should be displayed. */
    protected boolean withOtherElement = false;

    /**
     * Constructor.
     * 
     * @param planningTree
     *            Tree needed to navigate inside planning.
     * @param withTimeLine
     *            True if time line should be displayed.
     */
    public BasePlanningWidget(PlanningTreePanel planningTree, Boolean withTimeLine)
    {
        this.boundaryPanel = new PlanningAbsolutePanel();
        this.planningTree = planningTree;
        this.withTimeLine = withTimeLine;
    }

    /**
     * Constructor.
     * 
     * @param planningTree
     *            Tree needed to navigate inside planning.
     * @param withTimeLine
     *            True if time line should be displayed.
     */
    public BasePlanningWidget(PlanningTreePanel planningTree, PlanningAbsolutePanel panel, Boolean withTimeLine)
    {
        this.boundaryPanel = panel;
        this.planningTree = planningTree;
        this.withTimeLine = withTimeLine;
    }

    /**
     * Set to true if other elements like milestones should be displayed in timeline.
     * 
     * @param withOtherElement
     *            True if other elements like milestones should be displayed in timeline.
     */
    public void setWithOtherElement(boolean withOtherElement)
    {
        this.withOtherElement = withOtherElement;
    }

    /**
     * Initialize planning widget components.
     */
    public void init()
    {
        this.createWestPanel();
        this.createCenterPanel();

        if (withTimeLine)
            this.setTimeLine();
    }

    /**
     * Build time line and adapts navigation tree header.
     * 
     */
    private void setTimeLine()
    {
        planningTree.setTimeLine(timeLine);
        DeferredCommand.addCommand(new Command() {
            public void execute()
            {
                int height = center.getBounds(true).height;
                planningTree.setHeight(height);
            }
        });
    }

    public ScrollPanel getTimeLinePanel()
    {
        return timeLinePanel;
    }

    public PlanningTreePanel getPlanningTree()
    {
        return this.planningTree;
    }

    public PlanningAbsolutePanel getPlanningPanel()
    {
        return boundaryPanel;
    }

    public ScrollPanel getPlanningScrollPanel()
    {
        return planningPanel;
    }

    public TimeLine getTimeline()
    {
        return timeLine;
    }

    public void setTimeLine(TimeLine timeLineToSet)
    {
        timeLine = timeLineToSet;
    }

    public void startLoading()
    {
        this.center.mask("Loading");
        this.planningTree.mask("Loading");
    }

    public void stopLoading()
    {
        this.center.unmask();
        this.planningTree.unmask();
    }

    protected void createWestPanel()
    {
        BorderLayoutData westData = mainPanel.addWest(planningTree, WEST_PANEL_WIDTH);
        westData.setMinSize(250);
        westData.setMaxSize(400);
        this.planningTree.setScrollMode(Scroll.NONE);
    }

    protected void createCenterPanel()
    {
        this.center = new ContentPanel();
        this.center.setLayout(new RowLayout(Orientation.VERTICAL));
        this.center.setHeaderVisible(false);
        this.center.setScrollMode(Scroll.NONE);
        this.center.setBorders(false);

        if (timeLine == null)
            timeLine = new TimeLine(new PlanningSettings());
        this.timeLinePanel = new ScrollPanel(timeLine);
        this.timeLinePanel.setWidth("100%");
        this.timeLinePanel.setAlwaysShowScrollBars(false);
        DOM.setStyleAttribute(timeLinePanel.getElement(), "marginRight", DEFAULT_SCROLL_HEIGHT + "px");
        DOM.setStyleAttribute(timeLinePanel.getElement(), "overflow", "hidden");
        this.center.add(timeLinePanel, new RowData(1, -1));

        this.planningPanel = new ScrollPanel(this.boundaryPanel);
        this.planningPanel.setWidth("100%");
        this.planningPanel.setStyleName("planningPanel");
        this.planningPanel.setAlwaysShowScrollBars(true);
        this.planningPanel.addScrollHandler(new ScrollHandler() {
            public void onScroll(ScrollEvent event)
            {
                int scrollLeft = planningPanel.getHorizontalScrollPosition();
                int scrollTop = planningPanel.getScrollPosition();

                if (timeLinePanel != null)
                    timeLinePanel.setHorizontalScrollPosition(scrollLeft);

                if (planningTree != null)
                    planningTree.getScrollPanel().setScrollPosition(scrollTop);
            }
        });

        this.center.add(planningPanel, new RowData(1, 1));

        this.mainPanel.addCenter(center, BorderedPanel.MARGIN, BorderedPanel.NO_MARGIN, BorderedPanel.MARGIN,
                BorderedPanel.NO_MARGIN);
        timeLine.setStyleAttribute("border-right", "1px solid #99BBE8");
        this.boundaryPanel.setStyleAttribute("border-right", "1px solid #99BBE8");
    }

    protected void setPlanningToolBar(ToolBar planningToolBar)
    {
        this.planningToolBar = planningToolBar;
        this.mainPanel.setTopComponent(this.planningToolBar);
    }

    public void scrollPlanning(int distanceX, int distanceY)
    {
        int scrollLeft = this.planningPanel.getHorizontalScrollPosition();
        int scrollTop = this.planningPanel.getScrollPosition();

        this.planningPanel.setHorizontalScrollPosition(scrollLeft + distanceX);
        this.planningPanel.setScrollPosition(scrollTop + distanceY);
        if (this.timeLinePanel != null)
            this.timeLinePanel.setHorizontalScrollPosition(scrollLeft + distanceX);
        if (this.planningTree != null)
            this.planningTree.getScrollPanel().setScrollPosition(scrollTop + distanceY);
    }

}
