package fr.hd3d.common.ui.client.widget.planning;

/**
 * Enumeration to define which task display mode is currently active.
 * 
 * @author HD3D
 */
public enum ETaskDisplayMode
{
    TASK_TYPE, STATUS, ON_TIME
}
