package fr.hd3d.common.ui.client.widget.planning.widget.tree;

import com.google.gwt.dom.client.Document;



public class TreeItemWidget extends NodeWidgetBase
{
    public TreeItemWidget()
    {
        this.setElement(Document.get().createDivElement());
    }

    @Override
    public void addItem(TreeItemWidget widget)
    {
        this.add(widget, getElement());
    }

    @Override
    public void removeItem(TreeItemWidget widget)
    {
        this.remove(widget);
    }

    @Override
    public void setLabel(String label)
    {}
}
