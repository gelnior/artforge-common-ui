package fr.hd3d.common.ui.client.widget.planning.widget.bar;

import fr.hd3d.common.ui.client.modeldata.BaseTaskModelData;
import fr.hd3d.common.ui.client.widget.planning.user.widget.TimeLine;
import fr.hd3d.common.ui.client.widget.planning.widget.line.ExtraLine;


/**
 * Contains basic methods for bar linked to task objects. Moreover task bar are always linked to an extra line.
 * 
 * @author Bar
 */
public class BaseTaskBar extends PlanningBar
{
    /** Extra line on which task appears. */
    protected ExtraLine extraLine = null;

    /** True it panel can move only on an horizontal plan. */
    private boolean constraintHorizontal = false;

    /**
     * Constructor.
     * 
     * @param timeLine
     *            Planning time line.
     * @param timeObject
     *            Time object represented by planning bar.
     * @param borderColor
     *            Border color of the planning bar.
     * @param bgColor
     *            Background color of the planning bar.
     */
    public BaseTaskBar(TimeLine timeLine, BaseTaskModelData baseTask, String borderColor, String bgColor)
    {
        super(timeLine, baseTask, borderColor, bgColor);
    }

    /**
     * Set if planning bar should be moved only horizontally.
     * 
     * @param constraintHorizontal
     *            Boolean to set.
     */
    public void setConstraintHorizontal(boolean constraintHorizontal)
    {
        this.constraintHorizontal = constraintHorizontal;
    }

    /**
     * @return True if current bar can be moved.
     */
    public boolean isConstraintHorizontal()
    {
        return constraintHorizontal;
    }

    /**
     * @return Parent extra line.
     */
    public ExtraLine getExtraLine()
    {
        return extraLine;
    }

    /**
     * Set parent extra line.
     * 
     * @param extraLine
     *            Extra line to set.
     */
    public void setExtraLine(ExtraLine extraLine)
    {
        this.extraLine = extraLine;
    }

    public static void showContextMenu(int clientX, int clientY)
    {
        // TaskPanelBase panelBase = planningModel.getCurrentTaskPanelSelected();
        // if (panelBase instanceof TaskPanel && TaskTypePlanningModel.isUserMode())
        // {
        // if (contextMenu == null)
        // {
        // contextMenu = createContextMenu(planningModel);
        // }
        // if (contextMenu != null)
        // {
        // contextMenu.showAt(clientX, clientY);
        // }
        // }
    }

    // protected void initListeners()
    // {
    // this.addListener(Events.Select, new Listener<ComponentEvent>() {
    // public void handleEvent(ComponentEvent be)
    // {
    // TaskPanelBase.this.selected = !TaskPanelBase.this.selected;
    // if (TaskPanelBase.this.selected)
    // {
    // TaskPanelBase.this.addStyleName(SELECTED_STYLE);
    // }
    // else
    // {
    // TaskPanelBase.this.removeStyleName(SELECTED_STYLE);
    // }
    // }
    // });
    // }

}
