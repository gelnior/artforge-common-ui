package fr.hd3d.common.ui.client.widget.planning.widget.bar;

import fr.hd3d.common.ui.client.modeldata.task.TaskGroupModelData;
import fr.hd3d.common.ui.client.util.CSSUtils;
import fr.hd3d.common.ui.client.widget.planning.user.widget.TimeLine;


public class TaskGroupBar extends BaseTaskBar
{

    public TaskGroupBar(TimeLine timeLine, TaskGroupModelData taskGroup, String borderColor, String bgColor)
    {
        super(timeLine, taskGroup, null, bgColor);

        this.setStyleAttribute("position", "relative");
        this.setBorderColor(null);
        this.setLabel(taskGroup.getName());

        CSSUtils.setFontWeight(this, CSSUtils.FONT_BOLD);
    }

    public TaskGroupModelData getTaskGroup()
    {
        return (TaskGroupModelData) this.getTimeObject();
    }

    // @Override
    // public void setPosition(Date startDate, Date endDate)
    // {
    // Date initialStartdate = this.getStartDate();
    //
    // this.setStartDate(startDate);
    // this.setEndDate(endDate);
    //
    // int nbDays = CalendarUtil.getDaysBetween(initialStartdate, getStartDate());
    // int difference = nbDays * BasePlanningWidget.CELL_WIDTH;
    // this.setLeft(getLeft() + difference);
    // }

    // public void refreshBounds(Date startDate, Date endDate)
    // {
    // int nbDays = CalendarUtil.getDaysBetween(startDate, endDate);
    // this.setWidth(((nbDays + 1) * TaskTypePlanningView.CELL_WIDTH) - 1);
    //
    // this.getHeaderText().setSize(this.getOffsetWidth() - 2 * TaskPanel.BORDER_WIDTH,
    // this.getOffsetHeight() - 2 * TaskPanel.BORDER_WIDTH);
    // this.setPosition(startDate, endDate);
    // EventDispatcher.forwardEvent(PlanningAppEvents.TASK_GROUP_MOVED, this.getTimeObject());
    // }
}
