package fr.hd3d.common.ui.client.widget.planning.widget.bar;

import java.util.Date;

import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.google.gwt.user.datepicker.client.CalendarUtil;

import fr.hd3d.common.ui.client.util.CSSUtils;
import fr.hd3d.common.ui.client.widget.planning.EZoomMode;
import fr.hd3d.common.ui.client.widget.planning.user.view.BasePlanningWidget;


/**
 * Widget used to mark special days such as current day or milestone day. It is displayed as a transparent column for a
 * given day.
 * 
 * @author HD3D
 */
public class ColumnBar extends LayoutContainer
{
    public static final String DAY_COLUMN_COLOR = "green";

    protected int left;

    public ColumnBar(String color)
    {
        this.setWidth("5px");
        this.setHeight("100%");

        CSSUtils.setBackground(this, color);
        CSSUtils.setPosition(this, CSSUtils.POSITION_ABSOLUTE);
        CSSUtils.setFloat(this, CSSUtils.LEFT);
        CSSUtils.setZindex(this, 3);
        CSSUtils.setOpacity(this, "0.2");
    }

    public Integer getLeft()
    {
        return this.left;
    }

    /**
     * Set column bar to the position corresponding to <i>date</i> inside planning.
     * 
     * @param date
     *            The date on which column bar will be set.
     */
    public void setDate(Date date)
    {
        int left = CalendarUtil.getDaysBetween(BasePlanningWidget.timeLine.getWrappers()[0].asDate(), date) + 1;
        left *= BasePlanningWidget.getCellWidth();
        left -= 5;
        if (BasePlanningWidget.getZoomMode() != EZoomMode.SMALL)
            left -= 3;
        CSSUtils.setLeft(this, left + "px");

        this.left = left;
    }
}
