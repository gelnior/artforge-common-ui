package fr.hd3d.common.ui.client.widget.planning.user.dialog;

import java.util.List;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.store.StoreEvent;
import com.extjs.gxt.ui.client.store.StoreListener;
import com.extjs.gxt.ui.client.util.Margins;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.DualListField;
import com.extjs.gxt.ui.client.widget.form.ListField;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.RowData;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.reader.PersonReader;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.widget.BaseToolBar;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;


public class AddUserDialog extends Window
{
    private static AddUserDialog instance;

    private final BaseToolBar toolBar = new BaseToolBar();

    private final ServiceStore<PersonModelData> availableStore = new ServiceStore<PersonModelData>(new PersonReader());
    private final ListStore<PersonModelData> selectedStore = new ListStore<PersonModelData>();

    private final DualListField<PersonModelData> dualListField = new DualListField<PersonModelData>();

    private EventType eventType = null;
    private Button addUsersButton;

    public static AddUserDialog getInstance(EventType eventType)
    {
        if (instance == null)
        {
            instance = new AddUserDialog(eventType);
        }
        else
        {
            instance.setEventType(eventType);
        }
        return instance;
    }

    @Override
    public void show()
    {
        availableStore.setPath(ServicesPath.PROJECTS + MainModel.getCurrentProject().getId() + "/"
                + ServicesPath.PERSONS);
        availableStore.reload();

        super.show();
    }

    @Override
    public void hide()
    {
        super.hide();

        this.availableStore.removeAll();
        this.selectedStore.removeAll();
    }

    private void buildUi()
    {
        final ListField<PersonModelData> fromField = dualListField.getFromList();
        final ListField<PersonModelData> toField = dualListField.getToList();

        fromField.setStore(this.availableStore);
        fromField.setTemplate(getTemplate());
        fromField.setItemSelector("div.thumb-wrap");
        fromField.getListView().addListener(Events.DoubleClick, new Listener<BaseEvent>() {

            public void handleEvent(BaseEvent be)
            {
                List<PersonModelData> sel = fromField.getSelection();
                for (PersonModelData model : sel)
                {
                    fromField.getStore().remove(model);
                }
                toField.getStore().add(sel);
            }
        });

        this.selectedStore.addStoreListener(new StoreListener<PersonModelData>() {
            @Override
            public void storeAdd(StoreEvent<PersonModelData> se)
            {
                super.storeAdd(se);
                addUsersButton.setEnabled(true);
            }

            @Override
            public void storeClear(StoreEvent<PersonModelData> se)
            {
                super.storeClear(se);
                addUsersButton.setEnabled(false);
            }

            @Override
            public void storeRemove(StoreEvent<PersonModelData> se)
            {
                super.storeRemove(se);
                if (se.getStore().getModels().size() == 0)
                {
                    addUsersButton.setEnabled(false);
                }
            }
        });
        toField.setStore(this.selectedStore);
        toField.setTemplate(getTemplate());
        toField.setItemSelector("div.thumb-wrap");
        this.add(dualListField, new RowData(1, 1, new Margins(5)));
    }

    private AddUserDialog(EventType eventType)
    {
        this.eventType = eventType;

        this.setLayout(new FitLayout());
        this.setSize(600, 400);

        this.setHeading("Add user test");

        this.buildUi();
        this.createButtons();
    }

    private void setEventType(EventType eventType)
    {
        this.eventType = eventType;
    }

    private void createButtons()
    {
        addUsersButton = new Button("Add persons");
        Button cancelButton = new Button("Cancel");
        addUsersButton.addSelectionListener(new SelectionListener<ButtonEvent>() {
            @Override
            public void componentSelected(ButtonEvent ce)
            {
                List<PersonModelData> personList = selectedStore.getModels();
                if (eventType != null && personList.size() > 0)
                {
                    AppEvent appEvent = new AppEvent(eventType, personList);
                    appEvent.setData("groupItem", null);
                    EventDispatcher.forwardEvent(appEvent);
                    hide();
                }
                hide();
            }
        });
        addUsersButton.setEnabled(false);
        cancelButton.addSelectionListener(new SelectionListener<ButtonEvent>() {
            @Override
            public void componentSelected(ButtonEvent ce)
            {
                hide();
            }
        });
        addButton(addUsersButton);
        addButton(cancelButton);
    }

    private native String getTemplate() /*-{
                                        return ['<tpl for=".">', 
                                        '<div class="thumb-wrap" style="border: 1px solid white">', 
                                        '<span class="x-editable">{name}</span></div>', 
                                        '</tpl>', 
                                        '<div class="x-clear"></div>'].join("");
                                        }-*/;
}
