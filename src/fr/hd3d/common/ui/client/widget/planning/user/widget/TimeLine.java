package fr.hd3d.common.ui.client.widget.planning.user.widget;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.util.DateWrapper;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.layout.AbsoluteData;
import com.extjs.gxt.ui.client.widget.layout.AbsoluteLayout;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;

import fr.hd3d.common.ui.client.calendar.DatetimeUtil;
import fr.hd3d.common.ui.client.modeldata.resource.MilestoneModelData;
import fr.hd3d.common.ui.client.util.CSSUtils;
import fr.hd3d.common.ui.client.widget.planning.EZoomMode;
import fr.hd3d.common.ui.client.widget.planning.user.view.BasePlanningWidget;
import fr.hd3d.common.ui.client.widget.planning.util.Hd3dDateUtil;
import fr.hd3d.common.ui.client.widget.planning.util.PlanningSettings;


/**
 * Time line displayed above planning. The time line also handles milestones.
 * 
 * @author HD3D
 */
public class TimeLine extends LayoutContainer
{
    private static final String TIMELINE_DAY_HEADER_STYLE = "timeline-day-header";
    private static final String TIMELINE_WEEK_DAY_STYLE = "weekDay";
    private static final String TIMELINE_WEEKEND_DAY_STYLE = "weekEndDay";

    public static final String DAY_COLOR = "#F0ECB2";
    public static final String WEEKEND_COLOR = "#DDEAF8";

    private static final int DAY_ROW_INDEX = 2;
    private static final int WEEK_ROW_INDEX = 1;
    private static final int MONTH_ROW_INDEX = 0;

    private final DateWrapper[] wrappers;
    private int lastWeekIndex = 0;
    private int lastMonthIndex = 0;
    private int lastMonth = 0;

    private final PlanningSettings planningSettings;
    private final List<MilestoneTimeLineElement> milestoneElements = new ArrayList<MilestoneTimeLineElement>();

    private final String[] days = "S,M,T,W,T,F,S".split(",");
    private final String[] months = "January,February,March,April,May,June,July,August,September,October,November,December"
            .split(",");

    /**
     * Create time line.
     * 
     * @param settings
     *            Planning settings (start date and end date).
     */
    public TimeLine(PlanningSettings settings)
    {
        this.planningSettings = settings;

        int nbDays = settings.getNbDays();
        if (nbDays < 0)
            nbDays = 0;
        this.wrappers = new DateWrapper[nbDays];

        this.setLayout(new AbsoluteLayout());
        CSSUtils.setPosition(this, CSSUtils.POSITION_RELATIVE);
        this.setHeight(5 * BasePlanningWidget.CELL_HEIGHT);
        this.setWidth(this.wrappers.length * BasePlanningWidget.getCellWidth());

        if (settings.getEndDate() != null)
        {
            buildMonthView();
        }
    }

    /**
     * @return Wrappers set on each column.
     */
    public DateWrapper[] getWrappers()
    {
        return wrappers;
    }

    public PlanningSettings getPlanningSettings()
    {
        return planningSettings;
    }

    /**
     * Remove all columns, clears the time line.
     */
    public void clear()
    {
        this.removeAll();
    }

    /**
     * @param date
     *            The date to check.
     * @return True if date is part of the time line.
     */
    public boolean isInPeriod(Date date)
    {
        if (getWrappers().length < 1)
            return false;

        DateWrapper startDate = getWrappers()[0];
        DateWrapper endDate = getWrappers()[getWrappers().length - 1];

        DateWrapper dateWrapper = new DateWrapper(date);

        if (startDate.before(dateWrapper) && dateWrapper.before(endDate))
            return true;

        return false;
    }

    /**
     * @param dateWrapper
     *            The date of which index is requested.
     * @return Index of a given date in the time line.
     */
    public int getIndexByDateWrapper(DateWrapper dateWrapper)
    {
        for (int i = 0; i < wrappers.length; i++)
        {
            DateWrapper wrapper = wrappers[i];
            if (wrapper.asDate().compareTo(dateWrapper.asDate()) == 0)
            {
                return i;
            }
        }
        return -1;
    }

    /**
     * @param index
     *            The index of the column of the requested milestone.
     * @return The milestone set at given index.
     */
    public MilestoneTimeLineElement getMileStoneElementByIndex(int index)
    {
        if (this.milestoneElements != null && index < this.milestoneElements.size())
        {
            return this.milestoneElements.get(index);
        }
        else
        {
            return null;
        }
    }

    /**
     * Displays the given milestone inside the timeline.
     * 
     * @param milestone
     *            The milestone to add.
     * @return Time Line element containing the added milestone.
     */
    public MilestoneTimeLineElement addMilestone(MilestoneModelData milestone)
    {
        MilestoneTimeLineElement milestoneElement = this.getMilestoneElement(milestone);
        if (milestoneElement != null)
        {
            milestoneElement.setMilestone(milestone);
            milestoneElement.showMilestone();
        }

        return milestoneElement;
    }

    /**
     * Remove the given from the time line.
     * 
     * @param milestone
     *            The milestone to remove.
     * @return Time Line element that contained the removed milestone.
     */
    public MilestoneTimeLineElement removeMilestone(MilestoneModelData milestone)
    {
        MilestoneTimeLineElement milestoneElement = this.getMilestoneElement(milestone);
        milestoneElement.removeMilestone();

        return milestoneElement;
    }

    /**
     * @param milestone
     *            The milestone of which time line element is requested.
     * @return The element of the time line that contains the milestone.
     */
    public MilestoneTimeLineElement getMilestoneElement(MilestoneModelData milestone)
    {
        for (MilestoneTimeLineElement element : milestoneElements)
        {
            DateWrapper elementDate = element.getDate();
            elementDate = elementDate.clearTime();
            DateWrapper milestoneDate = new DateWrapper(milestone.getDate());
            milestoneDate = milestoneDate.clearTime();

            if (milestoneDate.getTime() == elementDate.getTime())
            {
                return element;
            }
        }
        return null;
    }

    /**
     * Build the whole time line based on the date wrapper list created in time line constructor.
     */
    protected void buildMonthView()
    {
        DateWrapper date = planningSettings.getStartDate();

        this.lastWeekIndex = 0;
        if (date != null)
            this.lastMonth = date.getMonth();

        for (int i = 0; i < wrappers.length; i++)
        {
            wrappers[i] = date;
            createDayHeader(date, DAY_ROW_INDEX, i, i == wrappers.length - 1);

            if (date.getDayInWeek() == 0 || i == wrappers.length - 1)
            {
                int colSpan = i - lastWeekIndex + 1;
                createWeekHeader(date, WEEK_ROW_INDEX, colSpan, i == wrappers.length - 1);
                lastWeekIndex = i + 1;
            }
            if (date.getMonth() != lastMonth || i == wrappers.length - 1)
            {
                int colSpan = i - lastMonthIndex;
                if (date.getMonth() == lastMonth)
                {
                    colSpan++;
                }

                createMonthHeader(wrappers[i - 1], 0, colSpan, i == wrappers.length - 1);
                lastMonthIndex = i;
                lastMonth = date.getMonth();
            }

            date = date.addDays(1);
        }
    }

    /**
     * Create a header for a given day of the time line.
     * 
     * @param date
     *            The date for which a day header is created.
     * @param row
     *            The row of used for day.
     * @param col
     *            The column where the day header will be displayed.
     * @param isLast
     *            True if it is the last day of the time line (needed for styling purposes).
     */
    protected void createDayHeader(DateWrapper date, int row, int col, boolean isLast)
    {
        LayoutContainer daysName = new LayoutContainer(new FlowLayout());
        LayoutContainer daysNumber = new LayoutContainer(new FlowLayout());

        if (BasePlanningWidget.getZoomMode() != EZoomMode.SMALL)
        {
            daysName.addText(days[date.getDay()]);
            daysNumber.addText("" + date.getDate());
        }

        daysName.addStyleName("unselectable");
        daysNumber.addStyleName("unselectable");
        daysName.setSize(BasePlanningWidget.getCellWidth(), BasePlanningWidget.CELL_HEIGHT);
        daysNumber.setSize(BasePlanningWidget.getCellWidth(), BasePlanningWidget.CELL_HEIGHT);

        if (BasePlanningWidget.getZoomMode() == EZoomMode.BIG)
        {
            CSSUtils.setFontSize(daysName, "16px");
            CSSUtils.setFontSize(daysNumber, "16px");
        }

        String style = "";
        if (DatetimeUtil.isWeekend(date))
        {
            style = TIMELINE_WEEKEND_DAY_STYLE;
        }
        else
        {
            style = TIMELINE_WEEK_DAY_STYLE;
        }

        daysName.setStyleName(TIMELINE_DAY_HEADER_STYLE + " " + style);
        daysNumber.setStyleName(TIMELINE_DAY_HEADER_STYLE + " " + style);

        this.add(daysName, new AbsoluteData(col * BasePlanningWidget.getCellWidth(), row
                * BasePlanningWidget.CELL_HEIGHT));

        this.add(daysNumber, new AbsoluteData(col * BasePlanningWidget.getCellWidth(), (row + 1)
                * BasePlanningWidget.CELL_HEIGHT));

        MilestoneTimeLineElement milestoneElement = new MilestoneTimeLineElement(date);
        milestoneElement.setStyleName(TIMELINE_DAY_HEADER_STYLE + " " + style);
        CSSUtils.setCursor(milestoneElement, CSSUtils.CURSOR_POINTER);

        this.add(milestoneElement, new AbsoluteData(col * BasePlanningWidget.getCellWidth(), (row + 2)
                * BasePlanningWidget.CELL_HEIGHT));
        this.milestoneElements.add(milestoneElement);

        if (isLast)
        {
            CSSUtils.set1pxBorderRight(milestoneElement, "#99BBE8");

            daysName.setStyleAttribute("border-right", "1px solid #99BBE8");
            daysNumber.setStyleAttribute("border-right", "1px solid #99BBE8");
        }
    }

    /**
     * Creates header for a week of the time line.
     * 
     * @param dateWrapper
     *            The beginning date of the week.
     * @param row
     *            The row where week headers are displayed.
     * @param colSpan
     *            The number of columns used by the week header.
     * @param isLast
     *            True if it is the last week header (for styling).
     */
    protected void createWeekHeader(DateWrapper dateWrapper, int row, int colSpan, boolean isLast)
    {
        LayoutContainer weekNumberContainer = new LayoutContainer(new FlowLayout());
        int weekNumber = Hd3dDateUtil.getWeekOfYear(dateWrapper, 1);
        int left = lastWeekIndex * BasePlanningWidget.getCellWidth();

        weekNumberContainer.addStyleName("unselectable");
        weekNumberContainer.addText("" + weekNumber);
        weekNumberContainer.setSize(colSpan * BasePlanningWidget.getCellWidth(), BasePlanningWidget.CELL_HEIGHT);
        weekNumberContainer.setStyleName("timeline-border-color timeline-week-header");
        if (isLast)
        {
            weekNumberContainer.setStyleName("timeline-border-right-color timeline-week-header");
        }
        else
        {
            weekNumberContainer.setStyleName("timeline-border-color timeline-week-header");
        }

        if (BasePlanningWidget.getZoomMode() == EZoomMode.BIG)
            CSSUtils.setFontSize(weekNumberContainer, "16px");

        this.add(weekNumberContainer, new AbsoluteData(left, WEEK_ROW_INDEX * BasePlanningWidget.CELL_HEIGHT));
    }

    /**
     * Creates header for a month of the time line.
     * 
     * @param dateWrapper
     *            The beginning date of the month.
     * @param row
     *            The row where month headers are displayed.
     * @param colSpan
     *            The number of columns used by the month header.
     * @param isLast
     *            True if it is the last week header (for styling).
     */
    protected void createMonthHeader(DateWrapper dateWrapper, int row, int colSpan, boolean isLast)
    {
        LayoutContainer monthName = new LayoutContainer(new FlowLayout());

        monthName.addStyleName("unselectable");
        monthName.addText(months[dateWrapper.getMonth()] + " " + dateWrapper.getFullYear());
        monthName.setSize(colSpan * BasePlanningWidget.getCellWidth(), BasePlanningWidget.CELL_HEIGHT);
        if (isLast)
        {
            monthName.setStyleName("timeline-border-right-color timeline-month-header");
        }
        else
        {
            monthName.setStyleName("timeline-border-color timeline-month-header");
        }

        if (BasePlanningWidget.getZoomMode() == EZoomMode.BIG)
        {
            CSSUtils.setFontSize(monthName, "16px");
            CSSUtils.setFontWeight(monthName, CSSUtils.FONT_BOLD);
        }

        int left = lastMonthIndex * BasePlanningWidget.getCellWidth();
        this.add(monthName, new AbsoluteData(left, MONTH_ROW_INDEX * BasePlanningWidget.CELL_HEIGHT));
    }

}
