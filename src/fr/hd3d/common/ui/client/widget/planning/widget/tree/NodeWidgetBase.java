package fr.hd3d.common.ui.client.widget.planning.widget.tree;

import com.google.gwt.user.client.ui.ComplexPanel;



public abstract class NodeWidgetBase extends ComplexPanel
{
    abstract public void removeItem(TreeItemWidget widget);

    abstract public void addItem(TreeItemWidget taskGroupItem);

    abstract public void setLabel(String label);

}
