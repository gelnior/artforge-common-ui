package fr.hd3d.common.ui.client.widget.planning.util;

import java.util.Date;

import com.extjs.gxt.ui.client.util.DateWrapper;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.datepicker.client.CalendarUtil;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.service.parameter.Constraint;


public class Hd3dDateUtil
{
    public static final long ONE_MIN = 60;
    public static final long ONE_HOUR = 60 * ONE_MIN;
    public static final long ONE_WORKING_DAY = 8 * ONE_HOUR;
    public static final long ONE_DAY = 24 * ONE_HOUR;
    public static final long MILLISECOND_IN_ONE_DAY = 86400000;
    public static final long MILLISECOND_IN_ONE_WEEK = 7 * MILLISECOND_IN_ONE_DAY;

    public static int daysBetween(Date startDate, Date endDate)
    {
        if (startDate == null || endDate == null)
        {
            return 0;
        }
        return CalendarUtil.getDaysBetween(startDate, endDate) + 1;
    }

    public static String longToDHM(long time)
    {
        String dhm = "";
        int nbDays = (int) (time / (ONE_WORKING_DAY));
        int nbHours = ((int) (time / (ONE_HOUR))) - (nbDays * 24);
        int nbMin = ((int) (time / (ONE_MIN))) - (nbDays * 24 * 60) - (nbHours * 60);
        if (nbDays > 0)
        {
            dhm += nbDays + "d ";
        }
        if (nbHours > 0)
        {
            dhm += nbHours + "h ";
        }
        if (nbMin > 0)
        {
            dhm += nbMin + "min";
        }
        return dhm;
    }

    public static String longToHM(long time)
    {
        String dhm = "";

        int nbHours = ((int) (time / (ONE_HOUR)));
        int nbMin = ((int) (time / (ONE_MIN))) - (nbHours * 60);

        if (nbHours > 0)
        {
            dhm += nbHours + "h ";
        }
        if (nbMin > 0)
        {
            dhm += nbMin + "min";
        }
        return dhm;
    }

    public static int returnWeekNumber(long daysBetween, Date startDate)
    {
        if (startDate != null)
        {
            DateWrapper wrapper = new DateWrapper(startDate);
            if (daysBetween > 7)
            {
                return (int) (daysBetween / 7);
            }
            else
            {
                if (wrapper.getDay() + daysBetween > 6)
                {
                    return 1;
                }
            }
        }
        return 0;
    }

    public static String getTasksYearConstraint()
    {
        Constraint constraint;
        DateTimeFormat dateTimeFormat = DateTimeFormat.getFormat("dd/MM/yyyy HH:mm");
        DateWrapper actualDate = new DateWrapper(new Date());
        DateWrapper firstDate = new DateWrapper(actualDate.getFullYear(), 0, 1);
        DateWrapper endDate = new DateWrapper(actualDate.getFullYear() + 1, 0, 1);
        constraint = new Constraint(EConstraintOperator.btw, "startDate", dateTimeFormat.format(firstDate.asDate()),
                dateTimeFormat.format(endDate.asDate()));
        return constraint.toString();
    }

    public static String dateToString(Date date)
    {
        DateTimeFormat dateTimeFormat = DateTimeFormat.getFormat("dd/MM/yyyy");
        String dateString = dateTimeFormat.format(date);
        return dateString;
    }

    public static int getWeekOfYear(DateWrapper firstDateOfWeek, int startWeekDay)
    {
        if (startWeekDay == 1)
        {
            return getWeekEu(firstDateOfWeek);
        }
        else
        {
            return getWeek(firstDateOfWeek);
        }
    }

    private static int getWeekEu(DateWrapper dateWrapper)
    {
        DateWrapper newYear = new DateWrapper(dateWrapper.getFullYear(), 0, 1);
        int day = newYear.getDay() - 1;
        day = (day >= 0 ? day : day + 7);
        double daynum = Math.floor((dateWrapper.getTime() - newYear.getTime()) / MILLISECOND_IN_ONE_DAY) + 1;
        double weekNumber = 0;
        if (day < 4)
        {
            weekNumber = Math.floor((daynum + day - 1) / 7) + 1;
            if (weekNumber > 52)
            {
                newYear = new DateWrapper(dateWrapper.getFullYear() + 1, 0, 1);
                day = newYear.getDay() - 1;
                day = day >= 0 ? day : day + 7;
                weekNumber = day < 4 ? 1 : 53;
            }
        }
        else
        {
            weekNumber = Math.floor((daynum + day - 1) / 7);
            if (weekNumber == 0)
            {
                newYear = new DateWrapper(dateWrapper.getFullYear() - 1, 0, 1);
                day = newYear.getDay() - 1;
                day = day >= 0 ? day : day + 7;
                if (day == 3 || (isLeapYear(newYear) && day == 2))
                {
                    weekNumber = 53;
                }
                else
                {
                    weekNumber = 52;
                }
            }
        }
        return (int) weekNumber;
    }

    private static boolean isLeapYear(DateWrapper dateWrapper)
    {
        return new DateWrapper(dateWrapper.getFullYear(), 1, 29).getDate() == 29;
    }

    private static int getWeek(DateWrapper dateWrapper)
    {
        int year = y2k(dateWrapper.getFullYear());
        DateWrapper newYear = new DateWrapper(year, 0, 1);
        int offset = 7 + 1 - newYear.getDay();
        if (offset == 8)
            offset = 1;
        long dayNum = ((dateWrapper.getTime() - newYear.getTime()) / MILLISECOND_IN_ONE_DAY) + 1;
        double weekNumber = Math.floor((dayNum - offset + 7) / 7);
        if (weekNumber == 0)
        {
            newYear = newYear.addYears(-1);
            int prevOffset = 7 + 1 - newYear.getDay();
            if (prevOffset == 2 || prevOffset == 8)
                weekNumber = 53;
            else
                weekNumber = 52;
        }
        return (int) weekNumber;
    }

    private static int y2k(int number)
    {
        return (number < 1000) ? number + 1900 : number;
    }

}
