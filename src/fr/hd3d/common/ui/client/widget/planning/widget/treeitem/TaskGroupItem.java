package fr.hd3d.common.ui.client.widget.planning.widget.treeitem;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.menu.Menu;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.modeldata.task.TaskGroupModelData;
import fr.hd3d.common.ui.client.widget.EasyMenu;
import fr.hd3d.common.ui.client.widget.planning.EPlanningDisplayMode;
import fr.hd3d.common.ui.client.widget.planning.events.PlanningCommonEvents;
import fr.hd3d.common.ui.client.widget.planning.user.view.BasePlanningWidget;
import fr.hd3d.common.ui.client.widget.planning.user.widget.SpanWidget;
import fr.hd3d.common.ui.client.widget.planning.widget.tree.TreeItemWidget;


public class TaskGroupItem extends PlanningTreeItem
{
    public static final String TREE_ITEM_TEXT = "my-tree-item-text";
    public static final String TREE_JOINT = "my-tree-joint";
    public static final String TREE_JOINT_OVER = "my-tree-joint-over";
    public static final String TREE_OPEN = "my-tree-open";
    public static final String TREE_CLOSE = "my-tree-close";

    private static EasyMenu contextMenu = null;

    private TaskGroupModelData taskGroup;

    private SpanWidget label;
    private Image userAddIcon;
    private final SpanWidget userAddIconSubstitute = new SpanWidget();
    private FocusPanel openCloseImage;

    private final PlanningTreeItem children = new PlanningTreeItem();
    private TaskGroupItemContent nodePanel;

    private final List<PlanningTreeItem> childItems = new ArrayList<PlanningTreeItem>();
    private final List<Long> workerdIds = new ArrayList<Long>();

    private boolean isOpen;
    private boolean isFirstExpand = true;

    public TaskGroupItem(TaskGroupModelData taskGroup)
    {
        super();

        this.setTaskGroup(taskGroup);
        this.add(this.createNodeLabel(taskGroup.getName()));
        this.initChildren();
        this.addStyleName("task-group-item");
    }

    public List<PlanningTreeItem> getChildItems()
    {
        return childItems;
    }

    public TaskGroupModelData getTaskGroup()
    {
        return taskGroup;
    }

    public boolean isLoaded()
    {
        return !this.isFirstExpand;
    }

    public void setFirstExpand(boolean isFirstExpand)
    {
        this.isFirstExpand = isFirstExpand;
    }

    public void expand()
    {
        this.isOpen = true;
        this.changeState();
    }

    public void collapse()
    {
        this.isOpen = false;
        this.changeState();
    }

    public boolean isOpen()
    {
        return isOpen;
    }

    public void setOpen(boolean isOpen)
    {
        this.isOpen = isOpen;
        if (isOpen)
        {
            openCloseImage.setStyleName(TREE_OPEN);
            AppEvent event = new AppEvent(PlanningCommonEvents.TASK_GROUP_OPENED, this.taskGroup);
            event.setData(PlanningCommonEvents.FIRST_EXPAND_EVENT_VAR_NAME, isFirstExpand);
            EventDispatcher.forwardEvent(event);
            if (isFirstExpand)
            {
                openCloseImage.setStyleName("x-status-busy");
                isFirstExpand = false;
            }
        }
        else
        {
            EventDispatcher.forwardEvent(PlanningCommonEvents.TASK_GROUP_CLOSED, this.taskGroup);
            openCloseImage.setStyleName(TREE_CLOSE);
        }
        changeState();
    }

    @Override
    public void addItem(TreeItemWidget Item)
    {
        super.addItem(Item);
        if (Item instanceof TaskGroupItem)
        {
            TaskGroupItem groupItem = (TaskGroupItem) Item;
            addItem(groupItem);
        }
        else if (Item instanceof ExtraLineItem)
        {
            ExtraLineItem extraLineItem = (ExtraLineItem) Item;
            addItem(extraLineItem);
        }
    }

    // private TaskGroupItem lastChild;

    public void addItem(TaskGroupItem taskGroupItem)
    {
        this.setParentAndTree(taskGroupItem);
        taskGroupItem.setRoot(false);
        this.children.add(taskGroupItem);
        this.childItems.add(taskGroupItem);

        // if (lastChild != null)
        // {
        // DOM.setStyleAttribute(lastChild.getElement(), "borderBottom", "1px solid black");
        // }
        // DOM.setStyleAttribute(taskGroupItem.getElement(), "borderBottom", "none");
        DOM.setStyleAttribute(taskGroupItem.getElement(), "borderTop", "2px solid black");
        DOM.setStyleAttribute(taskGroupItem.getElement(), "paddingLeft", "10px");
        // lastChild = taskGroupItem;
    }

    public void addItem(ExtraLineItem extraLine)
    {
        // if (lastChild != null)
        // DOM.setStyleAttribute(lastChild.getElement(), "borderBottom", "2px solid black");

        setParentAndTree(extraLine);
        Long personId = extraLine.getExtraLine().getPersonID();
        if (personId != null)
        {
            this.workerdIds.add(extraLine.getExtraLine().getPersonID());
        }

        DOM.setStyleAttribute(extraLine.getElement(), "paddingLeft", "10px");
        this.children.add(extraLine);
        this.childItems.add(extraLine);

        String iconPath = "";
        if (taskGroup.getType() == EPlanningDisplayMode.TASK_TYPE)
            iconPath = Hd3dImages.PERSON_PATH;
        if (taskGroup.getType() == EPlanningDisplayMode.CONSTITUENT)
            iconPath = Hd3dImages.CONSTITUENT_PATH;
        if (taskGroup.getType() == EPlanningDisplayMode.SHOT)
            iconPath = Hd3dImages.SHOT_PATH;
        extraLine.setIcon(iconPath);

        this.showChildren();
    }

    public void removeItem(TaskGroupItem taskGroupItem)
    {
        if (this.children.getWidgetCount() > 0)
        {
            this.children.remove(taskGroupItem);
            this.childItems.remove(taskGroupItem);
        }
    }

    @Override
    public void setSelected(boolean selected)
    {
        if (selected)
        {
            EventDispatcher.forwardEvent(PlanningCommonEvents.TASK_GROUP_SELECTED, this.taskGroup);
            this.nodePanel.addStyleName("x-ftree2-selected");
        }
        else
        {
            this.nodePanel.removeStyleName("x-ftree2-selected");
        }
    }

    public void changeState()
    {
        this.children.setVisible(isOpen);
    }

    public int getChildrenCount()
    {
        return this.getChildren().size();
    }

    @Override
    public void removeItem(TreeItemWidget widget)
    {
        if (widget instanceof TaskGroupItem)
        {
            TaskGroupItem groupItem = (TaskGroupItem) widget;
            removeItem(groupItem);
        }
    }

    @Override
    public Menu getContextMenu()
    {
        if (contextMenu == null)
        {
            createContextMenu();
        }
        ;
        return contextMenu;
    }

    private void createContextMenu()
    {
        contextMenu = new EasyMenu();

        if (this.taskGroup.getType() == EPlanningDisplayMode.TASK_TYPE)
        {
            contextMenu.addItem("Auto resize group", PlanningCommonEvents.TASK_GROUP_SYNCHRONIZE_BOUNDS_CLICKED,
                    Hd3dImages.getTaskGroupAutoResizeIcon());
            contextMenu.addItem("Load worked days", PlanningCommonEvents.TASK_GROUP_LOAD_WORKED_DAYS_CLICKED,
                    Hd3dImages.getListDetail());
        }
        contextMenu.addItem("Refresh", PlanningCommonEvents.TASK_GROUP_REFRESH_CLICKED, Hd3dImages.getRefreshIcon());
    }

    @Override
    public void setLabel(String label)
    {
        this.label.setText(label);
    }

    public List<Long> getWorkerdIds()
    {
        return workerdIds;
    }

    public void refreshChildren()
    {
        for (Widget widget : children.getChildren())
        {
            children.remove(widget);
        }
        this.setFirstExpand(true);
        this.setOpen(true);
    }

    public void clearChildren()
    {
        for (PlanningTreeItem taskGroupItem : this.childItems)
        {
            this.children.remove(taskGroupItem);
        }
        this.childItems.clear();
    }

    private void setTaskGroup(TaskGroupModelData taskGroup)
    {
        this.taskGroup = taskGroup;
        DOM.setStyleAttribute(this.getElement(), "backgroundColor", taskGroup.getColor());
    }

    private void initChildren()
    {
        this.children.setVisible(false);
        this.add(children);
    }

    private void showChildren()
    {
        if (this.children.getWidgetCount() == 1)
        {
            setOpen(true);
        }
    }

    private void setParentAndTree(PlanningTreeItem widget)
    {
        widget.setParentWidget(this);
        widget.setTree(getTree());
    }

    private FlexTable createNodeLabel(String name)
    {
        nodePanel = new TaskGroupItemContent();
        nodePanel.setCellPadding(0);
        nodePanel.setCellSpacing(0);
        nodePanel.setBorderWidth(0);
        openCloseImage = new FocusPanel();

        label = new SpanWidget();
        label.setText(name);
        label.setStyleName("task-group-item");

        String iconPath = "";
        if (taskGroup.getType() == EPlanningDisplayMode.TASK_TYPE)
            iconPath = Hd3dImages.TASK_TYPE_COLOR_PATH;
        if (taskGroup.getType() == EPlanningDisplayMode.CONSTITUENT)
            iconPath = Hd3dImages.CATEGORY_PATH;
        if (taskGroup.getType() == EPlanningDisplayMode.SHOT)
            iconPath = Hd3dImages.SEQUENCE_PATH;
        Image nodeIcon = new Image(iconPath);
        nodeIcon.setStyleName("task-group-item-icon");

        // build ui
        nodePanel.setWidget(0, 0, openCloseImage);
        nodePanel.setWidget(0, 1, nodeIcon);
        nodePanel.setWidget(0, 2, label);

        DOM.setStyleAttribute(label.getElement(), "cursor", "default");
        if (this.taskGroup.getType() == EPlanningDisplayMode.TASK_TYPE)
        {
            userAddIcon = new Image("images/explorateur/add.png");
            DOM.setStyleAttribute(userAddIcon.getElement(), "cursor", "pointer");
            userAddIcon.addClickHandler(new ClickHandler() {
                public void onClick(ClickEvent event)
                {
                    if (getTree().getSelectedItem() != null)
                    {
                        getTree().getSelectedItem().setSelected(false);
                    }
                    setSelected(true);
                    getTree().setSelectedItem(TaskGroupItem.this);
                    EventDispatcher.forwardEvent(PlanningCommonEvents.TASK_GROUP_ADD_WORKERS_CLICKED, taskGroup);
                }
            });
            nodePanel.setWidget(0, 3, userAddIcon);
            userAddIconSubstitute.setText("&nbsp;&nbsp;&nbsp;&nbsp;");
            userAddIconSubstitute.setWidth("100px");
            nodePanel.setWidget(0, 4, userAddIconSubstitute);
            this.userAddIcon.setVisible(false);
            this.userAddIcon.setStyleName("task-group-item-icon");
        }

        // css
        nodePanel.setHeight(BasePlanningWidget.CELL_HEIGHT + "px");
        nodePanel.getFlexCellFormatter().setStyleName(0, 1, TREE_ITEM_TEXT);
        nodePanel.getFlexCellFormatter().setStyleName(0, 0, TREE_JOINT);
        openCloseImage.setStyleName(TREE_CLOSE);

        // listeners
        openCloseImage.addMouseOverHandler(new MouseOverHandler() {

            public void onMouseOver(MouseOverEvent event)
            {
                nodePanel.getFlexCellFormatter().addStyleName(0, 0, TREE_JOINT_OVER);
            }
        });
        openCloseImage.addMouseOutHandler(new MouseOutHandler() {
            public void onMouseOut(MouseOutEvent event)
            {
                nodePanel.getFlexCellFormatter().removeStyleName(0, 0, TREE_JOINT_OVER);
            }
        });
        openCloseImage.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event)
            {
                isOpen = !isOpen;
                setOpen(isOpen);
            }
        });

        nodePanel.addMouseOverHandler(new MouseOverHandler() {
            public void onMouseOver(MouseOverEvent event)
            {
                if (taskGroup.getType() == EPlanningDisplayMode.TASK_TYPE)
                {
                    userAddIcon.setVisible(true);
                    userAddIconSubstitute.setVisible(false);
                }
            }
        });
        nodePanel.addMouseOutHandler(new MouseOutHandler() {
            public void onMouseOut(MouseOutEvent event)
            {
                if (taskGroup.getType() == EPlanningDisplayMode.TASK_TYPE)
                {
                    userAddIcon.setVisible(false);
                    userAddIconSubstitute.setVisible(true);
                }
            }
        });
        return nodePanel;
    }
}
