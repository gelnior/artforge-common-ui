package fr.hd3d.common.ui.client.widget.planning.user.event;

import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;


public class UserTasksEvent extends AppEvent
{

    private PersonModelData personModelData;
    private List<TaskModelData> tasks;

    public UserTasksEvent(EventType type, PersonModelData personModelData, List<TaskModelData> tasks)
    {
        super(type);
        this.personModelData = personModelData;
        this.tasks = tasks;
    }

    public PersonModelData getPersonModelData()
    {
        return personModelData;
    }

    public void setPersonModelData(PersonModelData personModelData)
    {
        this.personModelData = personModelData;
    }

    public List<TaskModelData> getTasks()
    {
        return tasks;
    }

    public void setTasks(List<TaskModelData> tasks)
    {
        this.tasks = tasks;
    }
}
