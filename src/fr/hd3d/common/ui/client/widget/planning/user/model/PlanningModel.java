package fr.hd3d.common.ui.client.widget.planning.user.model;

import com.extjs.gxt.ui.client.store.ListStore;

import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;


public class PlanningModel extends MainModel
{
    private ListStore<TaskModelData> tasksStore;

    public void setTasksStore(ListStore<TaskModelData> taskListStore)
    {
        this.tasksStore = taskListStore;
        this.tasksStore.setMonitorChanges(true);
    }

    public ListStore<TaskModelData> getTasksStore()
    {
        return tasksStore;
    }

}
