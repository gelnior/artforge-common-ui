package fr.hd3d.common.ui.client.widget.planning.user.model;

public enum EPlanningDisplayMode
{
    TASK_TYPE, CONSTITUENT, SHOT, ALL, TASK_TYPE_SHOT;
}
