package fr.hd3d.common.ui.client.widget.planning.widget.listener;

import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.Widget;


public interface ExtendedClickListener
{

    void onRightClick(Widget sender, Event event);

    void onClick(Event event);

}
