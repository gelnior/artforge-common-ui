package fr.hd3d.common.ui.client.widget.planning.util;

public class ColorUtil
{

    public static final String COLOR_PREFIX = "#";
    private static final String RGB_PREFIX = "rgb(";
    private static final String HEXADECIMAL_VALUES = "0123456789ABCDEF";

    public static String RGBtoHex(String r, String g, String b)
    {
        String rHex = toHex(r);
        String gHex = toHex(g);
        String bHex = toHex(b);
        return "#" + rHex + gHex + bHex;
    }

    /**
     * 
     * @param rgbString
     *            Must be formatted like rgb(R,G,B) or R,G,B
     * @return Color at hexadecimal format : #RGB.
     */
    public static String RGBtoHex(String rgbString)
    {
        rgbString = rgbString.replace(RGB_PREFIX, "");
        rgbString = rgbString.replace(")", "");
        String[] rgbComponent = rgbString.split(",");
        return RGBtoHex(rgbComponent[0].trim(), rgbComponent[1].trim(), rgbComponent[2].trim());
    }

    private static String toHex(String rgbComponent)
    {
        if (rgbComponent == null)
        {
            return "00";
        }
        int rgbValue = Integer.parseInt(rgbComponent);
        if (rgbValue == 0)
        {
            return "00";
        }
        // we verify if rgbValue is between 0 and 255
        rgbValue = Math.max(0, rgbValue);
        rgbValue = Math.min(rgbValue, 255);

        char firstElement = HEXADECIMAL_VALUES.charAt((rgbValue - rgbValue % 16) / 16);
        char secondElement = HEXADECIMAL_VALUES.charAt(rgbValue % 16);

        return "" + firstElement + secondElement;
    }

}
