package fr.hd3d.common.ui.client.widget.planning.user.event;

import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.modeldata.resource.AbsenceModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;


public class UserAbscencesEvent extends AppEvent
{

    private PersonModelData personModelData;
    private List<AbsenceModelData> abscences;

    public UserAbscencesEvent(EventType type, PersonModelData personModelData, List<AbsenceModelData> abscences)
    {
        super(type);
        this.personModelData = personModelData;
        this.abscences = abscences;
    }

    public PersonModelData getPersonModelData()
    {
        return personModelData;
    }

    public void setPersonModelData(PersonModelData personModelData)
    {
        this.personModelData = personModelData;
    }

    public List<AbsenceModelData> getAbscences()
    {
        return abscences;
    }

    public void setAbscences(List<AbsenceModelData> abscences)
    {
        this.abscences = abscences;
    }
}
