package fr.hd3d.common.ui.client.widget.planning.widget.treeitem;

import com.extjs.gxt.ui.client.widget.menu.Menu;
import com.google.gwt.event.dom.client.HasMouseUpHandlers;
import com.google.gwt.event.dom.client.MouseUpEvent;
import com.google.gwt.event.dom.client.MouseUpHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.ui.WidgetCollection;

import fr.hd3d.common.ui.client.widget.planning.widget.listener.ExtendedClickListener;
import fr.hd3d.common.ui.client.widget.planning.widget.listener.SourceExtendedClickListener;
import fr.hd3d.common.ui.client.widget.planning.widget.tree.BasePlanningTree;
import fr.hd3d.common.ui.client.widget.planning.widget.tree.TreeItemWidget;


public class PlanningTreeItem extends TreeItemWidget implements HasMouseUpHandlers, SourceExtendedClickListener
{
    private HandlerRegistration mouseUp;
    private ExtendedClickListener extendedClickListener;
    private BasePlanningTree tree;

    private PlanningTreeItem parentWidget;

    public PlanningTreeItem()
    {
        this.sinkEvents(Event.ONMOUSEUP | Event.ONCONTEXTMENU);
        this.initListeners();
    }

    public BasePlanningTree getTree()
    {
        return tree;
    }

    public void setTree(BasePlanningTree tree)
    {
        this.tree = tree;
    }

    private void initListeners()
    {
        this.addClickListener(new ExtendedClickListener() {

            public void onClick(Event event)
            {
                event.stopPropagation();
                select();
            }

            public void onRightClick(Widget sender, Event event)
            {
                if (tree != null)
                {
                    if (tree.getSelectedItem() != null)
                    {
                        tree.getSelectedItem().setSelected(false);
                    }
                    setSelected(true);
                    tree.setSelectedItem((PlanningTreeItem) sender);
                    showContextMenu(event);
                }
            }
        });
    }

    public PlanningTreeItem getParentWidget()
    {
        return parentWidget;
    }

    public void setParentWidget(PlanningTreeItem parentWidget)
    {
        this.parentWidget = parentWidget;
    }

    @Override
    public void add(Widget child)
    {
        super.add(child, getElement());
    }

    @Override
    public void onBrowserEvent(Event event)
    {
        // This will stop the event from being propagated to parent elements.
        event.stopPropagation();
        event.preventDefault();
        switch (DOM.eventGetType(event))
        {
            case Event.ONMOUSEUP:

                if (DOM.eventGetButton(event) == Event.BUTTON_LEFT)
                {
                    if (extendedClickListener != null)
                    {
                        extendedClickListener.onClick(event);
                    }
                }
                if (DOM.eventGetButton(event) == Event.BUTTON_RIGHT)
                {
                    if (extendedClickListener != null)
                    {
                        extendedClickListener.onRightClick(this, event);
                    }
                }
                break;
            case Event.ONCONTEXTMENU:
                if (extendedClickListener != null)
                {
                    extendedClickListener.onRightClick(this, event);
                }
                break;
        }
    }

    @Override
    protected void insert(Widget child, Element container, int beforeIndex)
    {
        super.insert(child, getElement(), beforeIndex, true);
    }

    public void setRoot(boolean isRoot)
    {
        if (isRoot)
        {
            DOM.setStyleAttribute(this.getElement(), "borderBottom", "2px solid black");
            // this.addStyleName("rootItem");
        }
        else
        {
            // DOM.setStyleAttribute(this.getElement(), "borderTop", "2px solid black");
            // this.addStyleName("childItem");
        }
    }

    public void addClickListener(ExtendedClickListener listener)
    {
        extendedClickListener = listener;
    }

    public void removeClickListener(ExtendedClickListener listener)
    {
        extendedClickListener = null;
    }

    @Override
    public WidgetCollection getChildren()
    {
        return super.getChildren();
    }

    public HandlerRegistration addMouseUpHandler(MouseUpHandler handler)
    {
        this.mouseUp = addDomHandler(handler, MouseUpEvent.getType());
        return this.mouseUp;
    }

    public void removeMouseUpHandler()
    {
        if (this.mouseUp != null)
        {
            this.mouseUp.removeHandler();
        }
    }

    public void select()
    {
        if (tree != null)
        {
            if (tree.getSelectedItem() != null)
            {
                if (tree.getSelectedItem() == this)
                {
                    tree.clearSelection();
                }
                else
                {
                    tree.getSelectedItem().setSelected(false);
                    setSelected(true);
                    tree.setSelectedItem(this);
                }
            }
            else
            {
                setSelected(true);
                tree.setSelectedItem(this);
            }
        }
    }

    public void showContextMenu(Event event)
    {
        if (getContextMenu() != null)
        {
            int clientX = event.getClientX();
            int clientY = event.getClientY();
            getContextMenu().showAt(clientX, clientY);
        }
    }

    public Menu getContextMenu()
    {
        return null;
    }

    public void setSelected(boolean selected)
    {}

    @Override
    public void setLabel(String label)
    {}
}
