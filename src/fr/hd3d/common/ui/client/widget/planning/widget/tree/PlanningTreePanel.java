package fr.hd3d.common.ui.client.widget.planning.widget.tree;

import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.layout.FitData;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.toolbar.ToolBar;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.DeferredCommand;
import com.google.gwt.user.client.ui.ScrollPanel;

import fr.hd3d.common.ui.client.widget.planning.user.view.BasePlanningWidget;
import fr.hd3d.common.ui.client.widget.planning.user.widget.TimeLine;


public class PlanningTreePanel extends ContentPanel
{
    private final ScrollPanel scrollPanel;
    private final ToolBar toolBar = new ToolBar();

    public PlanningTreePanel()
    {
        this.setStyles();
        this.setToolbars();

        this.scrollPanel = new ScrollPanel();
        this.scrollPanel.setStyleName("scrollPanel");

        DOM.setStyleAttribute(scrollPanel.getElement(), "overflowX", "hidden");
        DOM.setStyleAttribute(scrollPanel.getElement(), "overflowY", "hidden");

        this.add(scrollPanel, new FitData());
        this.toolBar.setStyleAttribute("padding", "5px");
    }

    public void setTimeLine(final TimeLine absoluteTimeLine)
    {
        DeferredCommand.addCommand(new Command() {
            public void execute()
            {
                int height = absoluteTimeLine.getBounds(false).height;
                height = height - getHeader().getOffsetHeight();
                getToolBar().setHeight(height + 1);
                toolBar.setVisible(true);
            }
        });
    }

    public ToolBar getToolBar()
    {
        return toolBar;
    }

    public ScrollPanel getScrollPanel()
    {
        return scrollPanel;
    }

    private void setStyles()
    {
        this.setWidth(400);
        this.setLayout(new FitLayout());
        this.setFrame(false);
        this.setHeaderVisible(false);
        this.setLayoutOnChange(true);
    }

    private void setToolbars()
    {
        ToolBar bottomToolBar = new ToolBar();
        bottomToolBar.setHeight(BasePlanningWidget.DEFAULT_SCROLL_HEIGHT);
        this.setBottomComponent(bottomToolBar);

        this.toolBar.addStyleName("planning-widget-toolbar");
        this.toolBar.setVisible(false);
        this.setTopComponent(toolBar);
    }

}
