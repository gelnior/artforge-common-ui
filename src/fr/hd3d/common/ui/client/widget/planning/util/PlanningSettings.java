package fr.hd3d.common.ui.client.widget.planning.util;

import java.util.Date;

import com.extjs.gxt.ui.client.util.DateWrapper;


/**
 * PlanningSettings contains planning settings like planning start date and end date. By default start date = first
 * january of current year and end date = first april of current year.
 * 
 * @author HD3D
 */
public class PlanningSettings
{
    private DateWrapper startDate = null;
    private DateWrapper endDate = null;

    /**
     * Constructor start date and end date are set to default.
     */
    public PlanningSettings()
    {
        DateWrapper currentDateWrapper = new DateWrapper(new Date());
        DateWrapper firstDateWrapper = new DateWrapper(currentDateWrapper.getFullYear(), 1, 1);

        setStartDate(firstDateWrapper);
        setEndDate(null);
    }

    /**
     * Constructor set as start date and end date,
     * 
     * @param startDate
     *            Start date of planning.
     * @param endDate
     *            End date of planning.
     */
    public PlanningSettings(Date startDate, Date endDate)
    {
        if (startDate != null)
            setStartDate(new DateWrapper(startDate));
        if (endDate != null)
            setEndDate(new DateWrapper(endDate));
    }

    public DateWrapper getStartDate()
    {
        return startDate;
    }

    public void setStartDate(DateWrapper firstDateWrapper)
    {
        this.startDate = firstDateWrapper;
    }

    public DateWrapper getEndDate()
    {
        return endDate;
    }

    public void setEndDate(DateWrapper endDate)
    {
        this.endDate = endDate;
    }

    /**
     * @return Number of days, between start date and end date.
     */
    public int getNbDays()
    {
        if (endDate != null && startDate != null)
        {
            Integer nbDaysBetween = Hd3dDateUtil.daysBetween(startDate.asDate(), endDate.asDate());
            if (nbDaysBetween == null)
                return 0;
            return nbDaysBetween;
        }
        else
        {
            return 0;
        }
    }
}
