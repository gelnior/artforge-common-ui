package fr.hd3d.common.ui.client.widget.planning.user.widget;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.util.DateWrapper;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;
import com.extjs.gxt.ui.client.widget.menu.Menu;
import com.extjs.gxt.ui.client.widget.menu.MenuItem;
import com.extjs.gxt.ui.client.widget.tips.ToolTipConfig;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.resource.MilestoneModelData;
import fr.hd3d.common.ui.client.util.CSSUtils;
import fr.hd3d.common.ui.client.widget.planning.events.PlanningCommonEvents;
import fr.hd3d.common.ui.client.widget.planning.user.view.BasePlanningWidget;


/**
 * MilestoneTimeLineElement allows user to add milestone to time line by displaying milestone color in the bottom of the
 * milestone day in the time line. If no milestone is linked to the MilestoneTimeLineElement, default color is
 * displayed. Each MilestoneTimeLineElement allows milestone creation and edition.
 * 
 * @author HD3D
 */
public class MilestoneTimeLineElement extends LayoutContainer
{
    /** Class name used to display that mouse is over time line element. */
    public static final String OTHER_ELEMENT_OVER_STYLE_NAME = "userRepere-selected";

    /** Milestone linked to mile stone time line element. */
    private MilestoneModelData milestone;

    /** Date corresponding to the position of this element in the time line. */
    private final DateWrapper date;

    /**
     * Constructor : set styles, listeners and context menu.
     * 
     * @param date
     *            Date corresponding to the position of this element in the time line.
     */
    public MilestoneTimeLineElement(DateWrapper date)
    {
        super(new FlowLayout());

        this.date = date;

        this.setSize(BasePlanningWidget.getCellWidth(), BasePlanningWidget.CELL_HEIGHT);

        this.addListeners();
        this.createContextMenu();
    }

    /**
     * @return Milestone set in this element. It returns null, if no milestone is set.
     */
    public MilestoneModelData getMilestone()
    {
        return milestone;
    }

    /**
     * Set a milestone in the milestone time line element.
     * 
     * @param milestone
     *            The milestone to set.
     */
    public void setMilestone(MilestoneModelData milestone)
    {
        this.milestone = milestone;
    }

    /**
     * @return Date linked to milestone time line element.
     */
    public DateWrapper getDate()
    {
        return this.date;
    }

    /**
     * Display milestone : fill the background with milestone color. If milestone has no color, the background is filled
     * with red.
     */
    public void showMilestone()
    {
        String color = milestone.getColor();
        if (color == null)
            color = MilestoneModelData.DEFAULT_COLOR;

        CSSUtils.setBackground(this, color);
        this.layout(true);

        ToolTipConfig toolTipConfig = new ToolTipConfig(milestone.getTitle(), milestone.getDescription());
        this.setToolTip(toolTipConfig);
    }

    /**
     * Remove set milestone and change background color with default color.
     */
    public void removeMilestone()
    {
        this.milestone = null;

        if (date.getDayInWeek() == 0 || date.getDayInWeek() == 6)
        {
            CSSUtils.setBackground(this, TimeLine.DAY_COLOR);
        }
        else
        {
            CSSUtils.setBackground(this, TimeLine.WEEKEND_COLOR);
        }
    }

    /**
     * Create context menu : allow to add a milestone, modify current mileston or delete current milestone.
     */
    private void createContextMenu()
    {
        Menu menu = new Menu();

        final MenuItem addMilestoneItem = new MenuItem("Add milestone");
        final MenuItem editMilestoneItem = new MenuItem("Edit milestone");
        final MenuItem deleteMilestoneItem = new MenuItem("Delete milestone");

        menu.add(addMilestoneItem);
        menu.add(editMilestoneItem);
        menu.add(deleteMilestoneItem);

        addMilestoneItem.addListener(Events.Select, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                EventDispatcher.forwardEvent(PlanningCommonEvents.ADD_MILESTONE_CLICKED, date);
            }
        });

        editMilestoneItem.addListener(Events.Select, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                if (milestone != null)
                {
                    EventDispatcher.forwardEvent(PlanningCommonEvents.UPDATE_MILESTONE_CLICKED, milestone);
                }
            }
        });

        deleteMilestoneItem.addListener(Events.Select, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                if (milestone != null)
                {
                    EventDispatcher.forwardEvent(PlanningCommonEvents.DELETE_MILESTONE_CLICKED, milestone);
                }
            }
        });

        menu.addListener(Events.BeforeShow, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                if (milestone != null)
                {
                    if (milestone != null)
                    {
                        addMilestoneItem.setEnabled(false);
                        editMilestoneItem.setEnabled(true);
                        deleteMilestoneItem.setEnabled(true);
                    }
                    else
                    {
                        addMilestoneItem.setEnabled(true);
                        editMilestoneItem.setEnabled(false);
                        deleteMilestoneItem.setEnabled(false);
                    }
                }
            }
        });

        deleteMilestoneItem.setEnabled(false);
        editMilestoneItem.setEnabled(false);
        this.setContextMenu(menu);
    }

    /**
     * Add listeners to highlight milestone time line element when mouse is over it.
     */
    private void addListeners()
    {
        this.sinkEvents(Events.OnMouseOver.getEventCode() | Events.OnMouseOut.getEventCode());
        this.addListener(Events.OnMouseOver, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                LayoutContainer container = (LayoutContainer) be.getSource();
                if (container != null)
                {
                    container.addStyleName(MilestoneTimeLineElement.OTHER_ELEMENT_OVER_STYLE_NAME);
                }
            };
        });
        this.addListener(Events.OnMouseOut, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                LayoutContainer container = (LayoutContainer) be.getSource();
                if (container != null)
                {
                    container.removeStyleName(MilestoneTimeLineElement.OTHER_ELEMENT_OVER_STYLE_NAME);
                }
            };
        });
    }

}
