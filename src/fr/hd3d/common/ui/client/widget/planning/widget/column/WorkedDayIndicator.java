package fr.hd3d.common.ui.client.widget.planning.widget.column;

import java.util.Date;

import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.google.gwt.user.datepicker.client.CalendarUtil;

import fr.hd3d.common.ui.client.modeldata.task.TaskActivityModelData;
import fr.hd3d.common.ui.client.util.CSSUtils;
import fr.hd3d.common.ui.client.widget.planning.user.view.BasePlanningWidget;


/**
 * Small widget to set on a planning bar that displays which days has been worked for this time period.
 * 
 * @author HD3D
 * 
 */
public class WorkedDayIndicator extends LayoutContainer
{

    private final Date date;

    /**
     * Constructor.
     * 
     * @param taskStartDate
     *            The start date of the task on which widget will be displayed.
     * @param activity
     *            The activity to display.
     */
    public WorkedDayIndicator(Date taskStartDate, TaskActivityModelData activity)
    {
        this.date = activity.getDayDate();

        this.setSize(BasePlanningWidget.getCellWidth() - 2 + "px", BasePlanningWidget.CELL_HEIGHT - 2 + "px");

        this.resetPosition(taskStartDate);

        CSSUtils.setPosition(this, CSSUtils.POSITION_ABSOLUTE);
        CSSUtils.setOpacity(this, "0.4");
        CSSUtils.setFontSize(this, "12px");
        CSSUtils.setFloat(this, CSSUtils.LEFT);
        this.setStyleAttribute("background-image", "url(images/planning/worked_day_bg.png)");
    }

    public void resetPosition(Date taskStartDate)
    {
        int xDay = CalendarUtil.getDaysBetween(taskStartDate, date);
        int left = xDay * BasePlanningWidget.getCellWidth();
        CSSUtils.setLeft(this, left + "px");
    }
}
