package fr.hd3d.common.ui.client.widget.planning.widget.bar;

import java.util.Date;

import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.util.DateWrapper;
import com.extjs.gxt.ui.client.util.Util;
import com.extjs.gxt.ui.client.widget.Html;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.Text;
import com.google.gwt.user.client.DOM;

import fr.hd3d.common.ui.client.modeldata.DurationModelData;
import fr.hd3d.common.ui.client.util.CSSUtils;
import fr.hd3d.common.ui.client.widget.planning.EZoomMode;
import fr.hd3d.common.ui.client.widget.planning.user.view.BasePlanningWidget;
import fr.hd3d.common.ui.client.widget.planning.user.widget.TimeLine;
import fr.hd3d.common.ui.client.widget.planning.util.ColorUtil;
import fr.hd3d.common.ui.client.widget.planning.util.Hd3dDateUtil;


/**
 * Base class used to display bar in planning. Each planning bar is link to a duration object, it means an object with a
 * start and an end date.
 * 
 * @author HD3D
 */
public class PlanningBar extends LayoutContainer
{
    public static final int BORDER_WIDTH = 3;
    public static final int DEFAULT_DURATION = 5;

    public static final String STYLE_NAME = "task-Label";

    /** Object represented by planning bar. */
    protected DurationModelData timeObject;
    /** Planning time line. */
    protected TimeLine timeLine;
    /** True if bar is currently selected. */
    protected Boolean selected = false;

    /** Left position (needed to minimize DOM requests). */
    protected Integer left;
    /** Object width (needed to minimize DOM requests). */
    protected Integer width;

    protected Text label = new Text();

    /**
     * Constructor
     * 
     * @param timeLine
     *            Planning time line.
     * @param timeObject
     *            Time object represented by planning bar.
     * @param borderColor
     *            Border color of the planning bar.
     * @param bgColor
     *            Background color of the planning bar.
     */
    public PlanningBar(TimeLine timeLine, DurationModelData timeObject, String borderColor, String bgColor)
    {
        this.timeLine = timeLine;
        this.timeObject = timeObject;

        this.setStyles(bgColor, "#000");// borderColor);
        this.initPosition();

        this.addListener(Events.OnMouseDown, new Listener<ComponentEvent>() {
            public void handleEvent(ComponentEvent ce)
            {
                onMouseDown(ce);
            }
        });
        this.addStyleName("unselectable");
        this.label.addStyleName("unselectable");
        // this.label.addListener(Events.Select, new Listener<ComponentEvent>() {
        // public void handleEvent(ComponentEvent ce)
        // {
        // ce.cancelBubble();
        // ce.setCancelled(true);
        // ce.preventDefault();
        // }
        // });
    }

    protected void onMouseDown(ComponentEvent ce)
    {
        ce.cancelBubble();
        ce.setCancelled(true);
        ce.preventDefault();
    }

    /**
     * @return Time object represented by planning bar.
     */
    public DurationModelData getTimeObject()
    {
        return timeObject;
    }

    /**
     * Set time object represented by planning bar.
     * 
     * @param timeObject
     *            The time object to set.
     */
    public void setTimeObject(DurationModelData timeObject)
    {
        this.timeObject = timeObject;
    }

    /**
     * @return true if planning bar is selected.
     */
    public boolean isSelected()
    {
        return selected;
    }

    /**
     * @param selected
     *            Set planning bar as selected.
     */
    public void setSelected(Boolean selected)
    {
        this.selected = selected;
    }

    /**
     * @return Bar left coordinate.
     */
    public int getLeft()
    {
        if (left == null)
        {
            String left = DOM.getStyleAttribute(this.getElement(), "left");
            if (!Util.isEmptyString(left))
            {
                left = left.replaceAll("px", "");
                return Integer.parseInt(left);
            }
        }
        else
            return left;
        return 0;
    }

    /**
     * Set bar left coordinate.
     * 
     * @param left
     *            Value to set as left coordinate..
     */
    public void setLeft(int left)
    {
        this.left = left;
        this.setStyleAttribute("left", left + "px");
    }

    /** @return Bar top coordinate. */
    public int getTop()
    {
        String top = DOM.getStyleAttribute(this.getElement(), "top");
        if (!Util.isEmptyString(top))
        {
            top = top.replaceAll("px", "");
            return Integer.parseInt(top);
        }
        return 0;
    }

    /**
     * Set bar top coordinate.
     * 
     * @param top
     *            Value to set as top coordinate...
     */
    public void setTop(int top)
    {
        this.setStyleAttribute("top", top + "px");
    }

    /**
     * Set bar position by changing its left and top properties.
     * 
     * @param x
     *            The value to set on left property.
     * @param y
     *            The value to set on top property.
     */
    public void setPositionSimply(int x, int y)
    {
        setLeft(x);
        setTop(y);
    }

    /** @return Bar label. */
    public Html getHeaderText()
    {
        return null;
        // return this.labelText;
    }

    @Override
    public void setSize(int width, int height)
    {
        super.setSize(width, height);

        this.setHeaderWidth(width);
        this.width = width;
    }

    /**
     * @return bar width in pixel.
     */
    @Override
    public int getWidth()
    {
        if (this.width != null)
            return width;
        return super.getWidth();
    }

    /**
     * Set header width.
     * 
     * @param widthInPixel
     *            The width to set (in pixel).
     */
    public void setHeaderWidth(int widthInPixel)
    {
        // this.labelText.setWidth(widthInPixel);
    }

    /**
     * @return Planning bar time object start date.
     */
    public Date getStartDate()
    {
        return timeObject.getStartDate();
    }

    /**
     * Set planning bar time object start date.
     * 
     * @param newStartDate
     *            Date to set.
     */
    public void setStartDate(Date newStartDate)
    {
        this.timeObject.setStartDate(newStartDate);
    }

    /**
     * Add <i>nbDays</i> to planning bar time object start date.
     * 
     * @param nbDays
     *            Number of days to add.
     */
    public void addDaysToStartDate(int nbDays)
    {
        if (this.timeObject.getStartDate() != null)
        {
            DateWrapper startDateWrapper = new DateWrapper(this.timeObject.getStartDate());
            startDateWrapper = startDateWrapper.addDays(nbDays);
            if (this.timeObject.getStartDate().compareTo(startDateWrapper.asDate()) != 0)
            {
                this.timeObject.setStartDate(startDateWrapper.asDate());
            }
            this.resetSize();
        }
    }

    /**
     * @return Planning bar time object end date.
     */
    public Date getEndDate()
    {
        return timeObject.getEndDate();
    }

    /**
     * Set planning bar time object end date.
     * 
     * @param newEndDate
     *            Date to set.
     */
    public void setEndDate(Date newEndDate)
    {
        this.timeObject.setEndDate(newEndDate);
    }

    /**
     * Add <i>nbDays</i> to planning bar time object end date.
     * 
     * @param nbDays
     *            Number of days to add.
     */
    public void addDaysToEndDate(int nbDays)
    {
        if (this.timeObject.getEndDate() != null)
        {
            DateWrapper endDateWrapper = new DateWrapper(this.timeObject.getEndDate());
            endDateWrapper = endDateWrapper.addDays(nbDays);
            if (this.timeObject.getEndDate().compareTo(endDateWrapper.asDate()) != 0)
            {
                this.timeObject.setEndDate(endDateWrapper.asDate());
            }
            this.resetSize();
        }
    }

    /**
     * Set initial position in current planning, depending on start date.
     */
    protected void initPosition()
    {
        if (timeLine.getWrappers() != null && timeLine.getWrappers().length > 0)
        {
            this.resetSize();
            this.setLeftPosition(timeObject.getStartDate());
        }
    }

    /**
     * Set left position depending on given date (for each day add cell width to left coordinate).
     * 
     * @param date
     *            The date to set.
     */
    protected void setLeftPosition(Date date)
    {
        DateWrapper firstDate = timeLine.getWrappers()[0];
        if (date != null)
        {
            int daysBetweenViewAndDate = Hd3dDateUtil.daysBetween(firstDate.asDate(), date) - 1;
            this.setPositionSimply((daysBetweenViewAndDate * BasePlanningWidget.getCellWidth()) + 1, 0);
        }
    }

    /**
     * Set bar on right position depending on startDate and endDate.
     * 
     * @param startDate
     *            Start date of corresponding time object.
     * @param endDate
     *            End date of corresponding time object.
     */
    public void setPosition(Date startDate, Date endDate)
    {
        this.setStartDate(startDate);
        this.setEndDate(endDate);

        this.resetPosition();
    }

    /**
     * Reset bar position based on time object start and end date.
     */
    public void resetPosition()
    {
        if (timeLine.getWrappers() != null && timeLine.getWrappers().length > 0)
        {
            DateWrapper firstDate = timeLine.getWrappers()[0];
            this.resetSize();
            if (timeObject.getStartDate() != null)
            {
                int daysBetweenViewAndDate = Hd3dDateUtil.daysBetween(firstDate.asDate(), timeObject.getStartDate()) - 1;
                this.setLeft((daysBetweenViewAndDate * BasePlanningWidget.getCellWidth()) + 1);
            }
        }
    }

    /**
     * Reset bar size depending on time object start date and end date.
     */
    public void resetSize()
    {
        int nbDaysBetween = DEFAULT_DURATION;

        Date startDate = timeObject.getStartDate();
        Date endDate = timeObject.getEndDate();

        if (startDate != null && endDate != null && (startDate.before(endDate) || startDate.equals(endDate)))
            nbDaysBetween = Hd3dDateUtil.daysBetween(startDate, endDate);
        this.setSize(nbDaysBetween * BasePlanningWidget.getCellWidth() - 1, BasePlanningWidget.CELL_HEIGHT);
    }

    /**
     * Add a dirty marker to the bar (red triangle).
     */
    public void setDirtyMarker()
    {
        addStyleName(CSSUtils.MODIFIED_STYLE);
    }

    /**
     * Change background and border color.
     * 
     * @param bgColor
     *            The background color to set.
     * @param borderColor
     *            The border color to set.
     */
    protected void setStyles(String bgColor, String borderColor)
    {
        if (bgColor != null && !bgColor.startsWith(ColorUtil.COLOR_PREFIX))
            bgColor = ColorUtil.COLOR_PREFIX + bgColor;
        else if (bgColor == null)
            bgColor = CSSUtils.COLOR_WHITE;
        CSSUtils.setBackground(this, bgColor);

        // if (borderColor == null)
        this.setBorderColor("#000");
        // else
        // this.setBorderColor(borderColor);

        CSSUtils.setCursor(this, CSSUtils.CURSOR_POINTER);
        CSSUtils.setPosition(this, CSSUtils.POSITION_ABSOLUTE);
        CSSUtils.setZindex(this, 12);
        CSSUtils.setFontSize(this, "12px");
        CSSUtils.setOverflow(this, CSSUtils.HIDDEN);
        CSSUtils.setTextAlign(this, CSSUtils.CENTER);
    }

    /**
     * Change border color with given color and sets 2px border. Sets no border if borderColor is null.
     * 
     * @param borderColor
     *            The color of the border to set.
     */
    public void setBorderColor(String borderColor)
    {
        if (borderColor != null)
        {
            // if (!borderColor.startsWith(ColorUtil.COLOR_PREFIX))
            // borderColor = ColorUtil.COLOR_PREFIX + borderColor;
            // borderColor = "black";

            if (BasePlanningWidget.getZoomMode() == EZoomMode.SMALL)
                CSSUtils.set1pxBorder(this, borderColor);
            else
                CSSUtils.set2pxBorder(this, borderColor);
        }
        else
        {
            CSSUtils.setNoBorder(this);
        }
    }

    /**
     * Set label text inside text container. Add container, if container is not inside bar.
     * 
     * @param labelText
     *            The text to set as label.
     */
    public void setLabel(String labelText)
    {
        if (!this.getItems().contains(label))
        {
            this.add(label);
            CSSUtils.setPadding(label, "2px 0px 0px 0px");
        }
        this.label.setText(labelText);
    }
}
