package fr.hd3d.common.ui.client.widget.planning.widget.line;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.util.DateWrapper;
import com.extjs.gxt.ui.client.widget.LayoutContainer;

import fr.hd3d.common.ui.client.calendar.DatetimeUtil;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.util.CSSUtils;
import fr.hd3d.common.ui.client.widget.planning.EZoomMode;
import fr.hd3d.common.ui.client.widget.planning.user.view.BasePlanningWidget;
import fr.hd3d.common.ui.client.widget.planning.user.widget.TimeLine;
import fr.hd3d.common.ui.client.widget.planning.widget.bar.TaskGroupBar;
import fr.hd3d.common.ui.client.widget.planning.widget.treeitem.TaskGroupItem;


public class TaskGroupLine extends LayoutContainer
{
    private final LayoutContainer children;
    private final TaskGroupItem taskGroupItem;
    private TaskGroupBar taskGroupBar;

    private TaskGroupLine lastTaskGroupLineAdded;

    private final FastMap<ExtraLine> workerLines = new FastMap<ExtraLine>();

    public TaskGroupLine(TimeLine timeLine, TaskGroupItem groupItem)
    {
        super();

        this.taskGroupItem = groupItem;
        this.children = new LayoutContainer();

        this.setStyleAttribute("position", "relative");
        this.setStyleAttribute("cursor", "default");

        // This is for ie because ie have a minimum size for div component.
        CSSUtils.setFontSize(this, "1px");
        CSSUtils.setOverflow(this, CSSUtils.HIDDEN);
        CSSUtils.setZindex(this, 1);

        this.add(this.children);
        this.children.setVisible(false);
        this.children.setWidth(this.children.getWidth() - 1);

        this.createBackground(timeLine);
        this.setLayoutOnChange(false);
    }

    @Override
    public String toString()
    {
        return this.taskGroupItem.getTaskGroup().getName();
    }

    public TaskGroupBar getGroupBar()
    {
        return taskGroupBar;
    }

    public void setGroupBar(TaskGroupBar groupPanel)
    {
        this.taskGroupBar = groupPanel;
        this.insert(groupPanel, 0);
    }

    public void addChild(LayoutContainer line)
    {
        if (line instanceof ExtraLine)
        {
            ExtraLine extraLine = (ExtraLine) line;
            this.workerLines.put(extraLine.getPersonId().toString(), extraLine);
        }
        else if (line instanceof TaskGroupLine)
        {
            if (lastTaskGroupLineAdded != null)
            {
                lastTaskGroupLineAdded.setStyleAttribute("border-bottom", "none");
            }
            // CSSUtils.set2pxBorderBottom(line, CSSUtils.COLOR_BLACK);
            CSSUtils.set2pxBorderTop(line, CSSUtils.COLOR_BLACK);
            lastTaskGroupLineAdded = (TaskGroupLine) line;
        }
        children.add(line);
        children.layout();
    }

    public LayoutContainer getChildren()
    {
        return children;
    }

    public TaskGroupItem getTaskGroupItem()
    {
        return taskGroupItem;
    }

    public ExtraLine getExtraLine(Long workerID)
    {
        return this.workerLines.get(workerID.toString());
    }

    public List<TaskModelData> getTasks()
    {
        List<TaskModelData> tasks = new ArrayList<TaskModelData>();

        for (ExtraLine line : this.workerLines.values())
        {
            tasks.addAll(line.getTasks());
        }

        return tasks;
    }

    public void expand()
    {
        this.children.setVisible(true);
    }

    public void collapse()
    {
        this.children.setVisible(false);
    }

    public void clearChildren()
    {
        this.children.removeAll();
        this.workerLines.clear();
        this.layout();
    }

    public void layoutLines()
    {
        children.layout();
        for (ExtraLine line : workerLines.values())
        {
            line.layout();
        }
    }

    public void moveAllTasks(int nbDays)
    {
        for (ExtraLine extraLine : workerLines.values())
        {
            extraLine.moveAllTasks(nbDays);
        }
    }

    /**
     * Set modification indicator from task group bar contained inside this line.
     */
    public void setModificationIndicator()
    {
        this.taskGroupBar.addStyleName(CSSUtils.MODIFIED_STYLE);
    }

    /**
     * Remove modification indicator from task group bar contained inside this line.
     */
    public void removeModificationIndicator()
    {
        this.taskGroupBar.removeStyleName(CSSUtils.MODIFIED_STYLE);
    }

    private void createBackground(TimeLine timeLine)
    {
        if (timeLine.getWrappers() != null && timeLine.getWrappers().length > 0)
        {
            DateWrapper wrapper = timeLine.getWrappers()[0];

            String style = DatetimeUtil.getDayFromDayInWeek(wrapper.getDay()) + "-line";
            if (BasePlanningWidget.getZoomMode() == EZoomMode.BIG)
                style += "-big";
            else if (BasePlanningWidget.getZoomMode() == EZoomMode.SMALL)
                style += "-small";

            this.addStyleName(style);
        }
    }
}
