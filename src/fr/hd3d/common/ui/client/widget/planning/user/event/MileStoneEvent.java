package fr.hd3d.common.ui.client.widget.planning.user.event;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.util.DateWrapper;

import fr.hd3d.common.ui.client.modeldata.resource.MilestoneModelData;
import fr.hd3d.common.ui.client.modeldata.task.PlanningModelData;
import fr.hd3d.common.ui.client.widget.planning.user.widget.MilestoneTimeLineElement;


public class MileStoneEvent extends AppEvent
{
    public static final EventType ADD_OR_UPDATE_MILESTONE_CLICKED = new EventType();
    public static final EventType DELETE_MILESTONE_CLICKED = new EventType();

    private String title;
    private String description;
    private DateWrapper date;
    private PlanningModelData planningModelData;
    private MilestoneTimeLineElement milestoneTimeLineElement;
    private boolean toUpdateOrSave = false;
    private String color;

    public MileStoneEvent(EventType type, MilestoneTimeLineElement milestoneTimeLineElement, String title,
            String description, DateWrapper date)
    {
        this(type, milestoneTimeLineElement, date);
        this.title = title;
        this.description = description;
    }

    public MileStoneEvent(EventType type, MilestoneTimeLineElement milestoneTimeLineElement, DateWrapper date)
    {
        super(type, milestoneTimeLineElement);
        this.date = date;
        this.milestoneTimeLineElement = milestoneTimeLineElement;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public DateWrapper getDate()
    {
        return date;
    }

    public void setDate(DateWrapper date)
    {
        this.date = date;
    }

    public MilestoneTimeLineElement getMilestoneTimeLineElement()
    {
        return milestoneTimeLineElement;
    }

    public void setMilestoneTimeLineElement(MilestoneTimeLineElement milestoneTimeLineElement)
    {
        this.milestoneTimeLineElement = milestoneTimeLineElement;
    }

    public boolean isToUpdateOrSave()
    {
        return toUpdateOrSave;
    }

    public void setToUpdateOrSave(boolean toUpdateOrSave)
    {
        this.toUpdateOrSave = toUpdateOrSave;
    }

    public PlanningModelData getPlanningModelData()
    {
        return planningModelData;
    }

    public void setPlanningModelData(PlanningModelData planningModelData)
    {
        this.planningModelData = planningModelData;
    }

    public MilestoneModelData getMilestoneFromData()
    {
        MilestoneModelData milestone = new MilestoneModelData();
        milestone.setTitle(title);
        milestone.setDescription(description);
        milestone.setDate(date.asDate());
        milestone.setColor(color);

        return milestone;
    }

    public void setColor(String color)
    {
        this.color = color;
    }
}
