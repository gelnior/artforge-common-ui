package fr.hd3d.common.ui.client.widget.planning.user.event;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;


public class AbsoluteTimeLineEvent extends AppEvent
{

    private int pos;
    private String style;
    private Object source;

    public AbsoluteTimeLineEvent(EventType type, int pos, String style, Object source)
    {
        super(type);
        this.pos = pos;
        this.style = style;
        this.source = source;
    }

    public int getPos()
    {
        return pos;
    }

    public void setPos(int pos)
    {
        this.pos = pos;
    }

    public String getStyle()
    {
        return style;
    }

    public void setStyle(String style)
    {
        this.style = style;
    }

    public Object getSource()
    {
        return source;
    }

    public void setSource(Object source)
    {
        this.source = source;
    }
}
