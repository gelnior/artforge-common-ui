package fr.hd3d.common.ui.client.widget.planning.user.widget;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.DomEvent;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.ComplexPanel;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Widget;


public class SpanWidget extends ComplexPanel implements HasText, HasClickHandlers
{
    private HandlerRegistration clickHandlerRegistration;

    public SpanWidget()
    {
        setElement(DOM.createSpan());
        sinkEvents(Event.ONCLICK);
    }

    @Override
    public void add(Widget w)
    {
        super.add(w, getElement());
    }

    public void insert(Widget w, int beforeIndex)
    {
        super.insert(w, getElement(), beforeIndex, true);
    }

    public String getText()
    {
        return DOM.getInnerText(getElement());
    }

    public void setText(String text)
    {
        DOM.setInnerHTML(getElement(), (text == null) ? "" : text);
    }

    @Override
    public void onBrowserEvent(Event event)
    {
        switch (DOM.eventGetType(event))
        {
            case Event.ONCLICK:
                if (clickHandlerRegistration != null)
                {
                    DomEvent.fireNativeEvent(event, this);
                }
                break;
        }
    }

    public HandlerRegistration addClickHandler(ClickHandler handler)
    {
        this.clickHandlerRegistration = addDomHandler(handler, ClickEvent.getType());
        return this.clickHandlerRegistration;
    }

    public void removeClickHandler()
    {
        if (this.clickHandlerRegistration != null)
        {
            clickHandlerRegistration.removeHandler();
        }
    }

}
