package fr.hd3d.common.ui.client.widget.planning.widget.bar;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.core.XTemplate;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.js.JsUtil;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.util.DateWrapper;
import com.extjs.gxt.ui.client.util.Params;
import com.extjs.gxt.ui.client.util.Util;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.menu.MenuItem;
import com.extjs.gxt.ui.client.widget.tips.ToolTipConfig;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.datepicker.client.CalendarUtil;

import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.common.ui.client.calendar.DatetimeUtil;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.task.TaskActivityModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskStatusMap;
import fr.hd3d.common.ui.client.util.CSSUtils;
import fr.hd3d.common.ui.client.widget.EasyMenu;
import fr.hd3d.common.ui.client.widget.NoteStatusComboBox;
import fr.hd3d.common.ui.client.widget.planning.ETaskDisplayMode;
import fr.hd3d.common.ui.client.widget.planning.events.PlanningCommonEvents;
import fr.hd3d.common.ui.client.widget.planning.user.view.BasePlanningWidget;
import fr.hd3d.common.ui.client.widget.planning.user.widget.PlanningAbsolutePanel;
import fr.hd3d.common.ui.client.widget.planning.user.widget.TimeLine;
import fr.hd3d.common.ui.client.widget.planning.util.ColorUtil;
import fr.hd3d.common.ui.client.widget.planning.util.Hd3dDateUtil;
import fr.hd3d.common.ui.client.widget.planning.widget.bar.listener.StatusSelectioner;
import fr.hd3d.common.ui.client.widget.planning.widget.column.WorkedDayIndicator;


/**
 * Task panel corresponds to task label displayed in planning.
 * 
 * @author HD3D
 */
public class TaskBar extends BaseTaskBar
{
    private static final String OK_COLOR = "9F9";
    private static final String NOK_COLOR = "F99";
    private static final String NOK_AVERAGE_COLOR = "FC9";

    // protected List<TaskBar> additionalTasks = new ArrayList<TaskBar>();
    /** Days where activities were set. It display a little square covering the whole day for this task bar. */
    protected List<WorkedDayIndicator> days = new ArrayList<WorkedDayIndicator>();

    /**
     * @param timeLine
     *            Planning time line.
     * @param task
     *            Task represented by task bar.
     * @param label
     *            Bar label.
     * @param boundaryPanel
     *            Planning panel on which bar is displayed.
     * @param mode
     *            Display mode (task type colored, status colored,late...);
     * @param readOnly
     *            True if task is in read only mode.
     * 
     * @return Created task bar.
     */
    public static TaskBar createInstance(TimeLine timeLine, TaskModelData task, String label,
            PlanningAbsolutePanel boundaryPanel, ETaskDisplayMode mode, boolean readOnly)
    {
        if (label == null)
        {
            label = task.getWorkObjectName();
        }
        TaskBar taskPanel = new TaskBar(timeLine, task, label);
        taskPanel.setBackgroundColor(mode);
        taskPanel.refreshToolTip();

        return taskPanel;
    }

    /**
     * Constructor
     * 
     * @param timeLine
     *            Planning time line.
     * @param task
     *            Task represented by task bar.
     * @param title
     *            Task bar label.
     */
    public TaskBar(TimeLine timeLine, TaskModelData task, String title)
    {
        super(timeLine, task, null, task.getColor());
        this.initListeners();
        this.setContextMenu();
    }

    /**
     * @return Task represented by task bar.
     */
    public TaskModelData getTask()
    {
        return (TaskModelData) this.timeObject;
    }

    /**
     * @return Task worker ID.
     */
    public Long getWorkerId()
    {
        return this.getTask().getWorkerID();
    }

    /**
     * Set task worker ID in task object.
     * 
     * @param workerId
     *            The ID to set.
     */
    public void setWorkerId(Long workerId)
    {
        this.getTask().setWorkerID(workerId);
    }

    // public List<TaskBar> getAdditionalTasks()
    // {
    // return this.additionalTasks;
    // }

    /**
     * Set background color depending on selected mode.<br>
     * TASK_TYPE => colored with task type color.<br>
     * STATUS => colored with task status color.<br>
     * ON_TIME => Red if task is in late, green either.
     * 
     * @param mode
     *            The mode selected to change background color.
     */
    public void setBackgroundColor(ETaskDisplayMode mode)
    {

        String taskColor = getTask().getColor();
        if (Util.isEmptyString(taskColor))
            taskColor = CSSUtils.COLOR_WHITE;

        if (mode == ETaskDisplayMode.TASK_TYPE)
        {
            this.setBackgroundColor(taskColor);
            this.setBorderColor("#000");
        }
        else if (mode == ETaskDisplayMode.STATUS)
        {
            if (getTask().getStatus() != null)
            {
                this.setBackgroundColor(TaskStatusMap.getColorForStatus(getTask().getStatus()));
            }
            else
            {
                this.setBackgroundColor("#FFF");
            }
            this.setBorderColor("#000");
            // this.setBorderColor(taskColor);
        }
        else if (mode == ETaskDisplayMode.ON_TIME)
        {
            ETaskStatus status = ETaskStatus.valueOf(getTask().getStatus());

            if ((status == ETaskStatus.NEW || status == ETaskStatus.STAND_BY || status == ETaskStatus.WORK_IN_PROGRESS)
                    && getTask().getStartDate() != null && getTask().getStartDate().before((new Date())))
            {
                if (getTask().getEndDate() != null && getTask().getEndDate().before((new Date())))
                    this.setBackgroundColor(NOK_COLOR);
                else
                    this.setBackgroundColor(NOK_AVERAGE_COLOR);
            }
            else if ((status != ETaskStatus.OK && status != ETaskStatus.CANCELLED && status != ETaskStatus.CLOSE)
                    && getTask().getEndDate() != null && getTask().getEndDate().before((new Date())))
            {
                this.setBackgroundColor(NOK_COLOR);
            }
            else
            {
                this.setBackgroundColor(OK_COLOR);
            }
            this.setBorderColor("#000");
            // this.setBorderColor(taskColor);
        }
    }

    /**
     * Change task bar background color with <i>color</i>.
     * 
     * @param color
     *            The color to set as background color.
     */
    public void setBackgroundColor(String color)
    {
        if (!color.startsWith(ColorUtil.COLOR_PREFIX))
            color = ColorUtil.COLOR_PREFIX + color;
        CSSUtils.setBackground(this, color);
    }

    /**
     * Update worker info from current extra line. If extra line has no worker data, nothing is done.
     */
    public void setWorkerFromExtraLine()
    {
        Long personID = extraLine.getExtraLineItem().getExtraLine().getPersonID();

        if (personID != null)
        {
            this.getTask().setWorkerID(personID);
            this.getTask().setWorkerName(extraLine.getExtraLineItem().getExtraLine().getName());
        }
    }

    /**
     * Add dirty marker to the bar (red triangle on top left).
     */
    public void setDirty()
    {
        this.addStyleName(CSSUtils.DIRTY_MARKER);
    }

    /**
     * Reload data displayed inside tool tip.
     */
    public void refreshToolTip()
    {
        ToolTipConfig toolTipConfig = getXTaskToolTip();
        this.setToolTip(toolTipConfig);
    }

    /**
     * Reset position and task bar task data with <i>task</i> task data.
     * 
     * @param task
     *            The task used to refresh task bar data.
     */
    public void refreshForTask(TaskModelData task)
    {
        this.getExtraLine().removeOccupation(this);
        this.getTask().setWorkerID(task.getWorkerID());
        this.getTask().setWorkerName(task.getWorkerName());

        this.setPosition(task.getStartDate(), task.getEndDate());

        int left = this.getLeft();
        this.getExtraLine().moveToRightLine(left, left + this.getWidth(), this, Boolean.FALSE);
        this.setDirtyMarker();
        this.refreshToolTip();
    }

    /**
     * Set task left position and width depending on his dates (actual start date and actual end date if they are set,
     * else start date and end date are used).
     */
    @Override
    public void resetPosition()
    {
        if (timeLine.getWrappers() != null && timeLine.getWrappers().length > 0)
        {
            DateWrapper firstDate = timeLine.getWrappers()[0];
            this.resetSize();

            TaskModelData task = this.getTask();

            if (!BasePlanningWidget.isPreviewMode() && task.getActualStartDate() != null)
            {
                int daysBetweenViewAndDate = Hd3dDateUtil.daysBetween(firstDate.asDate(), task.getActualStartDate()) - 1;
                this.setLeft((daysBetweenViewAndDate * BasePlanningWidget.getCellWidth()) + 1);
            }
            else if (task.getStartDate() != null)
            {
                int daysBetweenViewAndDate = Hd3dDateUtil.daysBetween(firstDate.asDate(), timeObject.getStartDate()) - 1;
                this.setLeft((daysBetweenViewAndDate * BasePlanningWidget.getCellWidth()) + 1);
            }
        }
    }

    /** Reset working day positions, inside task bar. */
    public void refreshActivities()
    {
        TaskModelData task = this.getTask();
        for (WorkedDayIndicator day : this.days)
        {
            if (!BasePlanningWidget.isPreviewMode() && task.getActualStartDate() != null)
                day.resetPosition(task.getActualStartDate());
            else
                day.resetPosition(this.getStartDate());
        }
    }

    /**
     * @return task duration in number of plain days. Day count round mode is "ceil".
     */
    public int getDayDuration()
    {
        Long duration = this.getTask().getDuration();
        if (duration != null)
        {
            float nbDaysFloat = duration / (float) DatetimeUtil.DAY_SECONDS;
            duration = (long) Math.ceil(nbDaysFloat);
        }
        else
            duration = 0L;
        return duration.intValue();
    }

    /**
     * Add a work day widget inside task bar (div element) corresponding to given activity.
     * 
     * @param activity
     *            The activity to add.
     */
    public void addWorkedDay(TaskActivityModelData activity)
    {
        WorkedDayIndicator day;
        if (!BasePlanningWidget.isPreviewMode() && this.timeObject.get(TaskModelData.ACTUAL_START_DATE_FIELD) != null)
            day = new WorkedDayIndicator((Date) this.timeObject.get(TaskModelData.ACTUAL_START_DATE_FIELD), activity);
        else
            day = new WorkedDayIndicator(this.getStartDate(), activity);

        this.days.add(day);
        this.insert(day, 0);
        this.layout();
        this.extraLine.layout();
    }

    /**
     * Add a work day widget inside task bar (div element) corresponding to given activity.
     */
    public void clearWorkedDay()
    {
        for (LayoutContainer day : this.days)
        {
            this.remove(day);
        }
        this.days.clear();
    }

    /**
     * @return Number of days that are not week-ends between start date and end date.
     */
    public int getOpenDays()
    {
        Date endDate = new DateWrapper(getEndDate()).addDays(1).asDate();
        int nbDays = CalendarUtil.getDaysBetween(getStartDate(), endDate);
        int nbWeekEndDays = DatetimeUtil.getNbWeekEndDayBetween(new DateWrapper(getStartDate()), new DateWrapper(
                getEndDate()));

        return nbDays - nbWeekEndDays;
    }

    /**
     * Reset bar size depending on time object start date and end date. If actual start date is set it is used as start
     * date. If actual end date is set and after end date, it is used as end date.
     */
    @Override
    public void resetSize()
    {
        int nbDaysBetween = DEFAULT_DURATION;
        TaskModelData task = this.getTask();

        Date startDate = null;
        Date endDate = null;
        Date actualStartDate = task.getActualStartDate();
        if (!BasePlanningWidget.isPreviewMode() && actualStartDate != null)
        {
            startDate = actualStartDate;

            Date actualEndDate = task.getActualEndDate();
            if (actualEndDate != null
                    && (this.getEndDate() == null || actualEndDate.after(this.getEndDate()) || task.isFinished()))
                endDate = actualEndDate;
            else
                endDate = this.timeObject.getEndDate();
        }
        else
        {
            startDate = this.timeObject.getStartDate();
            endDate = this.timeObject.getEndDate();
        }

        if (startDate != null && endDate != null && (startDate.before(endDate) || startDate.equals(endDate)))
            nbDaysBetween = Hd3dDateUtil.daysBetween(startDate, endDate);
        this.setSize(nbDaysBetween * BasePlanningWidget.getCellWidth() - 1, BasePlanningWidget.CELL_HEIGHT);
    }

    // public void addAdditionalBar(TaskBar taskPanel)
    // {
    // this.additionalTasks.add(taskPanel);
    // }

    // private void popAdditionalTasks()
    // {
    // while (additionalTasks.size() > 0)
    // {
    // TaskBar bar = additionalTasks.remove(0);
    //
    // this.extraLine.addTask(bar);
    // this.extraLine.moveToRightLine(this.getLeft(), this.getLeft() + this.getWidth(), bar, Boolean.TRUE);
    // }
    // this.refreshToolTip();
    // this.extraLine.layout();
    // }

    @Override
    public void addDaysToEndDate(int nbDays)
    {

        if (!BasePlanningWidget.isPreviewMode() && this.getTask().getActualEndDate() != null)
        {
            Date actualEndDate = this.getTask().getActualEndDate();
            if (actualEndDate != null && actualEndDate.after(this.getEndDate()))
            {
                DateWrapper endDateWrapper = new DateWrapper(actualEndDate);
                endDateWrapper = endDateWrapper.addDays(nbDays);
                this.timeObject.setEndDate(endDateWrapper.asDate());

                this.resetSize();
            }
            else
                super.addDaysToEndDate(nbDays);
        }
        else
            super.addDaysToEndDate(nbDays);
    }

    /**
     * Set initial position in current planning, depending on start date.
     */
    @Override
    protected void initPosition()
    {
        if (timeLine.getWrappers() != null && timeLine.getWrappers().length > 0)
        {
            this.resetSize();

            Date actualStartDate = getTask().getActualStartDate();
            if (!BasePlanningWidget.isPreviewMode() && actualStartDate != null)
                this.setLeftPosition(actualStartDate);
            else
                this.setLeftPosition(this.timeObject.getStartDate());
        }
    }

    protected void initListeners()
    {
        // this.addListener(Events.OnClick, new Listener<BaseEvent>() {
        // public void handleEvent(BaseEvent be)
        // {
        // onClick();
        // }
        // });
        // this.addListener(Events.OnDoubleClick, new Listener<BaseEvent>() {
        // public void handleEvent(BaseEvent be)
        // {
        // popAdditionalTasks();
        // }
        // });
    }

    private void setContextMenu()
    {
        EasyMenu menu = new EasyMenu();
        MenuItem item = new MenuItem("To do");
        item.addSelectionListener(new StatusSelectioner(this, ETaskStatus.TO_DO));
        menu.add(item);
        item = new MenuItem("Stand By");
        item.addSelectionListener(new StatusSelectioner(this, ETaskStatus.STAND_BY));
        menu.add(item);

        this.setContextMenu(menu);
    }

    private void deselect()
    {
//        this.selected = Boolean.FALSE;
//        this.removeStyleName("selected");
//        this.setBorderColor("#000");
    }

    private void select()
    {
//        this.selected = Boolean.TRUE;
//        this.addStyleName("selected");
//        this.setBorderColor("red");
    }

    protected Boolean selected = Boolean.FALSE;

    @Override
    protected void onMouseDown(ComponentEvent ce)
    {
//        if (!this.selected)
//        {
//            this.select();
//            AppEvent mvcEvent = new AppEvent(PlanningCommonEvents.TASK_SELECTED, this.getTask());
//            mvcEvent.setData("is-ctrl", Boolean.valueOf(ce.isControlKey()));
//            EventDispatcher.forwardEvent(mvcEvent);
//        }
//        else
//        {
//            this.deselect();
//            AppEvent mvcEvent = new AppEvent(PlanningCommonEvents.TASK_DESELECTED, this.getTask());
//            mvcEvent.setData("is-ctrl", Boolean.valueOf(ce.isControlKey()));
//            EventDispatcher.forwardEvent(mvcEvent);
//        }

        ce.cancelBubble();
        ce.setCancelled(true);
        ce.preventDefault();
    }

    // private void onClick()
    // {
    // if (selected)
    // {
    // this.selected = Boolean.FALSE;
    // this.removeStyleName("selected");
    // }
    // else
    // {
    // this.selected = Boolean.TRUE;
    // this.addStyleName("selected");
    // }
    // }

    /**
     * @return Tool tip corresponding to task.
     */
    private ToolTipConfig getXTaskToolTip()
    {
        XTemplate taskTemplate = XTemplate.create(getXTaskToolTipTemplate("Start date", "End date",
                "Actual Start date", "Actual End date", "Working period", "Activities Duration", "Duration", "Worker",
                "Project", "Status", "Task Type"));

        Params params = new Params();
        DateTimeFormat dateTimeFormat = DateTimeFormat.getFormat("dd/MM/yyyy");

        if (this.getTask().getStartDate() != null)
        {
            params.set("readableStartDate", dateTimeFormat.format(this.getTask().getStartDate()));
        }
        if (this.getTask().getEndDate() != null)
        {
            params.set("readableEndDate", dateTimeFormat.format(this.getTask().getEndDate()));
        }
        if (this.getTask().getActualStartDate() != null)
        {
            params.set("readableActualStartDate", dateTimeFormat.format(this.getTask().getActualStartDate()));
        }
        if (this.getTask().getActualEndDate() != null)
        {
            params.set("readableActualEndDate", dateTimeFormat.format(this.getTask().getActualEndDate()));
        }
        if (getTask().getStartDate() != null && getTask().getEndDate() != null)
        {
            long nbDays = Hd3dDateUtil.daysBetween(getTask().getStartDate(), getTask().getEndDate());
            nbDays *= Hd3dDateUtil.ONE_WORKING_DAY;
            params.set("effectiveDays", Hd3dDateUtil.longToDHM(nbDays));
        }
        if (getTask().getDuration() != null)
        {
            params.set("readableDuration", Hd3dDateUtil.longToDHM(this.getTask().getDuration()));
        }
        if (getTask().getTotalActivityDuration() != null)
        {
            params.set("readableActivityDuration", Hd3dDateUtil.longToDHM(this.getTask().getTotalActivityDuration()));
        }
        if (getTask().getWorkerName() != null)
        {
            params.set("readableWorkerName", getTask().getWorkerName());
        }
        if (getTask().getStatus() != null)
        {
            params.set("readableStatus", NoteStatusComboBox.getFullStatusMap().get(getTask().getStatus().toString()));
        }
        if (getTask().getTaskTypeName() != null)
        {
            params.set("readableTaskType", getTask().getTaskTypeName());
        }
        // if (additionalTasks.size() > 0)
        // {
        // params.set("nbAdditionalTasks", additionalTasks.size());
        // }
        String html = taskTemplate.applyTemplate(JsUtil.toJavaScriptObject(params.getMap()));
        String title = getTask().getWorkObjectParentsName() + getTask().getWorkObjectName();
        ToolTipConfig toolTipConfig = new ToolTipConfig(title, html);
        toolTipConfig.setShowDelay(200);
        toolTipConfig.setHideDelay(0);
        return toolTipConfig;
    }

    private native String getXTaskToolTipTemplate(String startDateLabel, String endDateLabel,
            String actualStartDateLabel, String actualEndDateLabel, String durationLabel, String elapsedLabel,
            String plannedDurationLabel, String workerLabel, String projectLabel, String statusLabel,
            String taskTypeLabel) /*-{
                                  return [
                                  '<br />',
                                  '<tpl if="typeof(readableStatus) !== &quot;undefined&quot;"><p>'+ statusLabel +': {readableStatus}</p></tpl>',
                                  '<tpl if="typeof(readableTaskType) !== &quot;undefined&quot;"><p>'+ taskTypeLabel +': {readableTaskType}</p></tpl>',
                                  '<tpl if="typeof(readableWorkerName) !== &quot;undefined&quot;"><p>'+ workerLabel +': {readableWorkerName}</p></tpl>',
                                  '<br />',
                                  '<tpl if="typeof(readableActualStartDate) !== &quot;undefined&quot;"><p>'+ actualStartDateLabel +': {readableActualStartDate}</p></tpl>',
                                  '<tpl if="typeof(readableActualEndDate) !== &quot;undefined&quot;"><p>'+ actualEndDateLabel +': {readableActualEndDate}</p></tpl>',     
                                  '<tpl if="typeof(readableStartDate) !== &quot;undefined&quot;"><p>'+ startDateLabel +': {readableStartDate}</p></tpl>',
                                  '<tpl if="typeof(readableEndDate) !== &quot;undefined&quot;"><p>'+ endDateLabel +': {readableEndDate}</p></tpl>',     
                                  '<br />',
                                  '<tpl if="typeof(readableActivityDuration) !== &quot;undefined&quot;"><p>'+ elapsedLabel +': {readableActivityDuration}</p></tpl>',
                                  '<tpl if="typeof(readableDuration) !== &quot;undefined&quot;"><p>'+ plannedDurationLabel +': {readableDuration}</p></tpl>'].join("");
                                  }-*/;

}
