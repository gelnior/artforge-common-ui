package fr.hd3d.common.ui.client.widget;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.widget.button.ToggleButton;
import com.google.gwt.user.client.ui.AbstractImagePrototype;

import fr.hd3d.common.ui.client.listener.ToggleButtonClickListener;


/**
 * The tool bar button already sets an icon and a listener that throws the event given in the constructor.
 * 
 * @author HD3D
 */
public class ToolBarToggleButton extends ToggleButton
{
    /**
     * Default Constructor.
     * 
     * @param iconStyle
     *            Icon at CCS format.
     * @param toolTip
     *            Tool tip to display.
     * @param eventType
     *            Event to forward on click.
     */
    public ToolBarToggleButton(String iconStyle, String toolTip, EventType eventType)
    {
        super();

        this.setStyle(iconStyle, toolTip);
        this.registerListener(eventType);
    }

    /**
     * Alternative Constructor.
     * 
     * @param icon
     *            Icon at GXT format.
     * @param toolTip
     *            Tool tip to display.
     * @param eventType
     *            Event to forward on click.
     */
    public ToolBarToggleButton(AbstractImagePrototype icon, String toolTip, EventType eventType)
    {
        super();

        this.setStyle(icon, toolTip);
        this.registerListener(eventType);
    }

    /**
     * Alternative Constructor.
     * 
     * @param iconStyle
     *            Icon at GXT format.
     * @param text
     *            Text to display.
     * @param toolTip
     *            Tool tip to display.
     * @param eventType
     *            Event to forward on click.
     */
    public ToolBarToggleButton(String iconStyle, String text, String toolTip, EventType eventType)
    {
        super();

        this.setText(text);
        this.setToolTip(toolTip);
        this.registerListener(eventType);
    }

    /**
     * Set buttons styles.
     * 
     * @param iconStyle
     *            Icon at GXT format.
     * @param toolTip
     *            Tool tip to display.
     */
    private void setStyle(AbstractImagePrototype icon, String toolTip)
    {
        this.setIcon(icon);
        this.setToolTip(toolTip);
    }

    /**
     * Register on tool item click event. When the button is clicked, it raises the <i>eventType</i> event.
     */
    protected void registerListener(EventType eventType)
    {
        if (eventType != null)
        {
            this.addSelectionListener(new ToggleButtonClickListener(eventType, this));
        }
    }

    /**
     * Sets icon and tool tip.
     */
    protected void setStyle(String iconStyle, String toolTip)
    {
        this.setIconStyle(iconStyle);
        this.setToolTip(toolTip);
    }
}
