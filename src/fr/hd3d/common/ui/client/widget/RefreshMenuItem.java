package fr.hd3d.common.ui.client.widget;

import com.extjs.gxt.ui.client.event.MenuEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.menu.MenuItem;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.service.store.BaseStore;


/**
 * Refresh Menu item automatically reload associated store when clicked.
 * 
 * @author HD3D
 * 
 * @param <M>
 */
public class RefreshMenuItem<M extends Hd3dModelData> extends MenuItem
{
    /** Constant strings to display : dialog messages, button label... */
    public static final CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);

    /** The store to refresh */
    private BaseStore<M> store;

    /**
     * Default constructor, set name, icon and selection listener.
     * 
     * @param store
     */
    public RefreshMenuItem(BaseStore<M> store)
    {
        super(COMMON_CONSTANTS.Refresh());

        this.store = store;

        this.setIcon(Hd3dImages.getRefreshIcon());
        this.addSelectionListener(new SelectionListener<MenuEvent>() {
            @Override
            public void componentSelected(MenuEvent ce)
            {
                onSelected();
            }
        });
    }

    /** @return Store reloaded by refresh item. */
    public BaseStore<M> getStore()
    {
        return store;
    }

    /**
     * Set store reloaded by refresh item.
     * 
     * @param store
     *            The store to set.
     */
    public void setStore(BaseStore<M> store)
    {
        this.store = store;
    }

    /**
     * When button is clicked service store associated is reloaded.
     */
    private void onSelected()
    {
        store.reload();
    }
}
