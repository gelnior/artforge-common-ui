package fr.hd3d.common.ui.client.widget;

import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.KeyListener;
import com.extjs.gxt.ui.client.event.SelectionChangedEvent;
import com.extjs.gxt.ui.client.event.SelectionChangedListener;
import com.extjs.gxt.ui.client.store.Store;
import com.extjs.gxt.ui.client.store.StoreFilter;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.user.client.Element;

import fr.hd3d.common.ui.client.config.CommonConfig;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.cookie.FavoriteCookie;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.util.KeyBoardUtils;


/**
 * A combo box containing the opened project list. Data can be initialized via setData method.
 */
public class ProjectCombobox extends SimpleProjectComboBox
{
    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    /**
     * GXT by default reload combo box when user click on it. To not reselect automatically the last selected sheet
     * (expected behavior when app asks for reloading), we use this variable to select last selected sheet only if
     * reloading comes from application and not from GXT.
     */
    private boolean firstLoad = true;

    /**
     * Set a filter and listener and filter on project combobox to allow user to filter projects by typing the beginning
     * of their name.
     */
    public ProjectCombobox()
    {
        super();
        this.store.addFilter(storeFilter);
        this.addListener(Events.KeyUp, new KeyListener() {
            @Override
            public void componentKeyUp(ComponentEvent event)
            {
                onKeyUp(event.getKeyCode());
            }
        });
    }

    /** When key is up, store is filtered. */
    protected void onKeyUp(int keyCode)
    {
        if (KeyBoardUtils.isTextKey(keyCode) || keyCode == KeyCodes.KEY_BACKSPACE)
        {
            this.getStore().applyFilters(RecordModelData.NAME_FIELD);
        }
    }

    /**
     * Select last project selected. The last project selected, is the one store in the favorite cookie in the variable
     * "current-project".
     */
    public void selectLastSelected()
    {
        String projectId = FavoriteCookie.getFavParameterValue(CommonConfig.COOKIE_VAR_PROJECT);

        if (projectId != null)
        {
            Long id = Long.parseLong(projectId);
            if (getValue() == null || getValue().getId().longValue() != id.longValue())
            {
                selectProject(id);
            }
        }
    }

    /**
     * When data are retrieved it selects the project given in the cookie.
     */
    @Override
    protected void onLoaderLoad()
    {
        super.onLoaderLoad();

        if (firstLoad)
        {
            this.selectLastSelected();
            firstLoad = false;
        }
        this.setStyleAttribute("overflow", "auto");
    }

    /**
     * Add a selection changed listener. When the selection is changed, it set the last selected project id value in the
     * favorite cookie.
     */
    @Override
    protected void setListener()
    {
        this.addSelectionChangedListener(new SelectionChangedListener<ProjectModelData>() {
            @Override
            public void selectionChanged(SelectionChangedEvent<ProjectModelData> se)
            {
                onSelectionChanged(se.getSelectedItem());
            }
        });
    }

    /**
     * When selection changes "last selected project" cookie value is updated.
     * 
     * @param project
     *            The project to save.
     */
    protected void onSelectionChanged(ProjectModelData project)
    {
        if (project != null)
        {
            Long id = project.getId();
            if (id != null)
            {
                FavoriteCookie.putFavValue(CommonConfig.COOKIE_VAR_PROJECT, id.toString());
            }
        }
    }

    /**
     * Set combo styles.
     */
    @Override
    protected void setStyles()
    {
        super.setStyles();
        this.setStyleAttribute("overflow", "auto");
        this.setEditable(true);
        this.setTemplate(getXTemplate());
    }

    /** Set bold font in text field used by combo box. */
    @Override
    protected void onRender(Element parent, int index)
    {
        super.onRender(parent, index);
        this.getInputEl().setElementAttribute("style", "font-weight: bold");
    }

    /** Filter used to filter project on their name. */
    protected StoreFilter<ProjectModelData> storeFilter = new StoreFilter<ProjectModelData>() {
        public boolean select(Store<ProjectModelData> store, ProjectModelData parent, ProjectModelData item,
                String property)
        {
            return onStoreFilter(store, parent, item, property);
        }
    };

    /**
     * @param store
     *            The store to filter.
     * @param parent
     *            useless
     * @param item
     *            The item to analyze.
     * @param property
     *            The property to use for filtering.
     * @return True if project contains in its name the raw value of the combo box (it means the text typed by user).
     */
    protected boolean onStoreFilter(Store<ProjectModelData> store, ProjectModelData parent, ProjectModelData item,
            String property)
    {
        String name = item.get(property);
        if (name == null)
            name = item.get(ProjectModelData.NAME_FIELD);

        return getRawValue() != null && name != null && name.toLowerCase().contains(getRawValue().toLowerCase());
    }

    /**
     * @return Combo box values display style.
     */
    public native String getXTemplate() /*-{
                                              return  [ 
                                              '<tpl for=".">', 
                                              '<div class="x-combo-list-item"> <span style="background-color:{color}; font-size: 13px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;<span style="font-weight: bold; font-size : 12px; text-transform: uppercase;">&nbsp;{name}&nbsp;&nbsp;</span> </div>', 
                                              '</tpl>' 
                                              ].join("");
                                              }-*/;

}
