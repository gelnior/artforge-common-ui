package fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.widget.button.ToggleButton;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;


public class ConstraintPanelToggle extends ToggleButton
{

    public ConstraintPanelToggle()
    {
        initButton();
    }

    private void initButton()
    {
        this.setIcon(Hd3dImages.getFilterIcon());
        this.setToolTip("Show/Hide constraint panel");

        this.addListener(Events.Select, new Listener<ButtonEvent>() {
            public void handleEvent(ButtonEvent be)
            {
                ToggleButton bu = (ToggleButton) be.getButton();
                if (bu.isPressed())
                {
                    EventDispatcher.forwardEvent(ExplorerEvents.SHOW_CONSTRAINT_PANEL);
                }
                else
                {
                    EventDispatcher.forwardEvent(ExplorerEvents.HIDE_CONSTRAINT_PANEL);
                }
            }
        });
    }
}
