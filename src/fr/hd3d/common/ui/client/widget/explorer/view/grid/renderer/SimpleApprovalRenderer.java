package fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer;

import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.GXT;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.util.Util;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.extjs.gxt.ui.client.widget.tips.QuickTip;

import fr.hd3d.common.ui.client.calendar.DateFormat;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.ExplorerColumnConfig;


public class SimpleApprovalRenderer implements GridCellRenderer<Hd3dModelData>,
        IExplorateurColumnContextMenu<Hd3dModelData>
{

    private QuickTip quickTip;

    public Object render(Hd3dModelData model, String property, ColumnData config, int rowIndex, int colIndex,
            ListStore<Hd3dModelData> store, Grid<Hd3dModelData> grid)
    {
        this.createQuickType(grid);

        String html = "";
        ColumnConfig cc = grid.getColumnModel().getColumn(colIndex);

        if (cc instanceof ExplorerColumnConfig)
        {
            List<ApprovalNoteModelData> approvals = model.get(property);

            int nbApproval = 0;
            if (approvals != null)
            {
                nbApproval = approvals.size();
            }

            if (nbApproval > 0)
            {
                ApprovalNoteModelData approval = approvals.get(0);
                if (approval.getStatus().equals(ApprovalNoteModelData.STATUS_OK))
                {
                    if (!Util.isEmptyString(approval.getComment()))
                    {
                        html = getRenderedValue(approval.getDate(), approval.getComment(), nbApproval, "#9F9");
                    }
                    else
                    {
                        html = getRenderedValue(approval.getDate(), ApprovalNoteModelData.STATUS_OK, nbApproval, "#9F9");
                    }
                }
                else if (approval.getStatus().equals(ApprovalNoteModelData.STATUS_NOK))
                {
                    html = getRenderedValue(approval.getDate(), approval.getComment(), nbApproval, "#F66");
                }
                else if (approval.getStatus().equals(ApprovalNoteModelData.STATUS_DONE))
                {
                    html = getRenderedValue(approval.getDate(), approval.getComment(), nbApproval, "#FC9");
                }
                else if (approval.getStatus().equals(ApprovalNoteModelData.STATUS_WFA))
                {
                    html = "CONSTANTS.WaitingForApproval";
                }
            }
            else
            {
                html = "&nbsp;";
            }
        }
        return html;
    }

    private void createQuickType(Grid<Hd3dModelData> grid)
    {
        quickTip = new QuickTip(grid);
        quickTip.initTarget(grid);
        quickTip.setInterceptTitles(false);
        quickTip.setShadow(false);
        quickTip.setBodyBorder(false);
        quickTip.setBorders(false);
    }

    /**
     * @param date
     *            The approval date.
     * @param text
     *            The approval comment.
     * @param count
     *            The number of action on this approval.
     * 
     * @return Approval note to display at HTML format.
     */
    private String getRenderedValue(Date date, String text, int count, String color)
    {
        String stringDate = DateFormat.FRENCH_DATE.format(date);// DateTimeFormat.getShortDateFormat().format(date);

        String html = "";

        html += "<div style=\"padding: 0 5px 0 5px; background-color:" + color + "\">";
        html += stringDate;
        if (text != null)
            html += "<span qtip='" + text.replace("'", "") + "' qtitle='" + stringDate
                    + "' style=\"padding-left: 10px\">";

        if (GXT.isIE)
        {
            html += text + " (" + count + ")";
        }
        else
        {
            html += text;
            html += "&nbsp;(" + count + ")";
        }
        if (text != null)
            html += "</span>";
        html += "</div>";

        return html;
    }

    public void handleGridEvent(GridEvent<Hd3dModelData> ge)
    {}
}
