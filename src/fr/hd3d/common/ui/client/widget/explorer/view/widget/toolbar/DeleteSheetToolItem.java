package fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar;

import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.ExplorateurCSS;
import fr.hd3d.common.ui.client.widget.sheeteditor.event.SheetEditorEvents;
import fr.hd3d.common.ui.client.widget.sheeteditor.listener.DeleteSheetClickListener;


/**
 * Button that forward the SheetEditorEvents.DELETE_SHEET event. Before it displays a confirmation dialog box.
 */
public class DeleteSheetToolItem extends Button
{
    /** Constant strings to display : dialog messages, button label... */
    public static final CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);

    /**
     * Default constructor
     */
    public DeleteSheetToolItem()
    {
        super();

        this.setStyle();
        this.registerListener();
    }

    private void registerListener()
    {
        this.addSelectionListener(new DeleteSheetClickListener(this));
    }

    private void setStyle()
    {
        this.setIconStyle(ExplorateurCSS.DELETE_VIEW_ICON_CLASS);
        this.getElement().setNodeValue(ExplorateurCSS.NEW_VIEW_ICON_CLASS);

        this.setToolTip(COMMON_CONSTANTS.DeleteCurrentSheet());
    }

    public void askConfirmation()
    {
        MessageBox.confirm(COMMON_CONSTANTS.Confirm(), COMMON_CONSTANTS.ConfirmDeleteSheet(),
                new ConfirmMessageBoxCallback(SheetEditorEvents.DELETE_SHEET_CLICKED, null, null));
    }
}
