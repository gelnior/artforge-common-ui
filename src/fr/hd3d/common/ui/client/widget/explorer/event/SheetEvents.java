package fr.hd3d.common.ui.client.widget.explorer.event;

import com.extjs.gxt.ui.client.event.EventType;



public class SheetEvents
{
    protected SheetEvents()
    {

    }

    private static final int SHEET_START_EVENT = ExplorerEvents.EXPLORATEUR_START_EVENT + 300;

    public static final EventType ITEMGROUP_LOADED = new EventType(SHEET_START_EVENT + 1);

    public static final EventType ALL_ITEMS_LOADED = new EventType(SHEET_START_EVENT + 2);
}
