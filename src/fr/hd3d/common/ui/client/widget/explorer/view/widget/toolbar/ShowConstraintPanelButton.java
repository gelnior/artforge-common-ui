package fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.widget.button.Button;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;


/**
 * 
 * an edit button to edit the current view with the sheet editor
 */
public class ShowConstraintPanelButton extends Button
{

    /**
     * Default constructor
     */
    public ShowConstraintPanelButton()
    {
        super();
        initButton();
    }

    private void initButton()
    {
        this.setText("Show constraint panel");

        //this.setIconStyle(ExplorateurCSS.EDIT_VIEW_ICON_CLASS);
        //this.getElement().setNodeValue(ExplorateurCSS.EDIT_VIEW_ICON_CLASS);

        this.setToolTip("Show constraint panel");
        
        this.addListener(Events.Select, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                EventDispatcher.forwardEvent(ExplorerEvents.SHOW_CONSTRAINT_PANEL);
            }
        });
    }
}
