package fr.hd3d.common.ui.client.widget.explorer.model.reader;

import com.extjs.gxt.ui.client.data.PagingLoadResult;

import fr.hd3d.common.ui.client.modeldata.reader.Hd3dJsonReader;
import fr.hd3d.common.ui.client.modeldata.technical.SheetDataModelData;


public class SheetDataReader extends Hd3dJsonReader<SheetDataModelData, PagingLoadResult<SheetDataModelData>>
{

    public SheetDataReader()
    {
        super(SheetDataModelData.getDataModelType());
    }

    @Override
    public SheetDataModelData newModelInstance()
    {
        return new SheetDataModelData();
    }

}
