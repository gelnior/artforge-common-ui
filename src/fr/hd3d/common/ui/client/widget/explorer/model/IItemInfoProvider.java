package fr.hd3d.common.ui.client.widget.explorer.model;

import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;

import fr.hd3d.common.ui.client.widget.explorer.model.modeldata.IColumn;


public interface IItemInfoProvider
{
    /**
     * get the renderer class for the given renderer name
     * 
     * @param name
     */
    public GridCellRenderer<?> getRenderer(String name);

    /**
     * get the editor class for the given editor name
     * 
     * @param name
     */
    public CellEditor getEditor(String name, IColumn column);
}
