package fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;


public class IntRenderer<M extends ModelData> implements GridCellRenderer<M>
{

    public Object render(M model, String property, ColumnData config, int rowIndex, int colIndex, ListStore<M> store,
            Grid<M> grid)
    {
        String stringValue = "";
        if (model.get(property) != null)
        {
            Object objValue = model.get(property);
            if (objValue instanceof Double)
            {
                Long longValue = Math.round((Double) objValue);
                stringValue = longValue.toString();
            }
        }
        return stringValue;
    }

}
