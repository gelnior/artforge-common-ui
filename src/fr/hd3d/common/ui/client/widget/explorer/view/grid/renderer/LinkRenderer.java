package fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;


public class LinkRenderer<M extends ModelData> implements GridCellRenderer<M>
{
    public Object render(M model, String property, ColumnData config, int rowIndex, int colIndex, ListStore<M> store,
            Grid<M> grid)
    {
        String link = model.get(property);
        if (link != null)
        {
            return "<a href='" + link + "'>" + link + "</a>";
        }
        else
        {
            return "";
        }
    }

}
