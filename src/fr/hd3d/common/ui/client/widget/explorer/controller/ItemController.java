package fr.hd3d.common.ui.client.widget.explorer.controller;

import com.extjs.gxt.ui.client.event.BaseObservable;

import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;

public class ItemController extends BaseObservable
{

    private ItemModelData item;

    public ItemController(ItemModelData item)
    {
        this.item = item;
    }
    
    public ItemModelData getItem()
    {
        return item;
    }
}
