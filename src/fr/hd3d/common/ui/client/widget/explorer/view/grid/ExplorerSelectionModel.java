package fr.hd3d.common.ui.client.widget.explorer.view.grid;

import java.util.ArrayList;

import com.extjs.gxt.ui.client.core.El;
import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.Component;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.extjs.gxt.ui.client.widget.grid.GridSelectionModel;
import com.google.gwt.core.client.JavaScriptException;
import com.google.gwt.event.dom.client.KeyCodes;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;


/***
 * ExplorerSelectionModel describes the selection behavior for the grid explorer.
 * 
 * @author HD3D
 */
public class ExplorerSelectionModel extends GridSelectionModel<Hd3dModelData>
{
    private static final int START_COLUMN = 0;

    /** Style used to display checking inside check box. */
    private static final String CHECK_ON_STYLE = "x-grid3-hd-checker-on";

    /** Column GXT configuration. */
    protected ColumnConfig config;

    /** True if there is a previous selection. */
    protected boolean isPrevSelection = false;

    /** Actually selected cell column index. */
    protected int columnIndex = -1;
    /** Actually selected cell row index. */
    protected int rowIndex = -1;
    /** Previously selected column index. */
    protected int previousColumnIndex = -1;
    /** Previously selected column index. */
    protected int previousRowIndex = -1;

    /** List of currently selected cells. */
    protected ArrayList<Cell> selectedCells = new ArrayList<Cell>();
    /** Grid view needed to display section. */
    protected ExplorerGridView view;

    /**
     * Default constructor, set selection column configuration for check boxes.
     */
    public ExplorerSelectionModel()
    {
        super();
        this.setSelectionColumnConfig();
    }

    /**
     * @return Column GXT configuration.
     */
    public ColumnConfig getSelectionColumnConfig()
    {
        return config;
    }

    /**
     * @return Currently selected cell index.
     */
    public int getSelectionColumnIndex()
    {
        return this.columnIndex;
    }

    /**
     * Register view to selection manager and add listener to header (needed by check box header).
     * 
     * @param component
     * @param view
     */
    public void init(Component component, ExplorerGridView view)
    {
        this.view = view;
        component.addListener(Events.HeaderClick, new Listener<GridEvent<Hd3dModelData>>() {
            public void handleEvent(GridEvent<Hd3dModelData> e)
            {
                onHeaderClick(e);
            }
        });
    }

    /**
     * Reset all selection, deselect everything and set to -1 actual column/row and previously selected column/row
     * index.
     */
    public void reset()
    {
        this.columnIndex = -1;
        this.rowIndex = -1;
        this.previousColumnIndex = -1;
        this.previousRowIndex = -1;
        this.isPrevSelection = false;
        this.deselectCellsAndRows();
    }

    @SuppressWarnings("unchecked")
    @Override
    public void handleEvent(BaseEvent e)
    {
        EventType type = e.getType();

        if (type == Events.RowMouseDown)
        {
            handleMouseDown((GridEvent<Hd3dModelData>) e);
        }
        else if (type == Events.ContextMenu)
        {
            handleMouseDown((GridEvent<Hd3dModelData>) e);
        }
        else if (type == Events.Refresh)
        {
            refresh();
        }
    }

    @Override
    protected void onKeyPress(GridEvent<Hd3dModelData> e)
    {
        int k = e.getKeyCode();
        switch (k)
        {
            case 32:
                this.onKeyEnter();
                break;
            case KeyCodes.KEY_ENTER:
                this.onKeyEnter();
                break;
        }
        if (k != 32)
            super.onKeyPress(e);
    }

    /**
     * When enter key is pressed, if there is one selected cell, edition mode is activated.
     */
    protected void onKeyEnter()
    {
        if (this.selectedCells.size() == 1)
        {
            Cell cell = this.selectedCells.get(0);
            ((ExplorerGrid) grid).startEditing(cell.row, cell.cell);
        }

        if (selectedCells.size() > 0)
        {
            int colIndex = selectedCells.get(0).cell;
            AppEvent event = new AppEvent(ExplorerEvents.ENTER_KEY_PRESSED, colIndex);
            EventDispatcher.forwardEvent(event);
        }
    }

    /**
     * When down key is pressed, the cell above the currently selected cell and its row are selected. If shift key is
     * not pressed, previously selected cell and its row are unselected.
     * 
     * @param e
     *            Key press event.
     */
    @Override
    protected void onKeyDown(GridEvent<Hd3dModelData> e)
    {
        e.preventDefault();
        Cell cell = null;

        if (this.selectedCells.size() > 0)
        {
            int row = 0;
            if (e.isShiftKey())
            {
                for (Cell c : selectedCells)
                {
                    if (cell == null || c.row > cell.row)
                    {
                        cell = c;
                    }
                }
                row = cell.row + 1;
            }
            else
            {
                cell = new Cell(previousRowIndex, previousColumnIndex);
                this.deselectCellsAndRows();

                if (cell.row + 1 < store.getModels().size())
                    row = cell.row + 1;
            }
            Cell newCell = new Cell(row, cell.cell);
            this.selectCellandRow(newCell.row, newCell.cell);
            this.registerSelection(newCell.row, newCell.cell);

            this.forwardSelectChangeEvent(listStore.getAt(newCell.row));
        }
    }

    /**
     * When up key is pressed, the cell over the currently selected cell and its row are selected. If shift key is not
     * pressed, previously selected cell and its row are unselected.<br>
     * <br>
     * SELECTION_CHANGE event is raised.
     * 
     * @param e
     *            Key press event.
     */
    @Override
    protected void onKeyUp(GridEvent<Hd3dModelData> e)
    {
        e.preventDefault();
        Cell cell = null;

        if (this.selectedCells.size() > 0)
        {
            int row = 0;
            if (e.isShiftKey())
            {
                for (Cell c : selectedCells)
                {
                    if (cell == null || c.row < cell.row)
                    {
                        cell = c;
                    }
                }

                row = cell.row - 1;
            }
            else
            {
                cell = new Cell(previousRowIndex, previousColumnIndex);
                this.deselectCellsAndRows();

                if (cell.row - 1 >= 0)
                {
                    row = cell.row - 1;
                }
                else
                {
                    row = store.getModels().size() - 1;
                }
            }
            Cell newCell = new Cell(row, cell.cell);
            this.selectCellandRow(newCell.row, newCell.cell);
            this.registerSelection(newCell.row, newCell.cell);

            this.forwardSelectChangeEvent(listStore.getAt(newCell.row));
        }
    }

    /**
     * When left key is pressed, the cell at the left of the currently selected cell is selected. If currently selected
     * cell is at the extreme left.
     */
    @Override
    protected void onKeyLeft(GridEvent<Hd3dModelData> ce)
    {
        super.onKeyLeft(ce);

        ce.preventDefault();
        if (this.selectedCells.size() == 1)
        {
            Cell cell = this.selectedCells.get(0);
            Cell newCell = this.walkCells(cell.row, cell.cell - 1, -1);

            if (newCell != null)
            {
                if (cell.row != newCell.row)
                {
                    this.deselectCellandRow(cell.row, cell.cell);
                    this.selectCellandRow(newCell.row, newCell.cell);
                    this.registerPreviousSelection();

                    this.forwardSelectChangeEvent(listStore.getAt(newCell.row));
                }
                else
                {
                    this.deselectCell(cell);
                    this.selectedCells.clear();
                    this.selectCell(newCell);
                }

                this.registerSelection(newCell.row, newCell.cell);
            }
        }
    }

    @Override
    protected void onKeyRight(GridEvent<Hd3dModelData> ce)
    {
        ce.preventDefault();
        if (this.selectedCells.size() == 1)
        {
            Cell cell = this.selectedCells.get(0);
            Cell newCell = this.walkCells(cell.row, cell.cell + 1, 1);

            if (newCell != null)
            {
                if (cell.row != newCell.row)
                {
                    this.deselectCellandRow(cell.row, cell.cell);
                    this.selectCellandRow(newCell.row, newCell.cell);
                    this.registerPreviousSelection();

                    this.forwardSelectChangeEvent(listStore.getAt(newCell.row));
                }
                else
                {
                    this.deselectCell(cell);
                    this.selectedCells.clear();
                    this.selectCell(newCell);
                }

                this.registerSelection(newCell.row, newCell.cell);
            }
        }
    }

    protected void registerSelection(int row, int cell)
    {
        this.rowIndex = row;
        this.columnIndex = cell;

        this.registerPreviousSelection();
    }

    @Override
    protected void handleMouseDown(GridEvent<Hd3dModelData> e)
    {
        if (!isLocked())
        {
            rowIndex = e.getRowIndex();
            columnIndex = e.getColIndex();
            Hd3dModelData selectedModel = listStore.getAt(rowIndex);
            // Event event = e.getEvent();
            // boolean isCheckBoxClicked = event != null && event.getButton() == Event.BUTTON_LEFT && e.getColIndex() ==
            // 0;

            if (!(e.isRightClick() && this.isSelected(selectedModel)))
            {
                // if (isCheckBoxClicked)
                // {
                // this.onCheckBoxClick(e, selectedModel);
                // }
                // else
                // {
                this.onCellClick(e, selectedModel);
                // }
            }
        }
    }

    /**
     * When a check box is clicked, the check box is selected or unselected if it was previously selected.
     * 
     * @param e
     *            the grid event to handle.
     * @param selectedModel
     *            The record of the row to select.
     */
    protected void onCheckBoxClick(GridEvent<Hd3dModelData> e, Hd3dModelData selectedModel)
    {
        El row = e.getTarget(".x-grid3-row", 15);
        if (row != null)
        {
            if (!isSelected(selectedModel))
            {
                this.selectRow(selectedModel);
            }
            else
            {
                this.deselectRow(selectedModel);
            }
        }
    }

    /**
     * When header is clicked, all check boxes are checked on and all rows are selected. If all check boxes were checked
     * on, check boxes are check off and rows unselected.
     * 
     * @param e
     */
    protected void onHeaderClick(GridEvent<Hd3dModelData> e)
    {
        if (e.getColIndex() == 0)
        {
            if (this.listStore.getCount() == selected.size())
            {
                deselectCellsAndRows();
            }
            else
            {
                this.deselectCells();
                selectAll();
            }
        }
    }

    private void onCellClick(GridEvent<Hd3dModelData> event, Hd3dModelData selectedModel)
    {
        try
        {
            if (event.isShiftKey() && previousColumnIndex == columnIndex && isPrevSelection
                    && this.selectedCells.size() > 0)
            {
                this.onShiftKey(event);
            }
            else if (event.isControlKey() && previousColumnIndex == columnIndex)
            {
                this.onControlClick(selectedModel);
            }
            else
            {
                this.onNoKeyClick();
            }

            this.testSelectionChange(selectedModel);
            this.registerPreviousSelection();
        }
        catch (JavaScriptException exception)
        {
            Logger.log("Explorer selection model error", exception);
            this.onJavascriptError();
        }
    }

    protected void onShiftKey(GridEvent<Hd3dModelData> event)
    {
        if (!event.isControlKey())
        {
            boolean isSelected = false;
            for (Cell c : selectedCells)
            {
                if (c.row == rowIndex && c.cell == columnIndex)
                {
                    isSelected = true;
                    continue;
                }
            }
            if (!isSelected)
                this.deselectCellsAndRows();
        }
        int beginSelection = 0;
        int endSelection = 0;

        this.isPrevSelection = false;

        if (previousRowIndex > rowIndex)
        {
            beginSelection = rowIndex;

            if (!event.isControlKey())
            {
                endSelection = previousRowIndex + 1;
            }
            else
            {
                endSelection = previousRowIndex;
            }
        }
        else
        {
            if (!event.isControlKey())
            {
                beginSelection = previousRowIndex;
            }
            else
            {
                beginSelection = previousRowIndex + 1;
            }
            endSelection = rowIndex + 1;
        }

        this.selectCellAndRows(beginSelection, endSelection);
    }

    protected void onControlClick(Hd3dModelData selectedModel)
    {
        this.selectedCells.add(new Cell(previousRowIndex, previousColumnIndex));
        this.isPrevSelection = false;

        if (selectedCells.size() == 0)
        {
            deselectAll();
        }

        if (isSelected(selectedModel))
        {
            this.deselectClickedCellandRow(selectedModel);
        }
        else
        {
            this.selectCellandRow(rowIndex, columnIndex);
        }
    }

    /**
     * When a click on a cell occurs and no key is pressed, the clicked cell and its row are selected.
     */
    protected void onNoKeyClick()
    {
        boolean isSelected = false;
        for (Cell c : selectedCells)
        {
            if (c.row == rowIndex && c.cell == columnIndex)
            {
                isSelected = true;
                continue;
            }
        }
        if (!isSelected)
            this.deselectCellsAndRows();
        this.selectCellandRow(rowIndex, columnIndex);
    }

    /**
     * When javascript error occurs, everything is unselected.
     */
    protected void onJavascriptError()
    {
        this.deselectCellsAndRows();
        this.isPrevSelection = false;
    }

    /**
     * When select changes, the check boxes header is updated and the previous selected cell is unselected. Select
     * Change event is forwarded by event dispatcher.
     * 
     * @see com.extjs.gxt.ui.client.widget.grid.GridSelectionModel#onSelectChange(com.extjs.gxt.ui.client.data.ModelData,
     *      boolean)
     */
    @Override
    protected void onSelectChange(Hd3dModelData model, boolean select)
    {
        super.onSelectChange(model, select);

        // this.updateCheckBoxHeader();

        if (this.isPrevSelection)
        {
            this.deselectCells();
        }
    }

    /**
     * Test if selection change event should be forwarded.
     * 
     * @param selectedModel
     */
    protected void testSelectionChange(Hd3dModelData selectedModel)
    {
        if (previousRowIndex != rowIndex)
        {
            if (this.selectedCells.size() == 1)
            {
                this.forwardSelectChangeEvent(selectedModel);
            }
            else
            {
                this.forwardSelectChangeEvent(null);
            }
        }
    }

    /**
     * Forward to event dispatcher the fact that another record (row) is selected and attach the record to the event.
     * 
     * @param selectedModel
     */
    protected void forwardSelectChangeEvent(Hd3dModelData selectedModel)
    {
        AppEvent event = new AppEvent(ExplorerEvents.SELECT_CHANGE);
        event.setData(ExplorerEvents.EVENT_VAR_MODELDATA, selectedModel);
        EventDispatcher.forwardEvent(event);
    }

    /**
     * Select row corresponding to <i>selectedModel</i> and deselect cell in that row.
     * 
     * @param selectedModel
     *            The model to select.
     */
    protected void deselectRow(Hd3dModelData selectedModel)
    {
        if (selectedCells.size() > 0)
        {
            int column = this.selectedCells.get(0).cell;

            this.deselectCell(new Cell(rowIndex, column));
            this.isPrevSelection = false;
        }

        this.deselectModel(selectedModel);
    }

    /**
     * Select row corresponding to <i>selectedModel</i> and select cell in that row corresponding to last click.
     * 
     * @param selectedModel
     *            The model to select.
     */
    protected void selectRow(Hd3dModelData selectedModel)
    {
        if (selectedCells.size() > 0)
        {
            int column = this.selectedCells.get(0).cell;

            this.selectCell(new Cell(rowIndex, column));
            this.isPrevSelection = false;
        }

        this.selectModel(selectedModel);
    }

    /**
     * Select row corresponding to <i>selectedModel</i> without cell selection.
     * 
     * @param selectedModel
     *            The model to select.
     */
    protected void selectModel(Hd3dModelData selectedModel)
    {
        ArrayList<Hd3dModelData> selection = new ArrayList<Hd3dModelData>();
        selection.add(selectedModel);
        doSelect(selection, true, false);
    }

    /**
     * Deselect row corresponding to <i>selectedModel</i> without cell unselection.
     * 
     * @param selectedModel
     *            The model to unselect.
     */
    protected void deselectModel(Hd3dModelData selectedModel)
    {
        ArrayList<Hd3dModelData> selection = new ArrayList<Hd3dModelData>();
        selection.add(selectedModel);
        doDeselect(selection, true);
    }

    /**
     * Select <i>cell</i> from view and add it from selected cell list. Then focus is done on the cell.
     * 
     * @param cell
     *            The cell to select.
     */
    protected void selectCell(Cell cell)
    {
        this.view.onCellSelect(cell.row, cell.cell);
        this.selectedCells.add(cell);

        this.view.focusCell(cell.row, cell.cell, true);
    }

    /**
     * Deselect <i>cell</i> from view and remove it from selected cell list.
     * 
     * @param cell
     *            The cell to deselect.
     */
    protected void deselectCell(Cell cell)
    {
        this.view.onCellDeselect(cell.row, cell.cell);
        for (Cell c : selectedCells)
        {
            if (cell.row == c.row && cell.cell == c.cell)
            {
                selectedCells.remove(c);
                break;
            }
        }
    }

    /**
     * Select the cell of coordinates (row, col) and the row of index row.
     */
    protected void selectCellandRow(int row, int col)
    {
        this.selectRow(listStore.getAt(row));
        this.selectCell(new Cell(row, col));
    }

    /**
     * Select rows from <i>beginRow</i> to <i>endRow</i>. Select the cell for each who corresponds to the currently
     * selected cell (same column).
     * 
     * @param beginRow
     *            Row where selection begins.
     * @param endRow
     *            Row where selection ends.
     */
    protected void selectCellAndRows(int beginRow, int endRow)
    {
        ArrayList<Hd3dModelData> selection = new ArrayList<Hd3dModelData>();
        for (int i = beginRow; i < endRow; i++)
        {
            selection.add(listStore.getAt(i));
            view.onCellSelect(i, columnIndex);
            this.selectedCells.add(new Cell(i, columnIndex));
        }
        doSelect(selection, true, true);
    }

    /**
     * Deselect the cell of coordinates (row, col) and the row of index row.
     */
    protected void deselectCellandRow(int row, int col)
    {
        ArrayList<Hd3dModelData> selection = new ArrayList<Hd3dModelData>();
        selection.add(listStore.getAt(row));
        doDeselect(selection, true);

        view.onCellDeselect(row, col);
        for (Cell cell : selectedCells)
        {
            if (cell.row == row && cell.cell == col)
            {
                selectedCells.remove(cell);
            }
        }
    }

    /**
     * Deselect the cell and row which has been clicked.
     */
    protected void deselectClickedCellandRow(Hd3dModelData selectedModel)
    {
        ArrayList<Hd3dModelData> selection = new ArrayList<Hd3dModelData>();
        selection.add(selectedModel);
        doDeselect(selection, true);
        view.onCellDeselect(rowIndex, columnIndex);
    }

    /**
     * Deselect all previously selected rows and cells.
     */
    public void deselectCellsAndRows()
    {
        this.deselectCells();
        deselectAll();
    }

    /**
     * Deselect all cells previously selected.
     */
    protected void deselectCells()
    {
        for (Cell cell : selectedCells)
        {
            this.view.onCellDeselect(cell.row, cell.cell);
        }

        selectedCells.clear();
    }

    /**
     * Register coordinates of last selection and say that there is a previous selection.
     */
    protected void registerPreviousSelection()
    {
        isPrevSelection = true;
        previousColumnIndex = columnIndex;
        previousRowIndex = rowIndex;
    }

    /**
     * If all rows are selected, the check box header is checked on. If not, it is checked off.
     */
    protected void updateCheckBoxHeader()
    {
        El hd = view.getCheckBoxHeader();

        if (getSelection().size() == grid.getStore().getCount())
        {
            hd.addStyleName(CHECK_ON_STYLE);
            this.forwardSelectChangeEvent(null);
        }
        else
        {
            hd.removeStyleName(CHECK_ON_STYLE);
        }
    }

    /**
     * Set check box column configuration.
     */
    protected void setSelectionColumnConfig()
    {
        config = new ColumnConfig();
        config.setHeader("<div class='x-grid3-hd-checker'>&#160;</div>");
        config.setId("checker");
        config.setWidth(20);
        config.setSortable(false);
        config.setResizable(false);
        config.setFixed(true);
        config.setMenuDisabled(true);
        config.setDataIndex("");
        config.setRenderer(new GridCellRenderer<Hd3dModelData>() {

            public Object render(Hd3dModelData model, String property, ColumnData config, int rowIndex, int colIndex,
                    ListStore<Hd3dModelData> store, Grid<Hd3dModelData> grid)
            {
                config.cellAttr = "rowspan='2'";
                return "<div class='x-grid3-row-checker'>&#160;</div>";
            }

        });
    }

    /**
     * Useful method to know which cell is considered to be on the left or right of the given cell
     * (<i>row</i>,<i>col</i>).
     * 
     * @param row
     *            The row from where walking starts.
     * @param col
     *            The column from where walking starts.
     * @param step
     *            The number of step that should be done for this walking.
     * @return cell that is located at <i>steps</i> from initial cell located at (<i>row</i>,<i>col</i>).
     */
    protected Cell walkCells(int row, int col, int step)
    {
        boolean first = true;
        int clen = grid.getColumnModel().getColumnCount();
        int rlen = grid.getStore().getCount();

        // Walk forward : to the right.
        if (step < 0)
        {
            if (col < START_COLUMN)
            {
                row--;
                first = false;
            }
            while (row >= 0)
            {
                if (!first)
                {
                    col = clen - 1;
                }
                first = false;
                while (col >= START_COLUMN)
                {
                    if (isSelectable(row, col, false))
                    {
                        return new Cell(row, col);
                    }
                    col--;
                }
                row--;
            }
        }
        // Walk backward : to the right.
        else
        {
            if (col >= clen)
            {
                row++;
                first = false;
            }
            while (row < rlen)
            {
                if (!first)
                {
                    col = START_COLUMN;
                }
                first = false;
                while (col < clen)
                {
                    if (isSelectable(row, col, false))
                    {
                        return new Cell(row, col);
                    }
                    col++;
                }
                row++;
            }
        }
        return null;
    }

}
