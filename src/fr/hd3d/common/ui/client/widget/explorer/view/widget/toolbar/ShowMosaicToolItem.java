package fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.widget.button.ToggleButton;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;


/**
 * Show explorer rows as mosaic (mosaic is made of row preview images).
 */
public class ShowMosaicToolItem extends ToggleButton
{
    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    /**
     * Default constructor.
     */
    public ShowMosaicToolItem()
    {
        this.setIcon(Hd3dImages.getMosaicIcon());
        this.setToolTip("Show/Hide mosaic view");

        this.addListener(Events.Select, new Listener<ButtonEvent>() {
            public void handleEvent(ButtonEvent be)
            {
                ToggleButton bu = (ToggleButton) be.getButton();
                if (bu.isPressed())
                {
                    EventDispatcher.forwardEvent(ExplorerEvents.SHOW_MOSAIC_CLICKED);
                }
                else
                {
                    EventDispatcher.forwardEvent(ExplorerEvents.HIDE_MOSAIC_CLICKED);
                }
            }
        });
    }
}
