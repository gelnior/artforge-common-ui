package fr.hd3d.common.ui.client.widget.explorer.model.modeldata;

import com.extjs.gxt.ui.client.data.ModelData;


public interface IHd3dBaseModel extends ModelData
{
    Long getId();

    void setId(Long id);

    public String getJsonRepresentation();
}
