package fr.hd3d.common.ui.client.widget.explorer;

import java.util.List;

import com.extjs.gxt.ui.client.store.ListStore;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.service.parameter.IUrlParameter;
import fr.hd3d.common.ui.client.service.parameter.LogicConstraint;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.widget.BorderedPanel;
import fr.hd3d.common.ui.client.widget.constraint.ConstraintPanel;
import fr.hd3d.common.ui.client.widget.constraint.IEditorProvider;
import fr.hd3d.common.ui.client.widget.constraint.widget.SheetFilterComboBox;
import fr.hd3d.common.ui.client.widget.explorer.controller.ExplorerController;
import fr.hd3d.common.ui.client.widget.explorer.model.ExplorerModel;
import fr.hd3d.common.ui.client.widget.explorer.model.ExplorerStore;
import fr.hd3d.common.ui.client.widget.explorer.model.IColumnInfoProvider;
import fr.hd3d.common.ui.client.widget.explorer.view.ExplorerView;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.ExplorerGrid;
import fr.hd3d.common.ui.client.widget.thumbnaileditor.ThumbnailPath;


/**
 * Explorer panel contains the HD3D explorer widget that displays data from sheet created by user via a GXT grid. It
 * should be configured with providers : one for grid columns and one for constraint editor.
 * 
 * @author HD3D
 */
public class ExplorerPanel extends BorderedPanel
{
    /** Controller which handles explorer events. */
    private final ExplorerController controller;
    /** Controller which contains explorer widget. */
    private final ExplorerView view = new ExplorerView(this);
    /** Model handles explorer data. */
    private final ExplorerModel model = new ExplorerModel();

    /** Panel which allows user to set filter on displayed sheet. */
    private final ConstraintPanel constraintPanel = new ConstraintPanel(this);

    /**
     * Default constructor
     */
    public ExplorerPanel()
    {
        this.controller = new ExplorerController(view, model);
        EventDispatcher.get().addController(controller);

        this.addCenter(view, 0, 0, 0, 0);

        this.setConstraintPanel();
        this.unidle();
        this.view.hideLoading();
    }

    /**
     * @return Explorer controller.
     */
    public ExplorerController getController()
    {
        return this.controller;
    }

    /**
     * @return Explorer grid data store.
     */
    public ExplorerStore getStore()
    {
        return this.model.getDataStore();
    }

    /**
     * Idle explorer and its constraint panel.
     */
    public void idle()
    {
        this.controller.mask();
        this.constraintPanel.getController().mask();
    }

    /**
     * Unidle explorer and its constraint panel.
     */
    public void unidle()
    {
        this.controller.unMask();
        this.constraintPanel.getController().unMask();
    }

    /**
     * @return explorer grid.
     */
    public ExplorerGrid getGrid()
    {
        return this.view.getGrid();
    }

    /**
     * @return Column information provider. Provider is useful to set columns corresponding to data displayed.
     */
    public IColumnInfoProvider getColumnInfoProvider()
    {
        return this.controller.getColumnInfoProvider();
    }

    /**
     * Set column information provider. Provider is useful to set columns corresponding to data displayed.
     * 
     * @param columnInfoProvider
     *            The provider to set.
     */
    public void setColumnInfoProvider(IColumnInfoProvider columnInfoProvider)
    {
        this.controller.setColumnInfoProvider(columnInfoProvider);
    }

    /**
     * Rebuild grid.
     */
    public void reconfigureGrid()
    {
        this.controller.showGrid();
    }

    /**
     * Set constraint field editor provider. Provider is useful to set right fields corresponding to data filtered.
     * 
     * @param editorProvider
     *            The constraint editor provider.
     */
    public void setConstraintEditorProvider(IEditorProvider editorProvider)
    {
        this.constraintPanel.setConstraintEditorProvider(editorProvider);
    }

    /**
     * @return Filter set on explorer (filter from application and filter from user).
     */
    public LogicConstraint getFilter()
    {
        return this.model.getFilters();
    }

    /** Add a new filter as a filter application. */
    public void setApplicationFilter(LogicConstraint filter)
    {
        this.model.setApplicationFilter(filter);
    }

    /** Rebuild explorer filter depending on set application filter and user filter. */
    public void refreshFilter()
    {
        this.model.refreshFilter();
    }

    /**
     * Add a new parameter to the explorer data store.
     * 
     * @param parameter
     *            The parameter to add.
     */
    public void addParameter(IUrlParameter parameter)
    {
        this.model.addParameter(parameter);
    }

    /**
     * Remove a parameter from the explorer data store.
     * 
     * @param parameter
     *            The parameter to remove.
     */
    public void removeParameter(IUrlParameter parameter)
    {
        this.model.removeParameter(parameter);
    }

    /** Hide constraint panel. */
    public void hideConstraintPanel()
    {
        this.constraintPanel.hide();
    }

    /**
     * Return current sheet loaded inside explorer.
     */
    public SheetModelData getCurrentSheet()
    {
        return this.model.getCurrentSheet();
    }

    /**
     * Load sheet inside explorer and reload explorer data.
     * 
     * @param sheet
     *            The sheet to load.
     * 
     */
    public void setCurrentSheet(SheetModelData sheet)
    {
        this.controller.setCurrentSheet(sheet, true);
    }

    /**
     * Load sheet inside explorer.
     * 
     * @param sheet
     *            The sheet to load.
     * @param isLoadData
     *            True to load data after setting sheet.
     */
    public void setCurrentSheet(SheetModelData sheet, boolean isLoadData)
    {
        this.controller.setCurrentSheet(sheet, isLoadData);
    }

    /**
     * Clear actual selection.
     */
    public void resetSelection()
    {
        this.view.resetSelection();
    }

    /**
     * @return First selected model (row).
     */
    public Hd3dModelData getFirstSelectedModel()
    {
        List<Hd3dModelData> selectedModels = this.getSelectedModels();
        return (Hd3dModelData) CollectionUtils.getFirst(selectedModels);
    }

    /**
     * @return All selected models.
     */
    public List<Hd3dModelData> getSelectedModels()
    {
        return this.view.getSelectedRows();
    }

    /**
     * Insert <i>record</i> inside explorer grid at index <i>i</i>.
     * 
     * @param record
     *            The record to insert.
     * @param i
     *            Index to insert record.
     */
    public void insert(Hd3dModelData record, int i)
    {
        this.model.getDataStore().insert(record, i);
    }

    /**
     * Update <i>record</i> data (GXT need this operation to refresh displayed data).
     * 
     * @param record
     *            The record to update.
     */
    public void update(Hd3dModelData record)
    {
        this.model.getDataStore().update(record);
    }

    /**
     * Remove <i>record</i> from explorer data store.
     * 
     * @param record
     *            The record to remove
     */
    public void deleteRow(Hd3dModelData record)
    {
        ListStore<Hd3dModelData> listStore = this.model.getDataStore();
        listStore.remove(record);
    }

    /**
     * Reload explorer data.
     */
    public void refreshData()
    {
        this.view.resetSelection();
        if (this.view.getPagingPage() != 1 && this.model.getDataStore().getCount() > 0)
        {
            this.view.setPagingPage(1);
        }
        else
        {
            this.model.loadData();
        }
        this.view.getBottomComponent().setEnabled(true);
    }

    /**
     * Recalculate thumbnail for a specific record.
     */
    public void recalculateThumbnail(Hd3dModelData record)
    {
        record.set(ThumbnailPath.CHANGED_MARKER, true);

        this.view.getGrid().getStore().update(record);
    }

    /**
     * Show loading indicator.
     */
    public void showLoading()
    {
        this.view.showLoading();
    }

    /**
     * Hide loading indicator.
     */
    public void hideLoading()
    {
        this.view.hideLoading();
    }

    /**
     * Build constraint panel.
     */
    private void setConstraintPanel()
    {
        this.constraintPanel.setBorders(false);
        this.constraintPanel.setBodyBorder(false);

        this.addNorth(constraintPanel, 100, 0, 0, 5, 0);
        this.constraintPanel.hide();
    }

    /**
     * Add an excluded constraint field to constraint panel (field cannot be used for row constraint building).
     * 
     * @param fieldName
     *            The field to exclude.
     */
    public void addExcludedConstraint(String fieldName)
    {
        this.constraintPanel.addExcludedConstraint(fieldName);
    }

    /**
     * Set sheet filter combo box controller as children of constraint panel controller.
     * 
     * @param sheetFilterComboBox
     *            The sheet filter combobox to register.
     */
    public void registerFilterComboBox(SheetFilterComboBox sheetFilterComboBox)
    {
        this.constraintPanel.getController().addChild(sheetFilterComboBox.getController());
    }

    public void setDefaultOrderColumn(String defaultOrderColumn)
    {
        this.model.setDefaultColumn(defaultOrderColumn);
    }

}
