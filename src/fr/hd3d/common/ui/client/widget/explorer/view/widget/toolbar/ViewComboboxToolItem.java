package fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar;

import com.extjs.gxt.ui.client.event.SelectionChangedEvent;
import com.extjs.gxt.ui.client.event.SelectionChangedListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.form.ComboBox;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;


/**
 * Combo box for tool bar to host the available sheet list, with predefined events.
 * 
 * @author HD3D
 */
public class ViewComboboxToolItem extends ComboBox<SheetModelData>
{
    /** Constant strings for displaying : dialog messages, button label... */
    public static final CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    /**
     * Default Constructor
     */
    public ViewComboboxToolItem()
    {
        createCombobox();
        this.setId("sheet-combo");
    }

    /**
     * @return number of sheet contained in combo box.
     */
    public int getCount()
    {
        return this.getStore().getCount();
    }

    /**
     * Select the view of which index is <i>i</i>.
     * 
     * @param i
     *            View index.
     */
    public void selectView(int i)
    {
        if (i >= 0 && this.getStore().getCount() > i)
        {
            SheetModelData value = this.getStore().getAt(i);
            this.setValue(value);
        }
        else
        {
            this.setValue(null);
        }
    }

    /**
     * @return The currently selected view.
     */
    public SheetModelData getSelectedView()
    {
        if (this.getSelection() != null && this.getStore().getCount() > 0)
        {
            return this.getSelection().get(0);
        }
        else
        {
            return null;
        }
    }

    /**
     * Select sheet given in parameter.
     * 
     * @param sheet
     *            The sheet to select.
     */
    public void setSelectedSheet(SheetModelData sheet)
    {
        this.setValue(sheet);
    }

    /**
     * Select view of which ID is equal to <i>id</i>.
     * 
     * @param id
     *            View to select ID.
     */
    public void selectViewById(Long id)
    {
        SheetModelData view = this.getStore().findModel(Hd3dModelData.ID_FIELD, id);
        this.setValue(view);
    }

    /**
     * Create the combo box and add listeners.
     */
    private void createCombobox()
    {
        this.setEmptyText(CONSTANTS.SelectSheet());
        this.setDisplayField(SheetModelData.NAME_FIELD);
        this.setWidth(150);
        this.setTypeAhead(true);
        this.setTriggerAction(TriggerAction.ALL);
        this.setEditable(false);

        SelectionChangedListener<SheetModelData> listener = new SelectionChangedListener<SheetModelData>() {
            @Override
            public void selectionChanged(SelectionChangedEvent<SheetModelData> se)
            {
                SheetModelData view = se.getSelectedItem();
                AppEvent event = new AppEvent(ExplorerEvents.CHANGE_VIEW, view);
                EventDispatcher.forwardEvent(event);
            }
        };
        this.addSelectionChangedListener(listener);

        this.setToolTip(CONSTANTS.SelectSheet());
    }
}
