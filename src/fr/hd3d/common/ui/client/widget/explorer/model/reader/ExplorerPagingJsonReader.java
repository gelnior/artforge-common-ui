package fr.hd3d.common.ui.client.widget.explorer.model.reader;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.data.BaseModelData;
import com.extjs.gxt.ui.client.data.BasePagingLoadResult;
import com.extjs.gxt.ui.client.data.ModelType;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONNumber;
import com.google.gwt.json.client.JSONObject;

import fr.hd3d.common.client.Const;
import fr.hd3d.common.ui.client.calendar.DateFormat;
import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.reader.Hd3dPagingJsonReader;
import fr.hd3d.common.ui.client.modeldata.reader.TaskReader;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetDataModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.widget.explorer.model.modeldata.IColumn;


public class ExplorerPagingJsonReader extends Hd3dPagingJsonReader<Hd3dModelData>
{
    protected SheetModelData sheet;
    protected final FastMap<IColumn> columns = new FastMap<IColumn>();

    protected List<Double> aggregatedValues = new ArrayList<Double>();

    public ExplorerPagingJsonReader()
    {
        super(SheetDataModelData.getDataModelType());
    }

    public List<Double> getAggregatedValues()
    {
        return aggregatedValues;
    }

    public ModelType getModeltype()
    {
        return this.modelType;
    }

    public void setSheet(SheetModelData sheet)
    {
        this.sheet = sheet;
        this.columns.clear();
        if (sheet != null && sheet.getColumns() != null)
            for (IColumn column : sheet.getColumns())
            {
                if (column.getId() != null)
                    columns.put(column.getId().toString(), column);
                else
                    Logger.warn("ExplorerPagingJsonReader : a column has no ID.");
            }
    }

    @Override
    public SheetDataModelData newModelInstance()
    {
        return new SheetDataModelData();
    }

    @Override
    public List<Hd3dModelData> readData(Object loadConfig, Object data)
    {
        return super.readData(loadConfig, data);
    }

    @Override
    public ArrayList<Hd3dModelData> readRecords(JSONArray root, ModelType modelType)
    {
        int size = 0;
        if (root != null)
            size = root.size();
        ArrayList<Hd3dModelData> models = new ArrayList<Hd3dModelData>();
        aggregatedValues.clear();

        for (int i = 0; i < size; i++)
        {
        	if(root.get(i) instanceof JSONObject) 
        	{
	            JSONObject obj = (JSONObject) root.get(i);
	
	            if (obj.get("sum") == null)
	            {
	                Hd3dModelData model = this.readObject(obj, modelType);
	
	                models.add(model);
	                List<JSONObject> boundTaskList = model.get(Hd3dModelData.BOUND_TASKS_FIELD);
	                if (boundTaskList != null && boundTaskList.size() > 0)
	                {
	                    List<TaskModelData> tmpList = new ArrayList<TaskModelData>(boundTaskList.size());
	                    for (JSONObject taskObject : boundTaskList)
	                    {
	                        TaskReader taskReader = new TaskReader();
	                        TaskModelData task = taskReader.readObject(taskObject, TaskModelData.getModelType());
	                        tmpList.add(task);
	                    }
	                    model.set(Hd3dModelData.BOUND_TASKS_FIELD, tmpList);
	                }
	            }
	            else
	            {
	                JSONArray array = obj.get("sum").isArray();
	                for (int j = 0; j < array.size(); j++)
	                {
	                    JSONNumber number = array.get(j).isNumber();
	                    if (number != null)
	                    {
	                        aggregatedValues.add(number.doubleValue());
	                    }
	                    else
	                    {
	                        aggregatedValues.add(null);
	                    }
	                }
	            }
        	}
        }

        return models;
    }

    /**
     * Responsible for the object being returned by the reader.
     * 
     * @param loadConfig
     *            the load configuration
     * @param records
     *            the list of models
     * @param totalCount
     *            the total count
     * @return the data to be returned by the reader
     */
    @Override
    protected BasePagingLoadResult<Hd3dModelData> createReturnData(Object loadConfig, List<Hd3dModelData> records,
            int totalCount)
    {
        List<Hd3dModelData> models = new ArrayList<Hd3dModelData>(records.size());

        for (Hd3dModelData record : records)
        {
            if (record instanceof SheetDataModelData)
            {
                SheetDataModelData obj = (SheetDataModelData) record;
                Hd3dModelData model = new Hd3dModelData();
                model.setId(obj.getId());
                model.setSimpleClassName(sheet.getBoundClassName());

                for (BaseModelData group : obj.getGroups())
                {
                    List<BaseModelData> items = group.get(SheetDataModelData.ITEMS_FIELD);
                    for (BaseModelData item : items)
                    {
                        Long itemId = item.get("id");
                        IColumn column = getColumnById(itemId);
                        if (column != null)
                        {
                            Object value = readValue(column, item.get("value"));
                            model.set(column.getDataIndex(), value);
                        }
                    }
                }

                models.add(model);
            }
        }
        BasePagingLoadResult<Hd3dModelData> result = new BasePagingLoadResult<Hd3dModelData>(models);
        result.setTotalLength(totalCount);

        return result;
    }

    private IColumn getColumnById(Long id)
    {
        return columns.get(id.toString());
    }

    private Object readValue(IColumn column, Object value)
    {
        if (column.getType().endsWith("Date") && value != null)
        {
            Date d;
            try
            {
                DateTimeFormat format = DateFormat.TIMESTAMP_FORMAT;
                d = format.parse((String) value);
            }
            catch (Exception e)
            {
                DateTimeFormat format = DateFormat.DATE_TIME;
                d = format.parse((String) value);
            }
            return d;
        }
        else if (Const.JAVA_LANG_LONG.equals(column.getType()))
        {
            if (value instanceof Double)
                return ((Double) value).longValue();
            else
                return value;
        }
        else
        {
            return value;
        }
    }

}
