package fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar;

import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.widget.ToolBarButton;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;


/**
 * 
 * a save button for the explorateur
 */
public class CsvSheetExportToolItem extends ToolBarButton
{
    /** Constant strings to display : dialog messages, button label... */
    public static final CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    /**
     * Default constructor
     */
    public CsvSheetExportToolItem()
    {
        super(Hd3dImages.getCsvExportIcon(), CONSTANTS.ExportToCsv(), ExplorerEvents.CSV_EXPORT_CLICKED);
    }

}
