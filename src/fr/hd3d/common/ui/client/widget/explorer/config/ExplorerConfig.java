package fr.hd3d.common.ui.client.widget.explorer.config;

public class ExplorerConfig
{
    /** Column size event variable name for event dispatcher. */
    public static final String COLUMN_SIZE_EVENT_VAR_NAME = "column_size";
    /** Column configuration event variable name for event dispatcher. */
    public static final String COLUMN_CONFIG_EVENT_VAR_NAME = "column_config";
    public static final String SORT_CONFIG_EVENT_VAR_NAME = "sort-info";

    /** Key for auto-save setting. */
    public static final String SETTING_AUTOSAVE = "autosave-mode";
    public static final String SHEET_EVENT_VAR_NAME = "event";
    public static final String IS_SHEET_FILTER_EVENT_VAR_NAME = "isSheetFilter";
    public static final String CONSTRAINT_STRING_EVENT_VAR_NAME = "constraintString";
    public static final String COLUMN_SORT_EVENT_VAR_NAME = "orderColumn";
}
