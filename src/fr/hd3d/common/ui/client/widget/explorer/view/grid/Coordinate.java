package fr.hd3d.common.ui.client.widget.explorer.view.grid;

public class Coordinate
{

    private final int x;
    private final int y;

    /**
     * Construct a new Coordinate from an X and Y coordinate.
     * 
     * @param initX
     *            the X coordinate.
     * @param initY
     *            the Y coordinate.
     */
    public Coordinate(int initX, int initY)
    {
        x = initX;
        y = initY;
    }

    /**
     * Get the X coordinate of the Coordinate.
     * 
     * @return the X coordinate.
     */
    public int getX()
    {
        return x;
    }

    /**
     * Get the Y coordinate of the Coordinate.
     * 
     * @return the Y coordinate.
     */
    public int getY()
    {
        return y;
    }

    /**
     * Generate a String representation of the Coordinate.
     * 
     * @return a String representation of the Coordinate.
     */
    @Override
    public String toString()
    {
        return "(" + x + "," + y + ")";
    }
}
