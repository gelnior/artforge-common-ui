package fr.hd3d.common.ui.client.widget.explorer.model;

import java.util.List;

import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.data.BaseModelData;
import com.extjs.gxt.ui.client.data.SortInfo;
import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.BaseObservable;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.Record;
import com.extjs.gxt.ui.client.util.Util;

import fr.hd3d.common.client.enums.EConstraintLogicalOperator;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.service.parameter.Constraints;
import fr.hd3d.common.ui.client.service.parameter.IUrlParameter;
import fr.hd3d.common.ui.client.service.parameter.ItemConstraints;
import fr.hd3d.common.ui.client.service.parameter.LogicConstraint;
import fr.hd3d.common.ui.client.setting.UserSettings;
import fr.hd3d.common.ui.client.widget.explorer.config.ExplorerConfig;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;
import fr.hd3d.common.ui.client.widget.explorer.event.SheetEvents;


public class ExplorerModel extends BaseObservable
{
    /** True if identity dialog is available in the explorer context menu. */
    private static boolean isIdentityDialogEnabled = false;

    /**
     * @return True if identity dialog is available in the explorer context menu.
     */
    public static boolean isIdentityDialogEnabled()
    {
        return isIdentityDialogEnabled;
    }

    /**
     * Activate or desactivate the identity dialog inside explorer.
     * 
     * @param isEnabled
     *            True if identity dialog is available in the explorer context menu.
     */
    public static void setIsIdentityDialogEnabled(boolean isEnabled)
    {
        isIdentityDialogEnabled = isEnabled;
    }

    private SheetModelData currentSheet;

    private final SheetDataRetriever retriever = new SheetDataRetriever();
    private final ExplorerSaveManager saveManager = new ExplorerSaveManager();

    private String sortColumn = "";
    private String groupColumn = "";
    private String defaultSortColumn = "";
    private String orderByColumns = "";
    private boolean isInitializing = false;
    private boolean isLoadData = true;

    public ExplorerStore getDataStore()
    {
        return retriever.getDataStore();
    }

    public SheetModelData getCurrentSheet()
    {
        return this.currentSheet;
    }

    public void setActiveView(SheetModelData sheet)
    {
        this.setActiveView(sheet, false);
    }

    public void setActiveView(SheetModelData view, boolean isLoadData)
    {
        SheetModelData sheet = view;
        this.isLoadData = isLoadData;

        if (this.currentSheet != null)
        {
            this.currentSheet.controller.removeAllListeners();
        }
        this.retriever.getDataStore().removeAll();

        this.currentSheet = sheet;
        if (currentSheet != null)
        {
            this.setActiveViewListener();
            this.currentSheet.controller.loadSheetItems();
        }
    }

    private void setActiveViewListener()
    {
        currentSheet.controller.removeAllListeners();
        currentSheet.controller.addListener(SheetEvents.ALL_ITEMS_LOADED, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                onViewInformationLoaded();
            }
        });
    }

    private void onViewInformationLoaded()
    {
        this.isInitializing = true;
        if (this.currentSheet != null && this.currentSheet.getColumns().size() > 0)
        {
            this.updateGroupColumnWithSettings();
            this.updateSortColumnWithSettings();

            AppEvent event = new AppEvent(ExplorerEvents.SHEET_INFORMATIONS_LOADED);
            event.setData(this.currentSheet.getColumns());
            event.setData(ExplorerConfig.COLUMN_SORT_EVENT_VAR_NAME, this.orderByColumns);
            EventDispatcher.forwardEvent(event);
            this.isInitializing = false;

            if (this.isLoadData)
                this.loadData("");
            else
            {
                this.isLoadData = true;
                EventDispatcher.forwardEvent(ExplorerEvents.DATA_LOADED);
            }
        }
        else
        {
            EventType eventType = ExplorerEvents.SHEET_INFORMATIONS_LOADED;
            EventDispatcher.forwardEvent(eventType, currentSheet.getColumns());
            EventDispatcher.forwardEvent(ExplorerEvents.DATA_LOADED);
        }
    }

    protected void updateGroupColumnWithSettings()
    {
        String groupKey = "explorer.";
        groupKey += "sheet.";
        groupKey += this.currentSheet.getId();
        groupKey += ".group";
        String groupColumn = UserSettings.getSetting(groupKey);
        if (!Util.isEmptyString(groupColumn) && !"null".equals(groupColumn) && this.currentSheet.isColumn(groupColumn))
        {
            this.setGroupColumn(groupColumn);
        }
        else
        {
            this.setGroupColumn(null);
            this.retriever.getDataStore().groupBy(null);
        }
    }

    protected void updateSortColumnWithSettings()
    {
        String key = "explorer.";
        key += "sheet.";
        key += this.currentSheet.getId();
        key += ".sort";
        this.orderByColumns = UserSettings.getSetting(key);
        if (Util.isEmptyString(orderByColumns)
                || (orderByColumns != null && !this.currentSheet.isColumn(orderByColumns)))
        {
            this.orderByColumns = this.currentSheet.getColumns().get(0).getId().toString();
        }
    }

    public void loadData(SortInfo sortInfo)
    {
        this.getDataStore().setSortDir(sortInfo.getSortDir());
        this.getDataStore().getLoadConfig().setSortDir(sortInfo.getSortDir());
        this.loadData(sortInfo.getSortField(), sortInfo.getSortDir());
    }

    public void loadData()
    {
        this.loadData("");
    }

    public void loadData(String sortColumn)
    {
        this.loadData(sortColumn, SortDir.ASC);
    }

    public void loadData(String sortColumn, SortDir sortDir)
    {
        this.sortColumn = sortColumn;
        this.orderByColumns = "";

        if (!this.isInitializing)
        {
            if (!Util.isEmptyString(this.groupColumn) && this.currentSheet.isColumn(groupColumn))
            {
                this.orderByColumns = this.groupColumn;
            }

            if (!Util.isEmptyString(this.sortColumn) && this.currentSheet != null
                    && this.currentSheet.isColumn(sortColumn))
            {
                if (orderByColumns.length() > 0 && !orderByColumns.endsWith(","))
                    this.orderByColumns += ",";
                if (SortDir.DESC == sortDir && !sortColumn.startsWith("-"))
                    sortColumn = "-" + sortColumn;
                this.orderByColumns += sortColumn;
            }

            if (this.currentSheet != null && !Util.isEmptyString(this.defaultSortColumn))
            {
                Long id = this.currentSheet.getColumnId(this.defaultSortColumn);
                if (id != null)
                {
                    if (orderByColumns.length() > 0 && !orderByColumns.endsWith(","))
                        this.orderByColumns += ",";
                    this.orderByColumns += String.valueOf(id);
                }
            }
            this.retriever.load(this.currentSheet, this.orderByColumns);
        }
    }

    public void save(Boolean silent)
    {
        List<Record> records = this.getDataStore().getModifiedRecords();
        this.saveManager.saveRecords(currentSheet, records, silent);
    }

    public void addParameter(IUrlParameter parameter)
    {
        this.retriever.addParameter(parameter);
    }

    public void removeParameter(IUrlParameter parameter)
    {
        this.retriever.removeParameter(parameter);
    }

    public LogicConstraint getFilters()
    {
        return retriever.getFilters();
    }

    public void setUserFilter(Constraints constraints)
    {
        LogicConstraint userFilter = new LogicConstraint();

        userFilter.setOperator(EConstraintLogicalOperator.AND);
        if (constraints.size() > 0)
        {
            userFilter.setLeftMember(constraints.get(0));

            if (constraints.size() > 1)
            {
                constraints.remove(constraints.get(0));
                this.recursivelyAddConstraints(userFilter, constraints);
            }
            this.retriever.setUserFilter(userFilter);
        }
        else
        {
            this.retriever.setUserFilter(null);
        }
    }

    public void setUserItemFilter(ItemConstraints itemConstraints)
    {
        this.retriever.setUserItemFilter(itemConstraints);
    }

    private void recursivelyAddConstraints(LogicConstraint filter, Constraints constraints)
    {
        if (constraints.size() == 1)
        {
            filter.setRightMember(constraints.get(0));
            constraints.remove(constraints.get(0));
        }
        else if (constraints.size() > 0)
        {
            LogicConstraint rightMember = new LogicConstraint();
            rightMember.setOperator(EConstraintLogicalOperator.AND);
            rightMember.setLeftMember(constraints.get(0));
            constraints.remove(constraints.get(0));

            filter.setRightMember(rightMember);
            this.recursivelyAddConstraints(rightMember, constraints);
        }
    }

    public void setApplicationFilter(LogicConstraint filter)
    {
        this.retriever.setApplicationFilter(filter);
    }

    public void refreshFilter()
    {
        this.retriever.updateFilters();
    }

    public void setLimit(int limit)
    {
        this.retriever.setLimit(limit);
    }

    public String getGroupColumn()
    {
        return this.groupColumn;
    }

    public void setGroupColumn(String column)
    {
        this.groupColumn = column;
    }

    public String getDefaultColumn()
    {
        return this.defaultSortColumn;
    }

    public void setDefaultColumn(String column)
    {
        this.defaultSortColumn = column;
    }

    public BaseModelData getAggregationModel()
    {
        return this.retriever.getAggregationModel(this.getCurrentSheet());
    }

    public String getSortColumn()
    {
        return this.sortColumn;
    }

}
