package fr.hd3d.common.ui.client.widget.explorer.view.grid.listener;

import com.extjs.gxt.ui.client.event.MenuEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.google.gwt.i18n.client.DateTimeFormat;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.ExplorerGridView;


public class DateMenuListener extends SelectionListener<MenuEvent>
{
    private ColumnConfig column;

    public DateMenuListener(ColumnConfig column)
    {
        this.column = column;
    }

    @Override
    public void componentSelected(MenuEvent ce)
    {
        column.setDateTimeFormat((DateTimeFormat) ce.getItem().getData(ExplorerGridView.DATE_FORMAT_DATA_KEY));
        EventDispatcher.forwardEvent(ExplorerEvents.REFRESH_VIEW);
        EventDispatcher.forwardEvent(ExplorerEvents.COLUMN_DATE_FORMAT_CHANGED, column);
    }

    public void setColumn(ColumnConfig column)
    {
        this.column = column;
    }
}
