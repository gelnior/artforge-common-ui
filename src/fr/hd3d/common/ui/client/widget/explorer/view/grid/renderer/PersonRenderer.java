package fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer;

import java.util.List;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridGroupRenderer;
import com.extjs.gxt.ui.client.widget.grid.GroupColumnData;
import com.google.gwt.dom.client.Element;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.user.client.DOM;

import fr.hd3d.common.client.PersonNameUtils;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;


/**
 * PersonRenderer is a cell grid renderer that displays first and last name for a single Person Model Data object and
 * displays first and last name for list of Person Model Data.
 * 
 * @author HD3D
 */
public class PersonRenderer implements IExplorerCellRenderer<Hd3dModelData>, GridGroupRenderer
{
    /** Separator used for list rendering. */
    private String separator = ", ";

    /**
     * Set separator used for list rendering.
     * 
     * @param separator
     *            The separator to set.
     */
    public void setSeparator(String separator)
    {
        this.separator = separator;
    }

    public Object render(Hd3dModelData model, String property, ColumnData config, int rowIndex, int colIndex,
            ListStore<Hd3dModelData> store, Grid<Hd3dModelData> grid)
    {

        Element div = DOM.createDiv();
        Object obj = model.get(property);
        if (obj != null)
        {
            div.setInnerHTML(getPersonRenderedValue(obj));
        }
        div.addClassName(" cell-long-text");

        return div.getString();

    }

    public String render(GroupColumnData data)
    {
        return getPersonRenderedValue(data.gvalue);
    }

    /**
     * @param object
     *            object from which name or name list are extracted.
     * @return User name for single object and User name list for a list of object.
     */
    private String getPersonRenderedValue(Object object)
    {
        StringBuffer stringValue = new StringBuffer();
        if (object instanceof JSONObject)
        {
            stringValue.append(getUserName((JSONObject) object));
        }
        else if (object instanceof List<?>)
        {
            @SuppressWarnings("unchecked")
            List<JSONValue> users = (List<JSONValue>) object;
            for (JSONValue value : users)
            {
                if (stringValue.length() > 0)
                {
                    stringValue.append(separator);
                }
                stringValue.append(getUserName((JSONObject) value));
            }
        }
        else if (object instanceof PersonModelData)
        {
            PersonModelData person = (PersonModelData) object;
            stringValue.append(PersonNameUtils.getFullName(person.getLastName(), person.getFirstName(),
                    person.getLogin()));
        }

        return stringValue.toString();
    }

    /**
     * @param user
     *            JSON from which name is extracted.
     * @return User name from JSON Object (it means First and Last name).
     */
    protected String getUserName(JSONObject user)
    {
        JSONValue firstName = user.get(PersonModelData.PERSON_FIRST_NAME_FIELD);
        JSONValue lastName = user.get(PersonModelData.PERSON_LAST_NAME_FIELD);
        JSONValue login = user.get(PersonModelData.PERSON_LOGIN_FIELD);

        return PersonNameUtils.getFullName(lastName.isString().stringValue(), firstName.isString().stringValue(), login
                .isString().stringValue());
    }

}
