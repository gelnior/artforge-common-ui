package fr.hd3d.common.ui.client.widget.explorer.model;

import java.util.List;

import com.extjs.gxt.ui.client.widget.grid.AggregationRowConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.widget.explorer.model.modeldata.IColumn;


public interface IColumnInfoProvider extends IItemInfoProvider
{
    /**
     * get the column config info for the given column
     * 
     * @param column
     */
    public ColumnConfig getColumnConfig(IColumn column, Long viewId);

    public List<ColumnConfig> getColumnConfig(List<IColumn> columns, Long viewId);

    public AggregationRowConfig<Hd3dModelData> getAggregation();
}
