package fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.util.Util;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.extjs.gxt.ui.client.widget.grid.GroupColumnData;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskStatusMap;


/**
 * Displays status on different colored background depending on status value.
 * 
 * @author HD3D
 */
public class TaskStatusRenderer<M extends Hd3dModelData> implements GridCellRenderer<M>
{
    /** Constant strings to display : dialog messages, button label... */
    public static final CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);

    public Object render(M model, String property, ColumnData config, int rowIndex, int colIndex, ListStore<M> store,
            Grid<M> grid)
    {
        String obj = model.get(property);

        return getStatusRendered(obj);
    }

    public String render(GroupColumnData data)
    {
        return getStatusRendered((String) data.gvalue);
    }

    private String getStatusRendered(String status)
    {
        String cellCode = "";
        if (!Util.isEmptyString(status))
        {
            cellCode = status;
            if (ETaskStatus.OK.toString().equals(cellCode))
            {
                cellCode = getStringValue(ETaskStatus.OK, COMMON_CONSTANTS.Ok());
            }
            else if (ETaskStatus.WAIT_APP.toString().equals(cellCode))
            {
                cellCode = getStringValue(ETaskStatus.WAIT_APP, COMMON_CONSTANTS.WaitApproval());
            }
            else if (ETaskStatus.CANCELLED.toString().equals(cellCode))
            {
                cellCode = getStringValue(ETaskStatus.CANCELLED, COMMON_CONSTANTS.Cancelled());
            }
            else if (ETaskStatus.STAND_BY.toString().equals(cellCode))
            {
                cellCode = getStringValue(ETaskStatus.STAND_BY, COMMON_CONSTANTS.StandBy());
            }
            else if (ETaskStatus.WORK_IN_PROGRESS.toString().equals(cellCode))
            {
                cellCode = getStringValue(ETaskStatus.WORK_IN_PROGRESS, COMMON_CONSTANTS.WorkInProgress());
            }
            else if (ETaskStatus.CLOSE.toString().equals(cellCode))
            {
                cellCode = getStringValue(ETaskStatus.CLOSE, COMMON_CONSTANTS.Closed());
            }
            else if (ETaskStatus.TO_DO.toString().equals(cellCode))
            {
                cellCode = getStringValue(ETaskStatus.TO_DO, COMMON_CONSTANTS.Todo());
            }
            else if (cellCode.equals(ETaskStatus.RETAKE.toString()))
            {
                cellCode = getStringValue(ETaskStatus.RETAKE, COMMON_CONSTANTS.Retake());
            }
            else if (cellCode.equals(ETaskStatus.NEW.toString()))
            {
                cellCode = getStringValue(ETaskStatus.NEW, "-");
            }
        }
        else
        {
            cellCode += "";
        }
        return cellCode;
    }

    private String getStringValue(ETaskStatus status, String text)
    {
        return "<div style=' padding: 3px;  white-space: normal; font-weight: bold ; text-align:center; background-color:"
                + TaskStatusMap.getColorForStatus(status.toString()) + ";'>" + text + "</div>";
    }
}
