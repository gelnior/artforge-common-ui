package fr.hd3d.common.ui.client.widget.explorer.model;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.data.BaseModelData;
import com.extjs.gxt.ui.client.widget.grid.AggregationRowConfig;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.extjs.gxt.ui.client.widget.grid.GridGroupRenderer;
import com.extjs.gxt.ui.client.widget.grid.SummaryType;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;

import fr.hd3d.common.client.Editor;
import fr.hd3d.common.client.Renderer;
import fr.hd3d.common.ui.client.calendar.DateFormat;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.setting.UserSettings;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.widget.explorer.model.modeldata.IColumn;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.ExplorerColumnConfig;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.Hd3dCheckColumnConfig;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.JavascriptRenderer;


/**
 * Provides column configurations, renderers, editors for the given parameters.
 */
public class BaseColumnInfoProvider extends ItemInfoProvider implements IColumnInfoProvider
{

    /**
     * Default constructor.
     */
    public BaseColumnInfoProvider()
    {}

    public List<ColumnConfig> getColumnConfig(List<IColumn> columns, Long viewId)
    {
        this.aggregation = null;
        List<ColumnConfig> cc = new ArrayList<ColumnConfig>();
        if (columns != null)
        {
            for (IColumn column : columns)
            {
                cc.add(this.getColumnConfig(column, viewId));
            }
        }
        return cc;
    }

    public ColumnConfig getColumnConfig(IColumn column, Long viewId)
    {
        String columnName = FieldUtils.translateColumnName(column);

        ColumnConfig cc;

        // GXT requires specific column config for booleans (checkbox columns).
        if (column.getRenderer().equals(Renderer.BOOLEAN))
        {
            cc = this.getBooleanColumnConfig(column, columnName);
        }
        else
        {
            cc = this.getStandardColumn(column, columnName);
        }

        // Set column groupable if column can be rendered as a group.
        if (cc.getRenderer() instanceof GridGroupRenderer || cc.getRenderer() == null)
        {
            cc.setGroupable(true);
        }
        else
        {
            cc.setGroupable(false);
        }

        if (FieldUtils.isLong(column.getType()))
        {
            this.addAggregationRow(column);
        }

        // Column editor selection.
        CellEditor editor = this.getEditor(column.getEditor().trim(), column);
        cc.setEditor(editor);

        // Column base configuration.
        cc.setId(column.getDataIndex());
        cc.setDataIndex(column.getDataIndex());
        cc.setHeader(columnName);
        cc.setSortable(true);

        // Configuration of the column based on user settings.
        this.setColumnWidth(cc, viewId);
        this.setColumnAlignement(cc, viewId);
        this.setColumnDateFormat(cc, viewId);
        this.setColumnVisibility(cc, viewId);

        return cc;
    }

    public void addAggregationRow(IColumn column)
    {
        if (this.aggregation == null)
        {
            this.aggregation = new AggregationRowConfig<Hd3dModelData>();
            this.aggregation.setModel(new BaseModelData());
        }
        this.aggregation.setHtml(column.getDataIndex(), "0");
        this.aggregation.setSummaryType(column.getDataIndex(), SummaryType.SUM);

        // // with renderer
        // averages.setSummaryType("change", SummaryType.AVG);
        // aggregation.setRenderer(column.getDataIndex(), new AggregationRenderer<Hd3dModelData>() {
        // public Object render(Number value, int colIndex, Grid<Hd3dModelData> grid, ListStore<Hd3dModelData> store)
        // {
        // return value.doubleValue();
        // }
        // });
    }

    private ColumnConfig getStandardColumn(IColumn column, String columnName)
    {
        ExplorerColumnConfig ecc = new ExplorerColumnConfig();
        ecc.setItemId(column.getId());
        ecc.setParameter(column.getParameter());
        ecc.setType(column.getType());

        if (Renderer.SHORT_DATE.equals(column.getRenderer()))
        {
            ecc.setDateTimeFormat(DateTimeFormat.getShortDateFormat());
        }
        else if (Renderer.FRENCH_DATE.equals(column.getRenderer()))
        {
            ecc.setDateTimeFormat(DateFormat.FRENCH_DATE);
        }
        else if (Renderer.REVERSED_FRENCH_DATE.equals(column.getRenderer()))
        {
            ecc.setDateTimeFormat(DateFormat.REVERSED_FRENCH_DATE);
        }
        else if (Renderer.DATE_AND_DAY.equals(column.getRenderer()))
        {
            ecc.setDateTimeFormat(DateFormat.DATE_AND_DAY);
        }

        if (column.getRenderer().equals(" ") || column.getRenderer().equals(""))
        {
            String type = column.getType();
            if (type != null && (type.endsWith("int") || type.endsWith("Integer") || type.endsWith("Long")))
            {
                ecc.setNumberFormat(NumberFormat.getFormat(""));
            }
        }

        GridCellRenderer<?> renderer = this.findRenderer(column);
        if (renderer != null)
        {
            ecc.setRenderer(renderer);
        }

        return ecc;
    }

    /**
     * @param column
     *            Column description.
     * @param columnName
     *            The column name to display.
     * @return Specific check box column configuration.
     */
    private ColumnConfig getBooleanColumnConfig(IColumn column, String columnName)
    {
        Hd3dCheckColumnConfig checkColumn = new Hd3dCheckColumnConfig();
        checkColumn.setId(column.getDataIndex());
        checkColumn.setHeader(columnName);
        checkColumn.setWidth(50);

        if (column.getEditor().equals(Editor.BOOLEAN))
        {
            checkColumn.setEditable(true);
        }
        return checkColumn;
    }

    /**
     * Set column width : default width is 150px. If width is set in the user settings, the user width is set. Key to
     * access to the column width: explorer:sheet:{sheetId}:item:{itemLabel}:width
     * 
     * @param cc
     *            The column configuration.
     * @param sheetId
     *            The current view id.
     */
    private void setColumnWidth(ColumnConfig cc, Long viewId)
    {
        String columnWidthKey = "explorer.sheet." + viewId;
        columnWidthKey += ".item." + cc.getId() + ":width";
        String columnWidth = UserSettings.getSetting(columnWidthKey);

        if (columnWidth != null)
        {
            cc.setWidth(Integer.parseInt(columnWidth));
        }
        else
        {
            cc.setWidth(150);
        }
    }

    /**
     * Set column alignment : default is left. If alignment is set in the user settings, the user alignment is set. Key
     * to access to the column alignment: explorer:sheet:{sheetId}:item:{itemLabel}:alignment
     * 
     * @param cc
     *            The column configuration.
     * @param sheetId
     *            The current view id.
     */
    private void setColumnAlignement(ColumnConfig cc, Long viewId)
    {
        String columnAlignmentKey = "explorer.sheet." + viewId;
        columnAlignmentKey += ".item." + cc.getId() + ":alignment";
        String columnAlignment = UserSettings.getSetting(columnAlignmentKey);

        if (columnAlignment != null)
        {
            cc.setAlignment(HorizontalAlignment.valueOf(columnAlignment));
        }
        else
        {
            cc.setAlignment(HorizontalAlignment.LEFT);
        }
    }

    private void setColumnVisibility(ColumnConfig cc, Long viewId)
    {
        String columnHiddenKey = "explorer.sheet." + viewId;
        columnHiddenKey += ".item." + cc.getId() + ":hidden";
        String columnHidden = UserSettings.getSetting(columnHiddenKey);

        if (columnHidden != null)
        {
            cc.setHidden(Boolean.parseBoolean(columnHidden));
        }
    }

    private void setColumnDateFormat(ColumnConfig cc, Long viewId)
    {
        // String columnDateFormatKey = "explorer.sheet." + viewId;
        // columnDateFormatKey += ".item." + cc.getId() + ":dateFormat";
        // String columnDateFormat = UserSettings.getSetting(columnDateFormatKey);
        //
        // if (columnDateFormat != null)
        // {
        // cc.setDateTimeFormat(DateTimeFormat.getFormat(columnDateFormat));
        // }
        // else
        // {
        // cc.setDateTimeFormat(DateTimeFormat.getShortDateTimeFormat());
        // }
    }

    private GridCellRenderer<?> findRenderer(IColumn column)
    {
        String name = column.getRenderer();
        GridCellRenderer<Hd3dModelData> renderer = null;

        if (name.endsWith(".js"))
        {
            renderer = new JavascriptRenderer<Hd3dModelData>(name);
        }
        else if ((renderer = this.getRenderer(name)) != null)
        {

        }
        else if ((renderer = this.getRenderer(column.getType())) != null)
        {

        }
        return renderer;
    }
}
