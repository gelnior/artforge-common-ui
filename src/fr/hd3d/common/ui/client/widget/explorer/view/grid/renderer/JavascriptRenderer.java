package fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer;

import org.restlet.client.Request;
import org.restlet.client.Response;
import org.restlet.client.data.Method;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;


public class JavascriptRenderer<M extends Hd3dModelData> implements GridCellRenderer<M>
{
    private final String file;
    private boolean ready;
    private String javascriptRenderer = "";

    public JavascriptRenderer(String file)
    {
        this.file = file;
        this.loadJS();
    }

    private void loadJS()
    {
        String oldServicePath = RestRequestHandlerSingleton.getInstance().getServicePath();

        RestRequestHandlerSingleton.getInstance().setServicePath("");
        String url = Window.Location.getPath().substring(0, Window.Location.getPath().lastIndexOf("/")) + "/"
                + this.file;
        BaseCallback callback = new BaseCallback() {
            @Override
            public void onSuccess(Request request, Response response)
            {
                jsLoaded(response);
            }
        };
        RestRequestHandlerSingleton.getInstance().handleRequest(Method.GET, url, null, callback);
        RestRequestHandlerSingleton.getInstance().setServicePath(oldServicePath);
    }

    private void jsLoaded(Response response)
    {
        try
        {
            this.ready = true;
            javascriptRenderer = response.getEntity().getText();
        }
        catch (Exception e)
        {
            GWT.log("Error occurs while parsing javascript data", e);
        }
    }

    public boolean isReady()
    {
        return this.ready;
    }

    private static native String render(String jsonString, Object model, String property, ColumnData config) /*-{
                                                                                                             
                                                                                                             var javascriptConfig = {
                                                                                                             css :  config.@com.extjs.gxt.ui.client.widget.grid.ColumnData::css,
                                                                                                             style : config.@com.extjs.gxt.ui.client.widget.grid.ColumnData::style,
                                                                                                             cellAttr : config.@com.extjs.gxt.ui.client.widget.grid.ColumnData::cellAttr,
                                                                                                             id : config.@com.extjs.gxt.ui.client.widget.grid.ColumnData::id,
                                                                                                             name : config.@com.extjs.gxt.ui.client.widget.grid.ColumnData::name
                                                                                                             }

                                                                                                             var javascriptModel = {
                                                                                                             get : function(arg){ return model.@com.extjs.gxt.ui.client.data.BaseModelData::get(Ljava/lang/String;)(arg)}
                                                                                                             };
                                                                                                             
                                                                                                             var v = eval('(' + jsonString + ')');
                                                                                                             if( v.render )
                                                                                                             {
                                                                                                             var value = v.render( javascriptModel, property, javascriptConfig);
                                                                                                             
                                                                                                             config.@com.extjs.gxt.ui.client.widget.grid.ColumnData::css = javascriptConfig.css;
                                                                                                             config.@com.extjs.gxt.ui.client.widget.grid.ColumnData::style = javascriptConfig.style;
                                                                                                             config.@com.extjs.gxt.ui.client.widget.grid.ColumnData::cellAttr = javascriptConfig.cellAttr;
                                                                                                             config.@com.extjs.gxt.ui.client.widget.grid.ColumnData::id = javascriptConfig.id;
                                                                                                             config.@com.extjs.gxt.ui.client.widget.grid.ColumnData::name = javascriptConfig.name;
                                                                                                             
                                                                                                             return value;
                                                                                                             }
                                                                                                             
                                                                                                             return "method 'render' not found on renderer";
                                                                                                             }-*/;

    public Object render(M model, String property, ColumnData config, int rowIndex, int colIndex, ListStore<M> store,
            Grid<M> grid)
    {
        String result = "";
        result = JavascriptRenderer.render(javascriptRenderer, model, property, config);
        return result;
    }
}
