package fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar;

import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.widget.ToolBarToggleButton;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;


/**
 * Toggle button that forwards ExplorerEvents.SAVE_DATA when it is pressed, with its state as attachment.
 * 
 * @author HD3D
 */
public class AutoSaveButton extends ToolBarToggleButton
{
    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    /**
     * Default Constructor.
     */
    public AutoSaveButton()
    {
        super(Hd3dImages.getAutoSaveIcon(), "Autosave mode", ExplorerEvents.AUTO_SAVE_TOGGLE);
    }
}
