package fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer;

public interface IAggregationRenderer
{

    Object renderAggregate(Object value);

}
