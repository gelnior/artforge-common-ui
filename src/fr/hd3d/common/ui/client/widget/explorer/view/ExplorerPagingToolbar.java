package fr.hd3d.common.ui.client.widget.explorer.view;

import com.extjs.gxt.ui.client.event.SelectionChangedEvent;
import com.extjs.gxt.ui.client.event.SelectionChangedListener;
import com.extjs.gxt.ui.client.widget.form.ComboBox.TriggerAction;
import com.extjs.gxt.ui.client.widget.form.SimpleComboBox;
import com.extjs.gxt.ui.client.widget.form.SimpleComboValue;
import com.extjs.gxt.ui.client.widget.toolbar.LabelToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.PagingToolBar;
import com.extjs.gxt.ui.client.widget.toolbar.SeparatorToolItem;

import fr.hd3d.common.ui.client.config.CommonConfig;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.setting.UserSettings;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;


/**
 * Default GXT toolbar does not provide tool to select how many rows should be displayed per page. This explorer paging
 * toolbar add this feature to the base GXT paging toolbar. It allows user to select via a combo box the number of rows
 * displayed from 50 to 300.
 * 
 * @author HD3D
 */
public class ExplorerPagingToolbar extends PagingToolBar
{
    /** Combo box to chose how many rows are displayed per page. */
    protected SimpleComboBox<Number> pageSizeCombo;

    /**
     * Default constructor.
     * 
     * @param pageSize
     *            The number of rows that should be displayed by default.
     */
    public ExplorerPagingToolbar(int pageSize)
    {
        super(pageSize);
        this.setPageSizeCombo();
        this.setPageSizeFromSettings();
        this.setListeners();
    }

    /**
     * Set page size combo box at the end of the toolbar.
     */
    private void setPageSizeCombo()
    {
        this.add(new SeparatorToolItem());
        this.add(new LabelToolItem("Page size: "));

        this.pageSizeCombo = new SimpleComboBox<Number>();
        this.pageSizeCombo.setLabelSeparator("Page size");
        this.pageSizeCombo.setWidth(50);
        this.pageSizeCombo.setAllowBlank(false);
        this.pageSizeCombo.setEditable(false);
        this.pageSizeCombo.add(50);
        this.pageSizeCombo.add(100);
        this.pageSizeCombo.add(150);
        this.pageSizeCombo.add(200);
        this.pageSizeCombo.add(250);
        this.pageSizeCombo.add(300);
        this.pageSizeCombo.setTriggerAction(TriggerAction.ALL);

        SimpleComboValue<Number> value = this.pageSizeCombo.getStore().findModel("value", this.getPageSize());
        this.pageSizeCombo.setValue(value);

        this.add(pageSizeCombo);
    }

    /**
     * Get last selected value from user settings and set it inside combo box.
     */
    private void setPageSizeFromSettings()
    {
        Integer limit = UserSettings.getSettingAsInteger(CommonConfig.SETTING_PAGINATION_QUANTITY);
        if (limit != null)
        {
            pageSizeCombo.setSimpleValue(limit);
            this.pageSize = limit;
        }
    }

    /**
     * Set listener to react when page size is changed (explorer data are reloaded).
     */
    private void setListeners()
    {
        this.pageSizeCombo.addSelectionChangedListener(new SelectionChangedListener<SimpleComboValue<Number>>() {
            @Override
            public void selectionChanged(SelectionChangedEvent<SimpleComboValue<Number>> se)
            {
                int pageSize = se.getSelectedItem().getValue().intValue();
                if (loader != null)
                {
                    setPageSize(pageSize);
                    EventDispatcher.forwardEvent(ExplorerEvents.PAGE_SIZE_CHANGED, Integer.valueOf(pageSize));
                }
            }
        });
    }
}
