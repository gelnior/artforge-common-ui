package fr.hd3d.common.ui.client.widget.explorer.view.grid.editor;

import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;

import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.StepModelData;
import fr.hd3d.common.ui.client.widget.grid.editor.FieldComboBoxEditor;
import fr.hd3d.common.ui.client.widget.identitysheet.view.renderers.StepField;


/**
 * Editor allows to change step value.
 * 
 * @author HD3D
 */
public class StepEditor extends FieldComboBoxEditor
{
    /** Object edited by editor needed to update it after combo value selection. */
    protected StepModelData step;

    public StepEditor(FastMap<FieldModelData> map)
    {
        super(new StepField());

        this.setCombo();
        this.setListeners();
    }

    @Override
    public Object preProcessValue(Object value)
    {
        if (value == null)
        {
            step = null;
            return combo.getValue(0L);
        }
        else if (value instanceof Long)
        {
            step = null;
            return combo.getValue(value);
        }
        else if (value instanceof StepModelData)
        {
            step = (StepModelData) value;
            return combo.getValue(step.getEstimatedDuration());
        }
        return null;
    }

    @Override
    public Object postProcessValue(Object value)
    {
        if (value == null)
        {
            return null;
        }
        if (step == null)
        {
            FieldModelData fieldVal = (FieldModelData) value;
            return fieldVal.getValue();
        }
        else
        {
            FieldModelData fieldVal = (FieldModelData) value;
            Long estimatedDuration = (Long) fieldVal.getValue();

            StepModelData newStep = new StepModelData();
            Hd3dModelData.copy(step, newStep);
            newStep.setEstimatedDuration(estimatedDuration);

            return newStep;
        }
    }

    /**
     * Set combo editable and sort on field ID.
     */
    private void setCombo()
    {
        this.combo.setEditable(true);
        this.combo.getStore().sort(FieldModelData.ID_FIELD, SortDir.ASC);
        this.combo.getStore().setSortField(FieldModelData.ID_FIELD);
    }

    /** Set listeners that expand combobox before step edition. */
    private void setListeners()
    {
        this.addListener(Events.StartEdit, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                combo.expand();
            }
        });
    }
}
