package fr.hd3d.common.ui.client.widget.explorer.view.grid.listener;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.widget.button.ToggleButton;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;


public class AutoSaveToggleListener implements Listener<ButtonEvent>
{
    public void handleEvent(ButtonEvent be)
    {
        ToggleButton bu = (ToggleButton) be.getButton();

        if (bu.isPressed())
            EventDispatcher.forwardEvent(ExplorerEvents.SAVE_DATA);
        EventDispatcher.forwardEvent(ExplorerEvents.AUTO_SAVE_TOGGLE, bu.isPressed());
    }
}
