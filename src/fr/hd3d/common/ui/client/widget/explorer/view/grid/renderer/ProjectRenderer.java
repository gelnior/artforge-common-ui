package fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer;

import java.util.Iterator;
import java.util.List;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GroupColumnData;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONValue;


/**
 * Displays list of projects as a name list separated by commas. If there is only one group, its name is displayed only.
 * 
 * @author HD3D
 */
public class ProjectRenderer<M extends ModelData> implements IExplorerCellRenderer<M>
{
    private String separator = ", ";

    public Object render(M model, String property, ColumnData config, int rowIndex, int colIndex, ListStore<M> store,
            Grid<M> grid)
    {
        Object obj = model.get(property);
        return this.getValueFromObject(obj);
    }

    public void setSeparator(String separator)
    {
        this.separator = separator;
    }

    public String render(GroupColumnData data)
    {
        return this.getValueFromObject(data.gvalue);
    }

    private String getValueFromObject(Object obj)
    {
        StringBuffer stringValue = new StringBuffer();
        if (obj instanceof JSONObject)
        {
            JSONObject json = (JSONObject) obj;
            JSONValue name = json.get("name");
            stringValue.append(name.isString().stringValue());
        }
        else if (obj instanceof List<?>)
        {
            @SuppressWarnings("unchecked")
            List<JSONValue> users = (List<JSONValue>) obj;
            for (Iterator<JSONValue> iterator = users.iterator(); iterator.hasNext();)
            {
                if (stringValue.length() > 0)
                    stringValue.append(separator);
                Object value = iterator.next();
                stringValue.append(getValueFromObject(value) + separator);

            }
        }
        return stringValue.toString();
    }

}
