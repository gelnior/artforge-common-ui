package fr.hd3d.common.ui.client.widget.explorer.view.grid.listener;

import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.event.MenuEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.ExplorerGridView;

public class AlignmentMenuListener extends SelectionListener<MenuEvent>
{

    private ColumnConfig column;

    public AlignmentMenuListener(ColumnConfig column)
    {
        this.column = column;
    }

    @Override
    public void componentSelected(MenuEvent ce)
    {
        column.setAlignment((HorizontalAlignment) ce.getItem().getData(ExplorerGridView.ALIGNMENT_DATA_KEY));
        EventDispatcher.forwardEvent(ExplorerEvents.REFRESH_VIEW);
        EventDispatcher.forwardEvent(ExplorerEvents.COLUMN_ALIGNMENT_CHANGED, column);
    }

    public void setColumn(ColumnConfig column)
    {
        this.column = column;
    }
}
