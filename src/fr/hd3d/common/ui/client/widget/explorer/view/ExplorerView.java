package fr.hd3d.common.ui.client.widget.explorer.view;

import java.util.List;

import com.extjs.gxt.ui.client.Style.Scroll;
import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.core.El;
import com.extjs.gxt.ui.client.data.BaseModelData;
import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.data.PagingLoader;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.LoadListener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.ComponentPlugin;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.grid.CheckColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.extjs.gxt.ui.client.widget.layout.CardLayout;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.user.client.Window;

import fr.hd3d.common.client.Sheet;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.widget.MosaicList;
import fr.hd3d.common.ui.client.widget.explorer.ExplorerPanel;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;
import fr.hd3d.common.ui.client.widget.explorer.model.modeldata.IColumn;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.ExplorerGrid;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.ExplorerGridView;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.ExplorerSelectionModel;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.Hd3dCheckColumnConfig;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.IAggregationRenderer;
import fr.hd3d.common.ui.client.widget.identitydialog.IdentityDialogDisplayer;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.common.ui.client.widget.thumbnaileditor.PreviewDisplayer;
import fr.hd3d.common.ui.client.widget.thumbnaileditor.ThumbnailEditorDisplayer;


/**
 * Explorer view contains widget specific to data displaying (it is explorer without constraint panel). It contains
 * explorer grid and explorer mosaic.
 */
public class ExplorerView extends ContentPanel implements IExplorateurView
{
    /** Constant strings to display : dialog messages, button label... */
    public static final CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    /** Selection mode. */
    private final ExplorerSelectionModel selectionModel = new ExplorerSelectionModel();

    /** The layout to host the loading panel, the grid panel and the mosaic panel. */
    private final CardLayout layout = new CardLayout();
    /** An empty panel to display nothing when no sheet is selected. */
    private final LayoutContainer empty = new LayoutContainer();
    /** Panel that contains explorer grid. */
    protected final ExplorerPanel parent;
    /** the grid panel */
    private ExplorerGrid grid;
    /** Toolbar associated to grid for pagination purpose. */
    private final ExplorerPagingToolbar pagingToolbar = new ExplorerPagingToolbar(50);
    /** Panel used to display mosaic list. */
    private MosaicList mosaic;

    /** True if mosaic mode is on. */
    private boolean isMosaicMode;

    /**
     * Default constructor
     */
    public ExplorerView(ExplorerPanel parent)
    {
        this.setStyles();

        this.parent = parent;

        layout.setActiveItem(empty);
        this.hideLoading();
    }

    /**
     * Get grid on which explorer is based.
     */
    public ExplorerGrid getGrid()
    {
        return this.grid;
    }

    /**
     * @return Name of first column.
     */
    public String getFirstColumnName()
    {
        for (IColumn column : this.parent.getCurrentSheet().getColumns())
        {
            if (Sheet.ITEMTYPE_ATTRIBUTE.equals(column.getType()))
            {
                return column.getName();
            }
        }
        return null;
    }

    /**
     * @return Number of page needed to display all data.
     */
    public int getTotalPages()
    {
        return this.pagingToolbar.getTotalPages();
    }

    /**
     * Show the loading message.
     */
    public void showLoading()
    {
        this.isMosaicMode = this.layout.getActiveItem() == mosaic;
        this.mask(CONSTANTS.Loading());
    }

    /**
     * Hide the loading message.
     */
    public void hideLoading()
    {
        this.unmask();
        this.pagingToolbar.enable();
    }

    /**
     * Display empty panel instead of grid.
     */
    public void hideGrid()
    {
        this.layout.setActiveItem(empty);
    }

    /**
     * Rebuild grid with a new store and a new column model. if grid does not exists, it is created from scratch.
     */
    public void showGrid(ColumnModel cm, ListStore<Hd3dModelData> store)
    {

        if (grid == null)
        {
            this.createGrid(cm, store);
        }
        else
        {
            this.rebuildGrid(cm, store);
            this.mosaic.setSheet(this.parent.getCurrentSheet());
        }

        if (this.isMosaicMode)
        {
            layout.setActiveItem(mosaic);
        }
        else
        {
            layout.setActiveItem(grid);
        }
        ((ExplorerGridView) this.grid.getView()).resetSortIcon();

        EventDispatcher.forwardEvent(ExplorerEvents.NEW_GRID_BUILT);
    }

    /**
     * Rebuild grid with a new store and a new column model.
     * 
     * @param cm
     *            The new column model.
     * @param store
     *            The new store.
     */
    private void rebuildGrid(ColumnModel cm, ListStore<Hd3dModelData> store)
    {
        for (ColumnConfig cc : this.grid.getColumnModel().getColumns())
        {
            if (cc instanceof Hd3dCheckColumnConfig)
                grid.removeListener(Events.CellMouseDown, ((Hd3dCheckColumnConfig) cc).getListener());
        }
        this.addCheckPluginIfNeeded(cm);
        this.grid.reconfigure(store, cm);
    }

    /**
     * Re render explorer grid.
     */
    public void refresh()
    {
        this.grid.getView().refresh(false);
    }

    /**
     * Return selected rows (mosaic selected rows if mosaic is active, else grid selected rows).
     */
    public List<Hd3dModelData> getSelectedRows()
    {
        if (mosaic == layout.getActiveItem())
        {
            return this.mosaic.getSelectionModel().getSelectedItems();
        }
        else if (this.grid != null && this.grid.getSelectionModel() != null)
        {
            return this.grid.getSelectionModel().getSelectedItems();
        }
        else
        {
            return null;
        }
    }

    /**
     * Reset grid selection.
     */
    public void resetSelection()
    {
        this.selectionModel.reset();
    }

    /**
     * Sort grid data.
     */
    public void gridViewSort(int column, SortDir sortDir)
    {
        ((ExplorerGridView) this.grid.getView()).localSort(column, sortDir);
    }

    /**
     * Open CSV service via a new navigator window.
     */
    public void callCSVService(String path)
    {
        path = path.replace("[", "%5B");
        path = path.replace("]", "%5D");
        Window.open(path, CONSTANTS.CSVExport(), "");
    }

    /** Select first page. */
    public void selectFirstPage()
    {
        this.pagingToolbar.first();
    }

    /**
     * Display thumbnail editor dialog for first selected model. If no model is selected, nothing happens.
     */
    public void showThumbnailEditor()
    {
        Hd3dModelData model = this.parent.getFirstSelectedModel();
        if (model != null)
        {
            model.setSimpleClassName((this.parent.getCurrentSheet()).getBoundClassName());

            ThumbnailEditorDisplayer.display(model);
        }
    }

    /**
     * When a thumbnail has been newly set, the thumbnail is refreshed.
     */
    public void refreshEditedThumbnail()
    {
        this.parent.recalculateThumbnail(this.getGrid().getStore()
                .findModel(Hd3dModelData.ID_FIELD, ThumbnailEditorDisplayer.getSelectedModel().getId()));
    }

    /** Show thumbnail at preview size (bigger) in a dialog. */
    public void showPreviewInDialog(String path)
    {
        PreviewDisplayer.displayPreview(path);
    }

    /**
     * Display mosaic instead of grid.
     */
    public void showMosaic()
    {
        SheetModelData sheet = this.parent.getCurrentSheet();

        if (sheet != null)
        {
            this.mosaic.setSheet(sheet);
            this.layout.setActiveItem(mosaic);
        }
    }

    /** Display grid instead of mosaic. */
    public void hideMosaic()
    {
        layout.setActiveItem(grid);
    }

    /** Display identity widget inside a dialog. */
    public void showIdentityWidget(Hd3dModelData model, SheetModelData sheet)
    {
        // IdentitySheetDisplayer.displayIdentity(sheet, model.getId());
        model.setSimpleClassName(sheet.getBoundClassName());
        IdentityDialogDisplayer.display(model, MainModel.getCurrentProject());
    }

    /** Send grid to for printing inside browser. */
    public void print()
    {
        if (rendered)
        {
            JavaScriptObject jsobj = open("", CONSTANTS.Print(), "");
            String css = getCss();

            El grid = this.grid.getView().getBody();
            grid = grid.getChild(0);
            El headers = this.grid.getView().getHeader().getContainer().el();
            headers.setStyleAttribute("height", "100%");
            headers.setStyleAttribute("position", "static");
            headers.getChild(0).setStyleAttribute("height", "100%");
            headers.getChild(0).setStyleAttribute("position", "static");
            headers.getChild(0).setStyleAttribute("overflow", "visible");
            headers.getChild(0).getChild(0).setStyleAttribute("height", "100%");
            headers.getChild(0).getChild(0).setStyleAttribute("position", "static");
            headers.getChild(0).getChild(0).setStyleAttribute("overflow", "visible");
            headers.getChild(0).getChild(0).getChild(1).setStyleAttribute("height", "100%");
            headers.getChild(0).getChild(0).getChild(1).setStyleAttribute("position", "static");
            headers.getChild(0).getChild(0).getChild(1).setStyleAttribute("overflow", "visible");
            String html = "<html><head>" + css + "</head><body >" + headers.getInnerHtml() + "</body>";

            setDocInnerHtml(jsobj, html);
            printDoc(jsobj);
        }
    }

    /**
     * Set tool bar page. Reload data if page change. Does nothing if page is the same as before.
     * 
     * @param i
     *            Page index.
     */
    public void setPagingPage(int i)
    {
        this.pagingToolbar.setActivePage(i);
    }

    /**
     * Get tool bar page index.
     */
    public int getPagingPage()
    {
        return this.pagingToolbar.getActivePage();
    }

    /**
     * Check if columns contains a boolean column. If yes, it adds the check column config to the grid.
     * 
     * @param cm
     *            The column model that contains columns.
     */
    protected void addCheckPluginIfNeeded(ColumnModel cm)
    {
        for (ColumnConfig cc : cm.getColumns())
        {
            if (cc instanceof CheckColumnConfig)
                ((ComponentPlugin) cc).init(grid);
        }
    }

    /**
     * @return All CSS sheets needed.
     */
    private String getCss()
    {
        String css = "";

        String href = Window.Location.getHref();
        String[] urlPart = href.split("/");
        StringBuilder url = new StringBuilder();
        for (int i = 0; i < urlPart.length - 1; i++)
        {
            url.append(urlPart[i] + "/");
        }
        NodeList<Element> cssLinks = Document.get().getElementsByTagName("link");
        int size = cssLinks.getLength();
        for (int i = 0; i < size; i++)
        {
            css += "<link rel='stylesheet' type='text/css' href='" + url.toString()
                    + cssLinks.getItem(i).getAttribute("href") + "' />";
        }
        return css;
    }

    /**
     * Create the explorer grid.
     * 
     * @param cm
     * @param store
     */
    private void createGrid(ColumnModel cm, ListStore<Hd3dModelData> store)
    {
        if (store != null && cm != null)
        {
            this.grid = new ExplorerGrid(store, cm, this.selectionModel);
            this.mosaic = new MosaicList(this.grid.getStore());

            this.pagingToolbar.bind((PagingLoader<?>) store.getLoader());
            this.setStoreListener(store);
            this.addCheckPluginIfNeeded(cm);

            this.add(grid);
            this.add(mosaic);
        }
        else
        {
            layout.setActiveItem(empty);
        }
    }

    /**
     * Set grid listeners : when store is loading, it asks for displaying a loading message.
     * 
     * @param store
     */
    private void setStoreListener(ListStore<Hd3dModelData> store)
    {
        store.getLoader().addLoadListener(new LoadListener() {
            @Override
            public void loaderBeforeLoad(LoadEvent le)
            {
                showLoading();
            }

            @Override
            public void loaderLoad(LoadEvent le)
            {
                hideLoading();
            }

            @Override
            public void loaderLoadException(LoadEvent le)
            {
                hideLoading();
            }
        });
    }

    /**
     * Set grid styles and grid panel styles.
     */
    private void setStyles()
    {
        this.setBorders(false);
        this.setBodyBorder(false);
        this.setHeaderVisible(false);
        this.setScrollMode(Scroll.NONE);

        this.setLayout(layout);
        this.add(empty);
        this.empty.show();

        this.pagingToolbar.getItem(10).setId("refresh-explorer-button");
        this.setBottomComponent(pagingToolbar);
    }

    private static native Document open(String url, String name, String features) /*-{
                                                                                  var wnd = $wnd.open(url, name, features);
                                                                                  return wnd;
                                                                                  }-*/;

    private static native void setDocInnerHtml(JavaScriptObject wnd, String innerHtml) /*-{
                                                                                       //doc.body.innerHTML = innerHtml;
                                                                                       wnd.document.write(innerHtml);
                                                                                       }-*/;

    private static native void printDoc(JavaScriptObject wnd) /*-{
                                                              var javascript = " <script> "
                                                              javascript += "function printFunction(){";
                                                              javascript += "    window.print();";
                                                              javascript += "}";
                                                              javascript += "t = setTimeout('printFunction()', 6);";
                                                              javascript += "</script>";
                                                              wnd.document.write(javascript);
                                                              }-*/;

    public void setSortColumn(String sortColumn)
    {
        this.grid.setSortColumn(sortColumn);
    }

    public void applyGrouping(String groupColumn)
    {
        this.grid.setGroupColumn(groupColumn);
    }

    public void displaySum(BaseModelData aggregationModel)
    {
        if (this.grid.getColumnModel() != null
                && CollectionUtils.isNotEmpty(this.grid.getColumnModel().getAggregationRows()))
        {
            for (String key : aggregationModel.getProperties().keySet())
            {
                Object value = aggregationModel.get(key);
                for (ColumnConfig column : this.grid.getColumnModel().getColumns())
                {
                    if (column.getDataIndex().equals(key))
                    {
                        GridCellRenderer<?> renderer = column.getRenderer();
                        if (renderer instanceof IAggregationRenderer)
                            value = ((IAggregationRenderer) renderer).renderAggregate(value);
                    }
                }

                if (value != null)
                {
                    this.grid.getColumnModel().getAggregationRow(0).setHtml(key, value.toString());
                }
            }
            this.grid.reconfigure(this.grid.getStore(), this.grid.getColumnModel());
        }
    }
}
