package fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;


public class PourcentRenderer implements GridCellRenderer<Hd3dModelData>
{
    private final static String POURCENT_CSS = " pourcent-";

    public Object render(Hd3dModelData model, String property, ColumnData config, int rowIndex, int colIndex,
            ListStore<Hd3dModelData> store, Grid<Hd3dModelData> grid)
    {
        Double value = model.get(property);

        value *= 10;
        Long roundedValue = Math.round(value);
        roundedValue *= 10;

        config.css = this.setCss(config.css, roundedValue);

        return (value * 10) + "%";
    }

    public String setCss(String css, Long pourcent)
    {
        int pos = css.indexOf(POURCENT_CSS);
        if (pos > -1)
        {
            int end = css.indexOf(" ", pos + 2);
            String search = css.substring(pos, end);
            css = css.replaceAll(search, "");
        }

        css += POURCENT_CSS + pourcent + " ";
        return css;
    }
}
