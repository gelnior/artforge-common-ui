package fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.store.Record;
import com.extjs.gxt.ui.client.widget.BoxComponent;
import com.extjs.gxt.ui.client.widget.ColorPalette;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.extjs.gxt.ui.client.widget.menu.ColorMenu;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;


public class ColorRenderer implements GridCellRenderer<Hd3dModelData>
{
    private boolean init;
    private ListStore<Hd3dModelData> store;
    private String selectedColor = null;

    public Object render(final Hd3dModelData model, String property, ColumnData config, final int rowIndex,
            final int colIndex, ListStore<Hd3dModelData> store, Grid<Hd3dModelData> grid)
    {

        this.store = store;

        if (!init)
        {
            init = true;
            grid.addListener(Events.ColumnResize, new Listener<GridEvent<Hd3dModelData>>() {

                public void handleEvent(GridEvent<Hd3dModelData> be)
                {
                    for (int i = 0; i < be.getGrid().getStore().getCount(); i++)
                    {
                        if (be.getGrid().getView().getWidget(i, be.getColIndex()) != null
                                && be.getGrid().getView().getWidget(i, be.getColIndex()) instanceof BoxComponent)
                        {
                            ((BoxComponent) be.getGrid().getView().getWidget(i, be.getColIndex())).setWidth(be
                                    .getWidth() - 10);
                        }
                    }
                }
            });
        }

        String stringValue = "<div style='width:100%; height:100%; text-align:center; background-color:"
                + (String) model.get(property) + ";'>" + "&nbsp;" + "</div>";

        final Button b = new Button(stringValue, new SelectionListener<ButtonEvent>() {
            @Override
            public void componentSelected(ButtonEvent ce)
            {}
        });

        ColorMenu menu = new ColorMenu();
        menu.getColorPalette().addListener(Events.Select, new Listener<ComponentEvent>() {

            public void handleEvent(ComponentEvent be)
            {
                selectedColor = ((ColorPalette) be.getComponent()).getValue();

                update(model, selectedColor);

                b.setText("<div style='width:100%; height:100%; text-align:center; background-color:" + selectedColor
                        + ";'>" + "&nbsp;" + "</div>");
            }
        });

        b.setMenu(menu);
        b.setWidth(grid.getColumnModel().getColumnWidth(colIndex) - 10);
        b.setToolTip("Click for change color");
        b.setWidth(30);

        return b;
    }

    private void update(Hd3dModelData theModel, String theSelectedColor)
    {
        Record rec = store.getRecord(theModel);
        if (!selectedColor.startsWith("#"))
            theSelectedColor = "#" + theSelectedColor;
        rec.set(ProjectModelData.PROJECT_COLOR, "" + theSelectedColor);
        rec.setDirty(true);
    }

};
