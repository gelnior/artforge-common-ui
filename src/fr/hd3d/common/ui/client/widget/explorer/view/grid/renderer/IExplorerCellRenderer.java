package fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;


public interface IExplorerCellRenderer<M extends ModelData> extends GridCellRenderer<M>
{

}
