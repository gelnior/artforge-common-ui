package fr.hd3d.common.ui.client.widget.explorer.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.restlet.client.Request;
import org.restlet.client.Response;
import org.restlet.client.data.Method;

import com.extjs.gxt.ui.client.data.ModelType;
import com.extjs.gxt.ui.client.event.BaseObservable;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.modeldata.technical.ItemGroupModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.widget.explorer.event.ItemGroupEvents;
import fr.hd3d.common.ui.client.widget.explorer.model.modeldata.IColumn;
import fr.hd3d.common.ui.client.widget.explorer.model.reader.ItemReader;


public class ItemGroupController extends BaseObservable
{
    private final ItemGroupModelData itemGroup;

    public ItemGroupController(ItemGroupModelData itemGroup)
    {
        this.itemGroup = itemGroup;
    }

    public void loadItems()
    {
        SheetModelData sheet = itemGroup.getSheet();
        if (sheet != null)
        {
            String url = ServicesPath.SHEETS + this.itemGroup.getSheetId().toString() + "/" + ServicesPath.ITEM_GROUPS
                    + this.itemGroup.getId().toString() + "/" + ServicesPath.ITEMS;

            if (sheet != null && sheet.getProjectId() != null)
                url = ServicesPath.PROJECTS + sheet.getProjectId() + "/" + url;

            BaseCallback callback = new BaseCallback() {
                @Override
                public void onSuccess(Request request, Response response)
                {
                    onItemGroupItemsLoaded(request, response);
                }
            };
            RestRequestHandlerSingleton.getInstance().handleRequest(Method.GET, url, null, callback);
        }
    }

    private void onItemGroupItemsLoaded(Request request, Response response)
    {
        try
        {
            String json = response.getEntity().getText();
            ModelType itemModelType = ItemModelData.getModelType();
            ItemReader reader = new ItemReader(itemModelType);

            List<ItemModelData> result = reader.read(json).getData();
            List<IColumn> columnResult = new ArrayList<IColumn>();

            for (ItemModelData item : result)
            {
                columnResult.add(item);
            }
            itemGroup.setColumns(columnResult);

            for (Iterator<?> itemIterator = result.iterator(); itemIterator.hasNext();)
            {
                ItemModelData item = (ItemModelData) itemIterator.next();
                item.setItemGroup(this.itemGroup);
            }

            this.fireEvent(ItemGroupEvents.ITEMS_LOADED);
        }
        catch (Exception e)
        {
            GWT.log("Error occurs while parsing item data.", e);
        }
    }
}
