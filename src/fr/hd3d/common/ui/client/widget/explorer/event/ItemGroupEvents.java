package fr.hd3d.common.ui.client.widget.explorer.event;

import com.extjs.gxt.ui.client.event.EventType;



public class ItemGroupEvents
{
    private static final int ITEM_GROUP_START_EVENT = ExplorerEvents.EXPLORATEUR_START_EVENT + 400;

    public static final EventType ITEMS_LOADED = new EventType (ITEM_GROUP_START_EVENT + 1);
}
