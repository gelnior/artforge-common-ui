package fr.hd3d.common.ui.client.widget.explorer.model.reader;

import fr.hd3d.common.ui.client.modeldata.reader.Hd3dListJsonReader;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;


public class SheetReader extends Hd3dListJsonReader<SheetModelData>
{

    public SheetReader()
    {
        super(SheetModelData.getModelType());
    }

    @Override
    public SheetModelData newModelInstance()
    {
        return new SheetModelData();
    }

}
