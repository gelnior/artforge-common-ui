package fr.hd3d.common.ui.client.widget.explorer.model;

import org.restlet.client.data.Method;

import com.extjs.gxt.ui.client.data.DataReader;
import com.extjs.gxt.ui.client.data.ListLoadConfig;
import com.extjs.gxt.ui.client.data.PagingLoadConfig;
import com.extjs.gxt.ui.client.data.PagingLoadResult;
import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.parameter.ItemOrderBy;
import fr.hd3d.common.ui.client.service.proxy.ServicesPagingProxy;


/**
 * Explorer need a specific proxy to handle sorting on item ID and not on column name.
 * 
 * @author HD3D
 * 
 */
public class ExplorerProxy extends ServicesPagingProxy<Hd3dModelData>
{
    /** Parameter used to sort data on services side. */
    protected ItemOrderBy itemOrderBy = new ItemOrderBy(0L);

    /**
     * Configures the sort proxy to retrieve data sort by
     */
    @Override
    protected void applySort(ListLoadConfig config)
    {
        itemOrderBy.clearColumns();
        if (config.getSortField() != null && config.getSortField().length() > 0)
        {
            String[] columns = config.getSortField().split(",");

            for (String column : columns)
            {

                if (!isDigit(column))
                {
                    Logger.log("Sorted column is not an item ID. Explorer store expects only item ID for sorting.",
                            new Exception());
                }
                else
                {
                    Long itemId = Long.valueOf(column);
                    itemOrderBy.addColumn(itemId);
                }
            }
            if (!this.parameters.contains(itemOrderBy) && !itemOrderBy.isEmpty())
            {
                this.parameters.add(itemOrderBy);
            }
            else
            {
                this.parameters.remove(itemOrderBy);
            }
        }
    }

    /**
     * @param string
     *            String to analyze.
     * @return True if string is only composed of numbers (digits).
     */
    private boolean isDigit(String string)
    {
        for (int i = 0; i < string.length(); i++)
        {
            boolean isDigit = Character.isDigit(string.charAt(i));
            boolean isMinus = string.charAt(i) == '-';
            if (!isDigit && !isMinus)
                return false;
        }
        return true;
    }

    /**
     * Remove order by and item order by parameters to the parameter list. By this way sorting does not interfere in
     * external parameter handling.
     */
    @Override
    protected void removeSort()
    {
        super.removeSort();
        if (this.parameters.contains(itemOrderBy))
        {
            this.parameters.remove(itemOrderBy);
        }
    }

    @Override
    protected void sendRequest(String paginatedPath, DataReader<PagingLoadResult<Hd3dModelData>> reader,
            AsyncCallback<PagingLoadResult<Hd3dModelData>> callback, PagingLoadConfig config)
    {
        IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
        requestHandler.handleRequest(Method.GET, paginatedPath, null, new ExplorerProxyCallback(reader, callback,
                config.getOffset()));
    }
}
