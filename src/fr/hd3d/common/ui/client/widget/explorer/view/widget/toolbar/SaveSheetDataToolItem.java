package fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.ExplorateurCSS;


/**
 * 
 * a save button for the explorateur
 */
public class SaveSheetDataToolItem extends Button
{
    /** Constant strings to display : dialog messages, button label... */
    public static final CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    /**
     * Default constructor
     */
    public SaveSheetDataToolItem()
    {
        super();
        initButton();
    }

    /**
     * initialize the class button and listeners
     */
    private void initButton()
    {
        this.setIconStyle(ExplorateurCSS.SAVE_CLASS);
        this.getElement().setNodeValue(ExplorateurCSS.SAVE_CLASS);

        this.addListener(Events.Select, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                EventDispatcher.forwardEvent(ExplorerEvents.SAVE_DATA);
            }
        });

        this.setToolTip(CONSTANTS.Save());
    }
}
