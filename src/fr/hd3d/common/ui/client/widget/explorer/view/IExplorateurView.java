package fr.hd3d.common.ui.client.widget.explorer.view;

import java.util.List;

import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.data.BaseModelData;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;


public interface IExplorateurView
{

    public void showLoading();

    public void hideLoading();

    public void showGrid(ColumnModel cm, ListStore<Hd3dModelData> store);

    public void refresh();

    public List<Hd3dModelData> getSelectedRows();

    public void resetSelection();

    public void print();

    public void selectFirstPage();

    public void hideGrid();

    public String getFirstColumnName();

    public void gridViewSort(int intValue, SortDir sortDir);

    public int getTotalPages();

    public void callCSVService(String path);

    public void showThumbnailEditor();

    public void refreshEditedThumbnail();

    public void showPreviewInDialog(String path);

    public void showMosaic();

    public void hideMosaic();

    public void showIdentityWidget(Hd3dModelData hd3dModelData, SheetModelData activeView);

    public void setSortColumn(String sortColumn);

    public void applyGrouping(String groupColumn);

    public void displaySum(BaseModelData aggregationModel);

}
