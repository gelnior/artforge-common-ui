package fr.hd3d.common.ui.client.widget.explorer.model;

import com.extjs.gxt.ui.client.widget.form.DateField;
import com.extjs.gxt.ui.client.widget.form.Field;
import com.extjs.gxt.ui.client.widget.form.NumberField;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.grid.AggregationRowConfig;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.google.gwt.i18n.client.NumberFormat;

import fr.hd3d.common.client.Editor;
import fr.hd3d.common.client.Renderer;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.widget.FieldComboBox;
import fr.hd3d.common.ui.client.widget.explorer.model.modeldata.IColumn;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.BoldRenderer;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.CategoryRenderer;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.GroupRenderer;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.LinkRenderer;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.LoginPersonRenderer;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.PersonRenderer;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.PourcentRenderer;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.ProjectRenderer;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.SequenceRenderer;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.TagRenderer;
import fr.hd3d.common.ui.client.widget.grid.editor.FieldComboBoxEditor;
import fr.hd3d.common.ui.client.widget.grid.editor.TextAreaEditor;
import fr.hd3d.common.ui.client.widget.grid.renderer.BigTextRenderer;
import fr.hd3d.common.ui.client.widget.grid.renderer.LongRenderer;
import fr.hd3d.common.ui.client.widget.grid.renderer.LongTextRenderer;
import fr.hd3d.common.ui.client.widget.grid.renderer.RecordModelDataRenderer;


public class ItemInfoProvider implements IItemInfoProvider
{

    /** List of aggregations to display for given config. */
    protected AggregationRowConfig<Hd3dModelData> aggregation = null;

    public AggregationRowConfig<Hd3dModelData> getAggregation()
    {
        return this.aggregation;
    }

    public CellEditor getEditor(String name, IColumn column)
    {
        Field<?> field = null;

        if (Editor.SHORT_TEXT.equals(name))
        {
            field = new TextField<String>();
        }
        else if (Editor.LONG_TEXT.equals(name))
        {
            return new TextAreaEditor();
        }
        else if (Editor.DATE.equals(name))
        {
            field = new DateField();
        }
        else if (Editor.NUMBER_EDITOR.equals(name))
        {
            field = new NumberField();
        }
        else if (Editor.INT.equals(name))
        {
            NumberField nfield = new NumberField();
            nfield.setFormat(NumberFormat.getFormat(""));
            nfield.setAllowDecimals(false);
            field = nfield;
        }
        else if (Editor.FLOAT.equals(name))
        {
            NumberField nfield = new NumberField();
            nfield.setAllowDecimals(true);
            field = nfield;
        }
        else if (Editor.LIST.equals(name))
        {
            FieldComboBox comboBox = new FieldComboBox();
            if (column.getListValues() != null)
            {
                int index = 0;
                for (String listValue : column.getListValues())
                {
                    comboBox.addField(index++, listValue, listValue);
                }
            }
            FieldComboBoxEditor comboBoxEditor = new FieldComboBoxEditor(comboBox);
            return comboBoxEditor;

        }
        if (field != null)
        {
            return new CellEditor(field);
        }
        return null;
    }

    public GridCellRenderer<Hd3dModelData> getRenderer(String name)
    {
        GridCellRenderer<Hd3dModelData> renderer = null;

        if (name == null)
        {
            return null;
        }
        else if (name.equals(CategoryModelData.SIMPLE_CLASS_NAME))
        {
            renderer = new CategoryRenderer<Hd3dModelData>();
        }
        else if (name.equals(Renderer.BOLD))
        {
            renderer = new BoldRenderer<Hd3dModelData>();
        }
        else if (name.equals(Renderer.BIG_TEXT))
        {
            renderer = new BigTextRenderer<Hd3dModelData>();
        }
        else if (name.equals(ProjectModelData.SIMPLE_CLASS_NAME))
        {
            renderer = new ProjectRenderer<Hd3dModelData>();
        }
        else if (name.equals(PersonModelData.SIMPLE_CLASS_NAME))
        {
            renderer = new PersonRenderer();
        }
        else if (name.equals(SequenceModelData.SIMPLE_CLASS_NAME))
        {
            renderer = new SequenceRenderer<Hd3dModelData>();
        }
        else if (name.equals(Renderer.HYPERLINK))
        {
            renderer = new LinkRenderer<Hd3dModelData>();
        }
        else if (name.equals(Renderer.GROUP))
        {
            renderer = new GroupRenderer<Hd3dModelData>();
        }
        else if (name.equals(Renderer.TAG))
        {
            renderer = new TagRenderer();
        }
        else if (name.equals(Renderer.POURCENT))
        {
            renderer = new PourcentRenderer();
        }
        else if (name.equals(Renderer.LONG_TEXT))
        {
            renderer = new LongTextRenderer<Hd3dModelData>();
        }
        else if (name.equals(Renderer.INT))
        {
            renderer = new LongRenderer<Hd3dModelData>();
        }
        else if (name.equals(Renderer.PERSON_LOGIN))
        {
            renderer = new LoginPersonRenderer();
        }
        else if (FieldUtils.isEntity(name))
        {
            renderer = new RecordModelDataRenderer();
        }
        return renderer;
    }

}
