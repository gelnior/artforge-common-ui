package fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.util.Util;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;

import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;


/**
 * For a given task, display the task above a colored background where color corresponds to type color.
 * 
 * @author HD3D
 */
public class TaskTypeRenderer implements GridCellRenderer<TaskModelData>
{
    public Object render(TaskModelData model, String property, ColumnData config, int rowIndex, int colIndex,
            ListStore<TaskModelData> store, Grid<TaskModelData> grid)
    {
        String obj = model.get(property);
        String color = model.get(TaskModelData.COLOR_FIELD);

        return getTypeRendered(obj, color);
    }

    public static String getTypeRendered(String type, String color)
    {
        String cellCode = "";
        if (!Util.isEmptyString(type))
        {
            cellCode = type;
            if (!Util.isEmptyString(color))
            {
                cellCode = "<div style='padding: 3px; font-weight: bold; text-align:center; background-color:" + color
                        + ";'>" + type + "</div>";
            }
            else
            {
                cellCode = "<div style='padding: 3px; font-weight: bold; text-align:center; background-color:white;'>"
                        + type + "</div>";
            }
        }
        else
        {
            cellCode += "";
        }
        return cellCode;
    }

    public static String getTypeRendered(String type, String color, String textColor)
    {
        if (Util.isEmptyString(textColor))
            return getTypeRendered(type, color);

        String cellCode = "";

        if (!Util.isEmptyString(type))
        {
            cellCode = type;
            if (!Util.isEmptyString(color))
            {
                cellCode = "<div style='padding: 3px; font-weight: bold; text-align:center; background-color:" + color
                        + "; color:" + textColor + ";'>" + type + "</div>";
            }
            else
            {
                cellCode = "<div style='padding: 3px; font-weight: bold; text-align:center; background-color:white; color:"
                        + textColor + ";'>" + type + "</div>";
            }
        }
        else
        {
            cellCode += "";
        }
        return cellCode;
    }

}
