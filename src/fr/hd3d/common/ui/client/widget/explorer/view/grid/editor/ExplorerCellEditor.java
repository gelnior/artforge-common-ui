package fr.hd3d.common.ui.client.widget.explorer.view.grid.editor;

import com.extjs.gxt.ui.client.widget.form.Field;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.google.gwt.json.client.JSONObject;


public class ExplorerCellEditor extends CellEditor
{

    public ExplorerCellEditor(Field<?> field)
    {
        super(field);
    }

    @Override
    public Object preProcessValue(Object value)
    {
        if (getField() instanceof IComplexModelEditor<?>)
        {
            if (value instanceof JSONObject)
            {
                IComplexModelEditor<?> combo = (IComplexModelEditor<?>) getField();
                return combo.getValueFromJSON((JSONObject) value);
            }
        }
        return value;
    }
}
