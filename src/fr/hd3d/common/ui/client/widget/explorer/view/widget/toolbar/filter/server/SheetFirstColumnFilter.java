package fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.filter.server;

import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.FieldEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.form.TriggerField;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;


/**
 * 
 * a field to add filter by the first column value
 */

public class SheetFirstColumnFilter extends TriggerField<String>
{
    // /** the trigger field */
    // private TriggerField<String> filterField;

    /**
     * Default constructor
     */
    public SheetFirstColumnFilter()
    {
        super();
        createFilterField();
        addFieldListener();
    }

    private void addFieldListener()
    {
        this.addListener(Events.Valid, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent be)
            {
                AppEvent event = new AppEvent(ExplorerEvents.FIRST_COLUMN_FILTER_UPDATE, getValue());
                EventDispatcher.forwardEvent(event);
            }
        });

        this.addListener(Events.TriggerClick, new Listener<FieldEvent>() {
            public void handleEvent(FieldEvent be)
            {
                setValue(null);
            }
        });
    }

    private void createFilterField()
    {
        this.setAutoValidate(true);
        this.setValidateOnBlur(false);
        this.setTriggerStyle("x-form-clear-trigger");

        this.setToolTip("Filter data");
    }
    //
    // @Override
    // protected void onRender(Element target, int index)
    // {
    // super.onRender(target, index);
    // ((Component) filterField).render(target, index);
    // setElement(filterField.getElement(), target, index);
    // }
    //
    // @Override
    // protected void doAttachChildren()
    // {
    // super.doAttachChildren();
    // ComponentHelper.doAttach(filterField);
    // }
    //
    // @Override
    // protected void doDetachChildren()
    // {
    // super.doDetachChildren();
    // ComponentHelper.doDetach(filterField);
    // }
}
