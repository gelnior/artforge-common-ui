package fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer;

import java.util.List;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.google.gwt.json.client.JSONObject;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.technical.TagModelData;


/**
 * Displays list of tags as a name list separated by commas. If there is only one tag, its name is displayed only.
 * 
 * @author HD3D
 */
public class TagRenderer implements GridCellRenderer<Hd3dModelData>
{
    private static final String SEPARATOR = ", ";

    @SuppressWarnings("unchecked")
    public Object render(Hd3dModelData model, String property, ColumnData config, int rowIndex, int colIndex,
            ListStore<Hd3dModelData> store, Grid<Hd3dModelData> grid)
    {
        StringBuffer html = new StringBuffer();
        Object value = model.get(property);
        if (value instanceof List<?>)
        {
            List<JSONObject> tags = (List<JSONObject>) value;
            for (JSONObject tag : tags)
            {
                if (html.length() == 0)
                {
                    html.append(tag.get(TagModelData.NAME_FIELD));
                }
                else
                {
                    html.append(SEPARATOR + tag.get(TagModelData.NAME_FIELD));
                }
            }
        }
        else if (value instanceof TagModelData)
        {
            html.append(((TagModelData) value).getName());
        }
        return html.toString();
    }
}
