package fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.store.Record;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.Rating;


public class RatingCellRenderer implements IExplorerCellRenderer<Hd3dModelData>
{
    private boolean init = false;
    private final List<Rating> widgets = new ArrayList<Rating>();
    private boolean editable = true;

    public boolean isEditable()
    {
        return this.editable;
    }

    public void setEditable(boolean editable)
    {
        this.editable = editable;
    }

    public Object render(final Hd3dModelData model, final String property, ColumnData config, int rowIndex,
            final int colIndex, ListStore<Hd3dModelData> store, final Grid<Hd3dModelData> grid)
    {
        if (!init)
        {
            init = true;
        }

        if (widgets.size() > rowIndex && widgets.get(rowIndex) != null)
        {
            Rating wdg = widgets.get(rowIndex);
            wdg.removeAllListeners();
            wdg.removeFromParent();
            widgets.remove(wdg);
        }

        final Record record = store.getRecord(model);
        final Rating ratingWidget = new Rating();
        Object value = model.get(property);

        if (value instanceof Double)
        {
            ratingWidget.setValue((Double) value);
        }
        else if (value instanceof Long)
        {
            ratingWidget.setValue(((Long) value).doubleValue());
        }
        else if (value instanceof Integer)
        {
            ratingWidget.setValue(((Integer) value).doubleValue());
        }
        else if (value instanceof String)
        {
            if (((String) value).length() == 0)
            {
                ratingWidget.setValue(0D);
            }
            else
            {
                ratingWidget.setValue(Double.parseDouble((String) value));
            }
        }

        // if (this.isEditable())
        // {
        ratingWidget.addListener(Events.Change, new Listener<ComponentEvent>() {
            public void handleEvent(ComponentEvent ce)
            {
                Double value = ratingWidget.getValue();
                record.set(property, value);

                GridEvent<Hd3dModelData> ge = new GridEvent<Hd3dModelData>(grid);
                ge.setColIndex(colIndex);
                ge.setModel(model);
                ge.setProperty(property);
                ge.setValue(ge.getModel().get(ge.getProperty()));
                grid.fireEvent(Events.AfterEdit, ge);
            }
        });
        // }
        // else
        // {
        // ratingWidget.disable();
        // }
        widgets.add(rowIndex, ratingWidget);
        return ratingWidget;

    }

}
