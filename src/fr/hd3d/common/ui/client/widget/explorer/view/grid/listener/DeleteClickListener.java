package fr.hd3d.common.ui.client.widget.explorer.view.grid.listener;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;


public class DeleteClickListener extends SelectionListener<ButtonEvent>
{
    @Override
    public void componentSelected(ButtonEvent ce)
    {
        EventDispatcher.forwardEvent(ExplorerEvents.DELETE_ROW);
    }
}
