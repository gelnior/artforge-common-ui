package fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer;

import java.util.List;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GroupColumnData;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONValue;


/**
 * Displays list of groups as a name list separated by commas. If there is only one group, its name is displayed only.
 * 
 * @author HD3D
 */
public class GroupRenderer<M extends ModelData> implements IExplorerCellRenderer<M>
{

    private String separator = ", ";

    public Object render(M model, String property, ColumnData config, int rowIndex, int colIndex, ListStore<M> store,
            Grid<M> grid)
    {
        Object obj = model.get(property);
        return this.getValueFromObject(obj);
    }

    public String render(GroupColumnData data)
    {
        return this.getValueFromObject(data.gvalue);
    }

    private String getValueFromObject(Object obj)
    {
        StringBuffer stringValue = new StringBuffer();
        if (obj instanceof JSONObject)
        {
            JSONObject json = (JSONObject) obj;
            JSONValue name = json.get("name");
            stringValue.append(name.isString().stringValue());
        }
        else if (obj instanceof List<?>)
        {
            @SuppressWarnings("unchecked")
            List<JSONValue> groups = (List<JSONValue>) obj;
            for (JSONValue group : groups)
            {
                if (stringValue.length() > 0)
                    stringValue.append(separator);
                JSONValue name = ((JSONObject) group).get("name");
                stringValue.append(name.isString().stringValue());
            }
        }
        return stringValue.toString();
    }

    public void setSeparator(String separator)
    {
        this.separator = separator;
    }
}
