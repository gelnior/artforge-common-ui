package fr.hd3d.common.ui.client.widget.explorer.view.grid.listener;

import com.extjs.gxt.ui.client.event.ColumnModelEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;


/**
 * When column width changes it fires the event COLUMN_WIDTH_CHANGED containing the column configuration of the changed
 * column.
 * 
 * @author HD3D
 */
public class ColumnHiddenChangeListener implements Listener<ColumnModelEvent>
{

    public void handleEvent(ColumnModelEvent be)
    {
        AppEvent event = new AppEvent(ExplorerEvents.COLUMN_HIDDEN_CHANGED);
        event.setData(be.getColumnModel().getColumn(be.getColIndex()));
        EventDispatcher.forwardEvent(event);
    }

}
