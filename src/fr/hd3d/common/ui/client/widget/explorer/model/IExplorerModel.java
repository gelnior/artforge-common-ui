// package fr.hd3d.common.ui.client.widget.explorer.model;
//
// import com.extjs.gxt.ui.client.event.Observable;
//
// import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
// import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
// import fr.hd3d.common.ui.client.service.parameter.Constraints;
// import fr.hd3d.common.ui.client.service.parameter.IUrlParameter;
// import fr.hd3d.common.ui.client.service.parameter.ItemConstraints;
// import fr.hd3d.common.ui.client.service.parameter.LogicConstraint;
// import fr.hd3d.common.ui.client.widget.explorer.model.store.ExplorerStore;
//
//
// public interface IExplorerModel extends Observable
// {
// /**
// * set the active view to display
// *
// * @param view
// */
// public void setActiveView(SheetModelData view);
//
// /**
// * Get the current view
// */
// public SheetModelData getActiveView();
//
// /**
// * Get the Data store
// */
// public ExplorerStore<Hd3dModelData> getDataStore();
//
// /**
// * Save the data
// */
// public boolean save(Boolean silent);
//
// public boolean loadData();
//
// /**
// * Load the data
// */
// public boolean loadData(String sortColumn);
//
// public LogicConstraint getFilters();
//
// public void setUserFilter(Constraints constraints);
//
// public void setUserItemFilter(ItemConstraints constraints);
//
// public void setApplicationFilter(LogicConstraint filter);
//
// public void refreshFilter();
//
// public void setLimit(int limit);
//
// public void addParameter(IUrlParameter parameter);
//
// public void removeParameter(IUrlParameter parameter);
//
// }
