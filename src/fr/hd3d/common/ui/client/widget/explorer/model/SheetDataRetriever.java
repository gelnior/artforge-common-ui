package fr.hd3d.common.ui.client.widget.explorer.model;

import java.util.List;

import com.extjs.gxt.ui.client.data.BaseModelData;
import com.extjs.gxt.ui.client.data.BasePagingLoadConfig;
import com.extjs.gxt.ui.client.data.PagingLoadConfig;
import com.extjs.gxt.ui.client.util.Util;

import fr.hd3d.common.client.enums.EConstraintLogicalOperator;
import fr.hd3d.common.ui.client.config.CommonConfig;
import fr.hd3d.common.ui.client.listener.EventLoadListener;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.IUrlParameter;
import fr.hd3d.common.ui.client.service.parameter.ItemConstraints;
import fr.hd3d.common.ui.client.service.parameter.LogicConstraint;
import fr.hd3d.common.ui.client.setting.UserSettings;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;


public class SheetDataRetriever
{
    private final ExplorerStore dataStore = new ExplorerStore();

    private final PagingLoadConfig config = new BasePagingLoadConfig();
    private final LogicConstraint filters = new LogicConstraint();
    private LogicConstraint applicationFilter;
    private LogicConstraint userFilter;
    private final ItemConstraints itemConstraints = new ItemConstraints();

    public SheetDataRetriever()
    {
        this.filters.setOperator(EConstraintLogicalOperator.AND);
        this.dataStore.addParameter(filters);
        this.dataStore.setStoreSorter(null);
        this.dataStore.setGroupOnSort(false);

        this.setPaginationQuantity();
        this.setListeners();
    }

    public ExplorerStore getDataStore()
    {
        return this.dataStore;
    }

    public void load(final SheetModelData sheet, String orderColumn)
    {
        String dataUrl = ServicesPath.SHEETS + sheet.getId().toString() + ServicesPath.DATA;
        if (MainModel.getCurrentProject() != null)
        {
            dataUrl = MainModel.getCurrentProject().getDefaultPath() + "/" + dataUrl;
        }

        this.dataStore.getReader().setSheet(sheet);
        this.dataStore.setPath(dataUrl);

        if (!Util.isEmptyString(orderColumn))
        {
            this.config.setSortDir(this.dataStore.getSortDir());
            this.config.setSortField(orderColumn);
        }
        else
        {
            this.config.setSortField(null);
        }

        Integer limit = UserSettings.getSettingAsInteger(CommonConfig.SETTING_PAGINATION_QUANTITY);
        if (limit != null)
        {
            this.dataStore.setLimit(limit);
        }
        this.config.setLimit(dataStore.getLimit());

        this.dataStore.reload(config);
    }

    public void setApplicationFilter(LogicConstraint filter)
    {
        this.applicationFilter = filter;

        this.updateFilters();
    }

    public void setUserFilter(LogicConstraint filter)
    {
        this.userFilter = filter;

        this.updateFilters();
    }

    public void updateFilters()
    {
        if (!dataStore.getParameters().contains(filters))
        {
            dataStore.addParameter(filters);
        }

        if (applicationFilter != null && applicationFilter.getLeftMember() != null)
        {
            this.filters.setLeftMember(applicationFilter);
            if (userFilter != null)
            {
                this.filters.setRightMember(userFilter);
            }
            else
            {
                this.filters.setRightMember(null);
            }
        }
        else if (userFilter != null)
        {
            this.filters.setLeftMember(userFilter);
            this.filters.setRightMember(null);
        }
        else
        {
            this.dataStore.removeParameter(filters);
            this.filters.setLeftMember(null);
            this.filters.setRightMember(null);
        }
    }

    public LogicConstraint getFilters()
    {
        return this.filters;
    }

    public void setLimit(int limit)
    {
        this.dataStore.setLimit(limit);
    }

    public void setUserItemFilter(ItemConstraints itemConstraints)
    {
        if (itemConstraints == null)
            return;

        this.itemConstraints.clear();
        this.itemConstraints.addAll(itemConstraints);
        if (CollectionUtils.isNotEmpty(this.itemConstraints))
        {
            if (!this.dataStore.getParameters().contains(this.itemConstraints))
            {
                this.dataStore.addParameter(this.itemConstraints);
            }
        }
        else
        {
            this.dataStore.removeParameter(this.itemConstraints);
        }
    }

    public void addParameter(IUrlParameter categoryConstraint)
    {
        this.dataStore.addParameter(categoryConstraint);
    }

    public void removeParameter(IUrlParameter parameter)
    {
        this.dataStore.removeParameter(parameter);
    }

    public void setPaginationQuantity()
    {
        Integer limit = UserSettings.getSettingAsInteger(CommonConfig.SETTING_PAGINATION_QUANTITY);
        if (limit != null)
        {
            this.config.setLimit(limit);
            this.dataStore.setLimit(limit);
        }
        else
        {
            this.config.setLimit(50);
            this.dataStore.setLimit(50);
        }
    }

    private void setListeners()
    {
        this.dataStore.addLoadListener(new EventLoadListener(ExplorerEvents.DATA_LOADED));
    }

    public BaseModelData getAggregationModel(SheetModelData sheet)
    {
        BaseModelData aggregation = new BaseModelData();
        List<Double> values = this.dataStore.getAggregationValues();

        if (sheet != null && CollectionUtils.isNotEmpty(sheet.getColumns()) && CollectionUtils.isNotEmpty(values))
        {
            for (int i = 0; i < sheet.getColumns().size(); i++)
            {
                if (i < values.size())
                    aggregation.set(sheet.getColumns().get(i).getDataIndex(), values.get(i));
            }
        }
        return aggregation;
    }

}
