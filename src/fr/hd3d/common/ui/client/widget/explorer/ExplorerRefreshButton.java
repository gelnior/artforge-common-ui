package fr.hd3d.common.ui.client.widget.explorer;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;

import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.widget.ToolBarButton;


public class ExplorerRefreshButton extends ToolBarButton
{

    /**
     * Set icon and tool tip. Link the button with given store.
     * 
     * @param explorer
     *            The refreshed explorer.
     */
    public ExplorerRefreshButton(final ExplorerPanel explorer)
    {
        super(Hd3dImages.getRefreshIcon(), "Refresh", null);

        this.addSelectionListener(new SelectionListener<ButtonEvent>() {
            @Override
            public void componentSelected(ButtonEvent ce)
            {
                explorer.showLoading();
                explorer.refreshData();
            }
        });
    }

}
