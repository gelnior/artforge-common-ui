package fr.hd3d.common.ui.client.widget.explorer.view.grid;

import com.extjs.gxt.ui.client.GXT;
import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.core.DomHelper;
import com.extjs.gxt.ui.client.core.El;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.data.SortInfo;
import com.extjs.gxt.ui.client.event.MenuEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.GroupingStore;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.Component;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.extjs.gxt.ui.client.widget.grid.GridGroupRenderer;
import com.extjs.gxt.ui.client.widget.grid.GridSelectionModel;
import com.extjs.gxt.ui.client.widget.grid.GroupingView;
import com.extjs.gxt.ui.client.widget.menu.CheckMenuItem;
import com.extjs.gxt.ui.client.widget.menu.Item;
import com.extjs.gxt.ui.client.widget.menu.Menu;
import com.extjs.gxt.ui.client.widget.menu.MenuItem;
import com.extjs.gxt.ui.client.widget.menu.SeparatorMenuItem;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.i18n.client.DateTimeFormat;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.listener.AlignmentMenuListener;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.listener.DateMenuListener;


public class ExplorerGridView extends GroupingView
{
    /** Constant strings to display : dialog messages, button label... */
    public static final CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    public static final String ALIGNMENT_DATA_KEY = "alignment";
    public static final String DATE_FORMAT_DATA_KEY = "dateFormat";

    static MenuItem dateFormatMenuItem;
    static DateMenuListener dateFormatMenuListener;
    static MenuItem alignmentMenuItem;
    static AlignmentMenuListener alignmentMenuListener;

    public ExplorerGridView()
    {
        super();
    }

    private MenuItem findGroupByMenuItem(Menu menu)
    {
        for (Component item : menu.getItems())
        {
            if (item instanceof MenuItem)
            {
                if (((MenuItem) item).getText().equals(GXT.MESSAGES.groupingView_groupByText()))
                {
                    return (MenuItem) item;
                }
            }
        }
        return null;
    }

    private MenuItem findUnGroupMenuItem(Menu menu)
    {
        for (Component item : menu.getItems())
        {
            if (item instanceof MenuItem)
            {
                if (((MenuItem) item).getText().equals(GXT.MESSAGES.groupingView_showGroupsText()))
                {
                    return (MenuItem) item;
                }
            }
        }
        return null;
    }

    @Override
    protected Menu createContextMenu(final int colIndex)
    {
        Menu menu = super.createContextMenu(colIndex);

        Item item = findGroupByMenuItem(menu);
        if (item != null)
        {
            item.removeAllListeners();
            item.addSelectionListener(new SelectionListener<MenuEvent>() {
                @Override
                public void componentSelected(MenuEvent ce)
                {
                    GridCellRenderer<?> renderer = cm.getRenderer(colIndex);
                    if (renderer == null || renderer instanceof GridGroupRenderer)
                    {
                        onHeaderClick(grid, colIndex);
                        setGroupRenderer((GridGroupRenderer) renderer);

                        String id = ((ExplorerColumnConfig) cm.getColumn(colIndex)).getItemId().toString();
                        EventDispatcher.forwardEvent(ExplorerEvents.COLUMN_GROUPED, id);
                        String dataIndex = cm.getDataIndex(colIndex);
                        ((GroupingStore<?>) ds).groupBy(dataIndex);
                    }
                    else
                    {
                        MessageBox.prompt("Info", "This column is not groupable");
                    }
                }
            });
        }

        item = findUnGroupMenuItem(menu);
        if (item != null)
        {
            item.removeAllListeners();
            item.addSelectionListener(new SelectionListener<MenuEvent>() {
                @Override
                public void componentSelected(MenuEvent ce)
                {
                    ((GroupingStore<?>) ds).clearGrouping();
                    EventDispatcher.forwardEvent(ExplorerEvents.COLUMN_UNGROUPED);
                }
            });
        }

        ColumnConfig cc = cm.getColumn(colIndex);
        if (cc != null && cc instanceof ExplorerColumnConfig)
        {
            if (((ExplorerColumnConfig) cc).getType() != null && ((ExplorerColumnConfig) cc).getType().endsWith("Date")
                    && cc.getRenderer().equals(""))
            {
                menu.add(new SeparatorMenuItem());
                menu.add(this.createDateFormatMenu(colIndex));
            }
        }

        menu.add(new SeparatorMenuItem());
        menu.add(this.createAlignmentMenu(colIndex));

        MenuItem hide = new MenuItem(CONSTANTS.Hide() + " " + cc.getHeader());
        hide.addSelectionListener(new SelectionListener<MenuEvent>() {

            @Override
            public void componentSelected(MenuEvent ce)
            {
                cm.setHidden(colIndex, true);
            }

        });
        menu.insert(hide, 3);
        return menu;
    }

    private MenuItem createDateFormatMenu(int colIndex)
    {
        ColumnConfig column = cm.getColumn(colIndex);

        if (dateFormatMenuItem == null)
        {
            dateFormatMenuItem = new MenuItem(CONSTANTS.DateFormat());
            Menu menu = new Menu();

            dateFormatMenuListener = new DateMenuListener(column);
            CheckMenuItem longDate = new CheckMenuItem("Long");
            longDate.setGroup(DATE_FORMAT_DATA_KEY);
            longDate.setData(DATE_FORMAT_DATA_KEY, DateTimeFormat.getMediumDateTimeFormat());
            longDate.addSelectionListener(dateFormatMenuListener);
            CheckMenuItem mediumDate = new CheckMenuItem("Medium");
            mediumDate.setGroup(DATE_FORMAT_DATA_KEY);
            mediumDate.setData(DATE_FORMAT_DATA_KEY, DateTimeFormat.getShortDateTimeFormat());
            mediumDate.addSelectionListener(dateFormatMenuListener);
            CheckMenuItem shortDate = new CheckMenuItem("Short");
            shortDate.setGroup(DATE_FORMAT_DATA_KEY);
            shortDate.setData(DATE_FORMAT_DATA_KEY, DateTimeFormat.getShortDateFormat());
            shortDate.addSelectionListener(dateFormatMenuListener);

            menu.add(shortDate);
            menu.add(mediumDate);
            menu.add(longDate);

            dateFormatMenuItem.setSubMenu(menu);
        }

        dateFormatMenuListener.setColumn(column);

        // uncheck all
        ((CheckMenuItem) dateFormatMenuItem.getSubMenu().getItem(0)).setChecked(false);
        ((CheckMenuItem) dateFormatMenuItem.getSubMenu().getItem(1)).setChecked(false);
        ((CheckMenuItem) dateFormatMenuItem.getSubMenu().getItem(2)).setChecked(false);

        String format = column.getDateTimeFormat().getPattern();
        if (format.equals(DateTimeFormat.getMediumDateTimeFormat().getPattern()))
        {
            ((CheckMenuItem) dateFormatMenuItem.getSubMenu().getItem(2)).setChecked(true);
        }
        else if (format.equals(DateTimeFormat.getShortDateTimeFormat().getPattern()))
        {
            ((CheckMenuItem) dateFormatMenuItem.getSubMenu().getItem(1)).setChecked(true);
        }
        else if (format.equals(DateTimeFormat.getShortDateFormat().getPattern()))
        {
            ((CheckMenuItem) dateFormatMenuItem.getSubMenu().getItem(0)).setChecked(true);
        }
        return dateFormatMenuItem;
    }

    private MenuItem createAlignmentMenu(int colIndex)
    {
        ColumnConfig column = cm.getColumn(colIndex);

        if (alignmentMenuItem == null)
        {
            alignmentMenuItem = new MenuItem(CONSTANTS.Alignment());
            Menu menu = new Menu();

            alignmentMenuListener = new AlignmentMenuListener(column);
            CheckMenuItem left = new CheckMenuItem(CONSTANTS.Left());
            left.setGroup(ALIGNMENT_DATA_KEY);
            left.setData(ALIGNMENT_DATA_KEY, HorizontalAlignment.LEFT);
            left.addSelectionListener(alignmentMenuListener);
            CheckMenuItem center = new CheckMenuItem(CONSTANTS.Center());
            center.setGroup(ALIGNMENT_DATA_KEY);
            center.setData(ALIGNMENT_DATA_KEY, HorizontalAlignment.CENTER);
            center.addSelectionListener(alignmentMenuListener);
            CheckMenuItem right = new CheckMenuItem(CONSTANTS.Right());
            right.setGroup(ALIGNMENT_DATA_KEY);
            right.setData(ALIGNMENT_DATA_KEY, HorizontalAlignment.RIGHT);
            right.addSelectionListener(alignmentMenuListener);

            menu.add(left);
            menu.add(center);
            menu.add(right);

            alignmentMenuItem.setSubMenu(menu);
        }

        alignmentMenuListener.setColumn(column);

        // uncheck all
        ((CheckMenuItem) alignmentMenuItem.getSubMenu().getItem(0)).setChecked(false);
        ((CheckMenuItem) alignmentMenuItem.getSubMenu().getItem(1)).setChecked(false);
        ((CheckMenuItem) alignmentMenuItem.getSubMenu().getItem(2)).setChecked(false);

        HorizontalAlignment alignment = column.getAlignment();
        if (alignment == null || alignment.equals(HorizontalAlignment.LEFT))
        {
            ((CheckMenuItem) alignmentMenuItem.getSubMenu().getItem(0)).setChecked(true);
        }
        else if (alignment.equals(HorizontalAlignment.CENTER))
        {
            ((CheckMenuItem) alignmentMenuItem.getSubMenu().getItem(1)).setChecked(true);
        }
        else if (alignment.equals(HorizontalAlignment.RIGHT))
        {
            ((CheckMenuItem) alignmentMenuItem.getSubMenu().getItem(2)).setChecked(true);
        }

        return alignmentMenuItem;
    }

    @Override
    protected void insertRows(ListStore<ModelData> store, int firstRow, int lastRow, boolean isUpdate)
    {
        Element e = mainBody.dom.getFirstChildElement();
        if (e != null && !hasRows())
        {
            mainBody.dom.setInnerHTML("");
        }

        String html = renderRows(firstRow, lastRow);
        Element before = getRow(firstRow);

        if (before != null)
        {
            DomHelper.insertBefore((com.google.gwt.user.client.Element) before, html);
        }
        else
        {
            DomHelper.insertHtml("beforeEnd", mainBody.dom, html);
        }

        if (!isUpdate)
        {
            processRows(firstRow, false);
            focusRow(firstRow);
        }
        else
        {
            GridSelectionModel<?> sm = this.grid.getSelectionModel();
            ExplorerSelectionModel esm = (ExplorerSelectionModel) sm;
            for (int i = firstRow; i <= lastRow; i++)
            {
                ModelData m = store.getAt(i);
                if (esm.isSelected((Hd3dModelData) m))
                {
                    super.onRowSelect(i);
                    if (esm.getSelectionColumnIndex() != -1)
                    {
                        super.onCellSelect(i, esm.getSelectionColumnIndex());
                    }
                }
            }
        }
    }

    public El getCheckBoxHeader()
    {
        return this.innerHd.child("div.x-grid3-hd-checker");
    }

    @Override
    public void onCellSelect(int row, int col)
    {
        super.onCellSelect(row, col);
    }

    @Override
    public void onCellDeselect(int row, int col)
    {
        super.onCellDeselect(row, col);
    }

    @Override
    protected void onHeaderClick(Grid<ModelData> grid, int column)
    {
        if (!headerDisabled && cm.isSortable(column))
        {
            SortDir sortDir = ds.getSortDir();
            if (sortDir == null)
            {
                sortDir = SortDir.NONE;
            }
            switch (sortDir)
            {
                case ASC:
                    sortDir = SortDir.DESC;
                    break;
                case NONE:
                case DESC:
                    sortDir = SortDir.ASC;
                    break;
                default:
                    sortDir = SortDir.ASC;
            }
            this.doSort(column, sortDir);
        }
        else
        {
            super.onHeaderClick(grid, column);
        }
    }

    public void localSort(int column, SortDir sortDir)
    {
        super.doSort(column, sortDir);
    }

    @Override
    protected void doSort(int colIndex, SortDir sortDir)
    {
        ExplorerColumnConfig ecc = (ExplorerColumnConfig) cm.getColumn(colIndex);
        SortInfo sortInfo = new SortInfo(ecc.getItemId().toString(), sortDir);

        updateSortIcon(colIndex, sortDir);
        AppEvent event = new AppEvent(ExplorerEvents.HEADER_CLICK);
        event.setData("column-index", Integer.valueOf(colIndex));
        event.setData("sort-info", sortInfo);

        EventDispatcher.forwardEvent(event);
    }

    public void resetSortIcon()
    {
        updateSortIcon(0, SortDir.DESC);
    }

    public void setSortIcon(int colIndex, SortDir sortDir)
    {
        this.updateSortIcon(colIndex, sortDir);
    }
}
