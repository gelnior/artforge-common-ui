package fr.hd3d.common.ui.client.widget.explorer.model.modeldata;

import java.util.List;

public interface IColumn extends IHd3dBaseModel
{
    final String NAME = "name";

    public String getRenderer();

    public String getEditor();

    public String getName();

    public String getType();

    public String getQuery();

    public String getTransformer();

    public String getDataIndex();

    public String getParameter();
    
    public List<String> getListValues();
}
