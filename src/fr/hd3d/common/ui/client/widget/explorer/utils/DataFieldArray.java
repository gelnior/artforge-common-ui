package fr.hd3d.common.ui.client.widget.explorer.utils;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;


/**
 * a special DataField to allow nested modelType
 * 
 */
public class DataFieldArray extends DataField
{
    private ModelType modelType;

    public DataFieldArray(String name)
    {
        super(name);
    }

    public DataFieldArray(String name, String map)
    {
        super(name, map);
    }

    public void setModelType(ModelType modelType)
    {
        this.modelType = modelType;
    }

    public ModelType getModelType()
    {
        return this.modelType;
    }
}
