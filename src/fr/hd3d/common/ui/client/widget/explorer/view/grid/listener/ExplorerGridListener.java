package fr.hd3d.common.ui.client.widget.explorer.view.grid.listener;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.widget.explorer.controller.ExplorerController;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.ExplorerGrid;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.IExplorateurColumnContextMenu;


/**
 * Grid listener to handle context menu on explorer grid and multiple edit (after edit event).
 * 
 * @author HD3D
 */
public class ExplorerGridListener implements Listener<GridEvent<Hd3dModelData>>
{
    /** Grid from where event comes. */
    private final ExplorerGrid grid;

    /**
     * Default constructor.
     * 
     * @param grid
     *            Grid from where event comes.
     */
    public ExplorerGridListener(ExplorerGrid grid)
    {
        this.grid = grid;
    }

    /**
     * Check if it is a context menu or an after edit event. If context menu event is raised, it checks if the renderer
     * needs a special context menu. Else default menu is used. If after edit event is raised, it checks if it is
     * multiple edition. If it is all selected cells are updated with new value.
     * 
     * @param ge
     *            Grid event.
     */
    public void handleEvent(GridEvent<Hd3dModelData> ge)
    {
        if (ge.getType() == Events.ContextMenu && ge.getColIndex() >= 0)
        {
            this.onContextMenu(ge);
        }
        else if (ge.getType() == Events.AfterEdit)
        {
            this.onAfterEdit(ge);
        }
    }

    /**
     * When context menu event is raised, it checks if the renderer needs a special context menu. Else default menu is
     * used.
     * 
     * @param ge
     *            Context menu grid event.
     */
    @SuppressWarnings("unchecked")
    protected void onContextMenu(GridEvent<Hd3dModelData> ge)
    {
        ColumnModel cm = ge.getGrid().getColumnModel();
        ColumnConfig cc = cm.getColumn(ge.getColIndex());
        GridCellRenderer<ModelData> renderer = cc.getRenderer();

        if (renderer instanceof IExplorateurColumnContextMenu<?>)
        {
            ((IExplorateurColumnContextMenu<Hd3dModelData>) renderer).handleGridEvent(ge);
        }
        else
        {
            this.grid.setContextMenu(this.grid.getDefaultContextMenu());
        }
    }

    /**
     * After edition it checks if it is multiple edition. If it is, all selected cells are updated with new value.
     * 
     * @param ge
     *            After edit grid event.
     */
    protected void onAfterEdit(GridEvent<Hd3dModelData> ge)
    {
        grid.autoEditMultiple(ge);
        if (ExplorerController.autoSave)
        {
            EventDispatcher.forwardEvent(ExplorerEvents.SAVE_DATA, Boolean.TRUE);
        }
    }

}
