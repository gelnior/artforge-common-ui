package fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar;

import com.extjs.gxt.ui.client.widget.button.Button;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.ExplorateurCSS;
import fr.hd3d.common.ui.client.widget.sheeteditor.listener.EditSheetClickListener;


/**
 * 
 * an edit button to edit the current view with the sheet editor
 */
public class EditSheetToolItem extends Button
{
    /** Constant strings to display : dialog messages, button label... */
    public static final CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    /**
     * Default constructor
     */
    public EditSheetToolItem()
    {
        super();
        initButton();
    }

    private void initButton()
    {
        this.addSelectionListener(new EditSheetClickListener());

        this.setIconStyle(ExplorateurCSS.EDIT_VIEW_ICON_CLASS);
        this.getElement().setNodeValue(ExplorateurCSS.EDIT_VIEW_ICON_CLASS);

        this.setToolTip(CONSTANTS.EditCurrentSheet());
    }
}
