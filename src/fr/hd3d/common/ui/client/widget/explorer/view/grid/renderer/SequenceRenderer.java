package fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GroupColumnData;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONValue;

import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;


public class SequenceRenderer<M extends ModelData> implements IExplorerCellRenderer<M>
{

    public Object render(M model, String property, ColumnData config, int rowIndex, int colIndex, ListStore<M> store,
            Grid<M> grid)
    {
        Object obj = model.get(property);
        return this.getValueFromObject(obj);
    }

    private String getValueFromObject(Object obj)
    {
        String stringValue = "";
        if (obj instanceof JSONObject)
        {
            JSONValue value = ((JSONObject) obj).get("name");
            stringValue = value.isString().stringValue();
        }
        else if (obj instanceof CategoryModelData)
        {
            stringValue = ((CategoryModelData) obj).getName();
        }
        return stringValue;
    }

    public String render(GroupColumnData data)
    {
        return this.getValueFromObject(data.gvalue);
    }

}
