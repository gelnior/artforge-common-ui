package fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.widget.Dialog;

import fr.hd3d.common.ui.client.event.EventDispatcher;


public class ConfirmMessageBoxCallback implements Listener<MessageBoxEvent>
{
    private final EventType okEvent;
    private final EventType nokEvent;
    private final Object data;

    public ConfirmMessageBoxCallback(EventType okEvent, EventType nokEvent, Object data)
    {
        this.okEvent = okEvent;
        this.nokEvent = nokEvent;
        this.data = data;
    }

    public void handleEvent(MessageBoxEvent be)
    {
        if (be.getButtonClicked().getItemId() == Dialog.YES)
        {
            if (okEvent != null)
                EventDispatcher.forwardEvent(okEvent, data);
        }
        else
        {
            if (nokEvent != null)
                EventDispatcher.forwardEvent(nokEvent, data);
        }
    }
}
