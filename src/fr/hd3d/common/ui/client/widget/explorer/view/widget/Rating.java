package fr.hd3d.common.ui.client.widget.explorer.view.widget;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.core.XDOM;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.util.Util;
import com.extjs.gxt.ui.client.widget.BoxComponent;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Event;


public class Rating extends BoxComponent
{
    private int maxValue;
    private Integer value;
    private List<Element> stars;

    public Rating()
    {
        super();
        init();
    }

    public Rating(Element element, boolean attach)
    {
        super();
        init();
    }

    private void init()
    {
        maxValue = 5;
        value = 1;
        stars = new ArrayList<Element>(maxValue);
    }

    public Double getValue()
    {
        return value.doubleValue();
    }

    public void setValue(Double value)
    {
        this.value = value.intValue();
        if (this.rendered)
        {
            this.refresh();
        }
    }

    public void setMaxValue(int maxValue)
    {
        this.maxValue = maxValue;
        if (this.rendered)
        {
            this.clear();
            this.refresh();
        }
    }

    public int getMaxValue()
    {
        return this.maxValue;
    }

    protected void renderRatingButton()
    {
        stars.clear();
        Element ul = DOM.createElement("ul");
        getElement().setClassName("radioGroupContainer");

        for (int i = 0; i < maxValue; i++)
        {
            Element li = DOM.createElement("li");
            Element img = DOM.createElement("div");

            img.setAttribute("radioValue", String.valueOf(i + 1));
            stars.add(img);
            if (i < value)
            {
                img.setClassName("cercle-check");
            }
            else
            {
                img.setClassName("cercle-uncheck");
            }
            li.setClassName("radioGroupList");
            li.appendChild(img);
            ul.appendChild(li);
        }
        getElement().appendChild(ul);
    }

    protected void refresh()
    {
        if (maxValue != 0 && stars.size() == 0)
        {
            this.renderRatingButton();
        }
        else
        {
            int nbButton = stars.size();
            for (int i = 0; i < nbButton; i++)
            {
                Element img = stars.get(i);
                if (i <= value)
                {
                    img.setClassName("cercle-check");
                }
                else
                {
                    img.setClassName("cercle-uncheck");
                }
            }
        }
    }

    private void clear()
    {
        this.getElement().setInnerHTML("");
    }

    public void onDoubleClick(ComponentEvent ce)
    {
        if (!this.disabled)
        {
            Element img = ce.getTarget();
            String newValue = img.getAttribute("radioValue");

            Double nVal = 0D;
            if (!Util.isEmptyString(newValue))
                nVal = Double.valueOf(newValue);

            if (nVal == 1.0D && value == 0.0D)
            {
                setValue(1.0D);
            }
            else if (nVal == 1.0D && value == 1.0D)
            {
                setValue(0.0D);
            }
            else
            {
                setValue(nVal);
            }
            this.fireEvent(Events.Change);
        }
        ce.stopEvent();
        ce.cancelBubble();
    }

    @Override
    public void onComponentEvent(ComponentEvent ce)
    {
        super.onComponentEvent(ce);
        switch (ce.getType().getEventCode())
        {
            case Event.ONDBLCLICK:
                onDoubleClick(ce);
                break;
        }
    }

    @Override
    public void onBrowserEvent(Event event)
    {
        super.onBrowserEvent(event);
    }

    @Override
    protected void onRender(Element target, int index)
    {
        super.onRender(target, index);
        setElement(DOM.createElement(XDOM.getUniqueId()), target, index);
        sinkEvents(Event.ONDBLCLICK);
        refresh();
    }
}
