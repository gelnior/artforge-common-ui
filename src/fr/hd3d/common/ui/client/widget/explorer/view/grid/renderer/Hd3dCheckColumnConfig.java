package fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.widget.Component;
import com.extjs.gxt.ui.client.widget.grid.CheckColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.Grid;


public class Hd3dCheckColumnConfig extends CheckColumnConfig
{
    private Boolean editable = false;
    private final Listener<GridEvent<ModelData>> listener = new Listener<GridEvent<ModelData>>() {
        public void handleEvent(GridEvent<ModelData> e)
        {
            onMouseDown(e);
        }
    };

    public Listener<?> getListener()
    {
        return this.listener;
    }

    public Boolean isEditable()
    {
        return this.editable;
    }

    public void setEditable(Boolean editable)
    {
        this.editable = editable;
    }

    @Override
    protected void onMouseDown(GridEvent<ModelData> ge)
    {
        if (this.isEditable() && this.grid.getColumnModel().getColumn(ge.getColIndex()) == this)
        {
            super.onMouseDown(ge);

            ge.setModel(this.grid.getStore().getAt(ge.getRowIndex()));
            ge.setProperty(this.getDataIndex());
            ge.setValue(ge.getModel().get(ge.getProperty()));
            this.grid.fireEvent(Events.AfterEdit, ge);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void init(Component component)
    {
        this.grid = (Grid<ModelData>) component;

        if (!grid.getListeners(Events.CellDoubleClick).contains(this.listener))
            grid.addListener(Events.CellDoubleClick, this.listener);
    }

    @Override
    protected String getCheckState(ModelData model, String property, int rowIndex, int colIndex)
    {
        Boolean v = model.get(property);
        String on = (v != null && v) ? "-on" : "";
        return on;
    }
}
