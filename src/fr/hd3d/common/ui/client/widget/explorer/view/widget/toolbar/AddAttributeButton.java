package fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.widget.toolbar.LabelToolItem;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.widget.sheeteditor.event.SheetEditorEvents;


/**
 * 
 * an edit button to edit the current view with the sheet editor
 */
public class AddAttributeButton extends LabelToolItem
{
    /** Constant strings to display : dialog messages, button label... */
    public static final CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    /**
     * Default constructor
     */
    public AddAttributeButton()
    {
        super();
        initButton();
    }

    private void initButton()
    {
        this.addListener(Events.Select, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                EventDispatcher.forwardEvent(SheetEditorEvents.SHOW_ADD_ATTRIBUTE_WINDOW);
            }
        });

        this.setToolTip(CONSTANTS.CreateNewAttribute());
    }

}
