package fr.hd3d.common.ui.client.widget.explorer.view.grid.editor;

import com.google.gwt.json.client.JSONObject;

public interface IComplexModelEditor<M>
{

    M getValueFromJSON(JSONObject value);

}
