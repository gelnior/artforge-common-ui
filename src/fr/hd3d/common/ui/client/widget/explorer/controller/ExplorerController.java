package fr.hd3d.common.ui.client.widget.explorer.controller;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.data.SortInfo;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.util.Util;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.HeaderGroupConfig;

import fr.hd3d.common.ui.client.service.parameter.LuceneConstraint;
import fr.hd3d.common.ui.client.config.CommonConfig;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ItemGroupModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.Constraints;
import fr.hd3d.common.ui.client.service.parameter.IUrlParameter;
import fr.hd3d.common.ui.client.service.parameter.ItemConstraint;
import fr.hd3d.common.ui.client.service.parameter.ItemConstraints;
import fr.hd3d.common.ui.client.service.parameter.LogicConstraint;
import fr.hd3d.common.ui.client.service.parameter.ParameterBuilder;
import fr.hd3d.common.ui.client.service.store.ServicesPagingStore;
import fr.hd3d.common.ui.client.setting.UserSettings;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.widget.constraint.model.ConstraintPanelModel;
import fr.hd3d.common.ui.client.widget.explorer.config.ExplorerConfig;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;
import fr.hd3d.common.ui.client.widget.explorer.model.BaseColumnInfoProvider;
import fr.hd3d.common.ui.client.widget.explorer.model.ExplorerModel;
import fr.hd3d.common.ui.client.widget.explorer.model.IColumnInfoProvider;
import fr.hd3d.common.ui.client.widget.explorer.view.IExplorateurView;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.common.ui.client.widget.thumbnaileditor.ThumbnailPath;


/**
 * Controller that handles explorer events
 */
public class ExplorerController extends MaskableController
{
    /** True when auto save mode is on (save action after each changes). */
    public static Boolean autoSave = Boolean.FALSE;

    /**
     * @return True when auto save mode is on (save action after each changes).
     */
    public static Boolean getAutoSave()
    {
        return autoSave;
    }

    /**
     * Set auto save mode state (if true auto save mode is on (save action after each changes)).
     * 
     * @param isAutoSave
     *            The boolean to set.
     */
    public static void setAutoSave(Boolean isAutoSave)
    {
        if (isAutoSave != null)
            autoSave = isAutoSave;
    }

    /** Model handles explorer data. */
    private final ExplorerModel model;
    /** View contains explorer widget. */
    private final IExplorateurView view;

    /** Column info provider handles column behavior : size, renderer, editor... */
    private IColumnInfoProvider columnInfoProvider;

    /**
     * Default constructor.
     * 
     * @param view
     *            View contains explorer widget.
     */
    public ExplorerController(IExplorateurView view, ExplorerModel model)
    {
        this.view = view;
        this.model = model;

        this.columnInfoProvider = new BaseColumnInfoProvider();

        this.registerEvents();
    }

    private ColumnModel getGridColumnModel()
    {
        final SheetModelData view = this.model.getCurrentSheet();

        if (view == null)
            return null;

        // create default column model
        List<ColumnConfig> ccs = columnInfoProvider.getColumnConfig(view.getColumns(), view.getId());

        // create columnModel
        ColumnModel cm = new ColumnModel(ccs);
        if (columnInfoProvider.getAggregation() != null)
        {
            cm.addAggregationRow(columnInfoProvider.getAggregation());
        }

        // add column grouping to column model
        List<ItemGroupModelData> groups = view.getColumnGroups();
        if (groups.size() > 1)
        {
            int columnStart = 0;
            for (ItemGroupModelData columnGroup : groups)
            {
                int size = columnGroup.getColumnsCount();
                cm.addHeaderGroup(0, columnStart, new HeaderGroupConfig(columnGroup.getName(), 1, size));
                columnStart += size;
            }
        }

        return cm;
    }

    /**
     * 
     * @param sheet
     * @param isLoadData
     */
    public void setCurrentSheet(SheetModelData sheet)
    {
        this.setCurrentSheet(sheet, true);
    }

    /**
     * Function called automatically when active view changed
     * 
     * @param sheet
     *            The sheet to set as current sheet.
     * @param isLoadData
     *            True to load data after setting sheet.
     */
    public void setCurrentSheet(SheetModelData sheet, boolean isLoadData)
    {
        if (sheet != null)
        {
            this.view.showLoading();
        }
        else
        {
            this.view.hideGrid();
        }
        this.model.setActiveView(sheet, isLoadData);
    }

    public IColumnInfoProvider getColumnInfoProvider()
    {
        return this.columnInfoProvider;
    }

    public void setColumnInfoProvider(IColumnInfoProvider columnInfoProvider)
    {
        this.columnInfoProvider = columnInfoProvider;
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();

        if (type == ExplorerEvents.CHANGE_VIEW)
        {
            this.onChangeView(event);
        }
        else if (type == ExplorerEvents.SHEET_INFORMATIONS_LOADED)
        {
            this.onSheetInformationsLoaded(event);
        }
        else if (type == ExplorerEvents.RELOAD_DATA)
        {
            this.onReloadData();
        }
        else if (type == ExplorerEvents.SAVE_DATA)
        {
            this.onSaveData(event);
        }
        else if (type == ExplorerEvents.DATA_LOADED)
        {
            this.onDataLoaded();
        }
        else if (type == ExplorerEvents.REFRESH_VIEW)
        {
            this.onRefreshView();
        }
        else if (type == ExplorerEvents.PRINT_CLICKED)
        {
            this.onPrintClicked();
        }
        else if (type == ExplorerEvents.AUTO_SAVE_TOGGLE)
        {
            this.onAutoSaveToggle(event);
        }
        else if (type == ExplorerEvents.COLUMN_HIDDEN_CHANGED)
        {
            this.onColumnHiddenChanged((ColumnConfig) event.getData());
        }
        else if (type == ExplorerEvents.COLUMN_WIDTH_CHANGED)
        {
            this.onColumnWidthChanged(event);
        }
        else if (type == ExplorerEvents.COLUMN_GROUPED)
        {
            this.onColumnGrouped(event);
        }
        else if (type == ExplorerEvents.COLUMN_UNGROUPED)
        {
            this.onColumnUngrouped(event);
        }
        else if (type == ExplorerEvents.COLUMN_ALIGNMENT_CHANGED)
        {
            this.onColumnAlignmentChanged((ColumnConfig) event.getData());
        }
        else if (type == ExplorerEvents.COLUMN_DATE_FORMAT_CHANGED)
        {
            this.onColumnDateFormatChanged((ColumnConfig) event.getData());
        }
        else if (type == ExplorerEvents.HEADER_CLICK)
        {
            this.onHeaderClicked(event);
        }
        else if (type == ExplorerEvents.NEW_APPLICATION_FILTER_SET)
        {
            this.onNewApplicationFilterSet(event);
        }
        else if (type == ExplorerEvents.NEW_USER_FILTER_SET)
        {
            this.onNewUserFilterSet(event);
        }
        else if (type == ExplorerEvents.CSV_EXPORT_CLICKED)
        {
            this.onCsvExportClicked(event);
        }
        else if (type == ExplorerEvents.THUMBNAIL_EDIT_CLICKED)
        {
            this.onSetThumbnailClicked();
        }
        else if (type == ExplorerEvents.SHOW_PREVIEW_CLICKED)
        {
            this.onShowPreviewClicked();
        }
        else if (type == ExplorerEvents.THUMBNAIL_UPLOADED)
        {
            this.onThumbnailUploaded();
        }
        else if (type == ExplorerEvents.SHOW_MOSAIC_CLICKED)
        {
            this.onShowMosaicClicked();
        }
        else if (type == ExplorerEvents.HIDE_MOSAIC_CLICKED)
        {
            this.onHideMosaicClicked();
        }
        else if (type == ExplorerEvents.SHOW_IDENTITY_CLICKED)
        {
            this.onShowIdentityClicked();
        }
        else if (type == ExplorerEvents.PAGE_SIZE_CHANGED)
        {
            this.onPageSizeChanged(event);
        }
        else if (type == CommonEvents.ERROR)
        {
            // this.model.loadData();
            // Weird thing : when error occurs, the error event is dispatched a lot of time instead of one time.
            this.forwardToChild(event);
        }
        else
        {
            this.forwardToChild(event);
        }
    }

    private void onSheetInformationsLoaded(AppEvent event)
    {
        this.showGrid();
        String sortColumn = event.getData(ExplorerConfig.COLUMN_SORT_EVENT_VAR_NAME);
        if (!Util.isEmptyString(sortColumn))
        {
            this.view.setSortColumn(sortColumn);
        }
    }

    private void onColumnGrouped(AppEvent event)
    {
        String column = event.getData();

        String key = "explorer.";
        key += "sheet.";
        key += this.model.getCurrentSheet().getId();
        key += ".group";

        String value = column;

        UserSettings.setSetting(key, value);

        this.model.setGroupColumn(column);
    }

    private void onColumnUngrouped(AppEvent event)
    {
        String key = "explorer.";
        key += "sheet.";
        key += this.model.getCurrentSheet().getId();
        key += ".group";

        String value = "null";

        UserSettings.setSetting(key, value);

        this.model.setGroupColumn("");
    }

    private void onPageSizeChanged(AppEvent event)
    {
        Integer limit = event.getData();
        UserSettings.setSetting(CommonConfig.SETTING_PAGINATION_QUANTITY, limit.toString());
        this.model.setLimit(limit.intValue());

        this.loadData();
    }

    private void onChangeView(AppEvent event)
    {
        this.view.resetSelection();
        this.setCurrentSheet((SheetModelData) event.getData());
    }

    private void onReloadData()
    {
        this.view.resetSelection();
        this.loadData();
    }

    private void loadData()
    {
        final ServicesPagingStore<Hd3dModelData> store = this.model.getDataStore();

        if (store == null || store.getLoadConfig() == null)
            return;
        this.model.loadData();
    }

    private void onSaveData(AppEvent event)
    {
        Boolean silent = event.getData();
        if (silent == null)
        {
            silent = Boolean.FALSE;
        }
        saveData(silent);
    }

    private void onDataLoaded()
    {
        this.view.displaySum(this.model.getAggregationModel());
        this.view.applyGrouping(this.model.getGroupColumn());
        this.view.setSortColumn(this.model.getSortColumn());
        this.view.hideLoading();
    }

    private void onRefreshView()
    {
        this.view.resetSelection();
        this.view.refresh();
    }

    private void onPrintClicked()
    {
        this.view.print();
    }

    private void onAutoSaveToggle(AppEvent event)
    {
        autoSave = event.getData();
        if (autoSave != null)
        {
            Boolean isUserAutoSave = UserSettings.getSettingAsBoolean(ExplorerConfig.SETTING_AUTOSAVE);
            if (isUserAutoSave == null || autoSave != isUserAutoSave)
                UserSettings.setSetting(ExplorerConfig.SETTING_AUTOSAVE, autoSave.toString());

            if (autoSave)
                this.saveData(true);
        }
    }

    /** When new filter from application is set, it is extracted from event then applied to the explorer proxy. */
    private void onNewApplicationFilterSet(AppEvent event)
    {
        LogicConstraint filter = event.getData();
        this.model.setApplicationFilter(filter);
    }

    /** When new filter from user is set, it is extracted from event then applied to the explorer proxy. */
    private void onNewUserFilterSet(AppEvent event)
    {
        this.model.setUserFilter((Constraints) event.getData(ConstraintPanelModel.CONSTRAINTS));
        this.model.setUserItemFilter((ItemConstraints) event.getData(ConstraintPanelModel.ITEMCONSTRAINTS));
        this.forwardToChild(event);
    }

    /**
     * Show the explorer grid.
     */
    public void showGrid()
    {
        ColumnModel cm = this.getGridColumnModel();
        this.view.showGrid(cm, this.model.getDataStore());
    }

    /**
     * Save updated data.
     */
    public void saveData(Boolean silent)
    {
        this.model.save(silent);
    }

    protected void onColumnWidthChanged(AppEvent event)
    {
        if (this.model.getCurrentSheet() == null)
        {
            return;
        }
        ColumnConfig cc = event.getData();

        String key = "explorer.";
        key += "sheet.";
        key += this.model.getCurrentSheet().getId();
        key += ".item.";
        key += cc.getId();
        key += ":width";

        String value = (Integer.valueOf(cc.getWidth())).toString();

        UserSettings.setSetting(key, value);
    }

    protected void onColumnDateFormatChanged(ColumnConfig cc)
    {
        String key = "explorer.";
        key += "sheet.";
        key += this.model.getCurrentSheet().getId();
        key += ".item.";
        key += cc.getId();
        key += ":dateFormat";

        String value = cc.getDateTimeFormat().getPattern();
        UserSettings.setSetting(key, value);
    }

    protected void onColumnHiddenChanged(ColumnConfig cc)
    {
        if (this.model.getCurrentSheet() == null)
        {
            return;
        }
        String key = "explorer.";
        key += "sheet.";
        key += this.model.getCurrentSheet().getId();
        key += ".item.";
        key += cc.getId();
        key += ":hidden";

        Boolean value = cc.isHidden();
        UserSettings.setSetting(key, value.toString());
    }

    protected void onColumnAlignmentChanged(ColumnConfig cc)
    {
        String key = "explorer.";
        key += "sheet.";
        key += this.model.getCurrentSheet().getId();
        key += ".item.";
        key += cc.getId();
        key += ":alignment";

        String value = (cc.getAlignment()).toString();
        UserSettings.setSetting(key, value);
    }

    private void onHeaderClicked(AppEvent event)
    {
        SortInfo sortInfo = (SortInfo) event.getData(ExplorerConfig.SORT_CONFIG_EVENT_VAR_NAME);
        String columnId = sortInfo.getSortField();
        if (columnId.startsWith("id_"))
        {
            columnId = columnId.substring(3);
        }

        if (sortInfo.getSortDir() == SortDir.DESC)
        {
            columnId = "-" + columnId;
        }
        sortInfo.setSortField(columnId);

        String key = "explorer.";
        key += "sheet.";
        key += this.model.getCurrentSheet().getId();
        key += ".sort";

        String value = columnId;

        UserSettings.setSetting(key, value);

        if (this.model.getCurrentSheet() != null)
        {
            this.model.loadData(sortInfo);
        }
    }

    /**
     * When CSV export button is clicked, it calls the CSV service for the current sheet by opening a new window.
     * 
     * @param event
     */
    private void onCsvExportClicked(AppEvent event)
    {
        SheetModelData sheet = this.model.getCurrentSheet();
        if (sheet != null)
        {
            IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();

            String path = ServicesPath.getPath(SheetModelData.SIMPLE_CLASS_NAME);
            path += sheet.getId();
            path += ServicesPath.DATA + ServicesPath.CSV + "/";
            if (this.model.getFilters() != null && this.model.getFilters().getLeftMember() != null)
            {
            	List<IUrlParameter> params = new ArrayList<IUrlParameter>();
            	IUrlParameter filter = this.model.getFilters();
            	for(IUrlParameter param : this.model.getDataStore().getParameters()) 
            	{
            		if(param instanceof LuceneConstraint 
            		   || param instanceof ItemConstraint 
            		   || param instanceof ItemConstraints)
            			params.add(param);
            	}
            	
            	if(params.size() > 0) {
            		params.add(filter);
            		path += ParameterBuilder.parametersToString(params);
            	}
            	else {
            		path += ParameterBuilder.parameterToString(this.model.getFilters());
            	}
            }

            this.view.callCSVService(requestHandler.getServicesUrl() + path);
        }
    }

    private void onSetThumbnailClicked()
    {
        this.view.showThumbnailEditor();
    }

    private void onShowPreviewClicked()
    {
        if (this.view.getSelectedRows() != null && this.view.getSelectedRows().size() > 0)
        {
            Hd3dModelData model = this.view.getSelectedRows().get(0);
            String path = ThumbnailPath.getPath(model, true);

            this.view.showPreviewInDialog(path);
        }
    }

    private void onThumbnailUploaded()
    {
        this.view.refreshEditedThumbnail();
    }

    private void onShowMosaicClicked()
    {
        this.view.showMosaic();
    }

    private void onHideMosaicClicked()
    {
        this.view.hideMosaic();
    }

    private void onShowIdentityClicked()
    {
        final List<Hd3dModelData> selection = this.view.getSelectedRows();
        if (CollectionUtils.isNotEmpty(selection))
        {
            final Hd3dModelData selectedModel = selection.get(0);
            if (selectedModel != null)
            {
                SheetModelData sheet = this.model.getCurrentSheet();
                if (FieldUtils.isShot(sheet.getBoundClassName()))
                {
                    selectedModel.setDefaultPath(ServicesPath.PROJECTS + MainModel.currentProject.getId() + "/"
                            + ServicesPath.SHOTS + selectedModel.getId());
                }
                else if (FieldUtils.isConstituent(sheet.getBoundClassName()))
                {
                    selectedModel.setDefaultPath(ServicesPath.PROJECTS + MainModel.currentProject.getId() + "/"
                            + ServicesPath.CONSTITUENTS + selectedModel.getId());
                }
                this.view.showIdentityWidget(selectedModel, sheet);
            }
        }
    }

    private void registerEvents()
    {
        this.registerEventTypes(ExplorerEvents.CHANGE_VIEW);
        this.registerEventTypes(ExplorerEvents.RELOAD_DATA);
        this.registerEventTypes(ExplorerEvents.SHEET_INFORMATIONS_LOADED);
        this.registerEventTypes(ExplorerEvents.SAVE_DATA);
        this.registerEventTypes(ExplorerEvents.DATA_LOADED);
        this.registerEventTypes(ExplorerEvents.REFRESH_VIEW);
        this.registerEventTypes(ExplorerEvents.AUTO_SAVE_TOGGLE);

        this.registerEventTypes(ExplorerEvents.PRINT_CLICKED);

        this.registerEventTypes(ExplorerEvents.COLUMN_WIDTH_CHANGED);
        this.registerEventTypes(ExplorerEvents.COLUMN_HIDDEN_CHANGED);
        this.registerEventTypes(ExplorerEvents.COLUMN_ALIGNMENT_CHANGED);
        this.registerEventTypes(ExplorerEvents.COLUMN_DATE_FORMAT_CHANGED);
        this.registerEventTypes(ExplorerEvents.COLUMN_GROUPED);
        this.registerEventTypes(ExplorerEvents.COLUMN_UNGROUPED);
        this.registerEventTypes(ExplorerEvents.HEADER_CLICK);

        this.registerEventTypes(ExplorerEvents.NEW_APPLICATION_FILTER_SET);
        this.registerEventTypes(ExplorerEvents.NEW_USER_FILTER_SET);

        this.registerEventTypes(ExplorerEvents.CSV_EXPORT_CLICKED);

        this.registerEventTypes(ExplorerEvents.THUMBNAIL_UPLOADED);
        this.registerEventTypes(ExplorerEvents.THUMBNAIL_EDIT_CLICKED);
        this.registerEventTypes(ExplorerEvents.SHOW_PREVIEW_CLICKED);

        this.registerEventTypes(ExplorerEvents.SHOW_MOSAIC_CLICKED);
        this.registerEventTypes(ExplorerEvents.HIDE_MOSAIC_CLICKED);

        this.registerEventTypes(ExplorerEvents.SHOW_IDENTITY_CLICKED);
        this.registerEventTypes(ExplorerEvents.PAGE_SIZE_CHANGED);

        this.registerEventTypes(CommonEvents.ERROR);
    }

}
