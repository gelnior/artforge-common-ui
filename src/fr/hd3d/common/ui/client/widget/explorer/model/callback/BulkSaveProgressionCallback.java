package fr.hd3d.common.ui.client.widget.explorer.model.callback;

import java.util.ArrayList;
import java.util.List;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.store.Record;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.widget.explorer.model.ExplorerSaveManager;


public class BulkSaveProgressionCallback extends SaveProgressionCallback
{
    private final ArrayList<Hd3dModelData> modelSaved = new ArrayList<Hd3dModelData>();
    private final ArrayList<Record> records;

    public BulkSaveProgressionCallback(ExplorerSaveManager sm, ArrayList<Record> records, List<Hd3dModelData> approvals)
    {
        super(sm, null);
        this.records = records;
        this.modelSaved.addAll(approvals);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void onSuccess(Request request, Response response)
    {
        if (response.getEntity().getLocationRef() != null)
        {
            String identifiers = response.getEntity().getLocationRef().toString();

            String[] urls = identifiers.substring(1, identifiers.length() - 1).split(",");

            for (int i = 0; i < urls.length; i++)
            {
                String url = urls[i];
                if (url != null && url.length() > 0)
                {
                    long id = new Long(url.substring(url.lastIndexOf('/') + 1));
                    modelSaved.get(i).setId(id);
                }
            }
        }
        for (Record record : (List<Record>) records.clone())
        {
            record.commit(false);
        }
        super.onSuccess(request, response);
    }
}
