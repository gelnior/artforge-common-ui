package fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.util.Util;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;

import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;


public class TaskProjectColorRenderer implements GridCellRenderer<TaskModelData>
{
    public Object render(TaskModelData model, String property, ColumnData config, int rowIndex, int colIndex,
            ListStore<TaskModelData> store, Grid<TaskModelData> grid)
    {
        String color = model.get(property);

        return getTypeRendered(color);
    }

    private String getTypeRendered(String color)
    {
        String cellCode = "";
        if (!Util.isEmptyString(color))
        {
            cellCode = "<div style='width:100%; height:100%; text-align:center; background-color:" + color + ";'>"
                    + "&nbsp;" + "</div>";
        }
        return cellCode;
    }
}
