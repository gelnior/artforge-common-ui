package fr.hd3d.common.ui.client.widget.explorer.model.reader;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.reader.Hd3dListJsonReader;
import fr.hd3d.common.ui.client.modeldata.technical.ItemGroupModelData;


public class ItemGroupReader extends Hd3dListJsonReader<ItemGroupModelData>
{
    public ItemGroupReader(ModelType modelType)
    {
        super(modelType);
    }

    public ItemGroupReader()
    {
        super(ItemGroupModelData.getModelType());
    }

    @Override
    public ItemGroupModelData newModelInstance()
    {
        return new ItemGroupModelData();
    }
}
