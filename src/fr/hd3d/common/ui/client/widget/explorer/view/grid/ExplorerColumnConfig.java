package fr.hd3d.common.ui.client.widget.explorer.view.grid;

import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;


/**
 * GXT column configuration extend for two parameters : type and parameter.
 * 
 * @author HD3D
 */
public class ExplorerColumnConfig extends ColumnConfig
{
    private String type;
    private String parameter;
    private Long itemId;

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getParameter()
    {
        return parameter;
    }

    public void setParameter(String parameter)
    {
        this.parameter = parameter;
    }

    public Long getItemId()
    {
        return this.itemId;
    }

    public void setItemId(Long id)
    {
        this.itemId = id;
    }
}
