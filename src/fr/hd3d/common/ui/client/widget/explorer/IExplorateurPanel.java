package fr.hd3d.common.ui.client.widget.explorer;

import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;


public interface IExplorateurPanel
{
    public abstract void setCurrentSheet(SheetModelData sheet);

    public abstract SheetModelData getCurrentSheet();
}
