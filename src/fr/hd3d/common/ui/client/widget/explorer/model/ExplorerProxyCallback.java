package fr.hd3d.common.ui.client.widget.explorer.model;

import com.extjs.gxt.ui.client.data.DataReader;
import com.extjs.gxt.ui.client.data.PagingLoadResult;
import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.service.proxy.ServicesPagingProxyCallback;


public class ExplorerProxyCallback extends ServicesPagingProxyCallback<Hd3dModelData>
{

    public ExplorerProxyCallback(DataReader<PagingLoadResult<Hd3dModelData>> reader,
            AsyncCallback<PagingLoadResult<Hd3dModelData>> callback, int offset)
    {
        super(reader, callback, offset);
    }

}
