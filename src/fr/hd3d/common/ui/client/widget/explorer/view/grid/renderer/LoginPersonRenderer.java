package fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer;

import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONValue;

import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;


/**
 * PersonRenderer is a cell grid renderer that displays login for a single Person Model Data object and displays logins
 * for list of Person Model Data.
 * 
 * @author HD3D
 */
public class LoginPersonRenderer extends PersonRenderer
{
    /**
     * @param user
     *            JSON from which name is extracted.
     * @return User name from JSON Object (it means First and Last name).
     */
    @Override
    protected String getUserName(JSONObject user)
    {
        JSONValue login = user.get(PersonModelData.PERSON_LOGIN_FIELD);

        return login.isString().stringValue();
    }

}
