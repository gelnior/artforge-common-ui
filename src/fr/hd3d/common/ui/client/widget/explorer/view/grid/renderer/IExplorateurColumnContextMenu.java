package fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.GridEvent;

public interface IExplorateurColumnContextMenu<M extends ModelData>
{

    void handleGridEvent(GridEvent<M> ge);
    
}
