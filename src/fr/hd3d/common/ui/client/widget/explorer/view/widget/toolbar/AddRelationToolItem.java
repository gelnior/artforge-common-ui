package fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar;

import com.extjs.gxt.ui.client.widget.button.Button;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.listener.AddRelationClickListener;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.ExplorateurCSS;


/**
 * Tool Item that raises an ADD_RELATION event when it is clicked.
 * 
 * @author HD3D
 */
public class AddRelationToolItem extends Button
{
    /** Constant strings to display : dialog messages, button label... */
    public static final CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    /**
     * Default Constructor.
     */
    public AddRelationToolItem()
    {
        super();

        this.setStyle();
        this.registerListener();
    }

    /**
     * Register on tool item click event. When the button is clicked, it raise the ADD_ROW event.
     */
    private void registerListener()
    {
        this.addSelectionListener(new AddRelationClickListener());
    }

    /**
     * Set image style.
     */
    private void setStyle()
    {
        setIconStyle(ExplorateurCSS.ADD_RELATION_ICON_CLASS);

        this.setToolTip(CONSTANTS.AddRelation());
    }
}
