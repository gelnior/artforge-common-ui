package fr.hd3d.common.ui.client.widget.explorer.model.reader;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.reader.Hd3dListJsonReader;
import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;


public class ItemReader extends Hd3dListJsonReader<ItemModelData>
{

    public ItemReader(ModelType modelType)
    {
        super(modelType);
    }

    public ItemReader()
    {
        super(ItemModelData.getModelType());
    }

    @Override
    public ItemModelData newModelInstance()
    {
        return new ItemModelData();
    }

}
