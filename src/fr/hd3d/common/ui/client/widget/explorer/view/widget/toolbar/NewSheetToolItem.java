package fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar;

import com.extjs.gxt.ui.client.widget.button.Button;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.ExplorateurCSS;
import fr.hd3d.common.ui.client.widget.sheeteditor.listener.NewSheetClickListener;


/**
 * Button that forward the SheetEditorEvents.SHOW_NEW_SHEET_WINDOW event.
 */
public class NewSheetToolItem extends Button
{
    /** Constant strings to display : dialog messages, button label... */
    public final static CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    /**
     * Default constructor
     */
    public NewSheetToolItem()
    {
        super();

        this.setStyle();
        this.registerListener();
    }

    private void registerListener()
    {
        this.addSelectionListener(new NewSheetClickListener());
    }

    private void setStyle()
    {
        this.setIconStyle(ExplorateurCSS.NEW_VIEW_ICON_CLASS);
        this.getElement().setNodeValue(ExplorateurCSS.NEW_VIEW_ICON_CLASS);

        this.setToolTip(CONSTANTS.CreateSheet());
    }
}
