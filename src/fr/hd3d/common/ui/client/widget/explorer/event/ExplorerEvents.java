package fr.hd3d.common.ui.client.widget.explorer.event;

import com.extjs.gxt.ui.client.event.EventType;


public class ExplorerEvents
{
    public static final String EVENT_VAR_MODELDATA = "selected-model";

    public static final int EXPLORATEUR_START_EVENT = 12000;

    public static final EventType CHANGE_VIEW = new EventType(EXPLORATEUR_START_EVENT + 1);

    /**
     * view information loaded
     */
    public static final EventType SHEET_INFORMATIONS_LOADED = new EventType(EXPLORATEUR_START_EVENT + 2);

    public static final EventType REFRESH_VIEW = new EventType(EXPLORATEUR_START_EVENT + 9);
    public static final EventType RELOAD_DATA = new EventType(EXPLORATEUR_START_EVENT + 10);

    /**
     * before loading the data
     */
    public static final EventType BEFORE_LOAD_DATA = new EventType(EXPLORATEUR_START_EVENT + 11);

    /**
     * data view information loaded
     */
    public static final EventType DATA_LOADED = new EventType(EXPLORATEUR_START_EVENT + 12);
    public static final EventType SAVE_DATA = new EventType(EXPLORATEUR_START_EVENT + 20);
    public static final EventType BEFORE_SAVE = new EventType(EXPLORATEUR_START_EVENT + 21);
    public static final EventType DURING_SAVE = new EventType(EXPLORATEUR_START_EVENT + 22);
    public static final EventType AFTER_SAVE = new EventType(EXPLORATEUR_START_EVENT + 23);

    public static final EventType FIRST_COLUMN_FILTER_UPDATE = new EventType(EXPLORATEUR_START_EVENT + 33);

    public static final EventType SHOW_CONSTRAINT_PANEL = new EventType(EXPLORATEUR_START_EVENT + 38);
    public static final EventType HIDE_CONSTRAINT_PANEL = new EventType(EXPLORATEUR_START_EVENT + 39);

    public static final EventType AUTO_SAVE_TOGGLE = new EventType();

    /** Manipulating Data */
    public static final EventType NEW_ROW = new EventType(EXPLORATEUR_START_EVENT + 50);
    public static final EventType DELETE_ROW = new EventType(EXPLORATEUR_START_EVENT + 51);

    public static final EventType SELECT_CHANGE = new EventType(EXPLORATEUR_START_EVENT + 60);
    public static final EventType RELATIONS_ADD_CLICK = new EventType(EXPLORATEUR_START_EVENT + 61);

    public static final EventType PRINT_CLICKED = new EventType();

    public static final EventType BEFORE_SET_COLUMN_MODEL = new EventType();
    public static final EventType COLUMN_WIDTH_CHANGED = new EventType();
    public static final EventType COLUMN_HIDDEN_CHANGED = new EventType();
    public static final EventType COLUMN_ALIGNMENT_CHANGED = new EventType();
    public static final EventType COLUMN_DATE_FORMAT_CHANGED = new EventType();
    public static final EventType COLUMN_GROUPED = new EventType();
    public static final EventType COLUMN_UNGROUPED = new EventType();

    public static final EventType HEADER_CLICK = new EventType();

    public static final EventType CSV_EXPORT_CLICKED = new EventType();
    public static final EventType CSV_EXPORT_SUCCESS = new EventType();

    public static final EventType NEW_USER_FILTER_SET = new EventType();

    public static final EventType NEW_APPLICATION_FILTER_SET = new EventType();

    public static final EventType THUMBNAIL_UPLOADED = new EventType();
    public static final EventType THUMBNAIL_EDIT_CLICKED = new EventType();
    public static final EventType SHOW_PREVIEW_CLICKED = new EventType();

    public static final EventType SHOW_MOSAIC_CLICKED = new EventType();
    public static final EventType HIDE_MOSAIC_CLICKED = new EventType();

    public static final EventType SHOW_IDENTITY_CLICKED = new EventType();
    public static final EventType ENTER_KEY_PRESSED = new EventType();

    public static final EventType PAGE_SIZE_CHANGED = new EventType();

    public static final EventType NEW_GRID_BUILT = new EventType();

}
