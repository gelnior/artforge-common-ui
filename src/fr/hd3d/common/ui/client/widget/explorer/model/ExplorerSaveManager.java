package fr.hd3d.common.ui.client.widget.explorer.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.BaseObservable;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.Record;
import com.google.gwt.json.client.JSONObject;

import fr.hd3d.common.client.Renderer;
import fr.hd3d.common.ui.client.calendar.DateFormat;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.modeldata.production.StepModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ItemGroupModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.BulkRequests;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;
import fr.hd3d.common.ui.client.widget.explorer.model.callback.BulkSaveProgressionCallback;
import fr.hd3d.common.ui.client.widget.explorer.model.callback.SaveProgressionCallback;
import fr.hd3d.common.ui.client.widget.explorer.model.modeldata.IColumn;
import fr.hd3d.common.ui.client.widget.explorer.model.modeldata.IColumnGroup;


/**
 * This class handles saving of all explorer data : item saving, step saving and note saving.
 * 
 * @author HD3D
 */
public class ExplorerSaveManager extends BaseObservable
{
    /** True to not notify via events that saving starts and finishes. */
    private Boolean silent;

    private Integer nbSaved;
    private Integer nbRequest;

    /** List of approvals to save. */
    private final ArrayList<Hd3dModelData> approvals = new ArrayList<Hd3dModelData>();
    /** List of steps to save. */
    private final ArrayList<Hd3dModelData> steps = new ArrayList<Hd3dModelData>();
    /** List of records (lines) to save. */
    private final ArrayList<Record> recs = new ArrayList<Record>();

    public ExplorerSaveManager()
    {}

    public Long getProgression()
    {
        return Long.valueOf(100 * nbSaved / nbRequest);
    }

    public void onSaveProgress(Record record)
    {
        if (record != null)
            record.commit(false);

        if (!this.silent)
        {
            AppEvent event = new AppEvent(ExplorerEvents.DURING_SAVE, this.getProgression());
            EventDispatcher.forwardEvent(event);
        }
    }

    public void oneMoreSaved()
    {
        this.nbSaved++;
        this.checkProgression();
    }

    public void saveRecords(SheetModelData sheet, List<Record> records, Boolean silent)
    {
        this.reset();

        this.silent = silent;

        if (!this.silent)
            EventDispatcher.forwardEvent(ExplorerEvents.BEFORE_SAVE);

        if (records.size() == 0)
        {
            if (!this.silent)
                EventDispatcher.forwardEvent(ExplorerEvents.AFTER_SAVE, 100L);
        }
        else
        {
            this.saveRecords(sheet, records);
            this.saveApprovals();
            this.saveSteps();
            this.checkProgression();
        }
    }

    /** Clears all list of data to save. */
    private void reset()
    {
        approvals.clear();
        steps.clear();
        recs.clear();

        nbSaved = 0;
        nbRequest = 0;
    }

    /**
     * Save all modified records.
     * 
     * @param sheet
     *            The sheet describing the records.
     * @param records
     *            The records to save.
     */
    private void saveRecords(SheetModelData sheet, List<Record> records)
    {
        for (Record record : records)
        {
            Hd3dModelData model = (Hd3dModelData) record.getModel();

            if (model.getId() != null)
            {
                Map<String, String> request = getRequest(record, sheet);
                for (Map.Entry<String, String> entry : request.entrySet())
                {
                    String url = entry.getKey();
                    String value = entry.getValue();

                    nbRequest++;
                    BaseCallback callback = new SaveProgressionCallback(this, record);
                    RestRequestHandlerSingleton.getInstance().putRequest(url + model.getId(), callback, value);
                }
            }
        }
    }

    private Map<String, String> getRequest(Record record, SheetModelData sheet)
    {
        // FastMap<String> request = new FastMap<String>();
        // FastMap<IColumn> columnMap = new FastMap<IColumn>();
        //
        // List<ItemGroupModelData> groups = sheet.getColumnGroups();
        // Map<String, Object> changes = record.getChanges();
        // Set<String> keys = changes.keySet();
        // List<IColumn> columns = sheet.getColumns();
        //
        // // Make column findable via their data index.
        // for (IColumn column : columns)
        // columnMap.put(column.getDataIndex(), column);
        //
        // for()
        // for (IColumn column : columns)
        // {
        // String json = createPutParameters(columnMap, record, keys);
        // String url = ServicesPath.SHEETS + sheet.getId() + "/" + ServicesPath.ITEM_GROUPS
        // + ((ItemModelData) column).getItemGroupId() + "/?objectId=";
        // if (json.length() > 10)
        // {
        // request.put(url, json);
        // }
        // }
        // return request;

        Map<String, String> request = new HashMap<String, String>();

        List<ItemGroupModelData> groups = sheet.getColumnGroups();
        Map<String, Object> changes = record.getChanges();
        Set<String> keys = changes.keySet();
        for (IColumnGroup columnGroup : groups)
        {
            List<IColumn> columns = columnGroup.getColumns();
            String json = createPutParameters(columns, record, keys);
            String url = ServicesPath.SHEETS + sheet.getId() + "/" + ServicesPath.ITEM_GROUPS + columnGroup.getId()
                    + "/?objectId=";
            if (json.length() > 10)
            {
                request.put(url, json);
            }
        }
        return request;
    }

    private String createPutParameters(List<IColumn> columns, Record record, Set<String> keys)
    {
        StringBuilder json = new StringBuilder();
        for (String key : keys)
        {
            for (IColumn column : columns)
            {
                if (column.getDataIndex().equals(key))
                {
                    if (FieldUtils.isApproval(column.getQuery())
                            || column.getDataIndex().startsWith(ItemModelData.APPROVAL_NOTE_QUERY)
                            || column.getRenderer().startsWith(Renderer.APPROVAL))
                    {
                        this.addApprovals(record, key);
                    }
                    else if (FieldUtils.isStep(column.getQuery()))
                    {
                        this.addSteps(record, key, column.getParameter());
                    }
                    else
                    {
                        ItemModelData item = (ItemModelData) column;
                        Object value = record.get(key);
                        Object extra = record.get(ItemModelData.EXTRA_FIELD);

                        json.append("{\"name\":\"");
                        json.append(item.getName());

                        json.append("\", \"value\":");
                        json.append(getFormatedValue(item, value));

                        json.append(", \"extra\":");
                        json.append(getFormatedValue(item, extra));
                        json.append("},");

                    }
                }
            }
        }

        if (json.length() > 10)
        {
            String finalJson = json.substring(0, json.length() - 1);
            finalJson = "[" + finalJson + "]";

            return finalJson;
        }
        else
        {
            return "";
        }

    }

    // private String createPutParameters(FastMap<IColumn> columnMap, Record record, Set<String> keys)
    // {
    // StringBuilder json = new StringBuilder();
    // for (String key : keys)
    // {
    // IColumn column = columnMap.get(key);
    //
    // if (column != null)
    // {
    // if (FieldUtils.isApproval(column.getQuery())
    // || column.getDataIndex().startsWith(ItemModelData.APPROVAL_NOTE_QUERY)
    // || column.getRenderer().startsWith(Renderer.APPROVAL))
    // {
    // this.addApprovals(record, key);
    // }
    // else if (FieldUtils.isStep(column.getQuery()))
    // {
    // this.addSteps(record, key, column.getParameter());
    // }
    // else
    // {
    // ItemModelData item = (ItemModelData) column;
    // Object value = record.get(key);
    // Object extra = record.get(ItemModelData.EXTRA_FIELD);
    //
    // json.append("{\"name\":\"");
    // json.append(item.getName());
    //
    // json.append("\", \"value\":");
    // json.append(getFormatedValue(item, value));
    //
    // json.append(", \"extra\":");
    // json.append(getFormatedValue(item, extra));
    // json.append("},");
    //
    // }
    // }
    // }
    //
    // if (json.length() > 10)
    // {
    // String finalJson = json.substring(0, json.length() - 1);
    // finalJson = "[" + finalJson + "]";
    //
    // return finalJson;
    // }
    // else
    // {
    // return "";
    // }
    //
    // }

    private String getFormatedValue(ItemModelData item, Object value)
    {
        StringBuilder json = new StringBuilder();
        if (value instanceof List<?>)
        {
            @SuppressWarnings("unchecked")
            List<Object> array = (List<Object>) value;
            for (Iterator<Object> iterator = array.iterator(); iterator.hasNext();)
            {
                Object arrayValue = iterator.next();
                if (json.length() > 0)
                {
                    json.append(",");
                }
                json.append(getFormatedValue(item, arrayValue));
            }
        }
        else if (value instanceof Date)
        {
            json.append("\"" + DateFormat.DATE_TIME.format((Date) value) + "\"");
        }
        else if (value instanceof JSONObject)
        {
            JSONObject val = (JSONObject) value;
            if (val.get(Hd3dModelData.ID_FIELD) != null)
            {
                json.append(val.get(Hd3dModelData.ID_FIELD).toString());
            }
        }
        else if (value instanceof String || item.getType().endsWith("String") || value == null)
        {
            if (value == null)
            {
                value = "";
            }

            String stringValue = (String) value;
            stringValue = stringValue.replace("\"", "\\\"");
            stringValue = stringValue.replace("\\", "\\\\");
            stringValue = stringValue.replace("/", "\\/");
            stringValue = stringValue.replace("\b", "\\b");
            stringValue = stringValue.replace("\f", "\\f");
            stringValue = stringValue.replace("\n", "\\n");
            stringValue = stringValue.replace("\r", "\\r");
            stringValue = stringValue.replace("\r", "\\t");

            json.append("\"" + stringValue + "\"");
        }

        else if (value instanceof ModelData)
        {
            Object obj = ((ModelData) value).get(Hd3dModelData.ID_FIELD);
            int id = (int) Double.parseDouble(obj.toString());
            json.append("\"" + id + "\"");
        }
        else if (item.getItemType().equals("metaData"))
        {
            json.append("\"" + value.toString() + "\"");
        }
        else
        {
            json.append(value.toString());
        }
        return json.toString();
    }

    /**
     * Add an approval note to the approval note to save list.
     * 
     * @param record
     *            The record that bears the approval note.
     * @param key
     *            The key of the record field in which the note is stored.
     */
    @SuppressWarnings("unchecked")
    private void addApprovals(Record record, String key)
    {
        recs.add(record);
        Object value = record.get(key);
        if (value instanceof List<?>)
        {
            for (ApprovalNoteModelData approval : (List<ApprovalNoteModelData>) value)
            {
                approvals.add(approval);
            }
        }
    }

    /**
     * Makes two bulks requests for steps to save (one post request for step creation, one put request for step
     * modification).
     */
    private void saveApprovals()
    {
        List<Hd3dModelData> postApprovals = new ArrayList<Hd3dModelData>();
        List<Hd3dModelData> putApprovals = new ArrayList<Hd3dModelData>();

        // Distinguishes modifications from creations.
        for (Hd3dModelData approval : approvals)
        {
            approval.setDefaultPath(ServicesPath.getApprovalNotePath(null));
            if (approval.getId() == null)
            {
                postApprovals.add(approval);
            }
            else
            {
                putApprovals.add(approval);
            }
        }

        // Save creations
        if (postApprovals.size() > 0)
        {
            nbRequest++;
            BaseCallback callback = new BulkSaveProgressionCallback(this, recs, postApprovals);
            BulkRequests.bulkPost(postApprovals, callback);
        }

        // Save modifications
        if (putApprovals.size() > 0)
        {
            nbRequest++;
            BaseCallback callback = new BulkSaveProgressionCallback(this, recs, postApprovals);
            BulkRequests.bulkPut(putApprovals, callback);
        }
    }

    /**
     * Add steps to the step to save list.
     * 
     * @param record
     *            The record that bears the step to save.
     * @param key
     *            The key of the record field in which the step is stored.
     * @param transformer
     *            The transformer which is in fact the ID of the step task type.
     */
    private void addSteps(Record record, String key, String transformer)
    {
        if (transformer != null)
        {
            Object stepVal = record.get(key);
            if (stepVal instanceof Long)
            {
                StepModelData step = new StepModelData((Hd3dModelData) record.getModel(), transformer,
                        (Long) record.get(key));

                record.set(key, step);
                this.steps.add(step);
                recs.add(record);
            }
            else if (stepVal instanceof StepModelData)
            {
                this.steps.add((StepModelData) stepVal);
                recs.add(record);
            }
        }
    }

    /**
     * Makes two bulks requests for approvals to save (one post request for approval creation, one put request for
     * approval modification).
     */
    private void saveSteps()
    {
        List<Hd3dModelData> postSteps = new ArrayList<Hd3dModelData>();
        List<Hd3dModelData> putSteps = new ArrayList<Hd3dModelData>();

        // Distinguishes modifications from creations.
        for (Hd3dModelData step : this.steps)
        {
            step.setDefaultPath(ServicesPath.getOneSegmentPath(ServicesPath.STEPS, step.getId()));
            if (step.getId() == null)
            {
                postSteps.add(step);
            }
            else
            {
                putSteps.add(step);
            }
        }

        // Save creations.
        if (postSteps.size() > 0)
        {
            nbRequest++;
            BaseCallback callback = new BulkSaveProgressionCallback(this, recs, postSteps);
            BulkRequests.bulkPost(postSteps, callback);
        }

        // Save modifications.
        if (putSteps.size() > 0)
        {
            nbRequest++;
            BaseCallback callback = new BulkSaveProgressionCallback(this, recs, postSteps);
            BulkRequests.bulkPut(putSteps, callback);
        }
    }

    /**
     * Check if saving is finished. It sens an AFTER SAVE event is silent is set to false.
     */
    private void checkProgression()
    {
        if (this.nbSaved.equals(this.nbRequest))
        {
            if (!this.silent)
            {
                AppEvent event = new AppEvent(ExplorerEvents.AFTER_SAVE, 100L);
                EventDispatcher.forwardEvent(event);
            }
        }
    }

}
