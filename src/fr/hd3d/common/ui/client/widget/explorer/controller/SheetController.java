package fr.hd3d.common.ui.client.widget.explorer.controller;

import java.util.ArrayList;
import java.util.List;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.data.ModelType;
import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.BaseObservable;
import com.extjs.gxt.ui.client.event.Listener;

import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.technical.ItemGroupModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.widget.explorer.event.ItemGroupEvents;
import fr.hd3d.common.ui.client.widget.explorer.event.SheetEvents;
import fr.hd3d.common.ui.client.widget.explorer.model.modeldata.IColumnGroup;
import fr.hd3d.common.ui.client.widget.explorer.model.reader.ItemGroupReader;


public class SheetController extends BaseObservable
{
    private final SheetModelData sheet;
    public Boolean groupLoaded = false;

    public SheetController(SheetModelData sheet)
    {
        this.sheet = sheet;
    }

    public void setGroupsLoaded(Boolean groupLoaded)
    {
        this.groupLoaded = groupLoaded;
    }

    public void loadSheetItems()
    {
        if (groupLoaded == false)
        {
            loadSheetItemGroup();
        }
        else
        {
            if (areAllItemsLoaded() == true)
            {
                this.onAllItemsLoaded();
            }
        }
    }

    private void loadSheetItemGroup()
    {
        String url = ServicesPath.SHEETS + sheet.getId().toString() + "/" + ServicesPath.ITEM_GROUPS;
        if (sheet.getProjectId() != null)
            url = ServicesPath.PROJECTS + sheet.getProjectId() + "/" + url;

        RestRequestHandlerSingleton.getInstance().getRequest(url, new BaseCallback() {
            @Override
            public void onSuccess(Request request, Response response)
            {
                onItemGroupLoaded(request, response);
            }
        });
    }

    private void onItemGroupLoaded(Request request, Response response)
    {
        try
        {
            String json = response.getEntity().getText();

            ModelType itemGroupModelType = ItemGroupModelData.getModelType();
            ItemGroupReader reader = new ItemGroupReader(itemGroupModelType);
            List<ItemGroupModelData> result = reader.read(json).getData();
            List<ItemGroupModelData> columnResult = new ArrayList<ItemGroupModelData>();

            for (ItemGroupModelData itemGroup : result)
            {
                columnResult.add(itemGroup);
            }

            sheet.setItemGroups(columnResult);
            this.registerItemGroupListener();
            this.loadItemGroupsItems();
            this.fireEvent(SheetEvents.ITEMGROUP_LOADED);
        }
        catch (Exception e)
        {
            Logger.error("Error occurs while parsing item group data", e);
        }
    }

    private void loadItemGroupsItems()
    {
        List<ItemGroupModelData> itemGroups = sheet.getColumnGroups();
        for (ItemGroupModelData group : itemGroups)
        {
            group.controller.loadItems();
        }
    }

    private void registerItemGroupListener()
    {
        List<ItemGroupModelData> itemGroups = sheet.getColumnGroups();
        if (itemGroups.size() == 0)
        {
            if (areAllItemsLoaded())
            {
                onAllItemsLoaded();
            }
        }
        else
        {
            for (IColumnGroup igroup : itemGroups)
            {
                Listener<BaseEvent> listener = new Listener<BaseEvent>() {
                    public void handleEvent(BaseEvent be)
                    {
                        if (areAllItemsLoaded())
                        {
                            onAllItemsLoaded();
                        }
                    }
                };
                ItemGroupModelData group = (ItemGroupModelData) igroup;
                group.controller.addListener(ItemGroupEvents.ITEMS_LOADED, listener);
            }
        }
    }

    private Boolean areAllItemsLoaded()
    {
        List<ItemGroupModelData> itemGroups = sheet.getColumnGroups();
        for (ItemGroupModelData group : itemGroups)
        {
            if (group.areColumnsLoaded() == false)
            {
                return false;
            }
        }
        return true;
    }

    private void onAllItemsLoaded()
    {
        this.fireEvent(SheetEvents.ALL_ITEMS_LOADED);
    }
}
