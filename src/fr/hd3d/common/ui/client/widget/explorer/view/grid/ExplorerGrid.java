package fr.hd3d.common.ui.client.widget.explorer.view.grid;

import java.util.List;

import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.store.Record;
import com.extjs.gxt.ui.client.util.Util;
import com.extjs.gxt.ui.client.widget.form.Field;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.EditorGrid;
import com.extjs.gxt.ui.client.widget.grid.GridGroupRenderer;
import com.extjs.gxt.ui.client.widget.grid.GroupingView;
import com.extjs.gxt.ui.client.widget.menu.Menu;
import com.extjs.gxt.ui.client.widget.menu.MenuItem;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.StepModelData;
import fr.hd3d.common.ui.client.widget.EasyMenu;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;
import fr.hd3d.common.ui.client.widget.explorer.model.ExplorerModel;
import fr.hd3d.common.ui.client.widget.explorer.model.ExplorerStore;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.listener.ColumnHiddenChangeListener;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.listener.ColumnWidthChangeListener;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.listener.ExplorerGridListener;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.Hd3dCheckColumnConfig;
import fr.hd3d.common.ui.client.widget.grid.editor.StepDurationEditor;


/**
 * The explorer grid. It has some specifics : easily reconfigurable for explorer, notifying controllers when a column is
 * hidden or when its width changes.
 * 
 * @author HD3D
 */
public class ExplorerGrid extends EditorGrid<Hd3dModelData>
{
    /** Grid listener to handle events like context menu of after edit events. */
    private ExplorerGridListener listener;
    /** Default context menu needed when renderer changes context menu. */
    private EasyMenu defaultContextMenu = new EasyMenu();

    /**
     * Default constructor to create a new explorer grid
     * 
     * @param store
     * @param cm
     */
    public ExplorerGrid(ListStore<Hd3dModelData> store, ColumnModel cm, ExplorerSelectionModel selectionModel)
    {
        super(store, cm);
        EventDispatcher.forwardEvent(ExplorerEvents.BEFORE_SET_COLUMN_MODEL, cm);

        this.setView(new ExplorerGridView());
        this.setStyles(selectionModel);

        this.configure();
    }

    /**
     * Reconfigures the grid to use a different Store and Column Model. The View will be bound to the new objects and
     * refreshed.
     * 
     * @param store
     *            the new store
     * @param cm
     *            the new column model
     */
    @Override
    public void reconfigure(ListStore<Hd3dModelData> store, ColumnModel cm)
    {
        EventDispatcher.forwardEvent(ExplorerEvents.BEFORE_SET_COLUMN_MODEL, cm);
        super.reconfigure(store, cm);
        this.setColumnModelListeners();
    }

    /** @return Default context menu needed when renderer changes context menu. */
    public Menu getDefaultContextMenu()
    {
        return this.defaultContextMenu;
    }

    public void autoEditMultiple(GridEvent<Hd3dModelData> ge)
    {
        List<Hd3dModelData> selection = this.getSelectionModel().getSelectedItems();
        ColumnConfig column = this.cm.getColumn(ge.getColIndex());
        CellEditor cellEditor = null;
        if (column != null)
        {
            cellEditor = column.getEditor();
        }
        if (selection.size() > 1 && cellEditor != null && (cellEditor).getField() instanceof Field<?>
                || column instanceof Hd3dCheckColumnConfig)
        {
            if (cellEditor instanceof StepDurationEditor)
            {
                Object stepVal = ge.getValue();
                Long estimatedDuration = null;
                if (stepVal instanceof StepModelData)
                {
                    estimatedDuration = ((StepModelData) stepVal).getEstimatedDuration();
                }
                else if (stepVal instanceof Long)
                {
                    estimatedDuration = (Long) stepVal;
                }

                for (Hd3dModelData model : selection)
                {
                    if (ge.getModel() != model)
                    {
                        Record rec = this.store.getRecord(model);
                        Object step = rec.get(ge.getProperty());
                        if (step instanceof StepModelData)
                        {
                            ((StepModelData) step).setEstimatedDuration(estimatedDuration);
                            StepModelData stepModel = new StepModelData();
                            Hd3dModelData.copy((StepModelData) step, stepModel);
                            rec.set(ge.getProperty(), stepModel);
                        }
                        else
                        {
                            rec.set(ge.getProperty(), estimatedDuration);
                        }
                    }
                }
            }
            else
            {
                for (Hd3dModelData model : selection)
                {
                    if (ge.getModel() != model)
                    {
                        Record rec = this.store.getRecord(model);
                        rec.set(ge.getProperty(), ge.getValue());
                    }
                }
            }
        }

        if (selection.size() > 0)
        {
            if (column.getDataIndex().equals(ConstituentModelData.CONSTITUENT_LABEL))
            {
                AppEvent event = new AppEvent(CommonEvents.NAME_CHANGED, selection);
                EventDispatcher.forwardEvent(event);
            }
        }
    }

    /**
     * @return Context menu with on item for identity sheet edition.
     */
    private EasyMenu createEditContextMenu()
    {
        final EasyMenu editContextMenu = new EasyMenu();

        if (ExplorerModel.isIdentityDialogEnabled())
        {
            editContextMenu.addItem("Display work object infos", ExplorerEvents.SHOW_IDENTITY_CLICKED, null);
        }
        else
        {
            MenuItem item = editContextMenu.addItem("No action available", (EventType) null, null);
            item.disable();
        }

        this.defaultContextMenu = editContextMenu;

        return editContextMenu;
    }

    /**
     * Set grid styles.
     * 
     * @param selectionModel
     *            The selection model to use for this grid.
     */
    private void setStyles(ExplorerSelectionModel selectionModel)
    {
        this.setTrackMouseOver(false);
        this.addStyleName("hd3d-explorer");
        this.editSupport.setClicksToEdit(ClicksToEdit.TWO);

        selectionModel.init(this, (ExplorerGridView) this.getView());
        this.setSelectionModel(selectionModel);

    }

    /**
     * Set default context menu and register listeners.
     */
    private void configure()
    {
        this.setContextMenu(this.createEditContextMenu());
        this.registerListeners();
    }

    /**
     * Register listeners like context menu event of after edit event. This is needed for cell with specific context
     * menu of for multiple edition.<br>
     * <br>
     * Column model also needs listener when a column width changes or is hidden. This information must be save to user
     * settings.
     */
    private void registerListeners()
    {
        if (this.listener == null)
        {
            this.listener = new ExplorerGridListener(this);

            if (!this.getListeners(Events.ContextMenu).contains(listener))
                this.addListener(Events.ContextMenu, listener);
            if (!this.getListeners(Events.AfterEdit).contains(listener))
                this.addListener(Events.AfterEdit, listener);
        }
        this.setColumnModelListeners();
    }

    /**
     * Set Column model listeners when a column width changes or is hidden. This information must be save to user
     * settings.
     */
    private void setColumnModelListeners()
    {
        this.cm.addListener(Events.WidthChange, new ColumnWidthChangeListener());
        this.cm.addListener(Events.HiddenChange, new ColumnHiddenChangeListener());
    }

    public void setSortColumn(String sortColumn)
    {
        if (!Util.isEmptyString(sortColumn))
        {
            SortDir sortDir = SortDir.ASC;
            if (sortColumn.startsWith("-"))
            {
                sortDir = SortDir.DESC;
                sortColumn = sortColumn.substring(1);
            }
            this.store.setSortDir(sortDir);
            int colIndex = 0;
            while (colIndex < this.cm.getColumnCount())
            {
                ColumnConfig column = this.cm.getColumn(colIndex);
                if (column instanceof ExplorerColumnConfig)
                    if (((ExplorerColumnConfig) column).getItemId() != null
                            && sortColumn.equals(((ExplorerColumnConfig) column).getItemId().toString()))
                        break;
                colIndex++;
            }
            ((ExplorerGridView) this.view).setSortIcon(colIndex, sortDir);
        }
    }

    public void setGroupColumn(String groupColumn)
    {
        if (groupColumn != null)
            for (int i = 0; i < this.cm.getColumnCount(); i++)
            {
                ColumnConfig column = this.cm.getColumn(i);
                if (column instanceof ExplorerColumnConfig)
                {
                    if (((ExplorerColumnConfig) column).getItemId() != null
                            && groupColumn.equals(((ExplorerColumnConfig) column).getItemId().toString()))
                    {
                        ExplorerStore explorerStore = (ExplorerStore) this.store;
                        ((GroupingView) this.getView()).setGroupRenderer((GridGroupRenderer) column.getRenderer());
                        explorerStore.groupBy(column.getDataIndex());
                    }
                }
            }
    }

}
