package fr.hd3d.common.ui.client.widget.explorer.model;

import java.util.List;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.BaseObservable;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.store.Record;
import com.extjs.gxt.ui.client.store.Store;
import com.extjs.gxt.ui.client.store.StoreEvent;

import fr.hd3d.common.client.enums.EConstraintLogicalOperator;
import fr.hd3d.common.ui.client.config.CommonConfig;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.service.parameter.Constraints;
import fr.hd3d.common.ui.client.service.parameter.IUrlParameter;
import fr.hd3d.common.ui.client.service.parameter.ItemConstraints;
import fr.hd3d.common.ui.client.service.parameter.LogicConstraint;
import fr.hd3d.common.ui.client.service.store.ServicesPagingStore;
import fr.hd3d.common.ui.client.setting.UserSettings;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.widget.explorer.controller.ExplorerController;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;
import fr.hd3d.common.ui.client.widget.explorer.event.SheetEvents;
import fr.hd3d.common.ui.client.widget.explorer.model.modeldata.IColumn;


public class SheetExplorerModel extends BaseObservable
{
    private final SheetDataRetriever retriever;
    private SheetModelData currentView;
    private final ExplorerSaveManager saveManager;
    private String orderColumn = "";

    public SheetExplorerModel()
    {
        retriever = new SheetDataRetriever();
        saveManager = new ExplorerSaveManager();

        this.setPaginationQuantity();
        this.setListeners();
    }

    private void setPaginationQuantity()
    {
        Integer limit = UserSettings.getSettingAsInteger(CommonConfig.SETTING_PAGINATION_QUANTITY);
        if (limit != null)
        {
            this.retriever.setLimit(limit);
        }
    }

    private void setListeners()
    {
        Listener<BaseEvent> saveManagerListener = new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                fireEvent(be.getType(), be);
            }
        };
        saveManager.addListener(ExplorerEvents.BEFORE_SAVE, saveManagerListener);
        saveManager.addListener(ExplorerEvents.DURING_SAVE, saveManagerListener);
        saveManager.addListener(ExplorerEvents.AFTER_SAVE, saveManagerListener);

        this.retriever.getDataStore().addListener(Store.Update, new Listener<StoreEvent<Hd3dModelData>>() {
            public void handleEvent(StoreEvent<Hd3dModelData> be)
            {
                onStoreUpdate();
            }
        });
    }

    private void onStoreUpdate()
    {
        if (ExplorerController.autoSave)
        {
            Boolean silent = Boolean.TRUE;
            EventDispatcher.forwardEvent(ExplorerEvents.SAVE_DATA, silent);
        }
    }

    public void setActiveView(SheetModelData view)
    {
        SheetModelData sheet = view;

        if (currentView != null)
        {
            currentView.controller.removeAllListeners();
        }
        retriever.getDataStore().removeAll();

        currentView = sheet;
        if (currentView != null)
        {
            currentView.controller.loadSheetItems();
            setActiveViewListener();
        }
    }

    private void setActiveViewListener()
    {
        currentView.controller.removeAllListeners();
        currentView.controller.addListener(SheetEvents.ALL_ITEMS_LOADED, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                onViewInformationLoaded();
            }
        });
    }

    private void onViewInformationLoaded()
    {
        EventType eventType = ExplorerEvents.SHEET_INFORMATIONS_LOADED;
        List<IColumn> columns = currentView.getColumns();
        EventDispatcher.forwardEvent(eventType, columns);
        if (this.currentView != null && columns.size() > 0)
        {
            for (IColumn column : columns)
            {
                String type = column.getType();
                if (FieldUtils.isNativeField(type))
                {
                    orderColumn = column.getDataIndex();
                    break;
                }
            }

            this.loadData(orderColumn);
        }
        else
        {
            EventDispatcher.forwardEvent(ExplorerEvents.DATA_LOADED);
        }
    }

    public boolean loadData()
    {
        return loadData(orderColumn);
    }

    public boolean loadData(String orderColumn)
    {
        if (this.currentView != null)
        {
            retriever.load(currentView, orderColumn);
            return true;
        }
        else
        {
            return false;
        }
    }

    public ServicesPagingStore<Hd3dModelData> getDataStore()
    {
        return retriever.getDataStore();
    }

    public SheetModelData getActiveView()
    {
        return this.currentView;
    }

    public SheetDataRetriever getRetreiver()
    {
        return retriever;
    }

    public boolean save(Boolean silent)
    {
        // on commence par recuperer les nombres de record dirty
        List<Record> records = this.getDataStore().getModifiedRecords();

        saveManager.saveRecords(currentView, records, silent);
        // this.getDataStore().commitChanges();
        return true;
    }

    public LogicConstraint getFilters()
    {
        return retriever.getFilters();
    }

    public void setUserFilter(Constraints constraints)
    {
        LogicConstraint userFilter = new LogicConstraint();

        userFilter.setOperator(EConstraintLogicalOperator.AND);
        if (constraints.size() > 0)
        {
            userFilter.setLeftMember(constraints.get(0));

            if (constraints.size() > 1)
            {
                constraints.remove(constraints.get(0));
                this.recursivelyAddConstraints(userFilter, constraints);
            }
            this.retriever.setUserFilter(userFilter);
        }
        else
        {
            this.retriever.setUserFilter(null);
        }
    }

    public void setUserItemFilter(ItemConstraints itemConstraints)
    {
        this.retriever.setUserItemFilter(itemConstraints);
    }

    private void recursivelyAddConstraints(LogicConstraint filter, Constraints constraints)
    {
        if (constraints.size() == 1)
        {
            filter.setRightMember(constraints.get(0));
            constraints.remove(constraints.get(0));
        }
        else if (constraints.size() > 0)
        {
            LogicConstraint rightMember = new LogicConstraint();
            rightMember.setOperator(EConstraintLogicalOperator.AND);
            rightMember.setLeftMember(constraints.get(0));
            constraints.remove(constraints.get(0));

            filter.setRightMember(rightMember);
            this.recursivelyAddConstraints(rightMember, constraints);
        }
    }

    public void setApplicationFilter(LogicConstraint filter)
    {
        this.retriever.setApplicationFilter(filter);
    }

    public void refreshFilter()
    {
        this.retriever.updateFilters();
    }

    public void setLimit(int limit)
    {
        this.retriever.setLimit(limit);
    }

    public void addParameter(IUrlParameter parameter)
    {
        this.retriever.addParameter(parameter);
    }

    public void removeParameter(IUrlParameter parameter)
    {
        this.retriever.removeParameter(parameter);
    }
}
