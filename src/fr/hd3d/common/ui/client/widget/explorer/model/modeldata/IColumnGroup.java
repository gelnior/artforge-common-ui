package fr.hd3d.common.ui.client.widget.explorer.model.modeldata;

import java.util.List;


public interface IColumnGroup extends IHd3dBaseModel
{

    void setColumns(List<IColumn> columns);

    List<IColumn> getColumns();

    Boolean areColumnsLoaded();

    int getColumnsCount();

    public String getName();

}
