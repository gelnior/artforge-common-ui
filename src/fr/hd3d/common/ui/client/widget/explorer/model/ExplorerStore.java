package fr.hd3d.common.ui.client.widget.explorer.model;

import java.util.List;

import com.extjs.gxt.ui.client.data.BaseModelData;
import com.extjs.gxt.ui.client.data.BasePagingLoader;
import com.extjs.gxt.ui.client.data.ListLoader;
import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.data.PagingLoadResult;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.service.proxy.ServicesPagingProxy;
import fr.hd3d.common.ui.client.service.store.ServicesPagingStore;
import fr.hd3d.common.ui.client.widget.explorer.model.reader.ExplorerPagingJsonReader;


/**
 * Specific store for handling column sorting (specific data proxy).
 * 
 * @author HD3D
 */
public class ExplorerStore extends ServicesPagingStore<Hd3dModelData>
{
    /** Reader used by store to parse data. */
    private final ExplorerPagingJsonReader reader = new ExplorerPagingJsonReader();

    protected BaseModelData aggregationModel = new BaseModelData();

    /**
     * Constructor: set proxy and loader.
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public ExplorerStore()
    {
        super();

        this.proxy = new ExplorerProxy();
        this.pagingLoader = new BasePagingLoader<PagingLoadResult<Hd3dModelData>>(
                (ServicesPagingProxy<Hd3dModelData>) proxy, reader);
        this.loader = (ListLoader) this.pagingLoader;
        this.setLoadListener();
    }

    /**
     * @return Explorer data reader.
     */
    public ExplorerPagingJsonReader getReader()
    {
        return this.reader;
    }

    @Override
    protected void applySort(boolean supressEvent)
    {}

    @Override
    protected void onLoad(LoadEvent le)
    {
        super.onLoad(le);
        // aggregationModel.getProperties().clear();
        // for (int j = 0; j < this.reader.getModeltype().getFieldCount(); j++)
        // {
        // aggregationModel.set(this.reader.getModeltype().getField(j).getName(), this.reader.getAggregatedValues()
        // .get(j));
        // }
        // Logger.log(aggregationModel.getProperties().toString());
    }

    public List<Double> getAggregationValues()
    {
        return this.reader.getAggregatedValues();
    }
}
