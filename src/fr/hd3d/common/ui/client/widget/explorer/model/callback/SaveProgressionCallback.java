package fr.hd3d.common.ui.client.widget.explorer.model.callback;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.store.Record;

import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.widget.explorer.model.ExplorerSaveManager;


public class SaveProgressionCallback extends BaseCallback
{
    private final ExplorerSaveManager saveManager;
    private final Record record;

    public SaveProgressionCallback(ExplorerSaveManager sm, Record record)
    {
        this.saveManager = sm;
        this.record = record;
    }

    @Override
    protected void onSuccess(Request request, Response response)
    {
        this.saveManager.oneMoreSaved();
        this.saveManager.onSaveProgress(record);
    }

}
