package fr.hd3d.common.ui.client.widget.explorer.view.grid.editor;

import java.util.List;

import org.restlet.client.Request;
import org.restlet.client.Response;
import org.restlet.client.data.Method;

import com.extjs.gxt.ui.client.data.ListLoadResult;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.form.ComboBox;
import com.google.gwt.core.client.GWT;
import com.google.gwt.json.client.JSONObject;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.common.ui.client.modeldata.reader.PersonReader;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;


public class PersonEditor extends ComboBox<PersonModelData> implements IComplexModelEditor<PersonModelData>
{
    /**
     * default constructor
     */
    public PersonEditor()
    {
        initCombo();
        loadPerson();
    }

    /**
     * Initialize the combobox (store, display info...)
     */
    private void initCombo()
    {
        store = new ListStore<PersonModelData>();

        setEmptyText("Select a person...");
        setDisplayField(PersonModelData.PERSON_LOGIN_FIELD);
        setSimpleTemplate("{lastName} {firstName} - {login}");
        setWidth(150);
        setStore(this.store);
        setTypeAhead(true);
        setTriggerAction(TriggerAction.ALL);
        setEditable(false);
        setWidth("100%");
    }

    /**
     * start loading open projects
     */
    private void loadPerson()
    {
        store.removeAll();
        BaseCallback callback = new BaseCallback() {
            @Override
            public void onSuccess(Request request, Response response)
            {
                onLoadPerson(request, response);
            }
        };
        RestRequestHandlerSingleton.getInstance().handleRequest(Method.GET, ServicesURI.PERSONS, null, callback);
    }

    /**
     * called when project are loaded
     * 
     * @param request
     * @param response
     */
    private void onLoadPerson(Request request, Response response)
    {
        try
        {
            String json = response.getEntity().getText();
            PersonReader reader = new PersonReader();
            ListLoadResult<PersonModelData> listResult = reader.read(new PersonModelData(), json);
            List<PersonModelData> result = listResult.getData();
            store.add(result);
        }
        catch (Exception e)
        {
            GWT.log("Error occurs while parsing person data for person editor.", e);
        }
    }

    public PersonModelData getValueFromJSON(JSONObject json)
    {
        PersonReader reader = new PersonReader();
        return reader.readObject(json, PersonModelData.getModelType());
    }
}
