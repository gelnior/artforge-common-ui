package fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer;

import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.MenuEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.extjs.gxt.ui.client.widget.menu.Menu;
import com.extjs.gxt.ui.client.widget.menu.MenuItem;

import fr.hd3d.common.ui.client.error.Hd3dException;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.common.ui.client.widget.proxyviewer.ProxyDisplayer;
import fr.hd3d.common.ui.client.widget.thumbnaileditor.ThumbnailPath;


/**
 * Thumbnail renderer displays thumbnail picture depending on represented model ID and represented model class. It
 * retrieves image directly from images/thumbnails/{class]/{id} service.
 * 
 * @author HD3D
 * 
 * @param <M>
 */
public class ThumbnailRenderer<M extends Hd3dModelData> implements GridCellRenderer<M>,
        IExplorateurColumnContextMenu<M>
{
    public Object render(M model, String property, ColumnData config, int rowIndex, int colIndex, ListStore<M> store,
            Grid<M> grid)
    {
        String path = ThumbnailPath.getPath(model);

        return getImageHtmlCode(path);
    }

    protected Object getImageHtmlCode(String path)
    {
        return "<img height=\"40\" src=\"" + path + "\" />";
    }

    protected Menu createContextMenu(final M model)
    {
        Menu menu = new Menu();
        menu.setAutoWidth(true);

        MenuItem menuItem = new MenuItem("Edit thumbnail");
        menuItem.addSelectionListener(new SelectionListener<MenuEvent>() {
            @Override
            public void componentSelected(MenuEvent ce)
            {
                EventDispatcher.forwardEvent(ExplorerEvents.THUMBNAIL_EDIT_CLICKED);
            }
        });
        menu.add(menuItem);

        menuItem = new MenuItem("Show Preview");
        menuItem.addSelectionListener(new SelectionListener<MenuEvent>() {
            @Override
            public void componentSelected(MenuEvent ce)
            {
                EventDispatcher.forwardEvent(ExplorerEvents.SHOW_PREVIEW_CLICKED);
            }
        });

        menu.add(menuItem);

        if (MainModel.getAssetManager())
        {
            menuItem = new MenuItem("Show Proxy");
            menuItem.addSelectionListener(new SelectionListener<MenuEvent>() {
                @Override
                public void componentSelected(MenuEvent ce)
                {
                    ProjectModelData project = MainModel.getCurrentProject();
                    if (project == null)
                    {
                        return;
                    }
                    try
                    {
                        ProxyDisplayer.showSimple(project.getId(), model.getId(), model.getSimpleClassName());
                    }
                    catch (Hd3dException e)
                    {
                        e.print();
                    }
                }
            });

            menu.add(menuItem);
        }
        return menu;
    }

    public void handleGridEvent(GridEvent<M> ge)
    {
        if (ge.getRowIndex() < 0)
        {
            return;
        }
        if (ge.getType() == Events.ContextMenu)
        {
            M model = ge.getModel();
            ge.getGrid().setContextMenu(this.createContextMenu(model));
        }
    }
}
