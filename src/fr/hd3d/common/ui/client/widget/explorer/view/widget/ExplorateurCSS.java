package fr.hd3d.common.ui.client.widget.explorer.view.widget;

public class ExplorateurCSS
{
    public static final String NEW_VIEW_ICON_CLASS = "newViewIcon";
    public static final String EDIT_VIEW_ICON_CLASS = "editViewIcon";
    public static final String REFRESH_ICON_CLASS = "refreshIcon";
    public static final String CLEAR_ICON_CLASS = "clearIcon";

    public static final String SAVE_CLASS = "saveIcon";
    public static final String AUTO_SAVE_CLASS = "autoSaveIcon";
    public static final String DELETE_ROW_ICON_CLASS = "explorer-delete-row-icon";
    public static final String ADD_ROW_ICON_CLASS = "explorer-add-row-icon";
    public static final String DELETE_VIEW_ICON_CLASS = "explorer-delete-view-icon";

    public static final String ADD_RELATION_ICON_CLASS = "explorer-add-relation-icon";;

    public static final String ADD_FOLDER = "addFolderIcon";
    public static final String EDIT_FOLDER = "editFolderIcon";
    public static final String DELETE_ITEM = "classicDelete";

    public static final String ADD_ATTRIBUTE = "addAttributeIcon";
    public static final String ADD_APPROVAL_TYPE = "add-approvalNoteType-icon";

    public static final String FILTER_DELETE = "filter-delete";
    public static final String FILTER_ADD = "filter-add";

    public static final String PRINTER = "printer";

    public static final String CONSTRAINT_EDITOR = "constraint-icon";
    public static final String NOTE_EDIT = "note-edit-icon";
    public static final String NOTE_DELETE = "note-delete-icon";

}
