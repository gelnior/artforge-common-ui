package fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar;

import com.extjs.gxt.ui.client.widget.button.Button;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.listener.DeleteClickListener;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.ExplorateurCSS;


/**
 * Tool Item that raises a DELETE_ROW event when it is clicked.
 * 
 * @author HD3D
 */
public class DeleteRowToolItem extends Button
{
    /** Constant strings to display : dialog messages, button label... */
    public static final CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    /**
     * Default Constructor.
     */
    public DeleteRowToolItem()
    {
        super();

        this.setStyle();
        this.registerListener();
    }

    /**
     * Register on tool item click event. When the button is clicked, it raise the DELETE_ROW event.
     */
    private void registerListener()
    {
        this.addSelectionListener(new DeleteClickListener());
    }

    /**
     * Set image style.
     */
    private void setStyle()
    {
        setIconStyle(ExplorateurCSS.DELETE_ROW_ICON_CLASS);

        this.setToolTip(CONSTANTS.DeleteRow());
    }
}
