package fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GroupColumnData;


public class HoursRenderer<M extends ModelData> implements IExplorerCellRenderer<M>
{

    public String render(GroupColumnData data)
    {
        return this.getValueFromObject(data.gvalue);
    }

    private String getValueFromObject(Object obj)
    {
        String stringValue = "";
        if (obj != null)
        {
            if (obj instanceof Double)
            {
                Float value = new Float((Double) obj);

                stringValue = this.getStringFromFloat(value);
            }
            else if (obj instanceof Long)
            {
                Float value = new Float((Long) obj);

                stringValue = this.getStringFromFloat(value);
            }
        }
        return stringValue;
    }

    private String getStringFromFloat(Float value)
    {
        float nbHours = value / 3600;
        int hours = (int) Math.floor(nbHours);
        value = value % 3600;

        String hourRender = "";
        if (hours > 0 && value == 0)
        {
            hourRender = hours + "h ";
        }
        if (value > 0)
        {
            hourRender = hours + "h " + (Math.round(value / 60));
        }

        hourRender = "<div style= 'text-align: center; font-size: 16px; font-weight: bold;'>" + hourRender + "</div>";

        return hourRender;
    }

    public Object render(M model, String property, ColumnData config, int rowIndex, int colIndex, ListStore<M> store,
            Grid<M> grid)
    {
        Object obj = model.get(property);
        config.css = "basic-editable";
        if (obj != null)
            config.css += " duration-column";
        else
            config.css += " empty-duration-column";
        return this.getValueFromObject(obj);
    }

}
