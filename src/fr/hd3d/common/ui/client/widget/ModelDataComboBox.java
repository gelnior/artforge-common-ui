package fr.hd3d.common.ui.client.widget;

import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.data.BaseListLoadConfig;
import com.extjs.gxt.ui.client.data.BasePagingLoadConfig;
import com.extjs.gxt.ui.client.data.ListLoadConfig;
import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.data.PagingLoadConfig;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.LoadListener;
import com.extjs.gxt.ui.client.widget.form.ComboBox;

import fr.hd3d.common.ui.client.listener.EventSelectionChangedListener;
import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.service.parameter.OrderBy;
import fr.hd3d.common.ui.client.service.store.ServiceStore;


/**
 * Model data combo box propose to select model data of the same type. The selection is made upon the name of the model
 * data.
 * 
 * @author HD3D
 * 
 * @param <C>
 *            Type of the data displayed in the combo box.
 */
public class ModelDataComboBox<C extends RecordModelData> extends ComboBox<C>
{
    /** Reader needed for data parsing. */
    protected IReader<C> reader;

    /** ? */
    static protected final EventType STORE_LOADED = new EventType();

    /** Store used by combo box to display data. */
    protected ServiceStore<C> store;

    /** Loading configuration (needed for sorting) */
    protected final ListLoadConfig config = new BaseListLoadConfig();

    /** Order by parameter applied to service store to remotely sort data. */
    protected OrderBy orderBy = new OrderBy(RecordModelData.NAME_FIELD);

    /**
     * Default constructor. Sets store, display field and value field.
     */
    public ModelDataComboBox(IReader<C> reader)
    {
        super();

        this.reader = reader;

        this.setStore(new ServiceStore<C>(reader));
        this.setStyles();
    }

    /**
     * Alternative constructor. Sets store with the one given in parameter, display field and value field.
     * 
     * @param store
     *            The store to set.
     */
    public ModelDataComboBox(ServiceStore<C> store)
    {
        super();

        this.reader = store.getReader();

        this.setStore(store);
        this.setStyles();
    }

    /**
     * @return Store as a service store to avoid unnecessary casts.
     */
    public ServiceStore<C> getServiceStore()
    {
        return this.store;
    }

    /**
     * Set store stuff : reader, proxy and store options (sorting and display field).
     */
    public void setStore(ServiceStore<C> store)
    {
        this.store = store;
        this.store.addParameter(orderBy);

        this.store.setSortDir(SortDir.ASC);
        this.store.setSortField(RecordModelData.NAME_FIELD);
        this.store.addLoadListener(new LoadListener() {

            @Override
            public void loaderBeforeLoad(LoadEvent le)
            {
                onBeforeLoad(le);
            }

            @Override
            public void loaderLoad(LoadEvent le)
            {
                onLoaderLoad();
            }

            @Override
            public void loaderLoadException(LoadEvent le)
            {
                onLoaderLoadException(le);
            }
        });

        super.setStore(store);
    }

    /**
     * Set combo box behavior.
     */
    protected void setStyles()
    {
        this.setForceSelection(true);
        this.setTriggerAction(TriggerAction.ALL);
        this.setEditable(false);
        this.setValidateOnBlur(false);

        this.setDisplayField(RecordModelData.NAME_FIELD);
        // this.setValueField(RecordModelData.NAME_FIELD);
        this.setTypeAhead(true);
        this.setMinChars(3);
    }

    protected void onBeforeLoad(LoadEvent le)
    {}

    protected void onLoaderLoad()
    {
        this.fireEvent(STORE_LOADED);
    }

    /**
     * When exception occurs while loading, it is written to development logs.
     * 
     * @param le
     *            The raise exception.
     */
    protected void onLoaderLoadException(LoadEvent le)
    {
        Logger.log("ModelDataCombobox failure", le.exception);
    }

    /**
     * Load data in the combo box.
     */
    protected void setData()
    {
        config.setSortDir(SortDir.ASC);
        config.setSortField(RecordModelData.NAME_FIELD);

        this.store.reload(config);
    }

    /**
     * @param name
     *            The name of the value to retrieve.
     * @return The combo box value whose name is equal to <i>name</i>
     */
    public C getValue(String name)
    {
        return this.getStore().findModel(RecordModelData.NAME_FIELD, name);
    }

    /**
     * @param id
     *            The id of the value to retrieve.
     * @return The combo box value of which id is equal to <i>id</i>
     */
    public C getValueById(Long id)
    {
        return this.getStore().findModel(RecordModelData.ID_FIELD, id);
    }

    /**
     * @param name
     *            The name of the value to retrieve.
     * @return The combo box value of which name is equal to <i>name</i>.
     */
    public C getValueByName(String name)
    {
        return this.getStore().findModel(RecordModelData.NAME_FIELD, name);
    }

    /**
     * Set path for current store from where data must be retrieved.
     * 
     * @param path
     *            The path to set.
     */
    public void setPath(String path)
    {
        this.store.setPath(path);
    }

    /**
     * 
     * @param id
     */
    public void setValueById(final Long id)
    {
        final LoadListener listener = new LoadListener() {
            @Override
            public void loaderLoad(LoadEvent le)
            {
                onIdLoad(this, id);
            }
        };

        this.getStore().getLoader().addLoadListener(listener);
        this.store.reload();
    }

    /**
     * When an object is loaded with its ID, ID loading load listener is removed from store and retrived model is set as
     * selected value.
     * 
     * @param listener
     *            Load listener to remove from
     * @param id
     *            The id of the model to set as selected value.
     * 
     */
    protected void onIdLoad(LoadListener listener, Long id)
    {
        getStore().getLoader().removeLoadListener(listener);

        setValue(getValueById(id));
    }

    /**
     * Set column on which sorting must occur.
     * 
     * @param column
     *            The column on which sorting must occur.
     */
    public void setOrderColumn(String column)
    {
        this.config.setSortField(column);
        this.store.setSortField(column);

        this.orderBy.clearColumns();
        this.orderBy.addColumn(column);
    }

    /**
     * Select first element of the combo box store.
     */
    public void selectFirst()
    {
        if (this.getStore().getCount() > 0)
        {
            this.setValue(this.getStore().getAt(0));
        }
    }

    /**
     * @return Loader configuration.
     */
    @Override
    protected PagingLoadConfig getParams(String query)
    {
        BasePagingLoadConfig config = new BasePagingLoadConfig();
        config.setLimit(this.getPageSize());
        config.setOffset(0);

        return config;
    }

    /** Set contextual menu that allows to refresh combo box store content. */
    public EasyMenu setContextMenu()
    {
        EasyMenu menu = new EasyMenu();
        RefreshMenuItem<C> item = new RefreshMenuItem<C>(store);
        menu.add(item);

        this.setContextMenu(menu);
        return menu;
    }

    /**
     * Set a selection changed listener that raises an event containing selected model data.
     * 
     * @param eventType
     *            The event type to raise when selection changes.
     */
    public void setSelectionChangedEvent(EventType eventType)
    {
        this.addSelectionChangedListener(new EventSelectionChangedListener<C>(eventType));
    }
}
