package fr.hd3d.common.ui.client.widget.treegrid;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.store.TreeStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.treegrid.TreeGrid;


/**
 * TreeGrid class for hd3d customing.
 * 
 * @author HD3D
 * 
 * @param <M>
 */
public class BaseTreeGrid<M extends ModelData> extends TreeGrid<M>
{

    public BaseTreeGrid(TreeStore<?> store, ColumnModel cm)
    {
        super(store, cm);
    }

    /**
     * Hide grid headers.
     */
    public void hideHeaders()
    {
        if (this.getView() instanceof BaseTreeGridView)
        {
            ((BaseTreeGridView) this.getView()).hideHeaders();
        }
    }

    /**
     * Show grid headers.
     */
    public void showHeaders()
    {
        if (this.getView() instanceof BaseTreeGridView)
        {
            ((BaseTreeGridView) this.getView()).showHeaders();
        }
    }
}
