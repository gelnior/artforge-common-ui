package fr.hd3d.common.ui.client.widget.treegrid;

import com.extjs.gxt.ui.client.widget.treegrid.TreeGridView;


public class BaseTreeGridView extends TreeGridView
{
    public void hideHeaders()
    {
        this.mainHd.setVisible(false);
        this.header.setVisible(false);
    }

    public void showHeaders()
    {
        this.mainHd.setVisible(true);
        this.header.setVisible(true);
    }
}
