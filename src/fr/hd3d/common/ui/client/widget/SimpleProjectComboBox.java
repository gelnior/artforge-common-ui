package fr.hd3d.common.ui.client.widget;

import com.google.gwt.core.client.GWT;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.client.enums.EProjectStatus;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.modeldata.reader.ProjectReader;
import fr.hd3d.common.ui.client.service.parameter.Constraint;


/**
 * Combo box allowing to select project from the open project list.
 * 
 * @author HD3D
 */
public class SimpleProjectComboBox extends ModelDataComboBox<ProjectModelData>
{
    /** Constant strings to display : dialog messages, button label... */
    public static final CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    /**
     * Default constructor
     */
    public SimpleProjectComboBox()
    {
        this(new ProjectReader());

        this.setId("project-combo");
        this.setContextMenu();
    }

    /**
     * Constructor with reader.
     */
    public SimpleProjectComboBox(IReader<ProjectModelData> reader)
    {
        super(reader);

        this.setStyles();
        this.setListener();

        Constraint constraint = new Constraint(EConstraintOperator.eq, ProjectModelData.PROJECT_STATUS,
                EProjectStatus.OPEN.toString(), null);
        this.store.addParameter(constraint);

        // this.setData();
    }

    /**
     * Force combo box to set the project whose id is equal to <id>.
     * 
     * @param id
     *            The project to select id.
     */
    public void selectProject(Long id)
    {
        if (id != null && getValueById(id) != null)
        {
            this.setValue(getValueById(id));
        }
    }

    /**
     * Before retrieving data, it adds a constraint on the status to retrieve only opened project.
     */
    @Override
    public void setData()
    {
        this.store.reload();
    }

    /**
     * Set combo styles.
     */
    @Override
    protected void setStyles()
    {
        super.setStyles();
        this.setEmptyText(CONSTANTS.SelectProject());
        this.setEditable(false);
    }

    /**
     * Method to override to set listeners on project combo box.
     */
    protected void setListener()
    {}
}
