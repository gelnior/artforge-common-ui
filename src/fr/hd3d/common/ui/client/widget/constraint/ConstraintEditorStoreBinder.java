package fr.hd3d.common.ui.client.widget.constraint;

import java.util.List;

import com.extjs.gxt.ui.client.binder.StoreBinder;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.store.StoreEvent;
import com.extjs.gxt.ui.client.widget.Component;
import com.extjs.gxt.ui.client.widget.LayoutContainer;

import fr.hd3d.common.ui.client.widget.constraint.model.ConstraintModelData;


public class ConstraintEditorStoreBinder extends
        StoreBinder<ListStore<ConstraintModelData>, LayoutContainer, ConstraintModelData>
{
    protected final ConstraintPanel editor;

    public ConstraintEditorStoreBinder(ListStore<ConstraintModelData> store, ConstraintPanel editor)
    {
        super(store, editor);
        this.editor = editor;
    }

    @Override
    protected void createAll()
    {}

    @Override
    public Component findItem(ConstraintModelData model)
    {
        return null;
    }

    @Override
    protected List<ConstraintModelData> getSelectionFromComponent()
    {
        return null;
    }

    @Override
    protected void onAdd(StoreEvent<ConstraintModelData> se)
    {
        this.editor.addRow(se.getModels().get(0));
    }

    @Override
    protected void onRemove(StoreEvent<ConstraintModelData> se)
    {
        this.editor.removeRowFromView(se.getModel());
    }

    @Override
    protected void onUpdate(StoreEvent<ConstraintModelData> se)
    {

    }

    @Override
    protected void removeAll()
    {
        this.editor.removeAllRows();
    }

    @Override
    protected void setSelectionFromProvider(List<ConstraintModelData> selection)
    {

    }

    @Override
    protected void update(ConstraintModelData model)
    {

    }

};
