package fr.hd3d.common.ui.client.widget.constraint;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.util.Util;

import fr.hd3d.common.client.Const;
import fr.hd3d.common.client.Sheet;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetFilterModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.service.ExcludedField;
import fr.hd3d.common.ui.client.service.ServicesClass;
import fr.hd3d.common.ui.client.service.ServicesField;
import fr.hd3d.common.ui.client.service.ServicesModelType;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.Constraints;
import fr.hd3d.common.ui.client.service.parameter.ItemConstraint;
import fr.hd3d.common.ui.client.service.parameter.ItemConstraints;
import fr.hd3d.common.ui.client.setting.UserSettings;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.util.IdGenerator;
import fr.hd3d.common.ui.client.widget.constraint.event.ConstraintEvents;
import fr.hd3d.common.ui.client.widget.constraint.model.ConstraintModelData;
import fr.hd3d.common.ui.client.widget.constraint.model.ConstraintPanelModel;
import fr.hd3d.common.ui.client.widget.explorer.config.ExplorerConfig;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;
import fr.hd3d.common.ui.client.widget.explorer.model.modeldata.IColumn;


/**
 * Controller that handles constraint editor event.
 * 
 * @author HD3D
 */
public class ConstraintPanelController extends MaskableController
{
    /** Model that handles constraint editor data. */
    final protected ConstraintPanelModel model;
    /** Constraint editor panel. */
    final protected IConstraintPanel view;

    /** list containing all the items on which a "constraint"(used in server side query construction) can be applied */
    final protected Set<Long> hibConstrainables = new HashSet<Long>();

    /**
     * Default constructor.
     * 
     * @param model
     *            Model that handles constraint editor data.
     * @param view
     *            Constraint editor panel.
     */
    public ConstraintPanelController(ConstraintPanelModel model, IConstraintPanel view)
    {
        this.model = model;
        this.view = view;

        this.registerEvents();
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType eventType = event.getType();

        if (eventType == ConstraintEvents.ADD_FILTER_CLICKED)
        {
            this.model.getEditorStore().add(new ConstraintModelData(hibConstrainables));
        }
        else if (eventType == ConstraintEvents.REMOVE_FILTER_CLICKED)
        {
            this.model.getEditorStore().remove((ConstraintModelData) event.getData());
        }
        else if (eventType == ConstraintEvents.LAST_FILTER_CLICKED)
        {
            this.onViewinformationsLoaded(event);
        }
        else if (eventType == ExplorerEvents.SHOW_CONSTRAINT_PANEL)
        {
            this.onShow();
        }
        else if (eventType == ExplorerEvents.HIDE_CONSTRAINT_PANEL)
        {
            this.view.hide();
        }
        else if (eventType == ExplorerEvents.SHEET_INFORMATIONS_LOADED)
        {
            this.onViewinformationsLoaded(event);
        }
        else if (eventType == ConstraintEvents.REMOVE_ALL_FILTER_CLICKED)
        {
            this.model.getEditorStore().removeAll();
        }
        else if (eventType == ConstraintEvents.APPLY_FILTER_CLICKED)
        {
            this.onApplyFilterClicked();
        }
        else if (eventType == ConstraintEvents.FILTER_SAVE_CLICKED)
        {
            this.onSaveFilterClicked(event);
        }
        else if (eventType == ConstraintEvents.FILTER_SELECTED)
        {
            this.onFilterSelected(event);
        }
        this.forwardToChild(event);
    }

    /**
     * When constraint panel is shown, it checks if it is first time that panel is shown. If it is, the constraint rows
     * are rebuilt from user settings. This is needed because of a GXT issue : it doesn't allow to create constraint
     * rows before rendering.
     */
    private void onShow()
    {
        this.view.show();
        if (this.model.isFirstShown())
        {
            this.onViewinformationsLoaded(null);
        }
    }

    /**
     * @return Specific key needed by settings to identify filter. The key is unique for each sheet.
     */
    private String getConstraintSettingKey()
    {
        return ConstraintPanel.CONSTRAINT_SETTING_PREFIX + this.view.getCurrentSheetId();
    }

    /**
     * When view changes, the filter is reset with constraint set in user settings. If no constraint is set, all
     * constraints are removed.
     */
    private void onViewinformationsLoaded(AppEvent event)
    {
        if (model == null)
            return;

        this.model.setIsFirstShown(this.view.isHidden());
        this.model.getEditorStore().removeAll();
        this.model.setAvailableColumns(getAvailableColumns(this.view.getCurrentSheet()));

        String setting = UserSettings.getSetting(getConstraintSettingKey());
        this.parseConstraint(setting);

        /* store Constraints and ItemConstraints in the global map */
        AppEvent eventToForward = new AppEvent(ExplorerEvents.NEW_USER_FILTER_SET);
        eventToForward.setData(ConstraintPanelModel.CONSTRAINTS, new Constraints(this.getConstraints()));
        eventToForward.setData(ConstraintPanelModel.ITEMCONSTRAINTS, new ItemConstraints(this.getItemConstraints()));
        eventToForward.setData(ExplorerConfig.CONSTRAINT_STRING_EVENT_VAR_NAME, setting);
        EventDispatcher.forwardEvent(eventToForward);
    }

    /**
     * When filter is selected, it is rebuilt from scratch, then it is applied to current grid.
     * 
     * @param event
     *            Filter selected event.
     */
    private void onFilterSelected(AppEvent event)
    {
        SheetFilterModelData filter = event.getData();
        if (filter != null)
        {
            this.model.setIsFirstShown(this.view.isHidden());
            this.model.getEditorStore().removeAll();

            this.parseConstraint(filter.getFilter());
            String setting = this.saveConstraintToSettings();

            /* store Constraints and ItemConstraints in the global map */
            AppEvent eventToForward = new AppEvent(ExplorerEvents.NEW_USER_FILTER_SET);
            eventToForward.setData(ConstraintPanelModel.CONSTRAINTS, new Constraints(this.getConstraints()));
            eventToForward
                    .setData(ConstraintPanelModel.ITEMCONSTRAINTS, new ItemConstraints(this.getItemConstraints()));
            eventToForward.setData(ExplorerConfig.IS_SHEET_FILTER_EVENT_VAR_NAME, Boolean.TRUE);
            eventToForward.setData(ExplorerConfig.CONSTRAINT_STRING_EVENT_VAR_NAME, setting);

            EventDispatcher.forwardEvent(eventToForward);
            EventDispatcher.forwardEvent(ExplorerEvents.RELOAD_DATA);
        }
    }

    /**
     * When apply filter is clicked a new LogicConstraint is created and send to the explorer via a NEW CONSTRAINT SET
     * event. Constraint is saved to user setting for current sheet.
     */
    private void onApplyFilterClicked()
    {
        String setting = this.saveConstraintToSettings();

        Constraints constraints = new Constraints(this.getConstraints());
        ItemConstraints itemConstraints = new ItemConstraints(this.getItemConstraints());

        AppEvent event = new AppEvent(ExplorerEvents.NEW_USER_FILTER_SET);
        event.setData(ConstraintPanelModel.CONSTRAINTS, constraints);
        event.setData(ConstraintPanelModel.ITEMCONSTRAINTS, itemConstraints);
        event.setData(ExplorerConfig.CONSTRAINT_STRING_EVENT_VAR_NAME, setting);

        EventDispatcher.forwardEvent(event);
        EventDispatcher.forwardEvent(ExplorerEvents.RELOAD_DATA);
    }

    /**
     * When save filter button is clicked, the event is forwarded to sheet filter combo box controller and current
     * constraint and current sheet are added to its data.
     */
    private void onSaveFilterClicked(AppEvent event)
    {
        String constraint = getConstraintsAsString();
        event.setData(constraint);
        event.setData(ExplorerConfig.SHEET_EVENT_VAR_NAME, this.view.getCurrentSheet());
    }

    /**
     * Parse constraint and create a constraint row for each constraint found.
     * 
     * @param setting
     *            The constraint setting string to parse.
     */
    private void parseConstraint(String setting)
    {
        if (Util.isEmptyString(setting))
            return;

        String[] constraintsString = setting.split("\\" + ConstraintPanel.CONSTRAINT_SETTING_SEPARATOR);
        for (String constraintString : constraintsString)
        {
            if (Util.isEmptyString(constraintString))
                continue;

            String[] constraint = constraintString.split("\\" + ConstraintPanel.CONSTRAINT_SETTING_DATA_SEPARATOR);
            this.createConstraint(constraint);
        }
    }

    /**
     * Save constraint set by user on current sheet to settings at string format :
     * column01|operator01|value01+column02|operator02|value02+... stringValue
     */
    private String saveConstraintToSettings()
    {
        String constraintString = getConstraintsAsString();
        UserSettings.setSetting(getConstraintSettingKey(), constraintString);

        return constraintString;
    }

    /**
     * Return constraint set by user on current sheet at string format :
     * column01|operator01|value01+column02|operator02|value02+... stringValue
     */
    private String getConstraintsAsString()
    {
        StringBuffer constraintSetting = new StringBuffer();

        for (ConstraintModelData constraintModel : this.model.getEditorStore().getModels())
        {
            constraintSetting.append(constraintModel.getActive().toString());
            constraintSetting.append(ConstraintPanel.CONSTRAINT_SETTING_DATA_SEPARATOR);
            if (constraintModel.getColumnId() != null)
            {
                constraintSetting.append(constraintModel.getColumnId().toString());
            }
            else
            {
                constraintSetting.append("0");
            }
            constraintSetting.append(ConstraintPanel.CONSTRAINT_SETTING_DATA_SEPARATOR);
            if (constraintModel.getColumn() != null)
                constraintSetting.append(constraintModel.getColumn().getName());
            constraintSetting.append(ConstraintPanel.CONSTRAINT_SETTING_DATA_SEPARATOR);
            if (constraintModel.getOperator() != null)
                constraintSetting.append(constraintModel.getOperator().getName());
            constraintSetting.append(ConstraintPanel.CONSTRAINT_SETTING_DATA_SEPARATOR);
            if (constraintModel.getValue() != null || constraintModel.getStringValue() != null)
            {
                final Constraint c = constraintModel.getConstraint();
                if (c != null && c.getLeftMember() != null)
                    constraintSetting.append(c.getLeftMember().toString());
                else
                    constraintSetting.append(constraintModel.getValue());
            }
            constraintSetting.append(ConstraintPanel.CONSTRAINT_SETTING_SEPARATOR);
        }
        if (constraintSetting.length() > 0)
        {
            constraintSetting.deleteCharAt(constraintSetting.length() - 1);
        }
        return constraintSetting.toString();
    }

    /**
     * @return Constraint model data list as a Constraint (IUrlParameter) list.
     */
    public Collection<? extends Constraint> getConstraints()
    {
        List<Constraint> constraints = new ArrayList<Constraint>();

        for (ConstraintModelData model : this.model.getEditorStore().getModels())
        {
            boolean constrainable = hibConstrainables.contains(model.getColumnId());

            /* This item filtering cannot be forwarded to hibernate, pointless to build a Constraint. */
            if (!constrainable)
                continue;

            /* Add constraint. */
            Constraint constraint = model.getConstraint();
            if (model.isValid() && constraint != null)
            {
                constraints.add(constraint);
            }
        }

        return constraints;
    }

    /**
     * @return ItemConstraint list as a ItemConstraint (IUrlParameter) list.
     */
    public Collection<? extends ItemConstraint> getItemConstraints()
    {
        List<ItemConstraint> itemConstraints = new ArrayList<ItemConstraint>();
        for (ConstraintModelData model : this.model.getEditorStore().getModels())
        {
            boolean constrainable = hibConstrainables.contains(model.getColumnId());

            /* this item filtering may be forwarded to Hibernate, pointless to build an ItemConstraint */
            if (constrainable)
                continue;

            /* add item constraint */
            ItemConstraint constraint = model.getItemConstraint(this.view.getConstraintProvider());
            if (constraint != null)
            {
                itemConstraints.add(constraint);
            }
        }

        return itemConstraints;
    }

    /**
     * Create constraint object and constraint row from a string table containing column name, operator name and the
     * value associated at String format.
     */
    public void createConstraint(String[] constraint)
    {
        ConstraintModelData row = new ConstraintModelData(hibConstrainables);

        row.setActive(Boolean.parseBoolean(constraint[0]));
        row.setColumnId(Long.parseLong(constraint[1]));
        row.setColumnName(constraint[2]);
        row.setOperatorName(constraint[3]);
        if (constraint.length > 4) // Is Null operator has no value.
            row.setStringValue(constraint[4]);

        this.model.getEditorStore().add(row);
    }

    /**
     * @param sheet
     *            The sheet on which available columns are asked.
     * @return Sheet columns on which filter can be set.
     */
    public List<ItemModelData> getAvailableColumns(final SheetModelData sheet)
    {
        /* empty sheet */
        if (sheet == null)
            return new ArrayList<ItemModelData>();

        /* sheet processing */
        /* Get valid sheet columns. */
        final List<ItemModelData> availableColumns = new ArrayList<ItemModelData>();

        if (sheet.getColumns() != null)
        {
            for (IColumn column : sheet.getColumns())
            {
                final ItemModelData col = (ItemModelData) column;

                boolean isConstrainable = this.isValidColumn(col);
                if (isConstrainable)
                {
                    if (isHibernateConstrainable(col))
                        hibConstrainables.add(col.getId());

                    availableColumns.add(col);
                }
            }
        }

        // Get many-to-many columns.
        final ModelType modelType = ServicesModelType.getModelType(sheet.getBoundClassName());

        if (modelType != null)
        {
            for (int i = 0; i < modelType.getFieldCount(); i++)
            {
                final DataField field = modelType.getField(i);

                /* invalid field */
                if (field == null || field.getName() == null || field.getType() == null)
                    continue;

                /* need to create field ? */
                final String lowerCaseName = field.getName().toLowerCase();
                boolean isEntryNeeded = field.getType() == List.class;
                isEntryNeeded &= lowerCaseName.endsWith("ids");
                isEntryNeeded &= !ExcludedField.isExcluded(field.getName());

                if (!isEntryNeeded)
                    continue;

                /* create field */
                final ItemModelData item = new ItemModelData();
                item.setId(IdGenerator.getNewId());
                item.setName(ServicesField.getHumanName(field.getName()));
                item.setQuery(ServicesField.getManyToManyQueryName(field.getName()));
                item.setItemType(Sheet.ITEMTYPE_ATTRIBUTE);
                item.setType(field.getFormat());
                item.setEditor("");

                hibConstrainables.add(item.getId());
                availableColumns.add(item);
            }
        }
        else
        {
            Logger.error("No model type is defined for current sheet entity.");
        }
        // Get Tag column if current bound class name is constituent or shot class.
        // if (ConstituentModelData.SIMPLE_CLASS_NAME.equals(sheet.getBoundClassName())
        // || ShotModelData.SIMPLE_CLASS_NAME.equals(sheet.getBoundClassName()))
        // {
        // ItemModelData item = new ItemModelData();
        // item.setParameter(Hd3dModelData.ID_FIELD);
        // item.setName(TagModelData.SIMPLE_CLASS_NAME);
        // item.setQuery(Hd3dModelData.ID_FIELD);
        // item.setType(TagModelData.SIMPLE_CLASS_NAME);
        // item.setItemType(Sheet.ITEMTYPE_ATTRIBUTE);
        // item.setEditor("");
        //
        // availableColumns.add(item);
        // }

        return availableColumns;
    }

    private boolean isHibernateConstrainable(ItemModelData col)
    {
        if (ApprovalNoteModelData.SIMPLE_CLASS_NAME.equals(col.getType()))
            return false;

        return true;
    }

    /**
     * @param column
     *            The column which is checked.
     * @return true if the column is valid for filtering.
     */
    public boolean isValidColumn(ItemModelData column)
    {
        if (column == null)
            return false;

        final String type = column.getType();
        final String query = column.getQuery();
        final String itemType = column.getItemType();

        if (type == null)
            return false;

        if (this.model.isExcludedConstraint(query))
            return false;

        if ("approvalNotes".equals(query))
            return true;

        /* Is java object or enumeration but is not a java set. */
        boolean validColumn = (type.startsWith(FieldUtils.JAVA_PREFIX) || FieldUtils.isEnum(type))
                && !type.equals(Const.JAVA_UTIL_SET);

        /* OR is an entity */
        validColumn |= ServicesClass.contains(type);

        /* AND item type is "attribute" OR (item type is "metaData" AND type is NOT Date) */
        validColumn &= ApprovalNoteModelData.SIMPLE_CLASS_NAME.equals(type)
                || Sheet.ITEMTYPE_ATTRIBUTE.equals(itemType)
                || (Sheet.ITEMTYPE_COLLECTIONQUERY.equals(itemType) && !type.equals(Const.JAVA_LANG_LONG)
                        && !type.equals(Const.JAVA_LANG_STRING) && !type.startsWith(Const.JAVA_UTIL_LIST))
                || (Sheet.ITEMTYPE_METADATA.equals(itemType) && !type.equals(Const.JAVA_UTIL_DATE));

        /* AND is not a Sequence, nor a Category */
        validColumn &= !type.equals(SequenceModelData.SIMPLE_CLASS_NAME)
                && !type.equals(CategoryModelData.SIMPLE_CLASS_NAME);

        return validColumn;
    }

    public boolean isNotValidColumn(ItemModelData column)
    {
        return !isValidColumn(column);
    }

    /**
     * Register event supported by the controller.
     */
    protected void registerEvents()
    {
        this.registerEventTypes(ExplorerEvents.SHOW_CONSTRAINT_PANEL);
        this.registerEventTypes(ExplorerEvents.HIDE_CONSTRAINT_PANEL);
        this.registerEventTypes(ConstraintEvents.ADD_FILTER_CLICKED);
        this.registerEventTypes(ConstraintEvents.REMOVE_FILTER_CLICKED);
        this.registerEventTypes(ConstraintEvents.APPLY_FILTER_CLICKED);
        this.registerEventTypes(ConstraintEvents.REMOVE_ALL_FILTER_CLICKED);
        this.registerEventTypes(ConstraintEvents.LAST_FILTER_CLICKED);
        this.registerEventTypes(ExplorerEvents.SHEET_INFORMATIONS_LOADED);

        this.registerEventTypes(ConstraintEvents.FILTER_SAVE_CLICKED);
        this.registerEventTypes(ConstraintEvents.FILTER_SAVED);
        this.registerEventTypes(ConstraintEvents.FILTER_SAVED_SUCCESS);
        this.registerEventTypes(ConstraintEvents.FILTER_SELECTED);
        this.registerEventTypes(ConstraintEvents.FILTER_RENAME_CLICKED);
        this.registerEventTypes(ConstraintEvents.FILTER_RENAMED);
        this.registerEventTypes(ConstraintEvents.FILTER_DELETE_CLICKED);
    }

}
