package fr.hd3d.common.ui.client.widget.constraint.event;

import com.extjs.gxt.ui.client.event.EventType;


public class ConstraintEvents
{
    public static final EventType REMOVE_FILTER_GROUP = new EventType();
    public static final EventType ADD_FILTER_CLICKED = new EventType();
    public static final EventType APPLY_FILTER_CLICKED = new EventType();
    public static final EventType REMOVE_FILTER_CLICKED = new EventType();
    public static final EventType LAST_FILTER_CLICKED = new EventType();;
    public static final EventType REMOVE_ALL_FILTER_CLICKED = new EventType();
    public static final EventType FILTER_SAVE_CLICKED = new EventType();
    public static final EventType FILTER_SAVED = new EventType();
    public static final EventType FILTER_SAVED_SUCCESS = new EventType();
    public static final EventType FILTER_SELECTED = new EventType();
    public static final EventType FILTER_DELETE_CLICKED = new EventType();
    public static final EventType FILTER_RENAME_CLICKED = new EventType();
    public static final EventType FILTER_RENAMED = new EventType();
    public static final EventType IS_EXIST_SUCCESS = new EventType();
}
