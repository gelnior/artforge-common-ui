package fr.hd3d.common.ui.client.widget.constraint.view.field;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.form.ComboBox;

import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.widget.constraint.OperatorListFactory;
import fr.hd3d.common.ui.client.widget.constraint.OperatorModelData;


/**
 * Combo box containing operator choice depending on column type given by user.
 * 
 * @author HD3D
 */
public class OperatorComboBox extends ComboBox<OperatorModelData>
{
    /**
     * Default Constructor.
     */
    public OperatorComboBox()
    {
        super();

        this.configure();
    }

    /**
     * Set style, store and behavior.
     */
    private void configure()
    {
        this.setEditable(false);
        this.setTriggerAction(TriggerAction.ALL);

        this.setValueField(OperatorModelData.OPERATOR_FIELD);
        this.setDisplayField(OperatorModelData.NAME_FIELD);
        this.setStore(new ListStore<OperatorModelData>());
        this.setWidth(125);
    }

    /**
     * Change the operator combo box constraint depending on column type.
     * 
     * @param column
     *            The selected column.
     */
    public void setStoreForColumn(PropertyModelData column)
    {
        String type = column.getType();

        this.store.removeAll();

        if (FieldUtils.isString(type))
        {
            this.store.add(OperatorListFactory.getStringOperators());
        }
        else if (FieldUtils.isNumber(type))
        {
            this.store.add(OperatorListFactory.getLongOperators());
        }
        else if (FieldUtils.isBoolean(type))
        {
            this.store.add(OperatorListFactory.getBooleanOperators());
        }
        else if (FieldUtils.isDate(type))
        {
            this.store.add(OperatorListFactory.getDateOperators());
        }
        else if (FieldUtils.isTag(type))
        {
            this.store.add(OperatorListFactory.getTagOperators());
        }
        else if (FieldUtils.isEnum(type))
        {
            this.store.add(OperatorListFactory.getEnumOperators());
        }
        /* specific entity operators */
        else if (FieldUtils.isApproval(type))
        {
            this.store.add(OperatorListFactory.getApprovalNoteOperators());
        }
        else if (FieldUtils.isList(type)) 
        {
        	this.store.add(OperatorListFactory.getBooleanOperators());
        }
        /* generic entity operators */
        else if (FieldUtils.isEntity(type))
        {
            this.store.add(OperatorListFactory.getModelDataOperators());
        }

        this.setValue(null);
    }
}
