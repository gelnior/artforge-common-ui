package fr.hd3d.common.ui.client.widget.constraint.widget;

import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.event.LoadListener;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.common.ui.client.modeldata.technical.SheetFilterModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.service.parameter.EqConstraint;
import fr.hd3d.common.ui.client.service.parameter.OrderBy;
import fr.hd3d.common.ui.client.service.store.ServiceStore;


/**
 * Handles sheet filter combo box data.
 * 
 * @author HD3D
 */
public class SheetFilterModel
{
    /** Constraint used to retrieve only filter associated to a given sheet. */
    protected EqConstraint filterConstraint = new EqConstraint("sheet");

    /** Filter to allow user to clear current filter from sheet filter combo box. */
    protected SheetFilterModelData noSheetFilter = new SheetFilterModelData("No filter");

    /** Store containing all filters available for current sheet. */
    private ServiceStore<SheetFilterModelData> store;

    /**
     * @return Store containing all filters available for current sheet.
     */
    public ServiceStore<SheetFilterModelData> getFilterStore()
    {
        return store;
    }

    /**
     * Register sheet filter combo store to model and configures store to retrieve right data (set path and
     * constraints).
     * 
     * @param serviceStore
     *            The store to set and configure.
     */
    public void setFilterStore(ServiceStore<SheetFilterModelData> serviceStore)
    {
        this.store = serviceStore;
        this.store.setPath(ServicesURI.SHEET_FILTERS + "/");
        this.store.addParameter(new OrderBy(SheetFilterModelData.NAME_FIELD));
        this.store.addParameter(filterConstraint);
        this.store.addLoadListener(new LoadListener() {
            @Override
            public void loaderLoad(LoadEvent le)
            {
                store.add(noSheetFilter);
            }
        });
    }

    /**
     * Reload store for a given sheet : retrieve filter saved for this sheet. If sheet is null store is cleared.
     * 
     * @param sheet
     *            The sheet of which filter will be loaded.
     */
    public void refreshFilterStore(SheetModelData sheet)
    {
        if (sheet != null && sheet.getId() != this.filterConstraint.getLeftMember())
        {
            this.filterConstraint.setLeftMember(sheet.getId());
            this.store.reload();
        }
        else if (sheet == null)
        {
            this.store.removeAll();
        }
        else
        {
            if (this.store.findModel(noSheetFilter) != null)
            {
                this.store.add(noSheetFilter);
            }
        }
    }

    /**
     * @return The model data representing the no filter command.
     */
    public SheetFilterModelData getNoFilter()
    {
        return this.noSheetFilter;
    }

}
