package fr.hd3d.common.ui.client.widget.constraint;

import java.util.HashMap;
import java.util.Map;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.client.enums.EItemFilterOperator;
import fr.hd3d.common.ui.client.modeldata.NameModelData;


/**
 * Model data representation of constraint operators.
 * 
 * @author HD3D
 */
public class ItemOperatorModelData extends NameModelData
{
    private static final long serialVersionUID = 6658977012847349295L;

    public static final String ITEMOPERATOR_FIELD = "itemoperator";

    public static final Map<EConstraintOperator, EItemFilterOperator> operatorMaps = new HashMap<EConstraintOperator, EItemFilterOperator>() {

        private static final long serialVersionUID = 1295079862649231489L;

        {
            put(EConstraintOperator.eq, EItemFilterOperator.eq);
            put(EConstraintOperator.neq, EItemFilterOperator.neq);
            put(EConstraintOperator.geq, EItemFilterOperator.geq);
            put(EConstraintOperator.leq, EItemFilterOperator.leq);
            put(EConstraintOperator.lt, EItemFilterOperator.lt);
            put(EConstraintOperator.gt, EItemFilterOperator.gt);
            put(EConstraintOperator.btw, EItemFilterOperator.btw);
            put(EConstraintOperator.in, EItemFilterOperator.in);
            put(EConstraintOperator.notin, EItemFilterOperator.notin);
            put(EConstraintOperator.like, EItemFilterOperator.like);
            put(EConstraintOperator.isnull, EItemFilterOperator.isnull);
            put(EConstraintOperator.isnotnull, EItemFilterOperator.isnotnull);
            put(EConstraintOperator.hasTag, null);
            put(EConstraintOperator.hasNotTag, null);
        }
    };

    /**
     * Default constructor.
     */
    public ItemOperatorModelData()
    {
        super("");
    }

    /**
     * Constructor with parameter.
     * 
     * @param name
     *            The name of the operator.
     * @param operator
     *            The operator described by the name.
     */
    public ItemOperatorModelData(String name, EItemFilterOperator operator)
    {
        super(name);

        this.setOperator(operator);
    }

    public EItemFilterOperator getOperator()
    {
        return this.get(ITEMOPERATOR_FIELD);
    }

    public void setOperator(EItemFilterOperator operator)
    {
        this.set(ITEMOPERATOR_FIELD, operator);
    }
}
