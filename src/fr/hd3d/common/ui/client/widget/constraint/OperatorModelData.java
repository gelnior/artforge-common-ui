package fr.hd3d.common.ui.client.widget.constraint;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.modeldata.NameModelData;


/**
 * Model data representation of constraint operators.
 * 
 * @author HD3D
 */
public class OperatorModelData extends NameModelData
{
    private static final long serialVersionUID = 6658977012847349295L;

    public static final String OPERATOR_FIELD = "operator";

    /**
     * Default constructor.
     */
    public OperatorModelData()
    {
        super("");
    }

    /**
     * Constructor with parameter.
     * 
     * @param name
     *            The name of the operator.
     * @param operator
     *            The operator described by the name.
     */
    public OperatorModelData(String name, EConstraintOperator operator)
    {
        super(name);

        this.setOperator(operator);
    }

    public EConstraintOperator getOperator()
    {
        return this.get(OPERATOR_FIELD);
    }

    public void setOperator(EConstraintOperator operator)
    {
        this.set(OPERATOR_FIELD, operator);
    }

    @Override
    public String toString()
    {
        if (this.getOperator() != null)
            return this.getOperator().toString();
        else
            return "No operator (null)";
    }
}
