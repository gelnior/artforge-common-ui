package fr.hd3d.common.ui.client.widget.constraint.widget;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.KeyListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.Store;
import com.extjs.gxt.ui.client.store.StoreFilter;
import com.extjs.gxt.ui.client.util.Util;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.form.TextField;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.technical.SheetFilterModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.service.callback.PostModelDataCallback;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.widget.ModelDataComboBox;
import fr.hd3d.common.ui.client.widget.constraint.event.ConstraintEvents;
import fr.hd3d.common.ui.client.widget.dialog.FormDialog;


/**
 * Save filter dialog allow to save current filter in a filter that already exists or in a new one.
 * 
 * 
 * @author HD3D
 */
public class SaveFilterDialog extends FormDialog
{
    /** Combo box to select the filter to update. */
    protected ModelDataComboBox<SheetFilterModelData> filterCombo;
    /** Name of the filter to create. */
    protected TextField<String> nameField = new TextField<String>();

    /** Text representing filter to save. */
    private String filterText;
    /** Sheet linked to the filter to save. */
    private SheetModelData sheet;

    /** filter save to be created. */
    private SheetFilterModelData filterToCreate;

    /** Filter used to not display no Filter data. */
    private final StoreFilter<SheetFilterModelData> filter = new StoreFilter<SheetFilterModelData>() {
        public boolean select(Store<SheetFilterModelData> store, SheetFilterModelData parent,
                SheetFilterModelData item, String property)
        {
            if (item.getId() == null)
                return false;
            else
                return true;
        }
    };

    /**
     * Constructor : take a filter store in argument to coordinate store from constraint panel to the available filters.
     * 
     * @param filterStore
     *            The Filter store for filter combo box.
     */
    public SaveFilterDialog(ServiceStore<SheetFilterModelData> filterStore)
    {
        super(ConstraintEvents.FILTER_SAVED, "Save filter");

        this.filterCombo = new ModelDataComboBox<SheetFilterModelData>(filterStore);
        this.setFields();
        this.setStyles();
    }

    /**
     * Display sheet filter save dialog and set parameters : sheet to know on which sheet the filter should be linked
     * and filterText, the string representation of the filter.
     * 
     * @param sheet
     *            The sheet of which the filter should be linked on.
     * @param filterText
     *            The string representation of the filter.
     */
    public void show(SheetModelData sheet, String filterText)
    {
        super.show();

        this.filterCombo.getStore().addFilter(filter);
        this.filterCombo.getStore().applyFilters(SheetFilterModelData.ID_FIELD);

        this.filterCombo.enable();
        this.filterText = filterText;
        this.sheet = sheet;
        this.filterCombo.setValue(null);
        this.nameField.clear();
    }

    @Override
    public void hide()
    {
        this.filterCombo.getStore().removeFilter(filter);
        this.filterCombo.getStore().applyFilters(SheetFilterModelData.ID_FIELD);
        super.hide();
    }

    /**
     * When OK button is clicked, a new filter is created if a name is set inside name field. If no name is set, the
     * selected field is updated.
     */
    @Override
    protected void onOkClicked()
    {

        if (this.nameField.getValue() != null)
        {
            this.createNewFilter();
        }
        else if (this.filterCombo.getSelection().size() != 0)
        {
            this.updateSelectedFilter();
        }
        else
        {
            this.openWarningEmptyFields();
        }
    }

    /**
     * Open a message box to alert that the fields are empty.
     */
    private void openWarningEmptyFields()
    {
        MessageBox.alert(COMMON_CONSTANTS.warning(),
                "Please select a filter to update or enter a filter name to create.", null);
    }

    @Override
    protected void onCancelClicked()
    {

        hide();
    }

    /**
     * Create a new filter for this sheet of which name is the text given in name field. Saving indicator is shown
     * during the whole operation;
     */
    private void createNewFilter()
    {
        this.showSaving();
        SheetFilterModelData filter = new SheetFilterModelData(this.nameField.getValue(), this.sheet.getId(),
                this.filterText);
        filterToCreate = filter;
        filter.checkIsExist(ConstraintEvents.IS_EXIST_SUCCESS, SheetFilterModelData.SHEET_ID_FIELD);
        // this.saveFilter(filter);
    }

    /**
     * Open a message box that alerts the users that this filter name is already used and reset the parameters.
     */
    public void openErrorFilterAlreadyExist()
    {
        filterToCreate = null;
        hideSaving();
        MessageBox.alert(COMMON_CONSTANTS.Error(), "A filter with this name already exists. Please pick another one.",
                null);

    }

    /**
     * Update filter selected from filter combobox.
     */
    private void updateSelectedFilter()
    {
        this.showSaving();
        SheetFilterModelData filter = this.filterCombo.getValue();
        filter.setFilter(this.filterText);
        this.saveFilter(filter);
    }

    /**
     * Save filter to services. When saving is ended, save indicator is hidden.
     * 
     * @param filter
     *            The filter to save.
     */
    public void saveFilter(SheetFilterModelData filter)
    {
        filter.save(new PostModelDataCallback(filter, new AppEvent(this.okEvent)) {
            @Override
            protected void onSuccess(Request request, Response response)
            {
                this.setNewId(response);

                AppEvent event = new AppEvent(ConstraintEvents.FILTER_SAVED_SUCCESS);
                event.setData(this.record);
                EventDispatcher.forwardEvent(event);

                hideSaving();
                hide();
            }

            @Override
            protected void onError()
            {
                hideSaving();
            }
        });
    }

    /**
     * Create the filter asked by users. This method is called after to check if filter is already used.
     */
    public void createFilter()
    {
        if (filterToCreate == null)
        {
            return;
        }
        filterToCreate.save(new PostModelDataCallback(filterToCreate, new AppEvent(this.okEvent)) {
            @Override
            protected void onSuccess(Request request, Response response)
            {
                this.setNewId(response);

                AppEvent event = new AppEvent(ConstraintEvents.FILTER_SAVED_SUCCESS);
                event.setData(this.record);
                EventDispatcher.forwardEvent(event);

                hideSaving();
                hide();
            }

            @Override
            protected void onError()
            {
                hideSaving();
            }
        });
        filterToCreate = null;
    }

    /** Set fields inside form : filter combo and name field. */
    private void setFields()
    {
        this.filterCombo.setLabelSeparator(":");
        this.filterCombo.setFieldLabel("Filter to update");
        this.filterCombo.setContextMenu();
        this.filterCombo.addKeyListener(enterListener);
        this.panel.add(filterCombo);

        this.nameField.setLabelSeparator(":");
        this.nameField.setFieldLabel("<b>OR</b> create a new filter");
        this.nameField.addKeyListener(enterListener);
        this.nameField.addKeyListener(new KeyListener() {
            @Override
            public void componentKeyUp(ComponentEvent event)
            {
                onNameChanged();
            }
        });

        this.panel.add(nameField);
    }

    /**
     * When name is changed, it supposed that user want to create a new filter, so filter combo is disabled and value is
     * set to null. If name is empty, filter combo is enabled.
     */
    private void onNameChanged()
    {
        if (Util.isEmptyString(nameField.getValue()))
        {
            this.filterCombo.setEnabled(true);
        }
        else
        {
            this.filterCombo.setValue(null);
            this.filterCombo.setEnabled(false);
        }
    }

    /** Set dialog styles. */
    private void setStyles()
    {
        this.setModal(true);
        this.setWidth(390);
        this.panel.setLabelWidth(130);
        this.filterCombo.setEditable(false);
        this.setHideOnButtonClick(false);
    }

}
