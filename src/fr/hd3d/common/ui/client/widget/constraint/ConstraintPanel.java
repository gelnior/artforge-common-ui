package fr.hd3d.common.ui.client.widget.constraint;

import com.extjs.gxt.ui.client.Style.Scroll;
import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.util.Margins;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.VBoxLayout;
import com.extjs.gxt.ui.client.widget.layout.VBoxLayout.VBoxLayoutAlign;
import com.extjs.gxt.ui.client.widget.layout.VBoxLayoutData;
import com.extjs.gxt.ui.client.widget.toolbar.FillToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.ToolBar;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.widget.ToolBarButton;
import fr.hd3d.common.ui.client.widget.constraint.event.ConstraintEvents;
import fr.hd3d.common.ui.client.widget.constraint.model.ConstraintModelData;
import fr.hd3d.common.ui.client.widget.constraint.model.ConstraintPanelModel;
import fr.hd3d.common.ui.client.widget.explorer.ExplorerPanel;
import fr.hd3d.common.ui.client.widget.explorer.model.IColumnInfoProvider;


/**
 * Constraint panel is a tool linked to the HD3D explorer. It allows user to filter data displayed in the explorer. A
 * filter is linked to columns. The user can add as many filter as he wants. Each filter acts like there is an AND
 * relationship between them. A filter is represented by a row where the user can select one of the displayed column, a
 * filter operator and the value needed by the operator. If the column is a complex column, there is no filter available
 * for it.
 * 
 * @author HD3D
 */
public class ConstraintPanel extends ContentPanel implements IConstraintPanel
{
    /** Constant strings to display : dialog messages, button label... */
    public static final CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    /** Setting name for setting storing of the filter. */
    public static final String CONSTRAINT_SETTING_PREFIX = "sheet.filter.";

    /**
     * Character used for data separation inside filter setting sequence.
     * column<i>separator</i>operator<i>separator</i>value.
     */
    public static final char CONSTRAINT_SETTING_DATA_SEPARATOR = '|';
    /**
     * Character used for filter separation for setting sequence.
     * constraint01<i>separator</i>constraint02<i>separator</i>...<i>separator</i>constraint.
     */
    public static final char CONSTRAINT_SETTING_SEPARATOR = '+';

    /** Row height in pixel. */
    public static final int ROW_HEIGHT = 25;
    /** Panel scroller width in pixel. */
    public static final int SCROLLER_WIDTH = 20;

    /** Model that handles constraint panel list store. */
    protected final ConstraintPanelModel model = new ConstraintPanelModel();
    /** Controller that handles constraint panel events. */
    protected final ConstraintPanelController controller = new ConstraintPanelController(model, this);
    /** Explorer linked to the constraint panel. */
    protected final ExplorerPanel explorer;

    /** Map associating Constraint to row (useful for row deletion). */
    protected final FastMap<ConstraintRow> rowMap = new FastMap<ConstraintRow>();
    /** Content panel containing rows. */
    protected final ContentPanel panel = new ContentPanel();

    /** Constraint provider provides editors for specific field (like enumeration). */
    protected IEditorProvider constraintProvider;

    /**
     * Default constructor.
     * 
     * @param explorateurPanel
     *            Explorer associated to constraint panel.
     */
    public ConstraintPanel(ExplorerPanel explorateurPanel)
    {
        super();

        this.explorer = explorateurPanel;

        this.setStore();
        this.setController();
        this.setStyle();
        this.setLayoutConfig();
        this.setToolBar();
    }

    /**
     * @return Editor constraint used by constraint panel.
     */
    public IEditorProvider getConstraintProvider()
    {
        return constraintProvider;
    }

    /**
     * Set constraint panel editor provider.
     * 
     * @param constraintProvider
     *            The editor provider the constraint panel wiil used.
     */
    public void setConstraintEditorProvider(IEditorProvider constraintProvider)
    {
        this.constraintProvider = constraintProvider;
    }

    /**
     * @return Column info provider of the explorer linked to the constraint panel.
     */
    public IColumnInfoProvider getInfoColumnProvider()
    {
        return this.explorer.getColumnInfoProvider();
    }

    /**
     * @return the current sheet selected in explorer.
     */
    public Long getCurrentSheetId()
    {
        if (this.explorer.getCurrentSheet() != null)
        {
            return this.explorer.getCurrentSheet().getId();
        }
        else
        {
            return null;
        }
    }

    /**
     * Add a new row to panel and set a new constraint (associated to the row) to the data filter. Row field values are
     * automatically set on constraint model data attributes.
     * 
     * @param constraint
     *            The constraint model data to add to the filter.
     */
    public void addRow(ConstraintModelData constraint)
    {
        SheetModelData sheet = this.explorer.getCurrentSheet();

        if (sheet != null)
        {
            ConstraintRow row = new ConstraintRow(this, this.model.getAvailableColumns(), constraint);
            this.panel.add(row, new VBoxLayoutData(new Margins(0, 0, 0, 0)));
            this.panel.setHeight(this.panel.getHeight() + ROW_HEIGHT);
            row.setWidth(this.getWidth() - SCROLLER_WIDTH);
            this.layout();

            this.rowMap.put(constraint.toString(), row);

            if (constraint.getColumnName() != null)
            {
                row.updatePropertyField(constraint.getColumnName());
            }
            if (constraint.getOperatorName() != null)
            {
                row.updateOperatorField(constraint.getOperatorName());
            }
            if (constraint.getStringValue() != null)
            {
                row.updateValue(constraint.getStringValue());
            }
        }
    }

    /**
     * Remove row from panel and constraint associated to the row from data filter.
     * 
     * @param row
     *            The row to remove.
     */
    public void removeRow(ConstraintRow row)
    {
        AppEvent event = new AppEvent(ConstraintEvents.REMOVE_FILTER_CLICKED);
        event.setData(row.getConstraint());
        this.controller.handleEvent(event);

        this.panel.setHeight(this.panel.getHeight() - ROW_HEIGHT);
    }

    /**
     * Remove all rows from panel (clear filter).
     */
    public void removeAllRows()
    {
        for (ConstraintRow row : this.rowMap.values())
        {
            this.panel.remove(row);
        }
        this.rowMap.clear();
        this.panel.setHeight(0);
    }

    /**
     * Remove row without removing constraint model data from list store.
     * 
     * @param model
     */
    public void removeRowFromView(ConstraintModelData model)
    {
        ConstraintRow row = rowMap.get(model.toString());

        if (row != null)
        {
            this.panel.remove(row);
            this.panel.layout();

            rowMap.remove(model.toString());
        }
    }

    /**
     * @return Constraint panel controller.
     */
    public MaskableController getController()
    {
        return this.controller;
    }

    /**
     * Set tool bar containing add row button and apply filter button.
     */
    protected void setToolBar()
    {
        ToolBar toolbar = new ToolBar();

        ToolBarButton addButton = new ToolBarButton(Hd3dImages.getAddIcon(), CONSTANTS.AddFilter(),
                ConstraintEvents.ADD_FILTER_CLICKED);
        ToolBarButton removeAllButton = new ToolBarButton(Hd3dImages.getRemoveAllFiltersIcon(),
                CONSTANTS.RemoveAllButtons(), ConstraintEvents.REMOVE_ALL_FILTER_CLICKED);
        ToolBarButton lastButton = new ToolBarButton(Hd3dImages.getUndoIcon(), CONSTANTS.LastFilterApplied(),
                ConstraintEvents.LAST_FILTER_CLICKED);
        ToolBarButton saveButton = new ToolBarButton(Hd3dImages.getSaveIcon(), CONSTANTS.Save(),
                ConstraintEvents.FILTER_SAVE_CLICKED);
        ToolBarButton applyButton = new ToolBarButton(Hd3dImages.getRefreshPageIcon(), CONSTANTS.ApplyFilter(),
                ConstraintEvents.APPLY_FILTER_CLICKED);

        toolbar.add(addButton);
        toolbar.add(removeAllButton);
        toolbar.add(lastButton);
        toolbar.add(saveButton);
        toolbar.add(applyButton);

        toolbar.add(new FillToolItem());

        this.setTopComponent(toolbar);
    }

    /**
     * Set store and bind it to the panel (to add and remove row when Constraint liststore is updated).
     */
    protected void setStore()
    {
        ConstraintEditorStoreBinder binder = new ConstraintEditorStoreBinder(this.model.getEditorStore(), this);
        binder.init();
    }

    /**
     * Set a vertical box layout configuration.
     */
    protected void setLayoutConfig()
    {
        this.setLayout(new FitLayout());

        VBoxLayout vLayout = new VBoxLayout();
        vLayout.setVBoxLayoutAlign(VBoxLayoutAlign.LEFT);

        this.panel.setLayout(vLayout);

        this.add(panel);
        this.panel.setHeight(0);
    }

    /**
     * Set panel styles.
     */
    protected void setStyle()
    {
        this.setHeaderVisible(false);
        this.setHeight(height);
        this.panel.setBodyBorder(false);
        this.panel.setHeaderVisible(false);
        this.panel.setBorders(false);
        this.setScrollMode(Scroll.AUTOY);
    }

    /**
     * Add controller to event dispatcher.
     */
    protected void setController()
    {
        EventDispatcher.get().addController(controller);
    }

    /**
     * @return Sheet currently active in the explorer.
     */
    public SheetModelData getCurrentSheet()
    {
        return this.explorer.getCurrentSheet();
    }

    /**
     * @return true if constraint panel is not visible.
     */
    public boolean isHidden()
    {
        return !super.isVisible();
    }

    /**
     * Add a field to the excluded constraint list. Excluded field, can't be selected in constraint builder.
     * 
     * @param fieldName
     *            The name of the field to exclude.
     */
    public void addExcludedConstraint(String fieldName)
    {
        this.model.addExcludedConstraint(fieldName);
    }

    public ListStore<ConstraintModelData> getEditorStore()
    {
        return this.model.getEditorStore();
    }
}
