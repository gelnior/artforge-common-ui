package fr.hd3d.common.ui.client.widget.constraint.widget;

import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.modeldata.reader.SheetFilterReader;
import fr.hd3d.common.ui.client.modeldata.technical.SheetFilterModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.util.CSSUtils;
import fr.hd3d.common.ui.client.widget.EasyMenu;
import fr.hd3d.common.ui.client.widget.ModelDataComboBox;
import fr.hd3d.common.ui.client.widget.constraint.SaveFilterDialogController;
import fr.hd3d.common.ui.client.widget.constraint.event.ConstraintEvents;
import fr.hd3d.common.ui.client.widget.dialog.RenameDialog;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.ConstraintPanelToggle;


/**
 * Combo box storing sheet filter with an automatically set context menu (for deleting, renaming, refreshing).
 * 
 * @author HD3D
 */
public class SheetFilterComboBox extends ModelDataComboBox<SheetFilterModelData> implements ISheetFilterView
{

    /** Model handling sheet filter combo box data. */
    protected SheetFilterModel model = new SheetFilterModel();
    /** Controller handling sheet filter combo box events. */
    protected SheetFilterController controller = new SheetFilterController(model, this);

    /** Dialog used to save current filter to sheet pre-defined filters. */
    protected SaveFilterDialog saveFilterDialog;
    /** Controller that handles constraint saveFilterDialog events. */
    protected SaveFilterDialogController saveFilterDialogController = new SaveFilterDialogController();
    /**
     * Reference to the filter panel button to highlight it when a filter is set and un-highlight it when nothing is
     * set.
     */
    private ConstraintPanelToggle filterPanelButton;

    /**
     * Constructor : set store, selection change event and context menu.
     * 
     */
    public SheetFilterComboBox()
    {
        super(new ServiceStore<SheetFilterModelData>(new SheetFilterReader()));
        this.model.setFilterStore(this.getServiceStore());

        this.setSelectionChangedEvent(ConstraintEvents.FILTER_SELECTED);
        this.setValidateOnBlur(false);
        this.setContextMenu();
    }

    /**
     * @return sheet filter combo box controller.
     */
    public MaskableController getController()
    {
        return this.controller;
    }

    /**
     * Set contextual menu that allows to refresh combo box store content, delete selected value or rename selected
     * value.
     */
    @Override
    public EasyMenu setContextMenu()
    {
        EasyMenu menu = super.setContextMenu();
        menu.insertItem(0, "Delete filter", ConstraintEvents.FILTER_DELETE_CLICKED, Hd3dImages.getDeleteIcon());
        menu.insertItem(0, "Rename filter", ConstraintEvents.FILTER_RENAME_CLICKED, Hd3dImages.getEditIcon());

        return menu;
    }

    /**
     * @return currently selected filter.
     */
    public SheetFilterModelData getSelectedFilter()
    {
        return this.getValue();
    }

    /**
     * Replace sheet filter combo box raw value with <i>name</i>.
     * 
     * @param name
     *            The name to set as raw value.
     */
    public void setUpdateFilterRawValue(String name)
    {
        this.setRawValue(name);
    }

    /** Display save filter dialog. */
    public void showSaveFilterDialog(String filterText, SheetModelData sheet)
    {
        if (saveFilterDialog == null)
        {
            saveFilterDialog = new SaveFilterDialog(this.model.getFilterStore());
            saveFilterDialogController.setView(saveFilterDialog);
            controller.addChild(saveFilterDialogController);
        }
        saveFilterDialog.show(sheet, filterText);
    }

    /**
     * Reload filter list depending on sheet.
     * 
     * @param sheet
     *            Sheet of which filter should be retrieved.
     */
    public void refreshFilterList(SheetModelData sheet)
    {
        this.model.refreshFilterStore(sheet);
    }

    /**
     * Display a rename dialog box for currently selected filter.
     * 
     * @param filter
     *            Filter to rename.
     */
    public void displayRenameFilter(SheetFilterModelData filter)
    {
        RenameDialog dialog = new RenameDialog("Rename filter");
        dialog.setEventType(ConstraintEvents.FILTER_RENAMED);
        dialog.show(filter);
    }

    /**
     * Set selected combo box value to null.
     */
    public void setNoFilterSelected()
    {
        this.setValue(this.model.getNoFilter());
    }

    /**
     * Display custom filter label.
     */
    public void setCustomLabel()
    {
        this.setRawValue("Custom filter");
    }

    /**
     * Register filter panel button to sheet filter combobox, to highlight it when a filter set and remove highlight
     * when no filter is set.
     * 
     * @param filterPanelButton
     *            The button to highlight.
     */
    public void registerToggleButton(ConstraintPanelToggle filterPanelButton)
    {
        this.filterPanelButton = filterPanelButton;
    }

    public void highlightFilterPanelButton()
    {
        if (filterPanelButton != null)
            CSSUtils.set2pxBorder(filterPanelButton, "red");
    }

    public void removeHighlightFilterPanelButton()
    {
        if (filterPanelButton != null)
            CSSUtils.set2pxBorder(filterPanelButton, "transparent");
    }

}
