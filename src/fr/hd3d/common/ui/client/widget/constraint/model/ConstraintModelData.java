package fr.hd3d.common.ui.client.widget.constraint.model;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.extjs.gxt.ui.client.data.BaseModelData;
import com.extjs.gxt.ui.client.util.Util;

import fr.hd3d.common.client.Sheet;
import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.client.enums.EItemFilterOperator;
import fr.hd3d.common.ui.client.calendar.DateFormat;
import fr.hd3d.common.ui.client.modeldata.BooleanModelData;
import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.writer.JsonEncoder;
import fr.hd3d.common.ui.client.service.ServicesClass;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.ItemConstraint;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.widget.constraint.IEditorProvider;
import fr.hd3d.common.ui.client.widget.constraint.ItemOperatorModelData;
import fr.hd3d.common.ui.client.widget.constraint.OperatorModelData;
import fr.hd3d.common.ui.client.widget.constraint.view.field.PropertyModelData;


/**
 * This is the model data shape of the Constraint object.
 * 
 * @author HD3D
 */
public class ConstraintModelData extends BaseModelData
{
    private static final long serialVersionUID = -9119223884546228179L;

    /** True when the constraint is active. */
    public static final String ACTIVE_FIELD = "active";
    /** The column concerned by the constraint. */
    public static final String COLUMN_FIELD = "column";
    /** The constraint operator. */
    public static final String OPERATOR_FIELD = "operator";
    /** The item constraint operator (needed for calculated fields). */
    public static final String ITEMOPERATOR_FIELD = "itemOperator";
    /** The value needed by the operator. */
    public static final String VALUE_FIELD = "value";

    protected Long columnId;
    /** String variant of the column name */
    protected String columnName;
    /** String variant of the operator name */
    protected String operatorName;
    /** String variant of the string value */
    protected String stringValue;

    protected Set<Long> hibConstrainableItems = null;

    /**
     * Default constructor. Set active field to true.
     */
    public ConstraintModelData(Set<Long> hibConstrainableItems)
    {
        this.setActive(true);
        this.hibConstrainableItems = hibConstrainableItems;
    }

    /**
     * Constructor with parameters.
     * 
     * @param active
     *            True when the constraint is active.
     * @param column
     *            The column concerned by the constraint.
     * @param operator
     *            The constraint operator.
     * @param value
     *            The value needed by the operator.
     */
    public ConstraintModelData(Boolean active, PropertyModelData column, OperatorModelData operator, Object value,
            Set<Long> hibConstrainableItems)
    {
        this.setActive(active);
        this.setColumn(column);
        this.setOperator(operator);
        this.setValue(value);
        this.hibConstrainableItems = hibConstrainableItems;
    }

    public Long getColumnId()
    {
        return columnId;
    }

    public void setColumnId(Long columnId)
    {
        this.columnId = columnId;
    }

    public String getColumnName()
    {
        return columnName;
    }

    public void setColumnName(String columnName)
    {
        this.columnName = columnName;
    }

    public String getOperatorName()
    {
        return operatorName;
    }

    public void setOperatorName(String operatorName)
    {
        this.operatorName = operatorName;
    }

    public String getStringValue()
    {
        return stringValue;
    }

    /**
     * Set value at string format. If the value is a 'like operator' value, the % chars are removed from the string.
     * 
     * @param value
     */
    public void setStringValue(String value)
    {
        if (EConstraintOperator.like.toString().equals(this.getOperatorName()) && value.length() > 2)
        {
            this.stringValue = value.substring(1, value.length() - 1);
        }
        else
        {
            this.stringValue = value;
        }
    }

    public Boolean getActive()
    {
        return this.get(ACTIVE_FIELD);
    }

    public void setActive(Boolean active)
    {
        this.set(ACTIVE_FIELD, active);
    }

    public PropertyModelData getColumn()
    {
        return this.get(COLUMN_FIELD);
    }

    public void setColumn(PropertyModelData column)
    {
        this.set(COLUMN_FIELD, column);
    }

    public OperatorModelData getOperator()
    {
        return this.get(OPERATOR_FIELD);
    }

    public void setOperator(OperatorModelData operator)
    {
        this.set(OPERATOR_FIELD, operator);
    }

    public Object getValue()
    {
        Object value = this.get(VALUE_FIELD);
        PropertyModelData column = this.getColumn();

        if (column != null && value instanceof Number)
        {
            value = FieldUtils.getNumber(column.getType(), value);
        }
        else if (value instanceof FieldModelData)
        {
            value = ((FieldModelData) value).getValue();
        }

        return value;
    }

    public void setValue(Object value)
    {
        this.set(VALUE_FIELD, value);
    }

    /**
     * @return True if the field is valid, it means, not null operator, not null column and a valid value corresponding
     *         to selected column and selected operator.
     */
    public Boolean isValid()
    {
        OperatorModelData operator = this.getOperator();
        PropertyModelData column = this.getColumn();
        Object value = this.getValue();

        // If there is no operator, constraint is not valid.
        if (operator == null)
            return false;

        // If there is no column, constraint is not valid.
        if (column == null)
            return false;

        // If there is no value and operator is not not null or is null, the constraint is considered as not valid.
        if ((value == null && this.stringValue == null)
                && !ServicesClass.contains(column.getType())
                && (operator.getOperator() != EConstraintOperator.isnotnull || operator.getOperator() != EConstraintOperator.isnull))
            return false;

        // In case of in operator the constraint is considered as valid if value is not an empty list or is not null.
        if (operator.getOperator() == EConstraintOperator.in)
        {
            if ((Util.isEmptyString(stringValue) || "[]".equals(stringValue)) && value == null)
                return false;
            if (value != null && CollectionUtils.isEmpty((Collection<?>) value)) // value nullity is tested because of
                                                                                 // the cast.
                return false;
        }

        return this.getActive();
    }

    /**
     * @return If constraint model data is valid, it returns the Constraint corresponding to Constraint Model data.
     */
    public Constraint getConstraint()
    {
        OperatorModelData operator = this.getOperator();
        PropertyModelData column = this.getColumn();
        Object value = this.getValue();

        boolean isConstrainable = hibConstrainableItems.contains(column.getItemId());

        if (isValid() && isConstrainable)
        {
            return this.getConstraint(column, operator, value);
        }
        return null;
    }

    /**
     * @param column
     *            The constraint column.
     * @param operator
     *            The constraint operator.
     * @param value
     *            The constraint value.
     * 
     * @return If constraint model data is valid, it returns the Constraint corresponding to given parameters.
     */
    @SuppressWarnings("unchecked")
    private Constraint getConstraint(PropertyModelData column, OperatorModelData operator, Object value)
    {
        Constraint constraint;
        String columnName = column.getName();

        if (operator.getOperator() == EConstraintOperator.like)
        {
            String valueString = "%" + value + "%";
            constraint = new Constraint(operator.getOperator(), columnName, valueString);
        }
        else if (value instanceof BooleanModelData)
        {
            Boolean valueBoolean = ((BooleanModelData) value).getValue();
            constraint = new Constraint(operator.getOperator(), columnName, valueBoolean);
        }
        else if (value instanceof Date)
        {
            String valueDate = DateFormat.DATE_TIME.format((Date) value);
            constraint = new Constraint(operator.getOperator(), columnName, valueDate);
        }
        else if (value instanceof Number || value instanceof Integer)
        {
            constraint = new Constraint(operator.getOperator(), columnName, FieldUtils.getNumber(column.getType(),
                    value));
        }
        else if (value instanceof FieldModelData)
        {
            FieldModelData valueModelData = ((FieldModelData) value);
            constraint = new Constraint(operator.getOperator(), columnName, valueModelData.getValue());
        }
        else if (value instanceof Hd3dModelData)
        {
            constraint = this.getModelDataConstraint(column, operator, (Hd3dModelData) value);
        }
        else if (value instanceof List<?> && CollectionUtils.isNotEmpty((List<?>) value)
                && ((List<?>) value).get(0) instanceof Hd3dModelData)
        {
            constraint = this.getModelDataListConstraint(column, operator, (List<? extends Hd3dModelData>) value);
        }
        else if (ServicesClass.contains(column.getName()) || ServicesClass.contains(column.getType()))
        {
            constraint = this.getServiceClassConstraint(columnName, operator);
        }
        else if (EConstraintOperator.in == operator.getOperator() && !Util.isEmptyString(this.stringValue)
                && !FieldUtils.NULL_STRING.equals(this.stringValue))
        {
            List<String> values = CollectionUtils.parseStringArray(this.stringValue);
            constraint = new Constraint(operator.getOperator(), columnName, values);
        }
        else
        {
            constraint = new Constraint(operator.getOperator(), columnName, value);
        }
        return constraint;
    }

    /**
     * @param columnName
     *            The column to filter on.
     * @param operator
     *            The constraint operator.
     * @param value
     *            The model data used as value.
     * @return New constraint adapted for model data field. It adds a suffix (".id") to the column name and put model
     *         data ID as value.
     */
    private Constraint getModelDataConstraint(PropertyModelData column, OperatorModelData operator, Hd3dModelData value)
    {
        Constraint constraint = null;

        Hd3dModelData valueModelData = (value);
        if (column.getAttributeType().equals(Sheet.ITEMTYPE_ATTRIBUTE))
        {
            constraint = new Constraint(operator.getOperator(), column.getName() + FieldUtils.ID_SUFFIX,
                    valueModelData.getId());
        }
        else
        {
            constraint = new Constraint(operator.getOperator(), column.getName(), valueModelData.getId());
        }
        return constraint;
    }

    /**
     * @param columnName
     *            The column to filter on.
     * @param operator
     *            The constraint operator.
     * @param value
     *            The model data list used as value.
     * @return New constraint adapted for model data list field. It adds a suffix (".id") to the column name and put
     *         list of model data ID as value.
     */
    private Constraint getModelDataListConstraint(PropertyModelData column, OperatorModelData operator,
            List<? extends Hd3dModelData> value)
    {
        Constraint constraint = null;
        if (column.getAttributeType().equals(Sheet.ITEMTYPE_ATTRIBUTE))
        {
            constraint = new Constraint(operator.getOperator(), column.getName() + FieldUtils.ID_SUFFIX,
                    CollectionUtils.getIds(value));
        }
        else
        {
            constraint = new Constraint(operator.getOperator(), column.getName(), CollectionUtils.getIds(value));
        }
        return constraint;
    }

    /**
     * @param columnName
     *            The column to filter on.
     * @param operator
     *            The constraint operator.
     * @return New constraint adapted for service class field (ex : field worker for a task). It adds a ".id" suffix to
     *         the constrained column then it use field value to build the constraint. String value is equal to ID and
     *         is used because it can be stored in settings easily (full object cannot).
     */
    private Constraint getServiceClassConstraint(String columnName, OperatorModelData operator)
    {
        if (operator.getOperator() == EConstraintOperator.isnull
                || operator.getOperator() == EConstraintOperator.isnotnull)
        {
            return new Constraint(operator.getOperator(), columnName + FieldUtils.ID_SUFFIX);
        }
        else if (operator.getOperator() == EConstraintOperator.in)
        {
            if (this.getValue() != null)
            {
                return new Constraint(operator.getOperator(), columnName + FieldUtils.ID_SUFFIX, this.getValue());
            }
            else if (!Util.isEmptyString(this.stringValue) && !FieldUtils.NULL_STRING.equals(this.stringValue))
            {
                List<Long> ids = JsonEncoder.parseJSONLongArray(stringValue);
                return new Constraint(operator.getOperator(), columnName + FieldUtils.ID_SUFFIX, ids);
            }
            else
                return null;
        }
        else if (stringValue != null)
        {
            try
            {
                return new Constraint(operator.getOperator(), columnName + FieldUtils.ID_SUFFIX,
                        Long.parseLong(this.stringValue));
            }
            catch (NumberFormatException exception)
            {
                return new Constraint(operator.getOperator(), columnName + FieldUtils.ID_SUFFIX, this.stringValue);
            }
        }
        else
            return null;
    }

    /**
     * @param editorProvider
     * @return If constraint model data is valid, it returns the Item Constraint corresponding to Constraint Model data.
     */
    public ItemConstraint getItemConstraint(IEditorProvider editorProvider)
    {
        OperatorModelData operator = this.getOperator();
        PropertyModelData column = this.getColumn();
        Object value = this.getValue();

        boolean isConstrainable = hibConstrainableItems.contains(column.getItemId());

        if (isValid() && !isConstrainable)
        {
            return this.getItemConstraint(editorProvider, column, operator, value);
        }
        return null;
    }

    /**
     * @param column
     *            The constraint column.
     * @param operator
     *            The constraint operator.
     * @param value
     *            The constraint value.
     * 
     * @return If constraint model data is valid, it returns the Constraint corresponding to given parameters.
     */
    private ItemConstraint getItemConstraint(IEditorProvider editorProvider, PropertyModelData column,
            OperatorModelData operator, Object value)
    {
        if (operator == null)
            return null;

        EItemFilterOperator itemOperator = ItemOperatorModelData.operatorMaps.get(operator.getOperator());

        ItemConstraint constraint;
        Long columnName = column.getItemId();
        final String fieldName = editorProvider.getItemConstraintColumnName(column.getEditor());

        if (itemOperator == EItemFilterOperator.like)
        {
            String valueString = "%" + value + "%";
            constraint = new ItemConstraint(itemOperator, columnName, valueString);

        }
        else if (value instanceof BooleanModelData)
        {
            Boolean valueBoolean = ((BooleanModelData) value).getValue();
            constraint = new ItemConstraint(itemOperator, columnName, valueBoolean);
        }
        else if (value instanceof Date)
        {
            String valueDate = DateFormat.DATE_TIME.format((Date) value);
            constraint = new ItemConstraint(itemOperator, columnName, valueDate);
        }
        else if (value instanceof Number)
        {
            constraint = new ItemConstraint(itemOperator, columnName, FieldUtils.getNumber(column.getType(), value));
        }
        else if (value instanceof FieldModelData)
        {
            FieldModelData valueModelData = ((FieldModelData) value);
            constraint = new ItemConstraint(itemOperator, columnName, valueModelData.getValue());
        }
        else if (value instanceof Hd3dModelData)
        {
            Hd3dModelData valueModelData = ((Hd3dModelData) value);
            if (column.getAttributeType().equals(Sheet.ITEMTYPE_ATTRIBUTE))
            {
                constraint = new ItemConstraint(itemOperator, column.getItemId(), valueModelData.getId());
            }
            else
            {
                constraint = new ItemConstraint(itemOperator, columnName, valueModelData.getId());
            }
        }
        else if (EConstraintOperator.in == operator.getOperator() && !Util.isEmptyString(this.stringValue)
                && !FieldUtils.NULL_STRING.equals(this.stringValue))
        {
            List<String> values = CollectionUtils.parseStringArray(this.stringValue);
            constraint = new ItemConstraint(itemOperator, columnName, values);
        }
        else if (ServicesClass.contains(column.getType()))
        {
            if (value != null)
            {
                constraint = new ItemConstraint(itemOperator, column.getItemId(), value);
            }
            else if (stringValue != null)
            {
                constraint = new ItemConstraint(itemOperator, column.getItemId(), Long.parseLong(this.stringValue));
            }
            else
                return null;
        }
        else
        {
            constraint = new ItemConstraint(itemOperator, columnName, value);
        }
        constraint.setField(fieldName);
        return constraint;
    }
}
