package fr.hd3d.common.ui.client.widget.constraint.view.field;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.form.ComboBox;


/**
 * Combo box used to select lightweight representation of item model data, named property model data.
 * 
 * @author HD3D
 */
public class PropertyComboBox extends ComboBox<PropertyModelData>
{
    public static final Integer WIDTH = 125;

    public PropertyComboBox()
    {
        super();

        this.setWidth(WIDTH);
        this.setDisplayField(PropertyModelData.DISPLAY_NAME_FIELD);
        this.setStore(new ListStore<PropertyModelData>());

        this.setTriggerAction(TriggerAction.ALL);
    }
}
