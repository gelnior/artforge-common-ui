package fr.hd3d.common.ui.client.widget.constraint;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.mvc.Controller;

import fr.hd3d.common.ui.client.widget.constraint.event.ConstraintEvents;
import fr.hd3d.common.ui.client.widget.constraint.widget.SaveFilterDialog;


/**
 * Controller that handles the event from saveFilterDialog.
 * 
 * @author HD3D
 * 
 */
public class SaveFilterDialogController extends Controller
{
    private SaveFilterDialog view = null;

    /**
     * Default Constructor : set the register events.
     */
    public SaveFilterDialogController()
    {
        this.registerEvents();
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType eventType = event.getType();
        if (eventType == ConstraintEvents.IS_EXIST_SUCCESS)
        {
            onIsExist(event);
        }
    }

    /**
     * Check if the filter that will create is already use.
     * 
     * @param event
     *            the handled event.
     */
    private void onIsExist(AppEvent event)
    {
        Boolean isExist = event.getData();
        if (isExist == null || view == null)
        {
            return;
        }

        if (isExist)
        {
            this.view.openErrorFilterAlreadyExist();
        }
        else
        {
            this.view.createFilter();
        }
    }

    /**
     * Register event supported by the controller.
     */
    protected void registerEvents()
    {
        this.registerEventTypes(ConstraintEvents.IS_EXIST_SUCCESS);
    }

    /**
     * Set the view.
     * 
     * @param saveFilterDialog
     *            the view.
     */
    public void setView(SaveFilterDialog saveFilterDialog)
    {
        this.view = saveFilterDialog;
    }
}
