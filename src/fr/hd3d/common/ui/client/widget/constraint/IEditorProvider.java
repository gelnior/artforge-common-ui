package fr.hd3d.common.ui.client.widget.constraint;

import com.extjs.gxt.ui.client.data.BaseModelData;
import com.extjs.gxt.ui.client.widget.form.Field;

import fr.hd3d.common.client.enums.EConstraintOperator;


public interface IEditorProvider
{
    public Field<?> getEditor(String name, EConstraintOperator operator, BaseModelData modelData);

    public String getItemConstraintColumnName(String name);
}
