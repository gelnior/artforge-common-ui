package fr.hd3d.common.ui.client.widget.constraint.view.field;

import java.util.List;

import fr.hd3d.common.ui.client.modeldata.NameModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;


/**
 * Simple class to represent in a lightweight way, explorer columns. It has only four attributes : display name, name,
 * type, and editor name. This class is needed to have faster performance for combo box displaying.
 * 
 * @author HD3D
 */
public class PropertyModelData extends NameModelData
{
    private static final long serialVersionUID = -7706786161289999721L;
    
    public static final String ITEMID_FIELD = "itemId";
    public static final String TYPE_FIELD = "type";
    public static final String EDITOR_FIELD = "editor";
    public static final String DISPLAY_NAME_FIELD = "displayName";
    public static final String ATTRIBUTE_TYPE_FIELD = "attributeType";
    public static final String LIST_VALUES_FIELD = ItemModelData.LIST_VALUES_FIELD;

    public PropertyModelData(Long itemId, String name, String displayName, String type, String editor, String attributeType, List<String> values)
    {
        super(name);
        this.setItemId(itemId);
        this.setDisplayName(displayName);
        this.setType(type);
        this.setEditor(editor);
        this.setAttributeType(attributeType);
        this.setListValues(values);
    }

    public void setListValues(List<String> values)
    {
        this.set(LIST_VALUES_FIELD, values);
    }

    public List<String> getListValues()
    {
        return this.get(LIST_VALUES_FIELD);
    }
    
    public Long getItemId()
    {
        return get(ITEMID_FIELD);
    }

    public void setItemId(Long itemId)
    {
        set(ITEMID_FIELD, itemId);
    }
    
    public String getDisplayName()
    {
        return get(DISPLAY_NAME_FIELD);
    }

    public void setDisplayName(String displayName)
    {
        set(DISPLAY_NAME_FIELD, displayName);
    }

    public String getType()
    {
        return get(TYPE_FIELD);
    }

    public void setType(String type)
    {
        set(TYPE_FIELD, type);
    }

    public String getEditor()
    {
        return get(EDITOR_FIELD);
    }

    public void setEditor(String editor)
    {
        set(EDITOR_FIELD, editor);
    }

    public String getAttributeType()
    {
        return get(ATTRIBUTE_TYPE_FIELD);
    }

    public void setAttributeType(String attributeType)
    {
        set(ATTRIBUTE_TYPE_FIELD, attributeType);
    }
}
