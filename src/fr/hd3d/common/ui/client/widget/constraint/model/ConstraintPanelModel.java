package fr.hd3d.common.ui.client.widget.constraint.model;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.store.ListStore;

import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;


/**
 * Model handling constraint editor data : constraint list represented by constraint rows of the constraint editor.
 * 
 * @author HD3D
 */
public class ConstraintPanelModel
{
    public final static String CONSTRAINTS = "constraints";
    public final static String ITEMCONSTRAINTS = "itemConstraints";

    /** Store containing all constraint set in the constraint editor. */
    protected final ListStore<ConstraintModelData> store = new ListStore<ConstraintModelData>();

    /** Column available for filtering. */
    protected List<ItemModelData> availableColumns = new ArrayList<ItemModelData>();

    /** A boolean indicator telling if it the first time that constraint panel is displayed. */
    private boolean isFirstShown = true;

    private final FastMap<Boolean> excludedConstraints = new FastMap<Boolean>();

    /**
     * Constructor.
     */
    public ConstraintPanelModel()
    {}

    /**
     * @return Store containing all constraint set in the constraint editor.
     */
    public ListStore<ConstraintModelData> getEditorStore()
    {
        return store;
    }

    /**
     * @return List of available columns for row constraint building.
     */
    public List<ItemModelData> getAvailableColumns()
    {
        return this.availableColumns;
    }

    /**
     * Set list of available columns for row constraint building.
     * 
     * @param availableColumns
     *            The list to set.
     */
    public void setAvailableColumns(List<ItemModelData> availableColumns)
    {
        this.availableColumns.clear();
        this.availableColumns.addAll(availableColumns);
    }

    /**
     * @return True if constraint panel is displayed for first time.
     */
    public boolean isFirstShown()
    {
        return this.isFirstShown;
    }

    /**
     * Set if constraint panel is displayed for the first time.
     * 
     * @param isFirstShown
     *            The boolean to set.
     */
    public void setIsFirstShown(boolean isFirstShown)
    {
        this.isFirstShown = isFirstShown;
    }

    /**
     * Add a field to the excluded constraint field list.
     * 
     * @param field
     *            The field to exclude.
     */
    public void addExcludedConstraint(String field)
    {
        this.excludedConstraints.put(field, Boolean.TRUE);
    }

    /**
     * @param field
     *            The field to test.
     * @return True if given field is excluded from row constraint building.
     */
    public boolean isExcludedConstraint(String field)
    {
        if (this.excludedConstraints.get(field) != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
