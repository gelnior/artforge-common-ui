package fr.hd3d.common.ui.client.widget.constraint;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.binding.FieldBinding;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.KeyListener;
import com.extjs.gxt.ui.client.event.SelectionChangedEvent;
import com.extjs.gxt.ui.client.event.SelectionChangedListener;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.util.Margins;
import com.extjs.gxt.ui.client.util.Util;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.extjs.gxt.ui.client.widget.form.Field;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.HBoxLayout;
import com.extjs.gxt.ui.client.widget.layout.HBoxLayout.HBoxLayoutAlign;
import com.extjs.gxt.ui.client.widget.layout.HBoxLayoutData;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.KeyCodes;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.widget.AutoMultiModelCombo;
import fr.hd3d.common.ui.client.widget.constraint.event.ConstraintEvents;
import fr.hd3d.common.ui.client.widget.constraint.model.ConstraintBinding;
import fr.hd3d.common.ui.client.widget.constraint.model.ConstraintModelData;
import fr.hd3d.common.ui.client.widget.constraint.view.field.OperatorComboBox;
import fr.hd3d.common.ui.client.widget.constraint.view.field.PropertyComboBox;
import fr.hd3d.common.ui.client.widget.constraint.view.field.PropertyModelData;


/**
 * Constraint row displayed by constraint panel.
 * 
 * @author HD3D
 */
public class ConstraintRow extends LayoutContainer
{
    /** Constant strings to display : dialog messages, button label... */
    public static final CommonConstants CONSTANTS = GWT.create(CommonConstants.class);
    /** Index where insert value field. */
    private static final int VALUE_FIELD_INDEX = 3;

    /** Constraint binded to the row. */
    protected ConstraintModelData constraint = new ConstraintModelData(null);
    /** Column list displayed in the column field. */
    protected List<ItemModelData> availableColumns;

    /** Check box binded to constraint active field. */
    protected CheckBox enabledField = new CheckBox();
    /** Combo box binded to column field. */
    protected PropertyComboBox propertyField = new PropertyComboBox();
    /** Combo box binded to constraint operator field. */
    protected OperatorComboBox operatorField = new OperatorComboBox();
    /** Field binded to the value field. */
    protected Field<?> valueField;
    /** Button that delete the row when clicked. */
    protected Button delete = new Button();

    /** Empty field used to set an empty space when no value is needed. */
    protected TextField<String> emptyField = new TextField<String>();

    /** Constraint panel displaying this row. */
    protected final ConstraintPanel parentPanel;

    /**
     * Default constructor.
     * 
     * @param parentPanel
     *            Constraint panel displaying this row.
     * @param availableColumns
     *            Column list displayed in the column field.
     * @param constraint
     *            Constraint binded to the row.
     */
    public ConstraintRow(ConstraintPanel parentPanel, List<ItemModelData> availableColumns,
            ConstraintModelData constraint)
    {
        super();

        this.parentPanel = parentPanel;
        this.constraint = constraint;

        this.setLayout();
        this.initRemoveButton();
        this.initFields();
        this.setListeners();
        this.setBinders();

        List<PropertyModelData> properties = new ArrayList<PropertyModelData>();
        for (ItemModelData item : availableColumns)
        {
            String editor = item.getEditor();
            if (Util.isEmptyString(editor))
            {
                editor = item.getRenderer();
            }
            properties.add(new PropertyModelData(item.getId(), item.getDataIndex(), item.getName(), item.getType(),
                    editor, item.getItemType(), item.getListValues()));
        }
        this.propertyField.getStore().add(properties);
    }

    /**
     * @return The constraint binded to the row.
     */
    public ConstraintModelData getConstraint()
    {
        return constraint;
    }

    /**
     * Set the constraint binded to the row.
     * 
     * @param constraint
     *            The constraint binded to the row.
     */
    public void setConstraint(ConstraintModelData constraint)
    {
        this.constraint = constraint;
    }

    /**
     * Select property which name is equal to columnName.
     * 
     * @param columnName
     *            The name of the column to select.
     */
    public void updatePropertyField(String columnName)
    {
        PropertyModelData column = this.propertyField.getStore().findModel(PropertyModelData.NAME_FIELD, columnName);
        this.propertyField.setValue(column);

        this.constraint.setColumn(column);
        this.constraint.setColumnId(column.getItemId());
    }

    /**
     * Select operator which name is equal to <i>name</i>.
     * 
     * @param name
     *            The operator to select name.
     */
    public void updateOperatorField(String name)
    {
        OperatorModelData operator = this.operatorField.getStore().findModel(OperatorModelData.NAME_FIELD, name);
        this.operatorField.setValue(operator);
        this.constraint.setOperator(operator);
    }

    /**
     * Update value field with stringValue.
     * 
     * @param stringValue
     *            Update field value with a value given at string format.
     */
    public void updateValue(String stringValue)
    {
        if (stringValue != null && valueField != null)
        {
            this.updateValueFieldValue(propertyField.getValue(), stringValue);
        }
    }

    /**
     * Set layout (HBoxLayout).
     */
    protected void setLayout()
    {
        HBoxLayout layout = new HBoxLayout();
        layout.setHBoxLayoutAlign(HBoxLayoutAlign.BOTTOM);
        this.setLayout(new HBoxLayout());
        this.setHeight(ConstraintPanel.ROW_HEIGHT);
    }

    /**
     * Set icon and tool tip on remove button.
     */
    protected void initRemoveButton()
    {
        delete.setIcon(Hd3dImages.getDeleteIcon());
        delete.setToolTip(CONSTANTS.RemoveFilter());
    }

    /**
     * Initialize fields and delete button and add them to the panel.
     */
    protected void initFields()
    {
        HBoxLayoutData flex = new HBoxLayoutData(new Margins(0, 0, 0, 0));
        flex.setFlex(1);

        this.emptyField.disable();
        this.enabledField.setValue(true);
        LayoutContainer container = new LayoutContainer();
        container.setLayout(new FitLayout());
        container.add(enabledField);

        this.add(container, new HBoxLayoutData(new Margins(0, 5, 0, 5)));
        this.add(propertyField, new HBoxLayoutData(new Margins(0, 0, 0, 0)));
        this.add(operatorField, new HBoxLayoutData(new Margins(0, 0, 0, 0)));
        this.add(emptyField, flex);
        this.add(delete);
    }

    /**
     * Bind fields to the constraint model data represented by this row.
     */
    private void setBinders()
    {
        FieldBinding enableBinder = new FieldBinding(enabledField, ConstraintModelData.ACTIVE_FIELD);
        enableBinder.bind(this.constraint);
        FieldBinding propertyBinder = new FieldBinding(propertyField, ConstraintModelData.COLUMN_FIELD);
        propertyBinder.bind(this.constraint);
        FieldBinding operatorBinder = new FieldBinding(operatorField, ConstraintModelData.OPERATOR_FIELD);
        operatorBinder.bind(this.constraint);
    }

    /**
     * On column field, operator field and delete button.
     */
    protected void setListeners()
    {
        this.propertyField.addSelectionChangedListener(new SelectionChangedListener<PropertyModelData>() {
            @Override
            public void selectionChanged(SelectionChangedEvent<PropertyModelData> se)
            {
                onColumnChanged(se.getSelectedItem());
            }
        });

        this.operatorField.addSelectionChangedListener(new SelectionChangedListener<OperatorModelData>() {
            @Override
            public void selectionChanged(SelectionChangedEvent<OperatorModelData> se)
            {
                onOperatorChanged(se.getSelectedItem());
            }
        });

        delete.addSelectionListener(new SelectionListener<ButtonEvent>() {
            @Override
            public void componentSelected(ButtonEvent ce)
            {
                onRemoveClick();
            }
        });
    }

    /**
     * When column field changed, it updates operator combo content depending on column type, same for value field.
     * 
     * @param selectedItem
     *            The selected column.
     */
    protected void onColumnChanged(PropertyModelData selectedItem)
    {
        if (selectedItem == null || constraint == null)
            return;

        this.constraint.setOperator(null);
        this.constraint.setValue(null);
        this.constraint.setColumnId(selectedItem.getItemId());

        updateValueField(selectedItem, constraint.getStringValue());
        if (valueField != null)
        {
            valueField.disable();
        }
        operatorField.setStoreForColumn(selectedItem);
        operatorField.setValue(null);
    }

    /**
     * When operator changed, the value field is disabled if the selected operator is "is null" or "is not null".
     * 
     * @param operator
     *            The selected operator.
     */
    protected void onOperatorChanged(OperatorModelData operator)
    {
        if (valueField != null)
        {
            if (operator == null || operator.getOperator() == EConstraintOperator.isnull
                    || operator.getOperator() == EConstraintOperator.isnotnull)
            {
                valueField.disable();
            }
            else
            {
                valueField.enable();
                this.updateValueField(this.propertyField.getValue(), constraint.getStringValue());
            }
        }
    }

    /**
     * Update value field depending on selected column type.
     * 
     * @param column
     *            Selected column.
     */
    protected void updateValueField(PropertyModelData column, String stringValue)
    {
        HBoxLayoutData flex = new HBoxLayoutData(new Margins(0, 0, 0, 0));
        flex.setFlex(1);

        this.removeOldValueField();
        this.insertNewValueField(column, flex);
        this.bindValueField();

        this.layout();
    }

    private void insertNewValueField(PropertyModelData column, HBoxLayoutData layoutData)
    {
        if (column == null || parentPanel == null)
            return;

        final String type = column.getType();
        final String editor = column.getEditor();

        OperatorModelData operatorWrapper = this.operatorField.getValue();
        EConstraintOperator operator = null;
        if (operatorWrapper != null)
            operator = operatorWrapper.getOperator();

        final IEditorProvider editorProvider = this.parentPanel.getConstraintProvider();
        if (editorProvider != null && editorProvider.getEditor(editor, operator, column) != null)
        {
            this.valueField = editorProvider.getEditor(editor, operator, column);
        }
        else
        {
            this.valueField = FieldUtils.getField(type, operator);
        }

        if (valueField == null)
        {
            this.valueField = emptyField;
        }

        valueField.addKeyListener(new KeyListener() {
            @Override
            public void componentKeyUp(ComponentEvent event)
            {
                if (valueField instanceof TextField<?> && event.getKeyCode() == KeyCodes.KEY_ENTER)
                {
                    if (valueField instanceof AutoMultiModelCombo<?>)
                        constraint.setValue(((AutoMultiModelCombo<?>) valueField).getValues());
                    else
                        constraint.setValue(valueField.getValue());
                    EventDispatcher.forwardEvent(ConstraintEvents.APPLY_FILTER_CLICKED);
                }
            }
        });
        this.insert(valueField, VALUE_FIELD_INDEX, layoutData);
    }

    /**
     * Remove value field from the row, it value field is null, empty field is removed.
     */
    protected void removeOldValueField()
    {
        if (valueField == null)
        {
            this.remove(emptyField);
        }
        else
        {
            this.remove(valueField);
        }
    }

    /**
     * Bind value field to constraint model data represented by this row.
     */
    protected void bindValueField()
    {
        if (valueField != null)
        {
            ConstraintBinding valueBinder = new ConstraintBinding(valueField, ConstraintModelData.VALUE_FIELD,
                    parentPanel.getEditorStore());
            valueBinder.bind(this.constraint);
        }
    }

    /**
     * When remove button is clicked, the row is removed from the constraint editor.
     */
    protected void onRemoveClick()
    {
        this.parentPanel.removeRow(this);
    }

    protected void updateValueFieldValue(PropertyModelData column, String stringValue)
    {
        String type = column.getType();
        FieldUtils.updateValue(valueField, type, stringValue);
        constraint.setValue(valueField.getValue());
        constraint.setColumnId(column.getItemId());
    }
}
