package fr.hd3d.common.ui.client.widget.constraint.widget;

import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.util.Util;

import fr.hd3d.common.client.Constraint;
import fr.hd3d.common.client.ItemConstraint;
import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.technical.SheetFilterModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.widget.constraint.event.ConstraintEvents;
import fr.hd3d.common.ui.client.widget.constraint.model.ConstraintPanelModel;
import fr.hd3d.common.ui.client.widget.explorer.config.ExplorerConfig;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;


/**
 * Sheet filter controller handles sheet filter combo box events.
 * 
 * @author HD3D
 */
public class SheetFilterController extends MaskableController
{
    /** Model handles sheet filter combo box data. */
    private final SheetFilterModel model;
    /** Sheet filter combo box. */
    private final ISheetFilterView view;

    /**
     * Constructor.
     * 
     * @param model
     *            Model handles sheet filter combo box data.
     * @param view
     *            Sheet filter combo box.
     */
    public SheetFilterController(SheetFilterModel model, ISheetFilterView view)
    {
        this.model = model;
        this.view = view;

        this.registerEvents();
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType eventType = event.getType();
        if (eventType == ConstraintEvents.FILTER_SAVE_CLICKED)
        {
            this.onSaveFilterClicked(event);
        }
        else if (eventType == ConstraintEvents.FILTER_SAVED_SUCCESS)
        {
            this.onSaveFilterSuccess(event);
        }
        else if (eventType == ConstraintEvents.FILTER_RENAME_CLICKED)
        {
            this.onFilterRenameClicked(event);
        }
        else if (eventType == ConstraintEvents.FILTER_RENAMED)
        {
            this.onFilterRenamed(event);
        }
        else if (eventType == ConstraintEvents.FILTER_DELETE_CLICKED)
        {
            this.onFilterDeleteClicked(event);
        }
        else if (eventType == ExplorerEvents.NEW_USER_FILTER_SET)
        {
            this.onNewUserFilterSet(event);
        }
        this.forwardToChild(event);
    }

    /**
     * When save filter button is clicked, the save filter dialog is displayed.
     */
    private void onSaveFilterClicked(AppEvent event)
    {
        String constraint = event.getData();
        SheetModelData sheet = event.getData(ExplorerConfig.SHEET_EVENT_VAR_NAME);
        this.view.showSaveFilterDialog(constraint, sheet);
    }

    /**
     * When saving filter succeeds, filter store is updated by adding newly created filter. If it was an update, nothing
     * is done.
     * 
     * @param event
     *            The save filter success event.
     */
    private void onSaveFilterSuccess(AppEvent event)
    {
        SheetFilterModelData filter = event.getData();
        if (filter != null)
        {
            if (this.model.getFilterStore().findModel(SheetFilterModelData.ID_FIELD, filter.getId()) == null)
                this.model.getFilterStore().add(filter);
            this.view.setUpdateFilterRawValue(filter.getName());
        }
        else
        {
            Logger.warn("Saved filter is null");
        }
    }

    /**
     * Open a rename dialog to allow user to change the filter name.
     * 
     * @param event
     *            The rename filter clicked event.
     */
    private void onFilterRenameClicked(AppEvent event)
    {
        SheetFilterModelData filter = this.view.getSelectedFilter();
        if (filter != null)
        {
            this.view.displayRenameFilter(filter);
        }
    }

    /** When filter is renamed field is updated inside store. */
    private void onFilterRenamed(AppEvent event)
    {
        SheetFilterModelData filter = event.getData();
        if (filter != null && filter != this.model.getNoFilter())
        {
            this.model.getFilterStore().update(filter);
            this.view.setUpdateFilterRawValue(filter.getName());
        }
        else
        {
            Logger.warn("Renamed filter is null");
        }
    }

    /**
     * Delete currently selected filter and remove it from sheet filter combo.
     * 
     * @param event
     *            The delete filter clicked event.
     */
    private void onFilterDeleteClicked(AppEvent event)
    {
        SheetFilterModelData filter = this.view.getSelectedFilter();
        if (filter != null && filter != this.model.getNoFilter())
        {
            filter.delete();
            this.model.getFilterStore().remove(filter);
            this.view.setNoFilterSelected();
            this.view.removeHighlightFilterPanelButton();
        }
    }

    /**
     * When a new user filter is set on explorer, sheet filter combo box displays right indicator to tell user which
     * filter is set :
     * <ul>
     * <li>an empty string if the filter is empty</li>
     * <li>"Custom filter" string if filter does not correspond to an existing filter.</li>
     * <li>Filter name if filter corresponds to an existing filter.</li>
     * </ul>
     * 
     * @param event
     */
    private void onNewUserFilterSet(AppEvent event)
    {
        Boolean isSheetFilter = event.getData(ExplorerConfig.IS_SHEET_FILTER_EVENT_VAR_NAME);
        if (!(isSheetFilter != null && isSheetFilter))
        {
            List<Constraint> constraints = event.getData(ConstraintPanelModel.CONSTRAINTS);
            List<ItemConstraint> itemConstraints = event.getData(ConstraintPanelModel.ITEMCONSTRAINTS);
            String constraintString = event.getData(ExplorerConfig.CONSTRAINT_STRING_EVENT_VAR_NAME);

            if (CollectionUtils.isEmpty(constraints) && CollectionUtils.isEmpty(itemConstraints)
                    && Util.isEmptyString(constraintString))
            {
                this.view.setNoFilterSelected();
                this.view.removeHighlightFilterPanelButton();
            }
            else if (!Util.isEmptyString(constraintString))
            {
                boolean isSavedFilter = false;
                for (SheetFilterModelData filter : this.model.getFilterStore().getModels())
                {
                    if (constraintString.equals(filter.getFilter()))
                    {
                        this.view.setRawValue(filter.getName());
                        this.view.setValue(filter);
                        this.view.highlightFilterPanelButton();
                        isSavedFilter = true;
                    }
                }
                if (!isSavedFilter)
                {
                    this.view.setValue(null);
                    this.view.setCustomLabel();
                    this.view.highlightFilterPanelButton();
                }
            }
            else
            {
                this.view.setValue(null);
                this.view.setCustomLabel();
                this.view.highlightFilterPanelButton();
            }
        }
        else
        {
            String constraintString = event.getData(ExplorerConfig.CONSTRAINT_STRING_EVENT_VAR_NAME);
            if (Util.isEmptyString(constraintString))
                this.view.removeHighlightFilterPanelButton();
            else
                this.view.highlightFilterPanelButton();
        }
    }

    /**
     * Register event supported by the controller.
     */
    protected void registerEvents()
    {
        this.registerEventTypes(ConstraintEvents.FILTER_SAVE_CLICKED);
        this.registerEventTypes(ConstraintEvents.FILTER_SAVED_SUCCESS);
        this.registerEventTypes(ConstraintEvents.FILTER_RENAME_CLICKED);
        this.registerEventTypes(ConstraintEvents.FILTER_RENAMED);
        this.registerEventTypes(ConstraintEvents.FILTER_DELETE_CLICKED);

        this.registerEventTypes(ExplorerEvents.NEW_USER_FILTER_SET);
    }
}
