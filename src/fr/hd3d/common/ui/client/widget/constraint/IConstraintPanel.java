package fr.hd3d.common.ui.client.widget.constraint;

import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;


public interface IConstraintPanel
{
    public abstract void show();

    public abstract void hide();

    public abstract Long getCurrentSheetId();

    public abstract SheetModelData getCurrentSheet();

    public abstract boolean isHidden();

    public abstract IEditorProvider getConstraintProvider();
}
