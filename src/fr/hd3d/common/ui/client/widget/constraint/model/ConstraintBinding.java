package fr.hd3d.common.ui.client.widget.constraint.model;

import com.extjs.gxt.ui.client.binding.FieldBinding;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.form.Field;

import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.widget.AutoMultiModelCombo;
import fr.hd3d.common.ui.client.widget.MultiFieldComboBox;


public class ConstraintBinding extends FieldBinding
{
    public ConstraintBinding(Field<?> field, String property, ListStore<ConstraintModelData> store)
    {
        super(field, property);
        this.store = store;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void updateModel()
    {
        if (field instanceof AutoMultiModelCombo<?>)
        {
            Object value = ((AutoMultiModelCombo<?>) field).getValues();
            model.set(property, value);
            if (store != null)
            {
                this.store.update(model);
            }
            else
            {
                Logger.error("ConstraintBinding : No store is set. Cannot bind data to constraint field.");
            }
        }
        else if (field instanceof MultiFieldComboBox)
        {
            model.set(property, ((MultiFieldComboBox) field).getDirectValues());
            if (store != null)
            {
                this.store.update(model);
            }
            else
            {
                Logger.error("ConstraintBinding : No store is set. Cannot bind data to constraint field.");
            }
        }
        else
        {
            super.updateModel();
        }
    }
}
