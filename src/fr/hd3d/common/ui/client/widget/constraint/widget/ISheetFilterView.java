package fr.hd3d.common.ui.client.widget.constraint.widget;

import fr.hd3d.common.ui.client.modeldata.technical.SheetFilterModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;


/**
 * View interface for sheet filter combo box.
 * 
 * @author HD3D
 */
public interface ISheetFilterView
{
    public void setNoFilterSelected();

    public SheetFilterModelData getSelectedFilter();

    public void setUpdateFilterRawValue(String name);

    public void showSaveFilterDialog(String filterText, SheetModelData sheet);

    public void refreshFilterList(SheetModelData sheet);

    public void displayRenameFilter(SheetFilterModelData filter);

    public void setCustomLabel();

    public void setValue(SheetFilterModelData filter);

    public void setRawValue(String name);

    public void highlightFilterPanelButton();

    public void removeHighlightFilterPanelButton();
}
