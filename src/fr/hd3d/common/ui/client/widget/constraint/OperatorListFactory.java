package fr.hd3d.common.ui.client.widget.constraint;

import java.util.ArrayList;
import java.util.List;

import fr.hd3d.common.client.enums.EConstraintOperator;


/**
 * Small factory giving available constraint operators for a given type.
 * 
 * @author HD3D
 */
public class OperatorListFactory
{
    private static final String EQUAL = "equal";
    private static final String NOT_EQUAL = "not equal";
    private static final String LIKE = "like";
    private static final String NULL = "null";
    private static final String NOT_NULL = "not null";
    private static final String GREATER_OR_EQUAL = "greater or equal";
    private static final String LESSER_OR_EQUAL = "lesser or equal";
    private static final String GREATER_THAN = "greater than";
    private static final String LESSER_THAN = "lesser than";
    private static final String BETWEEN = "between";
    private static final String HAS_TAG = "has tag";
    private static final String HAS_NOT_TAG = "has not tag";
    private static final String IN = "in";

    private static List<OperatorModelData> booleanOperators = new ArrayList<OperatorModelData>();;
    private static List<OperatorModelData> enumOperators = new ArrayList<OperatorModelData>();;
    private static List<OperatorModelData> stringOperators = new ArrayList<OperatorModelData>();;
    private static List<OperatorModelData> longOperators = new ArrayList<OperatorModelData>();;
    private static List<OperatorModelData> dateOperators = new ArrayList<OperatorModelData>();;
    private static List<OperatorModelData> modelDataOperators = new ArrayList<OperatorModelData>();;
    private static List<OperatorModelData> tagOperators = new ArrayList<OperatorModelData>();;
    private static List<OperatorModelData> approvalNoteOperators = new ArrayList<OperatorModelData>();;

    public static List<OperatorModelData> getStringOperators()
    {
        if (stringOperators.isEmpty())
        {
            stringOperators.add(new OperatorModelData(EQUAL, EConstraintOperator.eq));
            stringOperators.add(new OperatorModelData(LIKE, EConstraintOperator.like));
            stringOperators.add(new OperatorModelData(NOT_EQUAL, EConstraintOperator.neq));
            stringOperators.add(new OperatorModelData(NULL, EConstraintOperator.isnull));
            stringOperators.add(new OperatorModelData(NOT_NULL, EConstraintOperator.isnotnull));
        }

        return stringOperators;
    }

    public static List<OperatorModelData> getBooleanOperators()
    {
        if (booleanOperators.isEmpty())
        {
            booleanOperators.add(new OperatorModelData(EQUAL, EConstraintOperator.eq));
            booleanOperators.add(new OperatorModelData(NOT_EQUAL, EConstraintOperator.neq));
            booleanOperators.add(new OperatorModelData(NULL, EConstraintOperator.isnull));
            booleanOperators.add(new OperatorModelData(NOT_NULL, EConstraintOperator.isnotnull));
        }

        return booleanOperators;
    }

    public static List<OperatorModelData> getModelDataOperators()
    {
        if (modelDataOperators.isEmpty())
        {
            modelDataOperators.add(new OperatorModelData(EQUAL, EConstraintOperator.eq));
            modelDataOperators.add(new OperatorModelData(NOT_EQUAL, EConstraintOperator.neq));
            modelDataOperators.add(new OperatorModelData(NULL, EConstraintOperator.isnull));
            modelDataOperators.add(new OperatorModelData(NOT_NULL, EConstraintOperator.isnotnull));
            modelDataOperators.add(new OperatorModelData(IN, EConstraintOperator.in));
        }

        return modelDataOperators;
    }

    public static List<OperatorModelData> getLongOperators()
    {
        if (longOperators.isEmpty())
        {
            longOperators.add(new OperatorModelData(EQUAL, EConstraintOperator.eq));
            longOperators.add(new OperatorModelData(NOT_EQUAL, EConstraintOperator.neq));
            longOperators.add(new OperatorModelData(GREATER_OR_EQUAL, EConstraintOperator.geq));
            longOperators.add(new OperatorModelData(LESSER_OR_EQUAL, EConstraintOperator.leq));
            longOperators.add(new OperatorModelData(GREATER_THAN, EConstraintOperator.gt));
            longOperators.add(new OperatorModelData(LESSER_THAN, EConstraintOperator.lt));
            longOperators.add(new OperatorModelData(BETWEEN, EConstraintOperator.btw));
            longOperators.add(new OperatorModelData(NULL, EConstraintOperator.isnull));
            longOperators.add(new OperatorModelData(NOT_NULL, EConstraintOperator.isnotnull));
        }

        return longOperators;
    }

    public static List<OperatorModelData> getDateOperators()
    {
        if (dateOperators.isEmpty())
        {
            dateOperators.add(new OperatorModelData(EQUAL, EConstraintOperator.eq));
            dateOperators.add(new OperatorModelData(NOT_EQUAL, EConstraintOperator.neq));
            dateOperators.add(new OperatorModelData(GREATER_OR_EQUAL, EConstraintOperator.geq));
            dateOperators.add(new OperatorModelData(LESSER_OR_EQUAL, EConstraintOperator.leq));
            dateOperators.add(new OperatorModelData(GREATER_THAN, EConstraintOperator.gt));
            dateOperators.add(new OperatorModelData(LESSER_THAN, EConstraintOperator.lt));
            dateOperators.add(new OperatorModelData(BETWEEN, EConstraintOperator.btw));
            dateOperators.add(new OperatorModelData(NULL, EConstraintOperator.isnull));
            dateOperators.add(new OperatorModelData(NOT_NULL, EConstraintOperator.isnotnull));
        }

        return dateOperators;
    }

    public static List<OperatorModelData> getTagOperators()
    {
        if (tagOperators.isEmpty())
        {
            tagOperators.add(new OperatorModelData(HAS_TAG, EConstraintOperator.hasTag));
            tagOperators.add(new OperatorModelData(HAS_NOT_TAG, EConstraintOperator.hasNotTag));
        }
        return tagOperators;
    }

    /**
     * @return Enumeration operator list :equal, not equal, null and not null.
     */
    public static List<? extends OperatorModelData> getEnumOperators()
    {
        if (enumOperators.isEmpty())
        {
            enumOperators.add(new OperatorModelData(EQUAL, EConstraintOperator.eq));
            enumOperators.add(new OperatorModelData(NOT_EQUAL, EConstraintOperator.neq));
            enumOperators.add(new OperatorModelData(NULL, EConstraintOperator.isnull));
            enumOperators.add(new OperatorModelData(NOT_NULL, EConstraintOperator.isnotnull));
            enumOperators.add(new OperatorModelData(IN, EConstraintOperator.in));
        }

        return enumOperators;
    }

    public static List<? extends OperatorModelData> getApprovalNoteOperators()
    {
        if (approvalNoteOperators.isEmpty())
        {
            approvalNoteOperators.add(new OperatorModelData(EQUAL, EConstraintOperator.eq));
            approvalNoteOperators.add(new OperatorModelData(NOT_EQUAL, EConstraintOperator.neq));
        }

        return approvalNoteOperators;
    }
}
