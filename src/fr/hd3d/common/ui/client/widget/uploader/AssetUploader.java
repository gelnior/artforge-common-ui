package fr.hd3d.common.ui.client.widget.uploader;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.FormEvent;
import com.extjs.gxt.ui.client.widget.form.HiddenField;

import fr.hd3d.common.ui.client.modeldata.asset.AssetRevisionModelData;


/**
 * Asset uploader is used to automatically connect a file revision to a specified asset. Asset id is provided through
 * the upload form. Services do the connection automatically on file reception.
 * 
 * @author HD3D
 */
public class AssetUploader extends FileUploader
{
    /** Field used to provide id of asset that will be linked file revision created for uploaded file */
    protected HiddenField<String> assetField = new HiddenField<String>();

    /** Default constructor : build dialog and set styles. */
    public AssetUploader(EventType event)
    {
        super(event);

        this.assetField.setName("assetId");
        this.panel.add(this.assetField);
    }

    /**
     * Show file uploader and configure it.
     * 
     * @param asset
     *            The asset to associate to the file.
     * @param variation
     *            The file variation.
     * @param revision
     *            The file revisions
     */
    public void show(AssetRevisionModelData asset, String variation, Integer revision)
    {
        this.revisionField.setValue(revision.toString());
        this.variationField.setValue(variation);
        this.assetField.setValue(asset.getId().toString());

        super.show();
    }

    @Override
    protected void onSubmitFinished(FormEvent be)
    {
        super.onSubmitFinished(be);
    }

}
