package fr.hd3d.common.ui.client.widget.uploader;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.FormEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.FileUploadField;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.form.HiddenField;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.form.FormPanel.Method;
import com.extjs.gxt.ui.client.widget.layout.AnchorLayout;
import com.extjs.gxt.ui.client.widget.layout.FormData;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.error.Hd3dException;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.asset.FileRevisionModelData;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.widget.SavingStatus;


/**
 * Dialog box that allows user to upload file to asset system. The file uploading is based on a form and file field.
 * 
 * @author HD3D
 */
public class FileUploader extends Dialog
{
    public static String FILE_VAR_FORM = "file";

    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    /** Services URL. */
    protected String servicesUrl = "";
    /** Status showing that upload is running. */
    protected SavingStatus savingStatus = new SavingStatus();

    /** Panel containing upload form. */
    protected final FormPanel panel = new FormPanel();
    /** Hidden field containing model id. */
    protected HiddenField<String> revisionField = new HiddenField<String>();

    protected final TextField<String> variationField = new TextField<String>();

    /** File field. */
    protected FileUploadField file = new FileUploadField();
    /** Button that submits upload form. */
    protected Button submit = new Button("submit");

    private final String title;
    /** Event to forward when upload is finished. */
    private EventType event;

    /**
     * Default constructor.
     */
    public FileUploader(EventType event)
    {
        this(event, "File Uploader");
    }

    /**
     * Default constructor.
     */
    public FileUploader(EventType event, String title)
    {
        this.event = event;
        this.title = title;

        this.setStyles();
        this.setButtonBar();
        this.createForm();
        this.setListener();
    }

    public EventType getEvent()
    {
        return event;
    }

    public void setEvent(EventType event)
    {
        this.event = event;
    }

    /**
     * This method allows to associate a file to the file uploader. So every uploaded file will be considered as a new
     * revision of set file.
     * 
     * @param fileRevisionId
     *            The file revision id.
     * @param variation
     *            The file variation.
     */
    public void setFile(Long fileRevisionId, String variation)
    {
        String path = ServicesPath.FILE_REVISIONS + ServicesPath.NEXT_REVISION + fileRevisionId + '/'
                + ServicesPath.UPLOAD;

        this.variationField.setValue(variation);
        this.setPath(path);
    }

    /** Set action path for form. */
    public void setPath(String path)
    {
        this.panel.setAction(this.servicesUrl + path);
    }

    /**
     * When dialog is shown, the file field is cleared.
     * 
     * @see com.extjs.gxt.ui.client.widget.BoxComponent#onShow()
     */
    @Override
    protected void onShow()
    {
        file.reset();
        super.onShow();
    }

    /**
     * @param ce
     * @throws Hd3dException
     */
    protected void onSubmitClicked(ButtonEvent ce) throws Hd3dException
    {
        if (this.file.getValue() == null || this.file.getValue().equals(""))
        {
            throw new Hd3dException("Please set a file.");
        }
        this.panel.submit();
        this.savingStatus.show();
    }

    /**
     * When submit is finished, the saving status is hidden, the dialog is hidden and the set event is raised.
     * 
     * @param be
     *            Base event raised by form signaling that submit is finished.
     */
    protected void onSubmitFinished(FormEvent be)
    {
        this.savingStatus.hide();
        hide();
        AppEvent appEvent = new AppEvent(event);
        appEvent.setData(be.getResultHtml().replaceAll("[^0-9]", ""));
        EventDispatcher.forwardEvent(appEvent);
    }

    /**
     * Set listeners on submit event : submit button clicking and submit finishing.
     */
    protected void setListener()
    {
        submit.addSelectionListener(new SelectionListener<ButtonEvent>() {
            @Override
            public void componentSelected(ButtonEvent ce)
            {
                try
                {
                    onSubmitClicked(ce);
                }
                catch (Hd3dException e)
                {
                    e.print();
                }
            }
        });

        panel.addListener(Events.Submit, new Listener<FormEvent>() {
            public void handleEvent(FormEvent be)
            {
                onSubmitFinished(be);
            }
        });
    }

    /**
     * Create the upload form.
     */
    protected void createForm()
    {
        this.panel.setBorders(false);
        this.panel.setBodyBorder(false);
        this.panel.setHeaderVisible(false);
        this.panel.setMethod(Method.POST);
        this.panel.setEncoding(FormPanel.Encoding.MULTIPART);

        this.servicesUrl = RestRequestHandlerSingleton.getInstance().getServicesUrl();
        this.panel.setAction(this.servicesUrl + ServicesPath.UPLOAD);

        this.file.setFieldLabel("File");
        this.file.setName(FILE_VAR_FORM);
        this.panel.add(this.file, new FormData("100%"));

        this.revisionField.setName(FileRevisionModelData.REVISION_FIELD);
        this.panel.add(this.revisionField);

        this.variationField.setFieldLabel("Variation");
        this.variationField.setName(FileRevisionModelData.VARIATION_FIELD);
        this.panel.add(this.variationField);

        this.add(panel);
    }

    /**
     * Add saving status and submit button ("OK") to the button bar.
     */
    protected void setButtonBar()
    {
        this.getButtonBar().add(savingStatus);
        this.getButtonBar().add(submit);
    }

    /**
     * Set editor styles : layout, size...
     */
    protected void setStyles()
    {
        this.setHeading(this.title);
        this.setSize(400, 140);
        this.setLayout(new AnchorLayout());
        this.setButtons("");
        this.setHideOnButtonClick(true);
        this.setModal(true);
    }

}
