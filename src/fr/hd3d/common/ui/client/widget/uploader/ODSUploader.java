package fr.hd3d.common.ui.client.widget.uploader;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.FormEvent;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.EventDispatcher;


public class ODSUploader extends FileUploader
{

    public ODSUploader(EventType event)
    {
        this(event, "ODS Uploader");
    }

    public ODSUploader(EventType event, String title)
    {
        super(event, title);
    }

    @Override
    protected void onSubmitFinished(FormEvent fe)
    {
        this.savingStatus.hide();
        hide();

        AppEvent mvcEvent = new AppEvent(this.getEvent());
        String fileName = "";

        // Warning, file name is not enough to retrieve right file revision.
        // It is better to get id from fe.getInHtml() and send it instead
        // of file name.
        if (file.getValue() != null)
        {
            String[] path = file.getValue().split("/");
            fileName = path[path.length - 1];

            mvcEvent.setData(fileName);
            EventDispatcher.forwardEvent(mvcEvent);
        }
    }
}
