package fr.hd3d.common.ui.client.widget;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.store.Store;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;


public class AutoSelectComboBox<M extends RecordModelData> extends ModelDataComboBox<M>
{

    public AutoSelectComboBox(IReader<M> reader)
    {
        super(reader);
    }

    class AutoComboStoreListener implements Listener<BaseEvent>
    {
        public void handleEvent(BaseEvent be)
        {
            autoSelect();
        }
    };

    @Override
    public void setStore(final ListStore<M> store)
    {
        super.setStore(store);
        bind();
    }

    private void bind()
    {
        AutoComboStoreListener listener = new AutoComboStoreListener();
        this.store.addListener(Store.Add, listener);
        this.store.addListener(Store.Remove, listener);
    }

    public void autoSelect()
    {
        if (store.getCount() == 1)
        {
            setValue(store.getAt(0));
        }
    }
}
