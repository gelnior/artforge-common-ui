package fr.hd3d.common.ui.client.widget;

import java.util.List;

import com.extjs.gxt.ui.client.event.LoadListener;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Timer;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.modeldata.reader.PersonReader;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.OrConstraint;
import fr.hd3d.common.ui.client.util.KeyBoardUtils;


public class AutoMultiPersonComboBox extends AutoMultiModelCombo<PersonModelData>
{
    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    private final Constraint loginConstraint = new Constraint(EConstraintOperator.like,
            PersonModelData.PERSON_LOGIN_FIELD, "%", null);
    private final Constraint firstNameConstraint = new Constraint(EConstraintOperator.like,
            PersonModelData.PERSON_FIRST_NAME_FIELD, "%", null);
    private final Constraint lastNameConstraint = new Constraint(EConstraintOperator.like,
            PersonModelData.PERSON_LAST_NAME_FIELD, "%", null);
    private final OrConstraint or = new OrConstraint(loginConstraint, new OrConstraint(firstNameConstraint,
            lastNameConstraint));

    public AutoMultiPersonComboBox()
    {
        super(new PersonReader());

        this.store.clearParameters();
        this.store.addParameter(or);
        this.store.addParameter(pagination);
        this.setOrderColumn(PersonModelData.PERSON_LAST_NAME_FIELD);
        this.setContextMenu();
    }

    /**
     * When key is up, the drop-down list is refreshed if max count is above MAX_RECORD_NUMBER or if text field is
     * empty.
     * 
     * @param keyCode
     *            The key typed by user.
     */
    @Override
    protected void onKeyUp(int keyCode)
    {
        if (this.isDelete)
        {
            this.values.remove(values.size() - 1);
            this.setValue();
            this.collapse();
            this.isDelete = false;
        }
        else if (KeyBoardUtils.isTextKey(keyCode))
        {
            if (!this.isTiming)
            {
                this.isTiming = true;
                Timer timer = new Timer() {
                    @Override
                    public void run()
                    {
                        isTiming = false;
                        loginConstraint.setLeftMember(getLastRawValue() + "%");
                        firstNameConstraint.setLeftMember(getLastRawValue() + "%");
                        lastNameConstraint.setLeftMember(getLastRawValue() + "%");

                        store.reload();
                        expand();
                    }
                };

                try
                {
                    timer.schedule(LOADING_DELAY);
                }
                catch (Exception e)
                {
                    this.isTiming = false;
                }
            }
        }
    }

    @Override
    public void setValueByIds(final List<Long> ids)
    {
        this.store.removeParameter(this.or);
        super.setValueByIds(ids);
    }

    @Override
    protected void onIdsLoaded(LoadListener listener)
    {
        this.getStore().getLoader().removeLoadListener(listener);

        this.values.clear();
        this.values.addAll(this.store.getModels());
        this.updateRawValue();

        this.store.addParameter(this.or);
        this.store.removeParameter(idConstraint);
    }

    private native String getFullNameTemplate() /*-{ 
                                                                                   return  [ 
                                                                                   '<tpl for=".">', 
                                                                                   '<div class="x-combo-list-item">{lastName}</div>', 
                                                                                   '</tpl>' 
                                                                                   ].join(""); 
                                                                                   }-*/;

}
