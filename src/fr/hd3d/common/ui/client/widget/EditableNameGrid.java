package fr.hd3d.common.ui.client.widget;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.util.GridUtils;
import fr.hd3d.common.ui.client.widget.grid.BaseEditorGrid;
import fr.hd3d.common.ui.client.widget.grid.MultiLineCellSelectionModel;
import fr.hd3d.common.ui.client.widget.grid.SelectableGridView;


/**
 * This grid will be used to display array fields.
 * 
 * @author HD3D
 */
public class EditableNameGrid<M extends RecordModelData> extends BaseEditorGrid<M>
{
    /** Constant strings to display : dialog messages, button label... */
    public static final CommonConstants CONSTANTS = GWT.create(CommonConstants.class);
    /** Parameterizable messages to display (like the counter label) */
    public static final CommonConstants MESSAGES = GWT.create(CommonConstants.class);

    /**
     * Default constructor.
     * 
     * @param store
     *            Store that contains data to display into the grid.
     */
    public EditableNameGrid(ServiceStore<M> store)
    {
        super(store, getNameGridColumnModel());

        this.setStyle();

        this.setSelectionModel(new MultiLineCellSelectionModel<M>((SelectableGridView) getView(), null));

        this.reconfigure(this.store, this.cm);
    }

    /**
     * Set grid columns : one for field names and one the field values.
     */
    public static ColumnModel getNameGridColumnModel()
    {
        final List<ColumnConfig> columns = new ArrayList<ColumnConfig>();
        ColumnConfig nameColumn = GridUtils.addColumnConfig(columns, RecordModelData.NAME_FIELD, CONSTANTS.Name(), 300);
        nameColumn.setResizable(true);
        nameColumn.setEditor(new CellEditor(new TextField<String>()));

        return new ColumnModel(columns);
    }

    /**
     * Set grid style : no headers, and size is forced to fit the parent panel.
     */
    private void setStyle()
    {
        this.setHideHeaders(true);
        this.setLoadMask(true);
        this.editSupport.setClicksToEdit(ClicksToEdit.TWO);
    }

    public void moveSelectedCells(int i)
    {
        ((MultiLineCellSelectionModel<M>) this.getSelectionModel()).moveSelectedCells(i);
    }
}
