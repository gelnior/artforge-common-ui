package fr.hd3d.common.ui.client.widget;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MenuEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.menu.Menu;
import com.extjs.gxt.ui.client.widget.menu.MenuItem;
import com.google.gwt.user.client.ui.AbstractImagePrototype;

import fr.hd3d.common.ui.client.listener.MenuButtonClickListener;
import fr.hd3d.common.ui.client.listener.MenuButtonLocalClickListener;
import fr.hd3d.common.ui.client.mvc.controller.CommandController;


/**
 * EasyMenu wraps GXT menu for easy item adding. It also adds a onBeforeShow method for overriding on before show
 * behavior.
 * 
 * @author HD3D
 */
public class EasyMenu extends Menu
{
    /**
     * Default constructor, it sets a before show event listener for overriding onBeforeShow behavior.
     */
    public EasyMenu()
    {
        this.setListeners();
    }

    /**
     * Add an item to the menu.
     * 
     * @param text
     *            Text to display on item.
     * @param event
     *            Event to raise on click.
     * @param icon
     *            Icon displayed before text.
     * 
     * @return The item added.
     */
    public MenuItem addItem(String text, final EventType event, AbstractImagePrototype icon)
    {
        MenuItem item = makeNewItem(text, event, icon);
        this.add(item);

        return item;
    }

    /**
     * Add an item to the menu.
     * 
     * @param text
     *            Text to display on item.
     * @param event
     *            Event to raise on click.
     * @param icon
     *            Icon displayed before text.
     * 
     * @return The item added.
     */
    public MenuItem addItem(String text, final AppEvent event, AbstractImagePrototype icon)
    {
        MenuItem item = makeNewItem(text, event, icon);
        this.add(item);

        return item;
    }

    /**
     * Tnsert an item to the menu at index <i>index</i>.
     * 
     * @param index
     *            Insertion index.
     * @param text
     *            Text to display on item.
     * @param event
     *            Event to raise on click.
     * @param icon
     *            Icon displayed before text.
     * 
     * @return The newly inserted item.
     */
    public MenuItem insertItem(int index, String text, final EventType event, AbstractImagePrototype icon)
    {
        MenuItem item = makeNewItem(text, event, icon);
        this.insert(item, index);

        return item;
    }

    /**
     * @param text
     *            Text to display on item.
     * @param event
     *            Event to raise on click.
     * @param icon
     *            Icon displayed before text.
     * 
     * @return The newly created item.
     */
    public MenuItem makeNewItem(String text, final EventType event, AbstractImagePrototype icon)
    {
        return this.makeNewItem(text, new MenuButtonClickListener(event), icon);
    }

    /**
     * @param text
     *            Text to display on item.
     * @param event
     *            Event to raise on click.
     * @param icon
     *            Icon displayed before text.
     * 
     * @return The newly created item.
     */
    public MenuItem makeNewItem(String text, final AppEvent event, AbstractImagePrototype icon)
    {
        return this.makeNewItem(text, new MenuButtonClickListener(event), icon);
    }

    public MenuItem makeNewItem(String text, SelectionListener<MenuEvent> listener, AbstractImagePrototype icon)
    {
        MenuItem item = new MenuItem(text);
        item.setIcon(icon);
        item.addSelectionListener(listener);

        return item;
    }

    /**
     * Set on Before show listener that point on onBeforeShow() method.
     */
    private void setListeners()
    {
        this.addListener(Events.BeforeShow, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                onBeforeShow(be);
            }
        });
    }

    /**
     * Before show method do nothing. It is only there for overriding purpose.
     * 
     * @param event
     *            Before show event.
     */
    protected void onBeforeShow(BaseEvent event)
    {}

    public MenuItem addLocalItem(CommandController controller, String text, EventType evenType,
            AbstractImagePrototype icon)
    {
        MenuItem item = this.makeNewItem(text, new MenuButtonLocalClickListener(controller, evenType), icon);
        this.add(item);

        return item;
    }
}
