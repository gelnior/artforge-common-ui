package fr.hd3d.common.ui.client.widget;

import com.extjs.gxt.ui.client.core.El;
import com.extjs.gxt.ui.client.event.ClickRepeaterEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.util.ClickRepeater;
import com.extjs.gxt.ui.client.widget.menu.Menu;
import com.google.gwt.dom.client.NodeList;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;


public class MenuFix extends Menu
{

    @Override
    protected void createScrollers()
    {
        NodeList<Element> nodes = el().select(".x-menu-scroller");
        if (nodes.getLength() == 0)
        {
            Listener<ClickRepeaterEvent> listener = new Listener<ClickRepeaterEvent>() {
                public void handleEvent(ClickRepeaterEvent be)
                {
                    onScroll(be);
                }
            };

            El scroller;

            scroller = new El(DOM.createDiv());
            scroller.addStyleName("x-menu-scroller", "x-menu-scroller-top");
            scroller.setInnerHtml("&nbsp;");
            ClickRepeater cr = new ClickRepeater(scroller);
            cr.doAttach();
            cr.addListener(Events.OnClick, listener);
            addAttachable(cr);

            el().insertFirst(scroller.dom);

            scroller = new El(DOM.createDiv());
            scroller.addStyleName("x-menu-scroller", "x-menu-scroller-bottom");
            scroller.setInnerHtml("&nbsp;");
            cr = new ClickRepeater(scroller);
            cr.doAttach();
            cr.addListener(Events.OnClick, listener);
            addAttachable(cr);

            el().appendChild(scroller.dom);
        }
        else
        {
            for (int i = 0; i < nodes.getLength(); i++)
            {
                fly(nodes.getItem(i)).show();
            }
        }
    }

}
