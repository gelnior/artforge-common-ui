package fr.hd3d.common.ui.client.widget.proxyplayer;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Widget;

import fr.hd3d.common.ui.client.modeldata.asset.ProxyModelData;


public class FlashPlayerWidget extends Widget implements IProxyPlayer
{

    private static final FlashPlayerImpl impl = GWT.create(FlashPlayerImpl.class);
    private final String[] paramNames = new String[] { "movie", "allowFullScreen" };
    private final String[] paramValues = new String[] { FlashPlayerImpl.PLAYER_URL_PARAM, "true" };

    public FlashPlayerWidget()
    {
        setElement(impl.createElement());
        this.setStyleName("html5player");
    }

    public FlashPlayerWidget(boolean debugMode, boolean flashVideo)
    {
        this();
        impl.setDebugMode(debugMode);
        impl.setFlashVideo(flashVideo);
    }

    @Override
    public void setPixelSize(int width, int height)
    {
        super.setPixelSize(width, height);
        impl.setPixelSize(width, height);
    }

    public void setSrc(String proxyUrl)
    {
        if (proxyUrl != null)
        {
            if (impl.isFlashVideo())
            {
                impl.setData(impl.convertUrl(proxyUrl));
            }
            else
            {
                impl.setData(proxyUrl);
            }
        }
        for (int i = 0; i < paramNames.length; i++)
        {
            String value = paramValues[i];
            if (FlashPlayerImpl.PLAYER_URL_PARAM.equals(value))
            {
                if (impl.isFlashVideo())
                {
                    impl.createAndAppendParam(this, paramNames[i], impl.convertUrl(proxyUrl));
                }
                else
                {
                    impl.createAndAppendParam(this, paramNames[i], proxyUrl);
                }
            }
            else
            {
                impl.createAndAppendParam(this, paramNames[i], value);
            }
        }
        if (impl.isFlashVideo())
        {
            impl.setSizeWithEmbedSWF();
        }
        impl.finish();
    }

    public void removeControllers()
    {}

    public void setPlayList(List<ProxyModelData> proxyModelDatas)
    {}

}
