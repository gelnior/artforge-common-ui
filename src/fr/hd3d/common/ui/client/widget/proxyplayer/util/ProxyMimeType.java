package fr.hd3d.common.ui.client.widget.proxyplayer.util;

import java.util.ArrayList;
import java.util.List;


public enum ProxyMimeType
{

    VIDEO_MP4
    {
        @Override
        public String getType()
        {
            return "video/mp4";
        }
    },
    VIDEO_OGG
    {
        @Override
        public String getType()
        {
            return "video/ogg";
        }
    },
    VIDEO_WEBM
    {
        @Override
        public String getType()
        {
            return "video/webm";
        }
    },
    IMAGE_GIF
    {
        @Override
        public String getType()
        {
            return "image/gif";
        }
    },
    IMAGE_JPEG
    {
        @Override
        public String getType()
        {
            return "image/jpeg";
        }
    },
    IMAGE_PNG
    {
        @Override
        public String getType()
        {
            return "image/png";
        }
    },
    SWF
    {
        @Override
        public String getType()
        {
            return "application/x-shockwave-flash";
        }
    };
    public abstract String getType();

    public static List<String> getTypes()
    {

        ProxyMimeType[] types = values();
        List<String> typeList = new ArrayList<String>(types.length);
        for (ProxyMimeType type : types)
        {
            typeList.add(type.getType());
        }
        return typeList;
    }

    /**
     * This does basically the same computation than {@link ProxyMimeType#getTypes()} but takes the UserAgent into
     * account. This handles following cases :
     * <ul>
     * <li>IE : leaves only {@link ProxyMimeType#VIDEO_MP4} as valid video format</li>
     * </ul>
     * 
     * @return List of String representation of available Proxy Mime types.
     */
    public static List<String> getAllowedTypes()
    {
        List<String> typeList = ProxyMimeType.getTypes();
        // On Internet Explorer, remove
        if ("ie".equals(getUserAgent()))
        {
            typeList.remove(ProxyMimeType.VIDEO_OGG);
            typeList.remove(ProxyMimeType.VIDEO_WEBM);
        }
        return typeList;
    }

    private static final native String getUserAgent() /*-{
                     var ua = navigator.userAgent.toLowerCase();
                     if (ua.indexOf("opera") != -1) {
                       return "opera";
                     } else if (ua.indexOf("webkit") != -1) {
                       return "safari";
                     } else if (ua.indexOf("msie") != -1) {
                           return "ie";
                     } else if (ua.indexOf("gecko") != -1) {
                       return "gecko";
                     }
                     return "unknown";
                   }-*/;

}
