package fr.hd3d.common.ui.client.widget.proxyplayer;

import java.util.List;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.MessageBox;

import fr.hd3d.common.ui.client.modeldata.asset.FileRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.asset.ProxyModelData;
import fr.hd3d.common.ui.client.service.callback.GetModelDataCallback;


public class ProxyPlayerDialog extends Dialog
{
    private static ProxyPlayerDialog instance;

    private ProxyPlayer player;
    private ProxyModelData proxy;

    public static ProxyPlayerDialog get()
    {
        return get((ProxyModelData) null);
    }

    public static ProxyPlayerDialog get(ProxyModelData proxy)
    {
        if (instance == null)
        {
            instance = new ProxyPlayerDialog();
        }
        if (instance.getProxy() != proxy)
        {
            instance.createPlayer(proxy);
        }
        return instance;
    }

    public static ProxyPlayerDialog get(List<Long> fileRevisionsId)
    {
        if (fileRevisionsId == null)
        {
            MessageBox.alert("Error", "No file", null);
            return get();
        }
        if (fileRevisionsId.size() == 1)
        {
            final FileRevisionModelData fileRevision = new FileRevisionModelData();
            fileRevision.setId(fileRevisionsId.get(0));
            fileRevision.refresh(new GetModelDataCallback(fileRevision) {
                @Override
                public void onSuccess(Request request, Response response)
                {
                    super.onSuccess(request, response);
                    if (fileRevision.getProxies().isEmpty())
                    {
                        MessageBox.alert("Warning", "This file there ar no proxy", null);
                        return;
                    }
                    final ProxyModelData proxy = new ProxyModelData();

                    proxy.setId(fileRevision.getProxies().get(0));
                    proxy.refresh(new GetModelDataCallback(proxy) {

                        @Override
                        public void onSuccess(Request request, Response response)
                        {
                            super.onSuccess(request, response);
                            get(proxy);
                        }
                    });

                }
            });
        }
        return get();
    }

    private ProxyPlayerDialog()
    {
        this.setButtons(Dialog.CLOSE);
        this.setPixelSize(415, 469);
        this.setHideOnButtonClick(true);
        this.setHeading("Displayer proxy");
    }

    private void createPlayer(ProxyModelData proxy)
    {
        if (proxy != null)
        {
            this.player = new ProxyPlayer(proxy);
        }
        else
        {
            this.player = new ProxyPlayer();
        }
        this.player.setPixelSize(400, 400);
        this.proxy = proxy;
        this.removeAll();
        this.add(player);
        this.layout(true);
    }

    public void setProxy(ProxyModelData proxy)
    {
        if (this.proxy != proxy)
        {
            createPlayer(proxy);
        }
    }

    public ProxyModelData getProxy()
    {
        return proxy;
    }

}
