package fr.hd3d.common.ui.client.widget.proxyplayer;

import java.util.List;

import fr.hd3d.common.ui.client.modeldata.asset.ProxyModelData;

public interface IProxyPlayer
{
    public void setSrc(String src);
    public void removeControllers();
    public void setPlayList(List<ProxyModelData> proxyModelDatas);
}
