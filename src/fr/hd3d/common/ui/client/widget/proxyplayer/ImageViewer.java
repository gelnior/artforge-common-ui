package fr.hd3d.common.ui.client.widget.proxyplayer;

import java.util.List;

import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.LoadEvent;
import com.google.gwt.event.dom.client.LoadHandler;
import com.google.gwt.user.client.ui.Image;

import fr.hd3d.common.ui.client.modeldata.asset.ProxyModelData;


public class ImageViewer extends Image implements IProxyPlayer
{

    public ImageViewer()
    {
        super();
        this.addLoadHandler(new LoadHandler() {

            public void onLoad(LoadEvent event)
            {
                Element element = event.getRelativeElement();
                if (element == ImageViewer.this.getElement())
                {
                    int originalHeight = ImageViewer.this.getOffsetHeight();
                    int originalWidth = ImageViewer.this.getOffsetWidth();
                    if (originalHeight > originalWidth)
                    {
                        ImageViewer.this.setWidth("");
                    }
                    else
                    {
                        ImageViewer.this.setHeight("");
                    }
                }
            }
        });
    }

    public void setSrc(String src)
    {
        this.setUrl(src);
    }

    public void removeControllers()
    {}

    public void setPlayList(List<ProxyModelData> proxyModelDatas)
    {}

}
