package fr.hd3d.common.ui.client.widget.proxyplayer;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.HTML;

import fr.hd3d.common.ui.client.constant.CommonMessages;
import fr.hd3d.common.ui.client.modeldata.asset.ProxyModelData;


public class DownloadProxy extends HTML implements IProxyPlayer
{

    /** Parameterizable messages to display (like the counter label) */
    public static final CommonMessages MESSAGES = GWT.create(CommonMessages.class);

    public DownloadProxy()
    {
        super();
        this.setWordWrap(true);
    }

    public void removeControllers()
    {}

    public void setPlayList(List<ProxyModelData> proxyModelDatas)
    {}

    public void setSrc(String src)
    {
        // this.setHTML(MESSAGES.downloadProxy(src));
    }

}
