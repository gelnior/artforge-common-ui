package fr.hd3d.common.ui.client.widget.proxyplayer;

import java.util.List;

import com.google.gwt.user.client.ui.FocusPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.hd3d.common.ui.client.modeldata.asset.ProxyModelData;
import fr.hd3d.common.ui.client.widget.proxyplayer.util.ProxyMimeType;
import fr.hd3d.html5.video.client.VideoWidget.TypeSupport;


public class ProxyPlayer extends FocusPanel
{
    private IProxyPlayer displayer;

    public ProxyPlayer()
    {
        this(new ProxyModelData());
    }

    public ProxyPlayer(List<ProxyModelData> proxyModelDatas)
    {
        if (proxyModelDatas != null && proxyModelDatas.size() > 0)
        {
            createPlayer(proxyModelDatas.get(0));
            setPlayList(proxyModelDatas);
        }
    }

    private ProxyModelData createFakeProxyModelData()
    {
        ProxyModelData proxyModelData = new ProxyModelData();
        proxyModelData.setProxyTypeOpenType(ProxyMimeType.VIDEO_MP4.getType());
        return proxyModelData;
    }

    public ProxyPlayer(ProxyModelData proxyModelData)
    {
        createPlayer(proxyModelData);
        setSource(proxyModelData);
    }

    private void createPlayer(ProxyModelData proxyModelData)
    {
        if (proxyModelData != null && proxyModelData.getProxyTypeOpenType() == null)
        {
            proxyModelData = createFakeProxyModelData();
        }
        if (isVideoType(proxyModelData))
        {
            try
            {
                displayer = new VideoPlayer(false, null, VideoPlayer.getDefaultResources());
                if (!isTypeSupported(proxyModelData))
                {
                    displayer = new FlashPlayerWidget(false, true);
                }
            }
            catch (Exception e)
            {
                displayer = new FlashPlayerWidget(false, true);
            }
        }
        else if (isImage(proxyModelData))
        {
            displayer = new ImageViewer();
        }
        else if (isFlash(proxyModelData))
        {
            displayer = new FlashPlayerWidget(false, false);
        }
        else
        {
            displayer = new DownloadProxy();
        }

    }

    public void setPlayList(List<ProxyModelData> proxyModelDatas)
    {
        if (displayer != null && displayer instanceof Widget)
        {
            displayer.setPlayList(proxyModelDatas);
            this.setWidget((Widget) displayer);
        }
    }

    private void setSource(ProxyModelData proxyModelData)
    {
        if (displayer != null && displayer instanceof Widget)
        {
            if (proxyModelData.getDownloadPath() != null)
            {
                displayer.setSrc(proxyModelData.getDownloadPath());
            }
            this.setWidget((Widget) displayer);
        }
    }

    private boolean isFlash(ProxyModelData proxyModelData)
    {
        return ProxyMimeType.SWF.getType().equalsIgnoreCase(proxyModelData.getProxyTypeOpenType());
    }

    private boolean isTypeSupported(ProxyModelData proxyModelData)
    {
        TypeSupport canPlayType = ((VideoPlayer) displayer).getVideoHTML().canPlayType(
                proxyModelData.getProxyTypeOpenType());
        return !TypeSupport.NO.equals(canPlayType);
    }

    @Override
    public void setPixelSize(int width, int height)
    {
        super.setPixelSize(width, height);
        if (displayer != null && displayer instanceof Widget)
        {
            ((Widget) displayer).setPixelSize(width, height);
        }
    }

    private boolean isImage(ProxyModelData proxyModelData)
    {
        return ProxyMimeType.IMAGE_GIF.getType().equalsIgnoreCase(proxyModelData.getProxyTypeOpenType())
                || ProxyMimeType.IMAGE_JPEG.getType().equalsIgnoreCase(proxyModelData.getProxyTypeOpenType())
                || ProxyMimeType.IMAGE_PNG.getType().equalsIgnoreCase(proxyModelData.getProxyTypeOpenType());

    }

    private boolean isVideoType(ProxyModelData proxyModelData)
    {
        return ProxyMimeType.VIDEO_MP4.getType().equalsIgnoreCase(proxyModelData.getProxyTypeOpenType())
                || ProxyMimeType.VIDEO_OGG.getType().equalsIgnoreCase(proxyModelData.getProxyTypeOpenType())
                || ProxyMimeType.VIDEO_WEBM.getType().equalsIgnoreCase(proxyModelData.getProxyTypeOpenType());
    }

}
