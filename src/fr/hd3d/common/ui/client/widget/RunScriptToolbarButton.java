package fr.hd3d.common.ui.client.widget;

import java.io.IOException;

import org.restlet.client.Request;
import org.restlet.client.Response;
import org.restlet.client.data.Status;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.button.Button;

import fr.hd3d.common.client.ScriptMessageGenerator;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;


/**
 * A tool bar button that launches a given script. Script name is given to button. When button is clicked, it sends a
 * GET request to the service that runs the script.
 * 
 * @author HD3D
 */
public class RunScriptToolbarButton extends Button
{
    public static final String PROJECT_ID_PARAM_NAME = "projectId";
    public static final String PERSON_ID_PARAM_NAME = "personId";

    /** The displayed name of the button. */
    private String name;
    /** The script name (must be equal to script registered on server). */
    private String scriptName;

    /**
     * Default constructor : set click listener.
     * 
     * @param name
     *            The displayed name of the button.
     * @param scriptName
     *            The script name (must be equal to script registered on server).
     */
    public RunScriptToolbarButton(String name, String scriptName)
    {
        super(name);
        this.name = name;
        this.scriptName = scriptName;
        addListener();
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getScriptName()
    {
        return scriptName;
    }

    public void setScriptName(String scriptName)
    {
        this.scriptName = scriptName;
    }

    /**
     * Add click listener that runs script when button is clicked.
     */
    private void addListener()
    {
        this.addSelectionListener(new SelectionListener<ButtonEvent>() {
            @Override
            public void componentSelected(ButtonEvent ce)
            {
                String params = createParams();
                String url = ServicesPath.SCRIPTS + scriptName + "?" + params;
                EventDispatcher.forwardEvent(CommonEvents.SCRIPT_EXECUTION_STARTED);
                RestRequestHandlerSingleton.getInstance().getRequest(url, new BaseCallback() {

                    @Override
                    protected void onSuccess(Request request, Response response)
                    {
                        ScriptMessageGenerator errorMessageGenerator = null;
                        try
                        {
                            if (Status.SUCCESS_OK.equals(response.getStatus()))
                            {
                                EventDispatcher.forwardEvent(CommonEvents.SCRIPT_EXECUTION_FINISHED, response
                                        .getEntity().getText());
                            }
                            else if (Status.SUCCESS_NO_CONTENT.equals(response.getStatus()))
                            {
                                errorMessageGenerator = new ScriptMessageGenerator(1, "No script found", "");
                                EventDispatcher.forwardEvent(CommonEvents.SCRIPT_EXECUTION_FINISHED,
                                        errorMessageGenerator.errorMessageToJsonString());
                            }
                        }
                        catch (IOException e)
                        {
                            errorMessageGenerator = new ScriptMessageGenerator(0, "Script finished succefully", "");
                            EventDispatcher.forwardEvent(CommonEvents.SCRIPT_EXECUTION_FINISHED, errorMessageGenerator
                                    .errorMessageToJsonString());
                        }
                    }
                });
            };
        });
    }

    /**
     * @return Parameters forwarded to the ran script.
     */
    protected String createParams()
    {
        String params = "params=[{";
        if (MainModel.currentProject != null)
        {
            params += "'" + PROJECT_ID_PARAM_NAME + "':" + MainModel.currentProject.getId();
        }
        if (MainModel.currentUser != null)
        {
            if (MainModel.currentProject != null)
            {
                params += ",";
            }
            params += "'" + PERSON_ID_PARAM_NAME + "':" + MainModel.currentUser.getId();
        }
        params += "}]";
        return params;
    }

}
