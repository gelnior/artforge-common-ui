package fr.hd3d.common.ui.client.widget;

public class SecondField extends FieldComboBox
{
    /**
     * Default constructor.
     */
    public SecondField()
    {
        super();
        this.setWidth(50);

        for (int i = 0; i < 60; i++)
            this.addField(i, i + "s", Integer.valueOf(i));
    }
}
