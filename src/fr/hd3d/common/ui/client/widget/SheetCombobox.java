package fr.hd3d.common.ui.client.widget;

import java.util.ArrayList;
import java.util.List;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.client.enums.ESheetType;
import fr.hd3d.common.ui.client.cookie.FavoriteCookie;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.AndConstraint;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.EqConstraint;
import fr.hd3d.common.ui.client.service.parameter.InConstraint;
import fr.hd3d.common.ui.client.service.parameter.LogicConstraint;
import fr.hd3d.common.ui.client.service.parameter.OrConstraint;
import fr.hd3d.common.ui.client.widget.explorer.model.reader.SheetReader;


public class SheetCombobox extends ModelDataComboBox<SheetModelData>
{
    private final LogicConstraint rootConstraint = new AndConstraint();
    private final LogicConstraint projectRootConstraint = new OrConstraint();
    private final Constraint projectConstraint = new EqConstraint("project.id");

    /** And constraint for Sheet data : type and entity. */
    private final LogicConstraint sheetRootConstraints = new AndConstraint();
    /** Equal constraint for setting sheet type : PRODUCTION, BREAKDOWN... If no type is set this constraint is ignored. */
    private final Constraint typeConstraint = new EqConstraint(SheetModelData.TYPE_FIELD);
    /** In constraint for setting entity to retrieve like : Shot, Constituent... */
    private final Constraint entityConstraint = new InConstraint(SheetModelData.BOUND_CLASS_NAME_FIELD,
            new ArrayList<String>());

    private String cookieKey;
    /**
     * GXT by default reload combo box when user click on it. To not reselect automatically the last selected sheet
     * (expected behavior when app asks for reloading), we use this variable to select last selected sheet only if
     * reloading comes from application and not from GXT.
     */
    private boolean firstLoad = true;

    public SheetCombobox()
    {
        super(new SheetReader());

        this.setConstraints();
        // this.setlisteners();
        this.setContextMenu();
    }

    public SheetCombobox(String entity)
    {
        super(new SheetReader());
        this.addAvailableEntity(entity);
    }

    public String getCookieKey()
    {
        return this.cookieKey;
    }

    public void setCookieKey(String cookieKey)
    {
        this.cookieKey = cookieKey;
    }

    public void selectFirstSheet()
    {
        if (store.getCount() > 0)
        {
            this.setValue(store.getAt(0));
        }
        else
        {
            this.setValue(null);
        }
    }

    public void reload()
    {
        this.firstLoad = true;
        this.setData();
    }

    @SuppressWarnings("unchecked")
    public void addAvailableEntity(String entity)
    {
        List<String> entities = (List<String>) entityConstraint.getLeftMember();
        entities.add(entity);
    }

    public void setProject(ProjectModelData project)
    {
        if (project != null)
        {
            String path = project.getDefaultPath();
            if (!path.endsWith("/"))
                path += "/";
            this.getServiceStore().setPath(path + ServicesPath.SHEETS);
            projectConstraint.setOperator(EConstraintOperator.eq);
            projectConstraint.setLeftMember(project.getId());
        }
        else
        {
            this.getServiceStore().setPath(ServicesPath.SHEETS);
            projectConstraint.setOperator(EConstraintOperator.isnull);
            projectConstraint.setLeftMember(null);
        }
    }

    @Override
    protected void onLoaderLoad()
    {
        super.onLoaderLoad();

        if (firstLoad)
        {
            selectLastSelectedSheet();
            firstLoad = false;
        }
    }

    /**
     * Select the sheet user select last time he used the application. Due to the fact that sheet combo box is used in a
     * lot of different application, cookieKey must be set by application before data loading.
     */
    public void selectLastSelectedSheet()
    {
        String sheetId = FavoriteCookie.getFavParameterValue(cookieKey);
        SheetModelData sheet = null;
        if (sheetId != null)
        {
            Long id = Long.parseLong(sheetId);
            sheet = this.getStore().findModel(SheetModelData.ID_FIELD, id);
        }

        if (sheet != null)
        {
            this.setValue(sheet);
        }
        else
        {
            this.selectFirstSheet();
        }
    }

    private void setConstraints()
    {
        projectRootConstraint.setLeftMember(new Constraint(EConstraintOperator.isnull, "project.id"));
        projectRootConstraint.setRightMember(projectConstraint);

        sheetRootConstraints.setLeftMember(entityConstraint);

        rootConstraint.setLeftMember(sheetRootConstraints);
        rootConstraint.setRightMember(projectRootConstraint);
        this.store.addParameter(rootConstraint);
    }

    public void remove(SheetModelData sheet)
    {
        if (sheet.equals(getValue()))
        {
            setValue(null);
        }
        this.getStore().remove(sheet);
    }

    /**
     * Add a constraint parameter to data store to make it retrieving only sheet of the selected type.
     * 
     * @param type
     */
    public void setType(ESheetType type)
    {
        sheetRootConstraints.setRightMember(typeConstraint);
        typeConstraint.setLeftMember(type.toString());
    }

    public void setIsFirstLoad(boolean b)
    {
        firstLoad = b;
    }

}
