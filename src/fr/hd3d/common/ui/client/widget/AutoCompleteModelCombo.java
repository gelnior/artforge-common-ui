package fr.hd3d.common.ui.client.widget;

import java.util.List;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.KeyListener;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.LoadListener;
import com.extjs.gxt.ui.client.store.Store;
import com.extjs.gxt.ui.client.store.StoreFilter;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.user.client.Timer;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.Pagination;
import fr.hd3d.common.ui.client.util.KeyBoardUtils;


/**
 * Text field with auto completion based on the model data name. A drop down list is available for name selection.
 * 
 * @author HD3D
 * 
 * @param <M>
 *            Type of model data to retrieve in combo box.
 */
public class AutoCompleteModelCombo<M extends RecordModelData> extends ModelDataComboBox<M>
{
    /** Maximum number of lines displayed in the drop-down list. */
    public static final Long MAX_RECORD_NUMBER = 40L;
    /** Delay between moment user type model and time box is reloaded. */
    public static final Integer LOADING_DELAY = 750;

    /** Constraint needed for name filtering. */
    protected Constraint likeConstraint = new Constraint(EConstraintOperator.like, RecordModelData.NAME_FIELD, "%", null);
    /** Constraint needed for id filtering when value should be initialized. */
    protected Constraint idConstraint = new Constraint(EConstraintOperator.eq, RecordModelData.ID_FIELD, 0L, null);
    /** Pagination parameter to limit number of displayed lines. */
    protected Pagination pagination = new Pagination(0L, MAX_RECORD_NUMBER + 1);

    protected StoreFilter<M> storeFilter = new StoreFilter<M>() {
        public boolean select(Store<M> store, M parent, M item, String property)
        {
            return onStoreFilter(store, parent, item, property);
        }
    };

    protected M continuedRecord;

    /** define if we add a empty field in the combobox. */
    private boolean emptyField;

    private M emptyRecord;

    protected Boolean isTiming = Boolean.FALSE;

    /**
     * Default constructor : set parameters, store and styles.
     * 
     * @param reader
     *            The reader needed to parse data from services. Should be a reader adapted for M parsing.
     */
    public AutoCompleteModelCombo(IReader<M> reader)
    {
        this(reader, false);
    }

    /**
     * Default constructor : set parameters, store and styles.
     * 
     * @param reader
     *            The reader needed to parse data from services. Should be a reader adapted for M parsing.
     * @param emptyField
     *            if true add a empty field in the combobox.
     */
    public AutoCompleteModelCombo(IReader<M> reader, boolean emptyField)
    {
        this(reader, emptyField, " ");
    }

    public AutoCompleteModelCombo(IReader<M> reader, boolean emptyField, String emptyFieldName)
    {
        super(reader);

        this.continuedRecord = reader.newModelInstance();
        this.continuedRecord.setName("........");

        this.setProxyParameters();
        this.setStyles();
        this.setListeners();

        this.emptyField = emptyField;
        if (emptyField)
        {
            this.emptyRecord = reader.newModelInstance();
            this.emptyRecord.setName(emptyFieldName);
        }
    }

    /**
     * Set the column on which to filter.
     * 
     * @param column
     *            The column to set.
     */
    public void setFilterColumn(String column)
    {
        this.likeConstraint.setColumn(column);
    }

    /**
     * Set proxy parameters on store proxy.
     */
    protected void setProxyParameters()
    {
        this.store.addParameter(pagination);
        this.store.addParameter(likeConstraint);
    }

    /**
     * Set combo styles.
     */
    @Override
    protected void setStyles()
    {
        this.setDisplayField(RecordModelData.NAME_FIELD);
        this.setSelectOnFocus(false);
        this.setTypeAhead(false);
        this.setForceSelection(false);
        this.setHideTrigger(false);
        this.setEditable(true);
    }

    @Override
    protected void onLoaderLoad()
    {
        if (store.getCount() == 0)
        {
            // this.setRawValue("No data");
            // this.setSelectionRange(0, 10);
        }
        else if (store.getCount() > MAX_RECORD_NUMBER)
        {
            store.add(continuedRecord);
        }
        if (emptyField)
        {
            store.insert(emptyRecord, 0);
        }
    }

    /**
     * Add key up listener.
     */
    protected void setListeners()
    {
        this.addListener(Events.KeyUp, new KeyListener() {
            @Override
            public void componentKeyUp(ComponentEvent event)
            {
                onKeyUp(event.getKeyCode());
            }
        });

        Listener<BaseEvent> selectionChangeListener = new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                List<M> list = getSelection();
                list.remove(continuedRecord);
                setSelection(list);
            }
        };

        this.addListener(Events.SelectionChange, selectionChangeListener);
    }

    /**
     * When key is up, the drop-down list is refreshed if max count is above MAX_RECORD_NUMBER or if text field is
     * empty.
     * 
     * @param keyCode
     *            The key typed by user.
     */
    protected void onKeyUp(int keyCode)
    {
        if (KeyBoardUtils.isTextKey(keyCode) || keyCode == KeyCodes.KEY_BACKSPACE)
        {
            if (store.getCount() > MAX_RECORD_NUMBER || getRawValue().length() < 2)
            {
                if (!this.isTiming)
                {
                    this.isTiming = true;
                    Timer timer = new Timer() {
                        @Override
                        public void run()
                        {
                            isTiming = false;
                            likeConstraint.setLeftMember(getLike());
                            store.reload();
                            store.applyFilters(likeConstraint.getColumn());
                            expand();
                        }
                    };

                    try
                    {
                        timer.schedule(LOADING_DELAY);
                    }
                    catch (Exception e)
                    {
                        this.isTiming = false;
                    }
                }
            }
            else
            {
                this.store.addFilter(storeFilter);
                this.store.applyFilters(likeConstraint.getColumn());
                this.expand();
            }
        }
    }

    protected String getLike()
    {
        return getRawValue() + "%";
    }

    /**
     * @param store
     *            The store where the item is stored.
     * @param parent
     *            The item parent.
     * @param item
     *            The item from which name is compared.
     * @param property
     *            The property name of name field (last name for person, label for constituent...).
     * @return true if compared string are equal (not case sensitive), false either.
     */
    protected boolean onStoreFilter(Store<M> store, M parent, M item, String property)
    {
        String name = item.get(property);
        if (name == null)
        {
            name = item.get(RecordModelData.NAME_FIELD);
        }

        boolean isOk = false;
        isOk = getRawValue() != null && name != null && name.toLowerCase().contains(getRawValue().toLowerCase());
        return isOk;
    }

    /**
     * Initialize a value with the model data of ID equal to <i>id</i> by loading the model data from services.
     * 
     * @param id
     *            The id of the model data to set as value.
     */
    @Override
    public void setValueById(final Long id)
    {
        this.idConstraint.setLeftMember(id);
        this.store.removeParameter(likeConstraint);
        this.store.removeParameter(pagination);
        this.store.addParameter(idConstraint);

        super.setValueById(id);
    }

    /**
     * After value initialization, when model data is loaded, constraint used to filter on id is removed and name
     * constraint is restored.
     */
    @Override
    protected void onIdLoad(LoadListener listener, Long id)
    {
        super.onIdLoad(listener, id);
        this.store.addParameter(likeConstraint);
        this.store.addParameter(pagination);
        this.store.removeParameter(idConstraint);
    }
}
