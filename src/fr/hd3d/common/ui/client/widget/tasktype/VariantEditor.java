package fr.hd3d.common.ui.client.widget.tasktype;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.util.Util;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.grid.CheckBoxSelectionModel;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;

import fr.hd3d.common.ui.client.error.Hd3dException;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.reader.TaskTypeReader;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.service.parameter.IsNullConstraint;
import fr.hd3d.common.ui.client.service.parameter.OrderBy;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.util.GridUtils;
import fr.hd3d.common.ui.client.widget.dialog.FormDialog;


public class VariantEditor extends FormDialog
{
    protected TextField<String> variantField = new TextField<String>();

    protected ServiceStore<TaskTypeModelData> taskTypeStore = new ServiceStore<TaskTypeModelData>(new TaskTypeReader());
    final CheckBoxSelectionModel<TaskTypeModelData> sm = new CheckBoxSelectionModel<TaskTypeModelData>();

    public VariantEditor()
    {
        super(CommonEvents.CREATE_STEP_VARIANTS_CONFIRMED, OKCANCEL);
        this.setHeading("Step variants creation");

        this.setVariantField();
        this.setTaskTypeGrid();
        this.configureStore();
        this.setSize(325, 500);
        this.setResizable(false);
    }

    @Override
    public void show()
    {
        this.taskTypeStore.reload();
        this.variantField.clear();
        this.okButton.disable();

        super.show();
    }

    private void configureStore()
    {
        this.taskTypeStore.addParameter(new IsNullConstraint("project"));
        this.taskTypeStore.addParameter(new OrderBy("name"));
    }

    private void setTaskTypeGrid()
    {
        Grid<TaskTypeModelData> taskTypeGrid = new Grid<TaskTypeModelData>(this.taskTypeStore, this.getColumnModel(sm
                .getColumn()));
        taskTypeGrid.setSelectionModel(sm);
        taskTypeGrid.addPlugin(sm);
        taskTypeGrid.setSize(300, 380);
        taskTypeGrid.setBorders(true);
        taskTypeGrid.setStyleAttribute("margin-left", "5px");
        taskTypeGrid.setAutoExpandColumn(TaskTypeModelData.NAME_FIELD);
        this.add(taskTypeGrid);
    }

    private ColumnModel getColumnModel(ColumnConfig checkBoxColumnConfig)
    {
        List<ColumnConfig> columns = new ArrayList<ColumnConfig>();

        columns.add(checkBoxColumnConfig);
        ColumnConfig config = GridUtils.addColumnConfig(columns, TaskTypeModelData.NAME_FIELD, "Task type");
        config.setRenderer(new TaskTypeNameRenderer());

        return new ColumnModel(columns);
    }

    private void setVariantField()
    {
        variantField.setFieldLabel("Variant");
        this.addKeyField(variantField);
    }

    @Override
    protected void onFieldChanged()
    {
        if (Util.isEmptyString(variantField.getValue()))
        {
            this.okButton.disable();
        }
        else
        {
            this.okButton.enable();
        }
    }

    @Override
    protected void onOkClicked() throws Hd3dException
    {
        AppEvent event = new AppEvent(CommonEvents.CREATE_STEP_VARIANTS_CONFIRMED);
        event.setData("taskTypes", this.sm.getSelection());
        event.setData("variant", this.variantField.getValue());

        EventDispatcher.forwardEvent(event);
        this.hide();
    }
}
