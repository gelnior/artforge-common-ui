package fr.hd3d.common.ui.client.widget.tasktype;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.service.parameter.EqConstraint;
import fr.hd3d.common.ui.client.service.parameter.IsNullConstraint;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.util.GridUtils;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.ColorRenderer;
import fr.hd3d.common.ui.client.widget.grid.editor.FieldComboBoxEditor;
import fr.hd3d.common.ui.client.widget.simpleexplorer.SimpleExplorerDialog;


/**
 * Dialog containing a simple event editor (CRUD operations).
 * 
 * @author HD3D
 */
public class TaskTypeEditor extends SimpleExplorerDialog<TaskTypeModelData>
{

    protected EqConstraint projectConstraint = new EqConstraint(ProjectModelData.SIMPLE_CLASS_NAME.toLowerCase()
            + FieldUtils.ID_SUFFIX);
    protected IsNullConstraint nullProjectConstraint = new IsNullConstraint(
            ProjectModelData.SIMPLE_CLASS_NAME.toLowerCase() + FieldUtils.ID_SUFFIX);

    protected Boolean isVariant = Boolean.FALSE;

    /** Default constructor : set styles and configure simple explorer. */
    public TaskTypeEditor()
    {
        this(Boolean.FALSE);
    }

    public TaskTypeEditor(Boolean isVariant)
    {
        super();

        this.isVariant = isVariant;

        this.explorer = new TaskTypeExplorerPanel(TaskTypeEditor.getColumnConfigs(), isVariant);
        this.setStyles();
        this.setExplorer(TaskTypeEditor.getColumnConfigs());
        this.explorer.getStore().addParameter(nullProjectConstraint);

        this.setHeading("Step editor");

        this.setWidth(550);
    }

    /**
     * Set store parameters to retrieve only task type from selected project.
     * 
     * @param project
     *            The project of which specific task types are requested.
     */
    public void setProject(ProjectModelData project)
    {
        if (!(this.explorer.getStore()).containsParameter(projectConstraint))
        {
            this.explorer.getStore().addParameter(projectConstraint);
            this.explorer.getStore().removeParameter(nullProjectConstraint);
        }

        if (project != null)
        {
            this.projectConstraint.setLeftMember(project.getId());
            ((TaskTypeExplorerPanel) this.explorer).setProject(project);
        }
    }

    @Override
    public void show()
    {
        super.show();
        EventDispatcher.forwardEvent(CommonEvents.TASK_TYPE_EDITOR_SHOWN);
    }

    @Override
    public void hide()
    {
        super.hide();
        EventDispatcher.forwardEvent(CommonEvents.TASK_TYPE_EDITOR_HIDDEN);
    }

    /**
     * @return Column configuration of simple explorer.
     */
    private static List<ColumnConfig> getColumnConfigs()
    {
        List<ColumnConfig> columnConfigs = new ArrayList<ColumnConfig>();

        ColumnConfig ccTitle = new ColumnConfig();
        ccTitle.setHeader("Name");
        ccTitle.setId(TaskTypeModelData.NAME_FIELD);
        ccTitle.setDataIndex(TaskTypeModelData.NAME_FIELD);
        ccTitle.setWidth(120);
        ccTitle.setEditor(new CellEditor(new TextField<String>()));
        ccTitle.setResizable(false);
        columnConfigs.add(ccTitle);

        ColumnConfig ccEntity = GridUtils.addColumnConfig(columnConfigs, TaskTypeModelData.ENTITY_NAME_FIELD, "Entity");
        ccEntity.setWidth(100);
        ccEntity.setEditor(new FieldComboBoxEditor(new EntityComboBox()));
        ccEntity.setResizable(false);

        ColumnConfig ccDescription = new ColumnConfig();
        ccDescription.setHeader("Description");
        ccDescription.setId(TaskTypeModelData.DESCRIPTION_FIELD);
        ccDescription.setDataIndex(TaskTypeModelData.DESCRIPTION_FIELD);
        ccDescription.setWidth(180);
        ccDescription.setEditor(new CellEditor(new TextField<String>()));
        ccDescription.setResizable(false);
        columnConfigs.add(ccDescription);

        ColumnConfig ccColor = new ColumnConfig();
        ccColor.setHeader("Color");
        ccColor.setId(TaskTypeModelData.COLOR_FIELD);
        ccColor.setDataIndex(TaskTypeModelData.COLOR_FIELD);
        ccColor.setWidth(50);

        ccColor.setRenderer(new ColorRenderer());

        ccColor.setResizable(false);
        columnConfigs.add(ccColor);

        return columnConfigs;
    }

}
