package fr.hd3d.common.ui.client.widget.tasktype;

import java.util.List;

import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.widget.FilterStoreField;
import fr.hd3d.common.ui.client.widget.ToolBarButton;
import fr.hd3d.common.ui.client.widget.simpleexplorer.SimpleExplorerPanel;


public class TaskTypeExplorerPanel extends SimpleExplorerPanel<TaskTypeModelData>
{
    private final VariantEditor variantEditor = new VariantEditor();

    private final Boolean isVariant;

    public TaskTypeExplorerPanel(List<ColumnConfig> columns, Boolean isVariant)
    {
        super();

        this.isVariant = isVariant;

        this.model = new TaskTypeExplorerModel();
        this.controller = new TaskTypeController(model, this);
        this.filterField = new FilterStoreField<TaskTypeModelData>(this.model.getModelStore());

        this.setStyle();
        this.setToolbar();
        this.setGrid(new ColumnModel(columns));
    }

    public void setProject(ProjectModelData project)
    {
        ((TaskTypeExplorerModel) this.model).setProject(project);
    }

    @Override
    protected void setToolbar()
    {
        super.setToolbar();

        if (this.isVariant)
            this.toolBar.insert(new ToolBarButton(Hd3dImages.getAssetIcon(), "Create step variants",
                    CommonEvents.CREATE_STEP_VARIANTS_CLICKED), 1);
    }

    public void displayVariantEditor()
    {
        variantEditor.show();
    }
}
