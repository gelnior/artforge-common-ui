package fr.hd3d.common.ui.client.widget.tasktype;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.widget.ToolBarButton;


/**
 * TaskTypeToolItem raises SHOW_NEW_TASK_TYPE_WINDOW event to display task type window.
 */
public class TaskTypeToolItem extends ToolBarButton
{

    /**
     * Default constructor
     */
    public TaskTypeToolItem()
    {
        super(Hd3dImages.getTaskTypeIcon(), "Edit task types", CommonEvents.TASK_TYPE_BUTTON_CLICKED);
    }
}
