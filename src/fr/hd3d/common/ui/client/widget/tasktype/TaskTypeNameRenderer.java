package fr.hd3d.common.ui.client.widget.tasktype;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;

import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.TaskTypeRenderer;


public class TaskTypeNameRenderer implements GridCellRenderer<TaskTypeModelData>
{

    public Object render(TaskTypeModelData model, String property, ColumnData config, int rowIndex, int colIndex,
            ListStore<TaskTypeModelData> store, Grid<TaskTypeModelData> grid)
    {
        String obj = model.get(property);
        String color = model.get(TaskTypeModelData.COLOR_FIELD);

        return TaskTypeRenderer.getTypeRendered(obj, color);
    }
}
