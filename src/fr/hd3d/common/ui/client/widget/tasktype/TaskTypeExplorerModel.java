package fr.hd3d.common.ui.client.widget.tasktype;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.store.Record;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.reader.TaskTypeReader;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.service.BulkRequests;
import fr.hd3d.common.ui.client.widget.simpleexplorer.SimpleExplorerEvents;
import fr.hd3d.common.ui.client.widget.simpleexplorer.SimpleExplorerModel;


public class TaskTypeExplorerModel extends SimpleExplorerModel<TaskTypeModelData>
{
    private ProjectModelData project;

    public TaskTypeExplorerModel()
    {
        super(new TaskTypeReader());
    }

    @Override
    public void saveUpdatedRecords()
    {
        List<Hd3dModelData> recordsToCreate = new ArrayList<Hd3dModelData>();
        List<Hd3dModelData> recordsToUpdate = new ArrayList<Hd3dModelData>();

        for (Record record : this.modelStore.getModifiedRecords())
        {
            TaskTypeModelData model = (TaskTypeModelData) record.getModel();
            model.setDefaultPath(this.getModelPath(model));

            if (project != null)
                model.setProjectID(project.getId());

            if (model.getId() == null)
            {
                recordsToCreate.add(model);
            }
            else
            {
                recordsToUpdate.add(model);
            }
        }

        if (recordsToCreate.size() > 0)
        {
            BulkRequests.bulkPost(recordsToCreate, SimpleExplorerEvents.CREATE_SUCCESS);
        }
        else
        {
            EventDispatcher.forwardEvent(SimpleExplorerEvents.CREATE_SUCCESS, recordsToCreate);
        }
        if (recordsToUpdate.size() > 0)
        {
            BulkRequests.bulkPut(recordsToUpdate, SimpleExplorerEvents.UPDATE_SUCCESS);
        }
        else
        {
            EventDispatcher.forwardEvent(SimpleExplorerEvents.UPDATE_SUCCESS, recordsToUpdate);
        }
    }

    public void setProject(ProjectModelData project)
    {
        this.project = project;
    }
}
