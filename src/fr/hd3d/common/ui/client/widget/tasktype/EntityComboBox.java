package fr.hd3d.common.ui.client.widget.tasktype;

import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.widget.FieldComboBox;


public class EntityComboBox extends FieldComboBox
{
    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    public EntityComboBox()
    {
        super();

        this.addField(1, CONSTANTS.Shot(), ShotModelData.SIMPLE_CLASS_NAME);
        this.addField(2, "Constituent", ConstituentModelData.SIMPLE_CLASS_NAME);
    }
}
