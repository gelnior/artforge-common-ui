package fr.hd3d.common.ui.client.widget.tasktype;

import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.Record;
import com.extjs.gxt.ui.client.util.Util;

import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.error.ErrorDispatcher;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.common.ui.client.widget.simpleexplorer.ISimpleExplorerPanel;
import fr.hd3d.common.ui.client.widget.simpleexplorer.SimpleExplorerController;
import fr.hd3d.common.ui.client.widget.simpleexplorer.SimpleExplorerModel;


public class TaskTypeController extends SimpleExplorerController<TaskTypeModelData>
{

    public TaskTypeController(SimpleExplorerModel<TaskTypeModelData> model, ISimpleExplorerPanel<TaskTypeModelData> view)
    {
        super(model, view);
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();

        if (CommonEvents.CREATE_STEP_VARIANTS_CLICKED == type)
        {
            this.onCreateStepVariantsClicked();
        }
        else if (CommonEvents.CREATE_STEP_VARIANTS_CONFIRMED == type)
        {
            this.onCreateStepVariantsConfirmed(event);
        }
        else
        {
            super.handleEvent(event);
        }
    }

    @Override
    protected void onSaveClicked()
    {
        Boolean isUnvalidTaskType = Boolean.FALSE;
        for (Record taskType : this.model.getModelStore().getModifiedRecords())
        {
            if (Util.isEmptyString(((TaskTypeModelData) taskType.getModel()).getEntityName()))
            {
                isUnvalidTaskType = Boolean.TRUE;
                break;
            }
        }

        if (isUnvalidTaskType)
            ErrorDispatcher.sendError(CommonErrors.TASK_TYPE_WITHOUT_ENTITY);
        else
            super.onSaveClicked();
    }

    @Override
    protected void registerEvents()
    {
        super.registerEvents();
        this.registerEventTypes(CommonEvents.CREATE_STEP_VARIANTS_CLICKED);
        this.registerEventTypes(CommonEvents.CREATE_STEP_VARIANTS_CONFIRMED);
    }

    private void onCreateStepVariantsClicked()
    {
        ((TaskTypeExplorerPanel) this.view).displayVariantEditor();
    }

    private void onCreateStepVariantsConfirmed(AppEvent event)
    {
        List<TaskTypeModelData> taskTypes = event.getData("taskTypes");
        String variant = event.getData("variant");

        for (TaskTypeModelData taskType : taskTypes)
        {
            TaskTypeModelData variantTaskType = new TaskTypeModelData();
            // variantTaskType.setName(taskType.getName() + " (" + variant + ")");
            variantTaskType.setColor(taskType.getColor());
            variantTaskType.setProjectID(MainModel.getCurrentProject().getId());
            variantTaskType.setEntityName(taskType.getEntityName());

            this.model.getModelStore().add(variantTaskType);
            this.model.getModelStore().getRecord(variantTaskType)
                    .set(TaskTypeModelData.NAME_FIELD, taskType.getName() + " (" + variant + ")");
            this.model.getCreatedRecords().add(variantTaskType);
        }
    }

}
