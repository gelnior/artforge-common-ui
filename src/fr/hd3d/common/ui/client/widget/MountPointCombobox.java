package fr.hd3d.common.ui.client.widget;

import java.util.HashMap;

import com.extjs.gxt.ui.client.store.Store;
import com.extjs.gxt.ui.client.store.StoreFilter;

import fr.hd3d.common.ui.client.modeldata.asset.MountPointModelData;
import fr.hd3d.common.ui.client.modeldata.reader.MountPointReader;


public class MountPointCombobox extends ModelDataComboBox<MountPointModelData>
{

    public MountPointCombobox()
    {
        super(new MountPointReader());
        this.setDisplayField(MountPointModelData.STORAGE_NAME_FIELD);
        this.config.setSortField(MountPointModelData.STORAGE_NAME_FIELD);
        this.setEmptyText("Select a mout point...");
        this.getStore().addFilter(new StoreFilter<MountPointModelData>() {
            HashMap<String, Boolean> map = new HashMap<String, Boolean>();

            public boolean select(Store<MountPointModelData> store, MountPointModelData parent,
                    MountPointModelData item, String property)
            {
                Boolean isExist = map.get(item.getStorageName());
                if (isExist == null)
                {
                    map.put(item.getStorageName(), true);
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    protected void onLoaderLoad()
    {
        super.onLoaderLoad();
        this.getStore().filter("");
    }
}
