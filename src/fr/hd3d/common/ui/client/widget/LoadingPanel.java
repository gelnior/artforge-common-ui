package fr.hd3d.common.ui.client.widget;

import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;

import fr.hd3d.common.ui.client.images.Hd3dImages;


/**
 * Panel that shows a loading message with an animated GIF.
 * 
 * @author HD3D
 */
public class LoadingPanel extends ContentPanel
{

    protected HTML textHtml = new HTML();

    /**
     * Default constructor : set loading text and image.
     * 
     * @param title
     *            The title, bold displayed.
     * @param loadingMessage
     *            The message displayed below the title.
     */
    public LoadingPanel(String title, String loadingMessage)
    {
        this.setHeaderVisible(false);
        this.setBodyBorder(false);
        this.addStyleName("loading-panel");

        this.setImage();
        this.setText(title, loadingMessage);
        this.add(textHtml);
    }

    /**
     * Set Image on the left. Image is the large loading from GXT library.
     */
    protected void setImage()
    {
        Image image = Hd3dImages.largeLoading();
        image.addStyleName("loading-img");
        this.add(image);
    }

    /**
     * Set loading text on the right.
     */
    public void setText(String title, String loadingMessage)
    {
        // this.remo
        // textHtml = new HTML();
        // text.setHTML(title);
        textHtml.setHTML(title + "<br /><span class=\"loading-msg\">" + loadingMessage + "</span>");
        textHtml.addStyleName("loading-text");

        // this.add(textHtml);
    }
}
