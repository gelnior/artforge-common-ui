package fr.hd3d.common.ui.client.widget;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.store.Store;
import com.extjs.gxt.ui.client.store.StoreFilter;
import com.extjs.gxt.ui.client.widget.form.TextField;


/**
 * A <code>TriggerField</code> that filters its bound stores based on the value of the field.
 * 
 * @param <M>
 *            the model type
 */
public abstract class SimpleStoreFilterField<M extends ModelData> extends TextField<String>
{

    protected StoreFilter<M> filter;
    protected List<Store<M>> stores = new ArrayList<Store<M>>();

    private String property;

    /**
     * Creates a new store filter field.
     */
    public SimpleStoreFilterField()
    {
        setAutoValidate(true);
        filter = new StoreFilter<M>() {
            public boolean select(Store<M> store, M parent, M model, String property)
            {
                String v = getRawValue();
                return doSelect(store, parent, model, property, v);
            }
        };
    }

    /**
     * Binds the store to the field.
     * 
     * @param store
     *            the store to add
     */
    public void bind(Store<M> store)
    {
        stores.add(store);
    }

    /**
     * Returns the current filter property.
     * 
     * @return the property name
     */
    public String getProperty()
    {
        return property;
    }

    /**
     * Sets the filter property name to be filtered.
     * 
     * @param property
     *            the property name
     */
    public void setProperty(String property)
    {
        this.property = property;
    }

    /**
     * Unbinds the store from the field.
     * 
     * @param store
     *            the store to be unbound
     */
    public void unbind(Store<M> store)
    {
        store.removeFilter(filter);
        stores.remove(store);
    }

    protected void applyFilters(Store<M> store)
    {
        if (getRawValue().length() > 0)
        {
            store.addFilter(filter);
            store.applyFilters(property);
        }
        else
        {
            store.removeFilter(filter);
        }
    }

    protected void onFilter()
    {
        for (Store<M> s : stores)
        {
            applyFilters(s);
        }
        focus();
    }

    protected abstract boolean doSelect(Store<M> store, M parent, M record, String property, String filter);

    @Override
    protected boolean validateValue(String value)
    {
        boolean ret = super.validateValue(value);
        onFilter();
        return ret;
    }
}
