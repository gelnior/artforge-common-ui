package fr.hd3d.common.ui.client.widget;

import com.extjs.gxt.ui.client.Style.Scroll;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.Text;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;


/**
 * LogBox is a little widget that lets you display log in a panel. If logs height is greater than the panel height, the
 * LogBox automatically sets a scroll bar.
 * 
 * @author HD3D
 */
public class LogBox extends LayoutContainer
{
    /** Text where logs are written. */
    Text text = new Text("");

    /** Default Constructor : sets fit layout, scroll mode at AUTO and add padding to the text. */
    public LogBox()
    {
        super(new FitLayout());

        add(text);
        setLayoutOnChange(true);
        setScrollMode(Scroll.AUTO);

        this.setTextStyle();
    }

    /**
     * Display a log message in the LogBox.
     * 
     * @param msg
     *            the message to display.
     */
    public void log(String msg)
    {
        int pos = getVScrollPosition();
        int max = text.getHeight() - getHeight();

        text.setText(text.getText() + msg + "\n");

        this.updateScrollPosition(pos, max);
    }

    /**
     * Sets pre-defined text style : auto width : on, auto height on, whitespace : pre, padding : 5px;
     */
    private void setTextStyle()
    {
        text.setAutoHeight(true);
        text.setAutoWidth(true);
        text.setStyleAttribute("whiteSpace", "pre");
        text.setStyleAttribute("padding", "5px;");
    }

    /**
     * Sets the scroll bar position to the last line.
     * 
     * @param pos
     *            the position before adding text.
     * @param max
     *            the maximum position before adding text.
     */
    private void updateScrollPosition(int pos, int max)
    {
        int newmax = text.getHeight() - getHeight();

        if ((pos == max) || (max < 0 && newmax > 0))
        {
            setVScrollPosition(newmax);
        }
    }
}
