package fr.hd3d.common.ui.client.widget;

import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.listener.ReloadListener;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.service.store.BaseStore;


/**
 * Simple button to connect on a store. When the button is clicked, the store is refreshed.
 * 
 * @author HD3D
 * 
 * @param <M>
 *            The type of data to display.
 */
public class RefreshButton<M extends Hd3dModelData> extends ToolBarButton
{

    /**
     * Set icon and tool tip. Link the button with given store.
     * 
     * @param store
     *            The refreshed store.
     */
    public RefreshButton(final BaseStore<M> store)
    {
        super(Hd3dImages.getRefreshIcon(), "Refresh", null);

        this.addSelectionListener(new ReloadListener<M>(store));
    }

}
