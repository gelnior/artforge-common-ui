package fr.hd3d.common.ui.client.widget;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.store.ListStore;

import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.util.CollectionUtils;


public class MultiFieldComboBox extends MultiComboBox<FieldModelData>
{
    public MultiFieldComboBox()
    {
        super();

        ListStore<FieldModelData> store = new ListStore<FieldModelData>();
        store.setSortField(FieldModelData.ID_FIELD);
        store.sort(FieldModelData.ID_FIELD, SortDir.ASC);

        this.setStore(store);

        this.setDisplayField(FieldModelData.NAME_FIELD);
        this.setValueField(FieldModelData.VALUE_FIELD);
    }

    public MultiFieldComboBox(FastMap<FieldModelData> map)
    {
        this();

        for (Map.Entry<String, FieldModelData> entry : map.entrySet())
        {
            this.getStore().add(entry.getValue());
        }
        this.getStore().sort(FieldModelData.NAME_FIELD, SortDir.ASC);
    }

    public Object getDirectValues()
    {
        List<Object> values = new ArrayList<Object>();
        List<FieldModelData> fields = this.getSelection();
        if (CollectionUtils.isNotEmpty(fields))
            for (FieldModelData field : fields)
                values.add(field.getValue());

        return values;
    }

    /**
     * Add a new field (couple display value - name) to the combo box.
     * 
     * @param name
     *            Displayed value.
     * @param value
     *            Real value.
     * 
     * @return added field.
     */
    public FieldModelData addField(int id, String name, Object value)
    {
        FieldModelData field = new FieldModelData(id, name, value);
        this.getStore().add(field);

        return field;
    }

    /**
     * @param realValue
     *            The expected value (not the field modeldata).
     * @return the field corresponding to the value.
     */
    public Object getValue(Object realValue)
    {
        for (FieldModelData field : this.getStore().getModels())
        {
            Object modelValue = field.getValue();

            if (modelValue.equals(realValue))
            {
                return field;
            }
        }
        return null;
    }

    public void setValueByStringValues(List<String> stringValues)
    {
        List<FieldModelData> fields = new ArrayList<FieldModelData>();
        for (String stringValue : stringValues)
        {
            FieldModelData field = this.getStore().findModel(FieldModelData.VALUE_FIELD, stringValue);
            fields.add(field);
        }

        this.setSelection(fields);
        this.collapse();
    }

    public void setValuesByStringValue(String stringValue)
    {
        List<String> values = new ArrayList<String>();
        if (stringValue.startsWith("[") && stringValue.length() > 2)
        {
            String tmpVal = stringValue.substring(1, stringValue.length() - 1);
            String[] valuesArray = tmpVal.split(", ");
            values.addAll(CollectionUtils.asList(valuesArray));
        }
        this.setValueByStringValues(values);
    }

    public void setValueByIntegerValues(List<Integer> values)
    {
        List<FieldModelData> fields = new ArrayList<FieldModelData>();
        for (Integer value : values)
        {
            FieldModelData field = this.getStore().findModel(FieldModelData.VALUE_FIELD, value);
            fields.add(field);
        }

        this.setSelection(fields);
    }

}
