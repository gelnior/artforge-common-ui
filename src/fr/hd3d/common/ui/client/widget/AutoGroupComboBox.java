package fr.hd3d.common.ui.client.widget;

import com.google.gwt.core.client.GWT;

import fr.hd3d.common.client.enums.EConstraintLogicalOperator;
import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.reader.ResourceGroupReader;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.LogicConstraint;


public class AutoGroupComboBox extends AutoCompleteModelCombo<ResourceGroupModelData>
{
    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    protected LogicConstraint projectFilterGroup;

    public AutoGroupComboBox()
    {
        this(false);
    }

    public AutoGroupComboBox(boolean emptyField)
    {
        super(new ResourceGroupReader(), emptyField);

        this.setStyles();
        this.store.setPath(ServicesPath.getPath(ResourceGroupModelData.SIMPLE_CLASS_NAME));
        this.store.addParameter(this.getProjectFilter());
        this.setContextMenu();

    }

    public void setProject(Long projectId)
    {
        LogicConstraint fg = this.getProjectFilter();
        if (projectId != null)
        {
            fg.setRightMember(new Constraint(EConstraintOperator.eq, "project.id", projectId));
        }
        this.setData();
    }

    private LogicConstraint getProjectFilter()
    {
        if (projectFilterGroup == null)
        {
            projectFilterGroup = new LogicConstraint(EConstraintLogicalOperator.OR);
            projectFilterGroup.setLeftMember(new Constraint(EConstraintOperator.isnull, "project.id", null));
        }

        return this.projectFilterGroup;
    }

    @Override
    protected String getLike()
    {
        return "%" + getRawValue() + "%";
    }

    /**
     * Set combo styles.
     */
    @Override
    protected void setStyles()
    {
        super.setStyles();
        this.setEmptyText("Select Group ...");
        this.setDisplayField(RecordModelData.NAME_FIELD);
    }

}
