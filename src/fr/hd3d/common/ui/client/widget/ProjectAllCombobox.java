package fr.hd3d.common.ui.client.widget;

import fr.hd3d.common.ui.client.config.CommonConfig;
import fr.hd3d.common.ui.client.cookie.FavoriteCookie;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;


/**
 * Combo box containing open project list plus the "All" project.
 * 
 * @author HD3D
 */
public class ProjectAllCombobox extends ProjectCombobox
{

    public ProjectAllCombobox()
    {
        super();
    }

    /**
     * When data are retrieved it inserts the "All project" and selects the project given in the cookie.
     */
    @Override
    protected void onLoaderLoad()
    {
        ProjectModelData project = new ProjectModelData();
        project.setId(-1L);
        project.setName("All");
        project.setColor("#000");

        store.insert(project, 0);

        super.onLoaderLoad();
    }

    /**
     * Save project id to favorite cookie only if id is not null and not equal with -1 (do not save the all project).
     * 
     * @see fr.hd3d.common.ui.client.widget.ProjectCombobox#onSelectionChanged(fr.hd3d.common.ui.client.modeldata.production.ProjectModelData)
     */
    @Override
    protected void onSelectionChanged(ProjectModelData project)
    {
        if (project != null)
        {
            Long id = project.getId();
            if (id != null)
            {
                FavoriteCookie.putFavValue(CommonConfig.COOKIE_VAR_PROJECT, id.toString());
            }
        }
    }
}
