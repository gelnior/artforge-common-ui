package fr.hd3d.common.ui.client.widget.thumbnaileditor;

import java.util.Date;

import com.extjs.gxt.ui.client.core.FastMap;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.ServicesPath;


/**
 * Static class used to store thumbnail path when thumbnail is modified by user. When a thumbnail is modified, if its
 * path does not change, it is not reloaded. So a random suffix is added to the path when thumbnail is changed. To store
 * this random suffix a static class is needed. Because when the thumbnail will be rendered, it will check if a path
 * with suffix is set before rendering image.
 * 
 * @author HD3D
 */
public class ThumbnailPath
{
    /** Model data property field used to notify that model data thumbnail has changed. */
    public static final String CHANGED_MARKER = "thumbnailChanged";

    /** This map contains thumbnail path and have the concerned model ID as key. */
    private final static FastMap<String> thumbnailPathMap = new FastMap<String>();
    private final static FastMap<String> previewPathMap = new FastMap<String>();

    /**
     * Add a path to thumbnail map.
     * 
     * @param id
     *            Id serves as the key.
     * @param path
     *            Path is the value to add.
     */
    // public static void putPath(String id, String path)
    // {
    // pathMap.put(id, path);
    // }

    /**
     * @param id
     *            Id of the model from which the thumbnail path is needed.
     * 
     * @return The path corresponding to the id given in parameter.
     */
    public static String getPath(String id, boolean isPreview)
    {
        if (id != null)
        {
            if (isPreview)
            {
                return previewPathMap.get(id);
            }
            else
            {
                return thumbnailPathMap.get(id);
            }
        }
        else
        {
            return null;
        }
    }

    /**
     * @param model
     *            The model of which thumbnail path is asked.
     * @return Thumbnail path corresponding to model. If thumbnail has changed, getPath takes care of it by giving path
     *         corresponding to last image version.
     */
    public static String getPath(Hd3dModelData model)
    {
        return getPath(model, false);
    }

    /**
     * @param model
     *            The model of which thumbnail path is asked.
     * @param isPreview
     *            Set to true if desired path correspond to a preview, false for thumbnails.
     * @return Thumbnail path corresponding to model. If thumbnail has changed, getPath takes care of it by giving path
     *         corresponding to last image version.
     */
    public static String getPath(Hd3dModelData model, boolean isPreview)
    {
        String previewPath = RestRequestHandlerSingleton.getInstance().getServicesUrl() + ServicesPath.PREVIEWS;
        String thumbnailPath = RestRequestHandlerSingleton.getInstance().getServicesUrl() + ServicesPath.THUMBNAILS;

        String entity = model.getSimpleClassName();
        previewPath += ServicesPath.getPath(entity);
        previewPath += model.getId() + "/";

        thumbnailPath += ServicesPath.getPath(model.getSimpleClassName());
        thumbnailPath += model.getId() + "/";

        if (model.get(CHANGED_MARKER) != null && (Boolean) model.get(CHANGED_MARKER))
        {
            previewPath += "?" + (new Date()).getTime();
            thumbnailPath += "?" + (new Date()).getTime();
        }

        if (model.getId() != null)
        {
            previewPathMap.put(model.getId().toString(), previewPath);
            thumbnailPathMap.put(model.getId().toString(), thumbnailPath);
        }

        model.set(CHANGED_MARKER, Boolean.valueOf(false));

        if (isPreview)
        {
            return previewPath;
        }
        else
        {
            return thumbnailPath;
        }
    }
}
