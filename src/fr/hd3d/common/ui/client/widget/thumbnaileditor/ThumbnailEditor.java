package fr.hd3d.common.ui.client.widget.thumbnaileditor;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.Status;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.FileUploadField;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.form.FormPanel.Method;
import com.extjs.gxt.ui.client.widget.form.HiddenField;
import com.extjs.gxt.ui.client.widget.layout.AnchorLayout;
import com.extjs.gxt.ui.client.widget.layout.FormData;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;


/**
 * Dialog box that allows user to upload image file to set new thumbnail for a given model.
 * 
 * @author HD3D
 */
public class ThumbnailEditor extends Dialog
{
    public static final String IMAGE_VAR_FORM = "image";

    /** Constant strings to display : dialog messages, button label... */
    public static final CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    /** Services URL. */
    protected String servicesUrl = "";
    /** The model concerned by the thumbnail edition. */
    protected Hd3dModelData selectedModel;
    /** Status showing that upload is running. */
    protected Status savingStatus = new Status();

    /** Panel containing upload form. */
    protected final FormPanel panel = new FormPanel();
    /** Hidden field containing model id. */
    protected HiddenField<String> field = new HiddenField<String>();
    /** File field. */
    protected FileUploadField file = new FileUploadField();
    /** Button that submits upload form. */
    protected Button submit = new Button("submit");

    /**
     * Default constructor.
     */
    public ThumbnailEditor()
    {
        this.setStyles();
        this.setButtonBar();
        this.createForm();
        this.setListener();
    }

    /**
     * @return The model associated to the thumbnail editor.
     */
    public Hd3dModelData getSelectedModel()
    {
        return selectedModel;
    }

    /**
     * When model data object associated to the editor is changed, the ID hidden field is updated with the new model ID
     * and the form action path is updated depending on model data nature (constituent, shot or person).
     * 
     * @param model
     */
    public void setThumbnailedObject(Hd3dModelData model)
    {
        this.selectedModel = model;

        this.field.setValue(model.getId().toString());
        this.panel.setAction(this.servicesUrl + ServicesPath.THUMBNAILS
                + ServicesPath.getPath(model.getSimpleClassName()));
    }

    /**
     * When dialog is shown, the file field is cleared.
     * 
     * @see com.extjs.gxt.ui.client.widget.BoxComponent#onShow()
     */
    @Override
    protected void onShow()
    {
        super.onShow();
        file.clear();
    }

    /**
     * When submit button is clicked, data are submitted and saving indicator is displayed.
     * 
     * @param ce
     *            The button event.
     */
    protected void onSubmitClicked(ButtonEvent ce)
    {
        if (file.getValue() == null)
        {
            MessageBox.info("Error", "Please chose a file before sending your thumbnail.", null);
        }
        else
        {
            panel.submit();
            savingStatus.setBusy(CONSTANTS.Saving());
            savingStatus.show();
        }
    }

    /**
     * When submit is finished, the saving status is hidden, the dialog is hidden and the THUMBNAILED_UPLOADED event is
     * raised.
     * 
     * @param be
     *            Base event raised by form signaling that submit is finished.
     */
    protected void onSubmitFinished(BaseEvent be)
    {
        savingStatus.hide();
        hide();

        AppEvent event = new AppEvent(ExplorerEvents.THUMBNAIL_UPLOADED, this.selectedModel);
        EventDispatcher.forwardEvent(event);
    }

    /**
     * Set listeners on submit event : submit button clicking and submit finishing.
     */
    protected void setListener()
    {
        submit.addSelectionListener(new SelectionListener<ButtonEvent>() {
            @Override
            public void componentSelected(ButtonEvent ce)
            {
                onSubmitClicked(ce);
            }

        });

        panel.addListener(Events.Submit, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                onSubmitFinished(be);
            }
        });
    }

    /**
     * Create the upload form.
     */
    protected void createForm()
    {
        this.panel.setBorders(false);
        this.panel.setBodyBorder(false);
        this.panel.setHeaderVisible(false);
        this.panel.setMethod(Method.POST);
        panel.setEncoding(FormPanel.Encoding.MULTIPART);

        this.servicesUrl = RestRequestHandlerSingleton.getInstance().getServicesUrl();
        this.panel.setAction(this.servicesUrl + ServicesPath.THUMBNAILS);

        file.setFieldLabel("Image");
        file.setName(IMAGE_VAR_FORM);
        panel.add(file, new FormData("100%"));

        field.setName(Hd3dModelData.ID_FIELD);
        panel.add(field);

        this.add(panel);
    }

    /**
     * Add saving status and submit button ("OK") to the button bar.
     */
    protected void setButtonBar()
    {
        this.getButtonBar().add(savingStatus);
        this.getButtonBar().add(submit);
    }

    /**
     * Set editor styles : layout, size...
     */
    protected void setStyles()
    {
        this.setHeading("Thumbnail Editor");
        this.setSize(400, 120);
        this.setLayout(new AnchorLayout());
        this.setButtons("");
        this.setHideOnButtonClick(true);
        this.setModal(true);
    }
}
