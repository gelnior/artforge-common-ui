package fr.hd3d.common.ui.client.widget.thumbnaileditor;

import com.extjs.gxt.ui.client.widget.Dialog;
import com.google.gwt.user.client.ui.Image;


/**
 * Preview displayer handles instance of preview dialog. Preview dialog is created only when asked for showing. Preview
 * dialog display preview image of services instances such as shot, constituent or person.
 * 
 * @author HD3D
 */
public class PreviewDisplayer
{
    /** The dialog to display. */
    private static Dialog previewDialog = null;

    /**
     * @return Create (if not exists) and return preview dialog.
     */
    public static Dialog getDialog()
    {
        if (previewDialog == null)
        {
            previewDialog = new Dialog();
            previewDialog.setSize(400, 300);
            previewDialog.setHeading("Preview");

            previewDialog.setButtons("");
        }

        return previewDialog;
    }

    /**
     * Show dialog and display preview image set at <i>path</i>.
     * 
     * @param path
     *            The path of image to display.
     */
    public static void displayPreview(String path)
    {
        Image img = new Image(path);
        Dialog dialog = getDialog();
        dialog.removeAll();

        dialog.add(img);
        dialog.layout();
        dialog.show();
        dialog.center();
    }
}
