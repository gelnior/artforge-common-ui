package fr.hd3d.common.ui.client.widget.thumbnaileditor;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;


/**
 * Preview displayer handles instance of preview dialog. Preview dialog is created only when asked for showing. Preview
 * dialog display preview image of services instances such as shot, constituent or person.
 * 
 * @author HD3D
 */
public class ThumbnailEditorDisplayer
{
    /** The dialog to display. */
    private static ThumbnailEditor dialog = null;

    /**
     * @return Create (if not exists) and return thumbnail editor dialog.
     */
    public static ThumbnailEditor getDialog()
    {
        if (dialog == null)
        {
            dialog = new ThumbnailEditor();
        }

        return dialog;
    }

    /**
     * Show dialog and display preview image set at <i>path</i>.
     * 
     * @param model
     *            The model of which thumbnail should be edited.
     */
    public static void display(Hd3dModelData model)
    {
        getDialog().setThumbnailedObject(model);
        getDialog().show();
        getDialog().center();
    }

    /**
     * @return Model concerned by thumbnail edition.
     */
    public static Hd3dModelData getSelectedModel()
    {
        if (dialog != null)
            return dialog.getSelectedModel();
        else
            return null;
    }
}
