package fr.hd3d.common.ui.client.widget;

import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.util.Util;
import com.google.gwt.event.dom.client.KeyCodes;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.store.ServicesPagingStore;


public class FilterServiceStoreField<C extends Hd3dModelData> extends FilterStoreField<C>
{
    /** Store containing data to filter. */
    protected ServicesPagingStore<C> serviceStore;

    protected Constraint serviceFilter = new Constraint(EConstraintOperator.like, this.nameField, "%");

    protected int limit = 100;

    public FilterServiceStoreField(ServicesPagingStore<C> store)
    {
        super(store);
        this.serviceStore = store;
        this.serviceStore.addParameter(this.serviceFilter);
    }

    public FilterServiceStoreField(ServicesPagingStore<C> store, int limit)
    {
        this(store);
        this.limit = limit;
    }

    @Override
    public void setNameField(String nameField)
    {
        super.setNameField(nameField);
        this.serviceFilter.setColumn(this.nameField);
    }

    /**
     * When key is up, the store is filtered.
     */
    @Override
    protected void onKeyUp(ComponentEvent event)
    {
        if (serviceStore.getCount() > limit || event.getKeyCode() == KeyCodes.KEY_BACKSPACE)
        {
            if (!Util.isEmptyString(getValue()))
            {
                serviceFilter.setLeftMember(getValue() + "%");
            }
            else
            {
                serviceFilter.setLeftMember("%");
            }
            serviceStore.reload();
        }
        super.onKeyUp(event);
    }
}
