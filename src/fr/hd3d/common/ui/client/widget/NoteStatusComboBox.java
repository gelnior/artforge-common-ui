package fr.hd3d.common.ui.client.widget;

import java.util.List;

import com.extjs.gxt.ui.client.core.FastMap;

import fr.hd3d.common.client.enums.EApprovalNoteStatus;
import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskStatusMap;


/**
 * Combo box displaying status available for a note. It also provides static map for centralizing label displayed for
 * status.
 * 
 * @author HD3D
 */
public class NoteStatusComboBox extends FieldComboBox
{
    /** Map that returns short name for a given status. */
    private static FastMap<String> abbreviationMap = new FastMap<String>();
    /** Map that returns full name for a given status. */
    private static FastMap<String> fullStatusMap = new FastMap<String>();

    /** @return Map that returns short name for a given status. */
    public static FastMap<String> getStatusAbbreviationMap()
    {
        if (abbreviationMap.isEmpty())
        {
            abbreviationMap.put(EApprovalNoteStatus.OK.toString(), "OK");
            abbreviationMap.put(EApprovalNoteStatus.RETAKE.toString(), "Retake");
            abbreviationMap.put(EApprovalNoteStatus.TO_DO.toString(), "Todo");
            abbreviationMap.put(EApprovalNoteStatus.STAND_BY.toString(), "Stand By");
            abbreviationMap.put(EApprovalNoteStatus.CANCELLED.toString(), "Cancelled");
            abbreviationMap.put(EApprovalNoteStatus.WORK_IN_PROGRESS.toString(), "WIP");
            abbreviationMap.put(EApprovalNoteStatus.WAIT_APP.toString(), "WFA");
            abbreviationMap.put(EApprovalNoteStatus.CLOSE.toString(), "Closed");
            abbreviationMap.put(EApprovalNoteStatus.NEW.toString(), "-");
        }
        return abbreviationMap;
    }

    /** @return Map that returns full name for a given status. */
    public static FastMap<String> getFullStatusMap()
    {
        if (fullStatusMap.isEmpty())
        {
            fullStatusMap.put(EApprovalNoteStatus.OK.toString(), "OK");
            fullStatusMap.put(EApprovalNoteStatus.RETAKE.toString(), "Retake");
            fullStatusMap.put(EApprovalNoteStatus.TO_DO.toString(), "To Do");
            fullStatusMap.put(EApprovalNoteStatus.STAND_BY.toString(), "Stand By");
            fullStatusMap.put(EApprovalNoteStatus.CANCELLED.toString(), "Cancelled");
            fullStatusMap.put(EApprovalNoteStatus.WORK_IN_PROGRESS.toString(), "Work In Progress");
            fullStatusMap.put(EApprovalNoteStatus.WAIT_APP.toString(), "Wait For Approval");
            fullStatusMap.put(EApprovalNoteStatus.CLOSE.toString(), "Closed");
            fullStatusMap.put(EApprovalNoteStatus.NEW.toString(), "-");
        }
        return fullStatusMap;
    }

    /**
     * Default constructor : fill combo box for all available status.
     */
    public NoteStatusComboBox()
    {
        this(false);
    }

    /**
     * Create status combo for a given list of statuses.
     * 
     * @param statuses
     *            Status to set inside combo box.
     */
    public NoteStatusComboBox(List<EApprovalNoteStatus> statuses)
    {
        super();
        this.setMinListWidth(150);
        this.setTemplate(getXTemplate());
        int i = 0;
        for (EApprovalNoteStatus status : statuses)
        {
            this.addField(i, status);
            i++;
        }
    }

    /**
     * Constructor that fills combobox with all available status if <i>isFilterComboBox</i> is set to true. Else it
     * removes New status.
     * 
     * @param isFilterComboBox
     *            The combo box on which filter should be set.
     */
    public NoteStatusComboBox(boolean isFilterComboBox)
    {
        super();
        this.setMinListWidth(150);
        this.setTemplate(getXTemplate());
        this.setEditable(false);

        this.addField(1, EApprovalNoteStatus.OK);
        this.addField(2, EApprovalNoteStatus.RETAKE);
        this.addField(3, EApprovalNoteStatus.TO_DO);
        this.addField(4, EApprovalNoteStatus.STAND_BY);
        this.addField(5, EApprovalNoteStatus.CANCELLED);
        this.addField(6, EApprovalNoteStatus.WORK_IN_PROGRESS);
        this.addField(7, EApprovalNoteStatus.WAIT_APP);
        this.addField(8, EApprovalNoteStatus.CLOSE);
        if (isFilterComboBox)
        {
            this.addField(9, EApprovalNoteStatus.NEW);
        }
    }

    /**
     * Add a field with id set to <i>id</i>
     * 
     * @param id
     *            The id used to order fields in combo box.
     * @param status
     *            The status to add.
     * @return Field created from given status.
     */
    public FieldModelData addField(int id, EApprovalNoteStatus status)
    {
        FieldModelData field = new FieldModelData(id, getFullStatusMap().get(status.toString()), status.toString());
        field.set("color", TaskStatusMap.getColorForStatus(status.toString()));
        this.getStore().add(field);

        return field;
    }

    /**
     * @return Combo box values display style.
     */
    public native String getXTemplate() /*-{
                                                                            return  [ 
                                                                            '<tpl for=".">', 
                                                                            '<div style="width: 100%; text-align: center; background-color:{color};" class="x-combo-list-item"> <span style="font-weight: bold; font-size: 16px;">&nbsp;{name}&nbsp;&nbsp;</span> </div>', 
                                                                            '</tpl>' 
                                                                            ].join("");
                                                                            }-*/;

}
