package fr.hd3d.common.ui.client.widget;

import com.extjs.gxt.ui.client.widget.Status;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;


/**
 * Little widget to display when saving occurs. It displays a loading ajax round.
 * 
 * @author HD3D
 */
public class SavingStatus extends Status
{
    /** Constant strings from common library. */
    public static final CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    public SavingStatus()
    {
        super();

        this.setBusy(CONSTANTS.Saving());
        this.hide();
    }
}
