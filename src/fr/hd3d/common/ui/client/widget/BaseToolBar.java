package fr.hd3d.common.ui.client.widget;

import com.extjs.gxt.ui.client.widget.toolbar.ToolBar;


/**
 * Base toolbar set automatically styles for HD3D toolbar. It also handles a saving status, to display loading indicator
 * when saving occurs or any other requesting. Saving status is not added by default.
 * 
 * @author HD3D
 */
public class BaseToolBar extends ToolBar
{
    /** Status to show user that interactions occur with server. */
    protected final SavingStatus status = new SavingStatus();

    /**
     * Constructor : set padding.
     */
    public BaseToolBar()
    {
        this.setStyleAttribute("padding", "5px");
    }

    /**
     * Hide saving status.
     */
    public void hideSaving()
    {
        this.status.hide();
    }

    /**
     * Show saving status.
     */
    public void showSaving()
    {
        this.status.show();
    }
}
