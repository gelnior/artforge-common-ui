package fr.hd3d.common.ui.client.widget.tabdialog;

import java.util.HashMap;
import java.util.Map;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.widget.Component;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.TabItem;
import com.extjs.gxt.ui.client.widget.TabPanel;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;


/**
 * Tabs dialog permit to manipulate multiple tabulated widgets while masking their controller when the tabulation is not
 * active.
 * 
 * @author HD3D
 */
public abstract class TabsDialog extends Dialog
{
    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    /** Folder containing all tabs (computer and device). */
    protected TabPanel folder = new TabPanel();

    /** Map linking tabulations and view contained in the tabulation. */
    protected final Map<TabItem, IMaskableView> views = new HashMap<TabItem, IMaskableView>();

    /** Last displayed view (last active tab). */
    protected IMaskableView lastSelected;

    /**
     * Default constructor : set a dialog with a tab folder.
     */
    public TabsDialog()
    {
        super();
        this.add(folder);
    }

    /**
     * @return last selected view.
     */
    public IMaskableView getLastSelected()
    {
        return this.lastSelected;
    }

    /**
     * Set last selected view.
     * 
     * @param lastSelected
     *            Element to set as last selected.
     */
    public void setLastSelected(IMaskableView lastSelected)
    {
        this.lastSelected = lastSelected;
    }

    /**
     * Add a tab to the tab folder containing the component given in parameter. View is the same object as component. It
     * is needed for its interface properties.<br>
     * Set a click listener on the tab header to idle the last selected view and to active the tab view.
     * 
     * 
     * @param title
     *            The title of the new tab created.
     * @param component
     *            The component to display in the tab.
     * @param view
     *            The component to display in the tab.
     * @return the tab item created.
     */
    public TabItem addTab(String title, Component component, final IMaskableView view)
    {
        final TabItem tabItem = new TabItem(title);

        tabItem.addStyleName("pad-text");
        tabItem.setLayout(new FitLayout());
        tabItem.getHeader().addListener(Events.OnClick, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                if (getLastSelected() != null)
                {
                    getLastSelected().idle();
                }
                views.get(tabItem).unIdle();
                setLastSelected(view);
            }
        });

        tabItem.add(component);
        view.idle();

        this.views.put(tabItem, view);
        this.folder.add(tabItem);

        return tabItem;
    }

    /**
     * When shown, it unmasks last selected controller. If there was no last selection, it unmasks the first view
     * contained in the tab folder.
     */
    @Override
    public void show()
    {
        super.show();

        if (lastSelected != null)
        {
            lastSelected.unIdle();
        }
        else
        {
            if (this.folder.getItemCount() > 0)
            {
                IMaskableView view = views.get(this.folder.getItem(0));
                view.unIdle();
                setLastSelected(view);
            }
        }
    }

    /**
     * Mask all view controllers.
     */
    @Override
    public void hide()
    {
        super.hide();

        for (TabItem tab : folder.getItems())
        {
            views.get(tab).idle();
        }
    }

    /**
     * Enable type tab Headers.
     */
    public void enableTabHeaders()
    {
        for (TabItem tabItem : this.folder.getItems())
        {
            tabItem.getHeader().enable();
        }
    }

    /**
     * Disable type tab Headers.
     */
    public void disableTabHeaders()
    {
        for (TabItem tabItem : this.folder.getItems())
        {
            tabItem.getHeader().disable();
        }
    }

}
