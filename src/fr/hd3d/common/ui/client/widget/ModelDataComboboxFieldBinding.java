package fr.hd3d.common.ui.client.widget;

import com.extjs.gxt.ui.client.binding.FieldBinding;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;


/**
 * Binder for a model data combo box and a Hd3dModelData instance.
 * 
 * @author HD3D
 * 
 * @param <M>
 *            Type of model to bind.
 */
public class ModelDataComboboxFieldBinding<M extends RecordModelData> extends FieldBinding
{
    /** Combo to bind. */
    private final ModelDataComboBox<M> modelDataComboBox;
    /** Property used to differentiate binded model data (usually ID field). */
    private final String propertyToCompare;

    public ModelDataComboboxFieldBinding(ModelDataComboBox<M> modelDataComboBox, String property,
            String propertyToCompare)
    {
        super(modelDataComboBox, property);
        this.modelDataComboBox = modelDataComboBox;
        this.propertyToCompare = propertyToCompare;
    }

    public ModelDataComboboxFieldBinding(ModelDataComboBox<M> modelDataComboBox, String property)
    {
        super(modelDataComboBox, property);
        this.modelDataComboBox = modelDataComboBox;
        this.propertyToCompare = property;
    }

    @Override
    protected Object onConvertFieldValue(Object value)
    {
        if (this.modelDataComboBox.getSelection().size() > 0)
        {
            M modelData = this.modelDataComboBox.getSelection().get(0);
            return modelData.get(propertyToCompare);
        }
        return null;
    }

    @Override
    protected Object onConvertModelValue(Object value)
    {
        M returnModelData = null;
        for (M modelData : this.modelDataComboBox.getStore().getModels())
        {
            if (modelData.get(propertyToCompare).equals(value))
            {
                returnModelData = modelData;
                break;
            }
        }
        return returnModelData;
    }
}
