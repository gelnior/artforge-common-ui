package fr.hd3d.common.ui.client.widget.identitypanel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.mvc.Controller;
import com.extjs.gxt.ui.client.store.ListStore;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.service.ExcludedField;
import fr.hd3d.common.ui.client.service.ServicesField;
import fr.hd3d.common.ui.client.service.ServicesModelType;


/**
 * Controller that handles identity retrieval and explorer events for displaying selected record identity. Identity is
 * composed by classic fields displayed in a list and relations lists that displayed all items linked to the selected
 * record.
 * 
 * @author HD3D
 */
public class IdentityPanelController extends Controller
{
    /** The panel that displays selected record data. */
    final protected IIdentityPanel view;
    /** Model that contains selected record, fields list store and relation list stores. */
    final protected IdentityPanelModel model;

    /** Flag to tell if it is the first selection since sheet change. */
    final protected boolean firstSelection = true;

    /** Map containing all static fields. */
    final protected FastMap<FieldModelData> fieldMap = new FastMap<FieldModelData>();

    protected boolean isRelationVisible = true;

    /**
     * Default constructor.
     * 
     * @param model
     *            Model that contains selected record, fields list store and relation list stores.
     * @param view
     *            The panel that displays selected record data
     */
    public IdentityPanelController(IdentityPanelModel model, IIdentityPanel view)
    {
        this.model = model;
        this.view = view;

        this.registerEvents();
    }

    public void setRelationsVisible(boolean visible)
    {
        this.isRelationVisible = visible;
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();
        if (type == IdentityPanelEvents.IDENTITY_RETRIEVED)
        {
            this.onIdentityRetrieved(event);
        }
        if (type == IdentityPanelEvents.RELATIONS_ADDED)
        {
            ;
        }
        if (type == IdentityPanelEvents.RELATIONS_REMOVED)
        {
            ;
        }
        else if (type == CommonEvents.ERROR)
        {
            this.view.unmask();
        }
        else
        {
            this.forwardToChild(event);
        }
    }

    /** Clear all fields and relation values. */
    public void clear()
    {
        for (int i = 0; i < this.model.getIdentityFieldsStore().getCount(); i++)
        {
            FieldModelData field = this.model.getIdentityFieldsStore().getAt(i);
            field.setValue(null);
            this.model.getIdentityFieldsStore().update(field);
        }
        this.model.clearRelationStoresData();
    }

    /** Remove all fields and relation panels data. */
    public void clearAll()
    {
        this.view.setEmptyTitle();
        this.model.clearRelationStores();
        this.view.removePanels();

        this.model.getIdentityFieldsStore().removeAll();
    }

    /**
     * Build all field and relation panel corresponding to <i>simpleClassName</i>.
     * 
     * @param simpleClassName
     *            The reference class.
     */
    public void buildFields(String simpleClassName)
    {
        ModelType type = ServicesModelType.getModelType(simpleClassName);
        this.model.setSimpleClassName(simpleClassName);
        this.fieldMap.clear();

        for (int i = 0; i < type.getFieldCount(); i++)
        {
            DataField field = type.getField(i);

            String value = null;
            this.analyseAndAddEntry(field, value);
        }

        this.view.unmask();
    }

    /**
     * Update displayed data from given record data.
     * 
     * @param record
     *            The instance from which displayed data should be updated.
     */
    public void updateData(Hd3dModelData record)
    {
        ModelType type = ServicesModelType.getModelType(this.model.getSimpleClassName());

        for (int i = 0; i < type.getFieldCount(); i++)
        {
            DataField field = type.getField(i);

            Object value = record.get(field.getName());
            if (value == null)
            {
                value = "";
            }

            this.analyseAndUpdateEntry(field, value);
        }
    }

    /**
     * @return true if the next selection will be the first since last sheet loading.
     */
    public boolean isFirstSelection()
    {
        return this.firstSelection;
    }

    /**
     * Refresh data with object whom ID is equal to <i>id</i>.
     * 
     * @param id
     *            The ID of object to set in the panel.
     */
    public void refreshIdentityData(Long id)
    {
        this.view.mask();
        this.model.refreshIdentityData(id);
    }

    /**
     * When identity data are retrieved, it updates the identity model and refreshes the identity panel.
     * 
     * @param event
     *            event telling carrying data retrieved.
     */
    protected void onIdentityRetrieved(AppEvent event)
    {
        Hd3dModelData record = this.model.getIdentityRecord();
        this.updateData(record);

        this.view.unmask();
        this.view.refreshLayout();
    }

    /**
     * Depending on field type, it adds a row to the identity field store or it creates a relation panel (list of
     * string).
     * 
     * If the field.is excluded, it does nothing.
     * 
     * @param field
     *            The field name.
     * @param value
     *            The field value.
     */
    protected void analyseAndAddEntry(DataField field, Object value)
    {
        String key = field.getName();

        if (ExcludedField.isExcluded(key.toLowerCase()))
        {
            return;
        }
        else if (field.getType() == List.class && this.isRelationVisible)
        {
            if (key.endsWith("Names"))
            {
                this.addRelationPanel(key, new ArrayList<String>());
            }
        }
        else if (field.getType() == Long.class)
        {
            this.addIdentityField(key, "");
        }
        else
        {
            this.addIdentityField(key, null);
        }
    }

    /**
     * Depending on field type, it updates row value of the identity field store or it updates relation panel data (list
     * of string).
     * 
     * If the field.is excluded, it does nothing.
     * 
     * @param field
     *            The field name.
     * @param value
     *            The field value.
     */
    protected void analyseAndUpdateEntry(DataField field, Object value)
    {
        String key = field.getName();

        if (ExcludedField.isExcluded(key))
        {
            return;
        }
        else if (field.getType() == List.class && this.isRelationVisible)
        {
            if (key.endsWith("Names") && value != null)
            {
                if (value instanceof String)
                {
                    this.updateRelationsPanel(key, new ArrayList<String>());
                }
                else
                {
                    this.updateRelationsPanel(key, (List<?>) value);
                }
            }
        }
        else if (field.getType() == Long.class)
        {
            try
            {
                Long longValue = (Long) value;
                this.updateIdentityField(key, longValue.toString());
            }
            catch (ClassCastException e)
            {
                this.updateIdentityField(key, "");
            }
        }
        else if (field.getType() == Date.class)
        {
            try
            {
                String date = this.view.dateToString((Date) value);
                this.updateIdentityField(key, date);
            }
            catch (ClassCastException e)
            {
                this.updateIdentityField(key, "");
            }
        }
        else
        {
            this.updateIdentityField(key, value.toString());
        }
    }

    /**
     * Creates a new relation panel with <i>title</i> as name.
     * 
     * @param title
     *            The relation panel title.
     * @param value
     *            The relation panel list of values.
     */
    protected void addRelationPanel(String title, List<?> value)
    {
        List<String> relationNames = new ArrayList<String>();
        for (Object relationName : value)
        {
            relationNames.add((String) relationName);
        }

        ListStore<RecordModelData> relationStore = this.model.addRelationStore(title, relationNames);
        this.view.addRelationToIdentityPanel(title, relationStore);
    }

    /**
     * Update data contained in the relation panel which name is equals to <i>title</i>.
     * 
     * @param title
     *            The relation panel title.
     * @param value
     *            The relation panel list of values.
     */
    protected void updateRelationsPanel(String title, List<?> value)
    {
        ListStore<RecordModelData> relationStore = this.model.getRelationStore(title);
        relationStore.removeAll();

        for (Object relationName : value)
        {
            relationStore.add(new RecordModelData(null, (String) relationName));
        }
        relationStore.sort(RecordModelData.NAME_FIELD, SortDir.ASC);
    }

    /**
     * Adds a new field to the field store
     * 
     * @param name
     *            The field name.
     * @param value
     *            The field value.
     */
    protected void addIdentityField(String name, String value)
    {
        if (fieldMap.get(name) == null)
        {
            String fieldName = ServicesField.getHumanName(name);
            if (fieldName == null)
            {
                fieldName = name;
            }

            FieldModelData field = new FieldModelData(0, fieldName, value);
            fieldMap.put(name, field);

            this.model.getIdentityFieldsStore().add(field);
        }
    }

    /**
     * Updates field value with <i>value</i> in the field store
     * 
     * @param name
     *            The field name.
     * @param value
     *            The field value.
     */
    protected void updateIdentityField(String name, String value)
    {
        if (fieldMap.get(name) != null)
        {
            FieldModelData field = fieldMap.get(name);
            if (value == null)
            {
                field.setValue("");
            }
            else
            {
                field.setValue(value);
            }
            this.model.getIdentityFieldsStore().update(field);
        }
    }

    /**
     * Register events that controller can handle.
     */
    protected void registerEvents()
    {
        this.registerEventTypes(IdentityPanelEvents.IDENTITY_RETRIEVED);
        this.registerEventTypes(IdentityPanelEvents.RELATIONS_ADDED);
        this.registerEventTypes(IdentityPanelEvents.RELATIONS_REMOVED);
        this.registerEventTypes(CommonEvents.ERROR);
    }
}
