package fr.hd3d.common.ui.client.widget.identitypanel;

import com.extjs.gxt.ui.client.event.EventType;


/**
 * Event code list.
 * 
 * @author HD3D
 */
public class IdentityPanelEvents
{
    public static final EventType IDENTITY_RETRIEVED = new EventType();
    public static final EventType RELATIONS_ADDED = new EventType();
    public static final EventType RELATIONS_REMOVED = new EventType();
}
