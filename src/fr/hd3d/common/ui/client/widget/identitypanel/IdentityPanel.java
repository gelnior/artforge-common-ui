package fr.hd3d.common.ui.client.widget.identitypanel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.Style.Scroll;
import com.extjs.gxt.ui.client.mvc.Controller;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.RowLayout;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.calendar.DateFormat;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.service.ServicesField;
import fr.hd3d.common.ui.client.widget.FieldGrid;
import fr.hd3d.common.ui.client.widget.NameGrid;


/**
 * Identity panel displays record static fields as a list and displays separated panels for relations list.
 * 
 * @author HD3D
 */
public class IdentityPanel extends ContentPanel implements IIdentityPanel
{
    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    /** Model that handles list data. */
    protected final IdentityPanelModel model;
    /** Controller that handles data retrieve and explorer selection change. */
    protected final IdentityPanelController controller;

    /** Panel that contains the identity fields grid. */
    protected final ContentPanel identityFieldsPanel = new ContentPanel();
    /** List of relation panels */
    protected final List<ContentPanel> relationsPanel = new ArrayList<ContentPanel>();

    /**
     * Default constructor.
     */
    public IdentityPanel()
    {
        this(true);
    }

    public IdentityPanel(Boolean isRelationVisible)
    {
        super();
        this.model = new IdentityPanelModel();
        this.controller = new IdentityPanelController(model, this);
        this.controller.setRelationsVisible(isRelationVisible);

        EventDispatcher.get().addController(controller);
        this.setStyle();
    }

    /**
     * Set identity fields visible.
     * 
     * @param visible
     *            True to set fields visible,
     */
    public void setFieldsVisible(boolean visible)
    {
        this.identityFieldsPanel.setVisible(visible);
    }

    public void setRelationsVisible(boolean visible)
    {
        this.controller.setRelationsVisible(visible);
    }

    /**
     * Add a relation panel to the identity panel with name as title.
     */
    public void addRelationToIdentityPanel(String name, ListStore<RecordModelData> relationNames)
    {
        ContentPanel identityRelationPanel = new ContentPanel();
        identityRelationPanel.setHeading(ServicesField.getHumanName(name));
        identityRelationPanel.setLayout(new FitLayout());
        identityRelationPanel.getHeader().addStyleName(IdentityPanelConfig.IDENTITY_RELATION_HEADER_STYLE);
        identityRelationPanel.setBodyBorder(false);
        identityRelationPanel.setHeaderVisible(true);
        identityRelationPanel.setCollapsible(true);

        NameGrid identityGrid = new NameGrid(relationNames);
        identityGrid.setBorders(false);
        identityGrid.setAutoHeight(true);
        identityRelationPanel.add(identityGrid);

        this.add(identityRelationPanel);
        this.relationsPanel.add(identityRelationPanel);

        // identityRelationPanel.layout();
        // this.layout();
    }

    /**
     * Remove relation panels from identity panels.
     */
    public void removePanels()
    {
        for (ContentPanel relationPanel : relationsPanel)
        {
            this.remove(relationPanel);
        }
        this.relationsPanel.clear();
    }

    /**
     * Set title of identity fields panel.
     */
    public void setIdentityTitle(String name)
    {
        this.setHeading(name);
    }

    /**
     * Collapse identity and relation panels.
     */
    public void collapseAll()
    {
        this.identityFieldsPanel.collapse();
        for (ContentPanel relationPanel : relationsPanel)
        {
            relationPanel.collapse();
        }
    }

    /**
     * Expand identity and relation panels.
     */
    public void unCollapseAll()
    {
        this.identityFieldsPanel.expand();
        for (ContentPanel relationPanel : relationsPanel)
        {
            relationPanel.expand();
        }
    }

    /**
     * Expand identity and relation panels.
     */
    public void setEmptyTitle()
    {
        this.setHeading(CONSTANTS.Information());
    }

    /**
     * Build all fields and relation panel corresponding to <i>simpleClassName</i>.
     * 
     * @param simpleClassName
     *            The reference class.
     */
    public void buildFields(String simpleClassName)
    {
        this.setHeading(CONSTANTS.Information());
        this.controller.buildFields(simpleClassName);
    }

    /**
     * Clear all fields and relations panel.
     */
    public void clearAll()
    {
        this.controller.clearAll();
    }

    /**
     * Refresh data with object whom ID is equal to <i>id</i>.
     * 
     * @param id
     *            The ID of object to set in the panel.
     */
    public void refreshIdentityData(Long id)
    {
        this.mask(CONSTANTS.Loading());
        this.controller.refreshIdentityData(id);
    }

    /**
     * Refresh identity data directly from record. Use only if fields have been built before.
     * 
     * @param record
     *            The instance from which displayed data should be updated.
     */
    public void refreshIdentityData(Hd3dModelData record)
    {
        this.model.setIdentityRecord(record);
        this.controller.updateData(record);
    }

    /**
     * Clear data displayed.
     */
    public void clearData()
    {
        this.controller.clear();
    }

    /**
     * Refresh identity data with data coming from server.
     */
    public void refreshIdentityData()
    {
        this.refreshIdentityData(this.model.getIdentityRecord().getId());
    }

    /**
     * Transform a date object in its string format.
     */
    public String dateToString(Date value)
    {
        return DateFormat.FRENCH_DATE.format(value);
    }

    /**
     * Call layout() method on every elements of the panel.
     */
    public void refreshLayout()
    {
        for (ContentPanel panel : relationsPanel)
        {
            panel.layout();
        }
        this.layout();
    }

    /**
     * @return MVC controller.
     */
    public Controller getContoller()
    {
        return controller;
    }

    public Hd3dModelData getIdentityRecord()
    {
        return this.model.getIdentityRecord();
    }

    /**
     * Set styles : big letters for identity fields header.
     */
    protected void setStyle()
    {
        this.setLayout(new RowLayout());
        // this.setLayout(new FitLayout());
        this.setBorders(false);
        this.setScrollMode(Scroll.AUTOY);
        this.getHeader().addStyleName(IdentityPanelConfig.IDENTITY_HEADER_STYLE);

        this.identityFieldsPanel.setLayout(new FitLayout());
        this.identityFieldsPanel.setBodyBorder(false);
        this.identityFieldsPanel.setCollapsible(true);
        this.identityFieldsPanel.setScrollMode(Scroll.NONE);
        this.identityFieldsPanel.setHeaderVisible(false);
        this.identityFieldsPanel.setVisible(false);
        this.getHeader().setVisible(false);

        FieldGrid identityGrid = new FieldGrid(this.model.getIdentityFieldsStore());
        identityGrid.setHideHeaders(true);
        this.identityFieldsPanel.add(identityGrid);
        this.identityFieldsPanel.setHeight(450);

        this.setEmptyTitle();

        this.add(identityFieldsPanel);
    }

}
