package fr.hd3d.common.ui.client.widget.identitypanel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.extjs.gxt.ui.client.store.ListStore;

import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;


/**
 * Model that handles identity data : selected record, fields to display.
 * 
 * @author HD3D
 */
public class IdentityPanelModel
{
    /** The record to display in the identity panel. */
    private Hd3dModelData identityRecord = new Hd3dModelData();

    /** The field list for the identity panel. */
    private final ListStore<FieldModelData> identityFields = new ListStore<FieldModelData>();
    /** Stores needed by relation lists widgets. */
    private final Map<String, ListStore<RecordModelData>> relationStores = new HashMap<String, ListStore<RecordModelData>>();
    /** Class name of the record displayed in the identity panel. */
    private String simpleClassName;

    /**
     * @return Class name of the record displayed in the identity panel.
     */
    public String getSimpleClassName()
    {
        return this.simpleClassName;
    }

    /**
     * Sets the class name of the record displayed in the identity panel.
     * 
     * @param simpleClassName
     *            Class name of the record displayed in the identity panel.
     */
    public void setSimpleClassName(String simpleClassName)
    {
        this.simpleClassName = simpleClassName;
    }

    /**
     * @return Field list for the identity panel
     */
    public ListStore<FieldModelData> getIdentityFieldsStore()
    {
        return this.identityFields;
    }

    /**
     * @return The record to display in the identity panel
     */
    public Hd3dModelData getIdentityRecord()
    {
        return this.identityRecord;
    }

    /**
     * Sets the record to display in the identity panel.
     * 
     * @param record
     *            the record to display
     */
    public void setIdentityRecord(Hd3dModelData record)
    {
        this.identityRecord = record;
    }

    /**
     * Query web services to retrieve data about the selected record, then forwards the identity retrieved event.
     * 
     * @param id
     *            The id of the selected record.
     */
    public void refreshIdentityData(Long id)
    {
        this.identityRecord.setId(id);
        this.identityRecord.setSimpleClassName(this.simpleClassName);
        this.identityRecord.setDefaultPath(null);
        this.identityRecord.refresh(IdentityPanelEvents.IDENTITY_RETRIEVED);
    }

    /**
     * @param fieldName
     *            The relation field that will serve as list store title.
     * @param relationNames
     *            The name list to put in the newly created store.
     * @return The newly created list store filled with <i>relationNames</i>.
     */
    public ListStore<RecordModelData> addRelationStore(String fieldName, List<String> relationNames)
    {
        ListStore<RecordModelData> relationStore = new ListStore<RecordModelData>();
        for (String name : relationNames)
        {
            relationStore.add(new RecordModelData(null, name));
        }
        this.relationStores.put(fieldName, relationStore);

        return relationStore;
    }

    /**
     * @param title
     *            The title of the store to retrieve.
     * @return The relation store referenced by <i>title</i>.
     */
    public ListStore<RecordModelData> getRelationStore(String title)
    {
        return this.relationStores.get(title);
    }

    /**
     * @return The relation stores.
     */
    public Map<String, ListStore<RecordModelData>> getRelationStores()
    {
        return this.relationStores;
    }

    /**
     * Clear all relations stores data then delete them.
     */
    public void clearRelationStores()
    {
        this.clearRelationStoresData();
        this.relationStores.clear();
    }

    /**
     * Clear relations store data.
     */
    public void clearRelationStoresData()
    {
        for (ListStore<RecordModelData> relationStore : this.relationStores.values())
        {
            relationStore.removeAll();
        }
    }
}
