package fr.hd3d.common.ui.client.widget.identitypanel;

/**
 * Static variables needed in application.
 * 
 * @author HD3D
 */
public class IdentityPanelConfig
{

    /** Identity record event variable name for event dispatcher. */
    public static final String IDENTITY_EVENT_VAR_NAME = "identity_event";

    /** CSS class for styling identity panel header. */
    public static final String IDENTITY_HEADER_STYLE = "identity-header";
    /** CSS class for styling identity fields grid. */
    public static final String IDENTITY_FIELDS_STYLE = "identity-fields-grid";
    /** CSS class for styling identity relation headers. */
    public static final String IDENTITY_RELATION_HEADER_STYLE = "identity-relation-header";
    /** CSS class for styling identity relation fields. */
    public static final String IDENTITY_RELATION_FIELDS_STYLE = "identity-relation-grid";
}
