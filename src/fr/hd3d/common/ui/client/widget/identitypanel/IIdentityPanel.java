package fr.hd3d.common.ui.client.widget.identitypanel;

import java.util.Date;

import com.extjs.gxt.ui.client.core.El;
import com.extjs.gxt.ui.client.store.ListStore;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;


/**
 * Interface for identity Panel.
 * 
 * @author HD3D
 */
public interface IIdentityPanel
{
    /**
     * Add a relation panel to the main identity panel. This panel lists all objects linked to the selected one for the
     * given class name.
     * 
     * @param fieldName
     *            The field that contains relations.
     * @param recordNames
     *            The item names linked to the selected record.
     */
    public void addRelationToIdentityPanel(String fieldName, ListStore<RecordModelData> recordNames);

    /**
     * Set the identity panel title.
     * 
     * @param title
     *            The title to set.
     */
    public void setIdentityTitle(String title);

    /**
     * Remove relation panels specifically set for the current sheet.
     */
    public void removePanels();

    public void unmask();

    public void collapseAll();

    public void unCollapseAll();

    public void setEmptyTitle();

    public El mask();

    public String dateToString(Date value);

    public void refreshLayout();
}
