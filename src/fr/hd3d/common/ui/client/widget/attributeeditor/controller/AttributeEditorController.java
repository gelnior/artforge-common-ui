package fr.hd3d.common.ui.client.widget.attributeeditor.controller;

import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.mvc.Controller;

import fr.hd3d.common.ui.client.modeldata.technical.ClassDynModelData;
import fr.hd3d.common.ui.client.modeldata.technical.DynTypeModelData;
import fr.hd3d.common.ui.client.widget.attributeeditor.event.AttributeEditorEvents;
import fr.hd3d.common.ui.client.widget.attributeeditor.model.AttributeEditorModel;
import fr.hd3d.common.ui.client.widget.attributeeditor.view.IAttributeEditorView;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata.EntityModelData;


/**
 * Attribute editor controller handles attribute editor events.
 * 
 * @author HD3D
 */
public class AttributeEditorController extends Controller
{
    /** View displaying widgets. */
    private final IAttributeEditorView view;

    /**
     * Default constructor.
     * 
     * @param view
     * 
     * @param model
     */
    public AttributeEditorController(IAttributeEditorView view, AttributeEditorModel model)
    {
        this.view = view;
        this.registerEventTypes(AttributeEditorEvents.END_SAVE);
    }

    /**
     * 
     * @param entity
     */
    public void setEntityType(EntityModelData entity)
    {
        this.view.setEntityType(entity);
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        if (event.getType() == AttributeEditorEvents.END_SAVE)
        {
            this.onEndSave();
        }
        else if (event.getType() == AttributeEditorEvents.NAME_KEY_UP)
        {
            this.onNameKeyUp();
        }
        else if (event.getType() == AttributeEditorEvents.TYPE_CHANGED)
        {
            this.onTypeChanged();
        }
        else
        {
            this.forwardToChild(event);
        }
    }

    /**
     * When type is changed, save button status is updated depending on form validity.
     */
    private void onTypeChanged()
    {
        this.updateSaveButtonStatus();
    }

    /**
     * When name is changed, save button status is updated depending on form validity.
     */
    private void onNameKeyUp()
    {
        this.updateSaveButtonStatus();
    }

    /**
     * If form is valid (all fields filled), the save button is enabled.Else it is disabled.
     */
    private void updateSaveButtonStatus()
    {
        if (this.view.isFormValid())
        {
            this.view.enableSaveButton();
        }
        else
        {
            this.view.disableSaveButton();
        }
    }

    /**
     * When saving ends, the saving indcator is hidden.
     */
    private void onEndSave()
    {
        this.view.hideSaving();
        this.view.hide();
    }

    /**
     * Build dynamic meta data type from fields value provided by user. Then save it database while activating save
     * indicator.
     */
    public void save()
    {
        String name = this.view.getName();
        String defaultValue = this.view.getDefaultValue();
        DynTypeModelData dataType = this.view.getDataType();
        String entityType = this.view.getEntityType();
        String nature = this.view.getNature();

        this.view.showSaving();

        ClassDynModelData classDynMeta = new ClassDynModelData();
        classDynMeta.setName(name);
        classDynMeta.setBoundClassName(entityType);
        classDynMeta.setPredicate(ClassDynModelData.DEFAULT_PREDICATE);
        classDynMeta.setDynMetaDataType(dataType.getId());
        classDynMeta.setDefaultValue(defaultValue);
        classDynMeta.setNature(nature);

        classDynMeta.save(AttributeEditorEvents.END_SAVE);
    }

}
