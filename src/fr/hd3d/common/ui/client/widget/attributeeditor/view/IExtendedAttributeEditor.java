package fr.hd3d.common.ui.client.widget.attributeeditor.view;

import com.extjs.gxt.ui.client.widget.form.Field;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.widget.simpleexplorer.ISimpleExplorerPanel;


public interface IExtendedAttributeEditor<C extends Hd3dModelData> extends ISimpleExplorerPanel<C>
{

    public void resetSelection();

    public void editAfterAdd();
    
    public Field<? extends Object> getDefaultValue();

}
