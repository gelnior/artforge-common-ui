package fr.hd3d.common.ui.client.widget.attributeeditor.view;

import com.extjs.gxt.ui.client.event.Observable;

import fr.hd3d.common.ui.client.modeldata.technical.DynTypeModelData;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata.EntityModelData;


/**
 * Interface for attribute editor view. Useful for mocking during unit tests.
 * 
 * @author HD3D
 */
public interface IAttributeEditorView extends Observable
{
    public void setEntityType(EntityModelData entity);

    public String getName();

    public String getDefaultValue();

    public DynTypeModelData getDataType();

    public String getNature();

    public String getEntityType();

    public void enableDefaultValue();

    public void disableDefaultValue();

    public void hideSaving();

    public void hide();

    public void showSaving();

    public boolean isFormValid();

    public void disableSaveButton();

    public void enableSaveButton();

}
