package fr.hd3d.common.ui.client.widget.attributeeditor.event;

import com.extjs.gxt.ui.client.event.EventType;


/**
 * Events raised by extended attribute editor.
 * 
 * @author HD3D
 */
public class ExtendedAttributeEditorEvents
{
    public static final EventType EDITOR_HIDDEN = new EventType();
    public static final EventType EDITOR_SHOWN = new EventType();

    public static final EventType DYN_TYPE_SAVED = new EventType();
    public static final EventType LOAD_DYN_TYPES_FOR_TYPE_COMBOBOX = new EventType();

    public static final EventType LIST_VALUES_VALUES_LOADED = new EventType();
    public static final EventType LIST_VALUES_SAVED = new EventType();
}
