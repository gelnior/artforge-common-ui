package fr.hd3d.common.ui.client.widget.attributeeditor;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.reader.ClassDynReader;
import fr.hd3d.common.ui.client.modeldata.technical.ClassDynModelData;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.util.GridUtils;
import fr.hd3d.common.ui.client.widget.sheeteditor.event.SheetEditorEvents;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata.EntityModelData;
import fr.hd3d.common.ui.client.widget.simpleexplorer.SimpleExplorerDialog;


/**
 * Dialog box that allows user to define a new attribute for current project.
 * 
 * @author HD3D
 */
public class AttributeEditor extends SimpleExplorerDialog<ClassDynModelData>
{
    public static CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    private final Constraint classNameConstraint = new Constraint(EConstraintOperator.eq,
            ClassDynModelData.ENTITY_FIELD, null);

    public AttributeEditor()
    {
        super(new ClassDynReader(), "Dynamic attribute editor");

        this.explorer.reconfigureGrid(this.getColumnConfig());

        this.explorer.addParameter(classNameConstraint);
        this.explorer.hideDeleteToolItem();
        this.setWidth(300);

        EventDispatcher.get().addController(this.explorer.getController());
    }

    private List<ColumnConfig> getColumnConfig()
    {
        List<ColumnConfig> configs = new ArrayList<ColumnConfig>();
        ColumnConfig nameColumn = GridUtils.addColumnConfig(configs, ClassDynModelData.NAME_FIELD, "Name", 120);
        nameColumn.setEditor(new CellEditor(new TextField<String>()));

        ColumnConfig type = GridUtils.addColumnConfig(configs, ClassDynModelData.DYNMETADATATYPE_NAME_FIELD, "Type",
                120);

        type.setEditor(new DynTypeCellEditor(this.explorer.getStore()));
        return configs;
    }

    public void setEntityType(EntityModelData entity)
    {
        this.classNameConstraint.setLeftMember(entity.getName());

        ClassDynModelData instance = new ClassDynModelData();
        instance.setEntityName(entity.getName());

        this.explorer.setDefaultInstance(instance);
    }

    @Override
    public void show()
    {
        super.show();
        EventDispatcher.forwardEvent(SheetEditorEvents.ATTRIBUTE_EDITOR_SHOWN);
    }

    @Override
    public void hide()
    {
        super.hide();
        EventDispatcher.forwardEvent(SheetEditorEvents.ATTRIBUTE_EDITOR_HIDDEN);
    }
}
