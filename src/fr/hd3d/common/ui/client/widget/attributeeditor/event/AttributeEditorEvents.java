package fr.hd3d.common.ui.client.widget.attributeeditor.event;

import com.extjs.gxt.ui.client.event.EventType;


/**
 * Events raised by attribute editor.
 * 
 * @author HD3D
 */
public class AttributeEditorEvents
{
    public static final int ATTRIBUTE_EDITOR_EVENTS = 13500;

    public static final EventType SAVE = new EventType();
    public static final EventType CANCEL = new EventType(ATTRIBUTE_EDITOR_EVENTS + 1);

    public static final EventType END_SAVE = new EventType(ATTRIBUTE_EDITOR_EVENTS + 11);
    public static final EventType NAME_KEY_UP = new EventType(ATTRIBUTE_EDITOR_EVENTS + 12);
    public static final EventType TYPE_CHANGED = new EventType(ATTRIBUTE_EDITOR_EVENTS + 13);
}
