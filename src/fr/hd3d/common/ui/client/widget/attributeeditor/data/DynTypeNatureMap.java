package fr.hd3d.common.ui.client.widget.attributeeditor.data;

import com.extjs.gxt.ui.client.core.FastMap;

import fr.hd3d.common.client.Const;
import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.util.FieldUtils;


public class DynTypeNatureMap extends FastMap<FieldModelData>
{
    private static final long serialVersionUID = 816409451137660203L;

    public DynTypeNatureMap()
    {
        FieldUtils.addFieldToMap(this, 0, "Simple value", Const.DYNMETADATATYPE_SIMPLE);
        FieldUtils.addFieldToMap(this, 1, "Choose list values", Const.JAVA_UTIL_LIST);
    }
}
