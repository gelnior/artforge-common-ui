package fr.hd3d.common.ui.client.widget.attributeeditor.model;

import java.util.HashMap;
import java.util.Map;

import com.extjs.gxt.ui.client.store.Record;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.reader.DynTypeReader;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.modeldata.technical.ClassDynModelData;
import fr.hd3d.common.ui.client.modeldata.technical.DynTypeModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ListValuesModelData;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.widget.simpleexplorer.SimpleExplorerModel;


public class ExtendedAttributeEditorModel<C extends Hd3dModelData, D extends Hd3dModelData> extends
        SimpleExplorerModel<C>
{
    private Boolean newAttribute = false;
    private Map<String, D> addDynTypeMap = new HashMap<String, D>();
    private Map<String, D> updateDynTypeMap = new HashMap<String, D>();
    private Map<String, ListValuesModelData> addListValuesMap = new HashMap<String, ListValuesModelData>();
    private Map<String, ListValuesModelData> updateListValuesMap = new HashMap<String, ListValuesModelData>();
    private ServiceStore<DynTypeModelData> dynTypeStoreForType = new ServiceStore<DynTypeModelData>(new DynTypeReader());

    public ExtendedAttributeEditorModel(IReader<C> reader)
    {
        super(reader);
    }

    public Boolean getNewAttribute()
    {
        return newAttribute;
    }

    public void setNewAttribute(Boolean newAttribute)
    {
        this.newAttribute = newAttribute;
    }

    public Map<String, D> getAddDynTypeMap()
    {
        return addDynTypeMap;
    }

    public Map<String, D> getUpdateDynTypeMap()
    {
        return updateDynTypeMap;
    }

    public Map<String, ListValuesModelData> getAddListValuesMap()
    {
        return addListValuesMap;
    }

    public Map<String, ListValuesModelData> getUpdateListValuesMap()
    {
        return updateListValuesMap;
    }

    public ServiceStore<DynTypeModelData> getDynTypeStoreForType()
    {
        return dynTypeStoreForType;
    }

    public ListValuesModelData getListValueModelDataByStructId(Long structId)
    {
        ListValuesModelData listValuesModelData = null;
        listValuesModelData = this.addListValuesMap.get(structId.toString());
        if (listValuesModelData == null)
        {
            listValuesModelData = this.updateListValuesMap.get(structId.toString());
        }
        return listValuesModelData;
    }

    public void clearSavedCollection()
    {
        this.addDynTypeMap.clear();
        this.updateDynTypeMap.clear();
        this.addListValuesMap.clear();
        this.updateListValuesMap.clear();
    }

    @SuppressWarnings("unchecked")
    public void updateClassDynModelData()
    {
        for (Record record : modelStore.getModifiedRecords())
        {
            C modelData = (C) record.getModel();
            if (modelData instanceof ClassDynModelData)
            {
                ClassDynModelData classDynModelData = (ClassDynModelData) modelData;
                if (classDynModelData.getDynMetaDataType().intValue() <= 0)
                {
                    D dynTypeModelData = addDynTypeMap.get(classDynModelData.getDynMetaDataType().toString());
                    classDynModelData.setDynMetaDataType(dynTypeModelData.getId());
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    public D getDynTypeById(Long dynMetaDataTypeId)
    {
        D dynTypeModelData = null;
        if (dynMetaDataTypeId != null)
        {
            dynTypeModelData = (D) dynTypeStoreForType.findModel(DynTypeModelData.ID_FIELD, dynMetaDataTypeId);
            if (dynTypeModelData == null)
            {
                dynTypeModelData = addDynTypeMap.get(dynMetaDataTypeId.toString());
            }
            if (dynTypeModelData == null)
            {
                dynTypeModelData = updateDynTypeMap.get(dynMetaDataTypeId.toString());
            }
        }
        return dynTypeModelData;
    }

}
