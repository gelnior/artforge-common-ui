package fr.hd3d.common.ui.client.widget.attributeeditor;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.Style.ButtonScale;
import com.extjs.gxt.ui.client.Style.LayoutRegion;
import com.extjs.gxt.ui.client.Style.SelectionMode;
import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.KeyListener;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.SelectionChangedEvent;
import com.extjs.gxt.ui.client.event.SelectionChangedListener;
import com.extjs.gxt.ui.client.store.Record;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.Status;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.extjs.gxt.ui.client.widget.form.ComboBox;
import com.extjs.gxt.ui.client.widget.form.ComboBox.TriggerAction;
import com.extjs.gxt.ui.client.widget.form.DateField;
import com.extjs.gxt.ui.client.widget.form.Field;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.form.NumberField;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.layout.BorderLayout;
import com.extjs.gxt.ui.client.widget.layout.BorderLayoutData;
import com.extjs.gxt.ui.client.widget.toolbar.FillToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.ToolBar;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.i18n.client.NumberFormat;

import fr.hd3d.common.client.Const;
import fr.hd3d.common.client.Editor;
import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.reader.ClassDynReader;
import fr.hd3d.common.ui.client.modeldata.reader.DynTypeReader;
import fr.hd3d.common.ui.client.modeldata.technical.ClassDynModelData;
import fr.hd3d.common.ui.client.modeldata.technical.DynTypeModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ListValuesModelData;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.util.GridUtils;
import fr.hd3d.common.ui.client.widget.DynTypeListValuesField;
import fr.hd3d.common.ui.client.widget.EditableNameGrid;
import fr.hd3d.common.ui.client.widget.FieldComboBox;
import fr.hd3d.common.ui.client.widget.FilterStoreField;
import fr.hd3d.common.ui.client.widget.ModelDataComboBox;
import fr.hd3d.common.ui.client.widget.PersonComboBox;
import fr.hd3d.common.ui.client.widget.ToolBarButton;
import fr.hd3d.common.ui.client.widget.attributeeditor.controller.ExtendedAttributeEditorController;
import fr.hd3d.common.ui.client.widget.attributeeditor.data.DynTypeNatureMap;
import fr.hd3d.common.ui.client.widget.attributeeditor.event.ExtendedAttributeEditorEvents;
import fr.hd3d.common.ui.client.widget.attributeeditor.model.ExtendedAttributeEditorModel;
import fr.hd3d.common.ui.client.widget.attributeeditor.view.IExtendedAttributeEditor;
import fr.hd3d.common.ui.client.widget.dialog.ConfirmationDisplayer;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata.EntityModelData;
import fr.hd3d.common.ui.client.widget.simpleexplorer.SimpleExplorerEvents;


public class ExtendedAttributeEditor extends Dialog implements IExtendedAttributeEditor<ClassDynModelData>
{
    public static final String DEFAULT_DYNTYPE_NAME = "@@defaultDynTypeModelName@@";
    public static final CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    /** Simple explorer needed to handle CRUD operations on M data. */
    protected final EditableNameGrid<ClassDynModelData> explorer;
    protected final ExtendedAttributeEditorModel<ClassDynModelData, DynTypeModelData> model;
    protected final ExtendedAttributeEditorController<ClassDynModelData, DynTypeModelData> controller;
    private final BorderLayout layout;
    private final Constraint classNameConstraint = new Constraint(EConstraintOperator.eq,
            ClassDynModelData.ENTITY_FIELD, null);

    /** Tool bar with create, delete, refresh and save buttons. */
    protected ToolBar toolBar = new ToolBar();
    /** Add tool item raise the ADD_CLICKED event when clicked. */
    protected ToolBarButton addToolItem = new ToolBarButton(Hd3dImages.getMediumAddIcon(), CONSTANTS.AddRow(),
            SimpleExplorerEvents.ADD_CLICKED);
    /** Delete tool item raise the DELETE_CLICKED event when clicked. */
    protected ToolBarButton deleteToolItem = new ToolBarButton(Hd3dImages.getDeleteIcon(), CONSTANTS.DeleteRow(),
            SimpleExplorerEvents.DELETE_CLICKED);
    /** Refresh tool item raise the REFRESH_CLICKED event when clicked. */
    protected ToolBarButton refreshToolItem = new ToolBarButton(Hd3dImages.getRefreshIcon(), CONSTANTS.Refresh(),
            SimpleExplorerEvents.REFRESH_CLICKED);
    /** Save tool item raise the SAVE_CLICKED event when clicked. */
    protected ToolBarButton saveToolItem = new ToolBarButton(Hd3dImages.getMediumSaveIcon(), CONSTANTS.Save(),
            SimpleExplorerEvents.SAVE_CLICKED);

    protected FilterStoreField<ClassDynModelData> filterField;
    private final ModelDataComboBox<DynTypeModelData> typeComboBox;
    /** Displays an animated GIF showing that application is saving data. */
    protected final Status savingToolItem = new Status();
    private FormPanel panel;

    private Field<? extends Object> defaultValueField;
    private final FieldComboBox natureComboBox;
    private DynTypeListValuesField valuesListField;
    private DynTypeModelData associatedDynTypeModelData;

    public ExtendedAttributeEditor()
    {
        layout = new BorderLayout();
        this.model = new ExtendedAttributeEditorModel<ClassDynModelData, DynTypeModelData>(new ClassDynReader());
        this.controller = new ExtendedAttributeEditorController<ClassDynModelData, DynTypeModelData>(model, this);
        this.explorer = new EditableNameGrid<ClassDynModelData>(model.getModelStore());
        this.filterField = new FilterStoreField<ClassDynModelData>(this.model.getModelStore());
        DynTypeNatureMap classDynNatureMap = new DynTypeNatureMap();
        natureComboBox = FieldUtils.getComboFromMap(classDynNatureMap);
        typeComboBox = new ModelDataComboBox<DynTypeModelData>(new DynTypeReader()) {
            @Override
            protected void setStyles()
            {
                setEditable(true);
                this.setValidateOnBlur(false);
                this.setTriggerAction(TriggerAction.ALL);
                this.setDisplayField(RecordModelData.NAME_FIELD);
                this.setMinChars(255);
            }
        };
        typeComboBox.setStore(model.getDynTypeStoreForType());
        typeComboBox.setEditable(true);
        this.setLayout(layout);
        this.setHeading("Dynamic attribute editor");
        reconfigureGrid();
        this.setToolbar();
        this.addListeners();
        this.add(explorer, new BorderLayoutData(LayoutRegion.WEST, 260));
        this.add(createForm(), new BorderLayoutData(LayoutRegion.CENTER));
        this.setWidth(600);
        this.setHeight(500);
        showAllFields(false);
        this.setButtons("");
        this.natureComboBox.setVisible(false);
        EventDispatcher.get().addController(this.controller);
        EventDispatcher.forwardEvent(ExtendedAttributeEditorEvents.LOAD_DYN_TYPES_FOR_TYPE_COMBOBOX);
    }

    private void addListeners()
    {
        this.explorer.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        this.explorer.addListener(Events.AfterEdit, new Listener<GridEvent<ClassDynModelData>>() {

            public void handleEvent(GridEvent<ClassDynModelData> be)
            {
                if (be.getProperty().equals(ClassDynModelData.NAME_FIELD))
                {
                    if (!"".equals(be.getValue()) && be.getValue() != null)
                    {
                        model.setNewAttribute(Boolean.FALSE);
                    }
                }
            }
        });
        this.explorer.getSelectionModel().addSelectionChangedListener(
                new SelectionChangedListener<ClassDynModelData>() {
                    @Override
                    public void selectionChanged(SelectionChangedEvent<ClassDynModelData> se)
                    {
                        if (se.getSelection().size() > 0)
                        {
                            panel.reset();
                            ClassDynModelData classDynModelData = se.getSelection().get(0);
                            associatedDynTypeModelData = null;
                            if (classDynModelData.getId() != null)
                            {
                                showAllFields(true);
                                associatedDynTypeModelData = model.getDynTypeById(classDynModelData
                                        .getDynMetaDataType());
                                bindClassDynModelData(classDynModelData, associatedDynTypeModelData, true);
                            }
                            else
                            {
                                associatedDynTypeModelData = model.getDynTypeById(classDynModelData
                                        .getDynMetaDataType());
                                if (associatedDynTypeModelData == null)
                                {
                                    associatedDynTypeModelData = new DynTypeModelData();
                                    associatedDynTypeModelData.setId(Long.valueOf(-(model.getAddDynTypeMap().size())));
                                    associatedDynTypeModelData.setName(DEFAULT_DYNTYPE_NAME);
                                    classDynModelData.setDynMetaDataType(associatedDynTypeModelData.getId());
                                    model.getAddDynTypeMap().put(associatedDynTypeModelData.getId().toString(),
                                            associatedDynTypeModelData);
                                    showAllFields(false);
                                    removeDefaultAndValuesField();
                                    panel.layout();
                                }
                                else
                                {
                                    bindClassDynModelData(classDynModelData, associatedDynTypeModelData, true);
                                }
                            }
                        }
                    }
                });
        this.explorer.addListener(Events.BeforeEdit, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                if (explorer.getSelectionModel() != null && explorer.getSelectionModel().getSelectedItem() != null
                        && !explorer.getSelectionModel().getSelectedItem().getUserCanUpdate())
                {
                    be.setCancelled(true);
                }
            }
        });
        this.natureComboBox.addSelectionChangedListener(new SelectionChangedListener<FieldModelData>() {
            @Override
            public void selectionChanged(SelectionChangedEvent<FieldModelData> se)
            {
                if (se.getSelection().size() > 0)
                {
                    String nature = se.getSelectedItem().getValue().toString();
                    resetForm();
                    typeComboBox.setVisible(true);
                    typeComboBox.getServiceStore().reload();
                    typeComboBox.getServiceStore().filter(DynTypeModelData.NATURE_FIELD, nature);
                    changeTypeComboBoxLabelFiel(nature);
                    if (se.getSelectedItem() != null && associatedDynTypeModelData != null)
                    {
                        updateDynType(associatedDynTypeModelData, DynTypeModelData.NATURE_FIELD, se.getSelectedItem()
                                .getValue());
                    }
                }
            }
        });
        this.typeComboBox.addKeyListener(new KeyListener() {
            @Override
            public void componentKeyDown(ComponentEvent event)
            {
                if (event.getKeyCode() == KeyCodes.KEY_ENTER)
                {
                    if (!typeComboBox.getRawValue().isEmpty() && associatedDynTypeModelData != null)
                    {
                        updateDynType(associatedDynTypeModelData, DynTypeModelData.NAME_FIELD,
                                typeComboBox.getRawValue());
                        if (associatedDynTypeModelData.getId().intValue() <= 0)
                        {
                            typeComboBox.getStore().add(associatedDynTypeModelData);
                            if (Const.JAVA_UTIL_LIST.equals(associatedDynTypeModelData.getNature())
                                    && associatedDynTypeModelData.getStructId() == null)
                            {
                                ListValuesModelData listValuesModelData = new ListValuesModelData();
                                listValuesModelData.setId(Long.valueOf(-model.getAddListValuesMap().size()));
                                listValuesModelData.setSeparator(";");
                                model.getAddListValuesMap().put(listValuesModelData.getId().toString(),
                                        listValuesModelData);
                                associatedDynTypeModelData.setStructId(listValuesModelData.getId());
                            }
                            typeComboBox.setValue(associatedDynTypeModelData);
                        }
                    }
                }
            }
        });
        this.typeComboBox.addSelectionChangedListener(new SelectionChangedListener<DynTypeModelData>() {
            @Override
            public void selectionChanged(SelectionChangedEvent<DynTypeModelData> se)
            {
                if (explorer.getSelection().size() > 0 && se.getSelectedItem() != null)
                {
                    ClassDynModelData classDynModelData = explorer.getSelection().get(0);
                    DynTypeModelData associatedDynTypeModelData = se.getSelectedItem();
                    // valueTypeComboBox.getServiceStore().filter(DynTypeModelData.NATURE_FIELD);
                    // valueTypeComboBox.setVisible(true);
                    if (associatedDynTypeModelData != null)
                    {
                        updateClassDyn(classDynModelData, ClassDynModelData.DYNMETADATATYPE_NAME_FIELD, se
                                .getSelectedItem().getName());
                        updateClassDyn(classDynModelData, ClassDynModelData.DYNMETADATATYPE_FIELD, se.getSelectedItem()
                                .getId());
                        if (defaultValueField == null && valuesListField == null)
                        {
                            createDefaultValueField(associatedDynTypeModelData);
                        }
                        // setValueComboBoxValue(associatedDynTypeModelData);
                    }
                }
            }
        });
    }

    private void changeTypeComboBoxLabelFiel(String nature)
    {
        if (Const.DYNMETADATATYPE_SIMPLE.equals(nature))
        {
            typeComboBox.setFieldLabel(CONSTANTS.Type());
        }
        else if (Const.JAVA_UTIL_LIST.equals(nature))
        {
            typeComboBox.setFieldLabel(CONSTANTS.ListName());
        }
    }

    protected void updateDynType(DynTypeModelData dynTypeModelData, String property, Object value)
    {
        dynTypeModelData.set(property, value);
        if (dynTypeModelData.getId().intValue() <= 0)
        {
            model.getAddDynTypeMap().put(dynTypeModelData.getId().toString(), dynTypeModelData);
        }
        else
        {
            model.getUpdateDynTypeMap().put(dynTypeModelData.getId().toString(), dynTypeModelData);
        }
    }

    private void showAllFields(boolean visible)
    {
        if (!this.natureComboBox.isVisible())
        {
            this.natureComboBox.setVisible(true);
        }
        this.typeComboBox.setVisible(visible);
        // this.valueTypeComboBox.setVisible(visible);
    }

    /**
     * Updates the model's value with the field value.
     * 
     * @param value
     * @param property
     */
    public void updateClassDyn(ClassDynModelData modelData, String property, Object value)
    {
        if (model.getModelStore() != null)
        {
            Record r = model.getModelStore().getRecord(modelData);
            if (r != null)
            {
                r.setValid(property, true);
                r.set(property, value);
            }
        }
    }

    private FormPanel createForm()
    {
        panel = new FormPanel();
        panel.setBorders(false);
        natureComboBox.setFieldLabel("Nature");
        natureComboBox.setName(DynTypeModelData.NATURE_FIELD);
        typeComboBox.setFieldLabel(CONSTANTS.Type());
        typeComboBox.setName(DynTypeModelData.NAME_FIELD);
        typeComboBox.setEmptyText(CONSTANTS.DynTypeNameEmptyText());
        // valueTypeComboBox.setFieldLabel(CONSTANTS.DynTypeValuesType());
        // valueTypeComboBox.setName(DynTypeModelData.TYPE_FIELD);
        panel.add(natureComboBox);
        panel.add(typeComboBox);
        // panel.add(valueTypeComboBox);
        return panel;
    }

    private void resetForm()
    {
        this.typeComboBox.clear();
        removeDefaultAndValuesField();
    }

    private void createDefaultValueField(DynTypeModelData associatedDynTypeModelData)
    {
        String typeName = associatedDynTypeModelData.getType();
        removeDefaultAndValuesField();
        if (DynTypeModelData.SIMPLE_NATURE.equals(associatedDynTypeModelData.getNature()))
        {
            if (associatedDynTypeModelData.getId() == null)
            {
                associatedDynTypeModelData.setStructId(null);
            }
            if (Const.JAVA_LANG_STRING.equals(typeName))
            {
                defaultValueField = new TextField<String>();
            }
            else if (Const.JAVA_LANG_BOOLEAN.equals(typeName))
            {
                defaultValueField = new CheckBox();
            }
            else if (Const.JAVA_UTIL_DATE.equals(typeName))
            {
                defaultValueField = new DateField();
            }
            else if (Const.JAVA_LANG_LONG.equals(typeName))
            {
                defaultValueField = new NumberField();
                ((NumberField) defaultValueField).setFormat(NumberFormat.getFormat("0*"));
                ((NumberField) defaultValueField).setAllowDecimals(false);
            }
            else if (Editor.PERSON.equals(typeName))
            {
                defaultValueField = new PersonComboBox();
            }
            if (defaultValueField != null)
            {
                defaultValueField.setFieldLabel("Default Value");
                defaultValueField.setName(ClassDynModelData.DEFAULT_VALUE_FIELD);
                panel.add(defaultValueField);
                panel.layout(true);
            }
        }
        else
        {
            valuesListField = new DynTypeListValuesField(associatedDynTypeModelData, model);
            this.defaultValueField = createListValuesComboboxFromValuesList();
            valuesListField.setFieldLabel("Values");
            panel.add(valuesListField);
            if (this.defaultValueField != null)
            {
                defaultValueField.setFieldLabel("Default Value");
                defaultValueField.setName(ClassDynModelData.DEFAULT_VALUE_FIELD);
                panel.add(this.defaultValueField);
            }
            panel.layout(true);
        }
    }

    private Field<? extends Object> createListValuesComboboxFromValuesList()
    {
        ComboBox<FieldModelData> comboBox = null;
        if (this.valuesListField != null)
        {
            comboBox = new ComboBox<FieldModelData>();
            comboBox.setStore(this.valuesListField.getListStore());
            comboBox.setForceSelection(true);
            comboBox.setTriggerAction(TriggerAction.ALL);
            comboBox.setEditable(false);
            comboBox.setValidateOnBlur(false);
            comboBox.setDisplayField(RecordModelData.NAME_FIELD);
            comboBox.addSelectionChangedListener(new SelectionChangedListener<FieldModelData>() {
                @Override
                public void selectionChanged(SelectionChangedEvent<FieldModelData> se)
                {
                    FieldModelData selectedItem = se.getSelectedItem();
                    if (selectedItem != null)
                    {
                        associatedDynTypeModelData.setDefaultValue(selectedItem.getName());
                        if (associatedDynTypeModelData.getId().intValue() <= 0)
                        {
                            model.getAddDynTypeMap().put(associatedDynTypeModelData.getId().toString(),
                                    associatedDynTypeModelData);
                        }
                        else
                        {
                            model.getUpdateDynTypeMap().put(associatedDynTypeModelData.getId().toString(),
                                    associatedDynTypeModelData);
                        }
                    }
                }
            });
        }
        return comboBox;
    }

    private void removeDefaultAndValuesField()
    {
        boolean defaultValueInitialize = (defaultValueField != null);
        if (valuesListField != null)
        {
            panel.remove(valuesListField);
            valuesListField = null;
        }
        if (defaultValueInitialize)
        {
            panel.remove(defaultValueField);
            defaultValueField = null;
        }
    }

    /**
     * Set tool bar tool items : add, delete, refresh, save and saving status.
     */
    protected void setToolbar()
    {
        this.setTopComponent(toolBar);
        this.addToolItem.setScale(ButtonScale.MEDIUM);
        this.saveToolItem.setScale(ButtonScale.MEDIUM);
        this.toolBar.add(addToolItem);
        // this.toolBar.add(deleteToolItem);
        this.toolBar.add(refreshToolItem);
        this.toolBar.add(saveToolItem);
        this.toolBar.add(savingToolItem);
        this.toolBar.add(new FillToolItem());
        this.toolBar.add(filterField);
        this.toolBar.setStyleAttribute("padding-right", "5px");
        this.savingToolItem.setBusy(CONSTANTS.Saving());
        this.savingToolItem.hide();
    }

    private List<ColumnConfig> getColumnConfig()
    {
        List<ColumnConfig> configs = new ArrayList<ColumnConfig>();
        ColumnConfig nameColumn = GridUtils.addColumnConfig(configs, RecordModelData.NAME_FIELD, CONSTANTS.Name(), 120);
        nameColumn.setResizable(true);
        nameColumn.setEditor(new CellEditor(new TextField<String>()));
        GridUtils.addColumnConfig(configs, ClassDynModelData.DYNMETADATATYPE_NAME_FIELD, "Type Name", 120);
        return configs;
    }

    /**
     * Reconfigure simple explorer grid with column model given in parameter.
     * 
     */
    public void reconfigureGrid()
    {
        ColumnModel cm = new ColumnModel(this.getColumnConfig());
        this.explorer.reconfigure(this.model.getModelStore(), cm);
        this.explorer.setHideHeaders(false);
        this.model.getModelStore().addParameter(classNameConstraint);
    }

    @Override
    public void show()
    {
        super.show();
        this.model.clearCreatedRecords();
        this.model.clearDeletedRecords();
        this.model.refresh();
        this.controller.unMask();
        EventDispatcher.forwardEvent(ExtendedAttributeEditorEvents.EDITOR_SHOWN);
    }

    public void setEntityType(EntityModelData entity)
    {
        this.classNameConstraint.setLeftMember(entity.getName());
        ClassDynModelData instance = new ClassDynModelData();
        instance.setEntityName(entity.getName());
        this.model.setDefaultInstance(instance);
    }

    private void bindClassDynModelData(ClassDynModelData classDynModelData,
            DynTypeModelData associatedDynTypeModelData, boolean editMode)
    {
        if (associatedDynTypeModelData != null)
        {
            FieldModelData fieldModelData = this.natureComboBox.getStore().findModel(FieldModelData.VALUE_FIELD,
                    associatedDynTypeModelData.getNature());
            // setValueComboBoxValue(associatedDynTypeModelData);
            if (fieldModelData != null)
            {
                natureComboBox.setValue(fieldModelData);
            }
            this.typeComboBox.setValue(associatedDynTypeModelData);
        }
        if (editMode)
        {
            panel.setHeading("Edit type for attribute : " + classDynModelData.getName());
        }
        else
        {
            panel.setHeading("Add new value");
        }
    }


    /** Update selection data to ensure display will be correctly cleaned, then clear selection. */
    public void resetSelection()
    {
        this.explorer.resetSelection();
    }

    public void editAfterAdd()
    {
        this.explorer.getView().scrollToTop();
        this.explorer.startEditing(0, 0);
    }

    @Override
    public void hide()
    {
        this.controller.mask();
        EventDispatcher.forwardEvent(ExtendedAttributeEditorEvents.EDITOR_HIDDEN);
        this.model.setNewAttribute(Boolean.FALSE);
        super.hide();
    }

    public void disableButtons()
    {
        this.saveToolItem.disable();
        this.refreshToolItem.disable();
    }

    public void displaySaveConfirmBox()
    {
        ConfirmationDisplayer.display(CONSTANTS.Confirm(), CONSTANTS.YouAreGoingToDeleteData(),
                SimpleExplorerEvents.SAVE_CONFIRMED);
    }

    public void enableButtons()
    {
        this.saveToolItem.enable();
        this.refreshToolItem.enable();
    }

    public void enableCreateToolItem(boolean enabled)
    {
        this.addToolItem.setEnabled(enabled);
    }

    public void enableDeleteToolItem(boolean enabled)
    {
        this.deleteToolItem.setEnabled(enabled);
    }

    public void enableSaveToolItem(boolean enabled)
    {
        this.saveToolItem.setEnabled(enabled);
    }

    public String getNewItemString()
    {
        return CONSTANTS.NewItem();
    }

    public List<ClassDynModelData> getSelection()
    {
        return this.explorer.getSelectionModel().getSelectedItems();
    }

    public void hideSaving()
    {
        savingToolItem.hide();
    }

    public void showSaving()
    {
        savingToolItem.show();
    }

    public void idle()
    {
        this.controller.mask();
    }

    public void unIdle()
    {
        this.controller.unMask();
    }

    public Field<? extends Object> getDefaultValue()
    {
        return defaultValueField;
    }
}
