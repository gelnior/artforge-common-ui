package fr.hd3d.common.ui.client.widget.attributeeditor.controller;

import java.io.IOException;

import org.restlet.client.Request;
import org.restlet.client.Response;
import org.restlet.client.ext.json.JsonRepresentation;

import com.extjs.gxt.ui.client.data.ListLoadResult;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.form.ComboBox;
import com.extjs.gxt.ui.client.widget.form.Field;

import fr.hd3d.common.client.Const;
import fr.hd3d.common.ui.client.config.CommonConfig;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.reader.DynTypeReader;
import fr.hd3d.common.ui.client.modeldata.technical.DynTypeModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ListValuesModelData;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.widget.attributeeditor.ExtendedAttributeEditor;
import fr.hd3d.common.ui.client.widget.attributeeditor.event.ExtendedAttributeEditorEvents;
import fr.hd3d.common.ui.client.widget.attributeeditor.model.ExtendedAttributeEditorModel;
import fr.hd3d.common.ui.client.widget.attributeeditor.view.IExtendedAttributeEditor;
import fr.hd3d.common.ui.client.widget.simpleexplorer.SimpleExplorerController;


public class ExtendedAttributeEditorController<C extends Hd3dModelData, D extends Hd3dModelData> extends
        SimpleExplorerController<C>
{
    private static final String BEFORE_SAVED_ID = "BEFORE_SAVED_ID";
    private final ExtendedAttributeEditorModel<C, D> model;
    private final IExtendedAttributeEditor<C> view;
    private int nbRequest = 0;
    private boolean isSaving;
    private static boolean DEBUG = false;

    public ExtendedAttributeEditorController(ExtendedAttributeEditorModel<C, D> model, IExtendedAttributeEditor<C> view)
    {
        super(model, view);
        this.model = model;
        this.view = view;
    }

    @Override
    protected void registerEvents()
    {
        super.registerEvents();
        registerEventTypes(ExtendedAttributeEditorEvents.LIST_VALUES_SAVED);
        registerEventTypes(ExtendedAttributeEditorEvents.LOAD_DYN_TYPES_FOR_TYPE_COMBOBOX);
        registerEventTypes(ExtendedAttributeEditorEvents.DYN_TYPE_SAVED);
        registerEventTypes(ExtendedAttributeEditorEvents.LIST_VALUES_VALUES_LOADED);
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        super.handleEvent(event);
        if (ExtendedAttributeEditorEvents.LIST_VALUES_SAVED.equals(event.getType()))
        {
            onListValuesSaved(event);
        }
        else if (ExtendedAttributeEditorEvents.LOAD_DYN_TYPES_FOR_TYPE_COMBOBOX.equals(event.getType()))
        {
            loadDynTypes();
        }
        else if (ExtendedAttributeEditorEvents.DYN_TYPE_SAVED.equals(event.getType()))
        {
            onDynTypeSaved(event);
        }
        else if (ExtendedAttributeEditorEvents.LIST_VALUES_VALUES_LOADED.equals(event.getType()))
        {
            setDefaultValue(event);
        }
    }

    @SuppressWarnings("unchecked")
    private void setDefaultValue(AppEvent event)
    {
        Field<? extends Object> defaultCombobox = this.view.getDefaultValue();
        DynTypeModelData associatedDynTypeModelData = event.getData();
        if ((defaultCombobox instanceof ComboBox<?>) && associatedDynTypeModelData != null
                && associatedDynTypeModelData.getDefaultValue() != null)
        {
            ComboBox<FieldModelData> comboBox = (ComboBox<FieldModelData>) defaultCombobox;
            FieldModelData defaultValue = comboBox.getStore().findModel(FieldModelData.NAME_FIELD,
                    associatedDynTypeModelData.getDefaultValue());
            comboBox.setValue(defaultValue);
        }
    }

    @SuppressWarnings("unchecked")
    private void onDynTypeSaved(AppEvent event)
    {
        D dynTypeModelData = (D) event.getData(CommonConfig.MODEL_EVENT_VAR_NAME);
        if (dynTypeModelData != null && dynTypeModelData.get(BEFORE_SAVED_ID) != null)
        {
            model.getAddDynTypeMap().put(dynTypeModelData.get(BEFORE_SAVED_ID).toString(), dynTypeModelData);
        }
        nbRequest--;
        model.updateClassDynModelData();
        model.clearSavedCollection();
        saveOrUpdateDynTypes();
    }

    private void onListValuesSaved(AppEvent event)
    {
        ListValuesModelData valuesModelData = event.getData(CommonConfig.MODEL_EVENT_VAR_NAME);
        if (valuesModelData != null && valuesModelData.get(BEFORE_SAVED_ID) != null)
        {
            model.getAddListValuesMap().put(valuesModelData.get(BEFORE_SAVED_ID).toString(), valuesModelData);
        }
        nbRequest--;
        saveOrUpdateDynTypes();
    }

    private void loadDynTypes()
    {
        RestRequestHandlerSingleton.getInstance().getRequest(ServicesPath.DYN_METADATA_TYPES, new BaseCallback() {
            @Override
            protected void onSuccess(Request request, Response response)
            {
                JsonRepresentation jsonRepresentation = new JsonRepresentation(response.getEntity());
                DynTypeReader dynTypeReader = new DynTypeReader();
                try
                {
                    ListLoadResult<DynTypeModelData> result = dynTypeReader.read(new DynTypeModelData(),
                            jsonRepresentation.getText());
                    model.getDynTypeStoreForType().add(result.getData());
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onAddClicked()
    {
        if (!this.model.getNewAttribute())
        {
            C newAttribute = this.model.getNewModelInstance();
            this.model.setNewAttribute(Boolean.TRUE);
            if (this.model.getModelStore() != null)
            {
                this.view.resetSelection();
                this.model.getModelStore().insert(newAttribute, 0);
                this.view.editAfterAdd();
            }
        }
    }

    @Override
    protected void onSaveClicked()
    {
        nbRequest = model.getAddListValuesMap().size() + model.getUpdateListValuesMap().size();
        if (model.getAddListValuesMap().size() > 0)
        {
            saveAddListValues();
        }
        if (model.getUpdateListValuesMap().size() > 0)
        {
            updateAddListValues();
        }
        saveOrUpdateDynTypes();
    }

    private void startSaving()
    {
        this.isSaving = true;
        this.view.showSaving();
        this.view.disableButtons();
    }

    private void saveOrUpdateDynTypes()
    {
        if (nbRequest == 0)
        {
            nbRequest = model.getAddDynTypeMap().size() + model.getUpdateDynTypeMap().size();
            if (model.getAddDynTypeMap().size() > 0)
            {
                saveDynTypes();
            }
            if (model.getUpdateDynTypeMap().size() > 0)
            {
                updateDynTypes();
            }
            if (nbRequest == 0 && !DEBUG)
            {
                super.onSaveClicked();
            }
            else
            {
                System.out.println("Save or update classDynModelDatas");
            }
        }
    }

    private void updateDynTypes()
    {
        if (!isSaving)
        {
            startSaving();
        }
        if (DEBUG)
        {
            System.out.println("DynType to update : ");
        }
        for (D modelData : model.getUpdateDynTypeMap().values())
        {
            if (modelData instanceof DynTypeModelData)
            {
                DynTypeModelData dynTypeModelData = (DynTypeModelData) modelData;
                if (Const.JAVA_UTIL_LIST.equals(dynTypeModelData.getNature()))
                {
                    dynTypeModelData.setStructName(DynTypeModelData.LIST_VALUES_STRUCT_NAME);
                }
                if (!DEBUG)
                {
                    dynTypeModelData.save(ExtendedAttributeEditorEvents.DYN_TYPE_SAVED);
                }
                else
                {
                    System.out.println(dynTypeModelData);
                    System.out.println(dynTypeModelData.toString());
                    AppEvent appEvent = new AppEvent(ExtendedAttributeEditorEvents.DYN_TYPE_SAVED);
                    appEvent.setData(CommonConfig.MODEL_EVENT_VAR_NAME, dynTypeModelData);
                    EventDispatcher.forwardEvent(appEvent);
                }
            }
        }
        if (DEBUG)
        {
            System.out.println("End of DynType to update");
        }
    }

    private void saveDynTypes()
    {
        if (!isSaving)
        {
            startSaving();
        }
        int index = 0;
        if (DEBUG)
        {
            System.out.println("DynType to add : ");
        }
        for (D modelData : model.getAddDynTypeMap().values())
        {
            if (modelData instanceof DynTypeModelData)
            {
                DynTypeModelData dynTypeModelData = (DynTypeModelData) modelData;
                if (!ExtendedAttributeEditor.DEFAULT_DYNTYPE_NAME.equals(dynTypeModelData.getName()))
                {
                    dynTypeModelData.set(BEFORE_SAVED_ID, dynTypeModelData.getId());
                    if (Const.JAVA_UTIL_LIST.equals(dynTypeModelData.getNature()))
                    {
                        dynTypeModelData.setStructName(DynTypeModelData.LIST_VALUES_STRUCT_NAME);
                        dynTypeModelData.setType(Const.JAVA_LANG_STRING);
                        if (dynTypeModelData.getStructId().intValue() <= 0)
                        {
                            ListValuesModelData listValuesModelData = model.getAddListValuesMap().get(
                                    dynTypeModelData.getStructId().toString());
                            if (listValuesModelData != null)
                            {
                                dynTypeModelData.setStructId(listValuesModelData.getId());
                            }
                        }
                    }
                    if (!DEBUG)
                    {
                        dynTypeModelData.setId(null);
                        dynTypeModelData.save(ExtendedAttributeEditorEvents.DYN_TYPE_SAVED);
                    }
                    else
                    {
                        dynTypeModelData.setId(Long.valueOf(index++));
                        System.out.println(dynTypeModelData.toString());
                        AppEvent appEvent = new AppEvent(ExtendedAttributeEditorEvents.DYN_TYPE_SAVED);
                        appEvent.setData(CommonConfig.MODEL_EVENT_VAR_NAME, dynTypeModelData);
                        EventDispatcher.forwardEvent(appEvent);
                    }
                }
                else
                {
                    nbRequest--;
                }
            }
        }
        if (DEBUG)
        {
            System.out.println("End of DynType to save");
        }
    }

    private void updateAddListValues()
    {
        if (!isSaving)
        {
            startSaving();
        }
        if (DEBUG)
        {
            System.out.println("List Values to update : ");
        }
        for (ListValuesModelData listValuesModelData : model.getUpdateListValuesMap().values())
        {
            if (!DEBUG)
            {
                listValuesModelData.save(ExtendedAttributeEditorEvents.LIST_VALUES_SAVED);
            }
            else
            {
                System.out.println(listValuesModelData.toString());
                AppEvent appEvent = new AppEvent(ExtendedAttributeEditorEvents.LIST_VALUES_SAVED);
                appEvent.setData(CommonConfig.MODEL_EVENT_VAR_NAME, listValuesModelData);
                EventDispatcher.forwardEvent(appEvent);
            }
        }
        if (DEBUG)
        {
            System.out.println("End of list Values to update");
        }
    }

    private void saveAddListValues()
    {
        if (!isSaving)
        {
            startSaving();
        }
        int index = 1;
        if (DEBUG)
        {
            System.out.println("List Values to add : ");
        }
        for (ListValuesModelData listValuesModelData : model.getAddListValuesMap().values())
        {
            listValuesModelData.set(BEFORE_SAVED_ID, listValuesModelData.getId());
            if (!DEBUG)
            {
                listValuesModelData.setId(null);
                listValuesModelData.save(ExtendedAttributeEditorEvents.LIST_VALUES_SAVED);
            }
            else
            {
                listValuesModelData.setId(Long.valueOf(index++));
                System.out.println(listValuesModelData.toString());
                AppEvent appEvent = new AppEvent(ExtendedAttributeEditorEvents.LIST_VALUES_SAVED);
                appEvent.setData(CommonConfig.MODEL_EVENT_VAR_NAME, listValuesModelData);
                EventDispatcher.forwardEvent(appEvent);
            }
        }
        if (DEBUG)
        {
            System.out.println("End of list Values to save");
        }
    }
}
