package fr.hd3d.common.ui.client.widget.attributeeditor;

import com.extjs.gxt.ui.client.widget.grid.CellEditor;

import fr.hd3d.common.ui.client.modeldata.reader.DynTypeReader;
import fr.hd3d.common.ui.client.modeldata.technical.ClassDynModelData;
import fr.hd3d.common.ui.client.modeldata.technical.DynTypeModelData;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.widget.ModelDataComboBox;


public class DynTypeCellEditor extends CellEditor
{
    private final ServiceStore<ClassDynModelData> store;
    private final static ModelDataComboBox<DynTypeModelData> typeComboBox = new ModelDataComboBox<DynTypeModelData>(
            new DynTypeReader());

    public DynTypeCellEditor(ServiceStore<ClassDynModelData> store)
    {
        super(typeComboBox);
        this.store = store;
    }

    @Override
    public Object preProcessValue(Object value)
    {
        if (value == null)
        {
            return value;
        }
        ClassDynModelData attribute = store.getAt(row);
        if (attribute.getDynMetaDataType() != null)
        {
            return typeComboBox.getStore().findModel(DynTypeModelData.ID_FIELD, attribute.getDynMetaDataType());
        }
        return typeComboBox.getStore().findModel((DynTypeModelData) value);
    }

    @Override
    public Object postProcessValue(Object value)
    {
        if (value == null)
        {
            return value;
        }
        ClassDynModelData attribute = store.getAt(row);
        attribute.setDynMetaDataType(((DynTypeModelData) value).getId());
        return ((DynTypeModelData) value).get(ClassDynModelData.NAME_FIELD);
    }
}
