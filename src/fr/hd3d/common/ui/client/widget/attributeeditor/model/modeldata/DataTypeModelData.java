package fr.hd3d.common.ui.client.widget.attributeeditor.model.modeldata;

import com.extjs.gxt.ui.client.data.BaseModelData;


public class DataTypeModelData extends BaseModelData
{

    private static final long serialVersionUID = -8673840969526160759L;

    public static final String NAME = "name";
    public static final String VALUE = "value";

    public void setName(String name)
    {
        this.set(NAME, name);
    }

    public void setValue(String value)
    {
        this.set(VALUE, value);
    }

    public String getValue()
    {
        return this.get(VALUE);
    }

    public String getName()
    {
        return this.get(NAME);
    }
}
