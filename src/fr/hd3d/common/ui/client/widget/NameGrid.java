package fr.hd3d.common.ui.client.widget;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.util.GridUtils;


/**
 * This grid will be used to display array fields
 * 
 * @author HD3D
 */
public class NameGrid extends Grid<RecordModelData>
{
    /** Constant strings to display : dialog messages, button label... */
    public static final CommonConstants CONSTANTS = GWT.create(CommonConstants.class);
    /** Parameterizable messages to display (like the counter label) */
    public static final CommonConstants MESSAGES = GWT.create(CommonConstants.class);

    /**
     * Default constructor
     * 
     * @param store
     *            Store that contains data to display into the grid.
     */
    public NameGrid(ListStore<RecordModelData> store)
    {
        super(store, getColumns());

        this.setStyle();
    }

    /**
     * @return grid columns : one for field names.
     */
    private static ColumnModel getColumns()
    {
        List<ColumnConfig> columns = new ArrayList<ColumnConfig>();

        GridUtils.addColumnConfig(columns, RecordModelData.NAME_FIELD, CONSTANTS.Name(), 150, true);

        return new ColumnModel(columns);
    }

    /**
     * Set grid style : no headers, and size is force to fit the parent panel.
     */
    private void setStyle()
    {
        this.setAutoExpandColumn(RecordModelData.NAME_FIELD);
        this.setHideHeaders(true);
        this.setLoadMask(true);
    }
}
