package fr.hd3d.common.ui.client.widget.workobject.browser.controller;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.store.Record;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;

import fr.hd3d.common.ui.client.cookie.FavoriteCookie;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.Hd3dCheckColumnConfig;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.common.ui.client.widget.sheeteditor.SheetEditorDisplayer;
import fr.hd3d.common.ui.client.widget.sheeteditor.event.SheetEditorEvents;
import fr.hd3d.common.ui.client.widget.workobject.browser.config.WorkObjectConfig;
import fr.hd3d.common.ui.client.widget.workobject.browser.events.WorkObjectEvents;
import fr.hd3d.common.ui.client.widget.workobject.browser.model.WorkObjectModel;
import fr.hd3d.common.ui.client.widget.workobject.browser.view.WorkObjectView;


/**
 * WorkObjectController handles work object view events.
 * 
 * @author HD3D
 */
public abstract class WorkObjectController extends MaskableController
{
    /** Model that handles work object view data. */
    protected final WorkObjectModel model;
    /** View that handles work object widgets. */
    protected final WorkObjectView view;
    
    protected boolean isFirstSheetLoading = false;

    public WorkObjectController(WorkObjectView view, WorkObjectModel model)
    {
        this.model = model;
        this.view = view;
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();
        if (type == WorkObjectEvents.SHEET_CHANGED)
        {
            this.onSheetChanged(event);
        }
        else if (type == SheetEditorEvents.NEW_SHEET_CLICKED)
        {
            this.onNewSheet(event);
        }
        else if (type == SheetEditorEvents.EDIT_SHEET_CLICKED)
        {
            this.onEditSheet(event);
        }
        else if (type == SheetEditorEvents.END_SAVE)
        {
            this.onSheetSaved(event);
        }
        else if (type == SheetEditorEvents.DELETE_SHEET_CLICKED)
        {
            this.onDeleteView();
        }
        else if (type == WorkObjectEvents.SHEET_DELETED)
        {
            this.onSheetDeleted();
        }
        else if (type == ExplorerEvents.BEFORE_SET_COLUMN_MODEL)
        {
            this.forwardToChild(event);
            this.onBeforeSetColumnModel(event);
        }
        else if (type == ExplorerEvents.SHEET_INFORMATIONS_LOADED)
        {
            this.forwardToChild(event);
            this.onSheetInformationsLoaded(event);
        }
        else if (type == ExplorerEvents.AUTO_SAVE_TOGGLE)
        {
            this.forwardToChildForced(event);
        }
        else if (type == ExplorerEvents.BEFORE_SAVE)
        {
            this.onBeforeSave();
        }
        else if (type == ExplorerEvents.AFTER_SAVE)
        {
            this.onExplorerEndSave(event);
        }
        else if (type == ExplorerEvents.ENTER_KEY_PRESSED)
        {
            this.onEnterKeyPressed(event);
        }
        else if (type == CommonEvents.NAME_CHANGED)
        {
            this.onWorkObjectNameChanged(event);
        }
        else if (type == ExplorerEvents.DATA_LOADED)
        {
            this.onExplorerDataLoaded(event);
        }
        else if (type == CommonEvents.ERROR)
        {
            this.onError(event);
        }
        else
        {
            this.forwardToChild(event);
        }
    }

    protected void onExplorerDataLoaded(AppEvent event) {
        this.view.getExplorer().hideLoading();
        this.forwardToChild(event);
	}

	protected void onWorkObjectNameChanged(AppEvent event)
    {}

    /**
     * When enter key is pressed upon explorer, if selected cell is boolean its value is inversed.
     * 
     * @param event
     *            Enter key pressed event.
     */
    protected void onEnterKeyPressed(AppEvent event)
    {
        Integer colIndex = event.getData();

        ColumnConfig column = this.view.getColumn(colIndex);

        if (column instanceof Hd3dCheckColumnConfig)
        {
            for (Hd3dModelData modelSelected : this.view.getSelection())
            {
                Boolean bool = modelSelected.get(column.getDataIndex());
                if (bool == null)
                    bool = Boolean.FALSE;
                Record record = model.getExplorerStore().getRecord(modelSelected);

                record.set(column.getDataIndex(), !bool);
            }
        }
    }

    /**
     * When sheet changed, data are reloaded for this sheet and its id is saved to settings. This is needed to reselect
     * it when project will be reselected.
     * 
     * @param event
     *            Sheet changed event.
     */
    private void onSheetChanged(AppEvent event)
    {
        SheetModelData sheet = event.getData();
        String cookieKey = WorkObjectConfig.SHEET_COOKIE_VAR_PREFIX + this.model.getEntityName();
        if (MainModel.currentProject != null)
        {
            cookieKey = WorkObjectConfig.SHEET_COOKIE_VAR_PREFIX + this.model.getEntityName() + "-"
                    + MainModel.currentProject.getId();
        }

        if (sheet != null && sheet.getId() != null)
            FavoriteCookie.putFavValue(cookieKey, sheet.getId().toString());

        this.view.displayWorkObjectList(sheet, this.isFirstSheetLoading);
        if (this.isFirstSheetLoading)
            this.isFirstSheetLoading = false;
    }

    /**
     * When sheet is saved, identity sheet is rebuilt and explorer grid is reconfigured.
     * 
     * @param event
     *            Sheet saved event.
     */
    private void onSheetSaved(AppEvent event)
    {
        SheetModelData sheet = event.getData();
        ListStore<SheetModelData> store = this.model.getSheetStore();
        SheetModelData storeSheet = store.findModel(SheetModelData.ID_FIELD, sheet.getId());

        if (storeSheet != null)
        {
            store.remove(storeSheet);
        }

        this.view.selectSheet(null);
        store.add(sheet);
        store.commitChanges();

        this.view.displayWorkObjectList(sheet, false);
    }

    /**
     * Display sheet editor window.
     * 
     * @param event
     *            New sheet clicked event.
     */
    public void onNewSheet(AppEvent event)
    {
        if (SheetEditorDisplayer.isNull())
        {
            SheetEditorDisplayer.createSheetEditorWindow(this.model.getEntities());
        }
        SheetEditorDisplayer.setEntities(this.model.getEntities());

        if (MainModel.currentProject != null)
        {
            SheetEditorDisplayer.showEditor(MainModel.currentProject.getId());
        }
        else
        {
            SheetEditorDisplayer.showEditor();
        }
    }

    /**
     * When sheet is asked for edition, sheet displayer is invoked with right configuration.
     * 
     * @param event
     *            Edit sheet event.
     */
    public void onEditSheet(AppEvent event)
    {
        if (SheetEditorDisplayer.isNull())
        {
            SheetEditorDisplayer.createSheetEditorWindow(this.model.getEntities());
        }
        SheetEditorDisplayer.setEntities(this.model.getEntities());
        SheetEditorDisplayer.showEditor(this.view.getSelectedSheet());
    }

    /**
     * When delete view button is clicked, it deletes from server the current view. Once the view is deleted it forwards
     * the event sheet deleted. Which has for effect to remove the view from combo store.
     */
    private void onDeleteView()
    {
        SheetModelData sheet = this.view.getSelectedSheet();
        if (sheet != null)
        {
            this.view.enableViewComboBox(false);
            sheet.delete(WorkObjectEvents.SHEET_DELETED);
        }
    }

    /**
     * After a sheet has been deleted from server, it removes it from the view combo store then it selects the first
     * sheet in the store.
     */
    private void onSheetDeleted()
    {
        this.model.getSheetStore().remove(this.view.getSelectedSheet());
        this.view.enableViewComboBox(true);
        this.view.selectFirstSheet();
    }

    /**
     * When data are loaded, view listeners are reset.
     */
    private void onSheetInformationsLoaded(AppEvent event)
    {
        this.view.setListeners();
    }

    /**
     * Before column configuration is set, if an aggregation row configuration is needed, it is added to the model
     * configuration.
     * 
     * @param event
     *            Before set column model event.
     */
    protected void onBeforeSetColumnModel(AppEvent event)
    {

    }

    /**
     * When sheet saving begins, the saving indicator is displayed.
     * 
     * @param event
     *            The before save event.
     */
    private void onBeforeSave()
    {
        this.view.showSaving();
    }

    /**
     * When explorer saving ends, save indicator is hidden.
     * 
     * @param event
     *            The end save event.
     */
    private void onExplorerEndSave(AppEvent event)
    {
        this.view.hideSaving();
    }

    /**
     * If error occurs, saving indicator is hidden.
     */
    protected void onError(AppEvent event)
    {
        this.view.hideSaving();
        this.forwardToChild(event);
    }

    /**
     * Register supported events.
     */
    protected void registerEvents()
    {
        this.registerEventTypes(ExplorerEvents.ENTER_KEY_PRESSED);
        this.registerEventTypes(ExplorerEvents.BEFORE_SET_COLUMN_MODEL);
        this.registerEventTypes(ExplorerEvents.SHEET_INFORMATIONS_LOADED);
        this.registerEventTypes(ExplorerEvents.AUTO_SAVE_TOGGLE);
        this.registerEventTypes(ExplorerEvents.DATA_LOADED);
        this.registerEventTypes(CommonEvents.NAME_CHANGED);

        this.registerEventTypes(WorkObjectEvents.SHEET_CHANGED);
        this.registerEventTypes(SheetEditorEvents.DELETE_SHEET_CLICKED);
        this.registerEventTypes(WorkObjectEvents.SHEET_DELETED);
        this.registerEventTypes(SheetEditorEvents.NEW_SHEET_CLICKED);
        this.registerEventTypes(SheetEditorEvents.EDIT_SHEET_CLICKED);
        this.registerEventTypes(SheetEditorEvents.END_SAVE);
    }
}
