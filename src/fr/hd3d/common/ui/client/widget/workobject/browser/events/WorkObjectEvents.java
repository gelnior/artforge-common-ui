package fr.hd3d.common.ui.client.widget.workobject.browser.events;

import com.extjs.gxt.ui.client.event.EventType;


public class WorkObjectEvents
{

    public static final EventType SHEET_CHANGED = new EventType();
    public static final EventType SHEET_DELETED = new EventType();

}
