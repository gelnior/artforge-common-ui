package fr.hd3d.common.ui.client.widget.workobject.browser.model;

import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.common.client.enums.EConstraintLogicalOperator;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.callback.ServerTimeCallback;
import fr.hd3d.common.ui.client.service.parameter.LogicConstraint;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.service.store.ServicesPagingStore;
import fr.hd3d.common.ui.client.widget.explorer.model.ExplorerStore;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata.EntityModelData;


/**
 * WorkObjectModel handles data for work object view (explorer store, sheet, store, filters...).
 * 
 * @author HD3D
 */
public class WorkObjectModel
{
    /** Store containing explorer data. */
    protected ExplorerStore store;
    /** Filter used on explorer store. */
    private final LogicConstraint filter = new LogicConstraint(EConstraintLogicalOperator.AND);

    /** Entities handled by explorer, sheet combo box and sheet editor. */
    private List<EntityModelData> entities;
    /** Store containing sheet list for selection. */
    private ServiceStore<SheetModelData> sheetStore;
    /** Type of data currently used by explorer. */
    private String entityName;

    /** true if serverTime is up to date. */
    private Boolean isServerTimeUpdate = false;

    /** Actual server time. */
    private final Date serverTime = new Date();

    public void setExplorerStore(ExplorerStore store, String entityName)
    {
        this.store = store;
        this.entityName = entityName;
    }

    public String getEntityName()
    {
        return this.entityName;
    }

    public LogicConstraint getFilter()
    {
        return filter;
    }

    public ServicesPagingStore<Hd3dModelData> getExplorerStore()
    {
        return store;
    }

    public List<EntityModelData> getEntities()
    {
        return entities;
    }

    public void setEntities(List<EntityModelData> entities)
    {
        this.entities = entities;
    }

    public ServiceStore<SheetModelData> getSheetStore()
    {
        return sheetStore;
    }

    public void setSheetStore(ServiceStore<SheetModelData> sheetStore)
    {
        this.sheetStore = sheetStore;
    }

    public boolean isServerTimeUpdate()
    {
        return isServerTimeUpdate;
    }

    public void setServerTimeUpdate(boolean serverTimeUpdate)
    {
        isServerTimeUpdate = serverTimeUpdate;
    }

    public Date getServerTime()
    {
        return serverTime;
    }

    public void refreshServerTime(AppEvent event)
    {
        String path = ServicesURI.SERVERTIME;
        this.isServerTimeUpdate = true;
        RestRequestHandlerSingleton.getInstance().getRequest(path, new ServerTimeCallback(serverTime, event));
    }
}
