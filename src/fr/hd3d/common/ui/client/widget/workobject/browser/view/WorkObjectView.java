package fr.hd3d.common.ui.client.widget.workobject.browser.view;

import java.util.List;

import com.extjs.gxt.ui.client.widget.Component;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.layout.BorderLayoutData;
import com.extjs.gxt.ui.client.widget.layout.CardLayout;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.toolbar.FillToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.SeparatorToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.ToolBar;

import fr.hd3d.common.ui.client.config.CommonConfig;
import fr.hd3d.common.ui.client.cookie.FavoriteCookie;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.util.CSSUtils;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.widget.BorderedPanel;
import fr.hd3d.common.ui.client.widget.SavingStatus;
import fr.hd3d.common.ui.client.widget.SheetCombobox;
import fr.hd3d.common.ui.client.widget.constraint.widget.SheetFilterComboBox;
import fr.hd3d.common.ui.client.widget.explorer.ExplorerPanel;
import fr.hd3d.common.ui.client.widget.explorer.ExplorerRefreshButton;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.ConstraintPanelToggle;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.CsvSheetExportToolItem;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.DeleteSheetToolItem;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.EditSheetToolItem;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.NewSheetToolItem;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.PrintButton;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.SaveSheetDataToolItem;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.ShowMosaicToolItem;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.common.ui.client.widget.workobject.browser.config.WorkObjectConfig;


/**
 * Base class to allow work object exploring. Work object could be shot or constituent.
 * 
 * @author HD3D
 */
public class WorkObjectView extends BorderedPanel
{
    /** Sheet combo box. */
    protected SheetCombobox sheetCombo = new SheetCombobox();

    /** The explorer panel */
    protected ExplorerPanel explorer;

    /** West panel contains navigation tree. */
    protected ContentPanel westPanel = new ContentPanel();
    /** Center layout display */
    protected CardLayout centerLayout = new CardLayout();

    protected ToolBar explorerBar = new ToolBar();

    /** Explorer constraint panel button. */
    public final ConstraintPanelToggle filterPanelButton = new ConstraintPanelToggle();

    /** Explorer save button. */
    public final SaveSheetDataToolItem saveButton = new SaveSheetDataToolItem();
    /** Explorer CSV export button */
    public final CsvSheetExportToolItem csvExportButton = new CsvSheetExportToolItem();

    /** Button that opens new sheet dialog box. */
    public final NewSheetToolItem newSheetToolItem = new NewSheetToolItem();
    /** Button that opens edit sheet dialog box. */
    public final EditSheetToolItem editSheetToolItem = new EditSheetToolItem();
    /** Button that deletes current sheet when clicked. */
    public final DeleteSheetToolItem deleteSheetToolItem = new DeleteSheetToolItem();
    /** Sheet filter combo box contains preset filtered for current sheet. */
    protected SheetFilterComboBox sheetFilterComboBox = new SheetFilterComboBox();

    /** Saving indicator. */
    public final SavingStatus status = new SavingStatus();

    /**
     * Default constructor.
     */
    public WorkObjectView()
    {
        this.setWestPanel();
        this.setCenterPanel();
        this.setExplorerToolBar();
    }

    /**
     * Block all events sent to the work object controller of this work object view.
     */
    public void idle()
    {
        this.explorer.idle();
    }

    /**
     * Allow all events sent to the work object controller of this work object view.
     */
    public void unidle()
    {
        this.explorer.unidle();
    }

    /**
     * Display explorer panel for data browsing. Displayed fields are based on the sheet given in parameter. Data are
     * reloaded
     * 
     * @param sheet
     *            The sheet used to display data.
     * @param isFirstSheetLoading 
     */
    public void displayWorkObjectList(SheetModelData sheet, boolean isFirstSheetLoading)
    {
        if (sheet != null && !sheet.equals(this.getSelectedSheet()))
        {
            this.sheetCombo.setValue(sheet);
            this.sheetFilterComboBox.refreshFilterList(sheet);
        }
        else
        {
            this.explorer.setCurrentSheet(sheet, !isFirstSheetLoading);
            this.sheetFilterComboBox.refreshFilterList(sheet);
        }
        this.centerLayout.setActiveItem(explorer);
    }

    /**
     * Refresh explorer data.
     */
    public void refreshWorkObjectList()
    {
        if (this.explorer.getCurrentSheet() != null)
            this.explorer.refreshData();
    }

    public void setExplorerView(SheetModelData sheet)
    {
        this.explorer.setCurrentSheet(sheet);
        this.sheetFilterComboBox.refreshFilterList(sheet);
    }

    /** No listeners to set but the method is here for easy overriding. */
    public void setListeners()
    {}

    /**
     * @return list of selected instances.
     */
    public List<Hd3dModelData> getSelection()
    {
        return this.explorer.getSelectedModels();
    }

    /**
     * Return the first selected model inside explorer selection. If no model is selected, <i>null</i> is returned.
     */
    public Hd3dModelData getFirstSelected()
    {
        return (Hd3dModelData) CollectionUtils.getFirst(this.getSelection());
    }

    /**
     * Return explorer column configuration corresponding to column which index is equal to <i>colIndex</i>.
     * 
     * @param colIndex
     *            The index of the desired column.
     */
    public ColumnConfig getColumn(int colIndex)
    {
        return this.explorer.getGrid().getColumnModel().getColumn(colIndex);
    }

    /**
     * Save to cookie for a given project and a given simple class name, the id of the last selected sheet.
     * 
     * @param projectId
     *            The project associated to the sheet.
     * @param viewId
     *            The view id.
     * @param simpleClassName
     *            "library" or "temporal"
     */
    public void saveViewIdToCookie(Long projectId, Long viewId, String simpleClassName)
    {
        String key = CommonConfig.COOKIE_VAR_PROJECT + "-" + projectId + "-";
        key += simpleClassName + CommonConfig.COOKIE_VAR_SHEET;
        FavoriteCookie.putFavValue(key.toLowerCase(), viewId.toString());
    }

    /**
     * Enable or disable the view combo box.
     * 
     * @param enabled
     *            True to enable the view combo box, false to disable it.
     */
    public void enableSheetComboBox(boolean enabled)
    {
        this.sheetCombo.setEnabled(enabled);
    }

    /** @return Currently selected sheet. */
    public SheetModelData getSelectedSheet()
    {
        return this.sheetCombo.getValue();
    }

    public void selectFirstSheet()
    {
        this.sheetCombo.selectFirst();
    }

    public void enableViewComboBox(boolean enabled)
    {
        this.sheetCombo.setEnabled(enabled);
    }

    public void reloadSheets(ProjectModelData project, String simpleClassName)
    {
        if (project != null)
        {
            this.sheetCombo.setCookieKey(WorkObjectConfig.SHEET_COOKIE_VAR_PREFIX + simpleClassName + "-"
                    + MainModel.currentProject.getId());
            this.sheetCombo.setProject(project);
        }
        else
        {
            this.sheetCombo.setCookieKey(WorkObjectConfig.SHEET_COOKIE_VAR_PREFIX + simpleClassName);
        }
        this.sheetCombo.reload();
    }

    /**
     * Remove <i>sheet</i> from the combo box store.
     * 
     * @param sheet
     *            The sheet to remove from the combo box store.
     */
    public void removeSheetFromCombo(SheetModelData sheet)
    {
        this.sheetCombo.remove(sheet);
    }

    public void setSheetCookie(String cookieKey)
    {
        this.sheetCombo.setCookieKey(cookieKey);
    }

    /** Show saving indicator. */
    public void showSaving()
    {
        this.status.show();
    }

    /** Hide saving indicator. */
    public void hideSaving()
    {
        this.status.hide();
    }

    protected void addToWestPanel(Component component)
    {
        this.westPanel.add(component);
    }

    /**
     * Build explorer toolbar and set all its button.
     */
    public void setExplorerToolBar()
    {
        explorerBar.setStyleAttribute("padding", "5px");

        explorerBar.add(sheetCombo);
        sheetCombo.setWidth(300);
        explorerBar.add(filterPanelButton);
        explorerBar.add(sheetFilterComboBox);
        sheetFilterComboBox.registerToggleButton(filterPanelButton);

        CSSUtils.set1pxBorder(filterPanelButton, "transparent");
        explorerBar.add(new SeparatorToolItem());
        explorerBar.add(new ShowMosaicToolItem());
        explorerBar.add(new SeparatorToolItem());
        explorerBar.add(new PrintButton());
        explorerBar.add(csvExportButton);
        explorerBar.add(new SeparatorToolItem());
        explorerBar.add(new ExplorerRefreshButton(this.explorer));
        explorerBar.add(saveButton);
        explorerBar.add(status);

        explorerBar.add(new FillToolItem());
        explorerBar.add(newSheetToolItem);
        explorerBar.add(editSheetToolItem);
        explorerBar.add(deleteSheetToolItem);

        explorer.setTopComponent(explorerBar);
    }

    private void setCenterPanel()
    {
        LayoutContainer container = new LayoutContainer();
        container.setLayout(centerLayout);

        this.setExplorer();

        container.add(explorer);
        centerLayout.setActiveItem(explorer);

        this.addCenter(container);
    }

    private void setExplorer()
    {
        this.explorer = new ExplorerPanel();
        this.setFieldProviders();

        this.explorer.setHeaderVisible(false);
        this.explorer.setBodyBorder(false);
        this.explorer.setBorders(true);
        this.explorer.registerFilterComboBox(sheetFilterComboBox);
        this.explorer.setDefaultOrderColumn("label");

        MaskableController explorerController = this.explorer.getController();
        EventDispatcher.get().removeController(explorerController);
    }

    /** Set no providers but it is here for easy overriding. */
    protected void setFieldProviders()
    {}

    public void setWestPanel()
    {
        this.westPanel.setHeaderVisible(true);
        this.westPanel.setLayout(new FitLayout());
        this.westPanel.setBorders(false);

        BorderLayoutData data = this.addWest(westPanel, 250);
        data.setMinSize(250);
        data.setCollapsible(true);

        this.westPanel.setHeading("Navigation");
    }

    public ToolBar getExplorerBar()
    {
        return explorerBar;
    }

    public void selectSheet(SheetModelData object)
    {
        this.sheetCombo.setValue(object);
    }

    public ExplorerPanel getExplorer()
    {
        return this.explorer;
    }

}
