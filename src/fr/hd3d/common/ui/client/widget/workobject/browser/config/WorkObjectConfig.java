package fr.hd3d.common.ui.client.widget.workobject.browser.config;

/**
 * Work object browser constants
 * 
 * @author HD3D
 */
public class WorkObjectConfig
{
    public static final String SHEET_COOKIE_VAR_PREFIX = "production-sheet-";
}
