package fr.hd3d.common.ui.client.widget;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.google.gwt.user.client.ui.AbstractImagePrototype;

import fr.hd3d.common.ui.client.listener.LocalButtonClickListener;
import fr.hd3d.common.ui.client.mvc.controller.CommandController;


/**
 * Tool bar local button is a button that sends this on click event to a given controller only. The event is not
 * dispatched to all controllers.
 * 
 * @author HD3D
 */
public class ToolBarLocalButton extends Button
{
    /**
     * Default Constructor.
     * 
     * @param controller
     *            The controller to which event will be forwarded.
     * @param icon
     *            Icon at CCS format.
     * @param toolTip
     *            Tool tip to display.
     * @param eventType
     *            Event to forward on click.
     */
    public ToolBarLocalButton(CommandController controller, AbstractImagePrototype icon, String toolTip,
            EventType eventType)
    {
        super();

        this.setStyle(icon, toolTip);
        this.registerListener(controller, eventType);
    }

    /**
     * Set buttons styles.
     * 
     * @param iconStyle
     *            Icon at GXT format.
     * @param toolTip
     *            Tool tip to display.
     */
    private void setStyle(AbstractImagePrototype icon, String toolTip)
    {
        this.setIcon(icon);
        this.setToolTip(toolTip);
    }

    /**
     * Register on tool item click event. When the button is clicked, it sends the <i>evenType</i> event to given
     * controller.
     * 
     * @param controller
     *            The controller that will be notified when button is clicked.
     * @param eventType
     *            The type of event to send.
     */
    protected void registerListener(CommandController controller, EventType eventType)
    {
        if (eventType != null)
        {
            this.addSelectionListener(new LocalButtonClickListener(eventType, controller));
        }
    }

    /**
     * Sets icon and tool tip.
     */
    protected void setStyle(String iconStyle, String toolTip)
    {
        this.setIconStyle(iconStyle);
        this.setToolTip(toolTip);
    }
}
