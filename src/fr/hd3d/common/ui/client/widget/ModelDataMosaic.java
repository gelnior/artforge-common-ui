package fr.hd3d.common.ui.client.widget;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.widget.ListView;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.service.store.BaseStore;
import fr.hd3d.common.ui.client.widget.thumbnaileditor.ThumbnailPath;


/**
 * Mosaic List displays HD3D instance as a list of thumbnails. The instance name is written above each thumbnail. A
 * double click on a list element opens the instance identity sheet.
 * 
 * @author HD3D
 */
public class ModelDataMosaic<C extends Hd3dModelData> extends ListView<C>
{

    public ModelDataMosaic(BaseStore<C> store)
    {
        this.setStore(store);

        this.setStyles();
        this.setListeners();
    }

    /**
     * Before displaying the model the thumbnail path is set. Thumbnail path is built from simple class name. Then the
     * name is set. It is built from name field if exists, label field or key field either. If instances are persons the
     * nam is built from first name and last name.
     * 
     * @param model
     *            The model to prepare.
     */
    @Override
    protected C prepareData(C model)
    {
        String path = ThumbnailPath.getPath(model, true);
        model.set("path", path);

        String name = null;

        if (PersonModelData.SIMPLE_CLASS_NAME.equals(model.getSimpleClassName()))
        {
            name = (String) model.get(PersonModelData.PERSON_FIRST_NAME_FIELD) + " "
                    + (String) model.get(PersonModelData.PERSON_LAST_NAME_FIELD);
        }
        if (name == null)
            name = model.get(RecordModelData.NAME_FIELD);
        if (name == null)
            name = model.get(ConstituentModelData.CONSTITUENT_LABEL);
        if (name == null)
            name = "";

        model.set(RecordModelData.NAME_FIELD, name);

        return model;
    }

    /**
     * @return javascript template to display a list element.
     */
    private native String getXTemplate() /*-{
                                                  return ['<tpl for=".">', 
                                                  '<div class="thumb-wrap" id="{name}" style="width: 200px; float : left; border : 1px solid white; padding : 5px; text-align: center;">', 
                                                  '<div class="thumb"><img src="{path}" style="height: 100px; margin-bottom : 8px;" title="{name}"></div>', 
                                                  '<span class="x-editable">{name}</span></div>', 
                                                  '</tpl>', 
                                                  '<div class="x-clear"></div>'].join("");
                                                  }-*/;

    /**
     * Set the double click listener.
     */
    private void setListeners()
    {
        this.addListener(Events.DoubleClick, new Listener<BaseEvent>() {

            public void handleEvent(BaseEvent be)
            {
                onDoubleClick(be);
            }
        });
    }

    /**
     * When there is a double click on a list element, the element identity sheet is displayed.
     * 
     * @param be
     */
    private void onDoubleClick(BaseEvent be)
    {}

    /**
     * Set list styles : borders, layout...
     */
    private void setStyles()
    {
        this.setItemSelector("div.thumb-wrap");
        this.setBorders(false);
        this.setTemplate(getXTemplate());
    }
}
