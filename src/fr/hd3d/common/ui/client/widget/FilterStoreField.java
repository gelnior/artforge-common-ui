package fr.hd3d.common.ui.client.widget;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.KeyListener;
import com.extjs.gxt.ui.client.store.Store;
import com.extjs.gxt.ui.client.widget.form.TextField;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;


/**
 * Text field that filters data contains in store by name. Filter applies only on local data. Filter keeps only data of
 * which name begins with value typed inside text field. The filter is updated every time a key is up.
 * 
 * @author HD3D
 * 
 * @param <C>
 *            Type of data contained in store.
 */
public class FilterStoreField<C extends ModelData> extends TextField<String>
{
    /** Default width of this text field. */
    public static final Integer DEFAULT_WIDTH = 100;

    /** Field used by filter (because sometimes name is called label). */
    protected String nameField = RecordModelData.NAME_FIELD;
    /** Store containing data to filter. */
    protected Store<C> store;

    /**
     * Default constructor.
     * 
     * @param store
     *            The store that will be filtered.
     */
    public FilterStoreField(Store<C> store)
    {
        this.store = store;

        this.setWidth(DEFAULT_WIDTH);
        this.setListeners();
        this.setToolTip("Type text here to filter data.");
    }

    private void setListeners()
    {
        this.addKeyListener(new KeyListener() {
            @Override
            public void componentKeyUp(ComponentEvent event)
            {
                onKeyUp(event);
            }
        });
    }

    /**
     * @return Field used by filter.
     */
    public String getNameField()
    {
        return nameField;
    }

    /**
     * Set field used by filter
     * 
     * @param nameField
     *            The field to set.
     */
    public void setNameField(String nameField)
    {
        this.nameField = nameField;
    }

    /**
     * When key is up, the store is filtered.
     */
    protected void onKeyUp(ComponentEvent event)
    {
        this.store.filter(nameField, getValue());
    }
}
