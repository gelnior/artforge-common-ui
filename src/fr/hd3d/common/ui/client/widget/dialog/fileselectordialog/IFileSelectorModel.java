package fr.hd3d.common.ui.client.widget.dialog.fileselectordialog;

import fr.hd3d.common.ui.client.modeldata.asset.FileRevisionModelData;


public interface IFileSelectorModel
{
    public void addFileRevision(FileRevisionModelData fileRevision);

    public void removeFileRevision(FileRevisionModelData file);
}
