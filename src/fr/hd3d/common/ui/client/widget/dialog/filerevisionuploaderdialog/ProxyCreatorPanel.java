package fr.hd3d.common.ui.client.widget.dialog.filerevisionuploaderdialog;

import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.Style.VerticalAlignment;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.FormEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.widget.HorizontalPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.form.FileUploadField;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.form.FormPanel.Method;
import com.extjs.gxt.ui.client.widget.layout.FormData;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.common.ui.client.error.Hd3dException;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.widget.proxycreatordialog.AutoProxyTypeComboBox;


public class ProxyCreatorPanel extends LayoutContainer
{
    public static final String FILE_VAR_FORM = "file";
    public static String PROXYTYPE_VAR_FORM = "proxyType";

    /** File field. */
    private final FileUploadField fileProxy = new FileUploadField();
    private final AutoProxyTypeComboBox proxyTypesBox = new AutoProxyTypeComboBox();
    private final FormPanel formProxy = new FormPanel();

    private final EventType submittedEvent;
    private boolean submit = false;
    private final HorizontalPanel panel = new HorizontalPanel();

    public ProxyCreatorPanel(EventType submittedEvent)
    {
        this.submittedEvent = submittedEvent;
        this.panel.setLayoutOnChange(true);
        this.panel.setHorizontalAlign(HorizontalAlignment.CENTER);
        this.panel.setVerticalAlign(VerticalAlignment.MIDDLE);

        createForm();
        setListener();
        this.add(panel);
    }

    private void createForm()
    {
        this.formProxy.setBorders(false);
        this.formProxy.setBodyBorder(false);
        this.formProxy.setHeaderVisible(false);
        this.formProxy.setMethod(Method.POST);
        this.formProxy.setEncoding(FormPanel.Encoding.MULTIPART);

        this.proxyTypesBox.setFieldLabel("Proxy type");
        this.proxyTypesBox.setName(PROXYTYPE_VAR_FORM);
        this.formProxy.add(this.proxyTypesBox, new FormData("100%"));

        this.fileProxy.setFieldLabel("Proxy");
        this.fileProxy.setName(FILE_VAR_FORM);
        this.formProxy.add(this.fileProxy, new FormData("100%"));
        this.panel.add(formProxy);
    }

    protected void setListener()
    {
        formProxy.addListener(Events.Submit, new Listener<FormEvent>() {
            public void handleEvent(FormEvent be)
            {
                onSubmitFinished(be);
            }
        });
    }

    private void onSubmitFinished(FormEvent be)
    {
        this.panel.add(Hd3dImages.getCheckIcon().createImage());
        this.submit = true;
        EventDispatcher.forwardEvent(submittedEvent);
    }

    public boolean isSubmit()
    {
        return submit;
    }

    public void submit(Long fileId) throws Hd3dException
    {
        if (checkField())
        {
            String servicesUrl = RestRequestHandlerSingleton.getInstance().getServicesUrl();
            String path = servicesUrl + ServicesURI.FILEREVISIONS + '/' + fileId + '/' + ServicesURI.PROXIES + '/'
                    + ServicesURI.UPLOAD;
            formProxy.setAction(path);
            formProxy.submit();
        }
        else
        {
            onSubmitFinished(null);
        }

    }

    public boolean checkField() throws Hd3dException
    {
        if (fileProxy.getValue() == null && proxyTypesBox.getValue() == null)
        {
            return false;
        }
        if (fileProxy.getValue() != null && proxyTypesBox.getValue() == null)
        {
            throw new Hd3dException("The preview file " + fileProxy.getValue() + " haven't preview type !");
        }

        return true;
    }

}
