package fr.hd3d.common.ui.client.widget.dialog.fileselectordialog;

import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.Style.VerticalAlignment;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.HorizontalPanel;
import com.extjs.gxt.ui.client.widget.Text;
import com.extjs.gxt.ui.client.widget.form.ComboBox;
import com.extjs.gxt.ui.client.widget.form.ComboBox.TriggerAction;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Image;

import fr.hd3d.common.ui.client.error.Hd3dException;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.modeldata.asset.FileRevisionModelData;


/**
 * Panel to selected a file revision or remove a selected file revision.
 * 
 * @author HD3D
 * 
 */
public class FileSelectorPanel
{
    /** Add a panel to selected file. */
    public static final int ADD_MODE = 1;

    /** Add a panel to remove selected file. */
    public static final int REMOVE_MODE = 2;

    /** The global panel. */
    private final HorizontalPanel filePanel;

    /** List of file revisions. */
    protected final ListStore<FileRevisionModelData> fileRevisions = new ListStore<FileRevisionModelData>();

    /** Combo box with the all revision of the file. */
    private final ComboBox<FileRevisionModelData> revisionComboBox;
    /** Forwarded event when a file is selected. */
    private final EventType selectedEvent;
    /** Forwarded event when a file is removed. */
    private final EventType removedEvent;

    /**
     * Default constructor.
     * 
     * @param fileRevisions
     *            All revision of a file.
     * @param mode
     *            The mode of panel (see static mode).
     * @param selectedEvent
     *            Forwarded event when a file is selected.
     * @param removedEvent
     *            Forwarded event when a file is removed.
     */
    public FileSelectorPanel(ListStore<FileRevisionModelData> fileRevisions, int mode, EventType selectedEvent,
            EventType removedEvent)
    {
        this.selectedEvent = selectedEvent;
        this.removedEvent = removedEvent;
        this.filePanel = new HorizontalPanel();
        this.filePanel.setSpacing(5);
        this.filePanel.setVerticalAlign(VerticalAlignment.MIDDLE);

        this.revisionComboBox = new ComboBox<FileRevisionModelData>();

        if (fileRevisions == null || fileRevisions.getCount() == 0)
        {
            return;
        }

        this.fileRevisions.add(fileRevisions.getModels());
        this.fileRevisions.sort(FileRevisionModelData.REVISION_FIELD, SortDir.DESC);

        switch (mode)
        {
            case ADD_MODE:
                addMode();
                break;
            case REMOVE_MODE:
                removeMode();
                break;
        }

    }

    /**
     * Set a remove panel.
     */
    private void removeMode()
    {

        for (final FileRevisionModelData file : this.fileRevisions.getModels())
        {
            Image removeButton = Hd3dImages.getDeleteIcon().createImage();
            removeButton.setTitle("Deselect this file");
            removeButton.addClickHandler(new ClickHandler() {
                public void onClick(ClickEvent event)
                {
                    EventDispatcher.forwardEvent(removedEvent, file);
                }
            });
            DOM.setStyleAttribute(removeButton.getElement(), "cursor", "pointer");
            filePanel.add(new Text(file.getName() + " version :" + file.getRevision()));
            filePanel.add(removeButton);
        }
    }

    /**
     * Set a add panel.
     */
    private void addMode()
    {
        Image addButton = Hd3dImages.getAddIcon().createImage();
        addButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event)
            {
                try
                {
                    FileRevisionModelData file = revisionComboBox.getValue();
                    if (file == null)
                    {
                        throw new Hd3dException("Select a revision...");
                    }
                    revisionComboBox.setValue(null);
                    EventDispatcher.forwardEvent(selectedEvent, file);
                }
                catch (Hd3dException e)
                {
                    e.print();
                }
            }
        });
        addButton.setTitle("Select this file");
        DOM.setStyleAttribute(addButton.getElement(), "cursor", "pointer");
        filePanel.add(addButton);

        revisionComboBox.setWidth(50);
        revisionComboBox.setStore(this.fileRevisions);
        revisionComboBox.setDisplayField(FileRevisionModelData.REVISION_FIELD);
        revisionComboBox.setTriggerAction(TriggerAction.ALL);
        filePanel.add(revisionComboBox);

        String fileName = fileRevisions.getAt(0).getName();
        Text text = new Text(fileName);
        filePanel.add(text);
    }

    /**
     * Return the panel.
     * 
     * @return the panel.
     */
    public HorizontalPanel getPanel()
    {
        return this.filePanel;
    }

}
