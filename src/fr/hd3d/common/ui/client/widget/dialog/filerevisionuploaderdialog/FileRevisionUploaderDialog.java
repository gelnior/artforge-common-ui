package fr.hd3d.common.ui.client.widget.dialog.filerevisionuploaderdialog;

import java.util.ArrayList;

import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.Style.VerticalAlignment;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.FormEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.HorizontalPanel;
import com.extjs.gxt.ui.client.widget.Info;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.FileUploadField;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.form.HiddenField;
import com.extjs.gxt.ui.client.widget.form.FormPanel.Method;
import com.extjs.gxt.ui.client.widget.layout.FormData;
import com.extjs.gxt.ui.client.widget.menu.SeparatorMenuItem;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Image;

import fr.hd3d.common.ui.client.error.Hd3dException;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.modeldata.asset.FileRevisionModelData;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.ServicesPath;


/**
 * Dialog to upload file revision and proxies. This dialog is a singleton.
 * 
 * @author HD3D
 * 
 */
public class FileRevisionUploaderDialog extends Dialog
{
    public static String FILE_VAR_FORM = "file";

    /** Singleton instance. */
    private static FileRevisionUploaderDialog instance;

    /** File field. */
    private final FileUploadField fileRevision = new FileUploadField();

    /** Field used to provide id of asset that will be linked file revision created for uploaded file */
    private final HiddenField<String> assetField = new HiddenField<String>();

    /** Panel containing the form to upload a file revision. */
    private final FormPanel formFileRevision = new FormPanel();

    /** Event forward when all files are submitted. */
    private EventType okEvent;
    /** List of panel to upload proxies. */
    private ArrayList<ProxyCreatorPanel> proxyPanelList = new ArrayList<ProxyCreatorPanel>();
    /** Panel containing panels to upload proxies. */
    private final LayoutContainer additionnalProxyPanel = new LayoutContainer();

    /** The model. */
    private final FileRevisionUploaderModel model;

    /**
     * Default contructor.
     */
    private FileRevisionUploaderDialog()
    {
        model = new FileRevisionUploaderModel();
        new FileRevisionUploaderController(this, model);

        this.setHeading("Upload new file and previews");
        this.okText = "Submit";
        this.setButtons(OKCANCEL);
        this.setLayoutOnChange(true);
        this.setAutoHeight(true);
        this.setWidth(400);

        additionnalProxyPanel.setLayoutOnChange(true);
        createFormFileRevision();
        setMessageWarning();
        this.add(formFileRevision);
        this.add(additionnalProxyPanel);
        this.setListeners();
    }

    /**
     * Set a warning message (upload can't be cancelled).
     */
    private void setMessageWarning()
    {
        HorizontalPanel panel = new HorizontalPanel();
        Image warning = Hd3dImages.getWarningIcon().createImage();
        panel.add(warning);

        panel.addText("Submitted files can't be cancelled!!");
        this.fbar.insert(panel, 0);
    }

    /**
     * Set listeners.
     */
    private void setListeners()
    {
        formFileRevision.addListener(Events.Submit, new Listener<FormEvent>() {
            public void handleEvent(FormEvent be)
            {
                onSubmitFinished(be);
            }
        });
    }

    /**
     * Submit finished event.
     * 
     * @param be
     */
    private void onSubmitFinished(FormEvent be)
    {
        Info.display("File submitted...", "Send previews file");
        AppEvent appEvent = new AppEvent(FileRevisionUploaderEvent.FILE_SUBMITTED);
        appEvent.setData(be.getResultHtml().replaceAll("[^0-9]", ""));
        EventDispatcher.forwardEvent(appEvent);
    }

    /** Mode proxy add. */
    private static final int ADD_MODE = 1;
    /** Model proxy remove. */
    private static final int REMOVE_MODE = 2;

    /**
     * Add a proxy panel to upload.
     * 
     * @param mode
     *            the mode(add or remove).
     */
    private void addFormProxies(int mode)
    {
        ProxyCreatorPanel proxyPanel = new ProxyCreatorPanel(FileRevisionUploaderEvent.PROXY_FILE_SUBMITTED);

        proxyPanelList.add(proxyPanel);

        final SeparatorMenuItem separator = new SeparatorMenuItem();

        final HorizontalPanel hpanel = new HorizontalPanel();
        hpanel.setHorizontalAlign(HorizontalAlignment.CENTER);
        hpanel.setVerticalAlign(VerticalAlignment.MIDDLE);
        hpanel.setSpacing(5);
        Image actionPanel = null;
        switch (mode)
        {
            case ADD_MODE:
                actionPanel = Hd3dImages.getAddIcon().createImage();
                actionPanel.setTitle("Add a new proxy uploader");
                actionPanel.addClickHandler(new ClickHandler() {
                    public void onClick(ClickEvent event)
                    {
                        addFormProxies(REMOVE_MODE);
                    }
                });
                break;
            case REMOVE_MODE:
                actionPanel = Hd3dImages.getDeleteIcon().createImage();
                actionPanel.setTitle("Remove this proxy uploader");
                actionPanel.addClickHandler(new ClickHandler() {
                    public void onClick(ClickEvent event)
                    {
                        additionnalProxyPanel.remove(separator);
                        additionnalProxyPanel.remove(hpanel);
                    }
                });
                break;
        }
        DOM.setStyleAttribute(actionPanel.getElement(), "cursor", "pointer");
        hpanel.add(actionPanel);
        hpanel.add(proxyPanel);
        additionnalProxyPanel.add(separator);
        additionnalProxyPanel.add(hpanel);
    }

    /**
     * Create the form to upload the file revision.
     */
    private void createFormFileRevision()
    {
        this.formFileRevision.setBorders(false);
        this.formFileRevision.setBodyBorder(false);
        this.formFileRevision.setHeaderVisible(false);
        this.formFileRevision.setMethod(Method.POST);
        this.formFileRevision.setEncoding(FormPanel.Encoding.MULTIPART);

        String servicesUrl = RestRequestHandlerSingleton.getInstance().getServicesUrl();
        this.formFileRevision.setAction(servicesUrl + ServicesPath.UPLOAD);

        this.fileRevision.setFieldLabel("File");
        this.fileRevision.setName(FILE_VAR_FORM);
        this.formFileRevision.add(this.fileRevision, new FormData("100%"));

        this.assetField.setName("assetId");
        this.formFileRevision.add(this.assetField);
    }

    /**
     * Return the instance of singleton.
     * 
     * @return the instance.
     */
    public static FileRevisionUploaderDialog get()
    {
        if (instance == null)
        {
            instance = new FileRevisionUploaderDialog();
        }
        return instance;
    }

    /**
     * Set the event forwarded when all files will be submitted.
     * 
     * @param event
     *            the event.
     */
    public void setEvent(EventType event)
    {
        this.okEvent = event;
    }

    /**
     * Set the asset revision.
     * 
     * @param assetId
     *            the asset revision id.
     */
    public void setAssetRevision(Long assetId)
    {
        this.assetField.setValue(assetId.toString());
    }

    @Override
    protected void onButtonPressed(Button button)
    {
        try
        {
            if (button.equals(getButtonById(OK)))
            {
                if (fileRevision.getValue() == null)
                {
                    throw new Hd3dException("No file selected !");
                }
                if (!checkProxy())
                {
                    MessageBox.confirm("Warning", "There is no preview file. Do you want to upload it ?",
                            new Listener<MessageBoxEvent>() {

                                public void handleEvent(MessageBoxEvent be)
                                {
                                    if (be.getButtonClicked().getItemId() == Dialog.NO)
                                    {
                                        formFileRevision.submit();
                                    }
                                }

                            });
                }
                else
                {
                    this.formFileRevision.submit();
                }
            }
            else
            {
                hide();
            }
        }
        catch (Hd3dException e)
        {
            e.print();
        }
    }

    private boolean checkProxy() throws Hd3dException
    {
        int validProxy = 0;
        for (ProxyCreatorPanel proxyPanel : proxyPanelList)
        {
            if (proxyPanel.checkField())
            {
                validProxy++;
            }
        }
        if (validProxy == 0)
        {
            return false;
        }
        return true;
    }

    /**
     * Reset forms.
     */
    public void reset()
    {
        proxyPanelList = new ArrayList<ProxyCreatorPanel>();
        additionnalProxyPanel.removeAll();
        this.fileRevision.setValue(null);
    }

    /**
     * Show the dialog and set asset revision and the event will forwarded when all files will be submitted.
     * 
     * @param assetId
     *            the asset revision id
     * @param eventType
     *            the event.
     */
    public void show(Long assetId, EventType eventType)
    {
        setAssetRevision(assetId);
        setEvent(eventType);
        show();
    }

    @Override
    public void show()
    {
        this.reset();
        super.show();
        addFormProxies(ADD_MODE);
    }

    /**
     * Submit the proxy file from the proxy form which don't submit yet.
     * 
     * @return false if all proxies are submitted.
     * @throws Hd3dException
     */
    public boolean submitNextProxy() throws Hd3dException
    {
        for (ProxyCreatorPanel panel : proxyPanelList)
        {
            if (!panel.isSubmit())
            {
                panel.submit(this.model.getFileRevision());
                return true;
            }
        }
        return false;
    }

    /**
     * Return the event .
     * 
     * @return the event.
     */
    public EventType getEvent()
    {
        return okEvent;
    }

    /**
     * Display a message to inform the user all files are submitted.
     */
    public void displayAllFileSubmittedMessage()
    {
        MessageBox.alert("Upload successfull", "All files are submitted.", new Listener<MessageBoxEvent>() {
            public void handleEvent(MessageBoxEvent be)
            {
                hide();
                FileRevisionModelData file = new FileRevisionModelData();
                file.setId(model.getFileRevision());
                AppEvent submittedEvent = new AppEvent(getEvent());
                submittedEvent.setData(file);
                file.refresh(submittedEvent);
            };
        });
    }
}
