package fr.hd3d.common.ui.client.widget.dialog.fileselectordialog.cmd;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.modeldata.asset.FileRevisionModelData;
import fr.hd3d.common.ui.client.mvc.controller.BaseCommand;
import fr.hd3d.common.ui.client.widget.dialog.fileselectordialog.FileSelectorDialog;
import fr.hd3d.common.ui.client.widget.dialog.fileselectordialog.FileSelectorModel;


/**
 * Command to removed the selected file revision from the view
 * 
 * @author HD3D
 * 
 */
public class SelectedFileRevisionRemovedCmd implements BaseCommand
{
    private final FileSelectorDialog view;
    private final FileSelectorModel model;

    /**
     * Default constructor.
     * 
     * @param view
     *            the view
     * @param model
     *            the model
     */
    public SelectedFileRevisionRemovedCmd(FileSelectorDialog view, FileSelectorModel model)
    {
        super();
        this.view = view;
        this.model = model;
    }

    public void execute(AppEvent event)
    {
        FileRevisionModelData file = event.getData();
        if (file == null)
        {
            return;
        }
        this.model.removeFileRevision(file);
        this.view.removeSelectedFilesRevision(file);
    }
}
