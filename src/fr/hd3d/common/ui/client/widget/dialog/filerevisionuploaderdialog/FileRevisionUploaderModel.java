package fr.hd3d.common.ui.client.widget.dialog.filerevisionuploaderdialog;

/**
 * Model to upload file revision.
 * 
 * @author HD3D
 * 
 */
public class FileRevisionUploaderModel
{
    /**
     * The file revision id.
     */
    private Long fileId;

    /**
     * Set the file revision.
     * 
     * @param fileId
     *            the file revivion id
     */
    public void setFileRevision(Long fileId)
    {
        this.fileId = fileId;
    }

    /**
     * Return the file revision.
     * 
     * @return the file revision id.
     */
    public Long getFileRevision()
    {
        return fileId;
    }
}
