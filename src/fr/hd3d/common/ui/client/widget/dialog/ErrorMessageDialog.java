package fr.hd3d.common.ui.client.widget.dialog;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;


/**
 * Tools for error message handling.
 * 
 * @author HD3D
 */
public class ErrorMessageDialog
{
    static ServerErrorDialog dialog;

    /**
     * Display an error dialog that displays an error message with an additional information message. Additional
     * informations are hidden and can be shown by user via a button.
     * 
     * @param errorHeader
     *            The dialog header.
     * @param errorTxt
     *            The user message.
     * @param stack
     *            The technical message.
     * @param callback
     *            The dialog call back to apply when dialog is hidden.
     */
    public static void alert(String errorHeader, String errorTxt, String stack, final Listener<MessageBoxEvent> callback)
    {
        if (dialog == null)
        {
            dialog = new ServerErrorDialog(errorHeader, errorTxt, stack);

            dialog.addListener(Events.Hide, new Listener<BaseEvent>() {
                public void handleEvent(BaseEvent be)
                {
                    MessageBoxEvent event = new MessageBoxEvent(null, dialog, null);
                    callback.handleEvent(event);
                }
            });
        }
        else
        {
            dialog.updateMsg(errorHeader, errorTxt, stack);
        }

        dialog.show();
        dialog.setDefaultSize();
    }
}
