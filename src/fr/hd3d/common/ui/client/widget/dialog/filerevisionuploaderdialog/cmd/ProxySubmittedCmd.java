package fr.hd3d.common.ui.client.widget.dialog.filerevisionuploaderdialog.cmd;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.error.Hd3dException;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.mvc.controller.BaseCommand;
import fr.hd3d.common.ui.client.widget.dialog.filerevisionuploaderdialog.FileRevisionUploaderDialog;
import fr.hd3d.common.ui.client.widget.dialog.filerevisionuploaderdialog.FileRevisionUploaderEvent;
import fr.hd3d.common.ui.client.widget.dialog.filerevisionuploaderdialog.FileRevisionUploaderModel;


/**
 * Command to submit the next proxy file.
 * 
 * @author HD3D
 * 
 */
public class ProxySubmittedCmd implements BaseCommand
{
    private final FileRevisionUploaderDialog view;
    @SuppressWarnings("unused")
    private final FileRevisionUploaderModel model;

    /**
     * Default constructor.
     * 
     * @param view
     *            the view
     * @param model
     *            the model
     */
    public ProxySubmittedCmd(FileRevisionUploaderDialog view, FileRevisionUploaderModel model)
    {
        this.view = view;
        this.model = model;
    }

    public void execute(AppEvent event)
    {
        try
        {
            if (!this.view.submitNextProxy())
            {
                EventDispatcher.forwardEvent(FileRevisionUploaderEvent.ALL_FILE_SUBMITTED);
            }
        }
        catch (Hd3dException e)
        {
            e.print();
        }
    }
}
