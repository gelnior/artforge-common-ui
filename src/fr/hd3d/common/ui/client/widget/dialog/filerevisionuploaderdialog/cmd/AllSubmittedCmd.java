package fr.hd3d.common.ui.client.widget.dialog.filerevisionuploaderdialog.cmd;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.mvc.controller.BaseCommand;
import fr.hd3d.common.ui.client.widget.dialog.filerevisionuploaderdialog.FileRevisionUploaderDialog;
import fr.hd3d.common.ui.client.widget.dialog.filerevisionuploaderdialog.FileRevisionUploaderModel;


/**
 * Command to inform the user all files are submitted.
 * 
 * @author HD3D
 * 
 */
public class AllSubmittedCmd implements BaseCommand
{
    private final FileRevisionUploaderDialog view;
    @SuppressWarnings("unused")
    private final FileRevisionUploaderModel model;

    /**
     * Default constructor.
     * 
     * @param view
     *            the view
     * @param model
     *            the model
     */
    public AllSubmittedCmd(FileRevisionUploaderDialog view, FileRevisionUploaderModel model)
    {
        this.view = view;
        this.model = model;
    }

    public void execute(AppEvent event)
    {
        this.view.displayAllFileSubmittedMessage();
    }

}
