package fr.hd3d.common.ui.client.widget.dialog.filerevisionuploaderdialog;

import com.extjs.gxt.ui.client.event.EventType;


public class FileRevisionUploaderEvent
{

    public static final EventType FILE_SUBMITTED = new EventType();
    public static final EventType PROXY_FILE_SUBMITTED = new EventType();
    public static final EventType ALL_FILE_SUBMITTED = new EventType();

}
