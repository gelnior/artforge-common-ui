package fr.hd3d.common.ui.client.widget.dialog.filerevisionuploaderdialog.cmd;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.error.Hd3dException;
import fr.hd3d.common.ui.client.mvc.controller.BaseCommand;
import fr.hd3d.common.ui.client.widget.dialog.filerevisionuploaderdialog.FileRevisionUploaderDialog;
import fr.hd3d.common.ui.client.widget.dialog.filerevisionuploaderdialog.FileRevisionUploaderModel;


/**
 * Command to set the file revision and submit the first proxy file.
 * 
 * @author HD3D
 * 
 */
public class FileSubmittedCmd implements BaseCommand
{
    private final FileRevisionUploaderDialog view;
    private final FileRevisionUploaderModel model;

    /**
     * Default constructor.
     * 
     * @param view
     *            the view
     * @param model
     *            the model
     */
    public FileSubmittedCmd(FileRevisionUploaderDialog view, FileRevisionUploaderModel model)
    {
        this.view = view;
        this.model = model;
    }

    public void execute(AppEvent event)
    {
        try
        {
            Long fileId = Long.valueOf((String) event.getData());
            this.model.setFileRevision(fileId);

            this.view.submitNextProxy();
        }
        catch (Hd3dException e)
        {
            e.print();
        }
    }

}
