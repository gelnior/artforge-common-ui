package fr.hd3d.common.ui.client.widget.dialog;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.config.CommonConfig;
import fr.hd3d.common.ui.client.error.Hd3dException;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.asset.FileRevisionModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;


class CommentTaskDialogController extends MaskableController
{
    private final CommentTaskDialog view;

    public CommentTaskDialogController(CommentTaskDialog view)
    {
        this.view = view;
        this.registerEvent();
        EventDispatcher.get().addController(this);
    }

    private void registerEvent()
    {
        this.registerEventTypes(CommentTaskDialogEvent.FILE_UPLOADED);
        this.registerEventTypes(CommentTaskDialogEvent.FILE_REFRESH);
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();
        if (type == CommentTaskDialogEvent.FILE_UPLOADED)
        {
            onFileUploaded(event);
        }
        else if (type == CommentTaskDialogEvent.FILE_REFRESH)
        {
            onFileRefresh(event);
        }

    }

    private void onFileRefresh(AppEvent event)
    {
        FileRevisionModelData fileRevision = event.getData(CommonConfig.MODEL_EVENT_VAR_NAME);
        // this.view.setFileRevision(fileRevision);
        // this.view.displayProxyCreator();
    }

    private void onFileUploaded(AppEvent event)
    {
        String idFileRevision = event.getData();
        try
        {
            try
            {
                Long idFile = Long.parseLong(idFileRevision);
                FileRevisionModelData file = new FileRevisionModelData();
                file.setId(idFile);
                file.refresh(CommentTaskDialogEvent.FILE_REFRESH);
            }
            catch (NumberFormatException nfe)
            {
                throw new Hd3dException("Error during the upload, the id of file is incorrect.");
            }

        }
        catch (Hd3dException e)
        {
            e.print();
        }
    }
}
