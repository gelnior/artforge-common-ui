package fr.hd3d.common.ui.client.widget.dialog.fileselectordialog;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.mvc.controller.CommandController;
import fr.hd3d.common.ui.client.widget.dialog.fileselectordialog.cmd.AssetRevisionLoadedCmd;
import fr.hd3d.common.ui.client.widget.dialog.fileselectordialog.cmd.FileRevisionLoadedCmd;
import fr.hd3d.common.ui.client.widget.dialog.fileselectordialog.cmd.FileRevisionSelectedCmd;
import fr.hd3d.common.ui.client.widget.dialog.fileselectordialog.cmd.SelectedFileRevisionRemovedCmd;


/**
 * Controller to select file revisions.
 * 
 * @author HD3D
 * 
 */
public class FileSelectorController extends CommandController
{
    /** The view. */
    private final FileSelectorDialog view;
    /** The model. */
    private final FileSelectorModel model;

    /**
     * Default constructor.
     * 
     * @param view
     *            the view
     * @param model
     *            the model
     */
    public FileSelectorController(FileSelectorDialog view, FileSelectorModel model)
    {
        this.view = view;
        this.model = model;
        this.registerEvents();
        EventDispatcher.get().addController(this);
    }

    /**
     * Register events.
     */
    private void registerEvents()
    {
        commandMap.put(FileSelectorEvent.FILE_REVISION_LOADED, new FileRevisionLoadedCmd(view, model));
        commandMap.put(FileSelectorEvent.ASSET_REVISION_LOADED, new AssetRevisionLoadedCmd(view, model));
        commandMap.put(FileSelectorEvent.FILE_REVISION_SELECTED, new FileRevisionSelectedCmd(view, model));
        commandMap.put(FileSelectorEvent.SELECTED_FILE_REVISION_REMOVED,
                new SelectedFileRevisionRemovedCmd(view, model));
    }
}
