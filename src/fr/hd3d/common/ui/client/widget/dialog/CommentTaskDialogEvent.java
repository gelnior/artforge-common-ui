package fr.hd3d.common.ui.client.widget.dialog;

import com.extjs.gxt.ui.client.event.EventType;


public class CommentTaskDialogEvent
{

    public static final EventType FILE_UPLOADED = new EventType();
    public static final EventType FILE_REFRESH = new EventType();
    public static final EventType PROXY_ADDED = new EventType();

}
