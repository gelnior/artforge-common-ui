package fr.hd3d.common.ui.client.widget.dialog.fileselectordialog;

import com.extjs.gxt.ui.client.event.EventType;


public class FileSelectorEvent
{
    public static final EventType FILE_REVISION_LOADED = new EventType();
    public static final EventType FILE_REVISION_SELECTED = new EventType();
    public static final EventType SELECTED_FILE_REVISION_REMOVED = new EventType();
    public static final EventType ASSET_REVISION_LOADED = new EventType();
}
