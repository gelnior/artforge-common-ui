package fr.hd3d.common.ui.client.widget.dialog;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.KeyListener;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.layout.FormData;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.KeyCodes;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.EventDispatcher;


/**
 * Name dialog is an OK/Cancel dialog with a form containing only one field : a name field. When OK button is clicked an
 * event is forwarded with the name field valua as attachment.
 * 
 * @author HD3D
 */
public class NameDialog extends Dialog
{
    /** Constant strings from common library. */
    public static final CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);

    /** The text field used to enter name. */
    protected final TextField<String> nameTextField = new TextField<String>();
    /** Event forwarded when OK button is clicked. */
    protected final EventType okEvent;
    /** The panel containing the name text field. */
    protected FormPanel panel = new FormPanel();

    /**
     * Constructor
     * 
     * @param okEvent
     *            The event forwarded when OK. button is clicked
     * @param header
     *            The dialog header.
     */
    public NameDialog(final EventType okEvent, String header)
    {
        this.okEvent = okEvent;

        FormData formData = new FormData("-20");

        nameTextField.setFieldLabel(COMMON_CONSTANTS.Name());

        panel.setBorders(false);
        panel.setHeaderVisible(false);
        panel.add(nameTextField, formData);
        panel.setBodyBorder(false);

        this.add(panel);
        this.setHeading(header);
        this.setHideOnButtonClick(true);
        this.setButtons(Dialog.OKCANCEL);
        this.setShadow(false);

        this.setListeners();
    }

    /**
     * When dialog is displayed, it focus on the name text field (field is cleared).
     */
    @Override
    public void show()
    {
        super.show();
        this.setFocusWidget(panel);
        this.nameTextField.clear();
        this.nameTextField.focus();
    }

    /**
     * Clear name field.
     */
    public void clearNameField()
    {
        this.nameTextField.clear();
    }

    /**
     * When OK button is clicked <i>okEvent</i> is forwarded with name text field value as attachment.
     */
    protected void onOkClicked()
    {
        AppEvent event = new AppEvent(okEvent, nameTextField.getValue());
        EventDispatcher.forwardEvent(event);
    }

    /**
     * Set click listener on ok button and enter key pressed on name text field.
     */
    private void setListeners()
    {
        Button okButton = (Button) this.getButtonBar().getItemByItemId(Dialog.OK);

        SelectionListener<ButtonEvent> listener = new SelectionListener<ButtonEvent>() {
            @Override
            public void componentSelected(ButtonEvent ce)
            {
                onOkClicked();
            }
        };

        this.nameTextField.addKeyListener(new KeyListener() {
            @Override
            public void componentKeyUp(ComponentEvent event)
            {
                if (event.getKeyCode() == KeyCodes.KEY_ENTER)
                {
                    hide();
                    onOkClicked();
                }
            }
        });

        okButton.addSelectionListener(listener);
    }

}
