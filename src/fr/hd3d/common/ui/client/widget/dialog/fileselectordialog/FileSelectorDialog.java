package fr.hd3d.common.ui.client.widget.dialog.fileselectordialog;

import java.util.List;

import com.extjs.gxt.ui.client.Style.Scroll;
import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.util.Margins;
import com.extjs.gxt.ui.client.util.Padding;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.Text;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.layout.HBoxLayout;
import com.extjs.gxt.ui.client.widget.layout.HBoxLayoutData;
import com.extjs.gxt.ui.client.widget.layout.RowLayout;
import com.extjs.gxt.ui.client.widget.layout.HBoxLayout.HBoxLayoutAlign;

import fr.hd3d.common.ui.client.error.Hd3dException;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.asset.FileRevisionModelData;
import fr.hd3d.common.ui.client.widget.dialog.filerevisionuploaderdialog.FileRevisionUploaderDialog;


/**
 * Class which allow to select the files from a task. It display the different asset revisions and their variations. It
 * could select a file and this revision. It could remove a file too if you don't want anymore.
 * 
 * @author HD3D
 * 
 */
public class FileSelectorDialog extends Dialog
{
    /** Event fired when ok is clicked. */
    public static final EventType FILE_REVISION_SELECTED = new EventType();

    /** Instance of singleton. */
    private static FileSelectorDialog instance;

    /** Model to manage the data. */
    private final FileSelectorModel model;

    /** Panel which containing the file to selected. */
    private final LayoutContainer fileToSelectPanel = new LayoutContainer();

    /** Panel which containing the selected file. */
    private ContentPanel selectedFilePanel;

    /** Button to upload a file. */
    private Button uploadFile;

    /**
     * Return the instance of singleton.
     * 
     * @return the dialog
     */
    public static FileSelectorDialog getInstance()
    {
        if (instance == null)
        {
            instance = new FileSelectorDialog();
        }
        return instance;
    }

    /**
     * Default Constructor.
     */
    protected FileSelectorDialog()
    {
        model = new FileSelectorModel();
        new FileSelectorController(this, model);
        this.styles();

    }

    /**
     * Set the style of the dialog.
     */
    private void styles()
    {
        this.setButtons(Dialog.OKCANCEL);
        this.setHideOnButtonClick(true);
        this.setScrollMode(Scroll.AUTO);
        this.setLayoutOnChange(true);

        this.setHeight(400);
        this.setWidth(300);
        this.setLayout(new RowLayout());

        this.setHeading("Select a file to link to the note");

        this.fileToSelectPanel.setLayout(new RowLayout());
        this.fileToSelectPanel.setScrollMode(Scroll.AUTOY);
        this.fileToSelectPanel.setHeight(150);
        this.fileToSelectPanel.setLayoutOnChange(true);
        this.add(fileToSelectPanel);

        uploadFile = new Button("Upload File", new SelectionListener<ButtonEvent>() {
            @Override
            public void componentSelected(ButtonEvent ce)
            {
                FileRevisionUploaderDialog.get().show(model.getAssetRevision(),
                        FileSelectorEvent.FILE_REVISION_SELECTED);
            };
        });
        LayoutContainer panel = new LayoutContainer();
        HBoxLayout layout = new HBoxLayout();
        layout.setPadding(new Padding(5));
        layout.setHBoxLayoutAlign(HBoxLayoutAlign.MIDDLE);
        panel.setLayout(layout);
        HBoxLayoutData flex = new HBoxLayoutData(new Margins(0, 5, 0, 0));
        flex.setFlex(1);
        panel.add(uploadFile, flex);
        this.add(panel);

        this.selectedFilePanel = new ContentPanel();
        this.selectedFilePanel.setLayout(new RowLayout());
        this.selectedFilePanel.setLayoutOnChange(true);
        selectedFilePanel.setAutoHeight(true);
        selectedFilePanel.setBorders(true);
        selectedFilePanel.setHeading("Selected Files");
        this.add(selectedFilePanel);

    }

    /**
     * Set the file revision in the file to select panel.
     * 
     * @param filesMap
     *            the map of file (key : file.key , value : list of file revision with the file.key).
     */
    public void setFileRevisionsToSelect(FastMap<ListStore<FileRevisionModelData>> filesMap)
    {
        LayoutContainer p = new LayoutContainer();
        if (filesMap == null || filesMap.keySet().isEmpty())
        {
            Text noFileText = new Text("No file for this Asset...");
            fileToSelectPanel.add(noFileText);
        }
        else
        {
            for (ListStore<FileRevisionModelData> files : filesMap.values())
            {
                FileSelectorPanel filePanel = new FileSelectorPanel(files, FileSelectorPanel.ADD_MODE,
                        FileSelectorEvent.FILE_REVISION_SELECTED, FileSelectorEvent.SELECTED_FILE_REVISION_REMOVED);

                p.add(filePanel.getPanel());
            }
            fileToSelectPanel.add(p);
        }
    }

    /**
     * Set a mask on the file to select panel.
     */
    public void maskFileToSelect()
    {
        fileToSelectPanel.mask("Loading .... ");
        uploadFile.disable();
    }

    /**
     * Remove the mask on the file to select panel.
     */
    public void unmaskFileToSelect()
    {
        fileToSelectPanel.unmask();
        uploadFile.enable();
    }

    public void disableUploadFileButton()
    {
        uploadFile.disable();
    }

    public void enableUploadFileButton()
    {
        uploadFile.enable();
    }

    /**
     * Set the given file revision in the selected file panel.
     * 
     * @param file
     *            the selected file.
     */
    public void setSelectedFilesRevision(FileRevisionModelData file)
    {
        ListStore<FileRevisionModelData> list = new ListStore<FileRevisionModelData>();
        list.add(file);
        FileSelectorPanel filePanel = new FileSelectorPanel(list, FileSelectorPanel.REMOVE_MODE,
                FileSelectorEvent.FILE_REVISION_SELECTED, FileSelectorEvent.SELECTED_FILE_REVISION_REMOVED);

        selectedFilePanel.add(filePanel.getPanel());
    }

    /**
     * Remove the given file revision in the selected file panel.
     * 
     * @param fileRemoved
     *            the deselected file.
     */
    public void removeSelectedFilesRevision(FileRevisionModelData fileRemoved)
    {
        selectedFilePanel.removeAll();
        List<FileRevisionModelData> files = this.model.getFileRevisionSelected().getModels();
        if (files == null)
        {
            return;
        }

        for (FileRevisionModelData file : files)
        {
            ListStore<FileRevisionModelData> list = new ListStore<FileRevisionModelData>();
            list.add(file);
            FileSelectorPanel filePanel = new FileSelectorPanel(list, FileSelectorPanel.REMOVE_MODE,
                    FileSelectorEvent.FILE_REVISION_SELECTED, FileSelectorEvent.SELECTED_FILE_REVISION_REMOVED);

            selectedFilePanel.add(filePanel.getPanel());
        }
    }

    /**
     * Set the data to configure the dialog.
     * 
     * @param task
     *            the task.
     */
    public void setData(Hd3dModelData task)
    {
        this.model.setData(task);
    }

    @Override
    public void show()
    {
        this.model.reset();
        this.fileToSelectPanel.removeAll();
        this.selectedFilePanel.removeAll();
        super.show();
        this.model.reload();
        this.maskFileToSelect();
    }

    @Override
    protected void onButtonPressed(Button button)
    {
        try
        {
            if (button.equals(getButtonById(Dialog.OK)))
            {
                List<FileRevisionModelData> fileRevisions = this.model.getFileRevisionSelected().getModels();
                if (fileRevisions == null || fileRevisions.isEmpty())
                {
                    throw new Hd3dException("No file selected!!");
                }
                AppEvent event = new AppEvent(FILE_REVISION_SELECTED);
                event.setData(fileRevisions);
                fireEvent(FILE_REVISION_SELECTED, event);
                EventDispatcher.forwardEvent(event);
            }
            super.onButtonPressed(button);
        }
        catch (Hd3dException e)
        {
            e.print();
        }
    }

    /**
     * Display a message to inform the asset is not valid.
     */
    public void noValidAssetMode()
    {
        this.uploadFile.disable();
        MessageBox.alert("Warning", "The file tree didn't create or isn't valid.", null);
    }
}
