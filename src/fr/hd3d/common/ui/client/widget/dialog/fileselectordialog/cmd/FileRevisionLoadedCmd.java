package fr.hd3d.common.ui.client.widget.dialog.fileselectordialog.cmd;

import java.util.List;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.ListStore;

import fr.hd3d.common.ui.client.modeldata.asset.FileRevisionModelData;
import fr.hd3d.common.ui.client.mvc.controller.BaseCommand;
import fr.hd3d.common.ui.client.widget.dialog.fileselectordialog.FileSelectorDialog;
import fr.hd3d.common.ui.client.widget.dialog.fileselectordialog.FileSelectorModel;


/**
 * Command to set the file revision loaded in the view.
 * 
 * @author HD3D
 * 
 */
public class FileRevisionLoadedCmd implements BaseCommand
{
    private final FileSelectorDialog view;
    private final FileSelectorModel model;

    /**
     * Default constructor.
     * 
     * @param view
     *            the view
     * @param model
     *            the model
     */
    public FileRevisionLoadedCmd(FileSelectorDialog view, FileSelectorModel model)
    {
        super();
        this.view = view;
        this.model = model;
    }

    public void execute(AppEvent event)
    {
        this.view.unmaskFileToSelect();
        List<FileRevisionModelData> files = this.model.getFileStore().getModels();
        if (files == null)
        {
            return;
        }
        if (files.isEmpty())
        {
            this.model.loadAssetRevision();
            this.view.maskFileToSelect();
        }
        else
        {

            FastMap<ListStore<FileRevisionModelData>> listRevisionById = new FastMap<ListStore<FileRevisionModelData>>();
            for (FileRevisionModelData file : files)
            {
                ListStore<FileRevisionModelData> fileRevisions = listRevisionById.get(file.getName());
                if (fileRevisions == null)
                {
                    fileRevisions = new ListStore<FileRevisionModelData>();
                    listRevisionById.put(file.getName(), fileRevisions);
                }
                fileRevisions.add(file);
            }

            this.view.setFileRevisionsToSelect(listRevisionById);
        }
    }

}
