package fr.hd3d.common.ui.client.widget.dialog;

import com.extjs.gxt.ui.client.Style.Scroll;
import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.ResizeEvent;
import com.extjs.gxt.ui.client.util.TextMetrics;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.Text;
import com.extjs.gxt.ui.client.widget.button.ToggleButton;
import com.extjs.gxt.ui.client.widget.form.FieldSet;
import com.extjs.gxt.ui.client.widget.layout.AnchorLayout;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.HBoxLayout;
import com.extjs.gxt.ui.client.widget.layout.HBoxLayout.HBoxLayoutAlign;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;


/**
 * Dialog adapted to display server error. It displays the error message dedicated to user. It also has an additional
 * information message containing technical error message. This part is hidden and is displayed when user click on the
 * "more" button.
 * 
 * @author HD3D
 */
public class ServerErrorDialog extends Dialog
{
    /** Constant strings to display : dialog messages, button label... */
    public static final CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    protected static final int DEFAULT_WIDTH = 400;
    protected static final int DEFAULT_HEIGHT = 150;
    protected static final int DEFAULT_HEIGHT_EXPANDED = 310;

    /** Message dedicated to user. */
    protected Text msgText = new Text();
    /** The "see more" button that displays/hides additional informations. */
    protected ToggleButton button = new ToggleButton("+");
    /** Container for additional informations text. */
    protected LayoutContainer area = new LayoutContainer();
    /** Technical error message. */
    protected Text stackText = new Text();
    /** Field set containing additional informations. */
    FieldSet fieldSet = new FieldSet();

    /**
     * Default constructor.
     * 
     * @param error
     *            Text displayed as header.
     * @param errorTxt
     *            User message.
     * @param stack
     *            Technical message.
     */
    public ServerErrorDialog(String error, String errorTxt, String stack)
    {
        this.setStackArea(stack);
        this.setText(errorTxt);
        this.setMoreInformations();
        this.setStyles(error);
        this.setListeners();
    }

    /**
     * Update message displayed in error dialog box.
     * 
     * @param header
     *            Header text.
     * @param errorTxt
     *            Message dedicated to user.
     * @param stack
     *            Technical error message.
     */
    public void updateMsg(String header, String errorTxt, String stack)
    {
        this.setHeading(header);
        this.msgText.setText(errorTxt);
        this.stackText.setText(stack);
        this.area.layout();

        if (button.isPressed())
        {
            area.hide();
            button.setText("+");
            button.toggle(false);
            TextMetrics.get().setFixedWidth(DEFAULT_WIDTH);
            this.setHeight(DEFAULT_HEIGHT + TextMetrics.get().getHeight(msgText.getText()));
            layout();
        }
    }

    /**
     * Resize the dialog to its default width and height (DEFAULT_WIDTH and DEFAULT_HEIGHT).
     */
    public void setDefaultSize()
    {
        setWidth(DEFAULT_WIDTH);
        TextMetrics.get().setFixedWidth(DEFAULT_WIDTH);
        this.setHeight(DEFAULT_HEIGHT + TextMetrics.get().getHeight(msgText.getText()));
    }

    /**
     * Set dialog styles, such as layout, size...
     * 
     * @param header
     *            Dialog header
     */
    protected void setStyles(String header)
    {
        this.setHeading(header);
        this.setWidth(DEFAULT_WIDTH);
        TextMetrics.get().setFixedWidth(DEFAULT_WIDTH);
        this.setHeight(DEFAULT_HEIGHT + TextMetrics.get().getHeight(msgText.getText()));
        this.setHideOnButtonClick(true);
        this.setButtons(Dialog.OK);
        this.setLayout(new AnchorLayout());
    }

    @Override
    public void show()
    {
        super.show();
        this.fieldSet.collapse();
        this.area.setVisible(false);
    }

    @Override
    public void onEndResize(ResizeEvent re)
    {
        super.onEndResize(re);
        this.updateAreaSizeToDialog();
    }

    /**
     * Resize detail text area to fit in the error dialog.
     */
    private void updateAreaSizeToDialog()
    {
        this.area.setHeight(this.getHeight() - this.msgText.getHeight() - 104);
        this.area.setWidth(this.getWidth() - 30);
    }

    /**
     * Add user message to the dialog.
     * 
     * @param errorTxt
     *            Message dedicated to the user.
     */
    protected void setText(String errorTxt)
    {
        msgText.setText(errorTxt);
        msgText.setStyleAttribute("padding", "10px");
        msgText.setStyleAttribute("font-size", "1.2em");
        msgText.setStyleAttribute("font-weight", "bold");
        this.add(msgText);
    }

    /**
     * Add the "see more" part to the dialog.
     */
    protected void setMoreInformations()
    {
        fieldSet.setHeading(CONSTANTS.AdditionalInformations());
        fieldSet.setCollapsible(true);

        area.setLayout(new FitLayout());
        area.setHeight(150);
        area.setScrollMode(Scroll.AUTO);
        area.add(stackText);

        LayoutContainer moreInformations = new LayoutContainer();
        HBoxLayout layout = new HBoxLayout();
        layout.setHBoxLayoutAlign(HBoxLayoutAlign.BOTTOM);

        moreInformations.setLayout(layout);

        fieldSet.collapse();

        this.add(fieldSet);
        this.fieldSet.add(area);
    }

    /**
     * Set stack text in the appropriate text widget.
     * 
     * @param stack
     *            Technical error message.
     */
    protected void setStackArea(String stack)
    {
        stackText.setText(stack);
        stackText.setHeight(150);
        stackText.setWidth(350);
        area.setStyleAttribute("border", "1px solid #999");

        area.setStyleAttribute("padding", "10px");
        area.setStyleAttribute("background", "white");
    }

    /**
     * Set listener on "see more" button.
     */
    protected void setListeners()
    {
        fieldSet.addListener(Events.Expand, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                layout();
                area.setVisible(true);
                setHeight(DEFAULT_HEIGHT_EXPANDED + TextMetrics.get().getHeight(msgText.getText()));
                updateAreaSizeToDialog();
            }
        });

        fieldSet.addListener(Events.Collapse, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                layout();
                area.setVisible(false);
                TextMetrics.get().setFixedWidth(getWidth() - 30);
                setHeight(DEFAULT_HEIGHT + TextMetrics.get().getHeight(msgText.getText()));
            }
        });
    }
}
