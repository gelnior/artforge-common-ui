package fr.hd3d.common.ui.client.widget.dialog;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.HorizontalPanel;
import com.extjs.gxt.ui.client.widget.Text;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.TextArea;
import com.extjs.gxt.ui.client.widget.layout.RowLayout;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.error.Hd3dException;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.asset.FileRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.widget.dialog.fileselectordialog.FileSelectorDialog;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;


/**
 * This class allowed to set a task comment during a change of status.
 * 
 * @author HD3D
 * 
 */
public class CommentTaskDialog extends Dialog
{
    /** Constant strings to display : dialog messages, button label... */
    public static final CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);

    /** TextArea that containing the comment for the new approval done created. */
    private final TextArea taskCommentTextArea = new TextArea();

    /** The default text print in the textArea for the comment of the approval */
    private final String defaultTextTaskComment = COMMON_CONSTANTS.TypeYourComment();
    /** Title of dialog to fill the comment of approval. */
    private final String headingTaskCommentDialog = COMMON_CONSTANTS.WorkCommentsSubmission();

    private final ArrayList<Hd3dModelData> tasks = new ArrayList<Hd3dModelData>();

    private final FileSelectorDialog fileSelectorDialog = FileSelectorDialog.getInstance();

    private final AppEvent cancelEvent;
    private final AppEvent okEvent;

    public static final String COMMENT_DATA = "COMMENT";
    public static final String TASKS_DATA = "TASKS";

    private List<FileRevisionModelData> fileRevisions = null;
    private final CommentTaskDialogController controller = new CommentTaskDialogController(this);

    private final Text fileRevisionText = new Text();

    /**
     * Default constructor with event parameter.
     * 
     * @param cancelEvent
     *            the event type send if cancel button is selected.
     * @param okEvent
     *            the event type send if yes button is selected .
     */
    public CommentTaskDialog(EventType cancelEvent, EventType okEvent)
    {
        this(new AppEvent(cancelEvent), new AppEvent(okEvent));
    }

    public CommentTaskDialog(AppEvent cancelEvent, AppEvent okEvent)
    {

        // position and size
        // BorderedPanel panel = new BorderedPanel();
        int width = 400;
        setLayout(new RowLayout());
        setHeight(300);
        setWidth(width);

        // button
        setButtons(Dialog.OKCANCEL);
        setHideOnButtonClick(true);

        // default text
        taskCommentTextArea.setEmptyText(defaultTextTaskComment);
        taskCommentTextArea.setWidth(width);
        taskCommentTextArea.setHeight(190);

        // modal
        setModal(true);

        // heading
        setHeading(headingTaskCommentDialog);
        add(taskCommentTextArea);

        HorizontalPanel panel = new HorizontalPanel();
        panel.setSpacing(10);

        fileRevisionText.setText("No file...");
        Button link = new Button("Link file");
        link.addSelectionListener(new SelectionListener<ButtonEvent>() {

            @Override
            public void componentSelected(ButtonEvent ce)
            {
                try
                {
                    if (tasks.isEmpty() || tasks.size() > 1)
                    {
                        throw new Hd3dException("Only one Task have to selected!!");
                    }
                    Hd3dModelData task = tasks.get(0);

                    fileSelectorDialog.setData(task);
                    fileSelectorDialog.show();
                }
                catch (Hd3dException e)
                {
                    e.print();
                }
            }

        });

        if (MainModel.getAssetManager())
        {
            panel.add(link);
            panel.add(fileRevisionText);
        }

        add(panel);

        fileSelectorDialog.addListener(FileSelectorDialog.FILE_REVISION_SELECTED, new Listener<AppEvent>() {

            public void handleEvent(AppEvent be)
            {
                List<FileRevisionModelData> files = be.getData();
                setFileRevision(files);
            }

        });

        this.cancelEvent = cancelEvent;
        this.okEvent = okEvent;
    }

    @Override
    protected void onButtonPressed(Button button)
    {
        super.onButtonPressed(button);

        if (button == getButtonById(CANCEL))
        {
            EventDispatcher.forwardEvent(cancelEvent);
        }
        else if (button == getButtonById(OK))
        {
            if (CollectionUtils.isNotEmpty(tasks))
            {
                for (Hd3dModelData task : tasks)
                {
                    task.set(TaskModelData.COMMENT_FIELD, taskCommentTextArea.getValue());
                    if (fileRevisions != null && !fileRevisions.isEmpty())
                    {

                        ArrayList<Long> ids = new ArrayList<Long>();
                        for (FileRevisionModelData filerevision : fileRevisions)
                        {
                            ids.add(filerevision.getId());
                        }

                        task.set(TaskModelData.FILE_REVISIONS_FIELD, ids);
                    }
                }
                okEvent.setData(COMMENT_DATA, taskCommentTextArea.getValue());
                okEvent.setData(TASKS_DATA, tasks);
                EventDispatcher.forwardEvent(okEvent);
            }
        }
    }

    /**
     * @return the controller
     */
    public CommentTaskDialogController getController()
    {
        return controller;
    }

    /**
     * Set the task to add comment.
     * 
     * @param task
     *            the task.
     */
    public void setTask(Hd3dModelData task)
    {
        ArrayList<Hd3dModelData> tasks = new ArrayList<Hd3dModelData>();
        tasks.add(task);
        this.setTask(tasks);
    }

    /**
     * Set tasks to add comment.
     * 
     * @param tasks
     *            the list of tasks.
     */
    public void setTask(List<Hd3dModelData> tasks)
    {
        this.tasks.clear();
        this.tasks.addAll(tasks);
    }

    @Override
    public void show()
    {
        reset();
        super.show();
    }

    private void reset()
    {
        this.taskCommentTextArea.clear();
        fileRevisions = null;
        fileRevisionText.setText("No file...");
    }

    public void setFileRevision(List<FileRevisionModelData> fileRevisions)
    {
        if (fileRevisions == null)
        {
            return;
        }
        StringBuilder text = new StringBuilder();
        for (FileRevisionModelData file : fileRevisions)
        {
            text.append(file.getKey()).append(',');
        }
        text.setLength(text.length() - 1);
        this.fileRevisionText.setText(text.toString());
        this.fileRevisions = fileRevisions;
    }

    // public void displayProxyCreator()
    // {
    // if (fileRevisions != null)
    // {
    // ProxyCreatorDisplayer.show(fileRevisions.getId(), new AppEvent(CommentTaskDialogEvent.PROXY_ADDED),
    // "Do you want to add a proxy file on " + fileRevisions.getName() + " ?");
    // }
    // }

}
