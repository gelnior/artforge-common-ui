package fr.hd3d.common.ui.client.widget.dialog.fileselectordialog.cmd;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.mvc.controller.BaseCommand;
import fr.hd3d.common.ui.client.widget.dialog.fileselectordialog.FileSelectorDialog;
import fr.hd3d.common.ui.client.widget.dialog.fileselectordialog.FileSelectorModel;


public class AssetRevisionLoadedCmd implements BaseCommand
{
    private final FileSelectorDialog view;
    private final FileSelectorModel model;

    /**
     * Default constructor.
     * 
     * @param view
     *            the view
     * @param model
     *            the model
     */
    public AssetRevisionLoadedCmd(FileSelectorDialog view, FileSelectorModel model)
    {
        super();
        this.view = view;
        this.model = model;
    }

    public void execute(AppEvent event)
    {
        this.view.unmaskFileToSelect();
        if (!this.model.isValidAsset())
        {
            this.view.noValidAssetMode();
        }
    }
}
