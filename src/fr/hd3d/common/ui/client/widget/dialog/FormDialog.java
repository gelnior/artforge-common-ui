package fr.hd3d.common.ui.client.widget.dialog;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.KeyListener;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.Field;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.layout.FormData;
import com.extjs.gxt.ui.client.widget.layout.FormLayout;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.KeyCodes;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.error.Hd3dException;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.widget.SavingStatus;


/**
 * Utility class to create easily form dialog. It provides preset styles and layout and a listener on OK button. The
 * method onOkClicked can be override to add a specific behavior on validation for the created dialog.
 * 
 * @author HD3D
 */
public class FormDialog extends Dialog
{
    /** Constant strings from common library. */
    public static final CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);

    /** Event to send when button is clicked. */
    protected EventType okEvent;
    /** Panel containing form. */
    protected FormPanel panel = new FormPanel();
    /** Form data used to add field to form panel. */
    protected FormData formData = new FormData("-20");
    /** Saving indicator located inside button bar. */
    protected SavingStatus status = new SavingStatus();

    /** OK button of the dialog. */
    protected Button okButton;
    /** Cancel button of the dialog. */
    protected Button cancelButton;

    /** Utility change listener set on every filter added to the form. */
    protected Listener<BaseEvent> changeListener = new Listener<BaseEvent>() {
        public void handleEvent(BaseEvent be)
        {
            onFieldChanged();
        }
    };
    /**
     * Utility change listener set on every filter added to the form. It completes the first change listener, because it
     * is inefficient for text field.
     */
    protected KeyListener changeKeyListener = new KeyListener() {
        @Override
        public void componentKeyUp(ComponentEvent event)
        {
            onFieldChanged();
        }
    };
    /** Utility listener to set on fields to activate ok button when enter key is up. */
    protected KeyListener enterListener = new KeyListener() {
        @Override
        public void componentKeyUp(ComponentEvent event)
        {
            if (event.getKeyCode() == KeyCodes.KEY_ENTER)
            {
                onButtonPressed(getButtonById(OK));
            }
        }
    };

    /**
     * Default constructor.
     * 
     * @param okEvent
     *            Event to send when button is clicked.
     * @param header
     *            Text displayed as header.
     */
    public FormDialog(final EventType okEvent, String header)
    {
        this.okEvent = okEvent;

        this.setLayout(new FormLayout());
        this.panel.setBorders(false);
        this.panel.setHeaderVisible(false);
        this.panel.setBodyBorder(false);

        this.add(panel);
        this.setHeading(header);
        this.setHideOnButtonClick(true);
        this.setShadow(false);

        this.setButtons(Dialog.OKCANCEL);
        this.okButton = (Button) this.getButtonBar().getItemByItemId(Dialog.OK);
        this.cancelButton = (Button) this.getButtonBar().getItemByItemId(Dialog.CANCEL);
        this.getButtonBar().insert(status, 0);
    }

    /**
     * When form panel is shown, the focus is set on panel.
     */
    @Override
    public void show()
    {
        super.show();
        this.setFocusWidget(panel);
    }

    /** Show saving indicator. */
    public void showSaving()
    {
        status.show();
    }

    /** Hide saving indicator. */
    public void hideSaving()
    {
        status.hide();
    }

    /**
     * When OK button is clicked, this method is called.
     * 
     * @throws Hd3dException
     */
    protected void onOkClicked() throws Hd3dException
    {
        AppEvent event = new AppEvent(okEvent);
        EventDispatcher.forwardEvent(event);
    }

    /**
     * When CANCEL button is clicked, this method is called.
     */
    protected void onCancelClicked()
    {}

    /**
     * When a registered field changes, this method is called.
     */
    protected void onFieldChanged()
    {}

    public void addField(Field<?> field)
    {
        this.panel.add(field);
        field.addListener(Events.Change, changeListener);
    }

    public void addField(Field<?> field, FormData data)
    {
        this.panel.add(field, data);
        field.addListener(Events.Change, changeListener);
    }

    public void addKeyField(TextField<?> field)
    {
        this.panel.add(field);
        field.addKeyListener(changeKeyListener);
    }

    public void addKeyField(TextField<?> field, FormData data)
    {
        this.panel.add(field, data);
        field.addKeyListener(changeKeyListener);
    }

    @Override
    protected void onButtonPressed(Button button)
    {
        if (button == null)
        {
            return;
        }
        try
        {
            if (button.getItemId().equals(OK))
            {
                onOkClicked();
            }
            else if (button.getItemId().equals(CANCEL))
            {
                onCancelClicked();
            }
            super.onButtonPressed(button);
        }
        catch (Hd3dException e)
        {
            e.print();
        }

    }

    public void setOkEvent(EventType okEvent)
    {
        this.okEvent = okEvent;
    }
}
