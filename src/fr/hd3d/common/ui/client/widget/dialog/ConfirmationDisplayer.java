package fr.hd3d.common.ui.client.widget.dialog;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.MessageBox;

import fr.hd3d.common.ui.client.event.EventDispatcher;


/**
 * Displays a confirmation dialog with text and event to forward if yes button is clicked.
 * 
 * @author HD3D
 */
public class ConfirmationDisplayer
{
    /** Displayed confirmation box. */
    private static MessageBox confirmationBox;
    /** Event sent when yes button is clicked. */
    private static AppEvent event;

    /**
     * Display a confirmation dialog that forwards <i>event</i> when yes button is clicked.
     * 
     * @param title
     *            Dialog box title.
     * @param message
     *            Message to display.
     * @param eventType
     *            Event to forward when yes button is clicked.
     */
    public static void display(String title, String message, final EventType eventType)
    {
        display(title, message, new AppEvent(eventType));
    }

    /**
     * Display a confirmation dialog that forwards <i>mvcEvent</i> when yes button is clicked.
     * 
     * @param title
     *            Dialog box title.
     * @param message
     *            Message to display.
     * @param mvcEvent
     *            Event to forward when yes button is clicked.
     */
    public static void display(String title, String message, AppEvent mvcEvent)
    {
        event = mvcEvent;

        confirmationBox = null;
        getConfirmationBox().setTitle(title);
        getConfirmationBox().setMessage("");
        getConfirmationBox().setMessage(message);
        getConfirmationBox().show();
    }

    /**
     * @return event to forward if yes button is clicked.
     */
    protected static AppEvent getEvent()
    {
        return event;
    }

    /**
     * @return Confirmation dialog box to display. Set icon, buttons and callback that will forwards event if yes button
     *         is clicked.
     */
    protected static MessageBox getConfirmationBox()
    {
        if (confirmationBox == null)
        {
            confirmationBox = new MessageBox();
            confirmationBox.setIcon(MessageBox.WARNING);
            confirmationBox.setButtons(MessageBox.YESNO);

            confirmationBox.addCallback(new Listener<MessageBoxEvent>() {
                public void handleEvent(MessageBoxEvent be)
                {
                    if (be.getButtonClicked().getItemId() == Dialog.YES)
                        EventDispatcher.forwardEvent(ConfirmationDisplayer.getEvent());
                }
            });

        }

        return confirmationBox;
    }

}
