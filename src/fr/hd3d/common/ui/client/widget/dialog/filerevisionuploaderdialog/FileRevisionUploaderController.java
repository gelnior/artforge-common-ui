package fr.hd3d.common.ui.client.widget.dialog.filerevisionuploaderdialog;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.mvc.controller.CommandController;
import fr.hd3d.common.ui.client.widget.dialog.filerevisionuploaderdialog.cmd.AllSubmittedCmd;
import fr.hd3d.common.ui.client.widget.dialog.filerevisionuploaderdialog.cmd.FileSubmittedCmd;
import fr.hd3d.common.ui.client.widget.dialog.filerevisionuploaderdialog.cmd.ProxySubmittedCmd;


public class FileRevisionUploaderController extends CommandController
{
    private final FileRevisionUploaderDialog view;
    private final FileRevisionUploaderModel model;

    public FileRevisionUploaderController(FileRevisionUploaderDialog view, FileRevisionUploaderModel model)
    {
        super();
        this.view = view;
        this.model = model;
        EventDispatcher.get().addController(this);
        this.registerEvents();
    }

    private void registerEvents()
    {
        commandMap.put(FileRevisionUploaderEvent.FILE_SUBMITTED, new FileSubmittedCmd(view, model));
        commandMap.put(FileRevisionUploaderEvent.PROXY_FILE_SUBMITTED, new ProxySubmittedCmd(view, model));
        commandMap.put(FileRevisionUploaderEvent.ALL_FILE_SUBMITTED, new AllSubmittedCmd(view, model));
    }

}
