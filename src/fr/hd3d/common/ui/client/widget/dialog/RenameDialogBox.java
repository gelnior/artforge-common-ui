package fr.hd3d.common.ui.client.widget.dialog;

import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.util.Rectangle;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.google.gwt.event.dom.client.KeyCodes;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;


public class RenameDialogBox extends MessageBox
{
    private static final String DEFAULT_MESSAGE = "New value";
    private static final String DEFAULT_TITLE = "Rename";
    private String property = null;
    private Hd3dModelData data = null;
    private EventType event = null;

    public enum DialogPosition
    {
        TOP_LEFT_CORNER, TOP_RIGHT_CORNER, BOTTOM_LEFT_CORNER, BOTTOM_RIGTH_CORNER
    }

    public RenameDialogBox(String propertyToChange, Hd3dModelData modelData, String title, String message)
    {
        super();
        this.property = propertyToChange;
        this.data = modelData;
        this.setTitle(title);
        this.setMessage(message);
        this.setType(MessageBoxType.PROMPT);
        this.setButtons(Dialog.OKCANCEL);
        this.setModal(false);
        this.addCallback(new Listener<MessageBoxEvent>() {
            public void handleEvent(MessageBoxEvent be)
            {
                if (be.getButtonClicked() != null)
                {
                    if (Dialog.OK.equalsIgnoreCase(be.getButtonClicked().getText()))
                    {
                        changeProperty(be.getValue());
                    }
                }
            }
        });
        this.show();
        this.getTextBox().addListener(Events.KeyPress, new Listener<ComponentEvent>() {
            public void handleEvent(ComponentEvent be)
            {
                if (be.getKeyCode() == KeyCodes.KEY_ENTER)
                {
                    changeProperty(getTextBox().getValue());
                    close();
                }
            }
        });
        if (propertyToChange != null && modelData != null)
        {
            this.getTextBox().setValue(modelData.get(propertyToChange).toString());
        }
    }

    public RenameDialogBox(String propertyToChange, Hd3dModelData modelData)
    {
        this(propertyToChange, modelData, DEFAULT_TITLE, DEFAULT_MESSAGE);
    }

    public RenameDialogBox(String propertyToChange, Hd3dModelData modelData, String title, String message,
            EventType eventToDispatchOnOk)
    {
        this(propertyToChange, modelData, title, message);
        this.event = eventToDispatchOnOk;
    }

    public RenameDialogBox(String propertyToChange, Hd3dModelData modelData, EventType eventToDispatchOnOk)
    {
        this(propertyToChange, modelData);
        this.event = eventToDispatchOnOk;
    }

    /**
     * @param rectangle
     *            the widget bounds to move the dialog
     * @param position
     *            the dialog's position based on widget
     * @param offsetTop
     *            the offset between top dialog and widget
     * @param offsetLeft
     *            the offset between left dialog and widget
     * 
     */
    public void move(Rectangle rectangle, DialogPosition position, int offsetTop, int offsetLeft)
    {
        if (DialogPosition.BOTTOM_LEFT_CORNER.equals(position))
        {
            this.getDialog().el().setLeftTop(rectangle.x + offsetLeft, rectangle.y + rectangle.height + offsetTop);
        }
        else if (DialogPosition.BOTTOM_RIGTH_CORNER.equals(position))
        {
            this.getDialog().el().setLeftTop(rectangle.x + rectangle.width + offsetLeft,
                    rectangle.y + rectangle.height + offsetTop);
        }
        else if (DialogPosition.TOP_LEFT_CORNER.equals(position))
        {
            this.getDialog().el().setLeftTop(rectangle.x + offsetLeft, rectangle.y + offsetTop);
        }
        else if (DialogPosition.TOP_RIGHT_CORNER.equals(position))
        {
            this.getDialog().el().setLeftTop(rectangle.x + rectangle.width + offsetLeft, rectangle.y + offsetTop);
        }
    }

    private void changeProperty(String value)
    {
        if (property != null && data != null)
        {
            Object oldProperty = data.get(property);
            if (!oldProperty.equals(value))
            {
                data.set(property, value);
                if (event != null)
                {
                    AppEvent appEvent = new AppEvent(event, data);
                    EventDispatcher.forwardEvent(appEvent);
                }
            }
        }
    }
}
