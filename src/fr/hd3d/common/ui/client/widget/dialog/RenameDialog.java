package fr.hd3d.common.ui.client.widget.dialog;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.widget.SavingStatus;


/**
 * Rename dialog is useful to rename any kind of service object which has a name. The dialog just displays a name field
 * to let user enter the new name.
 * 
 * @author HD3D
 */
public class RenameDialog extends NameDialog
{
    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    /** The instance to rename. */
    protected RecordModelData record;
    /** The status showing that data are saving. */
    protected SavingStatus status;

    /** Event type to forward when record is renamed. */
    protected EventType eventType = CommonEvents.RECORD_RENAMED;

    /**
     * Default constructor, set styles and title.
     * 
     * @param title
     *            The title to set as dialog title.
     */
    public RenameDialog(String title)
    {
        super(null, title);

        this.status = new SavingStatus();
        this.getButtonBar().insert(status, 0);
        this.status.hide();
    }

    /**
     * Set event type forwarded when record is forwarded.
     * 
     * @param eventType
     *            The event type to set.
     */
    public void setEventType(EventType eventType)
    {
        this.eventType = eventType;
    }

    /**
     * When dialog is shown, name field is reset and focus is put on it. Instance associated to the dialog is changed by
     * <i>record</i>.
     * 
     * @param record
     */
    public void show(RecordModelData record)
    {
        this.record = record;
        this.clearNameField();
        this.nameTextField.focus();
        super.show();
        String name = record.getName();
        this.nameTextField.setValue(name);
    }

    /**
     * When save is a success saving indicator is hidden and the dialog is hiddent too.
     */
    protected void onSaveSuccess()
    {
        this.status.hide();
        this.hide();

        AppEvent event = new AppEvent(eventType, this.record);
        EventDispatcher.forwardEvent(event);
    }

    /**
     * When an error occurs, it hides the saving indicator.
     */
    protected void onSaveError()
    {
        this.status.hide();
    }

    /**
     * When OK button is clicked, the record new name is saved to services. A saving indicator is displayed until the
     * saving is finished or returns an error.
     */
    @Override
    protected void onOkClicked()
    {
        if (record != null && this.nameTextField.getValue() != null && this.nameTextField.getValue().length() > 0)
        {
            this.status.show();

            record.setName(nameTextField.getValue());

            if (FieldUtils.isWorkObject(record))
                record.set(ConstituentModelData.CONSTITUENT_LABEL, nameTextField.getValue());

            record.save(null, new BaseCallback() {
                @Override
                protected void onSuccess(Request request, Response response)
                {
                    onSaveSuccess();
                }

                @Override
                protected void onError()
                {
                    onSaveError();
                }
            });
        }
    }
}
