package fr.hd3d.common.ui.client.widget.dialog.fileselectordialog;

import java.util.List;

import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.event.LoadListener;
import com.extjs.gxt.ui.client.store.ListStore;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.asset.AssetRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.asset.FileRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.reader.AssetRevisionReader;
import fr.hd3d.common.ui.client.modeldata.reader.FileRevisionReader;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.Constraints;
import fr.hd3d.common.ui.client.service.store.ServiceStore;


/**
 * Model to manage the asset revision and file revisions.
 * 
 * @author HD3D
 * 
 */
public class FileSelectorModel implements IFileSelectorModel
{

    /** Constraint on the task type of asset revision. */
    private final Constraint taskTypeConstraintForFile = new Constraint(EConstraintOperator.eq,
            "assetRevision.taskType.id");
    /** Constraint on the work object of asset revision groups. */
    private final Constraint workObjectConstraintForFile = new Constraint(EConstraintOperator.eq,
            "assetRevision.assetRevisionGroups.value");
    /** Constraint on the bound entity name of asset revision groups. */
    private final Constraint boundEntityNameConstraintForFile = new Constraint(EConstraintOperator.eq,
            "assetRevision.assetRevisionGroups.criteria");

    /** Constraint on the task type of asset revision. */
    private final Constraint taskTypeConstraintForAsset = new Constraint(EConstraintOperator.eq, "taskType.id");
    /** Constraint on the work object of asset revision groups. */
    private final Constraint workObjectConstraintForAsset = new Constraint(EConstraintOperator.eq,
            "assetRevisionGroups.value");
    /** Constraint on the bound entity name of asset revision groups. */
    private final Constraint boundEntityNameConstraintForAsset = new Constraint(EConstraintOperator.eq,
            "assetRevisionGroups.criteria");

    /** Work object id. */
    private Long workObjectId;
    /** Task type id. */
    private Long taskTypeId;

    /** Bound entity name. */
    private String boundEntityName;

    /** List of constraint on the file revision store. */
    private final Constraints filesConstraints = new Constraints();
    /** List of constraint on the asset revision store. */
    private final Constraints assetConstraints = new Constraints();

    /** File revision store to get file revisions concerning the work object and task type. */
    private final ServiceStore<FileRevisionModelData> fileRevisionStore = new ServiceStore<FileRevisionModelData>(
            new FileRevisionReader());

    /** File revision store to get file revisions concerning the work object and task type. */
    private final ServiceStore<AssetRevisionModelData> assetStore = new ServiceStore<AssetRevisionModelData>(
            new AssetRevisionReader());

    /** File revision selected store. */
    private final ListStore<FileRevisionModelData> fileRevisionSelected = new ListStore<FileRevisionModelData>();

    /** Asset revivion id. */
    private long assetId;

    /**
     * Default constructor.
     */
    public FileSelectorModel()
    {
        fileRevisionStore.setPath(ServicesURI.MY_FILEREVISIONS);
        fileRevisionStore.addParameter(filesConstraints);
        fileRevisionStore.addEventLoadListener(FileSelectorEvent.FILE_REVISION_LOADED);
        fileRevisionStore.addLoadListener(new LoadListener() {
            @Override
            public void loaderLoad(LoadEvent le)
            {
                List<FileRevisionModelData> files = fileRevisionStore.getModels();
                if (files != null && !files.isEmpty())
                {
                    assetId = files.get(0).getAssetRevision();
                }
            }
        });
        assetStore.setPath(ServicesURI.MY_ASSETREVISIONS);
        assetStore.addParameter(assetConstraints);
        assetStore.addEventLoadListener(FileSelectorEvent.ASSET_REVISION_LOADED);
        assetStore.addLoadListener(new LoadListener() {
            @Override
            public void loaderLoad(LoadEvent le)
            {
                List<AssetRevisionModelData> assets = assetStore.getModels();
                if (assetStore != null && !assets.isEmpty())
                {
                    assetId = assets.get(0).getId();
                }
            }
        });
    }

    /**
     * Set data to configure the model.
     * 
     * @param task
     *            the task.
     */
    public void setData(Hd3dModelData task)
    {
        taskTypeId = task.get(TaskModelData.TASK_TYPE_ID_FIELD);
        boundEntityName = task.get(TaskModelData.BOUND_ENTITY_NAME_FIELD);
        workObjectId = task.get(TaskModelData.WORK_OBJECT_ID_FIELD);

        filesConstraints.clear();

        workObjectConstraintForFile.setLeftMember(workObjectId.toString());
        filesConstraints.add(workObjectConstraintForFile);

        if (taskTypeId != null)
        {
            taskTypeConstraintForFile.setLeftMember(taskTypeId);
            filesConstraints.add(taskTypeConstraintForFile);
        }

        boundEntityNameConstraintForFile.setLeftMember(boundEntityName);
        filesConstraints.add(boundEntityNameConstraintForFile);

    }

    /**
     * Return the file store.
     * 
     * @return the file store.
     */
    public ServiceStore<FileRevisionModelData> getFileStore()
    {
        return fileRevisionStore;
    }

    /**
     * Reload the file store.
     */
    public void reload()
    {
        fileRevisionStore.reload();
    }

    /**
     * Reset the model.
     */
    public void reset()
    {
        // obliger de faire sa sinon removeAll supprimer les listeners
        for (int i = 0; i < this.fileRevisionSelected.getCount(); i++)
        {
            this.fileRevisionSelected.remove(i);
        }
    }

    /**
     * Add the selected file revivision.
     * 
     * @param fileRevision
     *            the selected file revision.
     */
    public void addFileRevision(FileRevisionModelData fileRevision)
    {
        if (this.fileRevisionSelected.contains(fileRevision))
        {
            return;
        }
        this.fileRevisionSelected.add(fileRevision);
    }

    /**
     * Remove the selected file revision.
     * 
     * @param fileRevision
     *            the selected file revision.
     */
    public void removeFileRevision(FileRevisionModelData fileRevision)
    {
        this.fileRevisionSelected.remove(fileRevision);
    }

    /**
     * Return the selected file revisions.
     * 
     * @return the selected file revisions.
     */
    public ListStore<FileRevisionModelData> getFileRevisionSelected()
    {
        return this.fileRevisionSelected;
    }

    /**
     * Return the asset revision.
     * 
     * @return the asset revision id.
     */
    public Long getAssetRevision()
    {
        return assetId;
    }

    public boolean isValidAsset()
    {
        if (assetStore.getCount() > 0)
        {
            if (assetStore.getAt(0).getWipPath() != null && !assetStore.getAt(0).getWipPath().equals(""))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Load the asset revision.
     */
    public void loadAssetRevision()
    {
        assetConstraints.clear();

        workObjectConstraintForAsset.setLeftMember(workObjectId.toString());
        assetConstraints.add(workObjectConstraintForAsset);

        if (taskTypeId != null)
        {
            taskTypeConstraintForAsset.setLeftMember(taskTypeId);
            assetConstraints.add(taskTypeConstraintForAsset);
        }

        boundEntityNameConstraintForAsset.setLeftMember(boundEntityName);
        assetConstraints.add(boundEntityNameConstraintForAsset);
        this.assetStore.reload();
    }
}
