package fr.hd3d.common.ui.client.widget;

import com.extjs.gxt.ui.client.Style.LayoutRegion;
import com.extjs.gxt.ui.client.util.Margins;
import com.extjs.gxt.ui.client.widget.Component;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.layout.BorderLayout;
import com.extjs.gxt.ui.client.widget.layout.BorderLayoutData;
import com.extjs.gxt.ui.client.widget.toolbar.ToolBar;
import com.google.gwt.user.client.ui.Widget;


/**
 * BorderedPanel is class to facilitate widget layout. It is a simply panel with a border layout as default layout. It
 * aims to easily put widget at east, center, north, south or west. It also integrates a toolbar. It is highly
 * recommended to use bordered panel as base panel for any HD3D application. Of course bordered panel can also be used
 * inside widgets.
 * 
 * @author HD3D
 */
public class BorderedPanel extends ContentPanel
{
    /** Margin between widgets added to bordered panel. */
    public static final int MARGIN = 5;
    /** NO_MARGIN is equal to zero. */
    public static final int NO_MARGIN = 0;

    /** Panel toolbar. */
    protected final ToolBar toolBar = new ToolBar();

    /**
     * Default constructor : remove header, borders, set border layout.
     */
    public BorderedPanel()
    {
        this.setHeaderVisible(false);
        this.setBorders(false);
        this.setBodyBorder(false);
        this.setLayout(new BorderLayout());
    }

    /**
     * @return Panel toolbar.
     */
    public ToolBar getToolBar()
    {
        return toolBar;
    }

    /** Set toolbar styles, and set it at panel top component. */
    public void setToolBar()
    {
        this.toolBar.setStyleAttribute("padding", "5px");
        this.setTopComponent(toolBar);
    }

    /**
     * Add <i>component</i> to panel toolbar. SetToolbar method must be called before adding components.
     * 
     * @param component
     *            The component to add.
     */
    public void addToToolBar(Component component)
    {
        this.toolBar.add(component);
    }

    public BorderLayoutData addCenter(Component component)
    {
        return this.addCenter(component, MARGIN, MARGIN, MARGIN, MARGIN);
    }

    public BorderLayoutData addCenter(Widget widget)
    {
        return this.addCenter(widget, MARGIN, MARGIN, MARGIN, MARGIN);
    }

    public BorderLayoutData addCenter(Component component, int height)
    {
        return this.addCenter(component, height, MARGIN, MARGIN, MARGIN, MARGIN);
    }

    public BorderLayoutData addCenter(Component component, int top, int left, int bottom, int right)
    {
        return this.addToRegion(component, LayoutRegion.CENTER, top, left, bottom, right);
    }

    public BorderLayoutData addCenter(Widget widget, int top, int left, int bottom, int right)
    {
        return this.addToRegion(widget, LayoutRegion.CENTER, top, left, bottom, right);
    }

    public BorderLayoutData addCenter(Component component, int size, int top, int left, int bottom, int right)
    {
        return this.addToRegion(component, LayoutRegion.CENTER, size, top, left, bottom, right);
    }

    public BorderLayoutData addNorth(Component component)
    {
        return this.addNorth(component, MARGIN, MARGIN, NO_MARGIN, MARGIN);
    }

    public BorderLayoutData addNorth(Component component, int height)
    {
        return this.addNorth(component, height, MARGIN, MARGIN, NO_MARGIN, MARGIN);
    }

    public BorderLayoutData addNorth(Component component, int top, int left, int bottom, int right)
    {
        return this.addToRegion(component, LayoutRegion.NORTH, top, left, bottom, right);
    }

    public BorderLayoutData addNorth(Component component, int height, int top, int left, int bottom, int right)
    {
        return this.addToRegion(component, LayoutRegion.NORTH, height, top, left, bottom, right);
    }

    public BorderLayoutData addEast(Component component)
    {
        return this.addEast(component, MARGIN, MARGIN, MARGIN, NO_MARGIN);
    }

    public BorderLayoutData addEast(Component component, int height)
    {
        return this.addEast(component, height, MARGIN, MARGIN, MARGIN, NO_MARGIN);
    }

    public BorderLayoutData addEast(Component component, int top, int left, int bottom, int right)
    {
        return this.addToRegion(component, LayoutRegion.EAST, top, left, bottom, right);
    }

    public BorderLayoutData addEast(Component component, int height, int top, int left, int bottom, int right)
    {
        return this.addToRegion(component, LayoutRegion.EAST, height, top, left, bottom, right);
    }

    public BorderLayoutData addSouth(Component component)
    {
        return this.addSouth(component, NO_MARGIN, MARGIN, MARGIN, MARGIN);
    }

    public BorderLayoutData addSouth(Component component, int height)
    {
        return this.addSouth(component, height, NO_MARGIN, MARGIN, MARGIN, MARGIN);
    }

    public BorderLayoutData addSouth(Component component, int top, int left, int bottom, int right)
    {
        return this.addToRegion(component, LayoutRegion.SOUTH, top, left, bottom, right);
    }

    public BorderLayoutData addSouth(Component component, int height, int top, int left, int bottom, int right)
    {
        return this.addToRegion(component, LayoutRegion.SOUTH, height, top, left, bottom, right);
    }

    public BorderLayoutData addWest(Component component)
    {
        return this.addWest(component, MARGIN, NO_MARGIN, MARGIN, MARGIN);
    }

    public BorderLayoutData addWest(Component component, int height)
    {
        return this.addWest(component, height, MARGIN, NO_MARGIN, MARGIN, MARGIN);
    }

    public BorderLayoutData addWest(Component component, int top, int left, int bottom, int right)
    {
        return this.addToRegion(component, LayoutRegion.WEST, top, left, bottom, right);
    }

    public BorderLayoutData addWest(Component component, int height, int top, int left, int bottom, int right)
    {
        return this.addToRegion(component, LayoutRegion.WEST, height, top, left, bottom, right);
    }

    protected BorderLayoutData addToRegion(Component component, LayoutRegion region, int top, int left, int bottom,
            int right)
    {
        BorderLayoutData layoutData = new BorderLayoutData(region);
        layoutData.setMargins(new Margins(top, left, bottom, right));

        this.add(component, layoutData);

        return layoutData;
    }

    protected BorderLayoutData addToRegion(Widget widget, LayoutRegion region, int top, int left, int bottom, int right)
    {
        BorderLayoutData layoutData = new BorderLayoutData(region);
        layoutData.setMargins(new Margins(top, left, bottom, right));

        this.add(widget, layoutData);

        return layoutData;
    }

    protected BorderLayoutData addToRegion(Component component, LayoutRegion region, int size, int top, int left,
            int bottom, int right)
    {
        BorderLayoutData layoutData = new BorderLayoutData(region);
        layoutData.setMargins(new Margins(top, left, bottom, right));
        layoutData.setSize(size);
        layoutData.setSplit(true);
        layoutData.setMaxSize(1000);

        this.add(component, layoutData);

        return layoutData;
    }
}
