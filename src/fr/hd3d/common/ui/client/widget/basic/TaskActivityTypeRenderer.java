package fr.hd3d.common.ui.client.widget.basic;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.util.Util;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;

import fr.hd3d.common.ui.client.modeldata.task.TaskActivityModelData;


/**
 * Displays activity task type name in a cell of which background has the same color as displayed task type.
 * 
 * @author HD3D
 */
public class TaskActivityTypeRenderer implements GridCellRenderer<TaskActivityModelData>
{
    /**
     * Displays activity task type name in a cell of which background has the same color as displayed task type.
     */
    public Object render(TaskActivityModelData model, String property, ColumnData config, int rowIndex, int colIndex,
            ListStore<TaskActivityModelData> store, Grid<TaskActivityModelData> grid)
    {
        String type = model.get(property);
        String color = model.getTaskTypeColor();

        return getTypeRendered(type, color);
    }

    /**
     * @param type
     *            Task type name
     * @param color
     *            Task type color
     * @return HTML DIV code that displays activity task type name in a cell of which background has the same color as
     *         displayed task type;
     */
    private String getTypeRendered(String type, String color)
    {
        String cellCode = "";
        if (!Util.isEmptyString(type))
        {
            cellCode = type;
            if (!Util.isEmptyString(color))
            {
                cellCode = "<div style='padding: 3px; font-weight: bold; text-align:center; background-color:" + color
                        + ";'>" + type + "</div>";
            }
        }
        else
        {
            cellCode += "unknow type,";
        }
        return cellCode;
    }

}
