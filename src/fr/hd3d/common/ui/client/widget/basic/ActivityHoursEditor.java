package fr.hd3d.common.ui.client.widget.basic;

import fr.hd3d.common.ui.client.calendar.DatetimeUtil;
import fr.hd3d.common.ui.client.modeldata.FieldModelData;


/**
 * Simple combobox proposing hours selection (from 1h to 12h by default).
 * 
 * @author HD3D
 */
public class ActivityHoursEditor extends HoursEditor
{

    public ActivityHoursEditor(int maxHour)
    {
        super(maxHour);
    }

    /**
     * Fill Combo box with quarter hours from 0 to <i>maxHour</i> like this : 0h, 0h15, 0h30, 0h45, 1h, 1h15, 1h30,
     * 1h45, 2h...
     */
    @Override
    protected void fillCombBox()
    {
        int i = 0;
        int j = 0;
        int hours = 0;
        String label = "";

        int hourIndex = 2;
        for (int length = 0; length <= maxHour * DatetimeUtil.HOUR_SECONDS; length += DatetimeUtil.QUARTER_HOUR_SECONDS)
        {
            if (i == 0)
                label = hours + "h";
            else if (i == 1)
                label = hours + "h 15";
            else if (i == 2)
                label = hours + "h 30";
            else
            {
                label = hours + "h 45";
                hours++;
            }

            if (i == 0)
            {
                FieldModelData field = null;
                if (hours == 4)
                {
                    field = this.addField(0, label, new Long(length));
                    field.set("displayClass", "full-hour big-text");
                }
                else if (hours == 8)
                {
                    field = this.addField(1, label, new Long(length));
                    field.set("displayClass", "full-hour big-text");
                }
                else
                {
                    field = this.addField(hourIndex++, label, new Long(length));
                    field.set("displayClass", "full-hour");
                }
            }
            else
            {
                this.addField(maxHour + j, label, new Long(length));
            }
            i = (i + 1) % 4;
            j++;
        }
    }
}
