package fr.hd3d.common.ui.client.widget.basic;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.TaskStatusRenderer;


public class BasicTaskStatusRenderer<M extends Hd3dModelData> extends TaskStatusRenderer<M>
{

    @Override
    public Object render(M model, String property, ColumnData config, int rowIndex, int colIndex, ListStore<M> store,
            Grid<M> grid)
    {
        config.css += " basic-editable";

        return super.render(model, property, config, rowIndex, colIndex, store, grid);
    }

}
