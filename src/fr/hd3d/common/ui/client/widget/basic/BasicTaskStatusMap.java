package fr.hd3d.common.ui.client.widget.basic;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.core.FastMap;

import fr.hd3d.common.client.enums.EApprovalNoteStatus;
import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.util.FieldUtils;


/**
 * Task status basic user is allowed to set.
 * 
 * @author HD3D
 */
public class BasicTaskStatusMap extends FastMap<FieldModelData>
{
    private static final long serialVersionUID = -6051502761188822001L;

    public static List<EApprovalNoteStatus> getAvailableStatus()
    {
        List<EApprovalNoteStatus> statuses = new ArrayList<EApprovalNoteStatus>();
        statuses.add(EApprovalNoteStatus.WAIT_APP);
        statuses.add(EApprovalNoteStatus.STAND_BY);
        statuses.add(EApprovalNoteStatus.WORK_IN_PROGRESS);
        statuses.add(EApprovalNoteStatus.TO_DO);

        return statuses;
    }

    /**
     * Default constructor.
     */
    public BasicTaskStatusMap()
    {
        super();
        FieldUtils.addFieldToMap(this, 0, "Waiting for approval", ETaskStatus.WAIT_APP.toString());
        FieldUtils.addFieldToMap(this, 1, "Stand By", ETaskStatus.STAND_BY.toString());
        FieldUtils.addFieldToMap(this, 2, "Work in Progress", ETaskStatus.WORK_IN_PROGRESS.toString());
    }
}
