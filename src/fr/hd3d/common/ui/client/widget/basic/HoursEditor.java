package fr.hd3d.common.ui.client.widget.basic;

import java.util.Date;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.util.DateWrapper;

import fr.hd3d.common.ui.client.calendar.DatetimeUtil;
import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.widget.FieldComboBox;


/**
 * Simple combobox proposing hours selection (from 1h to 12h by default).
 * 
 * @author HD3D
 */
public class HoursEditor extends FieldComboBox
{
    /** Top limit of displayed hours. */
    public int maxHour = 12;

    /**
     * Default constructor : add one field for each quarter hour between 0 and 12h.
     * 
     * @param maxHour
     *            Maximum number of hours displayed.
     */
    public HoursEditor(int maxHour)
    {
        super();

        this.maxHour = maxHour;

        this.setTemplate(this.getBasicTemplate());
        this.setItemSelector("div.search-item");
        this.fillCombBox();
    }

    /**
     * Set listener on expand that automatically select value located at <i>defaultValueIndex</i> when combo box is
     * expanded and current value is null.
     * 
     * @param defaultValueIndex
     *            The value to set by default on field, if its current value is null.
     */
    public void setAutoSelection(final int defaultValueIndex)
    {
        this.addListener(Events.Expand, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                if (getValue() == null)
                {
                    FieldModelData defaultValue = getStore().getAt(defaultValueIndex);
                    setValue(defaultValue);
                }
            }
        });
    }

    /**
     * Select hour corresponding to date. It uses date time fields to select right value in combo box.
     * 
     * @param date
     *            The date of which hours and minutes are used to select value.
     */
    public void setValueByDate(Date date)
    {
        Long time = 0L;
        DateWrapper wrapper = new DateWrapper(date);
        time += wrapper.getHours() * DatetimeUtil.HOUR_SECONDS;
        time += wrapper.getMinutes() * DatetimeUtil.MINUTE_SECONDS;

        FieldModelData field = this.getStore().findModel(FieldModelData.VALUE_FIELD, time);
        this.setValue(field);
        this.select(field);
    }

    /**
     * Fill Combo box with quarter hours from 0 to <i>maxHour</i> like this : 0h, 0h15, 0h30, 0h45, 1h, 1h15, 1h30,
     * 1h45, 2h...
     */
    protected void fillCombBox()
    {
        int i = 0;
        int j = 0;
        int hours = 0;
        String label = "";

        for (int length = 0; length <= maxHour * DatetimeUtil.HOUR_SECONDS; length += DatetimeUtil.QUARTER_HOUR_SECONDS)
        {
            if (i == 0)
                label = hours + "h";
            else if (i == 1)
                label = hours + "h 15";
            else if (i == 2)
                label = hours + "h 30";
            else
            {
                label = hours + "h 45";
                hours++;
            }
            FieldModelData field = this.addField(j, label, new Long(length));
            if (i == 0)
                field.set("displayClass", "full-hour");
            i = (i + 1) % 4;
            j++;
        }
    }

    /**
     * @return Template used to display hour in combo box.
     */
    private native String getBasicTemplate() /*-{
                                             return [ 
                                             '<tpl for="."><div class="{displayClass} search-item hour-field">', 
                                             '{name}', 
                                             '</div></tpl>' 
                                             ].join("");
                                             }-*/;

}
