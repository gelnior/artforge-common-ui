package fr.hd3d.common.ui.client.widget;

import java.util.List;

import fr.hd3d.common.client.enums.EApprovalNoteStatus;
import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskStatusMap;


/**
 * Combo box displaying status available for a note. It also provides static map for centralizing label displayed for
 * status.
 * 
 * @author HD3D
 */
public class MultiNoteStatusComboBox extends MultiFieldComboBox
{
    /**
     * Default constructor : fill combo box for all available status.
     */
    public MultiNoteStatusComboBox()
    {
        this(true);
    }

    /**
     * Create status combo for a given list of statuses.
     * 
     * @param statuses
     *            Status to set inside combo box.
     */
    public MultiNoteStatusComboBox(List<EApprovalNoteStatus> statuses)
    {
        super();
        this.setMinListWidth(150);
        int i = 0;
        for (EApprovalNoteStatus status : statuses)
        {
            this.addField(i, status);
            i++;
        }
    }

    /**
     * Constructor that fills combobox with all available status if <i>isFilterComboBox</i> is set to true. Else it
     * removes New status.
     * 
     * @param isFilterComboBox
     *            The combo box on which filter should be set.
     */
    public MultiNoteStatusComboBox(boolean isFilterComboBox)
    {
        super();
        this.setMinListWidth(150);
        this.getListView().setTemplate(getXTemplate());
        this.initList();
        this.setEditable(false);

        this.addField(1, EApprovalNoteStatus.OK);
        this.addField(2, EApprovalNoteStatus.RETAKE);
        this.addField(3, EApprovalNoteStatus.TO_DO);
        this.addField(4, EApprovalNoteStatus.STAND_BY);
        this.addField(5, EApprovalNoteStatus.CANCELLED);
        this.addField(6, EApprovalNoteStatus.WORK_IN_PROGRESS);
        this.addField(7, EApprovalNoteStatus.WAIT_APP);
        this.addField(8, EApprovalNoteStatus.CLOSE);
        if (isFilterComboBox)
        {
            this.addField(9, EApprovalNoteStatus.NEW);
        }
    }

    /**
     * Add a field with id set to <i>id</i>
     * 
     * @param id
     *            The id used to order fields in combo box.
     * @param status
     *            The status to add.
     * @return Field created from given status.
     */
    public FieldModelData addField(int id, EApprovalNoteStatus status)
    {
        FieldModelData field = new FieldModelData(id, NoteStatusComboBox.getFullStatusMap().get(status.toString()),
                status.toString());
        field.set("color", TaskStatusMap.getColorForStatus(status.toString()));
        this.getStore().add(field);

        return field;
    }

    /**
     * @return Combo box values display style.
     */
    public native String getXTemplate() /*-{
                                        return  [ 
                                        '<tpl for=".">',
                                        '<div class=\"x-view-item x-view-item-check\"><table><tr><td><input class=\"x-view-item-checkbox\" type=\"checkbox\" /></td><td><td>',
                                        '<div style="width: 100%; text-align: center; background-color:{color};" class="x-combo-list-item"> <span style="font-weight: bold; font-size: 16px;">&nbsp;{name}&nbsp;&nbsp;</span> </div>', 
                                        '</td></tr></table></div></tpl>' 
                                        ].join("");
                                        }-*/;

}
