package fr.hd3d.common.ui.client.widget.mainview;

import com.extjs.gxt.ui.client.widget.button.Button;

import fr.hd3d.common.ui.client.widget.RunScriptToolbarButton;


/**
 * CustomButtonInfos contains custom buttons informations : displayed name of the custom button and name of the script
 * that should be executed when button is clicked.
 * 
 * @author HD3D
 */
public class CustomButtonInfos
{
    /** Displayed name of the custom button. */
    private String name;
    /** Name of the script that should be executed when button is clicked. */
    private String script;

    /**
     * Default constructor.
     * 
     * @param name
     *            Displayed name of the custom button.
     * @param script
     *            Name of the script that should be executed when button is clicked.
     */
    public CustomButtonInfos(String name, String script)
    {
        this.setName(name);
        this.setScript(script);
    }

    /**
     * @param name
     *            The name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * @return The name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param script
     *            The script to set
     */
    public void setScript(String script)
    {
        this.script = script;
    }

    /**
     * @return The script
     */
    public String getScript()
    {
        return script;
    }

    /**
     * @return Custom Button widget corresponding to custom infos.
     */
    public Button asButton()
    {
        Button button = new RunScriptToolbarButton(name, script);

        return button;
    }
}
