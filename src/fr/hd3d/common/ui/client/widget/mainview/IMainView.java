package fr.hd3d.common.ui.client.widget.mainview;


/**
 * Interface for the application main view.
 * 
 * @author HD3D
 */
public abstract interface IMainView
{
    /**
     * Display a message box about the error corresponding to the error code given in parameter.
     * 
     * @param error
     *            The error code of the error to display.
     * */
    public void displayError(Integer error);

    public void displayError(Integer error, String userMsg, String stack);

    /**
     * Show loading panel at inventory Startup.
     */
    public void showStartPanel();

    /**
     * Hide loading panel shown at startup.
     */
    public void hideStartPanel();

    /**
     * Show script loading panel when a script execution started.
     */
    public void showScriptRunningPanel();

    /**
     * Hide script loading panel when a script execution finished.
     */
    public void hideScriptRunningPanel();
    
}
