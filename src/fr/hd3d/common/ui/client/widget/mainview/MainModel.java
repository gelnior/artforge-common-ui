package fr.hd3d.common.ui.client.widget.mainview;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.restlet.client.Request;
import org.restlet.client.Response;
import org.restlet.client.data.Method;
import org.restlet.client.representation.Representation;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.common.ui.client.config.CommonConfig;
import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.error.ErrorDispatcher;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.http.HttpRequestHandlerSingleton;
import fr.hd3d.common.ui.client.http.IHttpRequestHandler;
import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SettingModelData;
import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.service.callback.EventCallback;
import fr.hd3d.common.ui.client.service.callback.GetModelDataCallback;
import fr.hd3d.common.ui.client.service.callback.InitServicesConfigurationCallback;
import fr.hd3d.common.ui.client.service.parameter.ScriptParams;
import fr.hd3d.common.ui.client.setting.InitSettingsCallback;
import fr.hd3d.common.ui.client.util.ScriptUtils;


/**
 * Main model handles services configuration operations, user settings retrieving and custom buttons setup. Customs
 * buttons are specific buttons for user described inside the config.xml files. Theses buttons run server scripts when
 * clicked.
 * 
 * @author HD3D
 */
public class MainModel
{
    /** XML tag name for custom button informations. */
    public static final String BUTTONS_XML_TAG = "button";

    /** Currently connected user. */
    public static PersonModelData currentUser = new PersonModelData();;
    /** Currently selected project. */
    public static ProjectModelData currentProject = null;

    public static Boolean assetManager = Boolean.FALSE;

    /**
     * Return true if asset manager interface is activated. This field is setd by a web service. The value is the same
     * as the one set on the server 'hd3d.step.createasset'.
     * 
     * @return true if asset manager interface is activated.
     */
    public static Boolean getAssetManager()
    {
        return assetManager;
    }

    /** @return Currently connected user. */
    public static PersonModelData getCurrentUser()
    {
        return currentUser;
    }

    /**
     * Set currently connected user.
     * 
     * @param person
     *            The person to set as current user.
     */
    public static void setCurrentUser(PersonModelData person)
    {
        currentUser = person;
    }

    /** @return Currently selected project. */
    public static ProjectModelData getCurrentProject()
    {
        return currentProject;
    }

    /**
     * Set current project.
     * 
     * @param project
     *            The project to set as current project.
     */
    public static void setCurrentProject(ProjectModelData project)
    {
        currentProject = project;
    }

    /** Objects needed to send HTTP requests to server. */
    protected final IHttpRequestHandler requestHandler = HttpRequestHandlerSingleton.getInstance();

    /** List of custom buttons to add inside main toolbar view. */
    protected List<CustomButtonInfos> customButtons = new ArrayList<CustomButtonInfos>();

    /**
     * Initializes server URL and services path with configuration file parameters.
     */
    public void initServicesConfig()
    {
        try
        {
            requestHandler.getRequest(CommonConfig.CONFIG_FILE_PATH, new InitServicesConfigurationCallback(this));
        }
        catch (Exception e)
        {
            ErrorDispatcher.sendError(CommonErrors.NO_CONFIG_FILE);
        }
    }

    /**
     * Retrieve user settings and store them in UserSettings class.
     */
    public void retrieveSettings()
    {
        IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
        requestHandler.handleRequest(Method.GET, ServicesPath.getPath(SettingModelData.SIMPLE_CLASS_NAME)
                + ServicesPath.SELF, null, new InitSettingsCallback());
    }

    /**
     * Add a custom button to the custom button list.
     * 
     * @param button
     *            Custom button to add.
     */
    public void addCustomButton(CustomButtonInfos button)
    {
        this.customButtons.add(button);
    }

    /**
     * @return Custom buttons list set by user.
     */
    public List<CustomButtonInfos> getCustomButtons()
    {
        return this.customButtons;
    }

    public void retrieveCurrentUser()
    {
        GetModelDataCallback callback = new GetModelDataCallback(currentUser, new AppEvent(
                CommonEvents.PERSON_INITIALIZED));
        RestRequestHandlerSingleton.getInstance().getRequest(ServicesPath.WHOIS, callback);
    }

    /**
     * Ask to script for script launching.
     * 
     * @param scriptName
     *            The name of the script to launch.
     * @param event
     *            Event to raise when script finishes
     */
    public void launchScript(String scriptName, EventType event)
    {
        loadScript(scriptName, event, new ScriptParams());
    }

    /**
     * Ask to script for script launching.
     * 
     * @param scriptName
     *            The name of the script to launch.
     * @param event
     *            Event to raise when script finishes
     * @param params
     *            to add to service.
     */
    public void loadScript(String scriptName, EventType event, ScriptParams params)
    {
        EventCallback callback = new EventCallback(event);
        if (MainModel.currentProject != null)
            params.set(ScriptUtils.PROJECT_PARAMETER, MainModel.currentProject.getId());

        ScriptUtils.callScript(scriptName, params, callback);
    }

    /** Send a log out request to server. */
    public void logOut()
    {
        try
        {
            IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
            requestHandler.getRequest(ServicesURI.LOGOUT, new EventCallback(CommonEvents.LOG_OUT));
        }
        catch (Exception e)
        {
            ErrorDispatcher.sendError(CommonErrors.NO_SERVICE_CONNECTION);
        }
    }

    public void initAssetManager()
    {
        IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
        requestHandler.handleRequest(Method.GET, ServicesURI.DISPLAY_ASSET_MANAGER + "/", null, new BaseCallback() {
            @Override
            protected void onSuccess(Request request, Response response)
            {
                try
                {
                    Representation rep = response.getEntity();
                    if (rep == null)
                    {
                        return;
                    }
                    assetManager = Boolean.valueOf(rep.getText());
                }
                catch (IOException e)
                {
                    Logger.log("Error - mainModel->Constructor : display asset manager.");
                }
            }
        });
    }

}
