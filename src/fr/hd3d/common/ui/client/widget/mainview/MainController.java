package fr.hd3d.common.ui.client.widget.mainview;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.google.gwt.http.client.URL;
import com.google.gwt.json.client.JSONNumber;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.json.client.JSONValue;

import fr.hd3d.common.client.ScriptMessageGenerator;
import fr.hd3d.common.ui.client.config.CommonConfig;
import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.service.ExcludedField;
import fr.hd3d.common.ui.client.service.Hd3dClassReadersFactory;
import fr.hd3d.common.ui.client.service.PermissionPath;
import fr.hd3d.common.ui.client.service.ServicesClass;
import fr.hd3d.common.ui.client.service.ServicesField;
import fr.hd3d.common.ui.client.service.ServicesModelType;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.userrights.UserRightsResolver;


/**
 * Main controller to handle configuration, error and start events.
 * 
 * @author HD3D
 */
public class MainController extends MaskableController
{
    /** Logs widget view */
    final private IMainView mainView;
    /** Data model */
    final private MainModel mainModel;

    /** Default constructor. */
    public MainController(MainModel model, IMainView view)
    {
        this.mainModel = model;
        this.mainView = view;

        this.registerEvents();
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();
        if (type == CommonEvents.START)
        {
            this.onStart();
        }
        else if (type == CommonEvents.CONFIG_INITIALIZED)
        {
            this.onConfigInitialized(event);
        }
        else if (type == CommonEvents.PERMISSION_INITIALIZED)
        {
            this.onPermissionsInitialized(event);
        }
        else if (type == CommonEvents.PERSON_INITIALIZED)
        {
            this.onPersonInitialized(event);
        }
        else if (type == CommonEvents.SETTINGS_INITIALIZED)
        {
            this.onSettingInitialized(event);
        }
        else if (type == CommonEvents.ERROR)
        {
            this.onError(event);
        }
        else if (type == CommonEvents.LOG_OUT_AFTER_COOKIE_EXPIRED)
        {
            this.onCookieExpired();
        }
        else if (type == CommonEvents.LOG_OUT)
        {
            this.onLogOut();
        }
        else if (type == CommonEvents.SCRIPT_EXECUTION_STARTED)
        {
            onScriptStarted();
        }
        else if (type == CommonEvents.SCRIPT_EXECUTION_FINISHED)
        {
            onScriptFinished((String) event.getData());
        }
    }

    /**
     * Display an error dialog box when a script execution failed
     * 
     * @param exitValues
     *            , a json representation of script execution result<br/>
     *            <li>exitCode : the exit code value</li> <li>errorMessage : if the script execution failed,
     *            errorMessage represent the script stack trace</li>
     */
    private void onScriptFinished(String exitValues)
    {
        this.mainView.hideScriptRunningPanel();
        try
        {
            exitValues = exitValues.replace("\n", "");
            JSONValue parser = JSONParser.parse(exitValues);
            JSONObject object = parser.isObject();
            int exitCode = -1;
            String errorMsg = "";
            if (object != null)
            {
                if (object.containsKey(ScriptMessageGenerator.EXIT_CODE_PARAMETER))
                {
                    JSONNumber number = object.get(ScriptMessageGenerator.EXIT_CODE_PARAMETER).isNumber();
                    if (number != null)
                    {
                        exitCode = (int) number.doubleValue();
                    }
                }
                if (object.containsKey(ScriptMessageGenerator.ERROR_MESSAGE_PARAMETER))
                {
                    JSONString errorMessageString = object.get(ScriptMessageGenerator.ERROR_MESSAGE_PARAMETER)
                            .isString();
                    errorMsg = errorMessageString.stringValue();
                    errorMsg = URL.decodeComponent(errorMsg, true);
                }
            }

            if (exitCode != 0)
            {
                AppEvent appEvent = new AppEvent(CommonEvents.ERROR);
                appEvent.setData(CommonConfig.ERROR_EVENT_VAR_NAME, Integer
                        .valueOf(CommonErrors.SCRIPT_EXECUTION_FAILED));
                appEvent.setData(CommonConfig.ERROR_MSG_EVENT_VAR_NAME, MainView.CONSTANTS.ScriptFailed());
                appEvent.setData(CommonConfig.ERROR_TECH_MSG_EVENT_VAR_NAME, errorMsg);
                EventDispatcher.forwardEvent(appEvent);
            }
        }
        catch (Exception e)
        {

        }
    }

    private void onScriptStarted()
    {
        this.mainView.showScriptRunningPanel();
    }

    /**
     * When the session cookie is deleted, force the application to return to login panel
     */
    private void onLogOut()
    {
        reloadBrowser();
    }

    /**
     * When the session cookie is deleted, force the application to return to login panel
     */
    private native void reloadBrowser()/*-{
                                                                                   $wnd.top.location.reload();
                                                                                   }-*/;

    /**
     * When the session cookie expired, delete the session cookie
     */
    private void onCookieExpired()
    {
        this.mainModel.logOut();
    }

    /**
     * When application starts, it retrieves configuration file and sets service URL into the REST request handler. <br>
     * Then it initializes:
     * <ul>
     * <li>Dictionary for bound class name / services path correspondence.</li>
     * <li>Dictionary for bound class name / services class correspondence.</li>
     * <li>Dictionary for human name field / JSON name field correspondence.</li>
     * <li>Dictionary for bound class name / model type correspondence.</li>
     * <li>Dictionary for bound class name / permission path correspondence.</li>
     * <li>List of excluded field for displaying.</li>
     * </ul>
     */
    protected void onStart()
    {
        this.mainView.showStartPanel();

        ServicesPath.initPath();
        ServicesClass.initClass();
        Hd3dClassReadersFactory.initClass();
        ServicesModelType.initModelTypes();

        ServicesField.initFields();
        ExcludedField.initExcludedFields();

        PermissionPath.initPath();

        this.mainModel.initServicesConfig();

    }

    /**
     * When configuration is initialized, it retrieves user rights.
     */
    protected void onConfigInitialized(AppEvent event)
    {
        UserRightsResolver.acquireUserRights();
        this.mainModel.initAssetManager();
    }

    /**
     * When user rights are initialized, it retrieves current user data.
     */
    protected void onPermissionsInitialized(AppEvent event)
    {
        this.mainModel.retrieveCurrentUser();
    }

    /**
     * When current user data are initialized, it retrieves current user settings.
     */
    private void onPersonInitialized(AppEvent event)
    {
        this.mainModel.retrieveSettings();
    }

    /**
     * When user rights are initialized, it does nothing.
     */
    protected void onSettingInitialized(AppEvent event)
    {}

    /**
     * When error occurs, it displays an appropriate message depending on the error code.
     * 
     * @param event
     *            The error event.
     */
    protected void onError(AppEvent event)
    {
        Integer error = event.getData(CommonConfig.ERROR_EVENT_VAR_NAME);
        String userMsg = event.getData(CommonConfig.ERROR_MSG_EVENT_VAR_NAME);
        String stack = event.getData(CommonConfig.ERROR_TECH_MSG_EVENT_VAR_NAME);

        this.mainView.hideStartPanel();
        if (error != null)
        {
            this.mainView.displayError(error.intValue(), userMsg, stack);
        }

        this.forwardToChild(event);
    }

    /** Register all events the controller can handle. */
    protected void registerEvents()
    {
        this.registerEventTypes(CommonEvents.START);
        this.registerEventTypes(CommonEvents.CONFIG_INITIALIZED);
        this.registerEventTypes(CommonEvents.PERMISSION_INITIALIZED);
        this.registerEventTypes(CommonEvents.PERSON_INITIALIZED);
        this.registerEventTypes(CommonEvents.SETTINGS_INITIALIZED);
        this.registerEventTypes(CommonEvents.ERROR);
        this.registerEventTypes(CommonEvents.SCRIPT_EXECUTION_STARTED);
        this.registerEventTypes(CommonEvents.SCRIPT_EXECUTION_FINISHED);
        this.registerEventTypes(CommonEvents.LOG_OUT_AFTER_COOKIE_EXPIRED);
        this.registerEventTypes(CommonEvents.LOG_OUT);

    }

}
