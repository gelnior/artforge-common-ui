package fr.hd3d.common.ui.client.widget.mainview;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MessageBoxEvent;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.Viewport;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.toolbar.SeparatorToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.ToolBar;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.widget.LoadingPanel;
import fr.hd3d.common.ui.client.widget.dialog.ErrorMessageDialog;


/**
 * Application main view that handles service loading panel at start, services configuration and common message error
 * displaying.
 * 
 * @author HD3D
 */
public abstract class MainView implements IMainView
{
    /** Constant strings to display : dialog messages, button label... */
    public static final CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    /** Viewport needed at startup before loading */
    protected Viewport startViewport = new Viewport();
    /** Panel to show at application startup */
    protected LoadingPanel loadingPanel;

    /** Application name. */
    protected String appName;
    /** Give error number actually displayed. */
    protected List<Integer> errorsDisplayed = new ArrayList<Integer>();

    private final LoadingPanel scriptLoadingPanel;

    /**
     * Default constructor.
     * 
     * @param appName
     *            Application name to display at application startup.
     */
    public MainView(String appName)
    {
        this.appName = appName;
        this.loadingPanel = new LoadingPanel(this.appName, CONSTANTS.Loading());
        this.scriptLoadingPanel = new LoadingPanel(CONSTANTS.ScriptRunning(), CONSTANTS.ScriptRunningMessage());
    }

    /** Initialize widgets, when services configuration is done. */
    public abstract void init();

    /**
     * Display error message depending on error code passed in parameter.
     * 
     * @param error
     *            The code of the error to display.
     */
    public void displayError(final Integer error)
    {
        displayError(error, null, null);
    }

    /**
     * Display error message. Ensure that the same error cannot be displayed twice in the same time.
     * 
     * @param error
     *            The code of the error to display.
     * @param userMsg
     *            The error message for user.
     * @param stack
     *            The stack trace returned by server (message for developer).
     */
    public void displayError(final Integer error, String userMsg, String stack)
    {
        if (!errorsDisplayed.contains(error))
        {
            errorsDisplayed.add(error);
            Listener<MessageBoxEvent> callback = new Listener<MessageBoxEvent>() {
                public void handleEvent(MessageBoxEvent be)
                {
                    errorsDisplayed.remove(error);
                    if (error == CommonErrors.NOT_AUTHENTIFIED)
                    {
                        EventDispatcher.forwardEvent(CommonEvents.LOG_OUT_AFTER_COOKIE_EXPIRED);
                    }
                }
            };

            this.displayErrorMessageBox(error, userMsg, stack, callback);
        }
    }

    /**
     * Display error message box containing error message for user and error message for developer.
     * 
     * 
     * @param error
     *            The code of the error to display.
     * @param userMsg
     *            The error message for user.
     * @param stack
     *            The stack trace returned by server (message for developer).
     * @param callback
     *            Callback to call when message box is hidden.
     */
    protected void displayErrorMessageBox(int error, String userMsg, String stack, Listener<MessageBoxEvent> callback)
    {
        switch (error)
        {
            case CommonErrors.NO_CONFIG_FILE:
                MessageBox.alert(CONSTANTS.Error(), CONSTANTS.configurationFileError(), callback);
                break;
            case CommonErrors.BAD_CONFIG_FILE:
                MessageBox.alert(CONSTANTS.Error(), CONSTANTS.malformedConfigurationFileError(), callback);
                break;
            case CommonErrors.SERVER_ERROR:
                ErrorMessageDialog.alert(CONSTANTS.Error(), CONSTANTS.dataServerError() + "<br />" + userMsg, stack,
                        callback);
                break;
            case CommonErrors.NO_SERVICE_CONNECTION:
                MessageBox.alert(CONSTANTS.Error(), CONSTANTS.dataServerError(), callback);
                break;
            case CommonErrors.NOT_AUTHENTIFIED:
                MessageBox.alert(CONSTANTS.Error(), CONSTANTS.notAuthenticated(), callback);
                break;
            case CommonErrors.NO_PERMISSION:
                MessageBox.alert(CONSTANTS.Error(), CONSTANTS.noPermission(), callback);
                break;
            case CommonErrors.NO_DATA_TYPE_CONFIG_FILE:
                MessageBox.alert(CONSTANTS.Error(), CONSTANTS.dataTypeFileError(), callback);
                break;
            case CommonErrors.TASK_TYPE_WITHOUT_ENTITY:
                MessageBox.alert(CONSTANTS.Error(),
                        "One or more task types has no entity please set it before saving.", callback);
                break;
            case CommonErrors.SCRIPT_EXECUTION_FAILED:
                ErrorMessageDialog.alert(CONSTANTS.Error(), userMsg, stack, callback);
                break;
            default:
                ;
        }
    }

    /**
     * Show loading panel at application startup.
     */
    public void showStartPanel()
    {
        startViewport.add(loadingPanel);
        RootPanel.get().add(startViewport);
    }

    /**
     * Hide loading panel application at startup.
     */
    public void hideStartPanel()
    {
        this.loadingPanel.hide();
        this.startViewport.removeFromParent();
    }

    /**
     * Show loading panel at application startup.
     */
    public void showScriptRunningPanel()
    {
        scriptLoadingPanel.show();
        RootPanel.get().add(scriptLoadingPanel);
    }

    /**
     * Hide loading panel application at startup.
     */
    public void hideScriptRunningPanel()
    {
        this.scriptLoadingPanel.hide();
    }

    /**
     * Insert panel inside navigator window. Panel fill the whole place.
     * 
     * @param panel
     *            The panel to insert.
     */
    protected void addToViewport(Widget panel)
    {
        Viewport viewport = new Viewport();
        viewport.setLayout(new FitLayout());
        viewport.add(panel);

        RootPanel.get().add(viewport);
    }

    /**
     * Add custom button defined in configuration file to <i>toolbar</i>.
     * 
     * @param toolBar
     *            The toolbar that will display custom buttons.
     * @param buttons
     *            Custom buttons defined by user.
     */
    protected void addCustomButtons(ToolBar toolBar, List<CustomButtonInfos> buttons)
    {
        if (CollectionUtils.isNotEmpty(buttons))
        {
            toolBar.add(new SeparatorToolItem());
            for (CustomButtonInfos button : buttons)
            {
                toolBar.add(button.asButton());
            }
        }
    }
}
