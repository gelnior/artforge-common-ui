package fr.hd3d.common.ui.client.widget;

import com.extjs.gxt.ui.client.binding.FieldBinding;

import fr.hd3d.common.ui.client.modeldata.FieldModelData;


/**
 * Adapt GXT FieldBinding to FieldDataComboBox.
 * 
 * @author HD3D
 */
public class FieldDataComboboxFieldBinding extends FieldBinding
{

    private final FieldComboBox fieldComboBox;

    public FieldDataComboboxFieldBinding(FieldComboBox fieldComboBox, String property)
    {
        super(fieldComboBox, property);
        this.fieldComboBox = fieldComboBox;
    }

    @Override
    protected Object onConvertFieldValue(Object value)
    {
        if (fieldComboBox.getSelection().size() > 0)
        {
            FieldModelData fieldModelData = fieldComboBox.getSelection().get(0);
            return fieldModelData.getValue();
        }
        return null;
    }

    @Override
    protected Object onConvertModelValue(Object value)
    {
        for (FieldModelData fieldModelData : this.fieldComboBox.getStore().getModels())
        {
            if (fieldModelData.getValue().equals(value))
            {
                return fieldModelData;
            }
        }
        return null;
    }
}
