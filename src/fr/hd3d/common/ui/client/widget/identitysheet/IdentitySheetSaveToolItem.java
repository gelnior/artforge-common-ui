package fr.hd3d.common.ui.client.widget.identitysheet;

import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.widget.ToolBarButton;
import fr.hd3d.common.ui.client.widget.identitysheet.events.IdentitySheetEvents;


/**
 * A save button for the identity panel.
 */
public class IdentitySheetSaveToolItem extends ToolBarButton
{
    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    /**
     * Default constructor
     */
    public IdentitySheetSaveToolItem()
    {
        super(Hd3dImages.getSaveIcon(), CONSTANTS.Save(), IdentitySheetEvents.SAVE_CLICKED);
    }
}
