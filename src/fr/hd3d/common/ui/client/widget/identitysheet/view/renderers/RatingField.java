package fr.hd3d.common.ui.client.widget.identitysheet.view.renderers;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.core.El;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.widget.form.Field;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Event;


/**
 * Rating field for identity sheet.
 */
public class RatingField extends Field<Integer>
{
    private int maxValue;
    private List<Element> stars;

    /**
     * Default constructor
     */
    public RatingField()
    {
        super();
        init();
    }

    /**
     * Specialized constructor for creating Rating from existing nodes. The given element should be part of the dom and
     * have a a parent element.
     * 
     * @param element
     *            the element
     * @param attach
     *            true to attach the Rating wigdet
     */
    public RatingField(Element element, boolean attach)
    {
        super();
        init();
    }

    @Override
    public void clearInvalid()
    {}

    @Override
    public void markInvalid(String msg)
    {}

    @Override
    public String getRawValue()
    {
        return value.toString();
    }

    @Override
    public void setRawValue(String value)
    {
        String rawValue = value;
        if (value.contains("."))
        {
            String[] tmpValue = value.split("\\.");
            rawValue = tmpValue[0];
        }
        if (value.contains(","))
        {
            String[] tmpValue = value.split("\\.");
            rawValue = tmpValue[0];
        }
        Integer v = Integer.parseInt(rawValue);
        this.value = v;
    }

    @Override
    public Integer getValue()
    {
        return value;
    }

    @Override
    public void setValue(Integer value)
    {
        this.value = value;
        if (this.rendered)
        {
            this.refresh();
        }
    }

    public int getMaxValue()
    {
        return this.maxValue;
    }

    public void setMaxValue(int maxValue)
    {
        this.maxValue = maxValue;
        if (this.rendered)
        {
            this.clear();
            this.refresh();
        }
    }

    protected void renderRatingButton()
    {
        stars.clear();
        Element ul = DOM.createElement("ul");
        getElement().setClassName("radioGroupContainer");

        for (int i = 0; i < maxValue; i++)
        {
            Element li = DOM.createElement("li");
            Element img = DOM.createElement("div");

            img.setAttribute("radioValue", String.valueOf(i));
            stars.add(img);
            if (i < value)
            {
                img.setClassName("cercle-check");
            }
            else
            {
                img.setClassName("cercle-uncheck");
            }
            li.setClassName("radioGroupList");
            li.appendChild(img);
            ul.appendChild(li);
        }
        getElement().appendChild(ul);
    }

    @Override
    protected void onRender(Element target, int index)
    {
        if (el() == null)
        {
            setElement(DOM.createDiv(), target, index);
        }
        refresh();
        El input = el().firstChild();

        addStyleName("x-form-field-wrap");
        input.addStyleName(fieldStyle);

        input.setId(getId() + "-input");

        super.onRender(target, index);
        removeStyleName(fieldStyle);

        el().addEventsSunk(Event.ONCLICK | Event.MOUSEEVENTS);
    }

    protected void refresh()
    {
        if (maxValue != 0 && stars.size() == 0)
        {
            this.renderRatingButton();
        }
        else
        {
            int nbButton = stars.size();
            for (int i = 0; i < nbButton; i++)
            {
                Element img = stars.get(i);
                if (i < value)
                {
                    img.setClassName("cercle-check");
                }
                else
                {
                    img.setClassName("cercle-uncheck");
                }
            }
        }
    }

    @Override
    public void clear()
    {
        this.getElement().setInnerHTML("");
    }

    @Override
    public void onClick(ComponentEvent ce)
    {
        super.onClick(ce);
        if (!readOnly)
        {
            if (!this.disabled)
            {
                Element img = ce.getTarget();
                String newValue = img.getAttribute("radioValue");
                if (newValue != "" && newValue.equals(String.valueOf(this.value)) == false)
                {
                    this.setValue(Integer.valueOf(newValue));
                    this.fireEvent(Events.Change);
                }
            }
        }
        else
        {
            ce.stopEvent();
            ce.cancelBubble();
        }
    }

    @Override
    public void onComponentEvent(ComponentEvent ce)
    {
        super.onComponentEvent(ce);
        switch (ce.getType().getEventCode())
        {
            case Event.ONCLICK:
                onClick(ce);
                break;
        }
    }

    protected void init()
    {
        maxValue = 5;
        value = Integer.valueOf(1);
        stars = new ArrayList<Element>(maxValue);
    }

    public void setValue(Number val)
    {
        if (val instanceof Double)
        {
            this.setValue(((Double) val).intValue());
        }
        else if (val instanceof Integer)
        {
            this.setValue(((Integer) val));
        }
        else if (val instanceof Long)
        {
            this.setValue(((Long) val).intValue());
        }
    }

    public void setOriginalValue(Number val)
    {
        if (val instanceof Double)
        {
            this.setOriginalValue(((Double) val).intValue());
        }
        else if (val instanceof Integer)
        {
            this.setOriginalValue(((Integer) val));
        }
        else if (val instanceof Long)
        {
            this.setOriginalValue(((Long) val).intValue());
        }
    }
}
