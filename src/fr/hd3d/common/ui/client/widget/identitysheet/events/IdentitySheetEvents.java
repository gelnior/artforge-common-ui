package fr.hd3d.common.ui.client.widget.identitysheet.events;

import com.extjs.gxt.ui.client.event.EventType;


/**
 * Events raised by identity sheet widget.
 * 
 * @author HD3D
 */
public class IdentitySheetEvents
{

    public static int START_EVENT = 21000;
    public static final EventType SAVE = new EventType(START_EVENT++);
    public static final EventType REFRESH_CLICKED = new EventType(START_EVENT++);
    public static final EventType DELETE_SUCCESS = new EventType(START_EVENT++);
    public static final EventType CREATE_SUCCESS = new EventType(START_EVENT++);
    public static final EventType UPDATE_SUCCESS = new EventType(START_EVENT++);
    public static final EventType UNDO = new EventType(START_EVENT++);
    public static final EventType REDO = new EventType(START_EVENT++);

    public static final EventType ERROR = new EventType(START_EVENT++);
    public static final EventType INIT = new EventType(START_EVENT++);
    public static final EventType INIT_DATA = new EventType(START_EVENT++);
    public static final EventType INIT_SUCCESS = new EventType(START_EVENT++);
    public static final EventType INIT_CONFIG = new EventType(START_EVENT++);
    public static final EventType LOAD_SHEET = new EventType(START_EVENT++);
    public static final EventType LOAD_PORTLETS = new EventType(START_EVENT++);
    public static final EventType PORTLETS_LOADED = new EventType(START_EVENT++);
    public static final EventType LOAD_ITEMS = new EventType(START_EVENT++);
    public static final EventType ITEMS_LOADED = new EventType(START_EVENT++);
    public static final EventType LOAD_DATAS = new EventType(START_EVENT++);
    public static final EventType DATAS_LOADED = new EventType(START_EVENT++);
    public static final EventType PREC_LINE = new EventType(START_EVENT++);
    public static final EventType SUIV_LINE = new EventType(START_EVENT++);
    public static final EventType SAVE_CLICKED = new EventType(START_EVENT++);
    public static final EventType SWITCH_MODE = new EventType(START_EVENT++);
    public static final EventType DATAS_SAVED = new EventType(START_EVENT++);
    public static final EventType SAVE_FAILED = new EventType(START_EVENT++);
    public static final EventType LOAD_DYNMETADATA = new EventType(START_EVENT++);
    public static final EventType DYNMETADATA_LOADED = new EventType(START_EVENT++);
    public static final EventType SAVE_DYNMETADATA = new EventType(START_EVENT++);
    public static final EventType DYNMETADATA_SAVED = new EventType(START_EVENT++);
    public static final EventType SEND_RENDERER = new EventType(START_EVENT++);
}
