package fr.hd3d.common.ui.client.widget.identitysheet.view.renderer.old;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GroupColumnData;

import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.IExplorateurColumnContextMenu;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.IExplorerCellRenderer;


public class ShortTextEditRenderer<M extends ModelData> implements IExplorerCellRenderer<M>,
        IExplorateurColumnContextMenu<M>
{

    public Object render(M model, String property, ColumnData config, int rowIndex, int colIndex, ListStore<M> store,
            Grid<M> grid)
    {
        Object obj = model.get(property);
        return getRenderedValue(obj);
    }

    public String render(GroupColumnData data)
    {
        return getRenderedValue(data.gvalue);
    }

    private String getRenderedValue(Object gValue)
    {
        String stringValue = "";
        if (gValue != null)
        {
            stringValue = "<div style= 'height: 20; border: 1px solid #DCDCDC;'>" + (String) gValue + "</div>";
        }
        else
        {
            stringValue = "<div style= 'height: 20; border: 1px solid #DCDCDC;'>" + " " + "</div>";
        }

        return stringValue;
    }

    public void handleGridEvent(GridEvent<M> ge)
    {

    }
}
