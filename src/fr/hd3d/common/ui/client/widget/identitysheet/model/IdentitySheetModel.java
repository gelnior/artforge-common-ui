package fr.hd3d.common.ui.client.widget.identitysheet.model;

import java.util.List;

import com.extjs.gxt.ui.client.data.BaseModel;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.ListStore;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.reader.DynValueReader;
import fr.hd3d.common.ui.client.modeldata.technical.DynValueModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ItemGroupModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetDataModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.widget.explorer.model.reader.ItemGroupReader;
import fr.hd3d.common.ui.client.widget.explorer.model.reader.ItemReader;
import fr.hd3d.common.ui.client.widget.explorer.model.reader.SheetDataReader;
import fr.hd3d.common.ui.client.widget.identitysheet.events.IdentitySheetEvents;


/**
 * Model holds data store for identity sheet.
 * 
 * @author HD3D
 */
public class IdentitySheetModel extends BaseModel
{
    private static final long serialVersionUID = 5716548900553775333L;

    /** Contains all data about sheet item groups. */
    private final ServiceStore<ItemGroupModelData> portletsStore = new ServiceStore<ItemGroupModelData>(
            new ItemGroupReader());
    /** Contains all data about sheet items. */
    private final ServiceStore<ItemModelData> itemsStore = new ServiceStore<ItemModelData>(new ItemReader());
    /** Contains all data about objects edited by identity sheet. */
    private final ServiceStore<SheetDataModelData> dataStore = new ServiceStore<SheetDataModelData>(
            new SheetDataReader());
    /** Contains all dynamic data about objects edited by identity sheet. */
    private final ServiceStore<DynValueModelData> dynMetaDataStore = new ServiceStore<DynValueModelData>(
            new DynValueReader());

    /** Sheet selected for displaying objects data. */
    private SheetModelData currentSheet;
    /** Simple class name of entity manipulated by the identity sheet. */
    private String simpleClassName;

    /**
     * Default constructor, set load listeners on store to notify when their loading is finished.
     */
    public IdentitySheetModel()
    {
        this.dataStore.addEventLoadListener(IdentitySheetEvents.DATAS_LOADED);
        this.dynMetaDataStore.addEventLoadListener(IdentitySheetEvents.DYNMETADATA_LOADED);
    }

    /**
     * @return Sheet selected for displaying objects data.
     */
    public SheetModelData getCurrentSheet()
    {
        return currentSheet;
    }

    /**
     * Register selected sheet.
     * 
     * @param sheet
     *            The sheet to set as selected sheet.
     */
    public void setCurrentSheet(SheetModelData sheet)
    {
        this.currentSheet = sheet;
    }

    /**
     * @return Store holding sheet item groups.
     */
    public ListStore<ItemGroupModelData> getPortletsStore()
    {
        return portletsStore;
    }

    public void setPortletsStore(List<ItemGroupModelData> portletsList)
    {
        this.portletsStore.removeAll();
        this.portletsStore.add(portletsList);
    }

    public ListStore<ItemModelData> getItemsStore()
    {
        return itemsStore;
    }

    public void setItemsStore(List<ItemModelData> itemsList)
    {
        this.itemsStore.add(itemsList);
    }

    public ListStore<SheetDataModelData> getDatasStore()
    {
        return dataStore;
    }

    public void setDatasStore(List<SheetDataModelData> datasList)
    {
        this.dataStore.removeAll();
        this.dataStore.add(datasList);
    }

    public ServiceStore<DynValueModelData> getDynMetaDataStore()
    {
        return dynMetaDataStore;
    }

    public String getSimpleClassName()
    {
        return this.simpleClassName;
    }

    public void setSimpleClassName(String simpleClassName)
    {
        this.simpleClassName = simpleClassName;
    }

    public void setConstraintDyn(Constraint constraint)
    {
        this.dynMetaDataStore.clearParameters();
        this.dynMetaDataStore.addParameter(constraint);
    }

    public void setConstraint(Constraint constraint)
    {
        this.dataStore.clearParameters();
        this.dataStore.addParameter(constraint);
    }

    /**
     * Clear all data contained in the stores.
     */
    public void clear()
    {
        this.portletsStore.removeAll();
        this.itemsStore.removeAll();
        this.dataStore.removeAll();
        this.dynMetaDataStore.removeAll();
    }

    /**
     * Load data inside data store only if there is at least one item group in portlet store.
     */
    public void loadPortletsDatas()
    {
        if (portletsStore.getCount() > 0)
        {
            String sheetPath = ServicesPath.getOneSegmentPath(ServicesPath.SHEETS, currentSheet.getId());
            this.dataStore.setPath(sheetPath + ServicesPath.DATA);
            this.portletsStore.setPath(sheetPath + ServicesPath.ITEM_GROUPS);
            this.itemsStore.setPath(sheetPath + ServicesPath.ITEMS);
            this.dataStore.reload();
        }
        else
        {
            AppEvent event = new AppEvent(IdentitySheetEvents.DATAS_LOADED);
            EventDispatcher.forwardEvent(event);
        }
    }

    /**
     * Reload dynamic data store.
     */
    public void loadDynMetaData()
    {
        this.dynMetaDataStore.reload();
    }

    /**
     * Searches inside dynamic data store which dynamic value corresponds to <i>dynValue</i> by comparing attached
     * dynamic class and parent id. Then sets <i>dynValue</i> with the the id of searched dynamic value.
     * 
     * @param dynValue
     */
    public void updateDynId(DynValueModelData dynValue)
    {
        for (DynValueModelData value : this.dynMetaDataStore.getModels())
        {
            Long dynValueParentId = dynValue.getParentId();
            Long valueParentId = value.getParentId();
            Long dynValueDynClassId = dynValue.getDynClassId();
            Long valueDynClassId = dynValue.getDynClassId();
            if (dynValue != null && dynValueParentId != null && valueParentId != null
                    && dynValueParentId.longValue() == valueParentId.longValue()
                    && dynValueDynClassId.longValue() == valueDynClassId.longValue())
            {
                dynValue.setId(value.getId());
                break;
            }
        }
    }

    public void configureForCurrentProject()
    {
        this.dynMetaDataStore.setPath(ServicesPath.DYN_METADATA_VALUES);
    }
}
