package fr.hd3d.common.ui.client.widget.identitysheet.view.renderers;

import com.extjs.gxt.ui.client.core.FastMap;

import fr.hd3d.common.ui.client.calendar.DatetimeUtil;
import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.modeldata.production.StepModelData;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.widget.FieldComboBox;


public class StepField extends FieldComboBox
{
    protected StepModelData step;

    protected static FastMap<FieldModelData> map = new FastMap<FieldModelData>();

    public static FastMap<FieldModelData> getPossibleValues()
    {
        if (map.isEmpty())
        {
            FieldUtils.addFieldToMap(map, 0, "0", 0L);
            FieldUtils.addFieldToMap(map, 1, "15m", DatetimeUtil.MINUTE_SECONDS * 15L);
            FieldUtils.addFieldToMap(map, 2, "30m", DatetimeUtil.MINUTE_SECONDS * 30L);
            FieldUtils.addFieldToMap(map, 3, "45m", DatetimeUtil.MINUTE_SECONDS * 45L);
            FieldUtils.addFieldToMap(map, 5, "1h", DatetimeUtil.HOUR_SECONDS * 1L);
            FieldUtils.addFieldToMap(map, 6, "2h", DatetimeUtil.HOUR_SECONDS * 2L);
            FieldUtils.addFieldToMap(map, 7, "3h", DatetimeUtil.HOUR_SECONDS * 3L);
            FieldUtils.addFieldToMap(map, 8, "4h", DatetimeUtil.HOUR_SECONDS * 4L);
            FieldUtils.addFieldToMap(map, 10, "1d", DatetimeUtil.DAY_SECONDS * 1L);
            FieldUtils.addFieldToMap(map, 11, "2d", DatetimeUtil.DAY_SECONDS * 2L);
            FieldUtils.addFieldToMap(map, 12, "3d", DatetimeUtil.DAY_SECONDS * 3L);
            FieldUtils.addFieldToMap(map, 13, "4d", DatetimeUtil.DAY_SECONDS * 4L);
            FieldUtils.addFieldToMap(map, 14, "5d", DatetimeUtil.DAY_SECONDS * 5L);
            FieldUtils.addFieldToMap(map, 15, "6d", DatetimeUtil.DAY_SECONDS * 6L);
            FieldUtils.addFieldToMap(map, 16, "7d", DatetimeUtil.DAY_SECONDS * 7L);
            FieldUtils.addFieldToMap(map, 17, "8d", DatetimeUtil.DAY_SECONDS * 8L);
            FieldUtils.addFieldToMap(map, 18, "9d", DatetimeUtil.DAY_SECONDS * 9L);
            FieldUtils.addFieldToMap(map, 19, "10d", DatetimeUtil.DAY_SECONDS * 10L);
            FieldUtils.addFieldToMap(map, 20, "15d", DatetimeUtil.DAY_SECONDS * 15L);
            FieldUtils.addFieldToMap(map, 21, "20d", DatetimeUtil.DAY_SECONDS * 20L);
            FieldUtils.addFieldToMap(map, 22, "1M", DatetimeUtil.DAY_SECONDS * 25L);
            FieldUtils.addFieldToMap(map, 23, "2M", DatetimeUtil.DAY_SECONDS * 25L * 2);
            FieldUtils.addFieldToMap(map, 24, "3M", DatetimeUtil.DAY_SECONDS * 25L * 3);
            FieldUtils.addFieldToMap(map, 25, "6M", DatetimeUtil.DAY_SECONDS * 25L * 6);
            FieldUtils.addFieldToMap(map, 26, "9M", DatetimeUtil.DAY_SECONDS * 25L * 9);
            FieldUtils.addFieldToMap(map, 27, "12M", DatetimeUtil.DAY_SECONDS * 25L * 12);
            FieldUtils.addFieldToMap(map, 28, "18M", DatetimeUtil.DAY_SECONDS * 25L * 15);
            FieldUtils.addFieldToMap(map, 29, "18M", DatetimeUtil.DAY_SECONDS * 25L * 18);
            FieldUtils.addFieldToMap(map, 30, "24M", DatetimeUtil.DAY_SECONDS * 25L * 24);
        }
        return map;
    }

    public StepField()
    {
        super(getPossibleValues());

        this.setEditable(false);

        this.setTemplate();
    }

    public void setLongValue(Long value)
    {
        this.setValue((FieldModelData) this.getValue(value));
    }

    public void setStepValue(StepModelData step)
    {
        this.step = step;
        this.setLongValue(step.getEstimatedDuration());
    }

    public StepModelData getStep()
    {
        return this.step;
    }

    private void setTemplate()
    {
        FastMap<Boolean> bigValues = new FastMap<Boolean>();
        bigValues.put("1h", Boolean.TRUE);
        bigValues.put("4h", Boolean.TRUE);
        bigValues.put("1d", Boolean.TRUE);
        bigValues.put("5d", Boolean.TRUE);

        for (FieldModelData field : this.getStore().getModels())
        {
            String name = field.getName();
            if (bigValues.get(name) != null)
                field.set("displayName", "<span style=\"font-size : 18px; font-weight: bold;\">" + name + "</span>");
            else
                field.set("displayName", "<span style=\"font-size : 12px;\">" + name + "</span>");

        }
        this.setTemplate(this.getXTemplate());
    }

    /**
     * @return Combo box values display style.
     */
    public native String getXTemplate() /*-{
                                              return  [ 
                                              '<tpl for=".">', 
                                              '<div class="x-combo-list-item"> {displayName} </div>', 
                                              '</tpl>' 
                                              ].join("");
                                              }-*/;
}
