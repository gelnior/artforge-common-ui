package fr.hd3d.common.ui.client.widget.identitysheet.view;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.widget.custom.Portal;


/**
 * Identity sheet portal
 * 
 * @author HD3D
 */
public class IdentitySheetPortal extends Portal
{
    /** Default portlet that displays preview image. */
    private final PreviewPortlet previewPortlet = new PreviewPortlet();

    /** Index of column on which adding portlet. */
    protected int ind = 0;
    /** Portlet displayed inside the portal. */
    protected List<IdentitySheetPortlet> portlets = new ArrayList<IdentitySheetPortlet>();

    protected int nbColumns = 0;

    /**
     * Default Constructor.
     * 
     * @param nbCol
     */
    public IdentitySheetPortal(int nbCol)
    {
        super(nbCol);
        this.nbColumns = nbCol;

        for (int i = 0; i < this.nbColumns; i++)
        {
            double width = (1.0 / new Float(nbCol));
            setColumnWidth(i, width);
        }

        this.add(this.previewPortlet, ind);
        this.previewPortlet.setContextMenu();
    }

    public List<IdentitySheetPortlet> getPortlets()
    {
        return portlets;
    }

    /**
     * @return Number of portlets inside the portal.
     */
    public int getNbPortlets(int column)
    {
        return portlets.size();
    }

    public void addPortlet(IdentitySheetPortlet portlet)
    {
        this.add(portlet, this.ind);
        this.ind++;
        this.ind = (this.ind % this.nbColumns);

        this.portlets.add(portlet);
    }

    public void removePortlets()
    {
        for (IdentitySheetPortlet portlet : portlets)
        {
            portlet.clearAll();
            portlet.removeFromParent();
        }
        portlets.clear();
        ind = 0;

        // previewPortlet.removeFromParent();
    }

    public void showPortlets()
    {
        ind = 0;
        // this.add(previewPortlet, ind);
        this.ind++;
        for (IdentitySheetPortlet portlet : portlets)
        {
            add(portlet, ind);
            // portlet.showForm();
            this.ind++;
            this.ind = this.ind % nbColumns;
        }
    }

    public void updatePreview(Long id, String simpleClassName)
    {
        this.previewPortlet.updatePreview(id, simpleClassName);
    }

    public IdentitySheetPortlet getPortletById(Long id)
    {
        for (IdentitySheetPortlet portlet : portlets)
        {
            if (portlet.getIdentifiant().equals(id))
                return portlet;
        }
        return null;
    }

    public IdentitySheetItem getItemByQuery(String query)
    {
        for (IdentitySheetPortlet portlet : portlets)
        {
            for (IdentitySheetItem item : portlet.getIdentitySheetItems())
            {
                if (item.getQuery().equals(query))
                {
                    return item;
                }
            }
        }
        return null;
    }

    public void updatePreview(Long id)
    {
        this.previewPortlet.updatePreview(id);
    }

    public void refreshPreview(Long id)
    {
        if (id != null && this.previewPortlet.getModelId() != null
                && id.longValue() == this.previewPortlet.getModelId().longValue())
        {
            this.previewPortlet.updatePreview();
        }
    }

    public boolean isPreviewVisible()
    {
        return this.previewPortlet.isVisible();
    }

    public void setPreviewVisible(boolean visible)
    {
        this.previewPortlet.setVisible(visible);
    }

}
