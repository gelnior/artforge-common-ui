package fr.hd3d.common.ui.client.widget.identitysheet.view.renderers;

import java.util.ArrayList;
import java.util.List;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.data.BaseModelData;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.extjs.gxt.ui.client.widget.form.DateField;
import com.extjs.gxt.ui.client.widget.form.Field;
import com.extjs.gxt.ui.client.widget.form.NumberField;
import com.extjs.gxt.ui.client.widget.form.TextArea;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.google.gwt.json.client.JSONNumber;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;

import fr.hd3d.common.client.Editor;
import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.calendar.DateFormat;
import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.reader.RecordReader;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;
import fr.hd3d.common.ui.client.service.ServicesClass;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.callback.GetModelDataCallback;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.widget.AutoCompleteModelCombo;
import fr.hd3d.common.ui.client.widget.DurationField;
import fr.hd3d.common.ui.client.widget.FieldComboBox;
import fr.hd3d.common.ui.client.widget.constraint.IEditorProvider;
import fr.hd3d.common.ui.client.widget.field.HtmlTextArea;


/**
 * Provides base field for data edition inside identity widget.
 * 
 * @author HD3D
 */
public class IdentitySheetEditorProvider implements IEditorProvider
{
    public Field<?> getEditor(String type, BaseModelData itemModelData)
    {
        return this.getEditor(type, null, itemModelData);
    }

    @SuppressWarnings("unchecked")
    public Field<?> getEditor(String editorName, final Object value, BaseModelData itemModelData)
    {
        Field<?> valueField = null;
        if (Editor.SHORT_TEXT.equals(editorName))
        {
            TextField<String> field = new TextField<String>();
            if (value != null && value instanceof String)
                field.setValue((String) value);
            valueField = field;
        }
        else if (Editor.LIST.equals(editorName))
        {
            final FieldComboBox comboBox = new FieldComboBox();
            List<String> values = itemModelData.get(ItemModelData.LIST_VALUES_FIELD);
            if (values != null)
            {
                int index = 0;
                for (String listValue : values)
                {
                    comboBox.addField(index++, listValue, listValue);
                }
            }
            else
            {
                ItemModelData item = new ItemModelData();
                item.setId((Long) itemModelData.get(ItemModelData.ID_FIELD));
                item.setDefaultPath((String) itemModelData.get(ItemModelData.DEFAULT_PATH_FIELD));

                item.refresh(new GetModelDataCallback(item) {
                    @Override
                    public void onSuccess(Request request, Response response)
                    {
                        super.onSuccess(request, response);
                        List<String> values = record.get(ItemModelData.LIST_VALUES_FIELD);
                        if (values != null)
                        {
                            int index = 0;
                            for (String listValue : values)
                            {
                                comboBox.addField(index++, listValue, listValue);
                            }
                        }
                        if (!"".equals(value))
                        {
                            FieldModelData defaultValueModelData = comboBox.getStore().findModel(
                                    FieldModelData.VALUE_FIELD, value);
                            if (defaultValueModelData != null)
                            {
                                comboBox.setValue(defaultValueModelData);
                            }
                        }
                    }
                });
            }
            if (!"".equals(value))
            {
                FieldModelData defaultValueModelData = comboBox.getStore().findModel(FieldModelData.VALUE_FIELD, value);
                if (defaultValueModelData != null)
                {
                    comboBox.setValue(defaultValueModelData);
                }
            }
            valueField = comboBox;
        }
        else if (Editor.LONG_TEXT.equals(editorName))
        {
            HtmlTextArea field = new HtmlTextArea();
            if (value != null && value instanceof String)
                field.setValue((String) value);
            valueField = field;
        }
        else if (Editor.FLOAT.equals(editorName))
        {
            TextField<Float> field = new TextField<Float>();
            if (value != null)
                field.setValue(((Double) value).floatValue());
            valueField = field;
        }
        else if (Editor.INT.equals(editorName))
        {
            NumberField field = new NumberField();
            if (value != null)
            {
                if (value instanceof String)
                    field.setValue(Integer.parseInt((String) value));
                else
                    field.setValue(((Double) value).longValue());
            }
            valueField = field;
        }
        else if (Editor.RATING.equals(editorName))
        {
            RatingField field = new RatingField();
            if (value != null)
                field.setValue(((Double) value).intValue());
            valueField = field;
        }
        else if (Editor.BOOLEAN.equals(editorName))
        {
            CheckBox field = new CheckBox();
            if (value != null && value instanceof Boolean)
                field.setValue((Boolean) value);
            else if (value != null && value instanceof Double)
                field.setValue(FieldUtils.doubleToBoolean((Double) value));
            valueField = field;
        }
        else if (Editor.DATE.equals(editorName))
        {
            DateField field = new DateField();
            if (value != null)
            {
                try
                {
                    field.setValue(DateFormat.TIMESTAMP_FORMAT.parse((String) value));
                }
                catch (Exception e)
                {
                    field.setValue(DateFormat.DATE_TIME.parse((String) value));
                }
            }
            valueField = field;
        }
        else if (Editor.DURATION.equals(editorName))
        {
            DurationField field = new DurationField();
            if (value != null)
                field.setValue(((Number) value));
            return field;
        }
        else if (ServicesClass.contains(editorName.toLowerCase()))
        {
            if (value != null && value instanceof ArrayList<?>)
            {
                TextArea field = new TextArea();
                List<JSONObject> values = (ArrayList<JSONObject>) value;
                StringBuffer sb = new StringBuffer();
                for (int i = 0, c = values.size(); i < c; i++)
                {
                    if (i > 0)
                    {
                        sb.append(", ");
                    }
                    JSONString name = (JSONString) values.get(i).get(RecordModelData.NAME_FIELD);
                    if (name != null)
                        sb.append(name.stringValue());
                }
                field.setValue(sb.toString());
                valueField = field;
            }
            else
            {
                AutoCompleteModelCombo<RecordModelData> combo = new AutoCompleteModelCombo<RecordModelData>(
                        new RecordReader());
                combo.setPath(ServicesPath.getPath(editorName));
                if (Editor.PERSON.equals(editorName.toLowerCase()))
                {
                    combo.setOrderColumn(PersonModelData.PERSON_FIRST_NAME_FIELD);
                    combo.setFilterColumn(PersonModelData.PERSON_FIRST_NAME_FIELD);
                }
                if (value != null && value instanceof JSONObject)
                {
                    JSONNumber id = (JSONNumber) ((JSONObject) value).get(Hd3dModelData.ID_FIELD);
                    JSONString name = (JSONString) ((JSONObject) value).get(RecordModelData.NAME_FIELD);
                    JSONString className = (JSONString) ((JSONObject) value).get(RecordModelData.CLASS_NAME_FIELD);
                    JSONString defaultPath = (JSONString) ((JSONObject) value).get(RecordModelData.DEFAULT_PATH_FIELD);
                    Double idd = id.doubleValue();
                    RecordModelData record = new RecordModelData(idd.longValue(), name.stringValue());
                    record.setClassName(className.stringValue());
                    record.setDefaultPath(defaultPath.stringValue());
                    combo.setValue(record);
                }
                valueField = combo;
            }
        }
        return valueField;
    }

    public String getItemConstraintColumnName(String name)
    {
        // TODO
        return null;
    }

    public Field<?> getEditor(String type, EConstraintOperator operator, BaseModelData baseModel)
    {
        return this.getEditor(type, (Object) null, baseModel);
    }
}
