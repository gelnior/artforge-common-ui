package fr.hd3d.common.ui.client.widget.identitysheet.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Stack;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.calendar.DateFormat;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.StepModelData;
import fr.hd3d.common.ui.client.modeldata.technical.DynValueModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ItemGroupModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.service.BulkRequests;
import fr.hd3d.common.ui.client.service.ServicesClass;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;
import fr.hd3d.common.ui.client.widget.explorer.model.modeldata.IColumn;
import fr.hd3d.common.ui.client.widget.explorer.model.modeldata.IColumnGroup;
import fr.hd3d.common.ui.client.widget.identitysheet.IdentitySheetWidget;
import fr.hd3d.common.ui.client.widget.identitysheet.events.IdentitySheetEvents;
import fr.hd3d.common.ui.client.widget.identitysheet.model.IdentitySheetModel;
import fr.hd3d.common.ui.client.widget.identitysheet.view.IdentitySheetItem;
import fr.hd3d.common.ui.client.widget.identitysheet.view.renderers.StepField;


/**
 * IdentitySheetController controller handles identity sheet events.
 * 
 * @author HD3D
 */
public class IdentitySheetController extends MaskableController
{
    /** View that displays identity sheet widgets. */
    private final IdentitySheetWidget view;
    /** Model that handles identity sheet data. */
    private final IdentitySheetModel model;

    /** True if identity sheet is currently refreshing. */
    private boolean isRefreshing;

    /**
     * Default constructor : register view, model and supported events.
     * 
     * @param view
     *            View that displays identity sheet widgets.
     * @param model
     *            Model that handles identity sheet data.
     */
    public IdentitySheetController(IdentitySheetWidget view, IdentitySheetModel model)
    {
        this.view = view;
        this.model = model;

        this.registerEvents();
    }

    /**
     * Say that only data should be refreshed.
     */
    public void setRefreshingOn()
    {
        this.isRefreshing = true;
    }

    /**
     * Say that portal should be fully rebuild.
     */
    public void setRefreshingOff()
    {
        this.isRefreshing = false;
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();
        if (type == IdentitySheetEvents.LOAD_SHEET)
        {
            this.onLoadSheet((SheetModelData) event.getData());
        }
        else if (type == IdentitySheetEvents.DATAS_LOADED)
        {
            this.onDatasLoaded();
        }
        else if (type == IdentitySheetEvents.DYNMETADATA_LOADED)
        {
            this.onDynDatasLoaded();
        }
        else if (type == IdentitySheetEvents.SAVE_CLICKED)
        {
            this.onSaveDatas();
        }
        else if (type == IdentitySheetEvents.DYNMETADATA_SAVED)
        {
            this.onDynMetaDataSaved();
        }
        else if (type == IdentitySheetEvents.REFRESH_CLICKED)
        {
            this.onRefreshClicked();
        }
        else if (type == ExplorerEvents.THUMBNAIL_UPLOADED)
        {
            this.onThumbnailUploaded(event);
        }
        else if (type == CommonEvents.ERROR)
        {
            this.view.hideStatus();
        }
        else
        {
            this.forwardToChild(event);
        }
    }

    private void onRefreshClicked()
    {
        this.view.showRefreshing();
        this.setRefreshingOn();
        this.loadDatas();
    }

    private void onThumbnailUploaded(AppEvent event)
    {
        Hd3dModelData record = event.getData();
        this.view.refreshPreview(record.getId());
    }

    /** Load a sheet, then load data. */
    public void onLoadSheet(SheetModelData sheet)
    {
        if (sheet != null)
        {
            model.clear();
            model.setCurrentSheet(sheet);
            model.setSimpleClassName(sheet.getBoundClassName());

            this.loadPortlets();
        }
    }

    /**
     * Load data corresponding to selected sheet and select records.
     */
    public void loadDatas()
    {
        model.loadPortletsDatas();
    }

    /**
     * Store sheet info inside model then load data.
     */
    private void loadPortlets()
    {
        if (model.getCurrentSheet() != null)
        {
            if (!(this.model.getCurrentSheet().getColumnGroups() == null))
            {
                List<ItemGroupModelData> groups = new ArrayList<ItemGroupModelData>();
                List<ItemModelData> items = new ArrayList<ItemModelData>();

                for (IColumnGroup group : this.model.getCurrentSheet().getColumnGroups())
                {
                    groups.add((ItemGroupModelData) group);
                }

                List<IColumn> columns = this.model.getCurrentSheet().getColumns();
                if (columns != null)
                {
                    for (IColumn column : columns)
                    {
                        items.add((ItemModelData) column);
                    }
                }

                this.model.setItemsStore(items);
                this.model.setPortletsStore(groups);
                this.model.getCurrentSheet().setGroupLoaded(true);
                this.loadDatas();
            }
        }
    }

    /**
     * When data are loaded, dynamic data values are loaded.
     */
    private void onDatasLoaded()
    {
        model.loadDynMetaData();
    }

    /**
     * When dynamic data are loaded, view is refreshed.
     */
    private void onDynDatasLoaded()
    {
        this.view.showTools();
        if (this.isRefreshing)
        {
            this.view.refreshData();
            this.view.hideStatus();
        }
        else
        {
            this.view.loadPortail();
            this.view.hideStatus();
        }
    }

    /**
     * When save button is clicked, a new object is created and filled with dirty fields from view. Then it is saved.
     * Once saving is finished, dynamic values are saved.
     */
    private void onSaveDatas()
    {
        final List<Hd3dModelData> dynValues = new ArrayList<Hd3dModelData>();
        final List<Hd3dModelData> stepsToUpdate = new ArrayList<Hd3dModelData>();
        final List<Hd3dModelData> stepsToCreate = new ArrayList<Hd3dModelData>();
        Hd3dModelData model = new Hd3dModelData();

        this.view.showSaving();

        model.setSimpleClassName(this.model.getSimpleClassName());
        model.setClassName(ServicesClass.getClass(model.getSimpleClassName()));
        model.setId(this.model.getDatasStore().getAt(0).getId());

        for (IdentitySheetItem item : this.view.getSheetItems().values())
        {
            String type = item.getAttributeType();
            boolean isDirty = item.isDirty();

            if (FieldUtils.isStaticAttribute(type) && isDirty)
            {
                model.set(item.getQuery(), item.getFieldValue());
                if (ServicesClass.contains(item.getQuery().toLowerCase()))
                    model.set(item.getQuery() + "Id", item.getFieldValue());

                if (this.view.isTitle(item.getQuery()))
                {
                    this.view.setBigTitle(item.getFieldValue());
                }
            }
            else if (FieldUtils.isDynAttribute(type) && isDirty)
            {
                DynValueModelData dynValue = new DynValueModelData();
                dynValue.setDynClassId(Long.parseLong(item.getQuery()));
                dynValue.setParentId(model.getId());
                dynValue.setParentType(this.model.getSimpleClassName());
                this.model.updateDynId(dynValue);

                Object value = item.getFieldValue();
                if (value instanceof Date)
                {
                    dynValue.setValue(DateFormat.TIMESTAMP_FORMAT.format((Date) value));
                }
                else if (value instanceof FieldModelData)
                {
                    dynValue.setValue(((FieldModelData) value).getValue().toString());
                }
                else if (value != null)
                {
                    dynValue.setValue(value.toString());
                }

                dynValues.add(dynValue);
            }
            else if (FieldUtils.isStep(item.getQuery()) && isDirty)
            {
                Long stepVal = (Long) ((StepField) item.getField()).getValue().getValue();
                StepModelData step = ((StepField) item.getField()).getStep();
                if (step == null)
                {
                    String taskTypeIdString = item.getParameter();
                    StepModelData stepToCreate = new StepModelData(model, taskTypeIdString, stepVal);

                    this.model.set("id_" + item.getId(), stepToCreate);
                    stepsToCreate.add(stepToCreate);
                }
                else
                {
                    step.setEstimatedDuration(stepVal);
                    stepsToUpdate.add(step);
                }
            }
        }

        model.save(new BaseCallback() {
            @Override
            protected void onSuccess(Request request, Response response)
            {
                Stack<List<? extends Hd3dModelData>> stack = new Stack<List<? extends Hd3dModelData>>();
                stack.push(stepsToCreate);
                stack.push(stepsToUpdate);
                stack.push(dynValues);
                BulkRequests.saveLists(stack, new AppEvent(IdentitySheetEvents.DYNMETADATA_SAVED));
            }
        });
    }

    /** When dynamic meta data are saved, all fields marked as dirty are unmarked. */
    private void onDynMetaDataSaved()
    {
        for (IdentitySheetItem item : this.view.getSheetItems().values())
        {
            if (item.isDirty())
                item.setNotDirty();
        }
        this.view.hideStatus();
    }

    /** Register all events the controller can handle. */
    private void registerEvents()
    {
        this.registerEventTypes(IdentitySheetEvents.LOAD_SHEET);
        this.registerEventTypes(IdentitySheetEvents.DATAS_LOADED);
        this.registerEventTypes(IdentitySheetEvents.DYNMETADATA_LOADED);
        this.registerEventTypes(IdentitySheetEvents.DYNMETADATA_SAVED);
        this.registerEventTypes(IdentitySheetEvents.SAVE_CLICKED);
        this.registerEventTypes(ExplorerEvents.THUMBNAIL_UPLOADED);
        this.registerEventTypes(IdentitySheetEvents.REFRESH_CLICKED);
    }
}
