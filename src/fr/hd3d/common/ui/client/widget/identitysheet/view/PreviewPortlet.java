package fr.hd3d.common.ui.client.widget.identitysheet.view;

import com.extjs.gxt.ui.client.event.MenuEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.custom.Portlet;
import com.extjs.gxt.ui.client.widget.menu.MenuItem;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Image;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.widget.EasyMenu;
import fr.hd3d.common.ui.client.widget.thumbnaileditor.ThumbnailEditorDisplayer;
import fr.hd3d.common.ui.client.widget.thumbnaileditor.ThumbnailPath;


/**
 * Preview portlet displays selected component preview image.
 * 
 * @author HD3D
 */
public class PreviewPortlet extends Portlet
{
    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    /** The displayed image. */
    protected Image preview = new Image(RestRequestHandlerSingleton.getInstance().getServicesUrl()
            + ServicesPath.PREVIEWS);
    /** Simple class of the selected record. */
    protected String simpleClassName = "";

    private Long modelId;

    /**
     * Default constructor.
     */
    public PreviewPortlet()
    {
        this.setFrame(true);
        this.setAutoHeight(true);
        LayoutContainer container = new LayoutContainer();
        container.setStyleAttribute("text-align", "center");
        container.setBorders(true);
        container.add(preview);
        this.add(container);

        this.setHeading(CONSTANTS.Preview());
    }

    public Long getModelId()
    {
        return this.modelId;
    }

    public void setContextMenu()
    {
        EasyMenu menu = new EasyMenu();

        MenuItem item = new MenuItem("Upload new preview");
        menu.add(item);
        item.addSelectionListener(new SelectionListener<MenuEvent>() {
            @Override
            public void componentSelected(MenuEvent ce)
            {
                Hd3dModelData model = new Hd3dModelData();
                model.setId(modelId);
                model.setSimpleClassName(simpleClassName);
                ThumbnailEditorDisplayer.display(model);
            }
        });

        this.setContextMenu(menu);
    }

    /**
     * Update displayed preview with the image preview of instance of which ID is equal to <i>id</i> and entity name is
     * equal to <i>simpleClassName</i>.
     * 
     * @param id
     *            The id of object of which preview is displayed.
     * @param simpleClassName
     *            The simple class name of object of which preview is displayed.
     */
    public void updatePreview(Long id, String simpleClassName)
    {
        this.simpleClassName = simpleClassName;
        this.modelId = id;
        this.updatePreview();
    }

    /**
     * Update displayed preview with the image preview of instance of which ID is equal to <i>id</i> and entity name is
     * equal to <i>simpleClassName</i>.
     * 
     * @param id
     *            The id of object of which preview is displayed.
     */
    public void updatePreview(Long id)
    {
        this.modelId = id;
        this.updatePreview();
    }

    /**
     * Update displayed preview with the image preview of instance of which ID is equal to <i>id</i>. Right simple class
     * name should be set before updating.
     */
    public void updatePreview()
    {
        Hd3dModelData model = new Hd3dModelData();
        model.setId(this.modelId);
        model.setSimpleClassName(simpleClassName);
        model.set(ThumbnailPath.CHANGED_MARKER, Boolean.TRUE);
        String path = ThumbnailPath.getPath(model, true);

        preview.setUrl(path);
        this.repaint();
    }

}
