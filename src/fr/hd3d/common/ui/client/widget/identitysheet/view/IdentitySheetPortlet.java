package fr.hd3d.common.ui.client.widget.identitysheet.view;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.widget.custom.Portlet;
import com.extjs.gxt.ui.client.widget.form.Field;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.FormData;
import com.extjs.gxt.ui.client.widget.layout.FormLayout;


/**
 * Identity Sheet portlet. Portlet displays loaded record data from a sheet group.
 * 
 * @author HD3D
 */
public class IdentitySheetPortlet extends Portlet
{
    /** Portlet width */
    public final static Integer WIDTH = new Integer(250);
    /** Portlet id */
    private final long id;

    /** Form Panel used to display data into fields. */
    protected FormPanel form = new FormPanel();
    /** Items contained in the portlet. */
    protected List<IdentitySheetItem> items = new ArrayList<IdentitySheetItem>();

    /**
     * Default Constructor
     * 
     * @param id
     *            Portlet id.
     * @param title
     *            Portlet title.
     */
    public IdentitySheetPortlet(Long id, String title)
    {
        this.id = id;
        this.setHeading(title);

        FormLayout layout = new FormLayout();

        this.setLayout(new FitLayout());
        this.setFrame(true);
        this.setBodyBorder(true);

        this.form.setAutoHeight(true);
        this.form.setLayout(layout);
        this.form.setHeaderVisible(false);
        this.form.setBodyBorder(true);
        this.form.setBorders(true);
        this.add(form);
    }

    /**
     * @return Portlet ID.
     */
    public Long getIdentifiant()
    {
        return id;
    }

    /**
     * Add a new field to the form of the portlet.
     * 
     * @param field
     *            Field to add.
     */
    public void addField(Field<?> field)
    {
        this.form.add(field, new FormData("0"));
        // this.layout();
    }

    /**
     * @return items contained in the portlet.
     */
    public List<IdentitySheetItem> getIdentitySheetItems()
    {
        return items;
    }

    /**
     * Add a new item to the portlet (add the associated field to the portlet form).
     * 
     * @param itemField
     * 
     */
    public void addItem(IdentitySheetItem itemField)
    {
        this.addField(itemField.getField());
        this.items.add(itemField);
    }

    /**
     * Remove all items inside form.
     */
    public void clearItems()
    {
        for (IdentitySheetItem item : items)
        {
            this.remove(item.getField());
        }
        this.items.clear();
    }

    /**
     * Remove all items inside form and remove forms.
     */
    public void clearAll()
    {
        for (IdentitySheetItem item : items)
        {
            if (this.form.getItems().contains(item))
                this.form.remove(item.getField());
        }

        items.clear();
        try
        {
            this.remove(form);
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
    }
}
