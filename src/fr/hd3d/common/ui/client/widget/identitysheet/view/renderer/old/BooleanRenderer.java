package fr.hd3d.common.ui.client.widget.identitysheet.view.renderer.old;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;


public class BooleanRenderer<M extends ModelData> implements GridCellRenderer<M>
{

    public Object render(M model, String property, ColumnData config, int rowIndex, int colIndex, ListStore<M> store,
            Grid<M> grid)
    {
        CheckBox check = new CheckBox();
        if (model.get(property) != null)
        {

            Object objValue = model.get(property);
            if (objValue instanceof Double)
            {
                Long longValue = Math.round((Double) objValue);
                if (longValue == 1)
                {
                    model.set(property, true);
                }

                else
                {
                    check.setValue(false);
                }
            }
            else if (objValue instanceof String)
            {
                if (objValue.equals("true"))
                {
                    check.setValue(true);
                }

                else
                {
                    check.setValue(false);
                }
            }
            else if (objValue instanceof Boolean)
            {
                check.setValue((Boolean) objValue);
            }
        }
        else
        {
            check.setValue(false);
        }
        check.setBorders(true);
        check.setHeight(18);
        check.setBorders(true);
        check.setWidth(20);

        return check;
    }
}
