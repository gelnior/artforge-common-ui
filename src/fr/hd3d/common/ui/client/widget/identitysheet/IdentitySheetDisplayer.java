package fr.hd3d.common.ui.client.widget.identitysheet;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;

import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;


public class IdentitySheetDisplayer
{
    private static Dialog identityDialog = null;
    private static IdentitySheetWidget identity = null;

    public static void displayIdentity(SheetModelData sheet, Long id)
    {
        List<String> ids = new ArrayList<String>();
        ids.add(id.toString());
        displayIdentity(sheet, ids);
    }

    public static void displayIdentity(SheetModelData sheet, List<String> ids)
    {
        getIdentity().loadObjectEdit(sheet, ids);
        getDialog().show();
    }

    private static Dialog getDialog()
    {
        if (identityDialog == null)
        {
            identityDialog = new Dialog();
            identityDialog.setSize(600, 500);
            identityDialog.setHeading("Informations");

            identityDialog.setLayout(new FitLayout());
            identityDialog.setButtons("");

            identityDialog.add(getIdentity());
        }

        return identityDialog;
    }

    private static IdentitySheetWidget getIdentity()
    {
        if (identity == null)
        {
            identity = new IdentitySheetWidget();
        }
        return identity;
    }
}
