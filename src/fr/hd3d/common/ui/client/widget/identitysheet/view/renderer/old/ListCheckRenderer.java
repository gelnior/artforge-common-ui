package fr.hd3d.common.ui.client.widget.identitysheet.view.renderer.old;

import java.util.Iterator;
import java.util.List;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.store.Record;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.extjs.gxt.ui.client.widget.form.CheckBoxGroup;
import com.extjs.gxt.ui.client.widget.form.Field;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONValue;

import fr.hd3d.common.ui.client.widget.identitysheet.IdentitySheetWidget;


public class ListCheckRenderer<M extends ModelData> implements GridCellRenderer<M>
{
    private CheckBoxGroup checkBoxGroup;
    private List<JSONObject> jsonGroup;
    private String type;
    private int editType;

    public Object render(M model, final String property, ColumnData config, int rowIndex, int colIndex,
            ListStore<M> store, Grid<M> grid)
    {
        Object obj = model.get(property);

        // this.switchMode(true);
        this.removeAllListenersCheckBoxGroup();
        this.removeAllSelCheckBoxGroup();

        if (obj instanceof List<?>)
        {
            @SuppressWarnings("unchecked")
            List<JSONValue> groups = (List<JSONValue>) obj;
            for (Iterator<JSONValue> iterator = groups.iterator(); iterator.hasNext();)
            {
                Object value = iterator.next();

                JSONObject json = (JSONObject) value;
                JSONValue name = json.get("name");
                String stringValue = name.isString().stringValue();

                CheckBox checkBox = getCheckBox(stringValue);
                checkBox.setValue(true);
                checkBox.removeAllListeners();
                this.addListener(checkBox, model, property, store, json);
            }
        }

        this.addListenersCheckBoxGroup(model, property, store);
        // this.initSwitchMode();
        return checkBoxGroup;
    }

    // public String render(GroupColumnData data)
    // {
    // // TODO Auto-generated method stub
    // return null;
    // }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public int getEditType()
    {
        return editType;
    }

    public void setEditType(int editType)
    {
        this.editType = editType;
    }

    public void setCheckBoxGroup(CheckBoxGroup checkBoxGroup)
    {
        this.checkBoxGroup = checkBoxGroup;
        switchMode(true);
        // removeAllSelCheckBoxGroup();
    }

    public void removeAllSelCheckBoxGroup()
    {
        List<Field<?>> lst = this.checkBoxGroup.getAll();
        for (Field<?> f : lst)
        {
            CheckBox checkBox = (CheckBox) f;
            checkBox.setValue(false);
        }
    }

    public void setJsonGroup(List<JSONObject> jsonGroup)
    {
        this.jsonGroup = jsonGroup;
    }

    public void addListenersCheckBoxGroup(M model, final String property, ListStore<M> store)
    {
        List<Field<?>> lstField = this.checkBoxGroup.getAll();
        int i = 0;
        for (Field<?> f : lstField)
        {
            CheckBox checkBox = (CheckBox) f;
            if (!checkBox.getValue())
            {
                JSONObject jsonObj = this.jsonGroup.get(i);
                checkBox.removeAllListeners();
                this.addListener(checkBox, model, property, store, jsonObj);
            }
            i++;
        }
    }

    public void removeAllListenersCheckBoxGroup()
    {
        List<Field<?>> lstField = this.checkBoxGroup.getAll();
        for (Field<?> f : lstField)
        {
            CheckBox checkBox = (CheckBox) f;
            checkBox.removeAllListeners();
        }
    }

    public void addListener(CheckBox checkBox, M model, final String property, ListStore<M> store,
            final JSONObject jsonObj)
    {
        final Record record = store.getRecord(model);
        checkBox.addListener(Events.Change, new Listener<ComponentEvent>() {
            @SuppressWarnings("unchecked")
            public void handleEvent(ComponentEvent ce)
            {
                List<JSONObject> lst = (List<JSONObject>) record.get(property);

                CheckBox check = (CheckBox) ce.getComponent();
                Boolean res = check.getValue();
                if (res)
                {
                    // Add
                    if (!lst.contains(jsonObj))
                    {
                        lst.add(jsonObj);
                    }
                }
                else
                {
                    // Del
                    lst.remove(jsonObj);
                }
                // record.set(property, newLst);
                record.setDirty(true);
            }
        });
    }

    private CheckBox getCheckBox(String name)
    {
        List<Field<?>> lst = this.checkBoxGroup.getAll();
        for (Field<?> f : lst)
        {
            CheckBox checkBox = (CheckBox) f;

            if (name.equals(checkBox.getBoxLabel()))
            {
                return checkBox;
            }
        }
        return null;
    }

    public void switchMode(Boolean editable)
    {
        List<Field<?>> lst = this.checkBoxGroup.getAll();
        for (Field<?> f : lst)
        {
            CheckBox checkBox = (CheckBox) f;
            checkBox.setReadOnly(!editable);
        }
    }

    public void initSwitchMode()
    {

        if (editType == IdentitySheetWidget.SIMPLE_EDITION_MODE)
        {
            this.switchMode(true);
        }
        else if (editType == IdentitySheetWidget.MULTI_EDITION_MODE)
        {
            this.removeAllSelCheckBoxGroup();
            this.switchMode(true);
        }
        else
        {
            this.switchMode(false);
            //
            // if (type == IdentitySheetWidget.UNEDITABLE && type == IdentitySheetWidget.MULTI_EDITION_MODE_UNEDITABLE)
            // {
            // this.disable();
            // }
        }
    }
}
