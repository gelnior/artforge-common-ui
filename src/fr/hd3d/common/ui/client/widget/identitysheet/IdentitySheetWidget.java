package fr.hd3d.common.ui.client.widget.identitysheet;

import java.util.List;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.data.BaseModelData;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.toolbar.LabelToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.SeparatorToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.ToolBar;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.client.Renderer;
import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.technical.DynValueModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ItemGroupModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetDataModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.widget.SavingStatus;
import fr.hd3d.common.ui.client.widget.ToolBarButton;
import fr.hd3d.common.ui.client.widget.identitysheet.controller.IdentitySheetController;
import fr.hd3d.common.ui.client.widget.identitysheet.events.IdentitySheetEvents;
import fr.hd3d.common.ui.client.widget.identitysheet.model.IdentitySheetModel;
import fr.hd3d.common.ui.client.widget.identitysheet.view.IdentitySheetItem;
import fr.hd3d.common.ui.client.widget.identitysheet.view.IdentitySheetPortal;
import fr.hd3d.common.ui.client.widget.identitysheet.view.IdentitySheetPortlet;
import fr.hd3d.common.ui.client.widget.identitysheet.view.renderers.IdentitySheetEditorProvider;


/**
 * Widget that display data of a specific work object or person. To know which data should be displayed the identity
 * sheet is based on explorer sheet
 * 
 * @author HD3D
 */
public class IdentitySheetWidget extends ContentPanel
{
    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    public static final int SIMPLE_EDITION_MODE = 1;
    public static final int MULTI_EDITION_MODE = 2;

    public static final int DEFAULT_COLUMN_NUMBER = 2;

    public static IdentitySheetEditorProvider itemInfoProvider;

    public static IdentitySheetEditorProvider getItemInfoProvider()
    {
        if (itemInfoProvider == null)
            itemInfoProvider = new IdentitySheetEditorProvider();
        return itemInfoProvider;
    }

    /** Data are displayed in a portal layout. */
    private final IdentitySheetPortal portal;
    /** Toolbar displays name and save button. */
    private final ToolBar toolBar = new ToolBar();
    /** Widget title. */
    private final LabelToolItem title = new LabelToolItem();
    /** Save button for toolbar. */
    private final IdentitySheetSaveToolItem saveButton = new IdentitySheetSaveToolItem();
    private final ToolBarButton refreshButton = new ToolBarButton(Hd3dImages.getRefreshIcon(), CONSTANTS.Refresh(),
            IdentitySheetEvents.REFRESH_CLICKED);

    /** Model that handles identity sheet data. */
    private final IdentitySheetModel model = new IdentitySheetModel();
    /** Controller that handles identity sheet events. */
    private final IdentitySheetController controller = new IdentitySheetController(this, model);
    /** True if identity sheet widget is active. */
    private boolean active;

    /** Simple or multi-edition mode. */
    private int editionMode;

    /** Current Object ID */
    private Long currentId;
    /** Items map for fast data refresh. */
    private final FastMap<IdentitySheetItem> items = new FastMap<IdentitySheetItem>();
    /** Tool item showing when data are saving or not. */
    private final SavingStatus status = new SavingStatus();

    private final SeparatorToolItem separator = new SeparatorToolItem();

    /**
     * Default Constructor
     */
    public IdentitySheetWidget()
    {
        this(DEFAULT_COLUMN_NUMBER, SIMPLE_EDITION_MODE, true);
    }

    /**
     * @param editionMode
     *            Simple or multi-edition mode.
     * @param active
     */
    public IdentitySheetWidget(int editionMode, boolean active)
    {
        this(DEFAULT_COLUMN_NUMBER, editionMode, active);
    }

    public void configureForCurrentProject()
    {
        this.model.configureForCurrentProject();
    }

    /**
     * Default Constructor
     * 
     * @param nbCol
     *            Number of columns of the portal.
     * @param editionMode
     *            Simple or multi-edition mode.
     * @param active
     */
    public IdentitySheetWidget(int nbCol, int editionMode, boolean active)
    {
        this.editionMode = editionMode;
        this.active = active;

        this.portal = new IdentitySheetPortal(nbCol);

        this.setController();
        this.createPortal();
        this.createToolbar();
        this.setStyles();

        this.hideWidgets();
    }

    private void hideWidgets()
    {
        this.saveButton.setVisible(false);
        this.separator.setVisible(false);
        this.refreshButton.setVisible(false);
        this.portal.setPreviewVisible(false);
    }

    /**
     * Initializes displayed data.
     */
    public void initIdentityData()
    {
        this.controller.handleEvent(new AppEvent(IdentitySheetEvents.INIT_DATA));
    }

    /**
     * Get identity sheet controller.
     */
    public IdentitySheetController getController()
    {
        return this.controller;
    }

    /**
     * Create portal layout.
     */
    public void createPortal()
    {
        this.portal.setBorders(false);
        this.add(portal);
        this.setBorders(false);
        this.setBodyBorder(false);
    }

    /**
     * Create identity sheet toolbar.
     */
    public ToolBar createToolbar()
    {
        // toolBar.add(title);
        // toolBar.add(separator);
        // toolBar.add(refreshButton);
        // toolBar.add(saveButton);
        // toolBar.add(status);
        // status.hide();
        //
        toolBar.setStyleAttribute("padding", "5px");
        this.setTopComponent(toolBar);

        return toolBar;
    }

    /**
     * Load structure and data for portal.
     */
    public void loadPortail()
    {
        this.portal.removePortlets();
        this.createPortlets();
        this.createItems();
        this.portal.showPortlets();
    }

    /**
     * Create portlets : add one portlet for each sheet group and add the preview portlet to display an image of the
     * selected work object/person.
     */
    private void createPortlets()
    {
        SheetModelData sheet = this.model.getCurrentSheet();

        this.portal.removePortlets();
        this.portal.updatePreview(this.currentId, sheet.getBoundClassName());

        if (this.model.getPortletsStore() != null)
        {
            for (ItemGroupModelData group : this.model.getPortletsStore().getModels())
            {
                IdentitySheetPortlet portlet = new IdentitySheetPortlet(group.getId(), group.getName());
                portal.addPortlet(portlet);
            }
        }
    }

    private void createItems()
    {
        items.clear();
        for (ItemModelData item : this.model.getItemsStore().getModels())
        {
            IdentitySheetPortlet portlet = portal.getPortletById(item.getItemGroupId());
            if (portlet == null)
                Logger.warn("An item is created but has no portlet corresponding to the item group by which it is owned.");
            SheetDataModelData obj = this.model.getDatasStore().getAt(0);

            if (obj != null)
            {
                Object val = null;
                List<BaseModelData> values = obj.getValues();
                for (BaseModelData value : values)
                {
                    Long id = value.get(Hd3dModelData.ID_FIELD);

                    String renderer = item.getRenderer();
                    if (item.getId().longValue() == id.longValue() && !renderer.startsWith(Renderer.THUMBNAIL))
                    {
                        val = value.get(FieldModelData.VALUE_FIELD);

                        if (this.isTitle(item.getQuery()))
                        {
                            this.setBigTitle(val);
                        }
                        IdentitySheetItem itemField = new IdentitySheetItem(editionMode, item, val);
                        if (portlet != null)
                            portlet.addItem(itemField);

                        items.put(id.toString(), itemField);
                    }
                }
            }
            else
            {
                title.setLabel("");
            }

        }
    }

    /**
     * Refresh item data.
     */
    public void refreshData()
    {
        SheetDataModelData obj = this.model.getDatasStore().getAt(0);
        if (obj != null)
        {
            List<BaseModelData> values = obj.getValues();
            for (BaseModelData value : values)
            {
                IdentitySheetItem item = items.get(value.get(Hd3dModelData.ID_FIELD).toString());
                Object val = value.get(FieldModelData.VALUE_FIELD);

                if (item != null)
                {
                    if (this.isTitle(item.getQuery()))
                    {
                        this.setBigTitle(val);
                    }

                    item.setFieldValue(val);
                }
            }

            this.portal.updatePreview(obj.getId());
        }

    }

    public void setBigTitle(Object val)
    {
        String stringValue = "<div style= 'font-weight: bold; font-size: 20px;'>" + val.toString() + "</div>";
        title.setLabel(stringValue);

        this.toolBar.layout();
    }

    /**
     * Get edition mode.
     */
    public int getEditionMode()
    {
        return editionMode;
    }

    /**
     * Set edition mode.
     * 
     * @param editionMode
     *            The edition mode.
     */
    public void setMode(int editionMode)
    {
        this.editionMode = editionMode;
    }

    /**
     * Update portal data.
     */
    public void updateData()
    {
        model.loadPortletsDatas();
    }

    /**
     * @return True if field is active.
     */
    public boolean isActive()
    {
        return active;
    }

    /**
     * Activate or un-activate identity sheet.
     * 
     * @param active
     */
    public void setActive(boolean active)
    {
        this.active = active;
        if (active)
            this.controller.unMask();
        else
            this.controller.mask();
    }

    /**
     * @return Sheet on which widget is based to display data.
     */
    public SheetModelData getSheetSelected()
    {
        return this.model.getCurrentSheet();
    }

    /**
     * Get the selected data.
     */
    public SheetDataModelData getDataSelected()
    {
        return this.model.getDatasStore().getAt(0);
    }

    /**
     * Load new object list in the identity sheet.
     * 
     * @param sheet
     *            Sheet on which widget will be based to display data.
     * @param ids
     *            Id list of objects to edit.
     */
    public void loadObjectEdit(SheetModelData sheet, List<String> ids)
    {
        this.currentId = Long.parseLong(ids.get(0));
        this.model.setConstraintDyn(new Constraint(EConstraintOperator.eq, DynValueModelData.PARENT_ID_FIELD,
                this.currentId));
        this.model.setConstraint(new Constraint(EConstraintOperator.eq, Hd3dModelData.ID_FIELD, this.currentId));

        if (sheet != model.getCurrentSheet() || getPortal().getPortlets().size() == 0)
        {
            this.model.setCurrentSheet(sheet);

            this.controller.setRefreshingOff();
            this.controller.onLoadSheet(sheet);
        }
        else
        {
            this.controller.setRefreshingOn();
            this.controller.loadDatas();
        }
    }

    /**
     * Load new object list in the identity sheet.
     * 
     * @param sheet
     *            Sheet on which widget will be based to display data.
     * @param id
     *            The object to display ID.
     */
    public void loadObject(SheetModelData sheet, Long id)
    {
        if (id == null || id.longValue() == 0L)
        {
            for (IdentitySheetItem item : this.items.values())
            {
                item.clearField();
            }
            this.saveButton.disable();
        }
        else
        {
            this.saveButton.enable();

            this.currentId = id;
            this.model.setConstraintDyn(new Constraint(EConstraintOperator.eq, DynValueModelData.PARENT_ID_FIELD,
                    this.currentId));
            this.model.setConstraint(new Constraint(EConstraintOperator.eq, Hd3dModelData.ID_FIELD, this.currentId));

            if (sheet != model.getCurrentSheet() || getPortal().getPortlets().size() == 0)
            {
                this.model.setCurrentSheet(sheet);

                this.controller.setRefreshingOff();
                this.controller.onLoadSheet(sheet);
            }
            else
            {
                this.controller.setRefreshingOn();
                this.controller.loadDatas();
            }
        }
    }

    public void reload()
    {
        this.controller.setRefreshingOff();
        this.controller.onLoadSheet(this.model.getCurrentSheet());
    }

    public void reloadDataForSheet(SheetModelData sheet)
    {
        this.model.setCurrentSheet(null);
        this.controller.setRefreshingOff();
        this.controller.onLoadSheet(sheet);
    }

    /**
     * @return Portal contained in the widget.
     */
    public IdentitySheetPortal getPortal()
    {
        return this.portal;
    }

    /**
     * Set widget styles : layout, borders...
     */
    private void setStyles()
    {
        this.setCollapsible(false);
        // this.setFrame(true);
        this.setBorders(false);
        this.setHeaderVisible(false);
        this.setLayout(new FitLayout());

        this.portal.setStyleAttribute("background-color", "#dfe8f6");
        this.setBodyBorder(true);
        this.setBorders(false);
    }

    /**
     * Register identity sheet controller to event dispatcher
     */
    private void setController()
    {
        EventDispatcher dispatcher = EventDispatcher.get();
        dispatcher.addController(controller);
    }

    /**
     * @return Items list represented as a map where the key is the id of the field, and the value is the field.
     */
    public FastMap<IdentitySheetItem> getSheetItems()
    {
        return items;
    }

    /**
     * @param field
     *            The field name to analyze.
     * @return True if query is a field that could be displayed as a title.
     */
    public boolean isTitle(String field)
    {
        return field.equals(ConstituentModelData.CONSTITUENT_LABEL) || field.equals(RecordModelData.NAME_FIELD)
                || field.equals(PersonModelData.PERSON_LAST_NAME_FIELD);
    }

    /**
     * Hide status (saving, refreshing...) indicator.
     */
    public void hideStatus()
    {
        status.clearStatus("");
        status.hide();
    }

    /**
     * Show saving indicator.
     */
    public void showSaving()
    {
        status.setBusy(CONSTANTS.Saving());
        status.show();
    }

    /**
     * Show refreshing indicator.
     */
    public void showRefreshing()
    {
        status.setBusy(CONSTANTS.Refresh());
        status.show();
    }

    public void refreshPreview(Long id)
    {
        this.portal.refreshPreview(id);
    }

    public void showTools()
    {
        if (!this.separator.isVisible())
            this.separator.setVisible(true);
        if (!this.saveButton.isVisible())
            this.saveButton.setVisible(true);
        if (!this.refreshButton.isVisible())
            this.refreshButton.setVisible(true);
        if (!this.portal.isPreviewVisible())
            this.portal.setPreviewVisible(true);
    }

    public void clearAll()
    {
        this.portal.removePortlets();
        this.title.setLabel("");
    }

    public void idle()
    {
        this.controller.mask();
    }

    public void unidle()
    {
        this.controller.unMask();
    }

    public ToolBar getToolbar()
    {
        return this.toolBar;
    }

}
