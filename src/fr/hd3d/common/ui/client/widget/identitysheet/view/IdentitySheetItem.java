package fr.hd3d.common.ui.client.widget.identitysheet.view;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.util.Util;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.extjs.gxt.ui.client.widget.form.ComboBox;
import com.extjs.gxt.ui.client.widget.form.DateField;
import com.extjs.gxt.ui.client.widget.form.Field;
import com.extjs.gxt.ui.client.widget.form.TextArea;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.google.gwt.json.client.JSONNumber;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;

import fr.hd3d.common.client.Editor;
import fr.hd3d.common.client.Renderer;
import fr.hd3d.common.ui.client.calendar.DateFormat;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.modeldata.production.StepModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;
import fr.hd3d.common.ui.client.service.ServicesClass;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.widget.ApprovalField;
import fr.hd3d.common.ui.client.widget.AutoCompleteModelCombo;
import fr.hd3d.common.ui.client.widget.FieldComboBox;
import fr.hd3d.common.ui.client.widget.field.HtmlTextArea;
import fr.hd3d.common.ui.client.widget.identitysheet.IdentitySheetWidget;
import fr.hd3d.common.ui.client.widget.identitysheet.events.IdentitySheetEvents;
import fr.hd3d.common.ui.client.widget.identitysheet.view.renderers.RatingField;
import fr.hd3d.common.ui.client.widget.identitysheet.view.renderers.StepField;


/**
 * Identity sheet field. Identity sheet field is linked to an item model date (sheet item).
 */
public class IdentitySheetItem
{
    private Object data;
    protected Field<?> field;
    protected boolean isDirty = false;
    private final ItemModelData item;

    public IdentitySheetItem(int editionMode, ItemModelData item, Object val)
    {
        this.data = val;
        this.item = item;

        populateItem();
    }

    public Field<?> getField()
    {
        if (Util.isEmptyString(this.field.getFieldLabel()))
            this.field.setFieldLabel(this.getTitle());
        return this.field;
    }

    public void populateItem()
    {
        Field<?> cellEditor = IdentitySheetWidget.getItemInfoProvider().getEditor(getEditor(), data, item);

        if (cellEditor == null)
        {
            cellEditor = IdentitySheetWidget.getItemInfoProvider().getEditor(getRenderer(), data, item);
            if (cellEditor == null)
            {
                TextField<String> tmpField = new TextField<String>();
                if (data != null)
                    tmpField.setValue(data.toString());
                this.field = tmpField;
            }
            else
            {
                this.field = cellEditor;
            }
            this.field.disable();
        }
        else
        {
            this.field = cellEditor;
        }

        this.field.addListener(Events.Change, new Listener<BaseEvent>() {

            public void handleEvent(BaseEvent be)
            {
                if (field.isDirty())
                {
                    if (field instanceof CheckBox)
                    {
                        field.addStyleName("x-grid3-dirty-cell");
                    }
                    else
                    {
                        field.addInputStyleName("x-grid3-dirty-cell");
                    }
                    EventDispatcher.forwardEvent(IdentitySheetEvents.SAVE_CLICKED);
                }
                else
                {
                    if (field instanceof CheckBox)
                    {
                        field.removeStyleName("x-grid3-dirty-cell");
                    }
                    else
                    {
                        field.removeInputStyleName("x-grid3-dirty-cell");
                    }
                }
            }
        });
    }

    public String getType()
    {
        return item.getType();
    }

    public String getTitle()
    {
        return item.getName();
    }

    public String getQuery()
    {
        return item.getQuery();
    }

    public Object getData()
    {
        return data;
    }

    public void setData(Object nvData)
    {
        data = nvData;
    }

    public Long getId()
    {
        return item.getId();
    }

    public String getRenderer()
    {
        return item.getRenderer();
    }

    public String getEditor()
    {
        return item.getEditor();
    }

    public String getAttributeType()
    {
        return item.getItemType();
    }

    public String getParameter()
    {
        return this.item.getParameter();
    }

    public boolean isDirty()
    {
        return this.field.isDirty();
    }

    public void setNotDirty()
    {
        Object value = this.field.getValue();
        this.setFieldValue(value);
    }

    @SuppressWarnings("unchecked")
    public Object getFieldValue()
    {
        if (field instanceof AutoCompleteModelCombo)
        {
            AutoCompleteModelCombo<RecordModelData> autoField = (AutoCompleteModelCombo<RecordModelData>) field;
            if (autoField.getValue() != null)
                return autoField.getValue().getId();
            else
                return null;
        }
        return this.field.getValue();
    }

    @SuppressWarnings("unchecked")
    public void setFieldValue(Object val)
    {
        String editor = this.getEditor();
        String renderer = this.getRenderer();
        if (Editor.SHORT_TEXT.equals(editor))
        {
            ((TextField<String>) field).setValue((String) val);
            ((TextField<String>) field).setOriginalValue((String) val);
        }
        else if (Editor.LONG_TEXT.equals(editor))
        {
            ((HtmlTextArea) field).setValue((String) val);
            ((HtmlTextArea) field).setOriginalValue((String) val);
        }
        else if (this.field instanceof RatingField)
        {
            ((RatingField) field).setValue((Double) val);
            ((RatingField) field).setOriginalValue((Double) val);
        }
        else if (Editor.LIST.equals(editor))
        {
            String value = null;
            if (val instanceof List)
            {
                List<String> values = (List<String>) val;
                if (values.size() > 0)
                {
                    value = values.get(0);
                }
            }
            else if (val instanceof String)
            {
                value = (String) val;
            }
            FieldModelData modelData = (FieldModelData) ((ComboBox<?>) this.field).getStore().findModel(
                    FieldModelData.VALUE_FIELD, value);
            if (modelData != null)
            {
                ((ComboBox<FieldModelData>) this.field).setValue(modelData);
                ((ComboBox<FieldModelData>) this.field).setOriginalValue(modelData);
            }
        }
        else if (Editor.FLOAT.equals(editor))
        {
            if (val instanceof Double)
            {
                ((TextField<Float>) field).setValue(((Double) val).floatValue());
                ((TextField<Float>) field).setOriginalValue(((Double) val).floatValue());
            }
            else if (val instanceof String)
            {
                ((TextField<Float>) field).setValue(Double.valueOf((String) val).floatValue());
                ((TextField<Float>) field).setOriginalValue(Double.valueOf((String) val).floatValue());
            }
        }
        else if (Editor.INT.equals(editor) || Editor.RATING.equals(editor))
        {
            if (val instanceof Double)
            {
                ((TextField<Integer>) field).setValue(((Double) val).intValue());
                ((TextField<Integer>) field).setOriginalValue(((Double) val).intValue());
            }
            else if (val instanceof String)
            {
                ((TextField<Integer>) field).setValue(Double.valueOf((String) val).intValue());
                ((TextField<Integer>) field).setOriginalValue(Double.valueOf((String) val).intValue());
            }
        }
        else if (Editor.DATE.equals(editor) || Renderer.DATE.equals(renderer))
        {
            if (val instanceof Date)
            {
                ((DateField) field).setValue((Date) val);
                ((DateField) field).setOriginalValue((Date) val);
            }
            else if (val instanceof String)
            {
                try
                {
                    ((DateField) field).setValue((DateFormat.TIMESTAMP_FORMAT.parse((String) val)));
                    ((DateField) field).setOriginalValue((DateFormat.TIMESTAMP_FORMAT.parse((String) val)));
                }
                catch (Exception e)
                {
                    try
                    {
                        ((DateField) field).setValue((DateFormat.DATE_TIME.parse((String) val)));
                        ((DateField) field).setOriginalValue((DateFormat.DATE_TIME.parse((String) val)));
                    }
                    catch (Exception ex)
                    {
                        Logger.log("Identity sheet widget : cannot parse date for date field.", ex);
                    }
                }
            }
        }
        else if (this.field instanceof StepField)
        {
            if (val != null)
            {
                if (val instanceof Long)
                {
                    ((StepField) this.field).setLongValue((Long) val);
                }
                else if (val instanceof StepModelData)
                {
                    ((StepField) this.field).setStepValue((StepModelData) val);
                }
                else if (val instanceof Double)
                {
                    ((StepField) this.field).setLongValue(((Double) val).longValue());
                }
            }
            else
            {
                ((StepField) this.field).setLongValue(0L);
            }

        }
        else if (this.field instanceof FieldComboBox)
        {
            if (val instanceof String)
            {
                ((FieldComboBox) this.field).setValueByStringValue((String) val);
            }
            else
            {
                ((FieldComboBox) this.field).setValueByStringValue((String) ((FieldModelData) val).getValue());
            }
        }
        else if (Editor.BOOLEAN.equals(editor) || Renderer.BOOLEAN.equals(renderer))
        {
            if (val instanceof Boolean)
            {
                ((CheckBox) field).setValue((Boolean) val);
                ((CheckBox) field).setOriginalValue((Boolean) val);
            }
            else if (val instanceof Double)
            {
                ((CheckBox) field).setValue(FieldUtils.doubleToBoolean((Double) val));
                ((CheckBox) field).setOriginalValue(FieldUtils.doubleToBoolean((Double) val));
            }
        }
        else if (Editor.APPROVAL.equals(editor))
        {
            ((ApprovalField) field).setValue((List<ApprovalNoteModelData>) val);
        }
        else if (ServicesClass.contains(editor) || ServicesClass.contains(renderer))
        {
            if (val instanceof ArrayList<?>)
            {
                List<JSONObject> values = (ArrayList<JSONObject>) val;
                StringBuffer sb = new StringBuffer();
                for (int i = 0, c = values.size(); i < c; i++)
                {
                    if (i > 0)
                    {
                        sb.append(", ");
                    }
                    JSONString name = (JSONString) values.get(i).get(RecordModelData.NAME_FIELD);
                    if (name != null)
                        sb.append(name.stringValue());
                }

                ((TextArea) field).setValue(sb.toString());
                ((TextArea) field).setOriginalValue(sb.toString());
            }
            else
            {
                AutoCompleteModelCombo<RecordModelData> combo = (AutoCompleteModelCombo<RecordModelData>) field;
                combo.setPath(ServicesPath.getPath(editor));

                if ("person".equals(editor.toLowerCase()))
                {
                    combo.setOrderColumn(PersonModelData.PERSON_FIRST_NAME_FIELD);
                    combo.setFilterColumn(PersonModelData.PERSON_FIRST_NAME_FIELD);
                }

                if (val instanceof JSONObject)
                {
                    JSONNumber id = (JSONNumber) ((JSONObject) val).get(Hd3dModelData.ID_FIELD);
                    JSONString name = (JSONString) ((JSONObject) val).get(RecordModelData.NAME_FIELD);
                    JSONString className = (JSONString) ((JSONObject) val).get(RecordModelData.CLASS_NAME_FIELD);
                    JSONString defaultPath = (JSONString) ((JSONObject) val).get(RecordModelData.DEFAULT_PATH_FIELD);
                    Double idd = id.doubleValue();

                    RecordModelData record = new RecordModelData(idd.longValue(), name.stringValue());
                    record.setClassName(className.stringValue());
                    record.setDefaultPath(defaultPath.stringValue());
                    combo.setValue(record);
                    combo.setOriginalValue(record);
                }
                else if (val instanceof RecordModelData)
                {
                    combo.setValue((RecordModelData) val);
                    combo.setOriginalValue((RecordModelData) val);
                }
                else
                {
                    combo.getStore().removeAll();
                    combo.setRawValue("");
                }
            }
        }
        else if (val != null)
        {
            field.setRawValue(val.toString());
        }

        if (field instanceof CheckBox)
        {
            field.removeStyleName("x-grid3-dirty-cell");
        }
        else
        {
            field.removeInputStyleName("x-grid3-dirty-cell");
        }
    }

    public void clearField()
    {
        this.field.clear();
    }

}
