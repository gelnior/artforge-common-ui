package fr.hd3d.common.ui.client.widget;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MenuEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.mvc.Controller;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.store.Record;
import com.extjs.gxt.ui.client.widget.BoxComponent;
import com.extjs.gxt.ui.client.widget.ColorPalette;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.menu.ColorMenu;
import com.extjs.gxt.ui.client.widget.menu.Menu;
import com.extjs.gxt.ui.client.widget.menu.MenuItem;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectTypeModelData;
import fr.hd3d.common.ui.client.modeldata.reader.ProjectReader;
import fr.hd3d.common.ui.client.modeldata.reader.ProjectTypeReader;
import fr.hd3d.common.ui.client.widget.simpleexplorer.SimpleExplorerPanel;


/**
 * Dialog containing a simple event editor (CRUD operations).
 * 
 * @author HD3D
 */
public class ProjectEditor extends Dialog
{
    /** Simple explorer needed for CRUD operations. */
    SimpleExplorerPanel<ProjectModelData> projectExplorer = new SimpleExplorerPanel<ProjectModelData>(
            new ProjectReader());
    private String selectedColor = null;

    public ProjectEditor()
    {
        this.setButtons("");
        this.setStyles();
        this.setProjectExplorer();
    }

    private void setProjectExplorer()
    {
        List<ColumnConfig> columnConfigs = new ArrayList<ColumnConfig>();
        final ModelDataComboBox<ProjectTypeModelData> projectTypeBox = new ModelDataComboBox<ProjectTypeModelData>(
                new ProjectTypeReader());

        ColumnConfig ccTitle = new ColumnConfig();
        ccTitle.setHeader("Name");
        ccTitle.setId(ProjectModelData.NAME_FIELD);
        ccTitle.setDataIndex(ProjectModelData.NAME_FIELD);
        ccTitle.setWidth(200);
        ccTitle.setEditor(new CellEditor(new TextField<String>()));
        ccTitle.setResizable(false);
        columnConfigs.add(ccTitle);

        ColumnConfig projectTypeTitle = new ColumnConfig();
        projectTypeTitle.setHeader("Type");
        projectTypeTitle.setId(ProjectModelData.PROJECT_TYPE_NAME_FIELD);
        projectTypeTitle.setDataIndex(ProjectModelData.PROJECT_TYPE_NAME_FIELD);
        projectTypeTitle.setWidth(200);
        projectTypeTitle.setEditor(new CellEditor(projectTypeBox) {
            @Override
            public Object preProcessValue(Object value)
            {
                if (value == null)
                {
                    return value;
                }
                ProjectModelData projectModelData = projectExplorer.getGrid().getStore().getAt(row);
                if (projectModelData.getProjectTypeId() != null)
                {
                    return projectTypeBox.getStore().findModel(ProjectTypeModelData.ID_FIELD,
                            projectModelData.getProjectTypeId());
                }
                return projectTypeBox.getStore().findModel((ProjectTypeModelData) value);
            }

            @Override
            public Object postProcessValue(Object value)
            {
                if (value == null)
                {
                    return value;
                }
                ProjectModelData projectModelData = projectExplorer.getGrid().getStore().getAt(row);
                projectModelData.setProjectTypeId(((ProjectTypeModelData) value).getId());
                return ((ProjectTypeModelData) value).get(ProjectTypeModelData.NAME_FIELD);
            }
        });
        projectTypeTitle.setResizable(false);
        columnConfigs.add(projectTypeTitle);

        GridCellRenderer<Hd3dModelData> comboBoxRenderer = new GridCellRenderer<Hd3dModelData>() {

            private final String[] statusList = { "open", "closed" };
            private boolean init = false;

            public Object render(Hd3dModelData model, String property, ColumnData config, int rowIndex, int colIndex,
                    final ListStore<Hd3dModelData> store, final Grid<Hd3dModelData> grid)
            {
                if (!init)
                {

                    init = true;
                    final Menu menu = new Menu();
                    menu.setAutoWidth(true);

                    MenuItem menuItem = null;
                    for (final String statusName : statusList)
                    {
                        menuItem = new MenuItem(statusName);
                        menuItem.addSelectionListener(new SelectionListener<MenuEvent>() {
                            @Override
                            public void componentSelected(MenuEvent ce)
                            {
                                Hd3dModelData md = grid.getSelectionModel().getSelectedItem();
                                Record rec = store.getRecord(md);
                                rec.set(ProjectModelData.PROJECT_STATUS, statusName);
                                rec.setDirty(true);

                            }
                        });
                        menu.add(menuItem);
                    }
                    menu.setWidth(180);

                    grid.setContextMenu(menu);
                }

                Object gValue = model.get(property);
                String stringValue = "";
                if (gValue != null)
                {
                    stringValue = (String) gValue;
                    if (stringValue.equals("open"))
                    {
                        stringValue = "<div style='width:100%; height:100%; text-align:center; background-color:"
                                + "9BD46D" + ";'>" + (String) gValue + "</div>";
                    }

                    else if (stringValue.equals("closed"))
                    {
                        stringValue = "<div style='width:100%; height:100%; text-align:center; background-color:"
                                + "8C99C9" + ";'>" + (String) gValue + "</div>";
                    }
                }
                else
                {
                    stringValue += "unknow status,";
                }
                return stringValue;
            }
        };

        ColumnConfig ccStatus = new ColumnConfig();
        ccStatus.setHeader("Status");
        ccStatus.setId(ProjectModelData.PROJECT_STATUS);
        ccStatus.setDataIndex(ProjectModelData.PROJECT_STATUS);
        ccStatus.setWidth(150);
        ccStatus.setRenderer(comboBoxRenderer);
        ccStatus.setResizable(false);
        columnConfigs.add(ccStatus);

        ColumnConfig ccColor = new ColumnConfig();
        ccColor.setHeader("Color");
        ccColor.setId(ProjectModelData.PROJECT_COLOR);
        ccColor.setDataIndex(ProjectModelData.PROJECT_COLOR);
        ccColor.setWidth(50);

        GridCellRenderer<Hd3dModelData> buttonRenderer = new GridCellRenderer<Hd3dModelData>() {

            private boolean init;
            private ListStore<Hd3dModelData> store;

            public Object render(final Hd3dModelData model, String property, ColumnData config, final int rowIndex,
                    final int colIndex, ListStore<Hd3dModelData> store, Grid<Hd3dModelData> grid)
            {

                this.store = store;

                if (!init)
                {
                    init = true;
                    grid.addListener(Events.ColumnResize, new Listener<GridEvent<Hd3dModelData>>() {

                        public void handleEvent(GridEvent<Hd3dModelData> be)
                        {
                            for (int i = 0; i < be.getGrid().getStore().getCount(); i++)
                            {
                                if (be.getGrid().getView().getWidget(i, be.getColIndex()) != null
                                        && be.getGrid().getView().getWidget(i, be.getColIndex()) instanceof BoxComponent)
                                {
                                    ((BoxComponent) be.getGrid().getView().getWidget(i, be.getColIndex())).setWidth(be
                                            .getWidth() - 10);
                                }
                            }
                        }
                    });
                }

                String stringValue = "<div style='width:100%; height:100%; text-align:center; background-color:"
                        + (String) model.get(property) + ";'>" + "." + "</div>";

                final Button b = new Button(stringValue, new SelectionListener<ButtonEvent>() {
                    @Override
                    public void componentSelected(ButtonEvent ce)
                    {}
                });

                ColorMenu menu = new ColorMenu();
                menu.getColorPalette().addListener(Events.Select, new Listener<ComponentEvent>() {

                    public void handleEvent(ComponentEvent be)
                    {
                        selectedColor = ((ColorPalette) be.getComponent()).getValue();

                        update(model, selectedColor);

                        b.setText("<div style='width:100%; height:100%; text-align:center; background-color:"
                                + selectedColor + ";'>" + "." + "</div>");
                    }
                });

                b.setMenu(menu);
                b.setWidth(grid.getColumnModel().getColumnWidth(colIndex) - 10);
                b.setToolTip("Click for change color");
                b.setWidth(30);

                return b;
            }

            private void update(Hd3dModelData theModel, String theSelectedColor)
            {
                Record rec = store.getRecord(theModel);
                if (!theSelectedColor.startsWith("#"))
                {
                    theSelectedColor = "#" + theSelectedColor;
                }
                rec.set(ProjectModelData.PROJECT_COLOR, "" + theSelectedColor);
                rec.setDirty(true);
            }

        };

        ccColor.setRenderer(buttonRenderer);
        ccColor.setResizable(false);
        columnConfigs.add(ccColor);

        ColumnModel cm = new ColumnModel(columnConfigs);
        this.projectExplorer.reconfigureGrid(cm);
        this.projectExplorer.refresh();
        this.add(projectExplorer);
    }

    private void setStyles()
    {
        this.setButtons("");
        this.setLayout(new FitLayout());
        this.setWidth(638);
        this.setModal(true);
        this.setResizable(false);
        this.setHeading("Project Manager");

        this.projectExplorer.setSortField(ProjectModelData.NAME_FIELD);
        this.projectExplorer.setSortDir(SortDir.ASC);
    }

    @Override
    public void show()
    {
        this.projectExplorer.refresh();
        this.projectExplorer.unIdle();
        super.show();
    }

    public Controller getController()
    {
        return this.projectExplorer.getController();
    }

}
