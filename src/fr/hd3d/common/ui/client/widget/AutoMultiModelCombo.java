package fr.hd3d.common.ui.client.widget;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.KeyListener;
import com.extjs.gxt.ui.client.event.LoadListener;
import com.extjs.gxt.ui.client.util.Util;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONNumber;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.user.client.Timer;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.util.KeyBoardUtils;


/**
 * A combo box that allows to select multiple values from model services.
 * 
 * @author HD3D
 * 
 * @param <M>
 *            The type of data to retrieve.
 */
public class AutoMultiModelCombo<M extends RecordModelData> extends AutoCompleteModelCombo<M>
{
    /** Values selected by user. */
    protected final ArrayList<M> values = new ArrayList<M>();

    /** True if last value must be deleted. */
    protected boolean isDelete;

    /**
     * Constructor
     * 
     * @param reader
     *            Reader to parse incoming data retrieved by auto completion.
     */
    public AutoMultiModelCombo(IReader<M> reader)
    {
        super(reader);
        this.idConstraint.setOperator(EConstraintOperator.in);
    }

    /**
     * Set listeners to react correctly when a key is typed.
     */
    @Override
    protected void setListeners()
    {
        this.addListener(Events.KeyUp, new KeyListener() {
            @Override
            public void componentKeyUp(ComponentEvent event)
            {
                onKeyUp(event.getKeyCode());
            }
        });
        this.addListener(Events.KeyDown, new KeyListener() {
            @Override
            public void componentKeyDown(ComponentEvent event)
            {
                onKeyDown(event.getKeyCode());
            }
        });
    }

    /**
     * Set combo box styles and behavior.
     */
    @Override
    protected void setStyles()
    {
        this.setForceSelection(false);
        this.setTriggerAction(TriggerAction.ALL);
        this.setEditable(true);
        this.setValidateOnBlur(false);

        this.setDisplayField(RecordModelData.NAME_FIELD);
        this.setTypeAhead(false);
        this.setMinChars(3);
        this.setSelectOnFocus(false);
    }

    public void setValue(List<?> value)
    {}

    public void setValue()
    {
        this.updateRawValue();
        this.likeConstraint.setLeftMember("%");
        this.fireEvent(Events.Change);
    }

    /**
     * Add value to current value list (if value is not null and not already in list). It updates the displayed value
     * with the name list of currently set values.
     */
    @Override
    public void setValue(M value)
    {
        if (value != null)
        {
            boolean isNew = true;

            for (M model : this.values)
            {
                if (model.getId().longValue() == value.getId().longValue())
                    isNew = false;
                break;
            }

            if (isNew)
                this.values.add(value);
        }
        this.updateRawValue();
        this.likeConstraint.setLeftMember("%");
        this.fireEvent(Events.Change);
    }

    /**
     * Clear combo as usual and clear value list.
     */
    @Override
    public void clear()
    {
        super.clear();
        this.values.clear();
    }

    /**
     * Clear default behavior when something is loaded.
     */
    @Override
    protected void onLoaderLoad()
    {}

    /**
     * When a key is down, it checks if it is a backspace and that there is no char between last raw value and current
     * cursor position. If there is not, it activates the isDelete tag that will remove last raw value when key will be
     * up.
     * 
     * @param keyCode
     */
    protected void onKeyDown(int keyCode)
    {
        if (KeyCodes.KEY_BACKSPACE == keyCode)
        {
            if (CollectionUtils.isNotEmpty(values) && getLastRawValue().equals(getLastValue().getName()))
            {
                this.isDelete = true;
            }
        }
    }

    /**
     * When key is up, if isDelete flag is set to true, it removes last added task type, else it reloads combo store for
     * currently typed name.
     */
    @Override
    protected void onKeyUp(int keyCode)
    {
        if (this.isDelete)
        {
            this.values.remove(values.size() - 1);
            this.setValue();
            this.collapse();
            this.isDelete = false;
        }
        else if (KeyBoardUtils.isTextKey(keyCode))
        {
            if (!this.isTiming)
            {
                this.isTiming = true;
                Timer timer = new Timer() {
                    @Override
                    public void run()
                    {
                        isTiming = false;
                        likeConstraint.setLeftMember(getLike());
                        store.reload();
                        expand();
                    }
                };

                try
                {
                    timer.schedule(LOADING_DELAY);
                }
                catch (Exception e)
                {
                    this.isTiming = false;
                }
            }
        }
    }

    /**
     * @return Last value set in the value list.
     */
    protected M getLastValue()
    {
        if (CollectionUtils.isNotEmpty(values))
        {
            return values.get(values.size() - 1);
        }
        return null;
    }

    /**
     * @return Return last text from raw value list (each text are separated by comma).
     */
    protected String getLastRawValue()
    {
        String[] values = getRawValue().split(", ");
        if (values.length > 0)
        {
            return values[values.length - 1];
        }
        else
            return "";
    }

    /**
     * Return last text from raw value list (each text are separated by comma) + "%" char to make it usable for like
     * constraint.
     */
    @Override
    protected String getLike()
    {
        return getLastRawValue() + "%";
    }

    /**
     * Build a list text separated by comma, where a text corresponds to the namem field of each value.
     */
    protected void updateRawValue()
    {
        StringBuilder displayValues = new StringBuilder();
        for (M value : values)
        {
            displayValues.append(value.getName());
            displayValues.append(", ");
        }
        this.setRawValue(displayValues.toString());
    }

    public ArrayList<M> getValues()
    {
        return this.values;
    }

    public Object getValueIds()
    {
        return CollectionUtils.getIds(getValues());
    }

    /**
     * 
     * @param stringValue
     *            List of long as JSON format.
     */
    public void setValueByStringValue(String stringValue)
    {
        if (!Util.isEmptyString(stringValue) && !"null".equals(stringValue))
        {
            JSONArray array = (JSONArray) JSONParser.parse(stringValue);
            List<Long> ids = new ArrayList<Long>();
            for (int i = 0; i < array.size(); i++)
            {
                JSONNumber idNumber = array.get(i).isNumber();
                ids.add(new Double(idNumber.doubleValue()).longValue());
            }

            if (CollectionUtils.isNotEmpty(ids))
            {
                this.store.removeAll();
                this.setValueByIds(ids);
            }
            else
            {
                this.values.clear();
                this.updateRawValue();
            }
        }
        else
        {
            this.values.clear();
            this.updateRawValue();
        }
    }

    /**
     * Set combo values by giving the id list of objects to set inside combo box.
     * 
     * @param ids
     *            The IDs of object to retrieve.
     */
    public void setValueByIds(final List<Long> ids)
    {
        this.idConstraint.setLeftMember(ids);
        this.store.removeParameter(likeConstraint);
        this.store.addParameter(idConstraint);

        final LoadListener listener = new LoadListener() {
            @Override
            public void loaderLoad(LoadEvent le)
            {
                onIdsLoaded(this);
            }
        };

        this.getStore().getLoader().addLoadListener(listener);
        this.store.reload();
    }

    /**
     * When objects are loaded, it clears value list and replace it with retrieved objects, then raw value is updated to
     * display the list of object names.
     * 
     * @param listener
     *            The listener that calls this method. It must be removed to not rebuild values after each loading.
     */
    protected void onIdsLoaded(LoadListener listener)
    {
        this.getStore().getLoader().removeLoadListener(listener);

        this.values.clear();
        this.values.addAll(this.store.getModels());
        this.updateRawValue();

        this.store.addParameter(likeConstraint);
        this.store.removeParameter(idConstraint);
    }
}
