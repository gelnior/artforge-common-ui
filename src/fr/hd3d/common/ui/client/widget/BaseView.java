package fr.hd3d.common.ui.client.widget;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.ContentPanel;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;


public class BaseView extends ContentPanel
{
    List<MaskableController> controllers;

    /** True if event should be forwarded only on local controller or to all controllers. */
    protected boolean isLocalEvents = false;

    public void setLocalEventsOn()
    {
        this.isLocalEvents = true;
    }

    public void forwardLocalEvent(AppEvent event)
    {
        for (MaskableController controller : getControllers())
        {
            if (!controller.isMasked())
                controller.handleEvent(event);
        }
    }

    public List<MaskableController> getControllers()
    {
        if (controllers == null)
        {
            controllers = new ArrayList<MaskableController>();
        }
        return controllers;
    }

    public void addController(MaskableController controller)
    {
        getControllers().add(controller);
    }

    public void forwardEvent(AppEvent event)
    {
        if (isLocalEvents)
        {
            this.forwardLocalEvent(event);
        }
        else
        {
            EventDispatcher.forwardEvent(event);
        }
    }
}
