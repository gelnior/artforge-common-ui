package fr.hd3d.common.ui.client.widget;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.constant.CommonMessages;
import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.util.GridUtils;


/**
 * This grid is used to display non-array fields
 * 
 * @author HD3D
 */
public class FieldGrid extends Grid<FieldModelData>
{
    /** Constant strings to display : dialog messages, button label... */
    public static final CommonConstants CONSTANTS = GWT.create(CommonConstants.class);
    /** Parameterizable messages to display (like the counter label) */
    public static final CommonMessages MESSAGES = GWT.create(CommonMessages.class);

    /**
     * Default constructor
     * 
     * @param store
     *            Store that contains data to display into the grid.
     */
    public FieldGrid(ListStore<FieldModelData> store)
    {
        super(store, getColumns());

        this.setStyle();
    }

    /**
     * @return grid columns : one for field names and one the field values.
     */
    private static ColumnModel getColumns()
    {
        List<ColumnConfig> columns = new ArrayList<ColumnConfig>();

        GridUtils.addColumnConfig(columns, FieldModelData.NAME_FIELD, CONSTANTS.Name(), 130, true);
        GridUtils.addColumnConfig(columns, FieldModelData.VALUE_FIELD, "value", 150, true);

        return new ColumnModel(columns);
    }

    /**
     * Set grid style : no headers, and size is force to fit the parent panel.
     */
    private void setStyle()
    {}
}
