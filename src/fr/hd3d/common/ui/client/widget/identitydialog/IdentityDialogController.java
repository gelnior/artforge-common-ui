package fr.hd3d.common.ui.client.widget.identitydialog;

import fr.hd3d.common.ui.client.mvc.controller.CommandController;
import fr.hd3d.common.ui.client.widget.identitydialog.command.CastingNotesLoadedCmd;
import fr.hd3d.common.ui.client.widget.identitydialog.command.CastingTasksLoadedCmd;
import fr.hd3d.common.ui.client.widget.identitydialog.command.CompositionDoubleClickedCmd;
import fr.hd3d.common.ui.client.widget.identitydialog.command.CompositionTaskTypeChangedCmd;
import fr.hd3d.common.ui.client.widget.identitydialog.command.DialogShowedCmd;
import fr.hd3d.common.ui.client.widget.identitydialog.command.MaximizeCastingTaskGridCmd;
import fr.hd3d.common.ui.client.widget.identitydialog.command.WorkObjectRefreshedCmd;


/**
 * Controller that links identity dialog events to the right command.
 * 
 * @author HD3D
 */
public class IdentityDialogController extends CommandController
{
    private final IdentityDialogModel model;
    private final IIdentityDialog view;

    /**
     * Default constructor : register events the controller can handle.
     * 
     * @param model
     *            The model handling identity dialog data.
     * @param view
     *            The identity dialog.
     */
    public IdentityDialogController(IdentityDialogModel model, IIdentityDialog view)
    {
        this.model = model;
        this.view = view;

        this.registerEvents();
    }

    private void registerEvents()
    {
        commandMap.put(IdentityDialogEvents.DIALOG_SHOWED, new DialogShowedCmd(model, view));
        commandMap.put(IdentityDialogEvents.WORK_OBJECT_REFRESHED, new WorkObjectRefreshedCmd(model, view));
        commandMap.put(IdentityDialogEvents.CASTING_TASKS_LOADED, new CastingTasksLoadedCmd(model, view));
        commandMap.put(IdentityDialogEvents.CASTING_NOTES_LOADED, new CastingNotesLoadedCmd(model, view));
        commandMap.put(IdentityDialogEvents.COMPOSITION_DBLE_CLICKED, new CompositionDoubleClickedCmd(model, view));
        commandMap.put(IdentityDialogEvents.COMPOSITION_TASK_TYPE_CHANGED, new CompositionTaskTypeChangedCmd(model,
                view));
        commandMap.put(IdentityDialogEvents.MAXIMIZE_CLICKED, new MaximizeCastingTaskGridCmd(model, view));
    }

}
