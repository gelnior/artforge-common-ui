package fr.hd3d.common.ui.client.widget.identitydialog;

import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.modeldata.reader.ReaderFactory;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.widget.MultiComboBox;


/**
 * Combo box allowing to select multiple task types.
 * 
 * @author HD3D
 */
public class MultiTaskTypeComboBox extends MultiComboBox<TaskTypeModelData>
{
    /**
     * Default constructor. Configure store to display name of task type to user and sort task types on their name.
     * 
     * @param store
     *            Store to set as combo box store.
     */
    public MultiTaskTypeComboBox(ServiceStore<TaskTypeModelData> store)
    {
        super();

        store.setSortField(FieldModelData.NAME_FIELD);
        store.sort(FieldModelData.NAME_FIELD, SortDir.ASC);

        this.setStore(store);

        this.setDisplayField(FieldModelData.NAME_FIELD);
        this.setValueField(FieldModelData.VALUE_FIELD);

        this.getListView().setTemplate(getXTemplate());
    }

    public MultiTaskTypeComboBox()
    {
        this(new ServiceStore<TaskTypeModelData>(ReaderFactory.getTaskTypeReader()));
    }

    /**
     * TODO: comments
     * 
     * @param eventType
     *            The event type to raise when selection changes.
     */
    public void setSelectionChangedEvent(final EventType eventType)
    {
        this.addListener(Events.Change, new Listener<BaseEvent>() {

            public void handleEvent(BaseEvent be)
            {
                EventDispatcher.forwardEvent(eventType, getSelection());
            }
        });
    }

    /**
     * @return Combo box values display style.
     */
    public native String getXTemplate() /*-{
                                        return  [ 
                                        '<tpl for=".">',
                                        '<div class=\"x-view-item x-view-item-check\"><table><tr><td><input class=\"x-view-item-checkbox\" type=\"checkbox\" /></td><td><td>',
                                        '<div style="width: 100%; text-align: center; background-color:{color};" class="x-combo-list-item"> <span style="font-weight: bold; font-size: 12px;">&nbsp;{name}&nbsp;&nbsp;</span> </div>', 
                                        '</td></tr></table></div></tpl>' 
                                        ].join("");
                                        }-*/;
}
