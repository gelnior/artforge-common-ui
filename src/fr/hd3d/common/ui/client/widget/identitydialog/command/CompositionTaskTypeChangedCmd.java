package fr.hd3d.common.ui.client.widget.identitydialog.command;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.Constraints;
import fr.hd3d.common.ui.client.service.parameter.InConstraint;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.widget.identitydialog.IIdentityDialog;
import fr.hd3d.common.ui.client.widget.identitydialog.IdentityDialogModel;


/**
 * When composition task type change, it reconfigures the casting stores (tasks and approvals) then reloads task store.
 * 
 * @author HD3D
 */
public class CompositionTaskTypeChangedCmd extends IdentityDialogCmd
{

    public CompositionTaskTypeChangedCmd(IdentityDialogModel model, IIdentityDialog view)
    {
        super(model, view);
    }

    public void execute(AppEvent event)
    {
        Hd3dModelData workObject = this.model.getCurrentWorkObject();
        List<TaskTypeModelData> taskTypes = this.view.getSelectedTaskTypes();

        if (CollectionUtils.isNotEmpty(taskTypes)
                && ((FieldUtils.isShot(workObject) && this.model.getConstituentStore().getCount() > 0) || (FieldUtils
                        .isConstituent(workObject) && this.model.getShotStore().getCount() > 0)))
        {
            this.updateTaskStoresPath(workObject);
            this.updateTaskStoreParameters(taskTypes, workObject);
            this.updateNoteStorePath();
            this.updateNoteStoreParameters(taskTypes, workObject);

            this.model.getCastingTaskStore().reload();
        }
    }

    private void updateTaskStoresPath(Hd3dModelData workObject)
    {
        if (FieldUtils.isShot(workObject))
            this.model.getCastingTaskStore().setPath(
                    workObject.getPath() + "/" + ServicesPath.CONSTITUENTS + ServicesPath.TASKS);
        else if (FieldUtils.isConstituent(workObject))
            this.model.getCastingTaskStore().setPath(
                    workObject.getPath() + "/" + ServicesPath.SHOTS + ServicesPath.TASKS);
    }

    private void updateTaskStoreParameters(List<TaskTypeModelData> taskTypes, Hd3dModelData workObject)
    {
        this.model.getCastingTaskStore().clearParameters();
        this.model.getCastingTaskStore().addInConstraint("taskType.id", CollectionUtils.getIds(taskTypes));
    }

    private void updateNoteStorePath()
    {
        this.model.getCastingNoteStore().clearParameters();
        this.model.getCastingNoteStore().setPath(ServicesPath.APPROVAL_NOTES);
    }

    private void updateNoteStoreParameters(List<TaskTypeModelData> taskTypes, Hd3dModelData workObject)
    {
        Constraints constraints = new Constraints();

        if (FieldUtils.isShot(workObject))
            this.addShotConstraint(constraints);
        else if (FieldUtils.isConstituent(workObject))
            this.addConstituentConstraint(constraints);

        constraints.addInConstraint("approvalNoteType.taskType.id", CollectionUtils.getIds(taskTypes));

        this.model.getCastingNoteStore().setPath(
                ServicesPath.PROJECTS + this.model.getProject().getId() + "/" + ServicesPath.APPROVAL_NOTES);
        this.model.getCastingNoteStore().addParameter(constraints);
    }

    private void addConstituentConstraint(Constraints constraints)
    {
        constraints.addEqConstraint(ApprovalNoteModelData.BOUND_ENTITY_NAME, ShotModelData.SIMPLE_CLASS_NAME);
        List<Long> workObjectIds = new ArrayList<Long>();
        for (ShotModelData model : this.model.getShotStore().getModels())
            workObjectIds.add(model.getId());
        constraints.add(new InConstraint(ApprovalNoteModelData.BOUND_ENTITY_ID, workObjectIds));
    }

    private void addShotConstraint(Constraints constraints)
    {
        constraints.addEqConstraint(ApprovalNoteModelData.BOUND_ENTITY_NAME, ConstituentModelData.SIMPLE_CLASS_NAME);
        List<Long> workObjectIds = new ArrayList<Long>();
        for (ConstituentModelData model : this.model.getConstituentStore().getModels())
            workObjectIds.add(model.getId());
        constraints.add(new InConstraint(ApprovalNoteModelData.BOUND_ENTITY_ID, workObjectIds));
    }

}
