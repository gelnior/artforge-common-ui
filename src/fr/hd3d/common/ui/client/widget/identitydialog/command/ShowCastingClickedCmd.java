package fr.hd3d.common.ui.client.widget.identitydialog.command;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.widget.identitydialog.IdentityDialog;
import fr.hd3d.common.ui.client.widget.identitydialog.IdentityDialogModel;


/**
 * When show casting is clicked, it displays casting work object grid and casting task grid.
 * 
 * @author HD3D
 */
public class ShowCastingClickedCmd extends IdentityDialogCmd
{

    public ShowCastingClickedCmd(IdentityDialogModel model, IdentityDialog view)
    {
        super(model, view);
    }

    public void execute(AppEvent event)
    {
        // if (this.model.isCastingDisplay())
        // {
        // this.model.setCastingDisplayOff();
        // this.view.hideCastingWidgets();
        // }
        // else
        // {
        // this.model.setCastingDisplayOn();
        // this.view.showCastingWidgets();
        // }
    }

}
