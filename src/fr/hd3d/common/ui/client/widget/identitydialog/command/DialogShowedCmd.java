package fr.hd3d.common.ui.client.widget.identitydialog.command;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.widget.identitydialog.IIdentityDialog;
import fr.hd3d.common.ui.client.widget.identitydialog.IdentityDialogModel;


/**
 * When dialog is showed, data and widgets are cleared then all stores are reconfigured.
 * 
 * @author HD3D
 */
public class DialogShowedCmd extends IdentityDialogCmd
{
    public DialogShowedCmd(IdentityDialogModel model, IIdentityDialog view)
    {
        super(model, view);
    }

    public void execute(AppEvent event)
    {
        if (FieldUtils.isShot(this.model.getCurrentWorkObject().getSimpleClassName()))
            this.view.displayConstituentGrid();
        else
            this.view.displayShotGrid();

        this.view.clearAll();
        this.model.clearAll();

        this.model.updateTaskStoresConfiguration();
        this.model.updateNoteStoreConfiguration();

        this.view.reloadData();
    }
}
