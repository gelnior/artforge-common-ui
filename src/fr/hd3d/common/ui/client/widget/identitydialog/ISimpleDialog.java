package fr.hd3d.common.ui.client.widget.identitydialog;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;


public interface ISimpleDialog
{

    /**
     * Display info dialog. Clear all data, then reload data for <i>workObject</i>
     * 
     * @param workObject
     *            Data from this work object will be displayed inside identity dialog.
     */
    public abstract void show(Hd3dModelData workObject);

    /**
     * Hide loading indicator of task grid.
     */
    public abstract void unmaskTaskGrid();

    public abstract void reloadData();
}
