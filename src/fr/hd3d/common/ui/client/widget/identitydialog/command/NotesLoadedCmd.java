package fr.hd3d.common.ui.client.widget.identitydialog.command;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.widget.identitydialog.IdentityDialog;
import fr.hd3d.common.ui.client.widget.identitydialog.IdentityDialogModel;


/**
 * When note are loaded, most recent note is selected for each task type. Then each task description field is updated
 * with note data corresponding to its task type.
 * 
 * @author HD3D
 */
public class NotesLoadedCmd extends IdentityDialogCmd
{

    public NotesLoadedCmd(IdentityDialogModel model, IdentityDialog view)
    {
        super(model, view);
    }

    public void execute(AppEvent event)
    {}

}
