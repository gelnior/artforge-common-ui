package fr.hd3d.common.ui.client.widget.identitydialog;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;


/**
 * Singleton used to display identity dialog.
 * 
 * @author HD3D
 */
public class IdentityDialogDisplayer
{
    /** Dialog to display (reusable). */
    protected static IdentityDialog dialog;

    public static IdentityDialog getDialog()
    {
        if (dialog == null)
            dialog = new IdentityDialog();

        return dialog;
    }

    /**
     * Display Info dialog for given work object.
     * 
     * @param workObject
     *            The work object for which dialog should be displayed.
     * @param project
     *            Project is needed to display available task types for casting task grid.
     */
    public static void display(Hd3dModelData workObject, ProjectModelData project)
    {
        getDialog().show(workObject, project);
    }
}
