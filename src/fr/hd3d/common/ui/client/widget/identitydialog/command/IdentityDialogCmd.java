package fr.hd3d.common.ui.client.widget.identitydialog.command;

import fr.hd3d.common.ui.client.mvc.controller.BaseCommand;
import fr.hd3d.common.ui.client.widget.identitydialog.IIdentityDialog;
import fr.hd3d.common.ui.client.widget.identitydialog.IdentityDialogModel;


/**
 * Base Class for Identity Dialog commands.
 * 
 * @author HD3D
 */
public abstract class IdentityDialogCmd implements BaseCommand
{
    /** Model handling identity dialog data. */
    protected final IdentityDialogModel model;
    /** View containing identity dialog widgets. */
    protected final IIdentityDialog view;

    /**
     * Default constructor.
     * 
     * @param model
     *            Model handling identity dialog data.
     * @param view
     *            View containing identity dialog widgets.
     */
    public IdentityDialogCmd(IdentityDialogModel model, IIdentityDialog view)
    {
        this.model = model;
        this.view = view;
    }
}
