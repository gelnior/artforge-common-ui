package fr.hd3d.common.ui.client.widget.identitydialog;

import fr.hd3d.common.ui.client.calendar.DateFormat;
import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.WorkObjectModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.modeldata.reader.ReaderFactory;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.service.parameter.OrderBy;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.util.FastLongMap;


/**
 * Model handling data for simple dialog.
 * 
 * @author HD3D
 */
public class SimpleIdentityDialogModel
{

    /** Store containing tasks linked to current work object. */
    protected ServiceStore<TaskModelData> taskStore = new ServiceStore<TaskModelData>(ReaderFactory.getTaskReader());

    /**
     * Store containing notes linked to current work object via compositions (used to update task data with last
     * approval data).
     */
    protected ServiceStore<ApprovalNoteModelData> noteStore = new ServiceStore<ApprovalNoteModelData>(
            ReaderFactory.getApprovalNoteReader());

    /**
     * Default constructor, set order by parameter on task store.
     */
    public SimpleIdentityDialogModel()
    {
        this.taskStore.addParameter(new OrderBy("taskType.name"));
    }

    /**
     * @return store containing tasks.
     */
    public ServiceStore<TaskModelData> getTaskStore()
    {
        return taskStore;
    }

    /**
     * @return Store containing approval notes used to .
     */
    public ServiceStore<ApprovalNoteModelData> getNoteStore()
    {
        return noteStore;
    }

    /**
     * Process approval notes to extract for each task, the comment and the of the last written approval note for this
     * task type.
     */
    public void updateData()
    {
        FastLongMap<ApprovalNoteModelData> taskNotes = new FastLongMap<ApprovalNoteModelData>();

        for (ApprovalNoteModelData note : this.noteStore.getModels())
        {
            this.addApprovalIfMoreRecent(note, taskNotes);
        }

        for (TaskModelData task : this.taskStore.getModels())
        {
            this.setLastNoteDateForTask(taskNotes, task);
        }
    }

    /**
     * Set date and comment from last note written for given task (link based on task type) in the description field of
     * the task (field used only in this widget).
     * 
     * @param taskNotes
     *            The task note map containing comments set for a givent task type.
     * @param task
     *            The task to update.
     */
    private void setLastNoteDateForTask(FastLongMap<ApprovalNoteModelData> taskNotes, TaskModelData task)
    {
        if (task.getTaskTypeId() != null)
        {
            Long type = task.getTaskTypeId();
            ApprovalNoteModelData note = taskNotes.get(type);
            if (note != null)
            {
                String date = DateFormat.DATE_TIME.format(note.getDate());
                String description = date + ": ";
                if (note.getComment() != null)
                    description += note.getComment();
                task.set(WorkObjectModelData.DESCRIPTION_FIELD, description);
                this.taskStore.update(task);
            }
        }
        else
        {
            Logger.warn("Identity Dialog : Some tasks have no task type");
        }
    }

    /**
     * Add note to task notes map, with task type id as key. If an approval already exists for this entry, it is
     * replaced only if current not is more recent that the one already set. Else it does nothing.
     * 
     * @param note
     *            The note to add.
     * @param taskNotes
     *            The task note map in which note must be set.
     */
    private void addApprovalIfMoreRecent(ApprovalNoteModelData note, FastLongMap<ApprovalNoteModelData> taskNotes)
    {
        if (note.getTaskTypeId() != null)
        {
            Long type = note.getTaskTypeId();
            if (taskNotes.get(type) == null || taskNotes.get(type).getDate().before(note.getDate()))
            {
                taskNotes.put(type, note);
            }
        }
        else
        {
            Logger.warn("Note with no task type has been retrieved.");
        }
    }
}
