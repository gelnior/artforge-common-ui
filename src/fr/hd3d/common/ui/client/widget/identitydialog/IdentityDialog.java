package fr.hd3d.common.ui.client.widget.identitydialog;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.GXT;
import com.extjs.gxt.ui.client.Style.Scroll;
import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.TabItem;
import com.extjs.gxt.ui.client.widget.TabPanel;
import com.extjs.gxt.ui.client.widget.Text;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.layout.ColumnData;
import com.extjs.gxt.ui.client.widget.layout.ColumnLayout;
import com.extjs.gxt.ui.client.widget.layout.MarginData;

import fr.hd3d.common.ui.client.listener.LocalButtonClickListener;
import fr.hd3d.common.ui.client.listener.LocalCellDoubleClickListener;
import fr.hd3d.common.ui.client.listener.LocalListener;
import fr.hd3d.common.ui.client.listener.LocalLoadListener;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.WorkObjectModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.util.GridUtils;
import fr.hd3d.common.ui.client.widget.grid.BaseGrid;
import fr.hd3d.common.ui.client.widget.grid.renderer.CommentRenderer;
import fr.hd3d.common.ui.client.widget.grid.renderer.LongTextRenderer;
import fr.hd3d.common.ui.client.widget.grid.renderer.PaddingTextRenderer;


/**
 * Identity dialog is a non editable dialog that shows main infos about a work object : name, path, description,
 * preview, tasks. It also allows to browse casting and tasks linked to casting.
 * 
 * @author HD3D
 */
public class IdentityDialog extends SimpleIdentityDialog implements IIdentityDialog
{

    /** Model handling identity dialog data. */
    protected IdentityDialogModel model = new IdentityDialogModel();

    /** Controller handling identity dialog events. */
    protected IdentityDialogController controller = new IdentityDialogController(model, this);

    /** Grid containing work object casting (if it a shot). */
    protected BaseGrid<ConstituentModelData> castingConstituentGrid = new BaseGrid<ConstituentModelData>(
            this.model.getConstituentStore(), getCompositionConstituentColumnModel());
    /** Grid containing work object casting (if it a constituent). */
    protected BaseGrid<ShotModelData> castingShotGrid = new BaseGrid<ShotModelData>(this.model.getShotStore(),
            getCompositionShotColumnModel());

    /** Grid containing tasks linked to selected work object for selected task types. */
    protected BaseGrid<TaskModelData> castingTaskGrid = new BaseGrid<TaskModelData>(this.model.getCastingTaskStore(),
            getCompositionTaskGridColumnModel());
    /** Combo box needed to browse casting tasks by task types. */
    protected MultiTaskTypeComboBox taskTypeCombo = new MultiTaskTypeComboBox(this.model.getTaskTypeStore());

    /** Layout stuff. */
    protected LayoutContainer taskTypeComboContainer = new LayoutContainer(new ColumnLayout());

    /** Dialog to display more info on a selected work object (via a task) of casting. */
    protected ISimpleDialog castingDialog = new SimpleIdentityDialog();

    protected Button maximizeCastingTaskGridButton = new Button("maximize");

    protected TabPanel castingPanel = new TabPanel();

    /**
     * Dialog constructor : set styles and configure data stores.
     */
    public IdentityDialog()
    {
        super();
        super.setModel(model);

        this.setAdditionalStyles();
        this.setListeners();
    }

    /**
     * Display info dialog, registers work object, configures data store then reload task grid for given work object.
     * 
     * @param workObject
     */
    public void show(Hd3dModelData workObject, ProjectModelData project)
    {
        this.workObject = workObject.copy();
        this.workObject.setSimpleClassName(workObject.getSimpleClassName());
        if (FieldUtils.isShot(this.workObject.getSimpleClassName()))
            this.workObject.setClassName(ShotModelData.CLASS_NAME);
        else
            this.workObject.setClassName(ConstituentModelData.CLASS_NAME);

        this.model.setCurrentWorkObject(this.workObject);
        this.model.setProject(project);

        this.controller.handleEvent(new AppEvent(IdentityDialogEvents.DIALOG_SHOWED, project));

        super.show();
    }

    /**
     * Display a simple identity dialog (no casting data) for a given work object.
     * 
     * @param castingWorkObject
     *            The work object for which the dialog is displayed.
     */
    public void displaySimpleDialog(Hd3dModelData castingWorkObject)
    {
        this.castingDialog.show(castingWorkObject);
    }

    /**
     * @return Task type selected in combo box.
     */
    public List<TaskTypeModelData> getSelectedTaskTypes()
    {
        return this.taskTypeCombo.getSelection();
    }

    /**
     * Clear the task type combo box.
     */
    public void clearTaskTypeCombo()
    {
        this.taskTypeCombo.setFireChangeEventOnSetValue(false);
        this.taskTypeCombo.setValue(null);
        this.taskTypeCombo.clearSelections();
        this.taskTypeCombo.setFireChangeEventOnSetValue(true);
    }

    /**
     * Clear all Data from the identity dialog.
     */
    @Override
    public void clearAll()
    {
        super.clearAll();
        this.clearTaskTypeCombo();
    }

    /**
     * Show widgets displaying basic informations on work object.
     */
    public void showInfoWidgets()
    {
        this.title.show();
        this.description.show();
        this.nbFrame.show();
        this.image.removeStyleName("hidden");
        this.taskGrid.show();
        infoContainer.setStyleAttribute("margin-top", "5px");
        infoTextContainer.setStyleAttribute("padding", "5px");
        this.description.setStyleAttribute("margin-top", "15px");
    }

    /**
     * Hide widgets displaying basic informations on work object.
     */
    public void hideInfoWidgets()
    {
        this.title.hide();
        this.description.hide();
        this.nbFrame.hide();
        this.image.addStyleName("hidden");
        this.taskGrid.hide();
        this.infoContainer.setStyleAttribute("margin-top", "0px");
        this.infoTextContainer.setStyleAttribute("padding", "0px");
        this.description.setStyleAttribute("margin-top", "0px");
    }

    /**
     * Make casting task grid size bigger.
     */
    public void maximizeCastingTaskGrid()
    {
        this.resizeGridsHeight();
        this.maximizeCastingTaskGridButton.setText("minimize");
    }

    /**
     * Set casting task grid at his default size.
     */
    public void minimizeCastingTaskGrid()
    {
        this.resizeGridsHeight();
        this.maximizeCastingTaskGridButton.setText("maximize");
    }

    /**
     * Display constituent grid and hide shot grid.
     */
    public void displayConstituentGrid()
    {
        this.castingConstituentGrid.show();
        this.castingShotGrid.hide();
    }

    /**
     * Display shot grid and hide constituent grid.
     */
    public void displayShotGrid()
    {
        this.castingShotGrid.show();
        this.castingConstituentGrid.hide();
    }

    /**
     * When work object is refreshed, it reloads task store and casting store.
     */
    @Override
    protected void onWorkObjectRefreshed()
    {
        super.onWorkObjectRefreshed();
        controller.handleEvent(IdentityDialogEvents.WORK_OBJECT_REFRESHED);
    }

    /**
     * When tasks are loaded, corresponding approval data are loaded.
     */
    @Override
    protected void onTasksLoaded()
    {
        this.taskGrid.mask(GXT.MESSAGES.loadMask_msg());
        this.model.getNoteStore().reload();
    }

    @Override
    protected void setNbFrames()
    {
        if (FieldUtils.isShot(this.workObject) && !this.model.isCastingTaskGridMaximized())
        {
            Integer frames = this.workObject.get(ShotModelData.SHOT_NBFRAME);
            if (frames != null)
                this.nbFrame.setText("Number of frames: " + frames.toString());
            else
                this.nbFrame.setText("Number of frames: 0");
            this.nbFrame.show();
        }
        else
            this.nbFrame.hide();
    }

    protected void setAdditionalStyles()
    {
        this.setSize(1100, 782);
        this.taskTypeCombo.setWidth(300);
        this.setCompositionWidgets();
        this.setScrollMode(Scroll.NONE);

        this.setResizable(true);
        this.setMinHeight(600);
        this.setMinWidth(650);
    }

    /**
     * Resize grid widths to make them fill the whole dialog width.
     */
    protected void resizeGridsWidth()
    {
        int newWidth = getWidth() - 24;
        taskGrid.setWidth(newWidth);
        castingTaskGrid.setWidth(newWidth);
        castingConstituentGrid.setWidth(newWidth);
        castingShotGrid.setWidth(newWidth);
    }

    /**
     * Resize casting task grid height, to make it fill the whole dialog.
     */
    protected void resizeGridsHeight()
    {
        if (this.model.isCastingTaskGridMaximized())
        {
            this.castingPanel.setHeight(getHeight() - 42);
            this.castingConstituentGrid.setHeight(getHeight() - 72);
            this.castingShotGrid.setHeight(getHeight() - 72);
            this.castingTaskGrid.setHeight(getHeight() - 105);
            this.layout();
        }
        else
        {
            this.castingPanel.setHeight(getHeight() - 352);
            this.castingConstituentGrid.setHeight(getHeight() - 382);
            this.castingShotGrid.setHeight(getHeight() - 382);
            this.castingTaskGrid.setHeight(getHeight() - 415);
            this.layout();
        }
    }

    /**
     * Register to controllers events occuring on data store or on widgets.
     */
    protected void setCompositionListeners()
    {
        this.model.getNoteStore().addLoadListener(new LocalLoadListener(IdentityDialogEvents.NOTES_LOADED, controller));
        this.model.getCastingTaskStore().addLoadListener(
                new LocalLoadListener(IdentityDialogEvents.CASTING_TASKS_LOADED, controller));
        this.model.getCastingNoteStore().addLoadListener(
                new LocalLoadListener(IdentityDialogEvents.CASTING_NOTES_LOADED, controller));

        this.castingTaskGrid.addListener(Events.CellDoubleClick, new LocalCellDoubleClickListener<TaskModelData>(
                IdentityDialogEvents.COMPOSITION_DBLE_CLICKED, controller));

        this.taskTypeCombo.addListener(Events.Change, new LocalListener(
                IdentityDialogEvents.COMPOSITION_TASK_TYPE_CHANGED, controller));
    }

    /**
     * Set a listener on resize event to make dialog filled by grids (synchronize dialog and grids resizing).
     */
    protected void setListeners()
    {
        this.addListener(Events.Resize, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                resizeGridsWidth();
                resizeGridsHeight();
            }
        });
    }

    /**
     * Set casting task grids and task type combo box.
     */
    protected void setCompositionWidgets()
    {
        this.taskGrid.setHeight(200);
        this.castingPanel.setHeight(430);

        TabItem item = new TabItem("Casting Tasks");
        Text helpText = new Text("Browse casting tasks by selecting a task type: ");
        helpText.setStyleAttribute("padding", "5px");
        this.taskTypeComboContainer.add(helpText, new ColumnData(-1));
        this.taskTypeComboContainer.add(this.taskTypeCombo, new ColumnData(-1));
        this.taskTypeComboContainer.add(this.maximizeCastingTaskGridButton, new ColumnData(-1));
        this.maximizeCastingTaskGridButton.addSelectionListener(new LocalButtonClickListener(
                IdentityDialogEvents.MAXIMIZE_CLICKED, controller));

        item.add(this.taskTypeComboContainer, new MarginData(5));
        item.add(this.castingTaskGrid);
        this.castingTaskGrid.setBorders(true);
        this.castingTaskGrid.setAutoExpandColumn(WorkObjectModelData.DESCRIPTION_FIELD);
        this.castingTaskGrid.setAutoExpandMax(2000);
        this.castingTaskGrid.setGroupingOn();
        this.castingTaskGrid.getView();
        this.castingPanel.add(item);

        item = new TabItem("Casting");
        this.castingConstituentGrid.setAutoExpandColumn(WorkObjectModelData.DESCRIPTION_FIELD);
        this.castingConstituentGrid.setAutoExpandMax(2000);
        this.castingConstituentGrid.setGroupingOn();
        this.castingShotGrid.setAutoExpandColumn(WorkObjectModelData.DESCRIPTION_FIELD);
        this.castingShotGrid.setAutoExpandMax(2000);
        this.castingShotGrid.setGroupingOn();
        item.add(castingConstituentGrid);
        item.add(castingShotGrid);

        this.castingPanel.add(item);
        this.add(castingPanel, new MarginData(5));

        this.setCompositionListeners();
    }

    /**
     * @return Column descriptions for composition grid.
     */
    protected ColumnModel getCompositionConstituentColumnModel()
    {
        List<ColumnConfig> columns = new ArrayList<ColumnConfig>();

        ColumnConfig path = GridUtils.addColumnConfig(columns, ConstituentModelData.CONSTITUENT_CATEGORIESNAMES,
                "Path", 160);
        path.setRenderer(new PaddingTextRenderer<Hd3dModelData>());
        ColumnConfig name = GridUtils.addColumnConfig(columns, WorkObjectModelData.LABEL_FIELD,
                COMMON_CONSTANTS.Name(), 120);
        name.setRenderer(new PaddingTextRenderer<Hd3dModelData>());
        ColumnConfig description = GridUtils.addColumnConfig(columns, WorkObjectModelData.DESCRIPTION_FIELD,
                COMMON_CONSTANTS.Description(), 695);
        description.setRenderer(new LongTextRenderer<Hd3dModelData>());

        return new ColumnModel(columns);
    }

    /**
     * @return Column descriptions for composition grid.
     */
    protected ColumnModel getCompositionShotColumnModel()
    {
        List<ColumnConfig> columns = new ArrayList<ColumnConfig>();

        ColumnConfig path = GridUtils.addColumnConfig(columns, ShotModelData.SHOT_SEQUENCESNAMES, "Path", 160);
        path.setRenderer(new PaddingTextRenderer<Hd3dModelData>());
        ColumnConfig name = GridUtils.addColumnConfig(columns, WorkObjectModelData.LABEL_FIELD,
                COMMON_CONSTANTS.Name(), 120);
        name.setRenderer(new PaddingTextRenderer<Hd3dModelData>());
        ColumnConfig description = GridUtils.addColumnConfig(columns, WorkObjectModelData.DESCRIPTION_FIELD,
                COMMON_CONSTANTS.Description(), 695);
        description.setRenderer(new LongTextRenderer<Hd3dModelData>());

        return new ColumnModel(columns);
    }

    /**
     * @return Column descriptions for composition task grid.
     */
    protected ColumnModel getCompositionTaskGridColumnModel()
    {
        ColumnModel cm = super.getTaskGridColumnModel();

        ColumnConfig workObjectName = GridUtils.addColumnConfig(1, cm.getColumns(),
                TaskModelData.WORK_OBJECT_NAME_FIELD, COMMON_CONSTANTS.Name(), 100);
        workObjectName.setRenderer(new CommentRenderer<TaskModelData>(this.castingTaskGrid));

        return cm;
    }

}
