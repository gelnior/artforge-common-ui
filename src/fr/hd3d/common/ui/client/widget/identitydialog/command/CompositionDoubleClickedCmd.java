package fr.hd3d.common.ui.client.widget.identitydialog.command;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.widget.identitydialog.IIdentityDialog;
import fr.hd3d.common.ui.client.widget.identitydialog.IdentityDialogModel;


/**
 * When a work object from casting task grid is double clicked, it displays the corresponding work object data via a
 * simple identity dialog.
 * 
 * @author HD3D
 */
public class CompositionDoubleClickedCmd extends IdentityDialogCmd
{

    public CompositionDoubleClickedCmd(IdentityDialogModel model, IIdentityDialog view)
    {
        super(model, view);
    }

    public void execute(AppEvent event)
    {
        TaskModelData task = event.getData();
        Hd3dModelData workObject = null;

        if (FieldUtils.isShot(this.model.getCurrentWorkObject()))
            workObject = this.model.getConstituentStore().findModel(Hd3dModelData.ID_FIELD, task.getWorkObjectId());
        else
            workObject = this.model.getShotStore().findModel(Hd3dModelData.ID_FIELD, task.getWorkObjectId());

        if (workObject != null)
            this.view.displaySimpleDialog(workObject);
        else
            Logger.error("No work object match the given task");
    }
}
