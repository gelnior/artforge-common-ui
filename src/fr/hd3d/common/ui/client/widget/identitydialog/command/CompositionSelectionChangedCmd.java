package fr.hd3d.common.ui.client.widget.identitydialog.command;

import java.util.List;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.widget.identitydialog.IdentityDialog;
import fr.hd3d.common.ui.client.widget.identitydialog.IdentityDialogModel;


/**
 * When casting selection changed, it configures casting task store and reloads casting task grid data.
 * 
 * @author HD3D
 */
public class CompositionSelectionChangedCmd extends IdentityDialogCmd
{

    public CompositionSelectionChangedCmd(IdentityDialogModel model, IdentityDialog view)
    {
        super(model, view);
    }

    public void execute(AppEvent event)
    {
        List<Hd3dModelData> selection = event.getData();
        if (CollectionUtils.isNotEmpty(selection))
        {
            this.view.clearTaskTypeCombo();

            this.model.reloadCastingTasks(selection.get(0));
        }
    }
}
