package fr.hd3d.common.ui.client.widget.identitydialog;

import com.extjs.gxt.ui.client.store.Store;
import com.extjs.gxt.ui.client.store.StoreSorter;

import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;


/**
 * This comparator sort task with the given property, then it compares the work object name then the task type name.
 * 
 * @author HD3D
 */
public class CastingTaskComparator extends StoreSorter<TaskModelData>
{
    @Override
    public int compare(Store<TaskModelData> store, TaskModelData m1, TaskModelData m2, String property)
    {
        if (property != null)
        {
            // Sort on given property.
            Object v1 = m1.get(property);
            Object v2 = m2.get(property);
            int comparison = comparator.compare(v1, v2);

            if (comparison == 0)
            {
                // Then sort on work object name.
                String workObjectName1 = m1.getWorkObjectName();
                String workObjectName2 = m2.getWorkObjectName();

                if (workObjectName1 == null)
                    return -1;

                comparison = workObjectName1.compareTo(workObjectName2);
                if (comparison == 0)
                {
                    // Then sort on task type name.
                    String taskTypeName1 = m1.getTaskTypeName();
                    String taskTypeName2 = m2.getTaskTypeName();

                    if (taskTypeName1 == null)
                        comparison = -1;
                    else
                        comparison = taskTypeName1.compareTo(taskTypeName2);
                }
            }

            return comparison;
        }
        return comparator.compare(m1, m2);
    }
}
