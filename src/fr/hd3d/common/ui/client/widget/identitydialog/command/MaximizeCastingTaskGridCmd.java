package fr.hd3d.common.ui.client.widget.identitydialog.command;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.widget.identitydialog.IIdentityDialog;
import fr.hd3d.common.ui.client.widget.identitydialog.IdentityDialogModel;


/**
 * Hide work object basic infos and make casting task grid fills the whole space.
 * 
 * 
 * @author HD3D
 */
public class MaximizeCastingTaskGridCmd extends IdentityDialogCmd
{
    public MaximizeCastingTaskGridCmd(IdentityDialogModel model, IIdentityDialog view)
    {
        super(model, view);
    }

    public void execute(AppEvent event)
    {

        this.model.toggleIsMaximized();
        if (this.model.isCastingTaskGridMaximized())
        {
            this.view.hideInfoWidgets();
            this.view.maximizeCastingTaskGrid();
        }
        else
        {
            this.view.showInfoWidgets();
            this.view.minimizeCastingTaskGrid();
        }
    }
}
