package fr.hd3d.common.ui.client.widget.identitydialog.command;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.widget.identitydialog.IIdentityDialog;
import fr.hd3d.common.ui.client.widget.identitydialog.IdentityDialogModel;


/**
 * When casting tasks are loaded, the casting note store is reloaded.
 * 
 * @author HD3D
 */
public class CastingTasksLoadedCmd extends IdentityDialogCmd
{

    public CastingTasksLoadedCmd(IdentityDialogModel model, IIdentityDialog view)
    {
        super(model, view);
    }

    public void execute(AppEvent event)
    {
        this.model.getCastingNoteStore().reload();
    }

}
