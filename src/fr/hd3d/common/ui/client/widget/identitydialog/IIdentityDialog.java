package fr.hd3d.common.ui.client.widget.identitydialog;

import java.util.List;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;


public interface IIdentityDialog extends ISimpleDialog
{

    /**
     * Display info dialog, registers work object, configures data store then reload task grid for given work object.
     * 
     * @param workObject
     */
    public abstract void show(Hd3dModelData workObject, ProjectModelData project);

    /**
     * Display a simple identity dialog (no casting data) for a given work object.
     * 
     * @param castingWorkObject
     *            The work object for which the dialog is displayed.
     */
    public abstract void displaySimpleDialog(Hd3dModelData castingWorkObject);

    /**
     * @return Task type selected in combo box.
     */
    public abstract List<TaskTypeModelData> getSelectedTaskTypes();

    /**
     * Clear the task type combo box.
     */
    public abstract void clearTaskTypeCombo();

    /**
     * Clear all Data from the identity dialog.
     */
    public abstract void clearAll();

    /**
     * Show widgets displaying basic informations on work object.
     */
    public abstract void showInfoWidgets();

    /**
     * Hide widgets displaying basic informations on work object.
     */
    public abstract void hideInfoWidgets();

    /**
     * Make casting task grid size bigger.
     */
    public abstract void maximizeCastingTaskGrid();

    /**
     * Set casting task grid at his default size.
     */
    public abstract void minimizeCastingTaskGrid();

    /**
     * Display constituent grid and hide shot grid.
     */
    public abstract void displayConstituentGrid();

    /**
     * Display shot grid and hide constituent grid.
     */
    public abstract void displayShotGrid();

}
