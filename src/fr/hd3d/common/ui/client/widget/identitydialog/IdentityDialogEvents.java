package fr.hd3d.common.ui.client.widget.identitydialog;

import com.extjs.gxt.ui.client.event.EventType;


/**
 * Events raised by identity dialog.
 * 
 * @author HD3D
 */
public class IdentityDialogEvents
{

    public static final EventType DIALOG_SHOWED = new EventType();
    public static final EventType WORK_OBJECT_REFRESHED = new EventType();
    public static final EventType NOTES_LOADED = new EventType();
    public static final EventType CASTING_TASKS_LOADED = new EventType();
    public static final EventType CASTING_NOTES_LOADED = new EventType();
    public static final EventType COMPOSITION_DBLE_CLICKED = new EventType();
    public static final EventType COMPOSITION_TASK_TYPE_CHANGED = new EventType();
    public static final EventType MAXIMIZE_CLICKED = new EventType();

}
