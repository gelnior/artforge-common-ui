package fr.hd3d.common.ui.client.widget.identitydialog.command;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.widget.identitydialog.IIdentityDialog;
import fr.hd3d.common.ui.client.widget.identitydialog.IdentityDialogModel;


/**
 * When work object is refreshed, composition data store are reloaded.
 * 
 * @author HD3D
 */
public class WorkObjectRefreshedCmd extends IdentityDialogCmd
{

    public WorkObjectRefreshedCmd(IdentityDialogModel model, IIdentityDialog view)
    {
        super(model, view);
    }

    public void execute(AppEvent event)
    {
        if (FieldUtils.isShot(this.model.getCurrentWorkObject().getSimpleClassName()))
            this.model.getConstituentStore().reload();
        else if (FieldUtils.isConstituent(this.model.getCurrentWorkObject().getSimpleClassName()))
            this.model.getShotStore().reload();

        this.model.configureTaskTypeStore();
        this.model.reloadTaskTypeStore();
    }

}
