package fr.hd3d.common.ui.client.widget.identitydialog;

import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.modeldata.reader.ReaderFactory;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.util.FieldUtils;


/**
 * Model handling identity dialog data.
 * 
 * @author HD3D
 */
public class IdentityDialogModel extends SimpleIdentityDialogModel
{
    /** Work object currently displayed. */
    private Hd3dModelData workObject;

    /** Store containing shots linked to current work object via compositions (only if work object is a constituent). */
    protected ServiceStore<ShotModelData> shotStore = new ServiceStore<ShotModelData>(ReaderFactory.getShotReader());
    /**
     * Store containing constituents linked to current work object via compositions (only if work object is a
     * constituent).
     */
    protected ServiceStore<ConstituentModelData> constituentStore = new ServiceStore<ConstituentModelData>(
            ReaderFactory.getConstituentReader());

    /** Store containing data for casting task grids. */
    protected ServiceStore<TaskModelData> castingTaskStore = new ServiceStore<TaskModelData>(
            ReaderFactory.getTaskReader());

    /** Store used to complete casting tasks with notes data. */
    protected ServiceStore<ApprovalNoteModelData> castingNoteStore = new ServiceStore<ApprovalNoteModelData>(
            ReaderFactory.getApprovalNoteReader());

    /** Store used to fill task type combo box. */
    private final ServiceStore<TaskTypeModelData> taskTypeStore = new ServiceStore<TaskTypeModelData>(
            ReaderFactory.getTaskTypeReader());

    /** Project of current work object (needed to retrieve right task types. */
    private ProjectModelData project;

    /** True if casting task grid is maximized. */
    private Boolean isCastingTaskGridMaximized = Boolean.FALSE;

    /** Default constructor. */
    public IdentityDialogModel()
    {
        this.castingTaskStore.groupBy(TaskModelData.WORK_OBJECT_PARENTS_NAME_FIELD);
        this.castingTaskStore.setSortField(RecordModelData.NAME_FIELD);
        this.castingTaskStore.setStoreSorter(new CastingTaskComparator());
        this.constituentStore.groupBy(ConstituentModelData.CONSTITUENT_CATEGORIESNAMES);
        this.shotStore.groupBy(ShotModelData.SHOT_SEQUENCESNAMES);
    }

    /**
     * @return True if casting task grid is maximized.
     */
    public Boolean isCastingTaskGridMaximized()
    {
        return isCastingTaskGridMaximized;
    }

    /**
     * @return project of current work object.
     */
    public ProjectModelData getProject()
    {
        return this.project;
    }

    /**
     * Set project of current work object.
     * 
     * @param project
     *            The project to set.
     */
    public void setProject(ProjectModelData project)
    {
        this.project = project;
    }

    /**
     * @return Task type store used to fill task type combo box.
     */
    public ServiceStore<TaskTypeModelData> getTaskTypeStore()
    {
        return this.taskTypeStore;
    }

    /**
     * @return Store containing shots linked to current work object via compositions (only if work object is a
     *         constituent).
     */
    public ServiceStore<ShotModelData> getShotStore()
    {
        return shotStore;
    }

    /**
     * @return Store containing constituents linked to current work object via compositions (only if work object is a
     *         constituent).
     */
    public ServiceStore<ConstituentModelData> getConstituentStore()
    {
        return constituentStore;
    }

    /**
     * @return Store containing tasks linked to current object via its compositions.
     */
    public ServiceStore<TaskModelData> getCastingTaskStore()
    {
        return this.castingTaskStore;
    }

    /**
     * @return Store containing approval notes linked to tasks of current work object.
     */
    @Override
    public ServiceStore<ApprovalNoteModelData> getNoteStore()
    {
        return this.noteStore;
    }

    /**
     * @return Store containing approval notes linked to tasks of work object linked to current work object via
     *         compositions.
     */
    public ServiceStore<ApprovalNoteModelData> getCastingNoteStore()
    {
        return this.castingNoteStore;
    }

    /**
     * @return Work object currently displayed.
     */
    public Hd3dModelData getCurrentWorkObject()
    {
        return this.workObject;
    }

    /**
     * Sets the work object currently displayed.
     * 
     * @param workObject
     *            The work object to set as current work object.
     */
    public void setCurrentWorkObject(Hd3dModelData workObject)
    {
        this.workObject = workObject;
    }

    /**
     * Clear all stores.
     */
    public void clearAll()
    {
        this.getNoteStore().removeAll();
        this.getTaskStore().removeAll();
        this.getConstituentStore().removeAll();
        this.getShotStore().removeAll();
        this.getCastingTaskStore().removeAll();
        this.getCastingNoteStore().removeAll();
    }

    /**
     * Update path of stores to make them retrieve the right data.
     */
    public void updateTaskStoresConfiguration()
    {
        String path = workObject.getPath();
        if (!path.endsWith("/"))
        {
            path += "/";
        }

        this.taskStore.setPath(path + ServicesPath.TASKS);

        if (FieldUtils.isShot(this.workObject.getSimpleClassName()))
            this.constituentStore.setPath(path + ServicesPath.CONSTITUENTS);

        else if (FieldUtils.isConstituent(this.workObject.getSimpleClassName()))
            this.shotStore.setPath(path + ServicesPath.SHOTS);

        else
            Logger.log("Work object class name is wrong, it cannot display casting data for this work object",
                    new Exception());
    }

    /**
     * Update note store path.
     */
    public void updateNoteStoreConfiguration()
    {
        String path = workObject.getPath();
        if (!path.endsWith("/"))
        {
            path += "/";
        }
        this.noteStore.setPath(path + ServicesPath.APPROVAL_NOTES);
    }

    /**
     * Reload tasks and notes linked to given work object. When notes are loaded, their data are use to add more details
     * on displayed tasks (last note written for each task).
     * 
     * @param castingWorkObject
     *            The object of which tasks and notes are loaded.
     */
    public void reloadCastingTasks(Hd3dModelData castingWorkObject)
    {
        this.castingNoteStore.removeAll();
        this.castingTaskStore.removeAll();

        this.castingTaskStore.clearParameters();
        this.castingTaskStore.setPath(castingWorkObject.getDefaultPath() + "/" + ServicesPath.TASKS);

        this.castingNoteStore.clearParameters();
        this.castingNoteStore.setPath(castingWorkObject.getDefaultPath() + "/" + ServicesPath.APPROVAL_NOTES);

        this.castingTaskStore.reload();
    }

    /**
     * Set the right path to retrieve only task types for given project and for right entity (only constituents are
     * casted in shots and vice versa).
     */
    public void configureTaskTypeStore()
    {
        this.taskTypeStore.setPath(ServicesPath.PROJECTS + project.getId() + "/" + ServicesPath.TASKTYPES);
        this.taskTypeStore.clearParameters();
        if (FieldUtils.isConstituent(workObject))
            this.taskTypeStore.addEqConstraint(TaskTypeModelData.ENTITY_NAME_FIELD, ShotModelData.SIMPLE_CLASS_NAME);
        else if (FieldUtils.isShot(workObject))
            this.taskTypeStore.addEqConstraint(TaskTypeModelData.ENTITY_NAME_FIELD,
                    ConstituentModelData.SIMPLE_CLASS_NAME);
    }

    /** Reload task type store data. */
    public void reloadTaskTypeStore()
    {
        this.taskTypeStore.reload();
    }

    /**
     * Change is maximized token state (if true put it to false and vice-versa).
     */
    public void toggleIsMaximized()
    {
        this.isCastingTaskGridMaximized = !this.isCastingTaskGridMaximized;
    }

}
