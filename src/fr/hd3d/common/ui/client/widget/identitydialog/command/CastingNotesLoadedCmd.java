package fr.hd3d.common.ui.client.widget.identitydialog.command;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.calendar.DatetimeUtil;
import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.WorkObjectModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.widget.identitydialog.IIdentityDialog;
import fr.hd3d.common.ui.client.widget.identitydialog.IdentityDialogModel;


/**
 * TODO: rebuild this command.
 * 
 * @author HD3D
 */
public class CastingNotesLoadedCmd extends IdentityDialogCmd
{
    public CastingNotesLoadedCmd(IdentityDialogModel model, IIdentityDialog view)
    {
        super(model, view);
    }

    public void execute(AppEvent event)
    {
        this.updateTaskDataForTaskTypeMode();
    }

    protected void updateTaskDataForTaskTypeMode()
    {
        FastMap<ApprovalNoteModelData> taskNotes = this.getMostRecentNoteMapForEachTasks();

        this.setDescriptionFieldForEachTaskWithMostRecentNote(taskNotes);
    }

    private FastMap<ApprovalNoteModelData> getMostRecentNoteMapForEachTasks()
    {
        FastMap<ApprovalNoteModelData> taskNotes = new FastMap<ApprovalNoteModelData>();

        for (ApprovalNoteModelData note : this.model.getCastingNoteStore().getModels())
        {
            if (note.getBoundEntityId() != null)
            {
                String workObjectId = note.getBoundEntityId().toString();
                if (taskNotes.get(workObjectId + "-" + note.getTaskTypeId()) == null
                        || taskNotes.get(workObjectId + "-" + note.getTaskTypeId()).getDate().before(note.getDate()))
                {
                    taskNotes.put(workObjectId + "-" + note.getTaskTypeId(), note);
                }
            }
            else
            {
                Logger.log("Note linked to no work object has been retrieved.", new Exception());
            }
        }

        return taskNotes;
    }

    private void setDescriptionFieldForEachTaskWithMostRecentNote(FastMap<ApprovalNoteModelData> taskNotes)
    {
        for (TaskModelData task : this.model.getCastingTaskStore().getModels())
        {
            if (task.getWorkObjectId() != null)
            {
                String workObjectId = task.getWorkObjectId().toString();
                ApprovalNoteModelData note = taskNotes.get(workObjectId + "-" + task.getTaskTypeId());
                if (note != null)
                {
                    String date = DatetimeUtil.formatDate(fr.hd3d.common.client.DateFormat.DATE_TIME_STRING,
                            note.getDate());
                    task.set(WorkObjectModelData.DESCRIPTION_FIELD, date + ": " + note.getComment());
                    this.model.getCastingTaskStore().update(task);
                }
            }
            else
            {
                Logger.log("Identity Dialog : Some tasks have no work object", new Exception());
            }
        }
    }
}
