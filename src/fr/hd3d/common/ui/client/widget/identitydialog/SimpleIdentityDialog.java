package fr.hd3d.common.ui.client.widget.identitydialog;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.event.LoadListener;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.Text;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.layout.ColumnData;
import com.extjs.gxt.ui.client.widget.layout.ColumnLayout;
import com.extjs.gxt.ui.client.widget.layout.MarginData;
import com.extjs.gxt.ui.client.widget.layout.RowLayout;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.Image;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.WorkObjectModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.callback.GetModelDataCallback;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.util.GridUtils;
import fr.hd3d.common.ui.client.util.HtmlUtils;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.TaskStatusRenderer;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.TaskTypeRenderer;
import fr.hd3d.common.ui.client.widget.grid.BaseGrid;
import fr.hd3d.common.ui.client.widget.grid.renderer.CommentRenderer;
import fr.hd3d.common.ui.client.widget.grid.renderer.PaddingTextRenderer;
import fr.hd3d.common.ui.client.widget.thumbnaileditor.ThumbnailPath;


/**
 * Dialog that displays for a given work object, tasks linked to the object and last note written for these tasks..
 * 
 * @author HD3D
 */
public class SimpleIdentityDialog extends Dialog implements ISimpleDialog
{
    /** Text used as loading indicator. */
    protected static final String LOADING = "Loading...";

    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);

    /** The work object of which data are displayed. */
    protected Hd3dModelData workObject;

    /** Layout element to display work object infos. */
    protected LayoutContainer infoContainer = new LayoutContainer(new ColumnLayout());
    /** Layout element to display work object infos. */
    protected LayoutContainer infoTextContainer = new LayoutContainer(new RowLayout());

    /** Work object name set as big text. */
    protected Text title = new Text(LOADING);
    /** Description of the work object. */
    protected Text description = new Text(LOADING);
    /** Number of frame of the work object. */
    protected Text nbFrame = new Text(LOADING);
    /** Image of the work object. */
    protected Image image = new Image();

    /** Grid containing tasks linked to current work object. */
    protected BaseGrid<TaskModelData> taskGrid;

    /** Model handling simple dialog data. */
    private SimpleIdentityDialogModel model = new SimpleIdentityDialogModel();

    /**
     * Dialog constructor : set styles and configure data stores.
     */
    public SimpleIdentityDialog()
    {
        this.setStyles();
        this.setBaseInfo();
        this.setTaskGrid();
    }

    /**
     * Display info dialog. Clear all data, then reload data for <i>workObject</i>
     * 
     * @param workObject
     *            Data from this work object will be displayed inside identity dialog.
     */
    public void show(Hd3dModelData workObject)
    {
        this.clearAll();
        this.workObject = workObject;
        String path = workObject.getPath();
        if (!path.endsWith("/"))
        {
            path += "/";
        }

        this.model.getTaskStore().setPath(path + ServicesPath.TASKS);
        this.model.getNoteStore().setPath(path + ServicesPath.APPROVAL_NOTES);

        this.reloadData();
        super.show();
    }

    /**
     * Clear all Data from the identity dialog.
     */
    public void clearAll()
    {
        this.setHeading(LOADING);
        title.setText(LOADING);
        description.setText(LOADING);
        nbFrame.setText(LOADING);

        this.model.getTaskStore().removeAll();
    }

    /**
     * Reload all data displayed inside identity dialog. It starts by main data, then it loads, tasks and notes data.
     */
    public void reloadData()
    {
        String path = ThumbnailPath.getPath(workObject, true);
        image.setUrl(path);

        workObject.refresh(new GetModelDataCallback(workObject) {
            @Override
            protected void onRecordReturned(List<Hd3dModelData> records)
            {
                Hd3dModelData newRecord = records.get(0);
                this.record.updateFromModelData(newRecord);
                onWorkObjectRefreshed();
            }
        });
    }

    /**
     * Hide loading indicator of task grid.
     */
    public void unmaskTaskGrid()
    {
        this.taskGrid.unmask();
    }

    /**
     * When work object is refreshed, it reloads task store and note store. Basic infos of work object are displayed :
     * name, path, description. If the work object is a shot, its number of frame is displayed.
     */
    protected void onWorkObjectRefreshed()
    {
        String name = FieldUtils.getName(workObject);
        this.setHeading(FieldUtils.getWorkObjectPath(workObject) + " " + name);
        this.title.setText(name);

        this.setNbFrames();

        this.description.setText(HtmlUtils.changeToHyperText((String) workObject
                .get(ConstituentModelData.CONSTITUENT_DESCRIPTION)));

        this.model.getTaskStore().reload();
    }

    /**
     * Displays number of frames for current work object, if this object is a shot.
     */
    protected void setNbFrames()
    {
        if (FieldUtils.isShot(this.workObject))
        {
            Integer frames = this.workObject.get(ShotModelData.SHOT_NBFRAME);
            if (frames != null)
                this.nbFrame.setText("Number of frames: " + frames.toString());
            else
                this.nbFrame.setText("Number of frames: 0");
            this.nbFrame.show();
        }
        else
            this.nbFrame.hide();
    }

    /**
     * Set task grid above basic info and register load listeners on task and note stores. When tasks are loaded, notes
     * are loaded then they are processed to be display last written note for each tasks.
     */
    protected void setTaskGrid()
    {
        this.taskGrid = new BaseGrid<TaskModelData>(this.model.getTaskStore(), getTaskGridColumnModel());
        this.add(this.taskGrid, new MarginData(5));
        this.taskGrid.setBorders(true);
        this.taskGrid.setHeight(300);
        this.taskGrid.setSelectionModel(null);
        this.taskGrid.setAutoExpandColumn(WorkObjectModelData.DESCRIPTION_FIELD);
        this.taskGrid.setAutoExpandMax(1000);
        this.taskGrid.setTrackMouseOver(false);

        this.model.getTaskStore().addLoadListener(new LoadListener() {
            @Override
            public void loaderLoad(LoadEvent le)
            {
                onTasksLoaded();
            }
        });

        this.model.getNoteStore().addLoadListener(new LoadListener() {
            @Override
            public void loaderLoad(LoadEvent le)
            {
                onNotesLoaded();
            }
        });
    }

    /**
     * When notes are loaded, task list is updated with note data (for each task, last note is displayed in the last
     * note cell). Then grid is unmasked.
     */
    protected void onNotesLoaded()
    {
        this.model.updateData();
        this.unmaskTaskGrid();
    }

    /**
     * When tasks are loaded, corresponding approval data are loaded.
     */
    protected void onTasksLoaded()
    {
        this.model.getNoteStore().reload();
    }

    /**
     * Display base info for current work object : name, path, description and thumbnail.
     */
    protected void setBaseInfo()
    {
        infoTextContainer.add(title);

        infoTextContainer.add(nbFrame);
        infoTextContainer.add(description);

        image.setHeight("100px");
        image.setStyleName("basic-preview-image");
        infoContainer.add(infoTextContainer, new ColumnData(400));
        infoContainer.add(image, new ColumnData());
        infoContainer.setStyleAttribute("margin-top", "5px");
        infoTextContainer.setStyleAttribute("padding", "5px");

        this.title.setStyleAttribute("font-weight", "bold");
        this.title.setStyleAttribute("font-size", "20px");
        this.title.setStyleAttribute("display", "inline");
        this.description.setStyleAttribute("margin-top", "15px");

        this.add(infoContainer);
    }

    /**
     * @return Column list for task grid.
     */
    protected ColumnModel getTaskGridColumnModel()
    {
        List<ColumnConfig> columns = new ArrayList<ColumnConfig>();

        ColumnConfig typeColumn = GridUtils.addColumnConfig(columns, TaskModelData.TASK_TYPE_NAME_FIELD,
                COMMON_CONSTANTS.Type(), 140);
        typeColumn.setRenderer(new TaskTypeRenderer());

        ColumnConfig statusColumn = GridUtils.addColumnConfig(columns, TaskModelData.STATUS_FIELD,
                COMMON_CONSTANTS.Status(), 140);
        statusColumn.setRenderer(new TaskStatusRenderer<Hd3dModelData>());

        ColumnConfig worker = GridUtils.addColumnConfig(columns, TaskModelData.WORKER_NAME_FIELD,
                COMMON_CONSTANTS.Worker(), 120);
        worker.setRenderer(new PaddingTextRenderer<Hd3dModelData>());

        ColumnConfig description = GridUtils.addColumnConfig(columns, WorkObjectModelData.DESCRIPTION_FIELD,
                "Last note");
        description.setRenderer(new CommentRenderer<TaskModelData>(this.taskGrid));

        return new ColumnModel(columns);
    }

    /**
     * Set dialog styles.
     */
    protected void setStyles()
    {
        this.setModal(false);
        this.setClosable(true);
        this.setResizable(false);
        this.setSize(900, 450);

        this.setLayout(new RowLayout());

        this.setButtons("");
    }

    /**
     * Set model for simple identity dialog data.
     * 
     * @param model
     *            Model handling data for simple identity dialog.
     */
    public void setModel(SimpleIdentityDialogModel model)
    {
        this.model = model;

        this.model.getTaskStore().addLoadListener(new LoadListener() {
            @Override
            public void loaderLoad(LoadEvent le)
            {
                onTasksLoaded();
            }
        });

        this.model.getNoteStore().addLoadListener(new LoadListener() {
            @Override
            public void loaderLoad(LoadEvent le)
            {
                onNotesLoaded();
            }
        });

        this.taskGrid.reconfigure(this.model.getTaskStore(), this.getTaskGridColumnModel());
    }
}
