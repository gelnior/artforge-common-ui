package fr.hd3d.common.ui.client.widget;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.ListView;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.widget.identitysheet.IdentitySheetDisplayer;
import fr.hd3d.common.ui.client.widget.thumbnaileditor.ThumbnailPath;


/**
 * Mosaic List displays HD3D instance as a list of thumbnails. The instance name is written above each thumbnail. A
 * double click on a list element opens the instance identity sheet.
 * 
 * @author HD3D
 */
public class MosaicList extends ListView<Hd3dModelData>
{
    /** The sheet used to display identity sheet. */
    private SheetModelData sheet;

    public MosaicList(ListStore<Hd3dModelData> store)
    {
        this.setStore(store);

        this.setStyles();
        this.setListeners();
    }

    /**
     * @return Sheet used to display identity sheets.
     */
    public SheetModelData getSheet()
    {
        return sheet;
    }

    /**
     * Set sheet used to display identity sheets.
     * 
     * @param sheet
     *            The sheet used to display elements.
     */
    public void setSheet(SheetModelData sheet)
    {
        this.sheet = sheet;
    }

    /**
     * Before displaying the model the thumbnail path is set. It is built from sheet simple class name. Then the name is
     * set. It is built from name field if exists, label field or key field either. If instances are persons the nam is
     * built from first name and last name.
     * 
     * @param model
     *            The model to prepare.
     */
    @Override
    protected Hd3dModelData prepareData(Hd3dModelData model)
    {
        String boundClassName = "";
        if (sheet != null)
        {
            model.setSimpleClassName(sheet.getBoundClassName());
            boundClassName = sheet.getBoundClassName();
        }

        String path = ThumbnailPath.getPath(model, true);
        model.set("path", path);

        String name = "";

        if (PersonModelData.SIMPLE_CLASS_NAME.equals(boundClassName))
        {
            name = (String) model.get(PersonModelData.PERSON_FIRST_NAME_FIELD) + " "
                    + (String) model.get(PersonModelData.PERSON_LAST_NAME_FIELD);
        }
        if (name == null || name.isEmpty())
            name = model.get(RecordModelData.NAME_FIELD);
        if (name == null || name.isEmpty())
            name = model.get(ConstituentModelData.CONSTITUENT_LABEL);
        if (name == null || name.isEmpty())
            name = "No column for name";

        model.set(RecordModelData.NAME_FIELD, name);

        return model;
    }

    /**
     * @return javascript template to display a list element.
     */
    private native String getXTemplate() /*-{
                                                  return ['<tpl for=".">', 
                                                  '<div class="thumb-wrap" id="{name}" style="width: 200px; float : left; border: 1px solid #DDD;  padding : 5px; text-align: center;">', 
                                                  '<div class="thumb"><img src="{path}" style="height: 100px; margin-bottom : 8px;" title="{name}"></div>', 
                                                  '<span class="x-editable">{name}</span></div>', 
                                                  '</tpl>', 
                                                  '<div class="x-clear"></div>'].join("");
                                                  }-*/;

    /**
     * Set the double click listener.
     */
    private void setListeners()
    {
        this.addListener(Events.DoubleClick, new Listener<BaseEvent>() {

            public void handleEvent(BaseEvent be)
            {
                onDoubleClick(be);
            }
        });
    }

    /**
     * When there is a double click on a list element, the element identity sheet is displayed.
     * 
     * @param be
     */
    private void onDoubleClick(BaseEvent be)
    {
        Hd3dModelData selectedModel = this.getSelectionModel().getSelectedItem();
        IdentitySheetDisplayer.displayIdentity(sheet, selectedModel.getId());
    }

    /**
     * Set list styles : borders, layout...
     */
    private void setStyles()
    {
        this.setItemSelector("div.thumb-wrap");
        this.setBorders(false);
        this.setTemplate(getXTemplate());
    }
}
