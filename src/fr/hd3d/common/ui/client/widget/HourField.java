package fr.hd3d.common.ui.client.widget;

public class HourField extends FieldComboBox
{
    /**
     * Default constructor.
     */
    public HourField()
    {
        super();
        this.setWidth(50);

        for (int i = 0; i < 24; i++)
            this.addField(i, i + "h", Integer.valueOf(i));
    }
}
