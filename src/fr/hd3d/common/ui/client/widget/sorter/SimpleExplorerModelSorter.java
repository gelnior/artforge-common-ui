package fr.hd3d.common.ui.client.widget.sorter;

import com.extjs.gxt.ui.client.store.Store;
import com.extjs.gxt.ui.client.store.StoreSorter;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;


public class SimpleExplorerModelSorter<M extends Hd3dModelData> extends StoreSorter<M>
{
    @Override
    public int compare(Store<M> store, M m1, M m2, String property)
    {
        if (m2.getId() == null && m1.getId() == null)
        {
            return 0;
        }
        else if (m2.getId() == null)
        {
            return 1;
        }
        else if (m1.getId() == null)
        {
            return -1;
        }
        return super.compare(store, m1, m2, property);
    };
}
