package fr.hd3d.common.ui.client.widget.proxytree;

import com.extjs.gxt.ui.client.event.SelectionChangedEvent;
import com.extjs.gxt.ui.client.event.SelectionChangedListener;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.Label;
import com.extjs.gxt.ui.client.widget.layout.FillData;
import com.extjs.gxt.ui.client.widget.layout.FillLayout;
import com.extjs.gxt.ui.client.widget.toolbar.ToolBar;

import fr.hd3d.common.ui.client.modeldata.reader.TaskTypeReader;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.widget.ModelDataComboBox;

public class SelectProxyWidget extends ContentPanel
{
    
    private ModelDataComboBox<TaskTypeModelData> taskTypeComboBox;
    private ProxyTree proxyTree;

    public SelectProxyWidget()
    {
        super(new FillLayout());
        this.setHeading("Select Proxy");
        taskTypeComboBox = new ModelDataComboBox<TaskTypeModelData>(new TaskTypeReader());
        proxyTree = new ProxyTree(new ProxyTreeModel());
        ToolBar toolBar = new ToolBar();
        toolBar.add(new Label("Select Task type :"));
        toolBar.add(taskTypeComboBox);
        this.setTopComponent(toolBar);
        this.add(proxyTree, new FillData(0));
        addListeners();
    }

    private void addListeners()
    {
        this.taskTypeComboBox.addSelectionChangedListener(new SelectionChangedListener<TaskTypeModelData>() {
            @Override
            public void selectionChanged(SelectionChangedEvent<TaskTypeModelData> se)
            {
                if (se.getSelectedItem()!= null) {
                    proxyTree.setTaskType(se.getSelectedItem().getId());
                    proxyTree.reload();
                }
            }
        });
    }

    public void setData(Long taskTypeId, Long workObjectId, String boundEntityName)
    {
        this.taskTypeComboBox.setValueById(taskTypeId);
        this.proxyTree.setTaskType(taskTypeId);
        this.proxyTree.setWorkObject(workObjectId, boundEntityName);
    }
    
}
