package fr.hd3d.common.ui.client.widget.proxytree;

import java.util.List;

import com.extjs.gxt.ui.client.data.DataReader;
import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.asset.AssetRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.asset.FileRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.asset.ProxyModelData;
import fr.hd3d.common.ui.client.modeldata.reader.RecordReader;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.Constraints;
import fr.hd3d.common.ui.client.service.parameter.EqConstraint;
import fr.hd3d.common.ui.client.service.proxy.TreeServicesProxy;


public class ProxyTreeServiceProxy extends TreeServicesProxy<RecordModelData>
{
    private final Constraint assetRevisionConstraint = new EqConstraint("assetRevision");
    private final Constraint proxyConstraint = new EqConstraint("fileRevision");
    private final Constraints constraints = new Constraints();
    private final Constraint taskTypeConstraint = new EqConstraint("taskType.id");
    private final Constraint workObjectConstraint = new EqConstraint("assetRevisionGroups.value");
    private final Constraint boundEntityNameConstraint = new EqConstraint("assetRevisionGroups.criteria");
    private Long taskTypeId;
    private Long workObjectId;
    private String boundEntityName;

    public ProxyTreeServiceProxy()
    {
        super(new RecordReader());
    }

    @Override
    public void load(DataReader<List<RecordModelData>> reader, Object loadConfig,
            AsyncCallback<List<RecordModelData>> callback)
    {
        this.constraints.clear();
        this.clearParameters();
        RecordReader recordReader = (RecordReader) this.reader;
        if (loadConfig == null)
        {
            recordReader.setModelType(AssetRevisionModelData.getModelType());
            this.path = ServicesPath.getPath(AssetRevisionModelData.SIMPLE_CLASS_NAME);
            if (taskTypeId != null)
            {
                taskTypeConstraint.setLeftMember(this.taskTypeId);
                constraints.add(taskTypeConstraint);
            }

            if (workObjectId != null)
            {
                workObjectConstraint.setLeftMember(workObjectId.toString());
                constraints.add(workObjectConstraint);
            }
            if (boundEntityName != null)
            {
                boundEntityNameConstraint.setLeftMember(boundEntityName);
                constraints.add(boundEntityNameConstraint);
            }
        }
        else
        {
            RecordModelData recordModelData = (RecordModelData) loadConfig;
            if (AssetRevisionModelData.CLASS_NAME.equals(((RecordModelData) loadConfig).getClassName()))
            {
                recordReader.setModelType(FileRevisionModelData.getModelType());
                this.path = ServicesPath.getPath(FileRevisionModelData.SIMPLE_CLASS_NAME);
                this.clearParameters();
                assetRevisionConstraint.setLeftMember(recordModelData.getId());
                constraints.add(assetRevisionConstraint);
            }
            else if (FileRevisionModelData.CLASS_NAME.equals(((RecordModelData) loadConfig).getClassName()))
            {
                recordReader.setModelType(ProxyModelData.getModelType());
                this.path = ServicesPath.getPath(ProxyModelData.SIMPLE_CLASS_NAME);
                this.clearParameters();
                proxyConstraint.setLeftMember(recordModelData.getId());
                constraints.add(proxyConstraint);
            }
        }
        this.addParameter(constraints);
        super.load(reader, loadConfig, callback);
    }

    public void setTaskType(Long id)
    {
        this.taskTypeId = id;
    }

    public void setWorkObject(Long workObjectId, String boundEntityName)
    {
        this.workObjectId = workObjectId;
        this.boundEntityName = boundEntityName;
    }
}
