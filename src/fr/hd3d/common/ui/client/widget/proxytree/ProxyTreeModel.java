package fr.hd3d.common.ui.client.widget.proxytree;

import com.extjs.gxt.ui.client.data.BaseTreeLoader;
import com.extjs.gxt.ui.client.data.TreeLoader;
import com.extjs.gxt.ui.client.store.TreeStore;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.asset.AssetRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.asset.FileRevisionModelData;


public class ProxyTreeModel
{
    private ProxyTreeServiceProxy proxyTreeServiceProxy;
    private TreeLoader<RecordModelData> loader;
    private TreeStore<RecordModelData> treeStore;

    public ProxyTreeModel()
    {
        proxyTreeServiceProxy = new ProxyTreeServiceProxy();
        loader = new BaseTreeLoader<RecordModelData>(proxyTreeServiceProxy) {
            @Override
            public boolean hasChildren(RecordModelData parent)
            {
                return AssetRevisionModelData.CLASS_NAME.equals(parent.getClassName())
                        || FileRevisionModelData.CLASS_NAME.equals(parent.getClassName());
            }

        };
        treeStore = new TreeStore<RecordModelData>(loader);
    }
    
    public TreeStore<RecordModelData> getTreeStore()
    {
        return treeStore;
    }

    public void setTaskType(Long taskTypeId)
    {
        if (taskTypeId != null) {
            this.proxyTreeServiceProxy.setTaskType(taskTypeId);
        }
    }
    
    public void setWorkObject(Long workObjectId, String boundEntityName)
    {
        if (workObjectId != null) {
            this.proxyTreeServiceProxy.setWorkObject(workObjectId, boundEntityName);
        }
    }
    
    public void reload() {
        this.loader.load();
    }
    
}
