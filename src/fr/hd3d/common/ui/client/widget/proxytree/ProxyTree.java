package fr.hd3d.common.ui.client.widget.proxytree;

import com.extjs.gxt.ui.client.event.TreePanelEvent;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.asset.AssetRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.asset.FileRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.asset.ProxyModelData;
import fr.hd3d.common.ui.client.widget.basetree.BaseTreePanel;
import fr.hd3d.common.ui.client.widget.proxyviewer.ProxyDialogEvents;


public class ProxyTree extends BaseTreePanel<RecordModelData>
{

    private final ProxyTreeModel proxyTreeModel;

    public ProxyTree(ProxyTreeModel proxyTreeModel)
    {
        super(proxyTreeModel.getTreeStore());
        this.proxyTreeModel = proxyTreeModel;
    }

    @Override
    protected String getText(RecordModelData model)
    {
        String name = "";
        if (AssetRevisionModelData.CLASS_NAME.equals(model.getClassName()))
        {
            name = model.get(AssetRevisionModelData.WORKOBJECT_NAME_FIELD) + "_" + model.getName();
        }
        else if (FileRevisionModelData.CLASS_NAME.equals(model.getClassName()))
        {
            name = model.get(FileRevisionModelData.KEY_FIELD);
        }
        else if (ProxyModelData.CLASS_NAME.equals(model.getClassName()))
        {
            name = model.get(ProxyModelData.FILENAME_FIELD);
        }
        return name;
    }

    public void setTaskType(Long taskTypeId)
    {
        this.proxyTreeModel.setTaskType(taskTypeId);
    }
    public void setWorkObject(Long workObjectId, String boundEntityName)
    {
        this.proxyTreeModel.setWorkObject(workObjectId, boundEntityName);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void onClick(TreePanelEvent tpe)
    {
        super.onClick(tpe);
        RecordModelData selectedItem = this.getSelectionModel().getSelectedItem();
        if (selectedItem != null && ProxyModelData.CLASS_NAME.equals(selectedItem.getClassName()))
        {
            ProxyModelData proxyModelData = new ProxyModelData();
            proxyModelData.setProperties(selectedItem.getProperties());
            EventDispatcher.forwardEvent(ProxyDialogEvents.PROXY_SELECTED, proxyModelData);
        }
    }

    public void reload()
    {
        this.proxyTreeModel.reload();
    }

}
