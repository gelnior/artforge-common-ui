package fr.hd3d.common.ui.client.widget.sheeteditor.view.widget.approvaltypeexplorer;

import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.Listener;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteTypeModelData;
import fr.hd3d.common.ui.client.modeldata.reader.ApprovalNoteTypeReader;
import fr.hd3d.common.ui.client.widget.simpleexplorer.SimpleExplorerPanel;


public class ApprovalNoteTypeEditor extends SimpleExplorerPanel<ApprovalNoteTypeModelData>
{
    public ApprovalNoteTypeEditor()
    {
        super(new ApprovalNoteTypeReader());

        this.controller = new ApprovalNoteTypeEditorController(model, this);

        this.nameGrid.addListener(Events.BeforeEdit, new Listener<GridEvent<ApprovalNoteTypeModelData>>() {
            public void handleEvent(GridEvent<ApprovalNoteTypeModelData> be)
            {
                if (!RecordModelData.NAME_FIELD.equals(be.getProperty()))
                {
                    be.setCancelled(true);
                }
            }
        });
    }
}
