package fr.hd3d.common.ui.client.widget.sheeteditor.model.callback;

import com.extjs.gxt.ui.client.data.ListLoadResult;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.Response;

import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.error.ErrorDispatcher;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.widget.sheeteditor.config.SheetEditorConfig;
import fr.hd3d.common.ui.client.widget.sheeteditor.event.SheetEditorEvents;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata.DataTypeRendererEditorModelData;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.reader.DataTypeRendererEditorJsonReader;


/**
 * callback that parses data type configuration file when it is retrieved from application server.
 * 
 * @author HD3D
 */
public class DataTypeConfigurationCallback implements RequestCallback
{
    protected String file;

    public DataTypeConfigurationCallback(String file)
    {
        this.file = file;
    }

    public void onError(Request request, Throwable exception)
    {
        ErrorDispatcher.sendError(CommonErrors.NO_CONFIG_FILE);
    }

    public void onResponseReceived(Request request, Response response)
    {
        try
        {
            DataTypeRendererEditorJsonReader reader = new DataTypeRendererEditorJsonReader();
            ListLoadResult<DataTypeRendererEditorModelData> result = reader.read(response.getText());
            AppEvent event = new AppEvent(SheetEditorEvents.DATA_TYPE_LOADED, result.getData());
            event.setData(SheetEditorConfig.FILE_EVENT_VAR_NAME, this.file);
            EventDispatcher.forwardEvent(event);
        }
        catch (Exception e)
        {
            ErrorDispatcher.sendError(CommonErrors.BAD_CONFIG_FILE);
        }
    }

}
