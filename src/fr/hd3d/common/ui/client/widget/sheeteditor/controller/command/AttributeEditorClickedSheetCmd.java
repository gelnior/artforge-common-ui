package fr.hd3d.common.ui.client.widget.sheeteditor.controller.command;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.widget.sheeteditor.model.SheetEditorModel;
import fr.hd3d.common.ui.client.widget.sheeteditor.view.ISheetEditorView;


/**
 * When attribute editor button is clicked, it displays the attribute editor.
 * 
 * @author HD3D
 */
public class AttributeEditorClickedSheetCmd extends SheetEditorCommand
{

    /**
     * Constructor.
     * 
     * @param model
     *            Model (data handler) for sheet editor.
     * @param view
     *            View (widget handler) for sheet editor.
     */
    public AttributeEditorClickedSheetCmd(SheetEditorModel model, ISheetEditorView view)
    {
        super(model, view);
    }

    /**
     * When attribute editor button is clicked, it displays the attribute editor.
     * 
     * @param event
     *            The attribute editor clicked event.
     */
    public void execute(AppEvent event)
    {
        this.view.showAttributeEditor();
    }

}
