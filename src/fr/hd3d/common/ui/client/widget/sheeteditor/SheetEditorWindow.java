package fr.hd3d.common.ui.client.widget.sheeteditor;

import java.util.List;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.listener.ButtonClickListener;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.widget.SavingStatus;
import fr.hd3d.common.ui.client.widget.explorer.event.SheetEvents;
import fr.hd3d.common.ui.client.widget.sheeteditor.event.SheetEditorEvents;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata.EntityModelData;
import fr.hd3d.common.ui.client.widget.sheeteditor.view.SheetEditorView;


/**
 * Sheet editor window allows user to build sheet by selecting entity type associated to sheet and items/item groups
 * that compose the sheet. Sheet is linked to the project given in parameter. If no project is set, the created sheet
 * will be global (trans-project).<br>
 * The sheet editor can also edit sheet. In this case, entity and project could not be changed. Only items and item
 * groups can be modified.
 * 
 * @author HD3D
 */
public class SheetEditorWindow extends Window
{
    private static final int DEFAULT_WIDTH = 960;
    private static final int DEFAULT_HEIGHT = 430;

    /** Constant strings to display : dialog messages, button label... */
    public static final CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    /** View (widget handler) for sheet editor. */
    private final SheetEditorView view = new SheetEditorView(this);

    /** Cancel button hides the window on click. */
    private Button cancelButton;
    /** Save button saves data then hides the window on click. */
    private Button saveButton;
    /** Save button saves data as a new sheet then hides the window on click. */
    private Button saveAsButton;

    /** Saving indicator. */
    private final SavingStatus status = new SavingStatus();

    /**
     * Default constructor.
     */
    public SheetEditorWindow()
    {
        super();

        this.setStyles();
        this.createButton();
        this.setListeners();
    }

    /**
     * Shows editor window in editionMode
     * 
     * @param sheet
     *            Sheet to edit.
     */
    public void showForEdition(final SheetModelData sheet)
    {
        // TODO: Refactoring
        if (sheet != null)
        {
            Boolean isLoaded = sheet.areGroupsLoaded();
            if (isLoaded)
            {
                this.registerSheet(sheet);
                this.show(sheet.getProjectId());
            }
            else
            {
                final Listener<BaseEvent> listener = new Listener<BaseEvent>() {
                    public void handleEvent(BaseEvent be)
                    {
                        registerSheet(sheet);
                        show(sheet.getProjectId());
                        sheet.controller.removeListener(SheetEvents.ALL_ITEMS_LOADED, this);
                    }
                };
                sheet.controller.addListener(SheetEvents.ALL_ITEMS_LOADED, listener);
                sheet.controller.loadSheetItems();
            }
        }
    }

    /**
     * Hide button that allows to access to validation editor.
     * 
     * TODO : Refactor make a child class for validation button.
     */
    public void hideValidationButton()
    {
        this.view.hideValidationButton();
    }

    /**
     * Shows editor window in creation mode.
     * 
     * @param projectId
     *            The project ID to associate to the new sheet.
     * 
     */
    public void showForCreation(Long projectId)
    {
        this.registerSheet(null);
        this.show(projectId);
    }

    /**
     * Sets entities that user can select to build his sheets.
     * 
     * @param entities
     *            Available entities for sheet.
     */
    public void setEntities(List<EntityModelData> entities)
    {
        if (CollectionUtils.isNotEmpty(entities))
        {
            AppEvent event = new AppEvent(SheetEditorEvents.SET_AVAILABLE_ENTITY_TYPE, entities);
            this.view.getController().handleEvent(event);
        }
    }

    /**
     * Sets sheet for edition. When a sheet is registered, the sheet editor is in edition mode.
     * 
     * @param sheet
     *            The sheet to edit.
     */
    public void registerSheet(SheetModelData sheet)
    {
        this.view.setSheet(sheet);
    }

    /**
     * Shows saving indicator.
     */
    public void showSaving()
    {
        this.status.show();
    }

    /**
     * Hides saving indicator.
     */
    public void hideSaving()
    {
        this.status.hide();
    }

    /**
     * Shows editor window in creation mode.
     * 
     * @param projectId
     *            The project associated to the sheet to create.
     */
    protected void show(Long projectId)
    {
        EventType eventType = SheetEditorEvents.WINDOW_SHOWED;
        AppEvent event = new AppEvent(eventType, projectId);
        this.view.getController().handleEvent(event);
    }

    /**
     * Set dialog styles : behavior and size.
     */
    private void setStyles()
    {
        this.setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
        this.setHeading(CONSTANTS.SheetEditor());
        this.setResizable(true);
        this.setOnEsc(true);
        this.setModal(true);
        this.setLayout(new FitLayout());
        this.add(view);
    }

    /**
     * Create dialog buttons (save and cancel).
     */
    private void createButton()
    {
        saveButton = new Button(CONSTANTS.Save());
        saveAsButton = new Button(CONSTANTS.SaveAs());
        cancelButton = new Button(CONSTANTS.Cancel());
        this.addButton(saveAsButton);
        this.addButton(saveButton);
        this.addButton(cancelButton);

        this.status.hide();
        this.getButtonBar().insert(status, 0);
    }

    /**
     * Set listeners on buttons.
     */
    private void setListeners()
    {
        cancelButton.addSelectionListener(new ButtonClickListener(SheetEditorEvents.CANCEL_CLICKED));
        saveButton.addSelectionListener(new ButtonClickListener(SheetEditorEvents.SAVE_CLICKED));
        saveAsButton.addSelectionListener(new ButtonClickListener(SheetEditorEvents.SAVE_AS_CLICKED));
    }

    /**
     * Hide save as button.
     */
    public void hideSaveAsButton()
    {
        this.saveAsButton.hide();
    }

    /**
     * Show save as button.
     */
    public void showSaveAsButton()
    {
        this.saveAsButton.show();
    }
}
