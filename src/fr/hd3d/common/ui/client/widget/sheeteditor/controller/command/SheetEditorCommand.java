package fr.hd3d.common.ui.client.widget.sheeteditor.controller.command;

import fr.hd3d.common.ui.client.mvc.controller.BaseCommand;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.SheetEditorModel;
import fr.hd3d.common.ui.client.widget.sheeteditor.view.ISheetEditorView;


/**
 * Base command for sheet editor controller.
 * 
 * @author HD3D
 */
public abstract class SheetEditorCommand implements BaseCommand
{

    /** Model (data handler) for sheet editor. */
    protected final SheetEditorModel model;
    /** View (widget handler) for sheet editor. */
    protected final ISheetEditorView view;

    /***
     * Constructor.
     * 
     * @param model
     *            Model (data handler) for sheet editor.
     * @param view
     *            View (widget handler) for sheet editor.
     */
    public SheetEditorCommand(SheetEditorModel model, ISheetEditorView view)
    {
        this.model = model;
        this.view = view;
    }
}
