package fr.hd3d.common.ui.client.widget.sheeteditor.controller.command;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.Record;

import fr.hd3d.common.ui.client.modeldata.technical.ItemGroupModelData;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.SheetEditorModel;
import fr.hd3d.common.ui.client.widget.sheeteditor.view.ISheetEditorView;


/**
 * Rename given group.
 * 
 * @author HD3D
 */
public class RenameGroupSheetCmd extends SheetEditorCommand
{

    /**
     * Constructor.
     * 
     * @param model
     *            Model (data handler) for sheet editor.
     * @param view
     *            View (widget handler) for sheet editor.
     */
    public RenameGroupSheetCmd(SheetEditorModel model, ISheetEditorView view)
    {
        super(model, view);
    }

    /**
     * Rename given group.
     * 
     * @param event
     *            Group renaming requested event.
     */
    public void execute(AppEvent event)
    {
        final Record groupRecord = this.view.getSelectedItemRecord();
        ModelData item = groupRecord.getModel();
        if (item != null && item instanceof ItemGroupModelData)
        {
            groupRecord.set(ItemGroupModelData.NAME_FIELD, event.getData());
            groupRecord.commit(false);
        }
    }

}
