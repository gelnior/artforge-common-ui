package fr.hd3d.common.ui.client.widget.sheeteditor.controller.command;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.error.ErrorDispatcher;
import fr.hd3d.common.ui.client.widget.sheeteditor.error.SheetEditorErrors;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.SheetEditorModel;
import fr.hd3d.common.ui.client.widget.sheeteditor.view.ISheetEditorView;


/**
 * When is exist request succeeds, an error event is dispatched to controllers. If the sheet does not exist, it saves
 * the sheet.
 * 
 * @author HD3D
 */
public class IsExistSucceedsSheetCmd extends SaveSheetCmd
{

    /**
     * Constructor.
     * 
     * @param model
     *            Model (data handler) for sheet editor.
     * @param view
     *            View (widget handler) for sheet editor.
     */
    public IsExistSucceedsSheetCmd(SheetEditorModel model, ISheetEditorView view)
    {
        super(model, view);
    }

    /**
     * When is exist request succeeds, an error event is dispatched to controllers. If the sheet does not exist, it
     * saves the sheet.
     */
    @Override
    public void execute(AppEvent event)
    {
        Boolean isExist = event.getData();
        if (isExist == null)
            return;

        if (!isExist)
        {
            if (this.model.isSaveAsMode())
            {
                this.saveAs();
            }
            else
            {
                this.save();
            }

        }
        else
        {
            ErrorDispatcher.sendError(SheetEditorErrors.SHEET_NAME_EXIST);
        }
        // this.model.setIsSaveAsMode(false);
    }

}
