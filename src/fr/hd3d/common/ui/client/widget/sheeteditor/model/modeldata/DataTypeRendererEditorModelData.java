package fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.BaseModelData;
import com.extjs.gxt.ui.client.data.ModelType;


public class DataTypeRendererEditorModelData extends BaseModelData
{
    private static final long serialVersionUID = -6739870093491632746L;

    public static final String ROOT = "records";
    public static final String DATA_TYPE_FIELD = "dataType";
    public static final String RENDERER_FIELD = "renderer";
    public static final String EDITOR_FIELD = "editor";

    /**
     * @return Base type at GXT format for a simple HD3D model data.
     */
    public static ModelType getModelType()
    {
        ModelType type = new ModelType();
        type.setRoot(ROOT);

        type.addField(DATA_TYPE_FIELD);
        type.addField(RENDERER_FIELD);
        type.addField(EDITOR_FIELD);

        return type;
    }

    public void setDataType(String dataType)
    {
        this.set(DATA_TYPE_FIELD, dataType);
    }

    public void setRenderer(List<String> renderer)
    {
        this.set(RENDERER_FIELD, renderer);
    }

    public void setEditor(List<String> editor)
    {
        this.set(EDITOR_FIELD, editor);
    }

    public String getDataType()
    {
        return this.get(DATA_TYPE_FIELD);
    }

    public List<String> getRenderer()
    {
        return this.get(RENDERER_FIELD);
    }

    public List<I18nFieldModelData> getRendererModelData()
    {
        List<I18nFieldModelData> rendererModelData = new ArrayList<I18nFieldModelData>();
        List<String> renderers = this.getRenderer();
        for (String renderer : renderers)
        {
            rendererModelData.add(new I18nFieldModelData(renderer));
        }
        return rendererModelData;
    }

    public List<I18nFieldModelData> getEditorModelData()
    {
        List<I18nFieldModelData> editorModelData = new ArrayList<I18nFieldModelData>();
        List<String> editors = this.getEditor();
        for (String editor : editors)
        {
            editorModelData.add(new I18nFieldModelData(editor));
        }
        return editorModelData;
    }

    public List<String> getEditor()
    {
        return this.get(EDITOR_FIELD);
    }
}
