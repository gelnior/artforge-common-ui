package fr.hd3d.common.ui.client.widget.sheeteditor.controller.command;

import java.util.List;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.client.enums.ESheetType;
import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.SheetEditorModel;
import fr.hd3d.common.ui.client.widget.sheeteditor.view.ISheetEditorView;


/**
 * When entity properties are loaded, it displays the entity property loading list and hides the indicator.
 * 
 * @author HD3D
 */
public class PropertyLoadedSheetCmd extends SheetEditorCommand
{

    /**
     * Constructor.
     * 
     * @param model
     *            Model (data handler) for sheet editor.
     * @param view
     *            View (widget handler) for sheet editor.
     */
    public PropertyLoadedSheetCmd(SheetEditorModel model, ISheetEditorView view)
    {
        super(model, view);
    }

    /**
     * When entity properties are loaded, it displays the entity property loading list and hides the indicator.
     * 
     * @param event
     *            The entity properties loaded event.
     */
    public void execute(AppEvent event)
    {
        List<ItemModelData> data = event.getData();
        this.model.getEntityPropertyModel().setEntityProperties(null, data);

        // TODO Refactor should be in a child class.
        for (ItemModelData item : data)
        {
            if (FieldUtils.isStep(item.getQuery()))
                item.setParameter(item.getParam());
        }

        // TODO Refactor should be in a child class.
        if (SheetEditorModel.getSheetType() == ESheetType.PRODUCTION)
        {
            this.model.getEntityPropertyModel().loadApprovalNoteType();
        }

        this.view.expandTrees();
        this.view.maskEntityPropertiesTree(false);
    }
}
