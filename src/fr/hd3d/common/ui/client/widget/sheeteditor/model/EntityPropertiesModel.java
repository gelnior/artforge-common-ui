package fr.hd3d.common.ui.client.widget.sheeteditor.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.event.LoadListener;
import com.extjs.gxt.ui.client.store.TreeStore;

import fr.hd3d.common.client.enums.ESheetType;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteTypeModelData;
import fr.hd3d.common.ui.client.modeldata.reader.ApprovalNoteTypeReader;
import fr.hd3d.common.ui.client.modeldata.technical.ItemGroupModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.service.ExcludedField;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.IsNullConstraint;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.widget.explorer.model.modeldata.IColumn;
import fr.hd3d.common.ui.client.widget.sheeteditor.event.SheetEditorEvents;


/**
 * Entity properties model handles data set inside property tree.
 * 
 * @author HD3D
 */
public class EntityPropertiesModel
{
    /** The sheet currently edited. */
    private SheetModelData sheet;

    /** The property tree store. */
    private final TreeStore<RecordModelData> entityPropertyStore;

    private final ItemGroupModelData approvalNode = this.makeNewApprovalNoteGroup("approvals");

    /**
     * Constructor.
     */
    public EntityPropertiesModel()
    {
        this.entityPropertyStore = new TreeStore<RecordModelData>();
    }

    /**
     * @return Entity property tree store.
     */
    public TreeStore<RecordModelData> getStore()
    {
        return this.entityPropertyStore;
    }

    /**
     * Registers currently edited sheet.
     * 
     * @param sheet
     *            The sheet to set.
     */
    public void setSheet(SheetModelData sheet)
    {
        this.sheet = sheet;
    }

    /**
     * Set "Available Entities" node as root node.
     */
    public void createRoot()
    {
        this.entityPropertyStore.removeAll();
        RecordModelData root = new RecordModelData();
        root.setName("Available columns:");
        this.entityPropertyStore.add(root, true);
        this.entityPropertyStore.commitChanges();
    }

    /**
     * Add group to root node.
     * 
     * @param group
     *            The group to add.
     */
    public void addEntityPropertyGroup(ItemGroupModelData group)
    {
        RecordModelData parent = this.entityPropertyStore.getRootItems().get(0);
        if (parent != null && entityPropertyStore.contains(group) == false)
        {
            entityPropertyStore.add(parent, group, false);
        }
    }

    /**
     * Set entity properties inside a given node.
     * 
     * @param parent
     *            The node in which entity properties will be added as leaves.
     * @param properties
     *            The entity properties.
     */
    public void setEntityProperties(RecordModelData parent, List<ItemModelData> properties)
    {
        parent = this.resetNode(parent);

        if (properties.size() > 0)
        {
            Collections.sort(properties, new ItemNameComparator());

            List<RecordModelData> childs = new ArrayList<RecordModelData>();
            List<RecordModelData> stepChilds = new ArrayList<RecordModelData>();

            for (ItemModelData property : properties)
            {
                String propertyQuery = property.getQuery();

                boolean isExcluded = ExcludedField.isExcluded(propertyQuery.toLowerCase());
                if (!isExcluded)
                {
                    // TODO: Refactor step distinction should be in a child class.
                    if (FieldUtils.isStep(property.getQuery()))
                    {
                        stepChilds.add(property);
                    }
                    else
                        childs.add(property);
                }
            }

            this.entityPropertyStore.add(parent, childs, false);
            this.addStepGroup(stepChilds);
        }
    }

    /**
     * Create a node to store available steps. Steps are used only in breakdown, it should only appears for breakdown
     * sheets.<br>
     * // TODO: Refactor should be in child class.
     * 
     * @param stepChilds
     *            Steps to set as children leaves of created step group.
     */
    protected void addStepGroup(List<RecordModelData> stepChilds)
    {
        ItemGroupModelData stepGroup = new ItemGroupModelData();
        if (SheetEditorModel.getSheetType() == ESheetType.BREAKDOWN)
        {
            stepGroup.setName("Steps");
            this.addEntityPropertyGroup(stepGroup);
            entityPropertyStore.add(stepGroup, stepChilds, false);
        }
    }

    /**
     * Remove all child from parent.
     * 
     * @param parent
     *            The parent to reset.
     * @return Parent, if parent was null, it is replaced by root node.
     */
    protected RecordModelData resetNode(RecordModelData parent)
    {
        if (parent == null)
        {
            parent = this.entityPropertyStore.getRootItems().get(0);
        }
        entityPropertyStore.removeAll(parent);

        return parent;
    }

    /**
     * TODO: Should be in a child class.<br>
     * 
     * Reload approval note types node.
     */
    public void loadApprovalNoteType()
    {
        this.resetNode(approvalNode);

        final ServiceStore<ApprovalNoteTypeModelData> approvalTypesTyped = new ServiceStore<ApprovalNoteTypeModelData>(
                new ApprovalNoteTypeReader());
        final ServiceStore<ApprovalNoteTypeModelData> approvalTypeNullTyped = new ServiceStore<ApprovalNoteTypeModelData>(
                new ApprovalNoteTypeReader());

        approvalTypesTyped.setPath(ServicesPath.getOneSegmentPath(ServicesPath.APPROVAL_NOTE_TYPES, null));
        approvalTypesTyped.addEqConstraint("taskType.entityName", this.sheet.getBoundClassName());

        approvalTypeNullTyped.setPath(ServicesPath.getOneSegmentPath(ServicesPath.APPROVAL_NOTE_TYPES, null));
        approvalTypeNullTyped.addParameter(new IsNullConstraint("taskType"));

        approvalTypeNullTyped.addLoadListener(new LoadListener() {
            @Override
            public void loaderLoad(LoadEvent le)
            {
                approvalTypesTyped.reload();
            }
        });
        approvalTypesTyped.addLoadListener(new LoadListener() {
            @Override
            public void loaderLoad(LoadEvent le)
            {
                List<ApprovalNoteTypeModelData> types = new ArrayList<ApprovalNoteTypeModelData>();
                types.addAll(approvalTypesTyped.getModels());
                types.addAll(approvalTypeNullTyped.getModels());

                setApprovalNoteType(types);
            }
        });
        approvalTypeNullTyped.reload();
    }

    /**
     * Add approval note types to the entity property tree inside a validation folder.
     * 
     * @param approvalNoteTypes
     *            The types to set in the entity property tree.
     */
    public void setApprovalNoteType(List<ApprovalNoteTypeModelData> approvalNoteTypes)
    {
        List<ItemModelData> items = new ArrayList<ItemModelData>();
        List<IColumn> columns = new ArrayList<IColumn>();

        for (ApprovalNoteTypeModelData approvalType : approvalNoteTypes)
        {
            ItemModelData item = new ItemModelData();
            item.setName(approvalType.getName());
            item.setParameter(approvalType.getId().toString());
            item.setQuery(ApprovalNoteTypeModelData.ITEM_QUERY);
            item.setItemType(ApprovalNoteTypeModelData.ITEM_TYPE);
            item.setType(ApprovalNoteTypeModelData.ITEM_META_TYPE);

            items.add(item);
            columns.add(item);

            this.entityPropertyStore.remove(approvalType);
        }
        approvalNode.setSheet(this.sheet);
        approvalNode.setColumns(columns);

        this.addEntityPropertyGroup(approvalNode);
        this.setEntityProperties(approvalNode, items);
        EventDispatcher.forwardEvent(SheetEditorEvents.APPROVAL_NOTE_TYPE_LOADED);
    }

    private ItemGroupModelData makeNewApprovalNoteGroup(String approvalNoteGroupName)
    {
        ItemGroupModelData group = new ItemGroupModelData();
        group.setName(approvalNoteGroupName);

        return group;
    }

}
