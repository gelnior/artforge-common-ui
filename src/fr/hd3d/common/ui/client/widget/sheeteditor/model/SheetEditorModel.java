package fr.hd3d.common.ui.client.widget.sheeteditor.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.restlet.client.data.Method;

import com.extjs.gxt.ui.client.data.BaseModelData;
import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.event.LoadListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.store.Record;
import com.extjs.gxt.ui.client.store.TreeStore;

import fr.hd3d.common.client.Const;
import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.client.enums.ESheetType;
import fr.hd3d.common.ui.client.config.CommonConfig;
import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.error.ErrorDispatcher;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.http.HttpRequestHandlerSingleton;
import fr.hd3d.common.ui.client.http.IHttpRequestHandler;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.reader.ClassDynReader;
import fr.hd3d.common.ui.client.modeldata.technical.ClassDynModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ItemGroupModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.widget.explorer.model.modeldata.IColumn;
import fr.hd3d.common.ui.client.widget.sheeteditor.event.SheetEditorEvents;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.callback.DataTypeConfigurationCallback;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.callback.LoadEntityPropertiesCallback;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata.DataTypeRendererEditorModelData;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata.EntityModelData;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata.I18nFieldModelData;


/**
 * Sheet editor model handles data for sheet editor widget.
 * 
 * @author HD3D
 */
public class SheetEditorModel
{
    /** True if available renderer/editors have been initialized. */
    private static Boolean dataTypeInitialized = false;

    /** Files describing available renderers/editors. */
    public static final HashMap<String, Boolean> dataTypeFiles = new HashMap<String, Boolean>();
    /** That store stores available renderers and editors for a given type. */
    public static final ListStore<DataTypeRendererEditorModelData> dataTypeStore = new ListStore<DataTypeRendererEditorModelData>();
    /** The store containing available renderers. */
    public static final ListStore<I18nFieldModelData> rendererStore = new ListStore<I18nFieldModelData>();
    /** The store containing available editors. */
    public static final ListStore<I18nFieldModelData> editorStore = new ListStore<I18nFieldModelData>();

    /**
     * The store containing dynamic attributs to display behind the item name
     */
    public final ServiceStore<ClassDynModelData> classDynModelDataStore = new ServiceStore<ClassDynModelData>(
            new ClassDynReader());
    /**
     * Constraint use to fetch dynamic attributs
     */
    public final Constraint constraint = new Constraint(EConstraintOperator.in, "id");

    /**
     * The type of sheet : if it is a Breakdown sheet, a Production sheet, a Task sheet... It is different from entity,
     * because it is used by application to display only right sheets.
     */
    private static ESheetType sheetType;

    /**
     * @return The type of sheet : if it is a Breakdown sheet, a Production sheet, a Task sheet... It is different from
     *         entity, because it is used by application to display only right sheets.
     */
    public static ESheetType getSheetType()
    {
        return sheetType;
    }

    /**
     * Set the type that will be set on each created sheets. Type is useful to show appropriate sheets by application.
     * 
     * @param type
     *            The sheet type to set.
     */
    public static void setType(ESheetType type)
    {
        SheetEditorModel.sheetType = type;
    }

    public static void updateRendererList(String dataType)
    {
        rendererStore.removeAll();
        List<DataTypeRendererEditorModelData> types = dataTypeStore.findModels(
                DataTypeRendererEditorModelData.DATA_TYPE_FIELD, dataType);
        for (DataTypeRendererEditorModelData type : types)
        {
            rendererStore.add(type.getRendererModelData());
        }
        rendererStore.add(new I18nFieldModelData(null));
    }

    public static void updateEditorList(String dataType)
    {
        editorStore.removeAll();
        List<DataTypeRendererEditorModelData> types = dataTypeStore.findModels(
                DataTypeRendererEditorModelData.DATA_TYPE_FIELD, dataType);
        for (DataTypeRendererEditorModelData type : types)
        {
            editorStore.add(type.getEditorModelData());
        }
        editorStore.add(new I18nFieldModelData(null));
    }

    public static Boolean areDataTypeFileLoaded()
    {
        return !SheetEditorModel.dataTypeFiles.containsValue(false);
    }

    public static void addDataFile(String file)
    {
        SheetEditorModel.dataTypeFiles.put(file, false);
    }

    /** Id of current project. The edited/crated sheet will be set on this project. */
    private Long projectId;
    /** The edited sheet or the sheet to create (depending on mode). */
    private SheetModelData sheet;

    /** Store containing available entities. */
    protected final ListStore<EntityModelData> entityTypeStore = new ListStore<EntityModelData>();
    /** Store containing properties set on sheet. */
    protected final TreeStore<BaseModelData> sheetPropertyStore = new TreeStore<BaseModelData>();

    /** Store containing available properties to build the sheet (displayed in left tree). */
    protected final EntityPropertiesModel entityPropertiesModel = new EntityPropertiesModel();
    /** Store containing available properties to build the sheet (displayed in left tree). */
    protected final SheetEditorSaveManager saveManager = new SheetEditorSaveManager(this);

    /** True if a new sheet must be created from currently edited sheet. */
    private Boolean isSaveAsMode = false;

    public SheetEditorModel()
    {
        this.classDynModelDataStore.addParameter(constraint);
        this.classDynModelDataStore.addLoadListener(new LoadListener() {
            @Override
            public void loaderLoad(LoadEvent le)
            {
                super.loaderLoad(le);
                onClassDynLoaded();
            }
        });
    }

    /** @return Store containing available entities. */
    public TreeStore<RecordModelData> getEntityPropertyStore()
    {
        return this.entityPropertiesModel.getStore();
    }

    public EntityPropertiesModel getEntityPropertyModel()
    {
        return this.entityPropertiesModel;
    }

    /** @return Store containing properties set on sheet. */
    public TreeStore<BaseModelData> getSheetPropertyStore()
    {
        return sheetPropertyStore;
    }

    /** @return Store containing available entities. */
    public ListStore<EntityModelData> getEntityTypeStore()
    {
        return entityTypeStore;
    }

    /**
     * Set available entity types.
     * 
     * @param entityType
     *            The type to set.
     */
    public void setEntityType(List<EntityModelData> entityType)
    {
        this.entityTypeStore.removeAll();
        this.entityTypeStore.add(entityType);
    }

    /**
     * @return Project id set on sheet.
     */
    public Long getProjectId()
    {
        return projectId;
    }

    /**
     * Set project id on sheet to link the sheet to the project.
     * 
     * @param projectId
     */
    public void setProjectId(Long projectId)
    {
        this.projectId = projectId;
    }

    /**
     * @return Currently edited sheet.
     */
    public SheetModelData getSheet()
    {
        return sheet;
    }

    /**
     * Set currently edited sheet.
     * 
     * @param sheet
     *            The sheet to set as current sheet.
     */
    public void setSheet(SheetModelData sheet)
    {
        this.sheet = sheet;
    }

    /**
     * Set edited sheet name.
     * 
     * @param name
     *            The name to set as the sheet name.
     */
    public void setSheetName(String name)
    {
        if (name != null)
        {
            this.sheet.setName(name);
        }
    }

    /**
     * @return True if sheet should be save as a new sheet.
     */
    public Boolean isSaveAsMode()
    {
        return this.isSaveAsMode;
    }

    /**
     * Set if sheet should be saved as a new sheet or just change current sheet.
     * 
     * @param isSaveAsMode
     *            True to set save as mode.
     */
    public void setIsSaveAsMode(Boolean isSaveAsMode)
    {
        this.isSaveAsMode = isSaveAsMode;
    }

    /**
     * @return The name of the bound class (the entity name).
     */
    public String getSheetEntityType()
    {
        return this.sheet.getBoundClassName();
    }

    /**
     * Save sheet to backend services.
     */
    public void saveSheet()
    {
        this.sheetPropertyStore.commitChanges();
        this.saveManager.saveSheet(sheet);
    }

    /**
     * @return Groups set inside sheet properties tree. Each group includes its items.
     */
    public List<ItemGroupModelData> getSheetItemGroups()
    {
        List<ItemGroupModelData> sheetGroups = new ArrayList<ItemGroupModelData>();
        List<BaseModelData> roots = sheetPropertyStore.getRootItems();
        if (roots.size() > 0)
        {
            BaseModelData root = sheetPropertyStore.getRootItems().get(0);
            List<BaseModelData> groups = sheetPropertyStore.getChildren(root);

            for (BaseModelData group : groups)
            {
                List<IColumn> groupItems = this.getGroupItems((ItemGroupModelData) group);
                ((ItemGroupModelData) group).setColumns(groupItems);
                sheetGroups.add((ItemGroupModelData) group);
            }
        }
        return sheetGroups;
    }

    /**
     * @param group
     *            The group of which items are requested.
     * @return The items set inside given group.
     */
    public List<IColumn> getGroupItems(ItemGroupModelData group)
    {
        List<IColumn> groupItems = new ArrayList<IColumn>();
        List<BaseModelData> items = sheetPropertyStore.getChildren(group);
        for (BaseModelData item : items)
        {
            groupItems.add((IColumn) item);
        }
        return groupItems;
    }

    /**
     * In creation mode it initializes current sheet with a new one and built a new property tree for the sheet property
     * tree widget.
     */
    public void createNewSheet()
    {
        sheet = new SheetModelData();
        sheet.setGroupLoaded(true);
        sheet.setProjectId(projectId);
        sheet.setBoundClassName("");
        if (SheetEditorModel.sheetType != null)
            sheet.setType(SheetEditorModel.sheetType.toString());

        entityPropertiesModel.createRoot();

        this.sheetPropertyStore.removeAll();
        BaseModelData sheetTreeRoot = new BaseModelData();
        sheetTreeRoot.set("name", "Columns" + ":");
        this.sheetPropertyStore.add(sheetTreeRoot, true);
        this.sheetPropertyStore.commitChanges();

        ItemGroupModelData defaultGroup = new ItemGroupModelData();
        defaultGroup.setName("Default");
        this.sheetPropertyStore.add(sheetTreeRoot, defaultGroup, true);
    }

    /**
     * Add an item group to the sheet properties tree.
     * 
     * @param groupName
     *            The name of the group to add.
     */
    public void createItemGroup(String groupName)
    {
        ItemGroupModelData group = new ItemGroupModelData();
        group.setName(groupName);
        group.setSheet(this.sheet);
        group.setColumns(null);

        List<ItemGroupModelData> groups = sheet.getColumnGroups();
        groups.add(group);
        sheet.setItemGroups(groups);

        BaseModelData root = this.sheetPropertyStore.getRootItems().get(0);
        this.sheetPropertyStore.add(root, group, false);
    }

    /**
     * Remove given item group from the sheet properties tree.
     * 
     * @param group
     *            The group to remove.
     */
    public void removeItemGroup(ItemGroupModelData group)
    {
        List<ItemGroupModelData> groups = sheet.getColumnGroups();
        groups.remove(group);
        sheet.setItemGroups(groups);
    }

    /**
     * Reload available properties for currently selected entity.
     */
    public void reloadEntityProperties()
    {
        if (this.sheet != null)
        {
            String entityType = this.sheet.getBoundClassName();
            if (entityType != null && !entityType.equals(""))
            {
                String url = ServicesPath.ENTITIES + entityType;
                // TODO Refactor this, should be in a child class. This is needed to retrieve steps available for this
                // project.
                if (sheetType == ESheetType.BREAKDOWN)
                {
                    url += "?projectId=" + this.projectId;
                }
                RestRequestHandlerSingleton.getInstance().handleRequest(Method.GET, url, null,
                        new LoadEntityPropertiesCallback());
            }
        }
    }

    public void loadSheet()
    {
        if (sheet == null)
        {
            this.createNewSheet();
            this.entityPropertiesModel.setSheet(sheet);
        }
        else
        {
            this.entityPropertiesModel.setSheet(sheet);
            this.entityPropertiesModel.createRoot();

            this.sheetPropertyStore.removeAll();
            BaseModelData root2 = new BaseModelData();
            root2.set("name", "Columns:");
            this.sheetPropertyStore.add(root2, true);

            List<ItemGroupModelData> groups = this.sheet.getColumnGroups();
            List<Long> dynamicAttributIds = new ArrayList<Long>();
            for (ItemGroupModelData columnGroup : groups)
            {
                this.sheetPropertyStore.add(root2, columnGroup, true);

                List<IColumn> items = columnGroup.getColumns();
                for (Iterator<IColumn> itemIterator = items.iterator(); itemIterator.hasNext();)
                {
                    ItemModelData column = (ItemModelData) itemIterator.next();
                    this.sheetPropertyStore.add(columnGroup, column, false);
                    if (Const.METADATA_ITEM_TYPE.equals(column.getItemType()))
                    {
                        try
                        {
                            dynamicAttributIds.add(Long.parseLong(column.getQuery()));
                        }
                        catch (NumberFormatException e)
                        {}
                    }
                }
            }
            if (dynamicAttributIds.size() > 0)
            {
                this.constraint.setLeftMember(dynamicAttributIds);
                this.classDynModelDataStore.reload();
            }
            this.sheetPropertyStore.commitChanges();
        }

        AppEvent event = new AppEvent(SheetEditorEvents.SHEET_NAME_CHANGED);
        event.setData(this.sheet.getName());
        EventDispatcher.forwardEvent(event);

        EntityModelData type = entityTypeStore.findModel(EntityModelData.NAME_FIELD, sheet.getBoundClassName());
        AppEvent event2 = new AppEvent(SheetEditorEvents.ENTITY_CHANGED, type);
        EventDispatcher.forwardEvent(event2);
    }

    private void onClassDynLoaded()
    {
        if (this.classDynModelDataStore != null && this.classDynModelDataStore.getModels() != null)
        {
            for (ClassDynModelData classDynModelData : this.classDynModelDataStore.getModels())
            {
                ItemModelData findModel = (ItemModelData) this.sheetPropertyStore.findModel(ItemModelData.QUERY,
                        classDynModelData.getId().toString());
                if (findModel != null)
                {
                    findModel.setClassDynName(classDynModelData.getName());
                    this.sheetPropertyStore.update(findModel);
                }
            }
        }
    }

    public void loadDataType()
    {
        if (dataTypeInitialized == false)
        {
            if (SheetEditorModel.dataTypeFiles.containsKey(CommonConfig.CONFIG_DATA_TYPE_FILE_PATH) == false)
            {
                SheetEditorModel.dataTypeFiles.put(CommonConfig.CONFIG_DATA_TYPE_FILE_PATH, false);
            }
            try
            {
                dataTypeInitialized = true;
                Set<String> files = SheetEditorModel.dataTypeFiles.keySet();
                for (String file : files)
                {
                    IHttpRequestHandler requestHandler = HttpRequestHandlerSingleton.getInstance();
                    requestHandler.getRequest(file, new DataTypeConfigurationCallback(file));
                }
            }
            catch (Exception e)
            {
                ErrorDispatcher.sendError(CommonErrors.NO_CONFIG_FILE);
            }
        }
    }

    public void setFileLoaded(String file)
    {
        if (SheetEditorModel.dataTypeFiles.containsKey(file))
            SheetEditorModel.dataTypeFiles.put(file, true);
    }

    /**
     * Set the renderer for a given item.
     * 
     * @param item
     *            The item of which render will be changed.
     * @param renderer
     *            The value to set as renderer.
     */
    public void setItemRenderer(ItemModelData item, String renderer)
    {
        Record record = this.sheetPropertyStore.getRecord(item);
        if (record != null)
        {
            record.set(ItemModelData.RENDERER, renderer);
        }
    }

    /**
     * Set the editor for a given item.
     * 
     * @param item
     *            The item of which editor will be changed.
     * @param editor
     *            The value to set as editor.
     */
    public void setItemEditor(ItemModelData item, String editor)
    {
        Record record = this.sheetPropertyStore.getRecord(item);
        if (record != null)
        {
            record.set(ItemModelData.EDITOR, editor);
        }
    }

    public void deleteItems(List<BaseModelData> selection)
    {
        for (BaseModelData model : selection)
        {
            if (model instanceof ItemModelData)
            {
                ItemModelData item = (ItemModelData) model;

                this.sheetPropertyStore.remove(model);

                if (item.getId() != null)
                    this.saveManager.addDeletedItem(item);
            }
            else if (model instanceof ItemGroupModelData)
            {
                ItemGroupModelData group = (ItemGroupModelData) model;
                this.sheetPropertyStore.removeAll(model);
                this.sheetPropertyStore.remove(model);

                if (group.getId() != null)
                    this.saveManager.addDeletedGroup(group);
            }
        }
    }

    public void addDataType(List<DataTypeRendererEditorModelData> dataTypes)
    {
        SheetEditorModel.dataTypeStore.add(dataTypes);
    }

    /**
     * Add an item to the list of item to delete.
     * 
     * @param item
     *            The item to remotely delete.
     */
    public void addDeletedItem(ItemModelData item)
    {
        this.sheetPropertyStore.remove(item);

        this.saveManager.addDeletedItem(item);
    }

    /**
     * Add an item group to the list of group to delete
     * 
     * @param group
     *            The group to remotely delete.
     */
    public void addDeletedGroup(ItemGroupModelData group)
    {
        this.sheetPropertyStore.remove(group);

        this.saveManager.addDeletedGroup(group);
    }

}
