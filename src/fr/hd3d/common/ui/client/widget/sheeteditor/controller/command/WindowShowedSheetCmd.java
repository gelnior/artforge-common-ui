package fr.hd3d.common.ui.client.widget.sheeteditor.controller.command;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.widget.sheeteditor.model.SheetEditorModel;
import fr.hd3d.common.ui.client.widget.sheeteditor.view.ISheetEditorView;


/**
 * When sheet editor is displayed, it reloads all data depending on set parameters.
 * 
 * @author HD3D
 */
public class WindowShowedSheetCmd extends SheetEditorCommand
{

    /**
     * Constructor.
     * 
     * @param model
     *            Model (data handler) for sheet editor.
     * @param view
     *            View (widget handler) for sheet editor.
     */
    public WindowShowedSheetCmd(SheetEditorModel model, ISheetEditorView view)
    {
        super(model, view);
    }

    /**
     * When sheet editor is displayed, it reloads all data depending on set parameters.
     * 
     * @param event
     *            The cancel clicked event.
     */
    public void execute(AppEvent event)
    {
        this.view.reset();
        this.model.setProjectId((Long) event.getData());
        this.model.loadDataType();
        this.view.showDialog();
        this.model.setIsSaveAsMode(false);

        if (this.model.getSheet() == null)
        {
            // TODO: rename this method.
            this.model.loadSheet();
            this.view.selectFirstEntity();
            this.view.hideSaveAsButton();
        }
        else
        {
            this.model.loadSheet();
            this.view.selectEntity(this.model.getSheet().getBoundClassName());
            this.view.showSaveAsButton();
        }
        this.view.expandTrees();
    }

}
