package fr.hd3d.common.ui.client.widget.sheeteditor.controller.command;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.widget.sheeteditor.model.SheetEditorModel;
import fr.hd3d.common.ui.client.widget.sheeteditor.view.ISheetEditorView;


/**
 * When note types are loaded, it expands note types node.
 * 
 * @author HD3D
 */
public class NoteTypesLoadedSheetCmd extends SheetEditorCommand
{

    /**
     * Constructor.
     * 
     * @param model
     *            Model (data handler) for sheet editor.
     * @param view
     *            View (widget handler) for sheet editor.
     */
    public NoteTypesLoadedSheetCmd(SheetEditorModel model, ISheetEditorView view)
    {
        super(model, view);
    }

    /**
     * When note types are loaded, it expands note types node.
     * 
     * @param event
     *            Note types loaded event.
     */
    public void execute(AppEvent event)
    {
        this.view.expandTrees();
    }
}
