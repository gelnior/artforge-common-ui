package fr.hd3d.common.ui.client.widget.sheeteditor.view.widget.sheetitemstreegrid;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.dnd.TreeGridDragSource;
import com.extjs.gxt.ui.client.event.DNDEvent;
import com.extjs.gxt.ui.client.widget.Component;
import com.extjs.gxt.ui.client.widget.treegrid.TreeGrid.TreeNode;

import fr.hd3d.common.ui.client.modeldata.technical.ItemGroupModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;


public class SheetItemsTreeGridDragSource extends TreeGridDragSource
{

    public SheetItemsTreeGridDragSource(Component component)
    {
        super(component);
    }

    @SuppressWarnings("rawtypes")
    @Override
    protected void onDragStart(DNDEvent e)
    {
        super.onDragStart(e);

        TreeNode n = this.treeGrid.findNode(e.getTarget());
        // cancel event if root node
        if (n != null)
        {
            ModelData m = n.getModel();

            if (m instanceof ItemGroupModelData || m instanceof ItemModelData)
            {
                e.setCancelled(false);
            }
            else
            {
                e.setCancelled(true);
            }
        }
    }
}
