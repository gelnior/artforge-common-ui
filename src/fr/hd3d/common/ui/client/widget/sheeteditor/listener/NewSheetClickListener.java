package fr.hd3d.common.ui.client.widget.sheeteditor.listener;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.widget.sheeteditor.event.SheetEditorEvents;


public class NewSheetClickListener extends SelectionListener<ButtonEvent>
{
    @Override
    public void componentSelected(ButtonEvent ce)
    {
        EventDispatcher.forwardEvent(SheetEditorEvents.NEW_SHEET_CLICKED);
    }
}
