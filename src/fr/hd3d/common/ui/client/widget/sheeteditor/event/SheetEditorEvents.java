package fr.hd3d.common.ui.client.widget.sheeteditor.event;

import com.extjs.gxt.ui.client.event.EventType;


/**
 * Events raised by sheet editor.
 * 
 * @author HD3D
 */
public class SheetEditorEvents
{
    public static final String SHEET_ID_EVENT_VAR = "sheet_id_event";

    public static final EventType ENTITY_CHANGED = new EventType();
    public static final EventType SHEET_NAME_CHANGED = new EventType();
    public static final EventType ENTITY_PROPERTY_LOADED = new EventType();
    public static final EventType SET_AVAILABLE_ENTITY_TYPE = new EventType();
    public static final EventType DATA_TYPE_LOADED = new EventType();

    public static final EventType SAVING_STARTED = new EventType();
    public static final EventType END_SAVE = new EventType();
    public static final EventType SHEET_CREATED = new EventType();
    public static final EventType DELETE_SHEET_CLICKED = new EventType();

    public static final EventType NEW_SHEET_CLICKED = new EventType();
    public static final EventType EDIT_SHEET_CLICKED = new EventType();

    public static final EventType ITEM_RENDERER_CHANGED = new EventType();
    public static final EventType ITEM_EDITOR_CHANGED = new EventType();

    public static final EventType SAVE_CLICKED = new EventType();
    public static final EventType SAVE_AS_CLICKED = new EventType();
    public static final EventType CANCEL_CLICKED = new EventType();
    public static final EventType WINDOW_SHOWED = new EventType();
    public static final EventType REGISTER_SHEET = new EventType();

    public static final EventType SHOW_ADD_ATTRIBUTE_WINDOW = new EventType();
    public static final EventType ATTRIBUTE_EDITOR_CLICKED = new EventType();
    public static final EventType ADD_ATTRIBUTE = new EventType();

    public static final EventType CREATE_GROUP_CLICK = new EventType();
    public static final EventType CREATE_GROUP_REQUESTED = new EventType();
    public static final EventType DELETE_ITEM_CLICKED = new EventType();
    public static final EventType ITEM_GROUP_DROPPED = new EventType();

    public static final EventType GROUP_RENAMED = new EventType();

    public static final EventType NOTE_TYPE_EDITOR_CLICKED = new EventType();
    public static final EventType RELOAD_APPROVAL_NOTE_TYPES = new EventType();

    public static final EventType ATTRIBUTE_EDITOR_SHOWN = new EventType();
    public static final EventType ATTRIBUTE_EDITOR_HIDDEN = new EventType();

    public static final EventType IS_EXIST_SUCCEEDS = new EventType();
    public static final EventType APPROVAL_NOTE_TYPE_LOADED = new EventType();

    public static final EventType ADD_NEW_ATTRIBUTE_CLICK = new EventType();

}
