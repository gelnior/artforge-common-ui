package fr.hd3d.common.ui.client.widget.sheeteditor.view.widget;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.dnd.TreePanelDragSource;
import com.extjs.gxt.ui.client.event.DNDEvent;
import com.extjs.gxt.ui.client.widget.treepanel.TreePanel;
import com.extjs.gxt.ui.client.widget.treepanel.TreePanel.TreeNode;


public class EntityPropertiesPanelDragSource extends TreePanelDragSource
{

    public EntityPropertiesPanelDragSource(TreePanel<?> tree)
    {
        super(tree);
    }

    @Override
    protected void onDragStart(DNDEvent e)
    {
        super.onDragStart(e);

        // cancelled event if root node
        TreeNode n = tree.findNode(e.getTarget());
        if (n != null)
        {
            ModelData m = n.getModel();
            if (this.tree.getStore().getRootItems().indexOf(m) != -1)
            {
                e.setCancelled(true);
            }
        }
    }
}
