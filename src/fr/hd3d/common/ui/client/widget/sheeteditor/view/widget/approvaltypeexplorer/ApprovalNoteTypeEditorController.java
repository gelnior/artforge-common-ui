package fr.hd3d.common.ui.client.widget.sheeteditor.view.widget.approvaltypeexplorer;

import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteTypeModelData;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.common.ui.client.widget.simpleexplorer.SimpleExplorerController;
import fr.hd3d.common.ui.client.widget.simpleexplorer.SimpleExplorerModel;


public class ApprovalNoteTypeEditorController extends SimpleExplorerController<ApprovalNoteTypeModelData>
{

    public ApprovalNoteTypeEditorController(SimpleExplorerModel<ApprovalNoteTypeModelData> model,
            ApprovalNoteTypeEditor view)
    {
        super(model, view);
    }

    @Override
    protected void onAddClicked()
    {
        ApprovalNoteTypeModelData type = this.model.getNewModelInstance();
        type.setProject(MainModel.currentProject.getId());

        if (this.model.getModelStore() != null)
        {
            this.view.resetSelection();
            this.model.getModelStore().insert(type, 0);
        }
    }
}
