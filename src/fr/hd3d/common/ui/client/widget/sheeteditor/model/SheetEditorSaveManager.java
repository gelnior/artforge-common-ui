package fr.hd3d.common.ui.client.widget.sheeteditor.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.event.BaseObservable;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.util.Util;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ItemGroupModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.service.BulkRequests;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.callback.BaseBulkCallback;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.widget.explorer.model.modeldata.IColumn;
import fr.hd3d.common.ui.client.widget.explorer.model.modeldata.IColumnGroup;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.common.ui.client.widget.sheeteditor.event.SheetEditorEvents;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.callback.SheetCallback;


/**
 * Handles data to save to services.
 * 
 * @author HD3D
 */
public class SheetEditorSaveManager extends BaseObservable
{
    /** Model handling data for sheet editor. */
    private final SheetEditorModel model;

    /** The sheet to save. */
    private SheetModelData sheet;

    /** List of items to delete when sheet is saved. */
    private final List<ItemModelData> itemToDelete = new ArrayList<ItemModelData>();
    /** List of groups to delete when sheet is saved. */
    private final List<ItemGroupModelData> groupToDelete = new ArrayList<ItemGroupModelData>();

    /** Map used to store temporary group created for sheet save as. */
    private final HashMap<ItemGroupModelData, List<ItemModelData>> groupMap = new HashMap<ItemGroupModelData, List<ItemModelData>>();

    private final List<ItemGroupModelData> groupSaveAs = new ArrayList<ItemGroupModelData>();

    public SheetEditorSaveManager(SheetEditorModel model)
    {
        super();

        this.model = model;
    }

    /**
     * Add a group to the list of group to delete.
     * 
     * @param group
     *            The group to delete.
     */
    public void addDeletedGroup(ItemGroupModelData group)
    {
        if (group.getId() != null)
            this.groupToDelete.add(group);
    }

    /**
     * Add an item to the list of group to delete.
     * 
     * @param item
     *            The item to delete.
     */
    public void addDeletedItem(ItemModelData item)
    {
        if (item.getId() != null)
            this.itemToDelete.add(item);
    }

    /**
     * Start sheet saving process. Items are saved first, then groups are updated with new item IDs and saved, then
     * sheet is updated with new group IDs and saved. When new sheet saving is complete, items and groups to delete are
     * deleted.<br>
     * <br>
     * 
     * In a case of a save as same process is done but sheet, items and groups are duplicated to make a whole new sheet
     * in memory. Moreover sheet ID is reset.
     * 
     * @param sheet
     *            The sheet to save.
     */
    public void saveSheet(SheetModelData sheet)
    {
        this.sheet = sheet;

        AppEvent event = new AppEvent(SheetEditorEvents.SAVING_STARTED, sheet);
        EventDispatcher.forwardEvent(event);

        this.saveItems(sheet);
    }

    /**
     * Grab items from current properties tree and save modifications to backend services. In case of saves, it
     * duplicates the items and creates new ones from duplicate ones. Then it saves groups that wraps these items.
     * 
     * @param sheet
     *            The sheet of which items must be saved.
     */
    public void saveItems(SheetModelData sheet)
    {
        List<ItemGroupModelData> itemGroups = this.model.getSheetItemGroups();
        List<Hd3dModelData> itemsToCreate = new ArrayList<Hd3dModelData>();
        final List<Hd3dModelData> itemsToUpdate = new ArrayList<Hd3dModelData>();
        this.groupMap.clear();
        this.groupSaveAs.clear();

        for (ItemGroupModelData group : itemGroups)
        {
            List<IColumn> items = group.getColumns();

            if (this.model.isSaveAsMode())
                groupMap.put(group, new ArrayList<ItemModelData>());

            for (IColumn column : items)
            {
                ItemModelData item = (ItemModelData) column;
                this.setSpecificConfiguration(item);

                if (this.model.isSaveAsMode())
                {
                    ItemModelData newItem = new ItemModelData();
                    Hd3dModelData.copy(item, newItem);

                    newItem.setId(null);
                    newItem.setItemGroup(null);
                    newItem.setDefaultPath(getSheetItemPath(group.getId(), group.getSheetId()));
                    groupMap.get(group).add(newItem);

                    itemsToCreate.add(newItem);
                }
                else if (item.getId() == null)
                {
                    item.setDefaultPath(getSheetItemPath(group.getId(), group.getSheetId()));
                    item.setItemGroup(null);
                    itemsToCreate.add(item);
                }
                else
                {
                    item.setDefaultPath(getSheetItemPath(group.getId(), group.getSheetId()) + item.getId());
                    itemsToUpdate.add(item);
                }
            }
        }

        if (itemsToCreate.size() > 0)
        {
            BulkRequests.bulkPost(itemsToCreate, new BaseBulkCallback(itemsToCreate) {
                @Override
                protected void onIdsUpdated()
                {
                    onItemsCreated(itemsToUpdate);
                }
            });
        }
        else
        {
            this.onItemsCreated(itemsToUpdate);
        }
    }

    /**
     * Set specific configuration for step items.<br>
     * TODO: Refactor this, should be an a child class.
     * 
     * @param item
     *            The item to configure
     */
    private void setSpecificConfiguration(ItemModelData item)
    {
        if (item.getMetaType() == null || Util.isEmptyString(item.getMetaType()))
            item.setType((String) item.get("dataType"));

        if (FieldUtils.isStep(item.getQuery()))
        {
            String parameter = (String) item.get(ItemModelData.PARAM2_FIELD);
            if (parameter == null)
                parameter = (String) item.get(ItemModelData.PARAMETER_FIELD);
            item.set(ItemModelData.PARAMETER_FIELD, parameter);
        }
    }

    /**
     * When items are created, it saves the items to modify. By default all items are considered to be modified.
     * 
     * @param itemsToUpdate
     *            The items to update.
     */
    private void onItemsCreated(List<Hd3dModelData> itemsToUpdate)
    {
        if (itemsToUpdate.size() > 0)
        {
            BulkRequests.bulkPut(itemsToUpdate, new BaseBulkCallback(itemsToUpdate) {
                @Override
                protected void onIdsUpdated()
                {
                    onItemsSaved();
                }
            });
        }
        else
        {
            this.onItemsSaved();
        }
    }

    /**
     * Grab groups from current properties tree and save modifications to backend services. In case of saves, it
     * duplicates the groups and creates new ones from duplicate ones. Then it delete groups and items to delete.
     */
    public void onItemsSaved()
    {
        List<ItemGroupModelData> groupsToCreate = new ArrayList<ItemGroupModelData>();
        final List<ItemGroupModelData> groupsToUpdate = new ArrayList<ItemGroupModelData>();
        List<ItemGroupModelData> newGroups = this.model.getSheetItemGroups();

        sheet.getColumnGroups().clear();
        sheet.setItemGroupIds(null);

        for (ItemGroupModelData group : newGroups)
        {
            if (this.model.isSaveAsMode())
            {
                ItemGroupModelData newGroup = new ItemGroupModelData();
                Hd3dModelData.copy(group, newGroup);
                newGroup.setId(null);
                newGroup.setItemIds(null);
                newGroup.setSheet(null);
                newGroup.setDefaultPath(getSheetItemGroupPath(sheet));

                List<Long> ids = new ArrayList<Long>();
                for (ItemModelData item : groupMap.get(group))
                {
                    ids.add(item.getId());
                }
                newGroup.setItemIds(ids);
                newGroup.setItems(groupMap.get(group));

                groupSaveAs.add(newGroup);
                groupsToCreate.add(newGroup);
            }

            else if (group.getId() == null)
            {
                group.setDefaultPath(getSheetItemGroupPath(sheet));
                group.setSheet(null);
                List<Long> ids = new ArrayList<Long>();
                for (IColumn item : group.getColumns())
                {
                    ids.add(item.getId());
                }
                group.setItemIds(ids);
                groupsToCreate.add(group);
            }

            else
            {
                group.setDefaultPath(getSheetItemGroupPath(sheet) + group.getId());
                List<Long> ids = new ArrayList<Long>();
                for (IColumn item : group.getColumns())
                {
                    ids.add(item.getId());
                }
                group.setItemIds(ids);
                groupsToUpdate.add(group);
            }
        }

        for (ItemGroupModelData group : groupsToCreate)
            if ("default".equals(group.getName().toLowerCase()) && !this.model.isSaveAsMode())
                this.sheet.getGroups().add(group);

        if (groupsToCreate.size() > 0)
        {
            BulkRequests.bulkPost(groupsToCreate, new BaseBulkCallback(groupsToCreate) {
                @Override
                protected void onIdsUpdated()
                {
                    onGroupsCreated(groupsToUpdate);
                }
            });
        }
        else
        {
            this.onGroupsCreated(groupsToUpdate);
        }

    }

    /**
     * When groups are created, it saves the groups to modify. By default all groups are considered to be modified.
     * 
     * @param groupsToUpdate
     *            The groups to update.
     */
    protected void onGroupsCreated(List<ItemGroupModelData> groupsToUpdate)
    {
        if (groupsToUpdate.size() > 0)
        {
            BulkRequests.bulkPut(groupsToUpdate, new BaseBulkCallback(groupsToUpdate) {
                @Override
                protected void onIdsUpdated()
                {
                    onGroupsSaved();
                }
            });
        }
        else
        {
            this.onGroupsSaved();
        }
    }

    /**
     * When groups are saved, it reset current sheet groups with group from sheet properties tree. Then it saves the
     * sheet to backend services. When sheet is saved, SHEET_CREATED event is forwarded.
     */
    private void onGroupsSaved()
    {
        List<ItemGroupModelData> groups = this.model.getSheetItemGroups();
        if (this.model.isSaveAsMode())
            groups = this.groupSaveAs;

        List<ItemGroupModelData> itemGroups = new ArrayList<ItemGroupModelData>();

        if (CollectionUtils.isNotEmpty(groups))
        {
            for (IColumnGroup group : groups)
            {
                List<IColumn> items = group.getColumns();
                itemGroups.add((ItemGroupModelData) group);

                if (CollectionUtils.isNotEmpty(items))
                {
                    for (IColumn column : items)
                    {
                        ((ItemModelData) column).setItemGroupId(group.getId());
                    }
                }
            }
        }

        this.sheet.setItemGroupIds(null);
        this.sheet.setItemGroupIds(CollectionUtils.getIds(itemGroups));
        this.sheet.setItemGroups(groups);
        String path = ServicesPath.SHEETS;
        if (MainModel.getCurrentProject() != null)
            path = MainModel.getCurrentProject().getDefaultPath() + "/" + path;
        if (sheet.getId() != null)
            path += sheet.getId();
        this.sheet.setDefaultPath(path);
        this.sheet.save(new SheetCallback(this, this.sheet));
    }

    /**
     * When sheet is saved, deletion starts. Groups are delete first.
     */
    public void onSheetSaved()
    {
        deleteItemGroups();
    }

    /**
     * Recursive method that deletes groups one by one. When deletion finishes, items deletion starts.
     */
    private void deleteItemGroups()
    {
        if (groupToDelete.size() > 0 && !this.model.isSaveAsMode())
        {
            final ItemGroupModelData group = groupToDelete.get(0);

            if (group.getId() != null)
            {
                group.setDefaultPath(getSheetItemGroupPath(sheet) + group.getId());

                group.delete(new BaseCallback() {
                    @Override
                    protected void onError()
                    {
                        groupToDelete.remove(group);
                        deleteItemGroups();
                    }

                    @Override
                    protected void onSuccess(Request request, Response response)
                    {

                        groupToDelete.remove(group);
                        deleteItemGroups();
                    }
                });
            }
            groupToDelete.remove(group);
        }
        else
        {
            this.groupToDelete.clear();
            this.deleteItems();
        }
    }

    /**
     * Recursive method that deletes items one by one. When deletion finishes, items deletion starts.
     */
    private void deleteItems()
    {
        if (itemToDelete.size() > 0 && !this.model.isSaveAsMode())
        {
            final ItemModelData item = itemToDelete.get(0);
            if (item.getId() != null)
            {
                item.setDefaultPath(getSheetItemPath(item.getItemGroupId(), sheet.getId()) + item.getId());

                item.delete(new BaseCallback() {
                    @Override
                    protected void onError()
                    {
                        itemToDelete.remove(item);
                        deleteItems();
                    }

                    @Override
                    protected void onSuccess(Request request, Response response)
                    {
                        itemToDelete.remove(item);
                        deleteItems();
                    }
                });
            }
            else
            {
                itemToDelete.remove(item);
                deleteItems();
            }
        }
        else
        {
            this.itemToDelete.clear();

            this.model.setIsSaveAsMode(false);
            AppEvent event = new AppEvent(SheetEditorEvents.END_SAVE, sheet);
            event.setData(SheetEditorEvents.SHEET_ID_EVENT_VAR, this.sheet.getId());
            EventDispatcher.forwardEvent(event);
        }
    }

    private String getSheetItemPath(Long groupId, Long sheetId)
    {
        String path = ServicesPath.SHEETS + sheetId + "/" + ServicesPath.ITEM_GROUPS + groupId + "/"
                + ServicesPath.ITEMS;
        if (MainModel.getCurrentProject() != null)
            path = MainModel.getCurrentProject().getDefaultPath() + "/" + path;

        return path;
    }

    private String getSheetItemGroupPath(SheetModelData sheet)
    {
        return getSheetItemGroupPath(sheet.getId());
    }

    private String getSheetItemGroupPath(Long sheetId)
    {
        String path = ServicesPath.SHEETS + sheetId + "/" + ServicesPath.ITEM_GROUPS;
        if (MainModel.getCurrentProject() != null)
            path = MainModel.getCurrentProject().getDefaultPath() + "/" + path;

        return path;
    }

}
