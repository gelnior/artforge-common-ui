package fr.hd3d.common.ui.client.widget.sheeteditor.model.callback;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.service.callback.PostModelDataCallback;
import fr.hd3d.common.ui.client.widget.sheeteditor.event.SheetEditorEvents;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.SheetEditorSaveManager;


public class SheetCallback extends PostModelDataCallback
{
    private final SheetEditorSaveManager saveManager;
    private final SheetModelData sheet;

    public SheetCallback(SheetEditorSaveManager manager, SheetModelData sheet)
    {
        super(sheet, new AppEvent(SheetEditorEvents.SHEET_CREATED));
        this.saveManager = manager;
        this.sheet = sheet;
    }

    @Override
    public void onSuccess(Request request, Response response)
    {
        this.setNewId(response);
        this.saveManager.onSheetSaved();
        this.event.setData(sheet);

        EventDispatcher.forwardEvent(this.event);
    }
}
