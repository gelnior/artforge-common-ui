package fr.hd3d.common.ui.client.widget.sheeteditor.model.reader;

import fr.hd3d.common.ui.client.modeldata.reader.Hd3dListJsonReader;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata.EntityPropertyModelData;


public class EntityPropertyReader extends Hd3dListJsonReader<EntityPropertyModelData>
{

    public EntityPropertyReader()
    {
        super(EntityPropertyModelData.getModelType());
    }

    @Override
    public EntityPropertyModelData newModelInstance()
    {
        return new EntityPropertyModelData();
    }
}
