package fr.hd3d.common.ui.client.widget.sheeteditor.controller.command;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;
import fr.hd3d.common.ui.client.widget.sheeteditor.config.SheetEditorConfig;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.SheetEditorModel;
import fr.hd3d.common.ui.client.widget.sheeteditor.view.ISheetEditorView;


/**
 * Change renderer name inside model after user edition. <br>
 * 
 * @author HD3D
 */
public class RendererChangedSheetCmd extends SheetEditorCommand
{

    /**
     * Constructor.
     * 
     * @param model
     *            Model (data handler) for sheet editor.
     * @param view
     *            View (widget handler) for sheet editor.
     */
    public RendererChangedSheetCmd(SheetEditorModel model, ISheetEditorView view)
    {
        super(model, view);
    }

    /**
     * Change renderer name inside model after user edition.
     * 
     * @param event
     *            The cancel clicked event.
     */
    public void execute(AppEvent event)
    {
        ItemModelData item = event.getData(SheetEditorConfig.ITEM_EVENT_VAR_NAME);
        String value = event.getData(SheetEditorConfig.VALUE_EVENT_VAR_NAME);
        this.model.setItemRenderer(item, value);
    }
}
