package fr.hd3d.common.ui.client.widget.sheeteditor.view.widget.sheetitemstreegrid;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.widget.form.ComboBox;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;

public class ComboboxEditor<C extends ModelData> extends CellEditor
{
    /** The field combo box which act as the editor. */
    private final ComboBox<C> combo;

    /** Default Constructor. */
    public ComboboxEditor( ComboBox<C> combo )
    {
        super( combo );
        this.combo = combo;
    }
    

    @Override
    public Object preProcessValue(Object value)
    {
        if (value == null)
        {
            return value;
        }
        return combo.getStore().findModel(combo.getValueField(), value);
    }

    @Override
    public Object postProcessValue(Object value)
    {
        if (value == null)
        {
            if( combo.isEditable())
            {
                value = combo.getRawValue();
            }
            return value;
        }
        return ((ModelData) value).get( combo.getValueField() );
    }
}

