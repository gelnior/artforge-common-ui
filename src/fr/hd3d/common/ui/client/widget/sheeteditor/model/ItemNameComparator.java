package fr.hd3d.common.ui.client.widget.sheeteditor.model;

import java.io.Serializable;
import java.util.Comparator;

import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;


/**
 * Comparator that sorts items by their name.
 * 
 * @author HD3D
 */
public class ItemNameComparator implements Comparator<ItemModelData>, Serializable
{
    private static final long serialVersionUID = -882549197144875301L;

    public int compare(ItemModelData e1, ItemModelData e2)
    {
        return e1.getName().compareTo(e2.getName());
    }
}
