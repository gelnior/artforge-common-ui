package fr.hd3d.common.ui.client.widget.sheeteditor.controller.command;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.error.ErrorDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.common.ui.client.widget.sheeteditor.error.SheetEditorErrors;
import fr.hd3d.common.ui.client.widget.sheeteditor.event.SheetEditorEvents;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.SheetEditorModel;
import fr.hd3d.common.ui.client.widget.sheeteditor.view.ISheetEditorView;


/**
 * Save sheet currently built in sheet editor.
 * 
 * @author HD3D
 */
public class SaveSheetCmd extends SheetEditorCommand
{

    /**
     * Constructor.
     * 
     * @param model
     *            Model (data handler) for sheet editor.
     * @param view
     *            View (widget handler) for sheet editor.
     */
    public SaveSheetCmd(SheetEditorModel model, ISheetEditorView view)
    {
        super(model, view);
    }

    /**
     * Save sheet currently built in sheet editor.
     * 
     * @param event
     *            Save sheet clicked event.
     */
    public void execute(AppEvent event)
    {
        this.model.setIsSaveAsMode(Boolean.FALSE);
        if (this.model.getSheet().getId() == null
                || !this.model.getSheet().getName().equals(this.view.getSheetNameValue()))
        {
            this.model.getSheet().setName(this.view.getSheetNameValue());
            this.checkIsExist(this.model.getSheet());
        }
        else
        {
            save();
        }
    }

    /**
     * Check if the sheet name is already used.
     * 
     * @param sheet
     *            The sheet to check.
     */
    protected void checkIsExist(SheetModelData sheet)
    {
        if (this.view.getSheetNameValue() == null)
        {
            ErrorDispatcher.sendError(SheetEditorErrors.NO_SHEET_NAME);
        }
        else
        {
            if (MainModel.getCurrentProject() == null)
                sheet.checkIsExist(SheetEditorEvents.IS_EXIST_SUCCEEDS, SheetModelData.NAME_FIELD);
            else
                sheet.checkIsExist(MainModel.getCurrentProject().getDefaultPath() + "/" + ServicesPath.SHEETS,
                        SheetEditorEvents.IS_EXIST_SUCCEEDS, SheetModelData.NAME_FIELD);
        }
    }

    /** Save sheet currently edited. */
    protected void save()
    {
        if (this.view.getSheetNameValue() == null)
        {
            ErrorDispatcher.sendError(SheetEditorErrors.NO_SHEET_NAME);
        }
        else
        {
            this.model.setSheetName(this.view.getSheetNameValue());
            this.model.saveSheet();
            this.view.reset();
            this.view.hideDialog();
        }
    }

    /**
     * Build a new sheet from currently edited one and save the new sheet as a separate sheet.
     */
    protected void saveAs()
    {
        SheetModelData newSheet = new SheetModelData();
        Hd3dModelData.copy(this.model.getSheet(), newSheet);
        newSheet.setId(null);
        newSheet.setGroupLoaded(true);

        this.model.setSheet(null);
        this.model.setSheet(newSheet);
        this.model.setIsSaveAsMode(true);

        this.save();
    }
}
