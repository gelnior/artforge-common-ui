package fr.hd3d.common.ui.client.widget.sheeteditor.listener;

import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.WindowEvent;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.widget.sheeteditor.event.SheetEditorEvents;


public class ApprovalNoteTypeEditorWindowListener implements Listener<WindowEvent>
{
    public void handleEvent(WindowEvent be)
    {
        if (be.getType() == Events.Hide)
        {
            EventDispatcher.forwardEvent(new AppEvent(SheetEditorEvents.RELOAD_APPROVAL_NOTE_TYPES));
        }
    }
}
