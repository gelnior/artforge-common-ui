package fr.hd3d.common.ui.client.widget.sheeteditor.controller.command;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.SheetEditorModel;
import fr.hd3d.common.ui.client.widget.sheeteditor.view.ISheetEditorView;


/**
 * Save sheet currently built in sheet editor as a new sheet.
 * 
 * @author HD3D
 */
public class SaveAsSheetCmd extends SaveSheetCmd
{

    /**
     * Constructor.
     * 
     * @param model
     *            Model (data handler) for sheet editor.
     * @param view
     *            View (widget handler) for sheet editor.
     */
    public SaveAsSheetCmd(SheetEditorModel model, ISheetEditorView view)
    {
        super(model, view);
    }

    /**
     * Save sheet currently built in sheet editor as a new sheet.
     */
    @Override
    public void execute(AppEvent event)
    {
        this.model.setIsSaveAsMode(true);
        if (!this.model.isSaveAsMode())
        {
            this.model.getSheet().setName(this.view.getSheetNameValue());
            this.checkIsExist(this.model.getSheet());
        }
        else
        {
            SheetModelData sheet = new SheetModelData();
            sheet.setName(this.view.getSheetNameValue());
            this.checkIsExist(sheet);
        }
    }
}
