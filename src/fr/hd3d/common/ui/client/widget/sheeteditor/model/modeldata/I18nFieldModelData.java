package fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata;

import com.extjs.gxt.ui.client.data.BaseModelData;

import fr.hd3d.common.ui.client.service.ServicesField;


public class I18nFieldModelData extends BaseModelData
{
    private static final long serialVersionUID = 2148399412879116200L;

    public static final String VALUE_FIELD = "value";
    public static final String NAME_FIELD = "name";

    public I18nFieldModelData(String value)
    {
        this.setValue(value);
    }

    public void setValue(String value)
    {
        this.set(VALUE_FIELD, value);
        String human = ServicesField.getHumanName(value);
        if (human == null)
        {
            human = value;
        }

        if (human == null)
        {
            human = "Default";
        }
        this.set(NAME_FIELD, human);
    }

    public String getValue()
    {
        return this.get(VALUE_FIELD);
    }

    public String getName()
    {
        return this.get(NAME_FIELD);
    }
}
