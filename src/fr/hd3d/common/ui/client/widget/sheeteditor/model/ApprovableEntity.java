package fr.hd3d.common.ui.client.widget.sheeteditor.model;

import java.util.ArrayList;

import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;

public class ApprovableEntity
{
    private static ApprovableEntity approv = null;
    private final static ArrayList<String> approvable = new ArrayList<String>();

    protected  ApprovableEntity()
    {
        initEntityList();
    }
    
    public static ApprovableEntity getInstance()
    {
        if(null == approv) {
            approv = new ApprovableEntity();
         }
         return approv;
    }
    
    
    /** Initialize the list of entity. */
    private static void initEntityList()
    {
        approvable.add(ConstituentModelData.SIMPLE_CLASS_NAME.toLowerCase());
        approvable.add(ShotModelData.SIMPLE_CLASS_NAME.toLowerCase());
        approvable.add(SequenceModelData.SIMPLE_CLASS_NAME.toLowerCase());
    }

    
    public void addEntity(String fieldName)
    {
        approvable.add(fieldName.toLowerCase());
    }

    
    public boolean isApprovable(String fieldName)
    {
        return approvable.contains(fieldName.toLowerCase());
    }
}
