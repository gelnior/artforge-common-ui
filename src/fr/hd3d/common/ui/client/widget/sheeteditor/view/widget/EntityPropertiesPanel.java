package fr.hd3d.common.ui.client.widget.sheeteditor.view.widget;

import com.extjs.gxt.ui.client.store.TreeStore;
import com.extjs.gxt.ui.client.widget.treepanel.TreePanel;

import fr.hd3d.common.client.Sheet;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ItemGroupModelData;
import fr.hd3d.common.ui.client.widget.sheeteditor.controller.SheetEditorController;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata.EntityPropertyModelData;


public class EntityPropertiesPanel extends TreePanel<RecordModelData>
{
    private final SheetEditorController controller;

    public EntityPropertiesPanel(SheetEditorController controller, TreeStore<RecordModelData> store)
    {
        super(store);
        this.controller = controller;

        this.setBorders(false);
        this.initTree();
    }

    public SheetEditorController getController()
    {
        return this.controller;
    }

    private void initTree()
    {
        this.setDisplayProperty("name");
    }

    @Override
    protected boolean hasChildren(RecordModelData model)
    {
        int depth = this.store.getDepth(model);
        if (depth < 2)
        {
            return true;
        }
        if (depth == 2)
        {
            if (model instanceof EntityPropertyModelData)
            {
                EntityPropertyModelData item = (EntityPropertyModelData) model;
                if (item.getItemType().equals(Sheet.ITEMTYPE_METHOD) && item.getParameter() != null)
                {
                    return true;
                }
            }
            else if (model instanceof ItemGroupModelData)
            {
                return true;
            }
        }

        return false;
    }

}
