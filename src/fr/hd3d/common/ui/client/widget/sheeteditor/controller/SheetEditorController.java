package fr.hd3d.common.ui.client.widget.sheeteditor.controller;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.mvc.controller.CommandController;
import fr.hd3d.common.ui.client.widget.attributeeditor.event.ExtendedAttributeEditorEvents;
import fr.hd3d.common.ui.client.widget.sheeteditor.controller.command.AttributeEditorClickedSheetCmd;
import fr.hd3d.common.ui.client.widget.sheeteditor.controller.command.AttributeEditorHiddenSheetCmd;
import fr.hd3d.common.ui.client.widget.sheeteditor.controller.command.CancelSheetCmd;
import fr.hd3d.common.ui.client.widget.sheeteditor.controller.command.ChangeEditorSheetCmd;
import fr.hd3d.common.ui.client.widget.sheeteditor.controller.command.CreateGroupSheetCmd;
import fr.hd3d.common.ui.client.widget.sheeteditor.controller.command.DataTypeLoadedSheetCmd;
import fr.hd3d.common.ui.client.widget.sheeteditor.controller.command.DeleteItemClickedSheetCmd;
import fr.hd3d.common.ui.client.widget.sheeteditor.controller.command.EndSaveSheetCmd;
import fr.hd3d.common.ui.client.widget.sheeteditor.controller.command.EntityTypeChangedCmd;
import fr.hd3d.common.ui.client.widget.sheeteditor.controller.command.ErrorSheetCmd;
import fr.hd3d.common.ui.client.widget.sheeteditor.controller.command.ExtendedAttributeEditorClickedSheetCmd;
import fr.hd3d.common.ui.client.widget.sheeteditor.controller.command.IsExistSucceedsSheetCmd;
import fr.hd3d.common.ui.client.widget.sheeteditor.controller.command.NameChangedSheetCmd;
import fr.hd3d.common.ui.client.widget.sheeteditor.controller.command.NoteTypeEditorClickedSheetCmd;
import fr.hd3d.common.ui.client.widget.sheeteditor.controller.command.NoteTypesLoadedSheetCmd;
import fr.hd3d.common.ui.client.widget.sheeteditor.controller.command.PropertyLoadedSheetCmd;
import fr.hd3d.common.ui.client.widget.sheeteditor.controller.command.RefreshSheetGroupSheetCmd;
import fr.hd3d.common.ui.client.widget.sheeteditor.controller.command.ReloadApprovalNoteTypesSheetCmd;
import fr.hd3d.common.ui.client.widget.sheeteditor.controller.command.RenameGroupSheetCmd;
import fr.hd3d.common.ui.client.widget.sheeteditor.controller.command.RendererChangedSheetCmd;
import fr.hd3d.common.ui.client.widget.sheeteditor.controller.command.SaveAsSheetCmd;
import fr.hd3d.common.ui.client.widget.sheeteditor.controller.command.SaveSheetCmd;
import fr.hd3d.common.ui.client.widget.sheeteditor.controller.command.SetAvailableEntityTypeSheetCmd;
import fr.hd3d.common.ui.client.widget.sheeteditor.controller.command.StartSaveSheetCmd;
import fr.hd3d.common.ui.client.widget.sheeteditor.controller.command.WindowShowedSheetCmd;
import fr.hd3d.common.ui.client.widget.sheeteditor.event.SheetEditorEvents;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.SheetEditorModel;
import fr.hd3d.common.ui.client.widget.sheeteditor.view.ISheetEditorView;


/**
 * Sheet editor controller handles sheet editor events.
 * 
 * @author HD3D
 */
public class SheetEditorController extends CommandController
{
    /** View (widget handler) for sheet editor. */
    protected ISheetEditorView view;
    /** Model (data handler) for sheet editor. */
    protected SheetEditorModel model;

    /**
     * Constructor.
     * 
     * @param model
     *            Model (data handler) for sheet editor.
     * @param view
     *            View (widget handler) for sheet editor.
     */
    public SheetEditorController(ISheetEditorView view, SheetEditorModel model)
    {
        this.view = view;
        this.model = model;

        this.registerEvents();
    }

    private void registerEvents()
    {
        this.commandMap.put(SheetEditorEvents.ENTITY_CHANGED, new EntityTypeChangedCmd(model, view));
        // TODO: Refactor this event. It looks useless.
        this.commandMap.put(SheetEditorEvents.SHEET_NAME_CHANGED, new NameChangedSheetCmd(model, view));

        this.commandMap.put(SheetEditorEvents.ATTRIBUTE_EDITOR_HIDDEN, new AttributeEditorHiddenSheetCmd(model, view));
        this.commandMap
                .put(ExtendedAttributeEditorEvents.EDITOR_HIDDEN, new AttributeEditorHiddenSheetCmd(model, view));
        this.commandMap
                .put(SheetEditorEvents.ATTRIBUTE_EDITOR_CLICKED, new AttributeEditorClickedSheetCmd(model, view));
        this.commandMap.put(SheetEditorEvents.ADD_NEW_ATTRIBUTE_CLICK, new ExtendedAttributeEditorClickedSheetCmd(
                model, view));
        this.commandMap.put(SheetEditorEvents.NOTE_TYPE_EDITOR_CLICKED, new NoteTypeEditorClickedSheetCmd(model, view));

        this.commandMap.put(SheetEditorEvents.DATA_TYPE_LOADED, new DataTypeLoadedSheetCmd(model, view));
        this.commandMap.put(SheetEditorEvents.ENTITY_PROPERTY_LOADED, new PropertyLoadedSheetCmd(model, view));
        // TODO: Refactor : should be in a child class.
        this.commandMap.put(SheetEditorEvents.APPROVAL_NOTE_TYPE_LOADED, new NoteTypesLoadedSheetCmd(model, view));

        this.commandMap.put(SheetEditorEvents.SAVING_STARTED, new StartSaveSheetCmd(model, view));
        this.commandMap.put(SheetEditorEvents.END_SAVE, new EndSaveSheetCmd(model, view));
        this.commandMap.put(SheetEditorEvents.IS_EXIST_SUCCEEDS, new IsExistSucceedsSheetCmd(model, view));
        this.commandMap.put(SheetEditorEvents.SAVE_CLICKED, new SaveSheetCmd(model, view));
        this.commandMap.put(SheetEditorEvents.SAVE_AS_CLICKED, new SaveAsSheetCmd(model, view));
        this.commandMap.put(SheetEditorEvents.CANCEL_CLICKED, new CancelSheetCmd(model, view));

        this.commandMap.put(SheetEditorEvents.CREATE_GROUP_REQUESTED, new CreateGroupSheetCmd(model, view));
        this.commandMap.put(SheetEditorEvents.GROUP_RENAMED, new RenameGroupSheetCmd(model, view));
        this.commandMap.put(SheetEditorEvents.ITEM_GROUP_DROPPED, new RefreshSheetGroupSheetCmd(model, view));

        // TODO: Try to refactor it, should not be needed.
        this.commandMap.put(SheetEditorEvents.ITEM_EDITOR_CHANGED, new ChangeEditorSheetCmd(model, view));
        // TODO: Try to refactor it, should not be needed.
        this.commandMap.put(SheetEditorEvents.ITEM_RENDERER_CHANGED, new RendererChangedSheetCmd(model, view));
        this.commandMap.put(SheetEditorEvents.DELETE_ITEM_CLICKED, new DeleteItemClickedSheetCmd(model, view));

        this.commandMap.put(SheetEditorEvents.WINDOW_SHOWED, new WindowShowedSheetCmd(model, view));
        // TODO: Refactor this event.
        this.commandMap.put(SheetEditorEvents.SET_AVAILABLE_ENTITY_TYPE,
                new SetAvailableEntityTypeSheetCmd(model, view));
        // TODO: Refactor this event.
        this.commandMap.put(SheetEditorEvents.RELOAD_APPROVAL_NOTE_TYPES, new ReloadApprovalNoteTypesSheetCmd(model,
                view));

        this.commandMap.put(CommonEvents.ERROR, new ErrorSheetCmd(model, view));
    }
}
