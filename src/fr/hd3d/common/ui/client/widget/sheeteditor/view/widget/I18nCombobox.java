package fr.hd3d.common.ui.client.widget.sheeteditor.view.widget;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.form.ComboBox;

import fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata.I18nFieldModelData;


public class I18nCombobox extends ComboBox<I18nFieldModelData>
{
    public I18nCombobox(ListStore<I18nFieldModelData> store)
    {
        super();
        this.setStore(store);
        this.setValueField(I18nFieldModelData.VALUE_FIELD);
        this.setDisplayField(I18nFieldModelData.NAME_FIELD);
        this.setTriggerAction(TriggerAction.ALL);
        this.setEditable(true);
    }
}
