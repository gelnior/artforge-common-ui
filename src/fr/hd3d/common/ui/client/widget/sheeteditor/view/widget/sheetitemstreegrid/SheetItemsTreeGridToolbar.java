package fr.hd3d.common.ui.client.widget.sheeteditor.view.widget.sheetitemstreegrid;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.toolbar.ToolBar;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.widget.ToolBarButton;
import fr.hd3d.common.ui.client.widget.dialog.NameDialog;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.ExplorateurCSS;
import fr.hd3d.common.ui.client.widget.sheeteditor.event.SheetEditorEvents;


public class SheetItemsTreeGridToolbar extends ToolBar
{
    /** Constant strings to display : dialog messages, button label... */
    public static final CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    private NameDialog createDialog;
    private NameDialog renameDialog;

    private final Button addGroup = new Button();
    private final Button editGroup = new Button();
    private final Button deleteItem = new ToolBarButton(Hd3dImages.getDeleteIcon(), CONSTANTS.DeleteSelectedElements(),
            SheetEditorEvents.DELETE_ITEM_CLICKED);

    public SheetItemsTreeGridToolbar()
    {
        createButtons();
        addListeners();
    }

    private void createButtons()
    {
        addGroup.setToolTip(CONSTANTS.CreateNewGroup());
        addGroup.setIconStyle(ExplorateurCSS.ADD_FOLDER);
        addGroup.getElement().setNodeValue(ExplorateurCSS.ADD_FOLDER);
        this.add(addGroup);

        editGroup.setToolTip(CONSTANTS.EditGroup());
        editGroup.setIconStyle(ExplorateurCSS.EDIT_FOLDER);
        editGroup.getElement().setNodeValue(ExplorateurCSS.EDIT_FOLDER);
        this.add(editGroup);

        this.add(deleteItem);
    }

    private void addListeners()
    {
        addGroup.addListener(Events.Select, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent event)
            {
                if (createDialog == null)
                {
                    createDialog = new NameDialog(SheetEditorEvents.CREATE_GROUP_REQUESTED, CONSTANTS.CreateNewGroup());
                }
                createDialog.show();
            }
        });

        editGroup.addSelectionListener(new SelectionListener<ButtonEvent>() {
            @Override
            public void componentSelected(ButtonEvent ce)
            {
                if (renameDialog == null)
                {
                    renameDialog = new NameDialog(SheetEditorEvents.GROUP_RENAMED, "Rename group");
                }
                renameDialog.show();
            }
        });
    }
}
