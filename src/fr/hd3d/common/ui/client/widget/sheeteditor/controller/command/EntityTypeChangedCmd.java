package fr.hd3d.common.ui.client.widget.sheeteditor.controller.command;

import java.util.ArrayList;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.SheetEditorModel;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata.EntityModelData;
import fr.hd3d.common.ui.client.widget.sheeteditor.view.ISheetEditorView;


/**
 * Loads properties for selected entity.
 * 
 * @author HD3D
 */
public class EntityTypeChangedCmd extends SheetEditorCommand
{

    /***
     * Constructor.
     * 
     * @param model
     *            Model (data handler) for sheet editor.
     * @param view
     *            View (widget handler) for sheet editor.
     */
    public EntityTypeChangedCmd(SheetEditorModel model, ISheetEditorView view)
    {
        super(model, view);
    }

    /**
     * Loads properties for selected entity.
     * 
     * @param event
     *            Entity type Changed event.
     */
    public void execute(AppEvent event)
    {
        EntityModelData entity = event.getData();

        if (this.view.getEntityType() != entity)
        {
            this.view.setEntityType(entity);
        }

        if (entity == null)
        {
            this.model.getEntityPropertyModel().setEntityProperties(null, new ArrayList<ItemModelData>());
        }
        else
        {
            this.view.maskEntityPropertiesTree(true);
            this.model.getSheet().setBoundClassName(entity.getClassName());
            this.model.reloadEntityProperties();
        }
    }
}
