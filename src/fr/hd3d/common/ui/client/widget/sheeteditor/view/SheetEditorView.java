package fr.hd3d.common.ui.client.widget.sheeteditor.view;

import java.util.List;

import com.extjs.gxt.ui.client.Style.LayoutRegion;
import com.extjs.gxt.ui.client.Style.Scroll;
import com.extjs.gxt.ui.client.data.BaseModelData;
import com.extjs.gxt.ui.client.dnd.DND.Feedback;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.store.Record;
import com.extjs.gxt.ui.client.util.Margins;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.Text;
import com.extjs.gxt.ui.client.widget.form.ComboBox;
import com.extjs.gxt.ui.client.widget.form.ComboBox.TriggerAction;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.grid.GridSelectionModel;
import com.extjs.gxt.ui.client.widget.layout.BorderLayoutData;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.HBoxLayout;
import com.extjs.gxt.ui.client.widget.layout.HBoxLayout.HBoxLayoutAlign;
import com.extjs.gxt.ui.client.widget.layout.HBoxLayoutData;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.error.ErrorDispatcher;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.listener.EnterKeyListener;
import fr.hd3d.common.ui.client.listener.EventSelectionChangedListener;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.widget.BorderedPanel;
import fr.hd3d.common.ui.client.widget.attributeeditor.AttributeEditor;
import fr.hd3d.common.ui.client.widget.attributeeditor.ExtendedAttributeEditor;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.common.ui.client.widget.sheeteditor.SheetEditorWindow;
import fr.hd3d.common.ui.client.widget.sheeteditor.controller.SheetEditorController;
import fr.hd3d.common.ui.client.widget.sheeteditor.error.SheetEditorErrors;
import fr.hd3d.common.ui.client.widget.sheeteditor.event.SheetEditorEvents;
import fr.hd3d.common.ui.client.widget.sheeteditor.listener.ApprovalNoteTypeEditorWindowListener;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.SheetEditorModel;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata.EntityModelData;
import fr.hd3d.common.ui.client.widget.sheeteditor.view.widget.EntityPropertiesPanel;
import fr.hd3d.common.ui.client.widget.sheeteditor.view.widget.EntityPropertiesPanelDragSource;
import fr.hd3d.common.ui.client.widget.sheeteditor.view.widget.EntityPropertiesPanelToolbar;
import fr.hd3d.common.ui.client.widget.sheeteditor.view.widget.approvaltypeexplorer.ApprovalNoteTypeEditorWindow;
import fr.hd3d.common.ui.client.widget.sheeteditor.view.widget.sheetitemstreegrid.SheetItemsTreeDropTarget;
import fr.hd3d.common.ui.client.widget.sheeteditor.view.widget.sheetitemstreegrid.SheetItemsTreeGrid;
import fr.hd3d.common.ui.client.widget.sheeteditor.view.widget.sheetitemstreegrid.SheetItemsTreeGridDragSource;
import fr.hd3d.common.ui.client.widget.sheeteditor.view.widget.sheetitemstreegrid.SheetItemsTreeGridListener;
import fr.hd3d.common.ui.client.widget.sheeteditor.view.widget.sheetitemstreegrid.SheetItemsTreeGridToolbar;


/**
 * Widgets contains in sheet editor dialog.
 * 
 * @author HD3D
 */
public class SheetEditorView extends BorderedPanel implements ISheetEditorView
{

    /** Constant strings to display : dialog messages, button label... */
    public static final CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    /** Sheet editor dialog containing this view. */
    private final SheetEditorWindow sheetEditorDialog;

    /** Model (data handler) for sheet editor. */
    private final SheetEditorModel model = new SheetEditorModel();

    /** Controller (event handler) for sheet editor. */
    private final SheetEditorController controller = new SheetEditorController(this, model);

    /** Field used to set sheet name. */
    private TextField<String> sheetNameField;
    /** Combo box that allows to select entity type (useless most of the time). */
    private ComboBox<EntityModelData> entityTypeCombo;

    /** Panel to display list of properties available for current entity. */
    private EntityPropertiesPanel entityPropertiesPanel;
    /** Tool bar for property tree. */
    private EntityPropertiesPanelToolbar propertyToolbar;

    /**
     * Tree grid that display current sheet items (made of dropped properties). Edition is made in this grid. Is is
     * where properties are dropped and where editor/renderer are set.
     */
    private SheetItemsTreeGrid sheetPropertiesTree;
    /** Tool bar for sheet items tree grid. */
    private SheetItemsTreeGridToolbar itemsToolbar;

    /** Attribute editor to manage dynamic attributes. */
    private AttributeEditor attributeEditorWindow;
    /** Attribute editor to manage dynamic attributes of type list. */
    private ExtendedAttributeEditor extendedAttributeEditor;
    /** Note type editor to manage note types. */
    private ApprovalNoteTypeEditorWindow noteTypeEditor;

    /**
     * Constructor : build layout and widgets.
     * 
     * @param sheetEditorDialog
     *            Parent dialog.
     */
    public SheetEditorView(SheetEditorWindow sheetEditorDialog)
    {
        this.sheetEditorDialog = sheetEditorDialog;
        EventDispatcher.get().addController(controller);

        createFieldPanel();
        createWest();
        createCenter();

        setListeners();
    }

    /** @return Controller that handles sheet editor events. */
    public SheetEditorController getController()
    {
        return this.controller;
    }

    /**
     * TODO: Refactor
     */
    public void showNoteTypeEditor(Long projectId)
    {
        if (noteTypeEditor == null)
        {
            noteTypeEditor = new ApprovalNoteTypeEditorWindow();
            noteTypeEditor.addListener(Events.Hide, new ApprovalNoteTypeEditorWindowListener());
        }
        if (MainModel.getCurrentProject() != null)
            noteTypeEditor.setProject(MainModel.currentProject.getId());
        else
            noteTypeEditor.setProject(null);
        noteTypeEditor.show();
    }

    /**
     * @return Entity type.
     */
    public EntityModelData getEntityType()
    {
        return this.entityTypeCombo.getValue();
    }

    /**
     * Select entity in entity combo box.
     * 
     * @param entityType
     *            The entity to select.
     */
    public void setEntityType(EntityModelData entityType)
    {
        if (entityType != null)
        {
            this.entityTypeCombo.setValue(entityType);
            this.entityTypeCombo.setEnabled(true);
            if (this.entityTypeCombo.getStore().getCount() == 1)
                this.entityTypeCombo.hide();
        }
        else
        {
            this.entityTypeCombo.setValue(null);
            this.entityTypeCombo.setRawValue("");
        }
    }

    /**
     * Sets <i>name</i> as sheet name field raw value
     * 
     * @param name
     *            The name to set.
     */
    public void setSheetName(String name)
    {
        this.sheetNameField.setRawValue(name);
    }

    /**
     * Expands property tree and sheet items tree grid.
     */
    public void expandTrees()
    {
        this.entityPropertiesPanel.expandAll();
        this.sheetPropertiesTree.setExpanded(this.sheetPropertiesTree.getStore().getAt(0), true, true);
    }

    /**
     * Displays saving indicator.
     */
    public void showSaving()
    {
        this.sheetEditorDialog.showSaving();
    }

    /**
     * Hides saving indicator.
     */
    public void hideSaving()
    {
        this.sheetEditorDialog.hideSaving();
    }

    /**
     * @return Sheet name value.
     */
    public String getSheetNameValue()
    {
        return this.sheetNameField.getValue();
    }

    /**
     * Enables or disables entity combo box.
     * 
     * @param enable
     *            True to enable, false to disable.
     */
    public void setEntityTypeComboboxEnabled(boolean enable)
    {
        this.entityTypeCombo.setEnabled(enable);
    }

    /**
     * Show attribute editor for current entity.
     */
    public void showAttributeEditor()
    {
        if (this.entityTypeCombo.getRawValue() == null || this.entityTypeCombo.getRawValue().equals(""))
        {
            ErrorDispatcher.sendError(SheetEditorErrors.NO_ENTITY_SELECTED);
        }
        else
        {
            if (attributeEditorWindow == null)
            {
                attributeEditorWindow = new AttributeEditor();
            }
            attributeEditorWindow.setEntityType(this.entityTypeCombo.getValue());
            attributeEditorWindow.show();
        }
    }

    /**
     * Select first entity set inside entity type combo box store.
     */
    public void selectFirstEntity()
    {
        if (entityTypeCombo.getStore().getCount() > 0)
        {
            entityTypeCombo.setValue(entityTypeCombo.getStore().getAt(0));
        }
    }

    /**
     * @return Items selected in sheet items tree grid.
     */
    public List<BaseModelData> getSelectedSheetItems()
    {
        return sheetPropertiesTree.getSelectionModel().getSelectedItems();
    }

    /**
     * Reset sheet editor fields.
     */
    public void reset()
    {
        this.entityTypeCombo.setValue(null);
        this.sheetNameField.setValue("");
    }

    /**
     * Mask entity property panel (to show that it is loading).
     * 
     * @param isMasked
     *            True if entity properties panel will be masked.
     */
    public void maskEntityPropertiesTree(Boolean isMasked)
    {
        if (isMasked)
        {
            this.entityPropertiesPanel.mask(CONSTANTS.Loading());
        }
        else
        {
            this.entityPropertiesPanel.unmask();
        }
    }

    /**
     * Select in entities combo box entity corresponding to simple class name.
     */
    public void selectEntity(String simpleClassName)
    {
        EntityModelData entity = entityTypeCombo.getStore()
                .findModel(EntityModelData.CLASS_NAME_FIELD, simpleClassName);

        if (entity != null)
        {
            entityTypeCombo.setValue(entity);
        }
    }

    /** @return Grid record corresponding to selected sheet item. */
    public Record getSelectedItemRecord()
    {
        GridSelectionModel<BaseModelData> sm = this.sheetPropertiesTree.getSelectionModel();
        BaseModelData item = sm.getSelectedItem();
        return this.sheetPropertiesTree.getStore().getRecord(item);
    }

    /**
     * Hide button that give access to validation editor.
     */
    public void hideValidationButton()
    {
        this.propertyToolbar.setApprovalVisiblity(false);
    }

    /**
     * Display sheet editor dialog.
     */
    public void showDialog()
    {
        this.sheetEditorDialog.show();
    }

    /**
     * Display sheet editor dialog.
     */
    public void hideDialog()
    {
        this.sheetEditorDialog.hide();
    }

    /**
     * Displays extended attribute editor.
     */
    public void showExtendedAttributeEditor()
    {
        if (this.entityTypeCombo.getRawValue() == null || this.entityTypeCombo.getRawValue().equals(""))
        {
            MessageBox.alert(CONSTANTS.Error(), "Please select a type before", null);
        }
        else
        {
            if (extendedAttributeEditor == null)
            {
                extendedAttributeEditor = new ExtendedAttributeEditor();
            }
            extendedAttributeEditor.setEntityType(this.entityTypeCombo.getValue());
            extendedAttributeEditor.show();
        }
    }

    /**
     * Display message corresponding to given error code.
     * 
     * @param error
     *            The error code.
     */
    public void displayError(int error)
    {
        switch (error)
        {
            case SheetEditorErrors.NO_SHEET_NAME:
                MessageBox.alert(CONSTANTS.Error(), CONSTANTS.SetASheetName(), null);
                break;
            case SheetEditorErrors.SAVE_GROUP_ERROR:
                MessageBox.alert(CONSTANTS.Error(), CONSTANTS.SavingErrorSheet(), null);
                break;
            case SheetEditorErrors.SHEET_NAME_EXIST:
                MessageBox.alert(CONSTANTS.Error(), CONSTANTS.SheetNameAlreadyExist(), null);
                break;
            case SheetEditorErrors.NO_ENTITY_SELECTED:
                MessageBox.alert(CONSTANTS.Error(), "Please select an entity before", null);
                break;
            default:
                break;
        }
    }

    /**
     * Set currently edited sheet.
     * 
     * @param sheet
     *            The sheet to edit.
     */
    public void setSheet(SheetModelData sheet)
    {
        this.model.setSheet(sheet);
    }

    /**
     * Hide save as button of the sheet editor dialog.
     */
    public void hideSaveAsButton()
    {
        this.sheetEditorDialog.hideSaveAsButton();
    }

    /**
     * Show save as button of the sheet editor dialog.
     */
    public void showSaveAsButton()
    {
        this.sheetEditorDialog.showSaveAsButton();
    }

    /**
     * Set to north sheet name field and entity type combo box.
     */
    private void createFieldPanel()
    {
        HBoxLayout layout = new HBoxLayout();
        layout.setHBoxLayoutAlign(HBoxLayoutAlign.TOP);

        LayoutContainer northContainer = new LayoutContainer();
        northContainer.setWidth("100%");
        northContainer.setHeight(45);
        northContainer.setLayout(layout);
        northContainer.setStyleAttribute("padding-top", "8px");

        this.sheetNameField = new TextField<String>();
        this.sheetNameField.setWidth(231);
        this.sheetNameField.setMaxLength(200);
        this.sheetNameField.setFieldLabel(CONSTANTS.SheetName());
        this.sheetNameField.setEmptyText("Sheet name...");

        entityTypeCombo = new ComboBox<EntityModelData>();
        entityTypeCombo.setEmptyText(CONSTANTS.SelectEntityType());
        entityTypeCombo.setDisplayField("name");
        entityTypeCombo.setWidth(150);
        entityTypeCombo.setTypeAhead(true);
        entityTypeCombo.setForceSelection(true);
        entityTypeCombo.setTriggerAction(TriggerAction.ALL);
        entityTypeCombo.setFieldLabel(CONSTANTS.Type());
        entityTypeCombo.setStore(this.model.getEntityTypeStore());
        entityTypeCombo.setEnabled(true);

        northContainer.add(sheetNameField, new HBoxLayoutData(new Margins(0, 0, 0, 5)));
        HBoxLayoutData flex = new HBoxLayoutData(new Margins(0, 0, 0, 0));
        flex.setFlex(1);
        northContainer.add(new Text(), flex);
        northContainer.add(entityTypeCombo, new HBoxLayoutData(new Margins(0, 5, 0, 0)));

        this.addNorth(northContainer, 30, 0, 0, 0, 0);
    }

    /**
     * Set to west the property tree.
     */
    private void createWest()
    {
        FitLayout layout = new FitLayout();

        ContentPanel westPanel = new ContentPanel();
        westPanel.setHeaderVisible(false);
        westPanel.setLayout(layout);
        westPanel.setBorders(false);
        westPanel.setHeight(200);

        entityPropertiesPanel = new EntityPropertiesPanel(controller, this.model.getEntityPropertyStore());
        propertyToolbar = new EntityPropertiesPanelToolbar(entityPropertiesPanel);
        westPanel.add(entityPropertiesPanel);
        westPanel.setTopComponent(propertyToolbar);

        this.addWest(westPanel, 230, 5, 0, 5, 5);
    }

    /**
     * Set to center the sheet item tree grid.
     */
    private void createCenter()
    {
        FitLayout layout = new FitLayout();
        itemsToolbar = new SheetItemsTreeGridToolbar();

        sheetPropertiesTree = new SheetItemsTreeGrid(this.model.getSheetPropertyStore());
        sheetPropertiesTree.addListener(Events.BeforeEdit, new SheetItemsTreeGridListener());

        ContentPanel centerPanel = new ContentPanel();
        centerPanel.setHeaderVisible(false);
        centerPanel.setLayout(layout);
        centerPanel.setBorders(false);
        centerPanel.setScrollMode(Scroll.AUTOX);
        centerPanel.setTopComponent(itemsToolbar);
        centerPanel.setHeight(200);
        centerPanel.add(sheetPropertiesTree);

        BorderLayoutData centerData = new BorderLayoutData(LayoutRegion.CENTER);
        centerData.setMargins(new Margins(5));
        this.add(centerPanel, centerData);
    }

    /**
     * Set change listener on entity type combo to refresh available properties when type is changed. Set key listener
     * on sheet name combo field when enter key is typed on sheet name field.
     */
    private void setListeners()
    {
        this.entityTypeCombo.addSelectionChangedListener(new EventSelectionChangedListener<EntityModelData>(
                SheetEditorEvents.ENTITY_CHANGED));
        this.sheetNameField.addKeyListener(new EnterKeyListener(SheetEditorEvents.SAVE_CLICKED));

        this.registerDragSource();
        this.registerDropTarget();
    }

    /**
     * Register drag source on entity properties list to allow dragging properties from property tree to item tree grid.
     */
    private void registerDragSource()
    {
        new EntityPropertiesPanelDragSource(this.entityPropertiesPanel);
        new SheetItemsTreeGridDragSource(this.sheetPropertiesTree);
    }

    /**
     * Register drop target on item tree grid to allow dragging properties from property tree to item tree grid.
     */
    private void registerDropTarget()
    {
        SheetItemsTreeDropTarget target = new SheetItemsTreeDropTarget(this.sheetPropertiesTree, this.controller);
        target.setAllowSelfAsSource(true);
        target.setFeedback(Feedback.BOTH);
    }

}
