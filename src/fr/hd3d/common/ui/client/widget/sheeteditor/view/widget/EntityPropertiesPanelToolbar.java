package fr.hd3d.common.ui.client.widget.sheeteditor.view.widget;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.store.Store;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.toolbar.FillToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.ToolBar;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.widget.SimpleStoreFilterField;
import fr.hd3d.common.ui.client.widget.ToolBarLocalButton;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.ExplorateurCSS;
import fr.hd3d.common.ui.client.widget.sheeteditor.event.SheetEditorEvents;


public class EntityPropertiesPanelToolbar extends ToolBar
{
    /** Constant strings to display : dialog messages, button label... */
    public static final CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    Button addAttribute;
    Button crudApprovalNoteType;

    ToolBarLocalButton addExtendedAttribute;
    EntityPropertiesPanel treePanel;
    SimpleStoreFilterField<RecordModelData> filterField = new SimpleStoreFilterField<RecordModelData>() {

        @Override
        protected boolean doSelect(Store<RecordModelData> store, RecordModelData parent, RecordModelData record,
                String property, String filter)
        {
            if (treePanel.hasChildren(record))
            {
                return false;
            }
            String name = record.getName();
            name = name.toLowerCase();
            if (name.startsWith(filter.toLowerCase()))
            {
                return true;
            }
            return false;
        }

    };

    public EntityPropertiesPanelToolbar(EntityPropertiesPanel treePanel)
    {
        this.treePanel = treePanel;

        filterField.bind(treePanel.getStore());
        filterField.setWidth(80);
        filterField.setEmptyText("Filter...");
        createButtons();
        addListeners();
    }

    public void setApprovalVisiblity(Boolean visible)
    {
        this.crudApprovalNoteType.setVisible(visible);
    }

    private void createButtons()
    {
        addAttribute = new Button();
        addAttribute.setToolTip(CONSTANTS.CreateNewAttribute());
        addAttribute.setIconStyle(ExplorateurCSS.ADD_ATTRIBUTE);
        addAttribute.getElement().setNodeValue(ExplorateurCSS.ADD_ATTRIBUTE);
        this.add(addAttribute);

        this.addExtendedAttribute = new ToolBarLocalButton(treePanel.getController(), Hd3dImages.getAddAttributeIcon(),
                "Add special attribute", SheetEditorEvents.ADD_NEW_ATTRIBUTE_CLICK);

        crudApprovalNoteType = new Button();
        crudApprovalNoteType.setToolTip(CONSTANTS.EditApprovalNoteType());
        crudApprovalNoteType.setIconStyle(ExplorateurCSS.ADD_APPROVAL_TYPE);
        crudApprovalNoteType.getElement().setNodeValue(ExplorateurCSS.ADD_APPROVAL_TYPE);
        this.add(crudApprovalNoteType);

        this.add(new FillToolItem());
        this.add(filterField);
    }

    private void addListeners()
    {
        addAttribute.addListener(Events.Select, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                EventDispatcher.forwardEvent(SheetEditorEvents.ATTRIBUTE_EDITOR_CLICKED);
            }
        });

        // addNewAttribute.addListener(Events.Select, new Listener<BaseEvent>() {
        // public void handleEvent(BaseEvent be)
        // {
        // EventDispatcher.forwardEvent(SheetEditorEvents.ADD_NEW_ATTRIBUTE_CLICK);
        // }
        // });

        crudApprovalNoteType.addListener(Events.Select, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                EventDispatcher.forwardEvent(SheetEditorEvents.NOTE_TYPE_EDITOR_CLICKED);
            }
        });
    }
}
