package fr.hd3d.common.ui.client.widget.sheeteditor.view.widget.sheetitemstreegrid;

import java.util.Arrays;

import com.extjs.gxt.ui.client.data.BaseModelData;
import com.extjs.gxt.ui.client.store.TreeStore;
import com.extjs.gxt.ui.client.widget.form.ComboBox;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.EditorGrid.ClicksToEdit;
import com.extjs.gxt.ui.client.widget.treegrid.EditorTreeGrid;
import com.extjs.gxt.ui.client.widget.treegrid.TreeGridCellRenderer;
import com.extjs.gxt.ui.client.widget.treegrid.TreeGridSelectionModel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Element;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.SheetEditorModel;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata.I18nFieldModelData;
import fr.hd3d.common.ui.client.widget.sheeteditor.view.widget.I18nCombobox;


public class SheetItemsTreeGrid extends EditorTreeGrid<BaseModelData>
{
    /** Constant strings to display : dialog messages, button label... */
    private final static CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    public SheetItemsTreeGrid(TreeStore<BaseModelData> store)
    {
        super(store, initColumnModel());
        initTree();
    }

    private static ColumnModel initColumnModel()
    {

        ColumnConfig name = new ColumnConfig("name", CONSTANTS.Name(), 230);
        name.setRenderer(new TreeGridCellRenderer<BaseModelData>());
        name.setSortable(false);
        name.setEditor(new CellEditor(new TextField<String>()));
        name.setToolTip("Name of the column to display inside sheet.");

        ColumnConfig dynName = new ColumnConfig(ItemModelData.CLASS_DYN_NAME_FIELD, "Metadata", 150);
        dynName.setSortable(false);
        dynName.setToolTip("Name of the associated dynamic attribut.");
        
        ColumnConfig renderer = new ColumnConfig("renderer", CONSTANTS.Display(), 150);
        renderer.setSortable(false);

        ComboBox<I18nFieldModelData> comboRenderer = new I18nCombobox(SheetEditorModel.rendererStore);
        comboRenderer.setEditable(false);
        CellEditor cellEditorRenderer = new ComboboxEditor<I18nFieldModelData>(comboRenderer);
        renderer.setEditor(cellEditorRenderer);
        renderer.setToolTip("Select the way you want display cell data. Simple text is default.");

        ColumnConfig editor = new ColumnConfig("editor", CONSTANTS.Edition(), 150);
        ComboBox<I18nFieldModelData> comboEditor = new I18nCombobox(SheetEditorModel.editorStore);
        comboEditor.setEditable(false);
        CellEditor cellEditorEditor = new ComboboxEditor<I18nFieldModelData>(comboEditor);
        editor.setEditor(cellEditorEditor);
        editor.setSortable(false);
        editor.setToolTip("Select the way you want edit cell data. If no value is entered, the column cells are not editable.");

        return new ColumnModel(Arrays.asList(name, dynName, renderer, editor));
    }

    private void initTree()
    {
        this.setBorders(false);
        this.setLazyRowRender(0);
        this.setClicksToEdit(ClicksToEdit.TWO);
        this.setSelectionModel(new TreeGridSelectionModel<BaseModelData>());
    }

    @Override
    protected boolean hasChildren(BaseModelData model)
    {
        if (model instanceof ItemModelData)
        {
            return false;
        }
        return true;
    }

    @Override
    protected void onRender(Element target, int index)
    {
        super.onRender(target, index);
    }

}
