package fr.hd3d.common.ui.client.widget.sheeteditor.listener;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;

import fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.DeleteSheetToolItem;


public class DeleteSheetClickListener extends SelectionListener<ButtonEvent>
{
    DeleteSheetToolItem toolItem;
    
    public DeleteSheetClickListener(DeleteSheetToolItem toolItem)
    {
        this.toolItem = toolItem;
    }
    
    @Override
    public void componentSelected(ButtonEvent ce)
    {
        this.toolItem.askConfirmation();
    }
}
