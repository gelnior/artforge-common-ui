package fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;


public class EntityPropertyModelData extends ItemModelData
{
    private static final long serialVersionUID = -2165790311307166805L;
    public static final String DATA_TYPE = "dataType";
    public static final String PARAMETER_FIELD = "parameter";

    public EntityPropertyModelData()
    {

    }

    public static ModelType getModelType()
    {
        ModelType type = new ModelType();
        type.setRoot(Hd3dModelData.ROOT);
        type.setTotalName(Hd3dModelData.NB_RECORDS);

        type.addField(NAME_FIELD);
        type.addField(QUERY);
        type.addField(ITEM_TYPE);
        type.addField(DATA_TYPE);
        type.addField(PARAMETER_FIELD);
        type.addField(PARAM_FIELD);

        return type;
    }

    @Override
    public String getType()
    {
        return get(DATA_TYPE);
    }

    @Override
    public void setParameter(String parameter)
    {
        this.set(PARAMETER_FIELD, parameter);
    }

    @Override
    public String getParameter()
    {
        return this.get(PARAMETER_FIELD);
    }
}
