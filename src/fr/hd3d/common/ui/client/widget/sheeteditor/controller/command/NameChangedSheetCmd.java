package fr.hd3d.common.ui.client.widget.sheeteditor.controller.command;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.widget.sheeteditor.model.SheetEditorModel;
import fr.hd3d.common.ui.client.widget.sheeteditor.view.ISheetEditorView;


/**
 * TODO: Weird event, should be refactored.
 * 
 * @author HD3D
 */
public class NameChangedSheetCmd extends SheetEditorCommand
{

    /**
     * Constructor.
     * 
     * @param model
     *            Model (data handler) for sheet editor.
     * @param view
     *            View (widget handler) for sheet editor.
     */
    public NameChangedSheetCmd(SheetEditorModel model, ISheetEditorView view)
    {
        super(model, view);
    }

    /**
     * TODO: Weird event, should be refactored.
     * 
     * @param event
     *            Sheet name changed event.
     */
    public void execute(AppEvent event)
    {
        this.view.setSheetName((String) event.getData());
        this.model.setSheetName((String) event.getData());
        this.view.expandTrees();
    }

}
