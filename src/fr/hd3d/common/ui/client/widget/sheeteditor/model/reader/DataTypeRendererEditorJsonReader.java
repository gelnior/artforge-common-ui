package fr.hd3d.common.ui.client.widget.sheeteditor.model.reader;

import fr.hd3d.common.ui.client.modeldata.reader.Hd3dListJsonReader;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata.DataTypeRendererEditorModelData;


public class DataTypeRendererEditorJsonReader extends Hd3dListJsonReader<DataTypeRendererEditorModelData>
{
    public DataTypeRendererEditorJsonReader()
    {
        super(DataTypeRendererEditorModelData.getModelType());
    }

    @Override
    public DataTypeRendererEditorModelData newModelInstance()
    {
        return new DataTypeRendererEditorModelData();
    }
}
