package fr.hd3d.common.ui.client.widget.sheeteditor.controller.command;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.config.CommonConfig;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.SheetEditorModel;
import fr.hd3d.common.ui.client.widget.sheeteditor.view.ISheetEditorView;


/**
 * When error occurs, saving indicator, it hides saving and displays an error message.
 * 
 * @author HD3D
 */
public class ErrorSheetCmd extends SheetEditorCommand
{

    /**
     * Constructor.
     * 
     * @param model
     *            Model (data handler) for sheet editor.
     * @param view
     *            View (widget handler) for sheet editor.
     */
    public ErrorSheetCmd(SheetEditorModel model, ISheetEditorView view)
    {
        super(model, view);
    }

    /**
     * When error occurs, saving indicator, it hides saving and displays an error message.
     * 
     * @param event
     *            The cancel clicked event.
     */
    public void execute(AppEvent event)
    {
        Integer error = event.getData(CommonConfig.ERROR_EVENT_VAR_NAME);

        this.view.hideSaving();
        this.view.displayError(error.intValue());
    }
}
