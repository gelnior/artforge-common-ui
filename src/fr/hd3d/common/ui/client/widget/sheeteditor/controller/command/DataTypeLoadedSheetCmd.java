package fr.hd3d.common.ui.client.widget.sheeteditor.controller.command;

import java.util.List;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.widget.sheeteditor.config.SheetEditorConfig;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.SheetEditorModel;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata.DataTypeRendererEditorModelData;
import fr.hd3d.common.ui.client.widget.sheeteditor.view.ISheetEditorView;


/**
 * When data types are loaded, they are stored inside model.
 * 
 * @author HD3D
 */
public class DataTypeLoadedSheetCmd extends SheetEditorCommand
{

    /**
     * Constructor.
     * 
     * @param model
     *            Model (data handler) for sheet editor.
     * @param view
     *            View (widget handler) for sheet editor.
     */
    public DataTypeLoadedSheetCmd(SheetEditorModel model, ISheetEditorView view)
    {
        super(model, view);
    }

    /**
     * When data types are loaded, they are stored inside model.
     * 
     * @param event
     *            The cancel clicked event.
     */
    public void execute(AppEvent event)
    {
        List<DataTypeRendererEditorModelData> dataType = event.getData();
        String file = event.getData(SheetEditorConfig.FILE_EVENT_VAR_NAME);
        this.model.setFileLoaded(file);
        this.model.addDataType(dataType);
    }

}
