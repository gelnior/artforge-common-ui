package fr.hd3d.common.ui.client.widget.sheeteditor.controller.command;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.widget.sheeteditor.model.SheetEditorModel;
import fr.hd3d.common.ui.client.widget.sheeteditor.view.ISheetEditorView;


/**
 * When saving sheet action finishes, it hides saving indicator.
 * 
 * @author HD3D
 */
public class EndSaveSheetCmd extends SheetEditorCommand
{

    /**
     * Constructor.
     * 
     * @param model
     *            Model (data handler) for sheet editor.
     * @param view
     *            View (widget handler) for sheet editor.
     */
    public EndSaveSheetCmd(SheetEditorModel model, ISheetEditorView view)
    {
        super(model, view);
    }

    /**
     * When saving sheet action finishes, it hides saving indicator.
     * 
     * @param event
     *            The saving sheet event.
     */
    public void execute(AppEvent event)
    {
        this.view.hideSaving();
    }

}
