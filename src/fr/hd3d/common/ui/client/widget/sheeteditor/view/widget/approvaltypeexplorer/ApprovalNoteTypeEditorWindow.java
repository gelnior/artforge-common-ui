package fr.hd3d.common.ui.client.widget.sheeteditor.view.widget.approvaltypeexplorer;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.store.Record;
import com.extjs.gxt.ui.client.util.Util;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteTypeModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.LogicConstraint;
import fr.hd3d.common.ui.client.widget.AutoTaskTypeComboBox;
import fr.hd3d.common.ui.client.widget.grid.editor.TaskTypeComboBoxEditor;


public class ApprovalNoteTypeEditorWindow extends Window
{
    protected ApprovalNoteTypeEditor explorer = new ApprovalNoteTypeEditor();
    protected AutoTaskTypeComboBox taskTypeCombo;

    protected LogicConstraint projectFilterGroup;
    protected Long projectId;

    public ApprovalNoteTypeEditorWindow()
    {
        super();
        this.taskTypeCombo = new AutoTaskTypeComboBox();

        this.setHeading("Approval Note Type editor");
        this.setStyles();
        this.setExplorer();
    }

    public void setProject(Long projectId)
    {
        this.projectId = projectId;
        this.taskTypeCombo.setProject(projectId);

        if (this.projectId == null)
        {
            this.explorer.setPath(ServicesPath.getPath(ApprovalNoteTypeModelData.SIMPLE_CLASS_NAME));
        }
        else
        {
            this.explorer.setPath(ServicesPath.getPath(ProjectModelData.SIMPLE_CLASS_NAME) + this.projectId + "/"
                    + ServicesPath.getPath(ApprovalNoteTypeModelData.SIMPLE_CLASS_NAME));
        }

        this.explorer.refresh();
    }

    @Override
    public void show()
    {
        super.show();
        this.explorer.unIdle();
    }

    @Override
    public void hide()
    {
        super.hide();
        this.explorer.idle();
    }

    private void setExplorer()
    {
        this.explorer.reconfigureGrid(this.getColumnModel());
        this.explorer.setBodyBorder(false);
        this.explorer.setBorders(false);
        this.add(explorer);

        this.explorer.getGrid().addListener(Events.ValidateEdit, new Listener<GridEvent<ApprovalNoteTypeModelData>>() {
            public void handleEvent(GridEvent<ApprovalNoteTypeModelData> be)
            {
                if (be.getProperty().equals(ApprovalNoteTypeModelData.TASK_TYPE_FIELD))
                {
                    if (be.getValue() instanceof TaskTypeModelData)
                    {
                        TaskTypeModelData value = (TaskTypeModelData) be.getValue();
                        Record rec = be.getRecord();

                        rec.set(ApprovalNoteTypeModelData.TASK_TYPE_FIELD, value.getId());
                        rec.set(ApprovalNoteTypeModelData.TASK_TYPE_NAME_FIELD, value.getName());

                        be.setValue(value.getId());
                    }
                }
            }
        });

        EventDispatcher.get().addController(this.explorer.getController());
    }

    private void setStyles()
    {
        this.setModal(true);
        this.setSize(400, 300);
        this.setLayout(new FitLayout());
    }

    private ColumnModel getColumnModel()
    {
        ColumnConfig nameCC = new ColumnConfig(ApprovalNoteTypeModelData.NAME_FIELD, "Name", 150);
        nameCC.setEditor(new CellEditor(new TextField<String>()));

        ColumnConfig taskTypeCC = new ColumnConfig(ApprovalNoteTypeModelData.TASK_TYPE_FIELD, "Task type", 150);
        CellEditor editor = new TaskTypeComboBoxEditor();
        taskTypeCC.setEditor(editor);
        taskTypeCC.setRenderer(new GridCellRenderer<ApprovalNoteTypeModelData>() {

            public Object render(ApprovalNoteTypeModelData model, String property, ColumnData config, int rowIndex,
                    int colIndex, ListStore<ApprovalNoteTypeModelData> store, Grid<ApprovalNoteTypeModelData> grid)
            {
                String cellCode = "";
                if (!Util.isEmptyString(model.getTaskTypeName()))
                {
                    cellCode = "<div style='padding: 3px; font-weight: bold; text-align:center; background-color:"
                            + model.getTaskTypeColor() + ";'>" + model.getTaskTypeName() + "</div>";
                }
                return cellCode;
            }
        });

        List<ColumnConfig> cc = new ArrayList<ColumnConfig>();
        cc.add(nameCC);
        cc.add(taskTypeCC);

        ColumnModel cm = new ColumnModel(cc);
        return cm;
    }

}
