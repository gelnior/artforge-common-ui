package fr.hd3d.common.ui.client.widget.sheeteditor.view.widget.sheetitemstreegrid;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.Listener;

import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.SheetEditorModel;

public class SheetItemsTreeGridListener implements Listener<GridEvent<ItemModelData>>
{
    public void handleEvent(GridEvent<ItemModelData> tge)
    {
        EventType eventType = tge.getType();
        if(eventType == Events.BeforeEdit)
        {
            if ( tge.getColIndex() > 0 )
            {
                Object selection = tge.getGrid().getSelectionModel().getSelectedItem();
                if( selection instanceof ItemModelData)
                {
                    ItemModelData item = (ItemModelData) selection;
                    SheetEditorModel.updateEditorList(item.getType());
                    SheetEditorModel.updateRendererList(item.getType());
                }
                else
                {
                    tge.setCancelled(true);
                }
            }
        }
    }

}
