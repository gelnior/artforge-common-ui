package fr.hd3d.common.ui.client.widget.sheeteditor.controller.command;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.widget.sheeteditor.model.SheetEditorModel;
import fr.hd3d.common.ui.client.widget.sheeteditor.view.ISheetEditorView;


/**
 * When attribute editor is hidden, properties displayed on the left are reloaded (easy way to ensure that modification
 * done in the attribute editor are well displayed).
 * 
 * @author HD3D
 */
public class AttributeEditorHiddenSheetCmd extends SheetEditorCommand
{
    /**
     * Constructor.
     * 
     * @param model
     *            Model (data handler) for sheet editor.
     * @param view
     *            View (widget handler) for sheet editor.
     */
    public AttributeEditorHiddenSheetCmd(SheetEditorModel model, ISheetEditorView view)
    {
        super(model, view);
    }

    /**
     * When attribute editor is hidden, properties displayed on the left are reloaded (easy way to ensure that
     * modification done in the attribute editor are well displayed).
     * 
     * @param event
     *            Entity type Changed event.
     */
    public void execute(AppEvent event)
    {
        this.view.maskEntityPropertiesTree(true);
        this.model.reloadEntityProperties();
    }

}
