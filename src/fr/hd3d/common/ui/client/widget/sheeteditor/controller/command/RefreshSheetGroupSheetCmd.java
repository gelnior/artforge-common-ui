package fr.hd3d.common.ui.client.widget.sheeteditor.controller.command;

import java.util.List;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.modeldata.technical.ItemGroupModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.SheetEditorModel;
import fr.hd3d.common.ui.client.widget.sheeteditor.view.ISheetEditorView;


public class RefreshSheetGroupSheetCmd extends SheetEditorCommand
{

    /**
     * Constructor.
     * 
     * @param model
     *            Model (data handler) for sheet editor.
     * @param view
     *            View (widget handler) for sheet editor.
     */
    public RefreshSheetGroupSheetCmd(SheetEditorModel model, ISheetEditorView view)
    {
        super(model, view);
    }

    public void execute(AppEvent event)
    {
        SheetModelData sheet = this.model.getSheet();
        List<ItemGroupModelData> groups = this.model.getSheetItemGroups();

        List<Long> ids = CollectionUtils.getIds(groups);
        sheet.setItemGroupIds(ids);
    }

}
