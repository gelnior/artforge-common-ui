package fr.hd3d.common.ui.client.widget.sheeteditor.model.reader;

import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata.EntityPropertyModelData;


public class EntityPropertyReaderSingleton
{
    /** The record reader instance to return. */
    private static IReader<EntityPropertyModelData> reader;

    /**
     * @return The record reader instance. If it is null, it returns a new real record reader.
     */
    public final static IReader<EntityPropertyModelData> getInstance()
    {
        if (reader == null)
        {
            reader = new EntityPropertyReader();
        }
        return reader;
    }

    /**
     * Sets the record instance. This method is useful for unit tests.
     * 
     * @param mock
     *            The request handler to set.
     */
    public final static void setInstanceAsMock(IReader<EntityPropertyModelData> mock)
    {
        reader = mock;
    }
}
