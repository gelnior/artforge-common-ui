package fr.hd3d.common.ui.client.widget.sheeteditor.model.callback;

import java.util.List;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.data.ListLoadResult;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.widget.sheeteditor.event.SheetEditorEvents;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata.EntityPropertyModelData;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.reader.EntityPropertyReaderSingleton;


/**
 * Callback used to handle response to request to retrieve properties available for a given entity. It forwards the
 * ENTITY_PROPERTY_LOADED event with attached properties.
 * 
 * @author HD3D
 */
public class LoadEntityPropertiesCallback extends BaseCallback
{
    @Override
    protected void onSuccess(Request request, Response response)
    {
        try
        {
            String json = response.getEntity().getText();

            IReader<EntityPropertyModelData> reader = EntityPropertyReaderSingleton.getInstance();
            ListLoadResult<EntityPropertyModelData> result = reader.read(json);
            List<EntityPropertyModelData> properties = result.getData();

            AppEvent event = new AppEvent(SheetEditorEvents.ENTITY_PROPERTY_LOADED, properties);
            EventDispatcher.forwardEvent(event);
        }
        catch (Exception e)
        {
            Logger.log("Error occurs while parsing entities data.", e);
        }
    }
}
