package fr.hd3d.common.ui.client.widget.sheeteditor.view;

import java.util.List;

import com.extjs.gxt.ui.client.data.BaseModelData;
import com.extjs.gxt.ui.client.store.Record;

import fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata.EntityModelData;


public interface ISheetEditorView
{

    public String getSheetNameValue();

    public EntityModelData getEntityType();

    public List<BaseModelData> getSelectedSheetItems();

    public Record getSelectedItemRecord();

    public void setSheetName(String name);

    public void setEntityType(EntityModelData entityType);

    public void selectEntity(String boundClassName);

    public void selectFirstEntity();

    public void maskEntityPropertiesTree(Boolean visible);

    public void setEntityTypeComboboxEnabled(boolean enable);

    public void expandTrees();

    public void showAttributeEditor();

    public void showNoteTypeEditor(Long projectId);

    public void hideValidationButton();

    public void displayError(int intValue);

    public void showSaving();

    public void hideSaving();

    public void reset();

    public void showDialog();

    public void hideDialog();

    public void showExtendedAttributeEditor();

    public void hideSaveAsButton();

    public void showSaveAsButton();

}
