package fr.hd3d.common.ui.client.widget.sheeteditor.config;

/**
 * Constants used by sheet editor.
 * 
 * @author HD3D
 */
public class SheetEditorConfig
{

    public static final String VALUE_EVENT_VAR_NAME = "value";
    public static final String ITEM_EVENT_VAR_NAME = "item";
    public static final String FILE_EVENT_VAR_NAME = "file";

}
