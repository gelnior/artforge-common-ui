package fr.hd3d.common.ui.client.widget.sheeteditor.controller.command;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.widget.sheeteditor.model.SheetEditorModel;
import fr.hd3d.common.ui.client.widget.sheeteditor.view.ISheetEditorView;


/**
 * Cancel currently sheet edition.
 * 
 * @author HD3D
 */
public class CancelSheetCmd extends SheetEditorCommand
{

    /**
     * Constructor.
     * 
     * @param model
     *            Model (data handler) for sheet editor.
     * @param view
     *            View (widget handler) for sheet editor.
     */
    public CancelSheetCmd(SheetEditorModel model, ISheetEditorView view)
    {
        super(model, view);
    }

    /**
     * Cancel currently sheet edition.
     * 
     * @param event
     *            The cancel clicked event.
     */
    public void execute(AppEvent event)
    {
        this.model.getSheetPropertyStore().rejectChanges();
        this.view.hideDialog();
    }

}
