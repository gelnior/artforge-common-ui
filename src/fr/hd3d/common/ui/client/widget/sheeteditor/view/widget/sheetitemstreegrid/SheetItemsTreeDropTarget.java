package fr.hd3d.common.ui.client.widget.sheeteditor.view.widget.sheetitemstreegrid;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.BaseModelData;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.data.TreeModel;
import com.extjs.gxt.ui.client.dnd.DND.Operation;
import com.extjs.gxt.ui.client.dnd.Insert;
import com.extjs.gxt.ui.client.dnd.TreeGridDropTarget;
import com.extjs.gxt.ui.client.event.DNDEvent;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.treegrid.TreeGrid;
import com.extjs.gxt.ui.client.widget.treegrid.TreeGrid.TreeNode;

import fr.hd3d.common.ui.client.modeldata.technical.ItemGroupModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;
import fr.hd3d.common.ui.client.mvc.controller.CommandController;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.widget.sheeteditor.event.SheetEditorEvents;


public class SheetItemsTreeDropTarget extends TreeGridDropTarget
{
    private CommandController controller;

    public SheetItemsTreeDropTarget(@SuppressWarnings("rawtypes") TreeGrid treeGrid, CommandController controller)
    {
        super(treeGrid);
        this.controller = controller;
    }

    /**
     * When a folder or an item is dropped, all folder items or item have theirs default renderer and editor set.
     */
    @SuppressWarnings("unchecked")
    @Override
    protected void onDragDrop(DNDEvent event)
    {
        ArrayList<TreeModel> sources = (ArrayList<TreeModel>) event.getDragSource().getData();

        for (TreeModel treeModel : sources)
        {
            Object objModel = treeModel.get("model");

            if (objModel instanceof ItemModelData)
            {
                ItemModelData sourceModel = (ItemModelData) objModel;
                if (FieldUtils.isStep(sourceModel.getQuery()))
                {
                    sourceModel.setParameter(sourceModel.getParam());
                }
                sourceModel.setDefaultRendererAndEditor();
            }
            else if (objModel instanceof ItemGroupModelData)
            {
                for (ModelData child : treeModel.getChildren())
                {
                    Object item = child.get("model");
                    if (item instanceof ItemModelData)
                    {
                        ((ItemModelData) item).setDefaultRendererAndEditor();
                    }
                }

                this.controller.handleEvent(new AppEvent(SheetEditorEvents.ITEM_GROUP_DROPPED, objModel));
            }

        }

        super.onDragDrop(event);
    }

    public SheetItemsTreeDropTarget(TreeGrid<BaseModelData> tree)
    {
        super(tree);
    }

    @Override
    protected void showFeedback(DNDEvent event)
    {
        if (event.getDragSource().getComponent() == event.getDropTarget().getComponent())
        {
            this.setOperation(Operation.MOVE);
        }
        else
        {
            this.setOperation(Operation.COPY);
        }

        super.showFeedback(event);

        TreeNode overItem = this.treeGrid.findNode(event.getTarget());
        if (overItem != null)
        {
            BaseModelData tgtm = (BaseModelData) overItem.getModel();
            List<BaseModelData> data = event.getData();
            for (final BaseModelData tm : data)
            {
                BaseModelData m = tm.get("model");
                if (!canDropOn(m, tgtm))
                {
                    event.getStatus().setStatus(false);
                    return;
                }
            }
        }
        else
        {
            event.getStatus().setStatus(false);
        }
    }

    private boolean canDropOn(BaseModelData source, BaseModelData target)
    {
        if (source instanceof ItemModelData)
        {

            Insert insert = Insert.get();
            if (insert.isVisible())
            {
                if (target instanceof ItemModelData)
                {
                    return true;
                }
            }
            else
            {
                if (target instanceof ItemGroupModelData)
                    return true;
            }
        }
        else if (source instanceof ItemGroupModelData)
        {
            Insert insert = Insert.get();
            if (insert.isVisible())
            {
                List<ModelData> roots = treeGrid.getTreeStore().getRootItems();
                if (roots.indexOf(target) == -1 && target instanceof ItemGroupModelData)
                    return true;
            }
            else
            {
                if (target instanceof ItemGroupModelData == false && target instanceof ItemModelData == false)
                {
                    return true;
                }
            }
        }

        return false;
    }
}
