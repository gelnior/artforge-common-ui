package fr.hd3d.common.ui.client.widget.sheeteditor.error;

/**
 * Error raised by sheet editor.
 * 
 * @author HD3D
 */
public class SheetEditorErrors
{
    public static final int FIRST_ERROR_NUMBER = 1000;

    public static final int NO_SHEET_NAME = FIRST_ERROR_NUMBER + 1;
    public static final int SAVE_GROUP_ERROR = FIRST_ERROR_NUMBER + 2;
    public static final int SHEET_NAME_EXIST = FIRST_ERROR_NUMBER + 3;
    public static final int NO_ENTITY_SELECTED = FIRST_ERROR_NUMBER + 4;
}
