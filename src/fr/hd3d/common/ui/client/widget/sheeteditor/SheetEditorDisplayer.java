package fr.hd3d.common.ui.client.widget.sheeteditor;

import java.util.List;

import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata.EntityModelData;


/**
 * Singleton that displays sheet editor window and provides utilities to configure sheet editor. Available entities
 * should be set first via <i>createSheetEditorWindow</i> method.
 * 
 * @author HD3D
 */
public class SheetEditorDisplayer
{
    /** Window needed for sheet editing. */
    private static SheetEditorWindow sheetEditor;

    /** True if editor window is still null. */
    public static boolean isNull()
    {
        return sheetEditor == null;
    }

    /**
     * Create sheet editor for given entities.
     * 
     * @param entities
     *            available in sheet editor.
     */
    public static void createSheetEditorWindow(List<EntityModelData> entities)
    {
        sheetEditor = new SheetEditorWindow();
        sheetEditor.setEntities(entities);
    }

    /**
     * Display sheet editor.
     * 
     * @param projectId
     *            The ID of the project associated to editor.
     * @param sheet
     *            The currently edited sheet.
     * @param isNewSheet
     *            True to display sheet editor in creation mode, false for edition mode.
     */
    public static void showEditor(SheetModelData sheet, Long projectId, boolean isNewSheet)
    {
        if (isNewSheet)
        {
            showEditor(projectId);
        }
        else
        {
            sheetEditor.registerSheet(sheet);
            sheetEditor.show(projectId);
        }
    }

    /**
     * Display sheet editor for new sheet creation. No project is associated.
     */
    public static void showEditor()
    {
        sheetEditor.showForCreation(null);
    }

    /**
     * Display sheet editor for new sheet creation.
     * 
     * @param projectId
     *            The ID of the project associated to editor.
     */
    public static void showEditor(Long projectId)
    {
        sheetEditor.showForCreation(projectId);
    }

    /**
     * Display sheet editor for sheet edition. No project is associated.
     * 
     * @param sheet
     *            The sheet to edit.
     */
    public static void showEditor(SheetModelData sheet)
    {
        sheetEditor.showForEdition(sheet);
    }

    /**
     * Set entities the user can select in sheet editor.
     * 
     * @param entities
     *            Available entities.
     */
    public static void setEntities(List<EntityModelData> entities)
    {
        sheetEditor.setEntities(entities);
    }

    /**
     * Hides validation button.
     */
    public static void hideValidationButton()
    {
        sheetEditor.hideValidationButton();
    }
}
