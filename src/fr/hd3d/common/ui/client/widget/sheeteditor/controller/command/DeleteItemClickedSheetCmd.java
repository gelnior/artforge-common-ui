package fr.hd3d.common.ui.client.widget.sheeteditor.controller.command;

import java.util.List;

import com.extjs.gxt.ui.client.data.BaseModelData;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.modeldata.technical.ItemGroupModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.SheetEditorModel;
import fr.hd3d.common.ui.client.widget.sheeteditor.view.ISheetEditorView;


public class DeleteItemClickedSheetCmd extends SheetEditorCommand
{

    public DeleteItemClickedSheetCmd(SheetEditorModel model, ISheetEditorView view)
    {
        super(model, view);
    }

    public void execute(AppEvent event)
    {
        List<BaseModelData> selection = this.view.getSelectedSheetItems();
        for (BaseModelData model : selection)
        {
            if (model instanceof ItemModelData)
            {
                ItemModelData item = (ItemModelData) model;

                this.model.addDeletedItem(item);
            }
            else if (model instanceof ItemGroupModelData)
            {
                ItemGroupModelData group = (ItemGroupModelData) model;

                this.model.addDeletedGroup(group);
            }
        }
    }

}
