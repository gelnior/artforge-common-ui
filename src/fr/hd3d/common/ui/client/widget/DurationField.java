package fr.hd3d.common.ui.client.widget;

import com.extjs.gxt.ui.client.util.Util;
import com.extjs.gxt.ui.client.widget.form.TextField;

import fr.hd3d.common.ui.client.calendar.DatetimeUtil;


/**
 * Field used to change duration for model data displayed via identity sheet.
 * 
 * @author HD3D
 */
public class DurationField extends TextField<Number>
{
    /** Convert value from day unit to ms unit. */
    @Override
    public Long getValue()
    {
        if (!Util.isEmptyString(super.getRawValue()))
        {
            try
            {
                String valString = super.getRawValue();
                Double val = Double.parseDouble(valString);

                if (val != null)
                {
                    return (long) (val * DatetimeUtil.DAY_SECONDS);
                }
            }
            catch (Exception e)
            {
                return 0L;
            }
        }
        return 0L;
    }

    /**
     * Convert value from ms unit to day unit.
     * 
     * @param valNumber
     *            The number to set as field value.
     */
    @Override
    public void setValue(Number valNumber)
    {
        if (valNumber != null)
        {
            Long val = valNumber.longValue();

            if (val > 0)
            {
                super.setValue(val);
                super.setRawValue(getStringFromLong(val));
            }
            else
            {
                super.setValue(val);
            }
        }
        else
        {
            super.setValue(0L);
        }
    }

    public static String getStringFromLong(Long value)
    {
        double nbDay = (value.doubleValue() / 28800.0);

        String valueString = Double.toString(nbDay);

        if (valueString.endsWith(".0"))
            valueString = valueString.substring(0, valueString.length() - 2);

        return valueString;
    }
}
