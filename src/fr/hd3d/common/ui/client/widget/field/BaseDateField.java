package fr.hd3d.common.ui.client.widget.field;

import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.widget.DatePicker;
import com.extjs.gxt.ui.client.widget.form.DateField;


/**
 * Date field that fire a change event when a date is selected via the date picker.
 * 
 * @author HD3D
 */
public class BaseDateField extends DateField
{
    /**
     * Returns the field's date picker.
     * 
     * @return the date picker
     */
    @Override
    public DatePicker getDatePicker()
    {
        DatePicker picker = super.getDatePicker();

        picker.addListener(Events.Select, new Listener<ComponentEvent>() {
            public void handleEvent(ComponentEvent ce)
            {
                fireEvent(Events.Change);
            }
        });
        return picker;
    }
}
