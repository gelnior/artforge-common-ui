package fr.hd3d.common.ui.client.widget.field;

import com.extjs.gxt.ui.client.widget.form.PropertyEditor;
import com.extjs.gxt.ui.client.widget.form.TextArea;

import fr.hd3d.common.ui.client.widget.grid.editor.TextAreaEditor;


/**
 * HTML Text Area converts carriage return to HTML br tag after editing and vice-versa after displaying. For the rest,
 * it acts as a normal text area.
 * 
 * @author HD3D
 */
public class HtmlTextArea extends TextArea
{
    /**
     * Constructor : set property editor to convert carriage return.
     */
    public HtmlTextArea()
    {
        super();
        this.setPropertyEditor(new PropertyEditor<String>() {
            public String getStringValue(String value)
            {
                String val = value.replace(TextAreaEditor.CR_HTML, TextAreaEditor.CR_STRING);
                return val;
            }

            public String convertStringValue(String value)
            {
                return value.replace(TextAreaEditor.CR_STRING, TextAreaEditor.CR_HTML);
            }
        });
    }
}
