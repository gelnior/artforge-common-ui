package fr.hd3d.common.ui.client.widget;

import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.HRElement;
import com.google.gwt.user.client.ui.Widget;


public class HrWidget extends Widget
{
    private final HRElement hr;

    public HrWidget()
    {
        hr = Document.get().createHRElement();
        this.setElement(hr);
    }

    public HRElement getHRElement()
    {
        return this.hr;
    }
}
