package fr.hd3d.common.ui.client.widget.relationeditor;

import java.util.List;

import com.extjs.gxt.ui.client.dnd.DND.Feedback;
import com.extjs.gxt.ui.client.dnd.GridDragSource;
import com.extjs.gxt.ui.client.dnd.GridDropTarget;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.mvc.Controller;
import com.extjs.gxt.ui.client.util.Margins;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.Status;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.layout.HBoxLayout;
import com.extjs.gxt.ui.client.widget.layout.HBoxLayoutData;
import com.extjs.gxt.ui.client.widget.toolbar.ToolBar;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.listener.KeyUpListener;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.widget.NameGrid;
import fr.hd3d.common.ui.client.widget.ToolBarButton;


/**
 * Panel visible on each relation tab. It has two lists. One for the available items, one other for the items to connect
 * on selected records.
 * 
 * @author HD3D
 */
public class RelationPanel extends ContentPanel implements IRelationPanel
{
    /** Constant strings to display : dialog messages, button label... */
    public static final CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    /** Grid for list of available records for adding relation. */
    protected NameGrid dataListGrid;
    /** Grid for list of records on which relation should be added. */
    protected NameGrid saveListGrid;

    /** Tool bar for saving relation edition. */
    protected final ToolBar saveToolBar = new ToolBar();
    /** Data tool bar for available relations selection. */
    protected final ToolBar dataToolBar = new ToolBar();

    /** Text field needed for filtering relation selection. */
    protected final TextField<String> dataFilterTextField = new TextField<String>();

    /** Save relations button. */
    protected final ToolBarButton saveToolItem = new ToolBarButton(Hd3dImages.getSaveIcon(), CONSTANTS.Save(),
            RelationEvents.SAVE_CLICKED);
    /** Remove selected relations button. */
    protected final ToolBarButton removeSelectedItem = new ToolBarButton(Hd3dImages.getRemoveAllFiltersIcon(),
            "Remove selected relations", RelationEvents.REMOVE_SELECTED);
    /** Refresh relations button. */
    protected final ToolBarButton refreshToolItem = new ToolBarButton(Hd3dImages.getUndoIcon(),
            "Undo all changes since last refresh", RelationEvents.REFRESH_CLICKED);

    /** Data model. */
    protected final RelationPanelModel model;
    /** Widget controller that handles events. */
    protected final RelationPanelController controller;

    /** Displays an animated GIF showing that application is saving data. */
    private final Status savingToolItem = new Status();

    /**
     * Default constructor, set widgets and listeners.
     * 
     * @param className
     *            The class name of the possible data to set.
     */
    public RelationPanel(String className)
    {
        this.model = new RelationPanelModel(className);
        this.controller = new RelationPanelController(model, this);

        this.setStyle();
        this.setWidgets();
        this.setDragAndDrop();
        this.setToolBars();

        this.addListeners();
    }

    /**
     * @return widget controller.
     */
    public Controller getController()
    {
        return this.controller;
    }

    /**
     * Remove selected item relations
     */
    public void removeSaveListSelection()
    {
        for (RecordModelData record : saveListGrid.getSelectionModel().getSelectedItems())
            this.saveListGrid.getStore().remove(record);
    }

    /**
     * Set selected items
     * 
     * @param selection
     *            The items to set as a selection.
     */
    public void setSelection(List<Hd3dModelData> selection)
    {
        this.model.setSelection(selection);
    }

    /**
     * Mask the the relation panel controller.
     */
    public void idle()
    {
        this.controller.mask();
    }

    /**
     * Un-mask the the relation panel controller.
     */
    public void unIdle()
    {
        this.controller.unMask();
    }

    /**
     * Refresh displayed data.
     */
    public void refresh()
    {
        this.controller.handleEvent(new AppEvent(RelationEvents.REFRESH_CLICKED));
    }

    /**
     * Return the filter value given by the user.
     */
    public String getFilterValue()
    {
        return this.dataFilterTextField.getValue();
    }

    /**
     * @return Simple class name currently set.
     */
    public String getSimpleClassName()
    {
        return this.model.getClassName();
    }

    /**
     * @return The IDs of the records to set as relation.
     */
    public List<Long> getSaveIds()
    {
        return this.controller.getIdsList();
    }

    /**
     * Set tool bar for both grids : filter text field for the available records grid and save, refresh and clear
     * buttons for relations grid.
     */
    protected void setToolBars()
    {
        this.saveToolBar.add(saveToolItem);
        this.saveToolBar.add(removeSelectedItem);
        this.saveToolBar.add(refreshToolItem);
        this.saveToolBar.add(savingToolItem);
        this.savingToolItem.setBusy(CONSTANTS.WaitSaving());
        this.savingToolItem.hide();

        this.dataFilterTextField.setEmptyText(CONSTANTS.TypeRecordName());
        this.dataFilterTextField.setWidth(140);
        this.dataFilterTextField.addListener(Events.KeyUp, new KeyUpListener(RelationEvents.FILTER_CHANGED));

        this.dataToolBar.setStyleAttribute("padding", "3px");
        this.dataToolBar.add(dataFilterTextField);

    }

    /**
     * Disable save and refresh buttons.
     */
    public void disableButtons()
    {
        this.saveToolItem.disable();
        this.removeSelectedItem.disable();
        this.refreshToolItem.disable();
    }

    /**
     * Enable save and refresh buttons.
     */
    public void enableButtons()
    {
        this.saveToolItem.enable();
        this.removeSelectedItem.enable();
        this.refreshToolItem.enable();
    }

    /**
     * Hide saving status.
     */
    public void hideSaving()
    {
        this.savingToolItem.hide();
    }

    /**
     * Show saving status.
     */
    public void showSaving()
    {
        this.savingToolItem.show();
    }

    /**
     * Set panel style, no border, row layout...
     */
    protected void setStyle()
    {
        this.setFrame(false);
        this.setBorders(false);
        this.setBodyBorder(false);
        this.setHeaderVisible(false);
        // this.setLayout(new RowLayout(Orientation.HORIZONTAL));
        this.setLayout(new HBoxLayout());
        this.setSize(300, 357);
    }

    /**
     * Sets two grids, one for available records and one for record to connect.
     */
    protected void setWidgets()
    {
        ContentPanel dataListPanel = new ContentPanel();
        ContentPanel saveListPanel = new ContentPanel();

        this.dataListGrid = new NameGrid(this.model.getDataListStore());
        this.saveListGrid = new NameGrid(this.model.getSaveListStore());

        this.dataListGrid.setBorders(false);
        this.saveListGrid.setBorders(false);

        dataListPanel.setHeading(CONSTANTS.AvailableRelations());
        dataListPanel.setLayout(new FitLayout());
        dataListPanel.add(dataListGrid);
        dataListPanel.setTopComponent(dataToolBar);
        this.dataListGrid.setAutoWidth(false);
        this.dataListGrid.setWidth(220);
        this.dataListGrid.setHeight(304);

        saveListPanel.setHeading(CONSTANTS.RelationsToSet());
        saveListPanel.setLayout(new FitLayout());
        this.saveListGrid.setWidth(224);
        this.saveListGrid.setHeight(304);
        saveListPanel.add(this.saveListGrid);
        saveListPanel.setTopComponent(saveToolBar);

        this.add(dataListPanel, new HBoxLayoutData(new Margins(0, 0, 0, 0)));// , data);
        this.add(saveListPanel, new HBoxLayoutData(new Margins(0, 0, 0, 0)));// , data);
        this.layout();
    }

    /**
     * Allow drag and drop between available records grid and record to connect grid.
     */
    protected void setDragAndDrop()
    {
        new RelationSaveGridDragSource(dataListGrid);
        new GridDragSource(saveListGrid);

        GridDropTarget target = new RelationDataGridDropTarget(dataListGrid);
        target.setFeedback(Feedback.APPEND);
        target.setAllowSelfAsSource(false);

        GridDropTarget target2 = new GridDropTarget(saveListGrid);
        target2.setFeedback(Feedback.APPEND);
        target2.setAllowSelfAsSource(false);
    }

    /**
     * Add listeners to the grids : when double click occurs on a row, the row is removed from the grid and added to the
     * other grid.
     */
    protected void addListeners()
    {
        RelationGridDoubleClick listener = new RelationGridDoubleClick(this.dataListGrid, this.saveListGrid);

        this.dataListGrid.addListener(Events.CellDoubleClick, listener);
        this.saveListGrid.addListener(Events.CellDoubleClick, listener);
    }

}
