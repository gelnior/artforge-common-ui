package fr.hd3d.common.ui.client.widget.relationeditor;

import com.extjs.gxt.ui.client.dnd.GridDragSource;
import com.extjs.gxt.ui.client.event.DNDEvent;
import com.extjs.gxt.ui.client.widget.grid.Grid;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;


public class RelationSaveGridDragSource extends GridDragSource
{
    public RelationSaveGridDragSource(Grid<RecordModelData> grid)
    {
        super(grid);
    }

    /**
     * Called when the user releases the mouse over the target component.
     * 
     * @param e
     *            the dd event
     */
    @Override
    protected void onDragDrop(DNDEvent e)
    {}
}
