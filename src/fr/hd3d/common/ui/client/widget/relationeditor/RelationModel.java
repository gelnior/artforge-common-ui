package fr.hd3d.common.ui.client.widget.relationeditor;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.service.BulkRequests;
import fr.hd3d.common.ui.client.service.ServicesClass;


/**
 * Model that handles Relation Dialog data. I.e. ListStores for relation selections and list of selected record for
 * which relations should be updated.
 * 
 * @author HD3D
 */
public class RelationModel
{
    /** Selection Model Type. */
    private ModelType modelType;
    /** Bound class name of selection. */
    private String boundClassName;

    /** List of selected records to update. */
    private List<Hd3dModelData> selectedModels = new ArrayList<Hd3dModelData>();

    /** Bound Class Names associated to tabs. */
    private final List<String> relationTypes = new ArrayList<String>();

    /** ... */
    protected List<Hd3dModelData> presetRecords = new ArrayList<Hd3dModelData>();

    /** List fields specifically excluded for this relation editor instance. */
    protected List<String> specificallyExcludedFields = new ArrayList<String>();

    /**
     * @return Selection Model Type.
     */
    public ModelType getModelType()
    {
        return modelType;
    }

    /**
     * Sets the selection Model Type.
     * 
     * @param modelType
     *            The model Type to set.
     */
    public void setModelType(ModelType modelType)
    {
        this.modelType = modelType;
    }

    /**
     * @return The selection bound class name.
     */
    public String getBoundClassName()
    {
        return boundClassName;
    }

    public List<Hd3dModelData> getPresetRecords()
    {
        return this.presetRecords;
    }

    /**
     * Sets the selection bound class name.
     * 
     * @param boundClassName
     *            The bound class name to set.
     * 
     */
    public void setBoundClassName(String boundClassName)
    {
        this.boundClassName = boundClassName;

        for (Hd3dModelData model : this.selectedModels)
        {
            model.setClassName(ServicesClass.getClass(boundClassName));
        }
    }

    /** List of selected records for relations update. */
    public List<Hd3dModelData> getSelection()
    {
        return this.selectedModels;
    }

    /**
     * Sets selected records for relations update.
     * 
     * @param selectedModels
     *            The selected models to set.
     */
    public void setSelection(List<Hd3dModelData> selectedModels)
    {
        this.selectedModels = selectedModels;

        for (Hd3dModelData model : this.selectedModels)
        {
            model.setClassName(ServicesClass.getClass(boundClassName));
        }
    }

    /**
     * @param i
     *            The index of the relation type to get.
     * @return Relation type of index i.
     */
    public String getRelationType(int i)
    {
        return this.relationTypes.get(i);
    }

    /**
     * For each data store corresponds a bound class name. These relations are stored in a list called relation type.
     * This method adds a bound class name to the relation types list.
     * 
     * @param boundClassName
     *            The bound class name to add.
     */
    public void addRelationType(String boundClassName)
    {
        this.relationTypes.add(boundClassName);
    }

    /**
     * Clears the relation type list.
     */
    public void clearRelationTypes()
    {
        this.relationTypes.clear();
    }

    public void saveAll(List<Hd3dModelData> recordsToSave)
    {
        BulkRequests.bulkPut(recordsToSave, RelationEvents.SAVE_ALL_SUCCESS);
    }

    public List<String> getRelationTypes()
    {
        return this.relationTypes;
    }

    public void addSpecificallyExlcudedField(String name)
    {
        if (name != null)
        {
            this.specificallyExcludedFields.add(name.toLowerCase());
        }
    }

    public boolean isSpecifciallyExcluded(String name)
    {
        return specificallyExcludedFields.contains(name);
    }
}
