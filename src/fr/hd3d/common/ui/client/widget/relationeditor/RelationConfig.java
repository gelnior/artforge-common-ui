package fr.hd3d.common.ui.client.widget.relationeditor;

public class RelationConfig
{
    /** Relation tab event variable name for event dispatcher. */
    public static final String RELATION_TAB_EVENT_VAR_NAME = "relation_tab_event";

    /** CSS class for styling relation dialog status. */
    public static final String RELATION_STATUS_STYLE = "relation-dialog-status";
    /** CSS class for styling relation dialog button bar. */
    public static final String RELATION_BUTTON_BAR_STYLE = "relation-dialog-button-bar";
    public static final String RELATION_PRESET_EVENT_VAR_NAME = "preset-records";

}
