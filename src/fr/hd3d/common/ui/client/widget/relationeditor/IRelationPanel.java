package fr.hd3d.common.ui.client.widget.relationeditor;

import fr.hd3d.common.ui.client.widget.tabdialog.IMaskableView;


public interface IRelationPanel extends IMaskableView
{

    public String getFilterValue();

    public void showSaving();

    public void disableButtons();

    public void hideSaving();

    public void enableButtons();

    public void removeSaveListSelection();
}
