package fr.hd3d.common.ui.client.widget.relationeditor;

import com.extjs.gxt.ui.client.dnd.GridDropTarget;
import com.extjs.gxt.ui.client.event.DNDEvent;
import com.extjs.gxt.ui.client.widget.grid.Grid;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;


public class RelationDataGridDropTarget extends GridDropTarget
{

    public RelationDataGridDropTarget(Grid<RecordModelData> grid)
    {
        super(grid);
    }

    /**
     * Called when the user releases the mouse over the target component.
     * 
     * @param event
     *            the dd event
     */
    @Override
    protected void onDragDrop(DNDEvent event)
    {}
}
