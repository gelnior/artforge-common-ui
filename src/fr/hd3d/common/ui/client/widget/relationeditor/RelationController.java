package fr.hd3d.common.ui.client.widget.relationeditor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.restlet.client.data.Method;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.mvc.Controller;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.ExcludedField;
import fr.hd3d.common.ui.client.service.ServicesModelType;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.InConstraint;
import fr.hd3d.common.ui.client.service.parameter.ParameterBuilder;


/**
 * Controller that handles event for the relation dialog box.
 * 
 * @author HD3D
 */
public class RelationController extends MaskableController
{
    /** Relation dialog widget */
    final private IRelationDialog view;
    /** Model that handles relation dialog data : items linkable with current object selected in explorer. */
    final private RelationModel model;

    /**
     * Default constructor.
     * 
     * @param model
     * @param view
     */
    public RelationController(RelationModel model, IRelationDialog view)
    {
        this.model = model;
        this.view = view;

        this.registerEvents();
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();
        if (this.isMasked)
        {
            ;
        }
        else if (type == RelationEvents.DIALOG_CLOSED)
        {
            this.view.hide();
            this.mask();
        }
        else if (type == RelationEvents.SAVE_CLICKED)
        {
            this.view.disableTabHeaders();
            this.forwardToChild(event);
        }
        else if (type == RelationEvents.SAVE_SUCCESS)
        {
            this.view.enableTabHeaders();
            this.forwardToChild(event);
        }
        else if (type == RelationEvents.SAVE_ALL_CLICKED)
        {
            this.onSaveAllClicked();
        }
        else if (type == RelationEvents.SAVE_ALL_SUCCESS)
        {
            this.onSaveAllSuccess();
        }
        else if (type == RelationEvents.REFRESH_ALL_CLICKED)
        {
            this.setPresetRecords();
        }
        else if (type == RelationEvents.PRESET_RETRIEVED)
        {
            for (Controller childController : this.children)
            {
                childController.handleEvent(event);
            }
        }
        else if (type == CommonEvents.ERROR)
        {
            this.onError();
            this.forwardToChild(event);
        }
        else
        {
            this.forwardToChild(event);
        }
    }

    /**
     * When save all succeeds all buttons and tabs are re-enabled. Saving status is hidden.
     */
    private void onSaveAllSuccess()
    {
        this.view.enableButtons();
        this.view.enableTabHeaders();
        this.view.hideSaving();
    }

    /**
     * When save all button is clicked, all relation tabs are saved in one request. Saving status is shown.
     */
    private void onSaveAllClicked()
    {
        Map<String, List<Long>> idsMap = this.view.getPanelIdsMap();
        List<Hd3dModelData> recordsToSave = new ArrayList<Hd3dModelData>();

        this.view.showSaving();
        this.view.disableButtons();
        this.view.disableTabHeaders();

        for (Hd3dModelData record : this.model.getSelection())
        {
            Hd3dModelData recordToSave = record.copy();

            recordToSave.setSimpleClassName(record.getSimpleClassName());
            recordToSave.setClassName(record.getClassName());
            for (Map.Entry<String, List<Long>> entry : idsMap.entrySet())
            {
                String fieldName = entry.getKey().substring(0, 1).toLowerCase();
                fieldName += entry.getKey().substring(1) + "IDs";
                List<Long> ids = entry.getValue();

                recordToSave.set(fieldName, ids);
            }

            recordsToSave.add(recordToSave);
        }

        if (recordsToSave.size() > 0)
        {
            this.model.saveAll(recordsToSave);
        }
        else
        {
            this.onSaveAllSuccess();
        }
    }

    /**
     * When error happened, if loading is shown, it enables button from button and hides loading message.
     */
    private void onError()
    {
        this.view.hideSaving();
        this.view.enableButtons();
        this.view.enableTabHeaders();
    }

    /**
     * When shown, for each outer join found in the model type (----IDs fields) it set
     * 
     * @param selection
     *            The records on which relations will be set.
     * @param boundClassName
     *            The class name of the selected records.
     */
    public void show(List<Hd3dModelData> selection, String boundClassName)
    {
        this.unMask();

        this.model.clearRelationTypes();
        this.model.setSelection(selection);

        if (!boundClassName.equals(this.model.getBoundClassName()))
        {
            this.model.setBoundClassName(boundClassName);
            this.model.setModelType(ServicesModelType.getModelType(boundClassName));

            this.view.resetTabs();
            if (this.children != null)
            {
                this.children.clear();
            }
            this.createTabs();
        }

        this.setPresetRecords();
        this.view.show();
    }

    /**
     * Set preset records (records needed to pre-build the "relation to save" list).
     */
    private void setPresetRecords()
    {
        List<Long> ids = new ArrayList<Long>();
        for (Hd3dModelData model : this.model.getSelection())
        {
            ids.add(model.getId());
        }

        this.model.getPresetRecords().clear();

        Constraint constraint = new InConstraint(RecordModelData.ID_FIELD, ids);
        String path = ServicesPath.getPath(this.model.getBoundClassName());
        path += ParameterBuilder.parameterToString(constraint);
        IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
        BaseCallback callback = new RetrievePresetDataCallback(this.model.getPresetRecords(), this.model.getModelType());
        requestHandler.handleRequest(Method.GET, path, null, callback);
    }

    /**
     * Analyze the model type of the selected record. For each fields which ends by IDs, it creates a new relation tab.
     */
    private void createTabs()
    {
        for (int i = 0; i < this.model.getModelType().getFieldCount(); i++)
        {
            DataField field = this.model.getModelType().getField(i);
            String lowerCaseName = field.getName().toLowerCase();
            boolean isTabNeeded = field.getType() == List.class;
            isTabNeeded &= field.getName().endsWith("IDs") || field.getName().endsWith("Ids");
            isTabNeeded &= !ExcludedField.isExcluded(lowerCaseName);
            isTabNeeded &= !this.model.isSpecifciallyExcluded(lowerCaseName);
            // isTabNeeded &= PermissionUtil.hasUpdateRights(field.getFormat());

            if (isTabNeeded)
            {
                this.model.addRelationType(field.getFormat());
                this.view.addTab(field.getFormat());
            }
        }

        if (this.view.getTabCount() < 2)
        {
            this.view.hideSaveAll();
        }
        else
        {
            this.view.showSaveAll();
        }
    }

    /**
     * Register events that controller can handle.
     */
    private void registerEvents()
    {
        this.registerEventTypes(RelationEvents.SAVE_CLICKED);
        this.registerEventTypes(RelationEvents.SAVE_SUCCESS);
        this.registerEventTypes(RelationEvents.SAVE_ALL_CLICKED);
        this.registerEventTypes(RelationEvents.SAVE_ALL_SUCCESS);
        this.registerEventTypes(RelationEvents.REFRESH_ALL_CLICKED);
        this.registerEventTypes(RelationEvents.DIALOG_CLOSED);
        this.registerEventTypes(CommonEvents.ERROR);
    }
}
