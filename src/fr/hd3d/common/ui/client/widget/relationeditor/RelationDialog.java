package fr.hd3d.common.ui.client.widget.relationeditor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.widget.Status;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.toolbar.FillToolItem;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.listener.ButtonClickListener;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.service.ServicesField;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.widget.tabdialog.TabsDialog;


/**
 * Dialog box used to set relations between computer, software and devices. It shows one list for each type of items on
 * which you can add a relation with the item selected from the explorer.
 * 
 * @author HD3D
 */
public class RelationDialog extends TabsDialog implements IRelationDialog
{
    /** Constant strings to display : dialog messages, button label... */
    public static final CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    /** Model that handles grid data. */
    private final RelationModel model;
    /** Controller that handles relation dialog events. */
    private final RelationController controller;

    /** Relation grid lists. */
    private final List<RelationPanel> relationPanels = new ArrayList<RelationPanel>();

    /** Status label to show loading informations */
    protected Status status;

    /** Button that hides the dialog. */
    protected Button closeButton = new Button(CONSTANTS.Close());
    /** Button to save all relation panel data. */
    protected Button saveAllButton = new Button(CONSTANTS.SaveAll());
    /** Refresh all relation panel data. */
    protected Button refreshAllButton = new Button("Refresh all");

    /**
     * Default constructor.
     */
    public RelationDialog()
    {
        super();
        model = new RelationModel();
        controller = new RelationController(model, this);

        EventDispatcher.get().addController(controller);

        this.setStyle();
        this.setStatus();
        this.setButtons();
    }

    /**
     * Shows the relation dialog box.
     * 
     * @param selection
     *            Selected item from explorer.
     */
    public void show(List<Hd3dModelData> selection)
    {
        controller.show(selection, selection.get(0).getSimpleClassName());

        for (RelationPanel panel : relationPanels)
        {
            panel.setSelection(selection);
            panel.refresh();
        }
    }

    public void show(final Hd3dModelData model)
    {
        // model.refresh(new GetModelDataCallback(model) {
        // @Override
        // protected void onRecordReturned(List<Hd3dModelData> records)
        // {
        // Hd3dModelData newRecord = records.get(0);
        // this.record.updateFromModelData(newRecord);
        show(CollectionUtils.asList(model));
        // }
        // });
    }

    @Override
    public void show()
    {
        super.show();
    }

    /**
     * Adds a tabulation and two selection grids for the specified class to the tab folder.
     * 
     * @param className
     *            The class name
     */
    public int addTab(String className)
    {
        RelationPanel relationPanel = new RelationPanel(className);

        this.addTab(ServicesField.getHumanName(className + "s"), relationPanel, relationPanel);
        this.relationPanels.add(relationPanel);
        this.controller.addChild(relationPanel.getController());

        return folder.getItemCount() - 1;
    }

    /**
     * Clears all tab and lists created.
     * 
     * @see fr.hd3d.common.ui.client.widget.relationeditor.IRelationDialog#resetTabs()
     */
    public void resetTabs()
    {
        this.folder.removeAll();
        this.views.clear();
        this.lastSelected = null;

        this.relationPanels.clear();
        this.layout();
    }

    /**
     * Show saving label tool item.
     */
    public void showSaving()
    {
        this.status.show();
    }

    /**
     * Hide saving label tool item.
     */
    public void hideSaving()
    {
        this.status.hide();
    }

    /**
     * Disable dialog buttons : save all and close.
     */
    public void disableButtons()
    {
        this.closeButton.disable();
        this.saveAllButton.disable();
    }

    /**
     * Enable dialog buttons : save all and close.
     */
    public void enableButtons()
    {
        this.closeButton.enable();
        this.saveAllButton.enable();
    }

    /**
     * Get Map associating a simple class name with an id list of relations to link with the selection.
     */
    public Map<String, List<Long>> getPanelIdsMap()
    {
        Map<String, List<Long>> map = new HashMap<String, List<Long>>();

        for (RelationPanel panel : relationPanels)
        {
            map.put(panel.getSimpleClassName(), panel.getSaveIds());
        }

        return map;
    }

    /**
     * Sets dialog layout and styles.
     */
    private void setStyle()
    {
        this.setLayout(new FitLayout());
        this.setClosable(false);
        this.setModal(true);
        this.setResizable(false);
        this.setBodyBorder(false);

        this.setHeading(CONSTANTS.RelationTool());
        this.setWidth(460);
        this.setHeight(450);

        this.folder.setWidth(450);
        this.folder.setAutoHeight(true);
        this.add(folder);
    }

    /**
     * Sets status label to show when dialog box is loading.
     */
    private void setStatus()
    {
        this.status = new Status();
        this.status.setBusy(CONSTANTS.WaitSaving());
        this.status.hide();

        this.getButtonBar().add(status);
        this.getButtonBar().add(new FillToolItem());
    }

    /**
     * Sets the save button at the bottom of the dialog.
     */
    private void setButtons()
    {
        this.setButtons("");
        this.getButtonBar().addStyleName(RelationConfig.RELATION_BUTTON_BAR_STYLE);
        this.setButtonAlign(HorizontalAlignment.LEFT);

        this.saveAllButton.addSelectionListener(new ButtonClickListener(RelationEvents.SAVE_ALL_CLICKED));
        this.closeButton.addSelectionListener(new ButtonClickListener(RelationEvents.DIALOG_CLOSED));
        this.refreshAllButton.addSelectionListener(new ButtonClickListener(RelationEvents.REFRESH_ALL_CLICKED));
        this.refreshAllButton.setIcon(Hd3dImages.getRefreshIcon());

        this.getButtonBar().add(new FillToolItem());
        this.getButtonBar().add(this.refreshAllButton);
        this.getButtonBar().add(this.saveAllButton);
        this.getButtonBar().add(this.closeButton);
    }

    public int getTabCount()
    {
        return this.folder.getItemCount();
    }

    public void hideSaveAll()
    {
        this.saveAllButton.hide();
    }

    public void showSaveAll()
    {
        this.saveAllButton.show();
    }

    public void addSpecificallyExlcudedField(String name)
    {
        this.model.addSpecificallyExlcudedField(name);
    }
}
