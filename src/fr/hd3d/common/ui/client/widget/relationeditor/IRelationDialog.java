package fr.hd3d.common.ui.client.widget.relationeditor;

import java.util.List;
import java.util.Map;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;


/**
 * View used to set relations between computer, software and devices.
 * 
 * @author HD3D
 */
public interface IRelationDialog
{

    /**
     * Shows the relation dialog box.
     * 
     * @param selection
     *            the selected items.
     */
    public void show(List<Hd3dModelData> selection);

    /**
     * Adds a tabulation and a grid for the specified class.
     * 
     * @param className
     *            the name of the class concerned by the tabulation.
     */
    public int addTab(String className);

    /**
     * Clears all tab and lists created.
     */
    public void resetTabs();

    /**
     * Hide view to user.
     */
    public void hide();

    /**
     * Show view to user.
     */
    public void show();

    /**
     * Disable save button and show saving status.
     */
    public void showSaving();

    /**
     * Enable save button and hide saving status.
     */
    public void hideSaving();

    public void disableButtons();

    public void enableButtons();

    public Map<String, List<Long>> getPanelIdsMap();

    public void enableTabHeaders();

    public void disableTabHeaders();

    public void hideSaveAll();

    public int getTabCount();

    public void showSaveAll();
}
