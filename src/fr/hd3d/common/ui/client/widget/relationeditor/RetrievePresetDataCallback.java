package fr.hd3d.common.ui.client.widget.relationeditor;

import java.util.List;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.data.ListLoadResult;
import com.extjs.gxt.ui.client.data.ModelType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.modeldata.reader.RecordReaderSingleton;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;


public class RetrievePresetDataCallback extends BaseCallback
{
    protected List<Hd3dModelData> presetRecords;
    protected ModelType modelType;

    public RetrievePresetDataCallback(List<Hd3dModelData> presetRecords, ModelType modelType)
    {
        this.presetRecords = presetRecords;
        this.modelType = modelType;
    }

    @Override
    protected void onSuccess(Request request, Response response)
    {
        try
        {
            String json = response.getEntity().getText();
            IReader<RecordModelData> reader = RecordReaderSingleton.getInstance();
            reader.setModelType(modelType);

            ListLoadResult<RecordModelData> result = reader.read(json);
            List<RecordModelData> records = result.getData();
            presetRecords.addAll(records);

            AppEvent event = new AppEvent(RelationEvents.PRESET_RETRIEVED);
            event.setData(RelationConfig.RELATION_PRESET_EVENT_VAR_NAME, presetRecords);
            EventDispatcher.forwardEvent(event);
        }
        catch (Exception e)
        {
            Logger.log("Error occurs while parsing preset callback", e);
        }
    }
}
