package fr.hd3d.common.ui.client.widget.relationeditor;

import com.extjs.gxt.ui.client.event.EventType;


public class RelationEvents
{
    public static final EventType DIALOG_SHOW = new EventType();
    public static final EventType RELATIONS_RETRIEVED = new EventType();

    public static final EventType SAVE_CLICKED = new EventType();
    public static final EventType REMOVE_SELECTED = new EventType();
    public static final EventType REFRESH_CLICKED = new EventType();
    public static final EventType FILTER_CHANGED = new EventType();
    public static final EventType DIALOG_CLOSED = new EventType();
    public static final EventType PRESET_RETRIEVED = new EventType();
    public static final EventType SAVE_SUCCESS = new EventType();
    public static final EventType SAVE_ALL_CLICKED = new EventType();
    public static final EventType SAVE_ALL_SUCCESS = new EventType();
    public static final EventType REFRESH_ALL_CLICKED = new EventType();
}
