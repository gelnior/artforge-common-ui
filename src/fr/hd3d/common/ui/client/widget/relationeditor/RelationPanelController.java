package fr.hd3d.common.ui.client.widget.relationeditor;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.service.ServicesClass;


/**
 * Controller that handles event for the relation dialog box.
 * 
 * @author HD3D
 */
public class RelationPanelController extends MaskableController
{
    /** Relation panel widget */
    final protected IRelationPanel view;
    /** Model that handles relation dialog data : items linkable with current object selection. */
    final protected RelationPanelModel model;

    /** ... */
    protected List<Hd3dModelData> presetRecords = new ArrayList<Hd3dModelData>();

    /**
     * Default constructor.
     * 
     * @param model
     *            Model handling data.
     * @param view
     *            Relation panel widget.
     */
    public RelationPanelController(RelationPanelModel model, IRelationPanel view)
    {
        this.model = model;
        this.view = view;

        this.registerEvents();
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();
        if (type == RelationEvents.PRESET_RETRIEVED)
        {
            this.presetRecords = event.getData(RelationConfig.RELATION_PRESET_EVENT_VAR_NAME);
            this.refreshSaveList();
        }
        else if (this.isMasked)
        {
            ;
        }
        else if (type == RelationEvents.FILTER_CHANGED)
        {
            this.onRelationFilterChanged();
        }
        else if (type == RelationEvents.SAVE_CLICKED)
        {
            this.onSave();
        }
        else if (type == RelationEvents.SAVE_SUCCESS)
        {
            this.view.hideSaving();
            this.view.enableButtons();
        }
        else if (type == RelationEvents.REMOVE_SELECTED)
        {
            this.onRemoveSelected();
        }
        else if (type == RelationEvents.REFRESH_CLICKED)
        {
            this.onRefreshClicked();
        }
        else if (type == CommonEvents.ERROR)
        {
            this.view.hideSaving();
            this.view.enableButtons();
        }
        else
        {
            this.forwardToChild(event);
        }
    }

    /**
     * When relation filter changes, it updates the available relations filter.
     */
    protected void onRelationFilterChanged()
    {
        String filterValue = this.view.getFilterValue();
        if (filterValue != null && filterValue.length() > 0)
        {
            this.model.setFilter();
            this.model.getFilter().setLeftMember(this.view.getFilterValue() + "%");
        }
        else
        {
            this.model.removeFilter();
        }
        this.model.reloadDataList();
    }

    /**
     * When refresh is clicked, it refreshes the record to save list with the already set relations list.
     */
    protected void onRefreshClicked()
    {
        this.model.getSaveListStore().removeAll();
        this.refreshSaveList();
    }

    /**
     * Remove selected records from save list.
     */
    private void onRemoveSelected()
    {
        this.view.removeSaveListSelection();
    }

    /**
     * Set save list with all common relations set in preset record list.
     */
    protected void refreshSaveList()
    {
        String fieldName = this.model.getClassName().substring(0, 1).toLowerCase();
        fieldName += this.model.getClassName().substring(1);

        String idField = fieldName + "IDs";
        String idField2 = fieldName + "Ids";
        String nameField = fieldName + "Names";
        List<RecordModelData> records = new ArrayList<RecordModelData>();

        if (presetRecords != null && presetRecords.size() > 0)
        {
            Hd3dModelData model = presetRecords.get(0);
            List<Long> recordIds = model.get(idField);
            if (recordIds == null)
            {
                recordIds = model.get(idField2);
            }

            if (recordIds != null)
            {
                recordIds = new ArrayList<Long>(recordIds);

                List<String> names = model.get(nameField);

                int j = 1;
                while (j < presetRecords.size() && recordIds.size() > 0)
                {
                    Hd3dModelData presetRecord = presetRecords.get(j);
                    List<Long> ids = presetRecord.get(idField);

                    List<Long> tmpIds = new ArrayList<Long>();
                    for (Long id : recordIds)
                    {
                        if (!ids.contains(id))
                        {
                            tmpIds.add(id);
                        }
                    }
                    recordIds.removeAll(tmpIds);
                    j++;
                }

                for (int i = 0; i < recordIds.size(); i++)
                {
                    RecordModelData record = new RecordModelData(recordIds.get(i), names.get(i));
                    records.add(record);
                }
            }
        }

        this.model.getSaveListStore().removeAll();
        this.model.getSaveListStore().add(records);
        this.model.getSaveListStore().sort(RecordModelData.NAME_FIELD, SortDir.ASC);
    }

    /**
     * When save is clicked, all relations are saved to database.
     */
    protected void onSave()
    {
        List<Long> ids = this.getIdsList();
        List<Hd3dModelData> recordsToSave = new ArrayList<Hd3dModelData>();

        this.view.showSaving();
        this.view.disableButtons();

        for (Hd3dModelData record : this.model.getSelection())
        {
            Hd3dModelData recordToSave = record.copy();
            String fieldName = this.model.getClassName().substring(0, 1).toLowerCase();
            fieldName += this.model.getClassName().substring(1) + "IDs";
            recordToSave.set(fieldName, ids);
            recordToSave.setSimpleClassName(record.getSimpleClassName());
            if (record.getClassName() != null)
            {
                recordToSave.setClassName(record.getClassName());
            }
            else
            {
                recordToSave.setClassName(ServicesClass.getClass(record.getSimpleClassName()));
            }

            recordsToSave.add(recordToSave);
        }

        this.model.saveAll(recordsToSave);
    }

    /**
     * @return The id of records to set as relations on each selected records in explorer.
     */
    protected List<Long> getIdsList()
    {
        List<Long> ids = new ArrayList<Long>();

        for (RecordModelData record : this.model.saveList.getModels())
        {
            ids.add(record.getId());
        }
        return ids;
    }

    /**
     * Register events that controller can handle.
     */
    protected void registerEvents()
    {
        this.registerEventTypes(RelationEvents.SAVE_SUCCESS);
        this.registerEventTypes(RelationEvents.PRESET_RETRIEVED);
        this.registerEventTypes(RelationEvents.RELATIONS_RETRIEVED);
        this.registerEventTypes(RelationEvents.RELATIONS_RETRIEVED);
        this.registerEventTypes(RelationEvents.REMOVE_SELECTED);
        this.registerEventTypes(RelationEvents.REFRESH_CLICKED);
        this.registerEventTypes(RelationEvents.SAVE_CLICKED);
        this.registerEventTypes(RelationEvents.FILTER_CHANGED);
        this.registerEventTypes(CommonEvents.ERROR);
    }
}
