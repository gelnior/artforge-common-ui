package fr.hd3d.common.ui.client.widget.relationeditor;

import java.util.List;

import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.Listener;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.widget.NameGrid;


/**
 * Occur on double click on relation grids. If one row is double clicked in a list of available records, it is removed
 * and added to the row to connect list, and vice-versa.
 * 
 * @author HD3D
 */
public class RelationGridDoubleClick implements Listener<GridEvent<RecordModelData>>
{
    /** Grid for list of available records for adding relation. */
    private final NameGrid dataListGrid;
    /** Grid for list of records on which relation should be added. */
    private final NameGrid saveListGrid;

    /**
     * Default constructor.
     * 
     * @param dataListGrid
     *            Grid for list of available records for adding relation.
     * @param saveListGrid
     *            Grid for list of records on which relation should be added.
     */
    public RelationGridDoubleClick(NameGrid dataListGrid, NameGrid saveListGrid)
    {
        this.dataListGrid = dataListGrid;
        this.saveListGrid = saveListGrid;
    }

    public void handleEvent(GridEvent<RecordModelData> be)
    {
        if (be.getSource() == dataListGrid)
        {
            List<RecordModelData> records = dataListGrid.getSelectionModel().getSelectedItems();

            for (RecordModelData record : records)
            {
                if (saveListGrid.getStore().findModel(RecordModelData.ID_FIELD, record.getId()) == null)
                {
                    saveListGrid.getStore().add(record);
                }
            }
        }
        else
        {
            List<RecordModelData> records = saveListGrid.getSelectionModel().getSelectedItems();
            for (RecordModelData record : records)
            {
                saveListGrid.getStore().remove(record);
            }
        }
    }
}
