package fr.hd3d.common.ui.client.widget.relationeditor;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.data.BaseListLoader;
import com.extjs.gxt.ui.client.data.ListLoadResult;
import com.extjs.gxt.ui.client.store.ListStore;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.modeldata.reader.RecordReaderSingleton;
import fr.hd3d.common.ui.client.service.BulkRequests;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.OrderBy;
import fr.hd3d.common.ui.client.service.parameter.Pagination;
import fr.hd3d.common.ui.client.service.proxy.ServicesProxy;


/**
 * Handles data for the relation panel.
 * 
 * @author HD3D
 */
public class RelationPanelModel
{
    /** The simple class name of data to save as */
    protected String className;

    /** List of selected records to update. */
    private List<Hd3dModelData> selection = new ArrayList<Hd3dModelData>();

    /** The store containing available records. */
    protected ListStore<RecordModelData> dataList;
    /** The store containing relations to save. */
    protected ListStore<RecordModelData> saveList = new ListStore<RecordModelData>();

    /** Loader sets data from proxy into the store. */
    protected BaseListLoader<ListLoadResult<RecordModelData>> loader;

    /** Proxy between available relation and services, useful for filtering. */
    ServicesProxy<RecordModelData> proxy;
    /** The proxy path constraint that will filter data on the name. */
    Constraint filter = new Constraint(EConstraintOperator.like, RecordModelData.NAME_FIELD, "%", null);

    /**
     * Default constructor.
     * 
     * @param className
     *            The class name of the available data.
     */
    public RelationPanelModel(String className)
    {
        this.className = className;

        this.setProxy();
        this.setLoader();
    }

    /**
     * @return the list store of available relations name.
     */
    public ListStore<RecordModelData> getDataListStore()
    {
        return dataList;
    }

    /**
     * @return the list store of data to save.
     */
    public ListStore<RecordModelData> getSaveListStore()
    {
        return saveList;
    }

    /**
     * @return the selected models on which relations will be added.
     */
    public List<Hd3dModelData> getSelection()
    {
        return selection;
    }

    /**
     * Set model selection on which relations will be added.
     * 
     * @param selection
     *            Models selection on which relations will be added.
     */
    public void setSelection(List<Hd3dModelData> selection)
    {
        this.selection = selection;
    }

    /**
     * @return The class name of data relations to set.
     */
    public String getClassName()
    {
        return this.className;
    }

    /**
     * @return The filter set on the available relation store.
     */
    public Constraint getFilter()
    {
        return this.filter;
    }

    /**
     * Refresh data list by reloading data.
     */
    public void reloadDataList()
    {
        this.loader.load();
    }

    /**
     * Set the data loader for available relations grid.
     */
    protected void setLoader()
    {
        this.loader = new BaseListLoader<ListLoadResult<RecordModelData>>(proxy);
        this.loader.setSortDir(SortDir.ASC);
        this.loader.setSortField(RecordModelData.NAME_FIELD);

        this.dataList = new ListStore<RecordModelData>(loader);
        this.dataList.sort(RecordModelData.NAME_FIELD, SortDir.ASC);
        this.loader.load();
    }

    /**
     * Set the data proxy for available relations grid.
     */
    protected void setProxy()
    {
        Pagination pagination = new Pagination(0L, 30L);
        OrderBy orderBy = new OrderBy(RecordModelData.NAME_FIELD);

        IReader<RecordModelData> reader = RecordReaderSingleton.getInstance();
        // RecordReader reader = new RecordReader();
        this.proxy = new ServicesProxy<RecordModelData>(reader);
        this.proxy.setPath(ServicesPath.getPath(className));
        this.proxy.addParameter(orderBy);
        this.proxy.addParameter(pagination);
        this.proxy.addParameter(filter);
    }

    /**
     * Save to database all records in recordsToSave list.
     * 
     * @param recordsToSave
     *            The list of records to save.
     * 
     */
    public void saveAll(List<Hd3dModelData> recordsToSave)
    {
        BulkRequests.bulkPut(recordsToSave, RelationEvents.SAVE_SUCCESS);
    }

    /**
     * Remove data grid filter from its proxy.
     */
    public void removeFilter()
    {
        this.proxy.removeParameter(filter);
    }

    /**
     * Set data grid filter on its proxy.
     */
    public void setFilter()
    {
        if (!this.proxy.hasParameter(filter))
        {
            this.proxy.addParameter(filter);
        }
    }
}
