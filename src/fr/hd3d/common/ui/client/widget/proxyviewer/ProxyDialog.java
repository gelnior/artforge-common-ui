package fr.hd3d.common.ui.client.widget.proxyviewer;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.FieldEvent;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.KeyListener;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.TreePanelEvent;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.TreeStore;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.HorizontalPanel;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.layout.ColumnLayout;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.google.gwt.user.client.Window;

import fr.hd3d.common.ui.client.error.Hd3dException;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.asset.AssetRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.asset.FileRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.asset.ProxyModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.util.GridUtils;
import fr.hd3d.common.ui.client.widget.AutoTaskTypeComboBox;
import fr.hd3d.common.ui.client.widget.EasyMenu;
import fr.hd3d.common.ui.client.widget.basetree.BaseTreePanel;
import fr.hd3d.common.ui.client.widget.grid.BaseGrid;
import fr.hd3d.common.ui.client.widget.proxyplayer.ProxyPlayer;


class ProxyDialog extends Dialog
{
    private final ProxyDialogModel model = new ProxyDialogModel();
    @SuppressWarnings("unused")
    private final ProxyDialogController controller = new ProxyDialogController(this, model);

    private final AutoTaskTypeComboBox taskTypeCombobox = new AutoTaskTypeComboBox();
    private final TextField<String> fileNameField = new TextField<String>();

    private final BaseGrid<ProxyModelData> proxyGrid = new BaseGrid<ProxyModelData>(this.model.getProxyStore(),
            getColumnModel());

    private BaseTreePanel<AssetRevisionModelData> tree;

    private ContentPanel imgPanel = new ContentPanel();

    private ColumnModel getColumnModel()
    {
        List<ColumnConfig> columns = new ArrayList<ColumnConfig>();
        GridUtils.addColumnConfig(columns, ProxyModelData.PROXYTYPE_NAME_FIELD, ProxyModelData.PROXYTYPE_NAME_FIELD);
        GridUtils.addColumnConfig(columns, ProxyModelData.LOCATION_FIELD, ProxyModelData.LOCATION_FIELD);
        GridUtils.addColumnConfig(columns, ProxyModelData.FILENAME_FIELD, ProxyModelData.FILENAME_FIELD);

        return new ColumnModel(columns);
    }

    public ProxyDialog()
    {

        this.setStyle();
        this.setListener();

    }

    private void setStyle()
    {
        tree = new BaseTreePanel<AssetRevisionModelData>(this.model.getTreeStore()) {
            @Override
            protected String getText(AssetRevisionModelData model)
            {
                String name = "";
                if (model.getClassName().equals(AssetRevisionModelData.CLASS_NAME))
                {
                    name = model.getName();
                }
                else if (model.getClassName().equals(FileRevisionModelData.CLASS_NAME))
                {
                    name = model.get(FileRevisionModelData.KEY_FIELD);
                }

                return name;
            }

            @Override
            protected void onClick(@SuppressWarnings("rawtypes") TreePanelEvent tpe)
            {
                super.onClick(tpe);
                @SuppressWarnings("unchecked")
                TreeNode node = tpe.getNode();
                if (node == null)
                {
                    return;
                }
                AssetRevisionModelData asset = node.getModel();
                if (asset.getClassName().equals(FileRevisionModelData.CLASS_NAME))
                {
                    AppEvent event = new AppEvent(ProxyDialogEvents.FILEREVISION_SELECTED);
                    imgPanel.removeAll();
                    event.setData(asset.getId());
                    EventDispatcher.forwardEvent(event);
                }
                else
                {
                    AppEvent event = new AppEvent(ProxyDialogEvents.ASSETREVISION_SELECTED);
                    event.setData(asset);
                    EventDispatcher.forwardEvent(event);
                }
            }
        };
        tree.setDisplayProperty(FileRevisionModelData.KEY_FIELD);
        this.model.getTreeStore().addListener(TreeStore.DataChanged, new Listener<BaseEvent>() {

            public void handleEvent(BaseEvent be)
            {
                if (model.getTreeStore().getChildCount() == 0)
                {
                    tree.getElement().setInnerHTML("No Asset...");
                }
            }

        });

        this.setHeading("Show proxy");
        this.setButtons(Dialog.CLOSE);
        this.setHideOnButtonClick(true);
        this.setLayout(new FitLayout());
        this.setWidth(1200);
        this.setHeight(656);

        this.fileNameField.setEmptyText("Filter on the filename...");

        ContentPanel globalPanel = new ContentPanel();
        globalPanel.setHeaderVisible(false);
        globalPanel.setLayout(new ColumnLayout());
        globalPanel.setHeight(576);

        // BorderedPanel proxyPanel = new BorderedPanel();
        // proxyPanel.setLayout(new FitLayout());

        ContentPanel treePanel = new ContentPanel();
        treePanel.setHeading("Select a file");
        // treePanel.setLayout(new FitLayout());
        treePanel.add(tree);
        treePanel.setHeight(576);
        globalPanel.add(treePanel);

        HorizontalPanel topPanel = new HorizontalPanel();
        topPanel.setSpacing(10);
        topPanel.add(taskTypeCombobox);
        topPanel.add(fileNameField);
        globalPanel.setTopComponent(topPanel);
        // proxyPanel.setToolBar();
        // proxyPanel.addToToolBar(topPanel);
        // proxyPanel.addWest(treePanel);

        ContentPanel gridPanel = new ContentPanel();
        gridPanel.setHeading("Open a proxy");

        proxyGrid.getView().setEmptyText("No proxy ...");
        proxyGrid.setHeight(576);
        gridPanel.add(proxyGrid);

        globalPanel.add(gridPanel);
        // globalPanel.add(proxyPanel);
        // this.add(proxyPanel);

        imgPanel = new ContentPanel();
        imgPanel.setBodyBorder(false);
        imgPanel.setHeaderVisible(false);
        imgPanel.setLayoutOnChange(true);
        imgPanel.setLayout(new FitLayout());
        imgPanel.setPixelSize(720, 576);
        globalPanel.add(imgPanel);
        this.add(globalPanel);
    }

    private void setListener()
    {
        this.proxyGrid.addListener(Events.ContextMenu, new Listener<GridEvent<ProxyModelData>>() {

            private EasyMenu menu;

            public void handleEvent(GridEvent<ProxyModelData> be)
            {
                if (menu == null)
                {
                    createMenu();
                }
                proxyGrid.setContextMenu(menu);
            }

            private void createMenu()
            {
                menu = new EasyMenu();

                menu.addItem("open Proxy", ProxyDialogEvents.PROXY_DOWNLOAD_CLICKED, Hd3dImages.getMosaicIcon());
            }
        });
        this.proxyGrid.addListener(Events.RowClick, new Listener<GridEvent<ProxyModelData>>() {

            public void handleEvent(GridEvent<ProxyModelData> be)
            {
                if (be.getRowIndex() == -1)
                {
                    return;
                }
                ProxyModelData proxy = be.getModel();
                if (proxy == null)
                {
                    return;
                }
                // String path = proxy.getDownloadPath();
                // imgPanel.getElement().setInnerHTML("<center><img width='600px'  src='" + path + "'/></center>");
                imgPanel.removeAll();
                ProxyPlayer pp = new ProxyPlayer(proxy);
                pp.setPixelSize(720, 576);
                imgPanel.add(pp);
                imgPanel.layout(true);
            }

        });

        this.taskTypeCombobox.addListener(Events.Select, new Listener<FieldEvent>() {

            public void handleEvent(FieldEvent be)
            {
                try
                {
                    List<TaskTypeModelData> tasktypes = taskTypeCombobox.getSelection();
                    if (tasktypes == null || tasktypes.isEmpty())
                    {

                        throw new Hd3dException("No taskType selected!!");

                    }
                    model.setTaskType(tasktypes.get(0).getId());
                    model.reload();
                }
                catch (Hd3dException e)
                {
                    e.print();
                }
            }

        });

        this.fileNameField.addKeyListener(new KeyListener() {
            @Override
            public void componentKeyUp(ComponentEvent event)
            {
                String fileName = fileNameField.getValue();
                model.setFileName(fileName);
            }
        });
    }

    @Override
    public void show()
    {
        this.model.getTreeStore().removeAll();
        this.model.getProxyStore().removeAll();
        this.model.reload();
        this.taskTypeCombobox.reset();
        this.fileNameField.reset();
        this.imgPanel.removeAll();
        super.show();
        this.imgPanel.addText("Display proxy...");
    }

    public List<ProxyModelData> getProxySelected() throws Hd3dException
    {
        List<ProxyModelData> selection = proxyGrid.getSelectionModel().getSelection();
        if (selection == null || selection.isEmpty())
        {
            throw new Hd3dException("No data selected.");
        }
        return selection;
    }

    public void openNewWindow(String url)
    {
        Window.open(url, "Download proxy", "");
    }

    public void setData(Long projectId, List<Hd3dModelData> models, String boundEntityName) throws Hd3dException
    {
        this.model.setData(projectId, models, boundEntityName);
    }

}
