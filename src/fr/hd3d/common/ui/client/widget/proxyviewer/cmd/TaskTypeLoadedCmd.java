package fr.hd3d.common.ui.client.widget.proxyviewer.cmd;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.mvc.controller.BaseCommand;
import fr.hd3d.common.ui.client.widget.proxyviewer.ProxySimpleDialog;
import fr.hd3d.common.ui.client.widget.proxyviewer.ProxySimpleDialogModel;


public class TaskTypeLoadedCmd implements BaseCommand
{

    private final ProxySimpleDialog view;
    private final ProxySimpleDialogModel model;

    public TaskTypeLoadedCmd(ProxySimpleDialog view, ProxySimpleDialogModel model)
    {
        this.view = view;
        this.model = model;
    }

    public void execute(AppEvent event)
    {
        this.view.displayTaskType(model.getTaskTypes());
    }

}
