package fr.hd3d.common.ui.client.widget.proxyviewer;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.BaseTreeLoader;
import com.extjs.gxt.ui.client.data.DataReader;
import com.extjs.gxt.ui.client.data.TreeLoader;
import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.store.TreeStore;
import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.error.Hd3dException;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.asset.AssetRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.asset.FileRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.asset.ProxyModelData;
import fr.hd3d.common.ui.client.modeldata.reader.AssetRevisionReader;
import fr.hd3d.common.ui.client.modeldata.reader.ProxyReader;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.Constraints;
import fr.hd3d.common.ui.client.service.proxy.TreeServicesProxy;
import fr.hd3d.common.ui.client.service.store.ServiceStore;


class ProxyDialogModel
{

    private final Constraint projectConstraint = new Constraint(EConstraintOperator.eq,
            "assetRevisionGroups.project.id");
    private final Constraint taskTypeConstraint = new Constraint(EConstraintOperator.eq, "taskType.id");
    private final Constraint workObjectConstraint = new Constraint(EConstraintOperator.in, "assetRevisionGroups.value");
    private final Constraint boundEntityNameConstraint = new Constraint(EConstraintOperator.eq,
            "assetRevisionGroups.criteria");

    private final Constraint assetRevisionFileNameConstraint = new Constraint(EConstraintOperator.like,
            "fileRevisions.key");
    private final Constraint fileRevisionFileNameConstraint = new Constraint(EConstraintOperator.like, "key");

    private final Constraints assetConstraints = new Constraints();

    private final Constraint assetRevisionConstraint = new Constraint(EConstraintOperator.eq, "assetRevision");

    private final ServiceStore<ProxyModelData> proxyStore = new ServiceStore<ProxyModelData>(new ProxyReader());

    private final Constraint proxyConstraint = new Constraint(EConstraintOperator.eq, "fileRevision");

    private Long projectId;

    private String fileName = "";

    TreeServicesProxy<AssetRevisionModelData> proxy = new TreeServicesProxy<AssetRevisionModelData>(
            new AssetRevisionReader()) {
        @Override
        public void load(DataReader<List<AssetRevisionModelData>> reader, Object loadConfig,
                AsyncCallback<List<AssetRevisionModelData>> callback)
        {

            if (loadConfig == null)
            {
                this.path = ServicesPath.getPath(AssetRevisionModelData.SIMPLE_CLASS_NAME);
                this.clearParameters();
                assetConstraints.clear();
                // setAssetContraint();-
                if (!ids.isEmpty())
                {
                    workObjectConstraint.setLeftMember(ids);
                    assetConstraints.add(workObjectConstraint);
                }
                if (taskTypeId != null)
                {
                    assetConstraints.add(taskTypeConstraint);
                }
                if (projectId != null)
                {
                    projectConstraint.setLeftMember(projectId);
                    assetConstraints.add(projectConstraint);
                }
                assetRevisionFileNameConstraint.setLeftMember(fileName + "%");
                assetConstraints.add(assetRevisionFileNameConstraint);

                boundEntityNameConstraint.setLeftMember(boundEntityName);
                assetConstraints.add(boundEntityNameConstraint);

            }
            else if (loadConfig instanceof AssetRevisionModelData)
            {
                AssetRevisionModelData asset = (AssetRevisionModelData) loadConfig;
                if (asset.getClassName().equals(AssetRevisionModelData.CLASS_NAME))
                {
                    this.path = ServicesPath.getPath(FileRevisionModelData.SIMPLE_CLASS_NAME);
                    this.clearParameters();
                    assetConstraints.clear();
                    assetRevisionConstraint.setLeftMember(asset.getId());
                    assetConstraints.add(assetRevisionConstraint);

                    fileRevisionFileNameConstraint.setLeftMember(fileName + "%");
                    assetConstraints.add(fileRevisionFileNameConstraint);
                }
            }
            this.addParameter(assetConstraints);
            super.load(reader, loadConfig, callback);

        }

    };

    TreeLoader<AssetRevisionModelData> loader = new BaseTreeLoader<AssetRevisionModelData>(proxy) {
        @Override
        public boolean hasChildren(AssetRevisionModelData parent)
        {
            return parent.getClassName().equals(AssetRevisionModelData.CLASS_NAME);
        }

    };
    TreeStore<AssetRevisionModelData> store = new TreeStore<AssetRevisionModelData>(loader);

    private String boundEntityName;

    private final ArrayList<String> ids = new ArrayList<String>();

    private Long taskTypeId;

    private final Constraint assetProxyConstraint = new Constraint(EConstraintOperator.eq,
            "fileRevision.assetRevision.id");

    public ProxyDialogModel()
    {
        proxyStore.addParameter(proxyConstraint);
        store.addListener(TreeStore.DataChanged, new Listener<BaseEvent>() {

            public void handleEvent(BaseEvent be)
            {
                List<AssetRevisionModelData> assets = store.getAllItems();
                for (AssetRevisionModelData asset : assets)
                {
                    if (asset.getClassName().equals(AssetRevisionModelData.CLASS_NAME))
                    {
                        asset.setName(asset.getWorkObjectName() + "_" + asset.getTaskTypeName() + " ("
                                + asset.getVariation() + ")");

                    }
                }
            }

        });
    }

    public TreeStore<AssetRevisionModelData> getTreeStore()
    {
        return store;
    }

    public void reload()
    {
        this.loader.load();
    }

    public void setData(Long projectId, List<Hd3dModelData> models, String boundEntityName) throws Hd3dException
    {
        if (models == null)
        {
            throw new Hd3dException("No models!!");
        }

        this.boundEntityName = boundEntityName;
        this.projectId = projectId;
        this.taskTypeId = null;
        this.fileName = "";

        this.ids.clear();
        for (Hd3dModelData model : models)
        {
            this.ids.add(model.getId().toString());
        }

    }

    public ServiceStore<ProxyModelData> getProxyStore()
    {
        return proxyStore;
    }

    public void loadProxy(Long fileRevisionId)
    {
        this.proxyStore.clearParameters();

        this.proxyConstraint.setLeftMember(fileRevisionId);
        this.proxyStore.addParameter(proxyConstraint);
        this.proxyStore.reload();
    }

    public void loadProxy(AssetRevisionModelData asset)
    {
        this.proxyStore.clearParameters();
        this.assetProxyConstraint.setLeftMember(asset.getId());
        this.proxyStore.addParameter(this.assetProxyConstraint);

        // this.proxyStore.reload();
    }

    public void setTaskType(Long id)
    {
        this.taskTypeId = id;
        this.taskTypeConstraint.setLeftMember(taskTypeId);
    }

    public void setFileName(String filename)
    {
        if (filename == null)
        {
            filename = "";
        }
        this.fileName = filename;
        this.loader.load();
    }
}
