package fr.hd3d.common.ui.client.widget.proxyviewer;

import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.Text;

import fr.hd3d.common.ui.client.modeldata.asset.ProxyModelData;
import fr.hd3d.common.ui.client.widget.proxyplayer.ProxyPlayer;


public class ProxyPlayerPanel extends LayoutContainer
{
    private ProxyPlayer player;
    private final Text textNoProxy = new Text("No preview to display...");

    public ProxyPlayerPanel()
    {
        this.setLayoutOnChange(true);
        player = new ProxyPlayer();
        setProxy(null);
    }

    public void setProxy(ProxyModelData proxy)
    {
        this.removeAll();
        if (proxy == null)
        {
            this.add(textNoProxy);
        }
        else
        {
            player = new ProxyPlayer(proxy);
            player.setPixelSize(640, 480);
            this.add(player);
        }

    }
}
