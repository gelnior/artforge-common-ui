package fr.hd3d.common.ui.client.widget.proxyviewer;

import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.error.Hd3dException;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.asset.AssetRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.asset.ProxyModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;


class ProxyDialogController extends MaskableController
{
    private final ProxyDialog view;
    private final ProxyDialogModel model;

    public ProxyDialogController(ProxyDialog view, ProxyDialogModel model)
    {
        this.view = view;
        this.model = model;
        this.registerEvent();
        EventDispatcher.get().addController(this);
    }

    private void registerEvent()
    {
        this.registerEventTypes(ProxyDialogEvents.PROXY_DOWNLOAD_CLICKED);
        this.registerEventTypes(ProxyDialogEvents.FILEREVISION_SELECTED);
        this.registerEventTypes(ProxyDialogEvents.ASSETREVISION_SELECTED);
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();
        if (type == ProxyDialogEvents.PROXY_DOWNLOAD_CLICKED)
        {
            onProxyDownloadClicked(event);
        }
        else if (type == ProxyDialogEvents.FILEREVISION_SELECTED)
        {
            onFileRevisionSelected(event);
        }
        else if (type == ProxyDialogEvents.ASSETREVISION_SELECTED)
        {
            onAssetRevisionSelected(event);
        }
    }

    private void onAssetRevisionSelected(AppEvent event)
    {
        AssetRevisionModelData asset = event.getData();
        if (asset == null)
        {
            return;
        }
        model.loadProxy(asset);
    }

    private void onFileRevisionSelected(AppEvent event)
    {
        Long id = event.getData();
        model.loadProxy(id);
    }

    private void onProxyDownloadClicked(AppEvent event)
    {
        try
        {
            List<ProxyModelData> proxies = this.view.getProxySelected();
            if (proxies.size() > 1 && proxies.isEmpty())
            {
                throw new Hd3dException("More one row selected !! Please select one row. ");
            }
            ProxyModelData proxy = proxies.get(0);
            this.view.openNewWindow(proxy.getDownloadPath());
        }
        catch (Hd3dException e)
        {
            e.print();
        }
    }
}
