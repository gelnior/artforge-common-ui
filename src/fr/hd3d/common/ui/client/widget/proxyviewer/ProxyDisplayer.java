package fr.hd3d.common.ui.client.widget.proxyviewer;

import java.util.List;

import fr.hd3d.common.ui.client.error.Hd3dException;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;


/**
 * Tool class to display task creator dialog.
 * 
 * @author HD3D
 */
public class ProxyDisplayer
{
    /** Dialog needed for displaying task creation form. */
    private static ProxyDialog proxyDialog;
    private static ProxySimpleDialog proxySimpleDialog;

    public static ProxyDialog getDialog()
    {
        if (proxyDialog == null)
        {
            proxyDialog = new ProxyDialog();
        }
        return proxyDialog;
    }

    public static ProxySimpleDialog getSimpleDialog()
    {
        if (proxySimpleDialog == null)
        {
            proxySimpleDialog = new ProxySimpleDialog();
        }
        return proxySimpleDialog;
    }

    public static void show(Long projectId, List<Hd3dModelData> models, String boundEntityName) throws Hd3dException
    {
        ProxyDialog dialog = getDialog();
        dialog.setData(projectId, models, boundEntityName);
        dialog.show();
    }

    public static void showSimple(Long projectId, Long workObjectId, String boundEntityName) throws Hd3dException
    {
        ProxySimpleDialog dialog = getSimpleDialog();
        dialog.setData(projectId, workObjectId, boundEntityName);
        dialog.show();
    }
}
