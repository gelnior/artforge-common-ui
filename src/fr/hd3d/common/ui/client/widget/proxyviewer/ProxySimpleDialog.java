package fr.hd3d.common.ui.client.widget.proxyviewer;

import java.util.List;

import com.extjs.gxt.ui.client.Style.Scroll;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.HorizontalPanel;

import fr.hd3d.common.ui.client.error.Hd3dException;
import fr.hd3d.common.ui.client.modeldata.asset.ProxyModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;


public class ProxySimpleDialog extends Dialog
{
    private final ProxySimpleDialogModel model;

    private TasktypePanel taskTypePanel;
    private ProxyPlayerPanel playerPanel;

    public ProxySimpleDialog()
    {
        model = new ProxySimpleDialogModel();
        new ProxySimpleDialogController(this, model);
        this.styles();
    }

    private void styles()
    {
        this.setPixelSize(800, 580);
        this.setButtons(Dialog.CLOSE);
        this.setScrollMode(Scroll.AUTOY);
        HorizontalPanel globalPanel = new HorizontalPanel();
        globalPanel.setSpacing(20);
        this.setHeading("Select a task type to display the last associated preview.");
        taskTypePanel = new TasktypePanel(ProxySimpleDialogEvent.TASKTYPE_SELECTED);
        taskTypePanel.setSize(100, 460);
        globalPanel.add(taskTypePanel);
        playerPanel = new ProxyPlayerPanel();

        globalPanel.add(playerPanel);
        this.add(globalPanel);
        this.setHideOnButtonClick(true);
        this.setLayoutOnChange(true);
    }

    public void setData(Long projectId, Long workObjectId, String boundEntityName) throws Hd3dException
    {
        model.setProject(projectId);
        model.setWorkObject(workObjectId, boundEntityName);
    }

    public void displayTaskType(List<TaskTypeModelData> taskTypes)
    {
        taskTypePanel.setTaskTypes(taskTypes);
    }

    public void setProxy(ProxyModelData proxy)
    {
        playerPanel.setProxy(proxy);
    }

    @Override
    public void show()
    {
        this.taskTypePanel.removeAll();
        this.playerPanel.setProxy(null);
        this.model.reloadTaskType();
        super.show();
    }

    public void maskPlayerPanel()
    {
        this.playerPanel.mask("Loading...");
    }

    public void unmaskPlayerPanel()
    {
        this.playerPanel.unmask();
    }
}
