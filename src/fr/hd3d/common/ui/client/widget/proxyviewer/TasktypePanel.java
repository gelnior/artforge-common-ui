package fr.hd3d.common.ui.client.widget.proxyviewer;

import java.util.List;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.util.Margins;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.layout.HBoxLayoutData;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;


public class TasktypePanel extends LayoutContainer
{
    private final EventType event;

    public TasktypePanel(EventType event)
    {
        this.setLayoutOnChange(true);
        this.event = event;
    }

    public void setTaskTypes(List<TaskTypeModelData> taskTypes)
    {
        this.removeAll();
        for (TaskTypeModelData tasktype : taskTypes)
        {
            Button button = createButton(tasktype);
            this.add(button, new HBoxLayoutData(new Margins(10)));
        }
    }

    private Button createButton(final TaskTypeModelData tasktype)
    {
        Button taskTypeButton = new Button(
                "<span style='background-color:"
                        + tasktype.getColor()
                        + "; font-size: 13px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;<span style='font-weight: bold; font-size : 12px;'>&nbsp;"
                        + tasktype.getName() + "&nbsp;&nbsp;</span> ");
        taskTypeButton.addSelectionListener(new SelectionListener<ButtonEvent>() {
            @Override
            public void componentSelected(ButtonEvent ce)
            {
                EventDispatcher.forwardEvent(event, tasktype);
            }
        });

        return taskTypeButton;
    }
}
