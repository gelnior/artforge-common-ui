package fr.hd3d.common.ui.client.widget.proxyviewer.cmd;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.mvc.controller.BaseCommand;
import fr.hd3d.common.ui.client.widget.proxyviewer.ProxySimpleDialog;
import fr.hd3d.common.ui.client.widget.proxyviewer.ProxySimpleDialogModel;


/***
 * Command to display the last proxy concerning the selected task type.
 * 
 * @author HD3D
 * 
 */
public class TaskTypeSelectedCmd implements BaseCommand
{
    private final ProxySimpleDialog view;
    private final ProxySimpleDialogModel model;

    /**
     * Default constructor.
     * 
     * @param view
     *            the view
     * @param model
     *            the model
     */
    public TaskTypeSelectedCmd(ProxySimpleDialog view, ProxySimpleDialogModel model)
    {
        this.view = view;
        this.model = model;
    }

    public void execute(AppEvent event)
    {
        TaskTypeModelData tasktype = event.getData();
        if (tasktype != null)
        {
            this.view.maskPlayerPanel();
            this.model.getProxyFromTaskType(tasktype);
        }
    }

}
