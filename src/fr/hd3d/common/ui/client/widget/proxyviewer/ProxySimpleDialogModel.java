package fr.hd3d.common.ui.client.widget.proxyviewer;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.event.LoadListener;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.common.ui.client.error.Hd3dException;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.asset.ProxyModelData;
import fr.hd3d.common.ui.client.modeldata.reader.ProxyReader;
import fr.hd3d.common.ui.client.modeldata.reader.TaskTypeReader;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.service.store.ServiceStore;


public class ProxySimpleDialogModel
{
    private final ServiceStore<TaskTypeModelData> tasktypeStore = new ServiceStore<TaskTypeModelData>(
            new TaskTypeReader());

    private final ServiceStore<ProxyModelData> proxyStore = new ServiceStore<ProxyModelData>(new ProxyReader());

    private String boundEntityName;

    private Long workObjectId;

    public ProxySimpleDialogModel()
    {
        proxyStore.setPath(ServicesURI.LAST_PROXIES);

        proxyStore.addLoadListener(new LoadListener() {
            @Override
            public void loaderLoad(LoadEvent le)
            {
                EventDispatcher.forwardEvent(ProxySimpleDialogEvent.PROXIES_LOADED, proxyStore.getModels());
            }
        });

        tasktypeStore.addLoadListener(new LoadListener() {
            @Override
            public void loaderLoad(LoadEvent le)
            {
                if (boundEntityName != null)
                {
                    ArrayList<TaskTypeModelData> allTaskTypes = new ArrayList<TaskTypeModelData>(tasktypeStore
                            .getModels());
                    tasktypeStore.removeAll();
                    for (TaskTypeModelData tasktype : allTaskTypes)
                    {
                        if (tasktype.getEntityName().equals(boundEntityName))
                        {
                            tasktypeStore.add(tasktype);
                        }
                    }
                    EventDispatcher.forwardEvent(ProxySimpleDialogEvent.TASKTYPE_LOADED);
                }
            }
        });
    }

    public void setProject(Long projectId) throws Hd3dException
    {
        if (projectId == null)
        {
            throw new Hd3dException("The project id is null.");
        }
        tasktypeStore.setPath(ServicesURI.PROJECTS + "/" + projectId + "/" + ServicesURI.TASK_TYPES);
    }

    public void reloadTaskType()
    {
        tasktypeStore.reload();
    }

    public List<TaskTypeModelData> getTaskTypes()
    {
        return tasktypeStore.getModels();
    }

    public void getProxyFromTaskType(TaskTypeModelData tasktype)
    {
        proxyStore.setPath(ServicesURI.WORKOBJECTS + "/" + workObjectId + "/" + boundEntityName + "/"
                + ServicesURI.TASK_TYPES + "/" + tasktype.getId() + "/");
        proxyStore.reload();
    }

    public void setWorkObject(Long workObjectId, String boundEntityName)
    {
        this.workObjectId = workObjectId;
        this.boundEntityName = boundEntityName;
    }

    public List<ProxyModelData> getProxies()
    {
        return this.proxyStore.getModels();
    }
}
