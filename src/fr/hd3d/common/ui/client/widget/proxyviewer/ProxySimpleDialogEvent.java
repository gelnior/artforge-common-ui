package fr.hd3d.common.ui.client.widget.proxyviewer;

import com.extjs.gxt.ui.client.event.EventType;


public class ProxySimpleDialogEvent
{

    public static final EventType TASKTYPE_LOADED = new EventType();
    public static final EventType TASKTYPE_SELECTED = new EventType();
    public static final EventType PROXIES_LOADED = new EventType();

}
