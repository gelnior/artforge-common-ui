package fr.hd3d.common.ui.client.widget.proxyviewer;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.mvc.controller.CommandController;
import fr.hd3d.common.ui.client.widget.proxyviewer.cmd.ProxiesLoadedCmd;
import fr.hd3d.common.ui.client.widget.proxyviewer.cmd.TaskTypeLoadedCmd;
import fr.hd3d.common.ui.client.widget.proxyviewer.cmd.TaskTypeSelectedCmd;


public class ProxySimpleDialogController extends CommandController
{

    private final ProxySimpleDialog view;
    private final ProxySimpleDialogModel model;

    public ProxySimpleDialogController(ProxySimpleDialog view, ProxySimpleDialogModel model)
    {
        this.view = view;
        this.model = model;
        this.registerEvent();
        EventDispatcher.get().addController(this);
    }

    private void registerEvent()
    {
        commandMap.put(ProxySimpleDialogEvent.TASKTYPE_LOADED, new TaskTypeLoadedCmd(view, model));
        commandMap.put(ProxySimpleDialogEvent.TASKTYPE_SELECTED, new TaskTypeSelectedCmd(view, model));
        commandMap.put(ProxySimpleDialogEvent.PROXIES_LOADED, new ProxiesLoadedCmd(view, model));
    }
}
