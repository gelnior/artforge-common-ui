package fr.hd3d.common.ui.client.widget.proxyviewer.cmd;

import java.util.List;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.modeldata.asset.ProxyModelData;
import fr.hd3d.common.ui.client.mvc.controller.BaseCommand;
import fr.hd3d.common.ui.client.widget.proxyviewer.ProxySimpleDialog;
import fr.hd3d.common.ui.client.widget.proxyviewer.ProxySimpleDialogModel;


/**
 * Command to display proxies in the view.
 * 
 * @author HD3D
 * 
 */
public class ProxiesLoadedCmd implements BaseCommand
{
    private final ProxySimpleDialog view;
    private final ProxySimpleDialogModel model;

    public ProxiesLoadedCmd(ProxySimpleDialog view, ProxySimpleDialogModel model)
    {
        this.view = view;
        this.model = model;
    }

    public void execute(AppEvent event)
    {
        List<ProxyModelData> proxies = this.model.getProxies();
        ProxyModelData proxy = null;
        if (proxies != null && !proxies.isEmpty())
        {
            proxy = proxies.get(0);
        }
        this.view.unmaskPlayerPanel();
        this.view.setProxy(proxy);
    }
}
