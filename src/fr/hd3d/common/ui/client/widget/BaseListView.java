package fr.hd3d.common.ui.client.widget;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.ListViewEvent;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.ListView;
import com.google.gwt.core.client.JavaScriptException;
import com.google.gwt.user.client.Element;


public class BaseListView<M extends ModelData> extends ListView<M>
{
    protected Element overElement;
    private final static String OVER_STYLE = "x-view-item-over";

    public BaseListView(ListStore<M> relationNames)
    {
        super(relationNames);
    }

    @Override
    protected void onMouseOut(ListViewEvent<M> ce)
    {
        try
        {
            super.onMouseOut(ce);
        }
        catch (JavaScriptException e)
        {
            if (overElement != null)
            {
                fly(overElement).removeStyleName(OVER_STYLE);
            }
            overElement = null;
        }
    }

    @Override
    protected void onMouseOver(ListViewEvent<M> ce)
    {
        if (ce.getIndex() != -1)
        {
            Element e = getElement(ce.getIndex());
            if (e != null && e != overElement)
            {
                overElement = e;
            }
        }

        super.onMouseOver(ce);
    }

    @Override
    protected void onRemove(M data, int index)
    {
        Element e = getElement(index);
        if (e != null)
        {
            if (overElement == e)
            {
                overElement = null;
            }
        }

        super.onRemove(data, index);
    }
}
