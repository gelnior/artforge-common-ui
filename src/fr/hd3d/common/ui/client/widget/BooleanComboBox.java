package fr.hd3d.common.ui.client.widget;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.form.ComboBox;

import fr.hd3d.common.ui.client.modeldata.BooleanModelData;


/**
 * Boolean combobox allow to select true or false value via a combobox.
 * 
 * @author HD3D
 */
public class BooleanComboBox extends ComboBox<BooleanModelData>
{
    /** True value represented by a ModelDaata object. */
    public static final BooleanModelData TRUE = new BooleanModelData("true", Boolean.TRUE);
    /** False value represented by a ModelDaata object. */
    public static final BooleanModelData FALSE = new BooleanModelData("false", Boolean.FALSE);

    /**
     * Default constructor, it sets store, fills it and configures combo box.
     */
    public BooleanComboBox()
    {
        this.setStore(new ListStore<BooleanModelData>());

        this.getStore().add(TRUE);
        this.getStore().add(FALSE);

        this.setDisplayField(BooleanModelData.NAME_FIELD);
        this.setValueField(BooleanModelData.VALUE_FIELD);

        this.setForceSelection(true);
        this.setTriggerAction(TriggerAction.ALL);
    }
}
