package fr.hd3d.common.ui.client.widget;

import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.event.LoadListener;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.client.enums.EConstraintLogicalOperator;
import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.asset.AssetRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.reader.AssetRevisionReader;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.AndConstraint;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.EqConstraint;
import fr.hd3d.common.ui.client.service.parameter.LogicConstraint;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;


/**
 * 
 * @author guillaume-maucomble
 * 
 */
public class AutoAssetRevisionComboBox extends AutoCompleteModelCombo<AssetRevisionModelData>
{
    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    protected LogicConstraint projectFilterGroup;
    protected AndConstraint andConstraint = new AndConstraint();

    public AutoAssetRevisionComboBox()
    {
        this(false);
    }

    public AutoAssetRevisionComboBox(boolean emptyField)
    {
        super(new AssetRevisionReader(), emptyField);

        this.setStyles();
        this.store.setPath(ServicesPath.getPath(AssetRevisionModelData.SIMPLE_CLASS_NAME));
        this.setContextMenu();
        this.setTypeAhead(false);

        this.andConstraint.setLeftMember(likeConstraint);
        this.andConstraint.setRightMember(getProjectFilter());
        this.store.removeParameter(likeConstraint);
        this.store.addParameter(andConstraint);
    }

    public AutoAssetRevisionComboBox(boolean emptyField, String emptyFieldName)
    {
        super(new AssetRevisionReader(), emptyField, emptyFieldName);

        this.setStyles();
        this.store.setPath(ServicesPath.getPath(AssetRevisionModelData.SIMPLE_CLASS_NAME));
        this.store.addParameter(this.getProjectFilter());
        this.setContextMenu();
        this.setTypeAhead(false);
    }

    @Override
    protected void onBeforeLoad(LoadEvent le)
    {
        if (MainModel.getCurrentProject() != null)
            this.setProject(MainModel.getCurrentProject().getId());
        else
            this.setProject(null);
    }

    public void setProject(Long projectId)
    {
        LogicConstraint fg = this.getProjectFilter();
        if (projectId != null)
        {
            if (fg.getRightMember() == null)
                fg.setRightMember(new EqConstraint("project.id", projectId));
            else
                ((EqConstraint) fg.getRightMember()).setLeftMember(projectId);
        }
        else
        {
            fg.setRightMember(null);
        }
    }

    private LogicConstraint getProjectFilter()
    {
        if (projectFilterGroup == null)
        {
            projectFilterGroup = new LogicConstraint(EConstraintLogicalOperator.OR);
            projectFilterGroup.setLeftMember(new Constraint(EConstraintOperator.isnull, "project.id", null));
        }

        return this.projectFilterGroup;
    }

    @Override
    protected String getLike()
    {
        return "%" + getRawValue() + "%";
    }

    /**
     * Initialize a value with the model data of ID equal to <i>id</i> by loading the model data from services.
     * 
     * @param id
     *            The id of the model data to set as value.
     */
    @Override
    public void setValueById(final Long id)
    {
        this.idConstraint.setLeftMember(id);
        this.store.removeParameter(andConstraint);
        this.store.removeParameter(pagination);
        this.store.addParameter(idConstraint);

        super.setValueById(id);
    }

    /**
     * After value initialization, when model data is loaded, constraint used to filter on id is removed and name
     * constraint is restored.
     */
    @Override
    protected void onIdLoad(LoadListener listener, Long id)
    {
        super.onIdLoad(listener, id);
        this.store.addParameter(andConstraint);
        this.store.addParameter(pagination);
        this.store.removeParameter(idConstraint);
    }

    /**
     * Set combo styles.
     */
    @Override
    protected void setStyles()
    {
        super.setStyles();
        this.setEmptyText("Select Asset Revision...");
        this.setDisplayField(RecordModelData.NAME_FIELD);
    }

}
