package fr.hd3d.common.ui.client.widget;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.modeldata.reader.PersonReader;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.OrConstraint;
import fr.hd3d.common.ui.client.util.KeyBoardUtils;


public class PersonComboBox extends AutoCompleteModelCombo<PersonModelData>
{
    private final Constraint loginConstraint = new Constraint(EConstraintOperator.like,
            PersonModelData.PERSON_LOGIN_FIELD, "%", null);
    private final Constraint firstNameConstraint = new Constraint(EConstraintOperator.like,
            PersonModelData.PERSON_FIRST_NAME_FIELD, "%", null);
    private final Constraint lastNameConstraint = new Constraint(EConstraintOperator.like,
            PersonModelData.PERSON_LAST_NAME_FIELD, "%", null);

    public PersonComboBox()
    {
        super(new PersonReader());

        OrConstraint or = new OrConstraint(loginConstraint, new OrConstraint(firstNameConstraint, lastNameConstraint));

        this.store.clearParameters();
        this.store.addParameter(or);
        this.store.addParameter(pagination);
        this.setOrderColumn(PersonModelData.PERSON_LAST_NAME_FIELD);
        this.setContextMenu();
    }

    /**
     * When key is up, the drop-down list is refreshed if max count is above MAX_RECORD_NUMBER or if text field is
     * empty.
     * 
     * @param keyCode
     *            The key typed by user.
     */
    @Override
    protected void onKeyUp(int keyCode)
    {
        if (KeyBoardUtils.isTextKey(keyCode))
        {

            if (store.getFilters() != null)
                store.getFilters().remove(storeFilter);

            this.loginConstraint.setLeftMember(getRawValue() + "%");
            this.firstNameConstraint.setLeftMember(getRawValue() + "%");
            this.lastNameConstraint.setLeftMember(getRawValue() + "%");

            this.store.reload();
            this.expand();
        }
    }

    @Override
    public PersonModelData getValue()
    {
        return value;
    }

    private native String getFullNameTemplate() /*-{ 
                                                                                   return  [ 
                                                                                   '<tpl for=".">', 
                                                                                   '<div class="x-combo-list-item">{lastName}</div>', 
                                                                                   '</tpl>' 
                                                                                   ].join(""); 
                                                                                   }-*/;
}
