package fr.hd3d.common.ui.client.widget;

import com.extjs.gxt.ui.client.widget.LayoutContainer;


public class FocusableComponent extends LayoutContainer
{
    public FocusableComponent()
    {
        setTabIndex(0);
    }
}
