package fr.hd3d.common.ui.client.widget;

import java.util.List;
import java.util.Map;

import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.form.ComboBox;

import fr.hd3d.common.ui.client.modeldata.FieldModelData;


/**
 * FieldComboBox is a combo box containing field objects (three fields, id, name and value).
 * 
 * @author HD3D
 */
public class FieldComboBox extends ComboBox<FieldModelData>
{
    /**
     * Default constructor. Sets store, display field and value field.
     */
    public FieldComboBox()
    {
        super();

        ListStore<FieldModelData> store = new ListStore<FieldModelData>();
        store.setSortField(FieldModelData.ID_FIELD);
        store.sort(FieldModelData.ID_FIELD, SortDir.ASC);

        this.setStore(store);
        this.setForceSelection(true);
        this.setTriggerAction(TriggerAction.ALL);

        this.setDisplayField(FieldModelData.NAME_FIELD);
        this.setValueField(FieldModelData.VALUE_FIELD);
    }

    /**
     * Default constructor. Sets store, display field and value field. Add list values to the combo data store.
     */
    public FieldComboBox(List<FieldModelData> list)
    {
        this();

        for (FieldModelData field : list)
        {
            this.getStore().add(field);
        }
        this.getStore().sort(FieldModelData.NAME_FIELD, SortDir.ASC);
    }

    /**
     * Default constructor. Sets store, display field and value field. Add map entries value to the combo data store.
     */
    public FieldComboBox(FastMap<FieldModelData> map)
    {
        this();

        for (Map.Entry<String, FieldModelData> entry : map.entrySet())
        {
            this.getStore().add(entry.getValue());
        }
        this.getStore().sort(FieldModelData.NAME_FIELD, SortDir.ASC);
    }

    /**
     * @return Value contained in the field selected, it returns the expected value, not the field wrapping it.
     */
    public Object getDirectValue()
    {
        return this.getValue().getValue();
    }

    /**
     * Add a new field (couple display value - name) to the combo box.
     * 
     * @param name
     *            Displayed value.
     * @param value
     *            Real value.
     * 
     * @return added field.
     */
    public FieldModelData addField(int id, String name, Object value)
    {
        FieldModelData field = new FieldModelData(id, name, value);
        this.getStore().add(field);

        return field;
    }

    /**
     * @param realValue
     *            The expected value (not the field modeldata).
     * @return the field corresponding to the value.
     */
    public Object getValue(Object realValue)
    {
        for (FieldModelData field : this.getStore().getModels())
        {
            Object modelValue = field.getValue();

            if (modelValue.equals(realValue))
            {
                return field;
            }
        }
        return null;
    }

    public void setValueByStringValue(String stringValue)
    {
        FieldModelData field = this.getStore().findModel(FieldModelData.VALUE_FIELD, stringValue);
        this.setValue(field);
        this.setOriginalValue(field);
        this.select(field);
    }

    public void setValueByIntegerValue(Integer value)
    {
        FieldModelData field = this.getStore().findModel(FieldModelData.VALUE_FIELD, value);
        this.setValue(field);
        this.setOriginalValue(field);
        this.select(field);
    }
}
