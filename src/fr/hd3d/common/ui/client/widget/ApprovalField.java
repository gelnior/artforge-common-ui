package fr.hd3d.common.ui.client.widget;

import java.util.List;

import com.extjs.gxt.ui.client.widget.form.TextField;

import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.util.CollectionUtils;


public class ApprovalField extends TextField<String>
{
    public void setValue(List<ApprovalNoteModelData> approvals)
    {
        if (CollectionUtils.isNotEmpty(approvals))
        {
            ApprovalNoteModelData approval = approvals.get(0);
            this.setStyleAttribute("background-color", "green");
            super.setRawValue(approval.getStatus());
            super.setValue(approval.getStatus());
        }
        else
        {
            this.setStyleAttribute("background-color", "transparent");
            super.setRawValue(" ");
            super.setValue(" ");
        }
    }
}
