package fr.hd3d.common.ui.client.widget.proxycreatordialog;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;


public class ProxyCreatorModel
{
    private Long fileRevisionId;

    public void setFileRevision(Long fileRevisionId)
    {
        this.fileRevisionId = fileRevisionId;
    }

    public String getProxyUploadPath()
    {
        String path = "";
        String servicesUrl = RestRequestHandlerSingleton.getInstance().getServicesUrl();
        path = servicesUrl + ServicesURI.FILEREVISIONS + '/' + fileRevisionId + '/' + ServicesURI.PROXIES + '/'
                + ServicesURI.UPLOAD;
        return path;
    }
}
