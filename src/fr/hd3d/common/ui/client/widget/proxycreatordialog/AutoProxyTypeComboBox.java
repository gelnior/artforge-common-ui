package fr.hd3d.common.ui.client.widget.proxycreatordialog;

import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.asset.ProxyTypeModelData;
import fr.hd3d.common.ui.client.modeldata.reader.ProxyTypeReader;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.LogicConstraint;
import fr.hd3d.common.ui.client.widget.AutoCompleteModelCombo;


public class AutoProxyTypeComboBox extends AutoCompleteModelCombo<ProxyTypeModelData>
{
    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    protected LogicConstraint projectFilterGroup;

    public AutoProxyTypeComboBox()
    {
        this(false, "");
    }

    public AutoProxyTypeComboBox(boolean emptyField)
    {
        this(emptyField, "");
    }

    public AutoProxyTypeComboBox(boolean emptyField, String emptyFieldName)
    {
        super(new ProxyTypeReader(), emptyField, emptyFieldName);

        this.setStyles();
        this.store.setPath(ServicesPath.getPath(ProxyTypeModelData.SIMPLE_CLASS_NAME));
        // this.store.addParameter(this.getProjectFilter());
        this.setContextMenu();
        this.setTypeAhead(false);
    }

    // public void setProject(Long projectId)
    // {
    // LogicConstraint fg = this.getProjectFilter();
    // if (projectId != null)
    // {
    // fg.setRightMember(new Constraint(EConstraintOperator.eq, "project.id", projectId));
    // }
    // this.setData();
    // }
    //
    // private LogicConstraint getProjectFilter()
    // {
    // if (projectFilterGroup == null)
    // {
    // projectFilterGroup = new LogicConstraint(EConstraintLogicalOperator.OR);
    // projectFilterGroup.setLeftMember(new Constraint(EConstraintOperator.isnull, "project.id", null));
    // }
    //
    // return this.projectFilterGroup;
    // }

    @Override
    protected String getLike()
    {
        return "%" + getRawValue() + "%";
    }

    /**
     * Set combo styles.
     */
    @Override
    protected void setStyles()
    {
        super.setStyles();
        this.setEmptyText("Select Proxy Type...");
        this.setDisplayField(RecordModelData.NAME_FIELD);
    }

}
