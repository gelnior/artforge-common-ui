package fr.hd3d.common.ui.client.widget.proxycreatordialog;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.FormEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.FileUploadField;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.form.FormPanel.Method;
import com.extjs.gxt.ui.client.widget.layout.AnchorLayout;
import com.extjs.gxt.ui.client.widget.layout.FormData;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.error.Hd3dException;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.widget.SavingStatus;


/**
 * Dialog box that allows user to upload file to asset system. The file uploading is based on a form and file field.
 * 
 * @author HD3D
 */
public class ProxyCreatorDialog extends Dialog
{
    public static String FILE_VAR_FORM = "file";
    public static String DESCRIPTION_VAR_FORM = "description";
    public static String PROXYTYPE_VAR_FORM = "proxyType";

    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    /** Status showing that upload is running. */
    protected SavingStatus savingStatus = new SavingStatus();

    /** Panel containing upload form. */
    protected final FormPanel panel = new FormPanel();

    /** proxy Type combo box. */
    protected AutoProxyTypeComboBox proxyTypeComboBox = new AutoProxyTypeComboBox();

    /** File field. */
    protected FileUploadField file = new FileUploadField();

    private final TextField<String> descriptionField = new TextField<String>();

    private final String title;
    /** Event to forward when upload is finished. */
    private AppEvent event;

    private final ProxyCreatorModel model = new ProxyCreatorModel();

    /** Button that submits upload form. */
    protected Button submit = new Button("OK");

    /** Button that submits upload form. */
    protected Button cancel = new Button("CANCEL");

    /**
     * Default constructor.
     */
    public ProxyCreatorDialog()
    {
        this(new AppEvent(ProxyCreatorEvents.PROXY_CREATED), "Proxy creation");
    }

    /**
     * Default constructor.
     */
    public ProxyCreatorDialog(AppEvent event, String title)
    {
        this.event = event;
        this.title = title;

        this.setButtons("");
        this.setStyles();
        this.setButtonBar();
        this.createForm();
        this.setListener();
    }

    /**
     * When dialog is shown, the file field is cleared.
     * 
     * @see com.extjs.gxt.ui.client.widget.BoxComponent#onShow()
     */
    @Override
    protected void onShow()
    {
        panel.reset();
        super.onShow();
    }

    /**
     * @param ce
     * @throws Hd3dException
     */
    protected void onSubmitClicked(ButtonEvent ce) throws Hd3dException
    {
        if (this.file.getValue() == null || this.file.getValue().equals(""))
        {
            throw new Hd3dException("Please set a file.");
        }
        if (proxyTypeComboBox.getValue() == null)
        {
            throw new Hd3dException("Please set a proxy type.");
        }

        this.panel.submit();
        this.savingStatus.show();
    }

    /**
     * When submit is finished, the saving status is hidden, the dialog is hidden and the set event is raised.
     * 
     * @param be
     *            Base event raised by form signaling that submit is finished.
     */
    protected void onSubmitFinished(FormEvent be)
    {
        this.savingStatus.hide();
        hide();
        EventDispatcher.forwardEvent(event);
    }

    /**
     * Set listeners on submit event : submit button clicking and submit finishing.
     */
    protected void setListener()
    {
        submit.addSelectionListener(new SelectionListener<ButtonEvent>() {
            @Override
            public void componentSelected(ButtonEvent ce)
            {
                try
                {
                    panel.setAction(model.getProxyUploadPath());
                    onSubmitClicked(ce);
                }
                catch (Hd3dException e)
                {
                    e.print();
                }
            }
        });

        cancel.addSelectionListener(new SelectionListener<ButtonEvent>() {
            @Override
            public void componentSelected(ButtonEvent ce)
            {
                hide();

            }
        });

        panel.addListener(Events.Submit, new Listener<FormEvent>() {
            public void handleEvent(FormEvent be)
            {
                onSubmitFinished(be);
            }
        });
    }

    /**
     * Create the upload form.
     */
    protected void createForm()
    {
        this.panel.setBorders(false);
        this.panel.setBodyBorder(false);
        this.panel.setHeaderVisible(false);
        this.panel.setMethod(Method.POST);
        this.panel.setEncoding(FormPanel.Encoding.MULTIPART);

        this.proxyTypeComboBox.setLabelSeparator(":");
        this.proxyTypeComboBox.setFieldLabel("Proxy type");
        this.proxyTypeComboBox.setName(PROXYTYPE_VAR_FORM);
        this.panel.add(proxyTypeComboBox);

        this.descriptionField.setFieldLabel("Description");
        this.descriptionField.setName(DESCRIPTION_VAR_FORM);
        this.panel.add(descriptionField);

        this.file.setFieldLabel("File");
        this.file.setName(FILE_VAR_FORM);
        this.panel.add(this.file, new FormData("100%"));

        this.add(panel);
    }

    /**
     * Add saving status and submit button ("OK") to the button bar.
     */
    protected void setButtonBar()
    {
        this.getButtonBar().add(savingStatus);
        this.getButtonBar().add(submit);
        this.getButtonBar().add(cancel);
    }

    /**
     * Set editor styles : layout, size...
     */
    protected void setStyles()
    {
        this.setHeading(this.title);
        this.setSize(400, 200);
        this.setLayout(new AnchorLayout());
        // this.setHideOnButtonClick(true);
        this.setModal(true);
    }

    public void setFileRevision(Long fileRevisionId)
    {
        this.model.setFileRevision(fileRevisionId);
    }

    public void setOkEvent(AppEvent okEvent)
    {
        this.event = okEvent;
    }
}
