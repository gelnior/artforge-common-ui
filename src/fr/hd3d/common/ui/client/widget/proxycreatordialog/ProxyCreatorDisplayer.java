package fr.hd3d.common.ui.client.widget.proxycreatordialog;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.error.Hd3dException;


public class ProxyCreatorDisplayer
{
    private static ProxyCreatorDialog ProxyCreator;

    /**
     * @return Proxy creation dialog.
     */
    public static ProxyCreatorDialog getDialog()
    {
        if (ProxyCreator == null)
        {
            ProxyCreator = new ProxyCreatorDialog();
        }
        return ProxyCreator;
    }

    /**
     * Display the dialog to add a proxy file.
     * 
     * @param fileRevisionId
     * @param okEvent
     */
    public static void show(Long fileRevisionId, EventType okEvent)
    {
        show(fileRevisionId, new AppEvent(okEvent));
    }

    /**
     * Display the dialog to add a proxy file.
     * 
     * 
     * @param fileRevisionId
     * @param okEvent
     * @param title
     */
    public static void show(Long fileRevisionId, AppEvent okEvent, String title)
    {
        try
        {
            if (fileRevisionId == null)
            {
                throw new Hd3dException("No file revision.");
            }

            ProxyCreatorDialog dialog = getDialog();

            // dialog.reset();

            dialog.setFileRevision(fileRevisionId);

            dialog.setOkEvent(okEvent);

            if (title != null)
            {
                dialog.setHeading(title);
            }

            dialog.show();
        }
        catch (Hd3dException e)
        {
            e.print();
        }
    }

    public static void show(Long fileRevisionId, AppEvent okEvent)
    {
        show(fileRevisionId, okEvent, null);
    }
}
