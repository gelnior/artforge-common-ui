package fr.hd3d.common.ui.client.widget.proxycreatordialog;

import com.extjs.gxt.ui.client.event.EventType;


public class ProxyCreatorEvents
{

    public static final EventType PROXY_CREATE_CLICKED = new EventType();
    public static final EventType PROXY_CREATED = new EventType();

}
