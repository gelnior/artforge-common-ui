package fr.hd3d.common.ui.client.widget;

public class MinuteField extends FieldComboBox
{
    /**
     * Default constructor.
     */
    public MinuteField()
    {
        super();
        this.setWidth(55);

        for (int i = 0; i < 60; i++)
            this.addField(i, i + "m", Integer.valueOf(i));
    }
}
