package fr.hd3d.common.ui.client.widget.workobjecttree.model.callback;

import org.restlet.client.Request;
import org.restlet.client.Response;

import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.widget.workobjecttree.model.TreeLibraryModel;


public class AllCategoriesCallBack extends BaseCallback
{
    private final TreeLibraryModel treeLibraryModel;

    public AllCategoriesCallBack(TreeLibraryModel _treeLibraryModel)
    {
        this.treeLibraryModel = _treeLibraryModel;

    }

    @Override
    public void onSuccess(Request request, Response response)
    {
    // JsonRepresentation representation = response.getEntityAsJson();
    // ModelType categoriesModelType = CategoryModelData.getModelType();
    //
    // CategoryModelData loadConfigCategoriesModelData = new CategoryModelData();
    // CategoryReader jsonReader = new CategoryReader(categoriesModelType);
    // ListLoadResult<CategoryModelData> result = jsonReader.read(loadConfigCategoriesModelData, representation
    // .getText());
    //
    // ListStore<CategoryModelData> treeStore = this.treeLibraryModel.getListCategories();
    // treeStore.removeAll();
    // treeStore.add(result.getData());
    }
}
