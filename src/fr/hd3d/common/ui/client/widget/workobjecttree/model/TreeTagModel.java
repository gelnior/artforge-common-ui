package fr.hd3d.common.ui.client.widget.workobjecttree.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.restlet.client.data.Method;

import com.extjs.gxt.ui.client.data.BaseTreeModel;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.store.TreeStore;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.technical.TagCategoryModelData;
import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.parameter.OrderBy;
import fr.hd3d.common.ui.client.widget.workobjecttree.Urls;
import fr.hd3d.common.ui.client.widget.workobjecttree.model.callback.CategoriesCallBackTreeTag;


public class TreeTagModel
{
    private static final long serialVersionUID = 5716548900553775333L;
    private TreeStore<BaseTreeModel> treeStore = null;
    private ProjectModelData currentProject = null;
    private BaseTreeModel itemExpand = null;
    // private BaseTreeModel itemCurrent = null;
    private ListStore<TagCategoryModelData> listCategories = new ListStore<TagCategoryModelData>();
    public BaseTreeModel root = null;
    private final HashMap<BaseTreeModel, Long> _loadingChildrenCounter = new HashMap<BaseTreeModel, Long>();

    void setNbLoadingChildren(BaseTreeModel item, Long nb)
    {
        _loadingChildrenCounter.put(item, nb);
    }

    private Long getNbLoadingChildren(BaseTreeModel item)
    {
        Long ret = _loadingChildrenCounter.get(item);
        if (ret == null)
        {
            ret = Long.valueOf(0);
            _loadingChildrenCounter.put(item, ret);
        }
        return ret;
    }

    public Long getLoadingChildrenNumber(BaseTreeModel item)
    {
        if (item == null)
        {
            return Long.valueOf(0);
        }
        return getNbLoadingChildren(item);
    }

    public void decreaseLoadingChildren(BaseTreeModel item)
    {
        if (item == null)
        {
            return;
        }
        Long nb = getNbLoadingChildren(item);
        nb--;
        setNbLoadingChildren(item, nb);

        item.set("loadingChildren", nb);
    }

    public void increaseLoadingChildren(BaseTreeModel item)
    {
        if (item == null)
        {
            return;
        }
        Long nb = getNbLoadingChildren(item);
        nb++;
        setNbLoadingChildren(item, nb);
        // GWT.log("increasing: " + nb, null);
        item.set("loadingChildren", nb);
    }

    public TreeTagModel()
    {
        treeStore = new TreeStore<BaseTreeModel>();
        // StoreSorter<BaseTreeModel> sorter = new StoreSorter<BaseTreeModel>() {
        // @Override
        // public int compare(Store store, BaseTreeModel m1, BaseTreeModel m2, String property)
        // {
        // String t1 = m1.get("type");
        // String t2 = m2.get("type");
        // // /*
        // // * if (m1Folder && !m2Folder) { # return -1; # } else if (!m1Folder && m2Folder) { # return 1; # }
        // // */
        // if (t1.compareTo(t2) == 0)
        // {
        // return super.compare(store, m1, m2, "label");
        // }
        // return -1;
        // }
        // };
        // TODO Ne fonctionne pas ????
        // treeStore.setStoreSorter(sorter);
    }

    public TreeStore<BaseTreeModel> getTreeStore()
    {
        return treeStore;
    }

    public void setTreeStore(TreeStore<BaseTreeModel> treeStore)
    {
        this.treeStore = treeStore;
    }

    public ProjectModelData getCurrentProject()
    {
        return currentProject;
    }

    public void setCurrentProject(ProjectModelData currentProject)
    {
        this.currentProject = currentProject;
    }

    public BaseTreeModel getItemExpand()
    {
        return itemExpand;
    }

    public void setItemExpand(BaseTreeModel itemExpand)
    {
        this.itemExpand = itemExpand;
    }

    public ListStore<TagCategoryModelData> getListTagCategories()
    {
        return listCategories;
    }

    public void setListCategories(ListStore<TagCategoryModelData> listCategories)
    {
        this.listCategories = listCategories;
    }

    public void initTree()
    {
        this.treeStore.removeAll();

        // on insert le premier element
        root = new BaseTreeModel();
        root.set("label", "tags");
        root.set("id", Long.valueOf(-1));
        root.set("type", "tags");
        this.treeStore.add(root, true);
    }

    public boolean isRootItem(BaseTreeModel tI)
    {
        String type = tI.get("type");
        return tI == null ? false : type.compareTo("tags") == 0;
    }

    public boolean isTagItem(BaseTreeModel tI)
    {
        String type = tI.get("type");
        return tI == null ? false : type.compareTo("tag") == 0;
    }

    public boolean isTagCategoryItem(BaseTreeModel tI)
    {
        String type = tI.get("type");
        return tI == null ? false : type.compareTo("tagcategory") == 0;
    }

    public void populateItem(BaseTreeModel currentItem, EventType signal)
    {
        populateItem(currentItem, false, signal);
    }

    public void populateItemRecursive(BaseTreeModel currentItem, EventType signal)
    {
        populateItem(currentItem, true, signal);
    }

    public void populateItem(BaseTreeModel currentItem, boolean recursive, EventType signal)
    {
        /*
         * if (itemExpand == null) { itemExpand = currentItem; return; }
         * 
         * itemExpand = null;
         */
        if (currentItem == null)
        {
            return;
        }

        if (!currentItem.isLeaf()) // si on ne l a pas deja remplis
        {
            return;
        }
        if (isRootItem(currentItem)) // si c est le root
        {
            populateTree(currentItem, true, recursive, signal);
            // this.itemCurrent = currentItem;
        }
        else
        {
            if (isTagCategoryItem(currentItem))// si c est une category on populate
            {
                populateTree(currentItem, false, recursive, signal);
                // this.itemCurrent = currentItem;
            }
            else
            {
                // c est un tag... pas normal :)
            }
        }
    }

    private void populateTree(BaseTreeModel currentModel, boolean isRoot, boolean isRecursive, EventType signal)
    {
        if (this.currentProject == null) // project selectionne
        {
            return;
        }
        // TODO REMOVE String urlProject = Urls.urlProject;
        String urlTagCategories = "";
        String urlTags = "";
        if (isRoot)
        {
            OrderBy orderBy = new OrderBy(RecordModelData.NAME_FIELD);
            urlTagCategories = Urls.urlTagCat + "?" + orderBy.toString();
            urlTags = "";
        }
        else
        {
            long currentCategory = (Long) currentModel.get("id");
            urlTagCategories = Urls.urlTagCat + "/" + currentCategory + "/children" + "?orderBy=[\"name\"]";
            urlTags = Urls.urlTagCat + "/" + currentCategory + "/" + Urls.urlTags + "?orderBy=[\"name\"]";
        }
        IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
        /*
         * if (urlConstituents.length() != 0) { try { requestHandler.handleRequest(Method.GET, urlConstituents, null,
         * new ConstituentsCallBackTreeTag(this, currentModel)); } catch (Exception e) { // TODO : Erreur } }
         */
        try
        {
            requestHandler.handleRequest(Method.GET, urlTagCategories, null, new CategoriesCallBackTreeTag(this,
                    currentModel, urlTags, (isRecursive ? currentModel : null), signal));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public String getHook(BaseTreeModel baseTreeModel, BaseTreeModel item)
    {
        String hook = baseTreeModel.get("label");
        BaseTreeModel baseTreeParent = item;
        Long idLong = item.get("id");
        String id = idLong.toString();
        while (!id.equals("-1"))
        {
            hook = baseTreeParent.get("label") + "_" + hook;
            baseTreeParent = this.treeStore.getParent(baseTreeParent);
            Long idParent = baseTreeParent.get("id");
            id = idParent.toString();
        }
        return hook;
    }

    public List<Long> getChildrenCategoriesId(String idCategory)
    {
        List<Long> lst = new ArrayList<Long>();
        Long id = null;
        if (!"-1".equals(idCategory))
        {
            id = Long.decode(idCategory);
            lst.add(id);
        }
        else
        {
            for (TagCategoryModelData md : this.listCategories.getModels())
            {
                lst.add(md.getId());
            }
            return lst;
        }
        List<TagCategoryModelData> children = this.listCategories.findModels("parent", id);
        for (TagCategoryModelData child : children)
        {
            lst.addAll(getChildrenCategoriesId(child.getId().toString()));
        }
        return lst;
    }

    public String getChildrenTagIdsRecursive(String idCategory)
    {
        Long id = Long.decode(idCategory);

        BaseTreeModel md = this.treeStore.findModel("id", id);
        if (md == null)
        {
            List<BaseTreeModel> c = this.treeStore.findModels("id", id);
            if (c.size() == 0)
            {
                for (BaseTreeModel m : treeStore.getModels())
                {
                    System.out.println("ID: " + m.get("id"));
                }
            }
        }
        else
        {
            String ret = TagCategoryModelData.getTagsString(md);

            for (BaseTreeModel child : treeStore.getChildren(md))
            {
                if ("tagcategory".equals(child.get("type")))
                {
                    String childTags = getChildrenTagIdsRecursive(child.get("id").toString());
                    if (!childTags.equals(""))
                    {
                        ret = ret + "," + childTags;
                    }
                }
            }
            return ret;
        }
        return "";
    }
}
