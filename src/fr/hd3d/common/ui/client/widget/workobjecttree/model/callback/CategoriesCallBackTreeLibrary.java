package fr.hd3d.common.ui.client.widget.workobjecttree.model.callback;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.data.BaseTreeModel;

import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.widget.workobjecttree.model.TreeLibraryModel;


public class CategoriesCallBackTreeLibrary extends BaseCallback
{
    private final TreeLibraryModel treeLibraryModel;
    private final BaseTreeModel modelData;
    private final String urlConstituents;

    public CategoriesCallBackTreeLibrary(TreeLibraryModel _treeLibraryModel, BaseTreeModel _modelData,
            String _urlConstituents)
    {
        this.treeLibraryModel = _treeLibraryModel;
        this.modelData = _modelData;
        this.urlConstituents = _urlConstituents;
    }

    @Override
    public void onSuccess(Request request, Response response)
    {
    // JsonRepresentation

    }
}
