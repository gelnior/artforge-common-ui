package fr.hd3d.common.ui.client.widget.workobjecttree.model.callback;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.data.BaseTreeModel;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.TreeStore;
import com.extjs.gxt.ui.client.widget.Info;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.widget.workobjecttree.Actions;
import fr.hd3d.common.ui.client.widget.workobjecttree.events.TreeEvents;


public class PutCallback extends BaseCallback
{
    private final TreeStore<BaseTreeModel> treeStore;
    private final BaseTreeModel item;
    private final String value;

    public PutCallback(TreeStore<BaseTreeModel> treeStore, BaseTreeModel item, String value)
    {
        this.treeStore = treeStore;
        this.item = item;
        this.value = value;
    }

    @Override
    public void onSuccess(Request request, Response response)
    {
        Info.display("Save Result", "The object have been correctly modified");
        item.set("label", value);
        treeStore.update(item);

        String type = item.get("type");
        if ("category".equals(type))
        {
            EventDispatcher.forwardEvent(new AppEvent(TreeEvents.REFRESH_TREE_LIBRARY_CATEGORIES));
        }
        else if ("tag".equals(item.get("type")))
        {
            EventDispatcher.forwardEvent(new AppEvent(TreeEvents.REFRESH_TREE_TAG_CATEGORIES));
        }
        else if ("tagcategory".equals(item.get("type")))
        {
            EventDispatcher.forwardEvent(new AppEvent(TreeEvents.REFRESH_TREE_TAG_CATEGORIES));
        }
        else
        {
            EventDispatcher.forwardEvent(new AppEvent(TreeEvents.REFRESH_TREE_TEMPORAL_SEQUENCES));
        }

        AppEvent event = new AppEvent(TreeEvents.REFRESH_VIEWS);
        event.setData("ACTION", Actions.REFRESH_AFTER_PUT);
        event.setData("TYPE", "SELECTED_TAB");
        EventDispatcher.forwardEvent(event);
    }
}
