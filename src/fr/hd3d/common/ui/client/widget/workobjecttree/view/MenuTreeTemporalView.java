package fr.hd3d.common.ui.client.widget.workobjecttree.view;

import com.extjs.gxt.ui.client.Style.Scroll;
import com.extjs.gxt.ui.client.data.BaseTreeModel;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.KeyListener;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MenuEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.layout.BorderLayout;
import com.extjs.gxt.ui.client.widget.layout.FormData;
import com.extjs.gxt.ui.client.widget.menu.Menu;
import com.extjs.gxt.ui.client.widget.menu.MenuItem;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.widget.workobjecttree.events.TreeEvents;
import fr.hd3d.common.ui.client.widget.workobjecttree.view.listeners.ViewElementTemporalListener;


public class MenuTreeTemporalView extends Menu
{
    final static int viewDetails = 0;
    final static int addSequenceNode = 1;
    final static int addShotNode = 2;
    final static int addSubSequenceNode = 3;
    final static int renameSequenceNode = 4;
    final static int deleteSequenceNode = 5;
    final static int cloneShotNode = 6;
    final static int renameShotNode = 7;
    final static int deleteShotNode = 8;
    final static int viewCollection = 9;
    final static int refreshSequenceNode = 10;

    final String[] titres = { "View details", "New temporal element", "New shot", "New sub-temporal element", "Rename",
            "Delete", "Clone shot", "Rename", "Delete", "View collection", "Refresh" };

    BaseTreeModel item = null;

    public MenuTreeTemporalView(BaseTreeModel item)
    {

        super();
        this.setWidth(190);
        this.item = item;

        MenuItem view = new MenuItem();
        view.setText(titres[viewDetails]);
        view.setIconStyle("view-details");

        MenuItem view2 = new MenuItem();
        view2.setText(titres[viewCollection]);
        view2.setIconStyle("view-details");

        view.addSelectionListener(new ViewElementTemporalListener<MenuEvent>(true));
        view2.addSelectionListener(new ViewElementTemporalListener<MenuEvent>(true));

        MenuItem addSequenceNodeItem = new MenuItem();
        addSequenceNodeItem.setText(titres[addSequenceNode]);
        addSequenceNodeItem.setIconStyle("add-sub-category");
        addSequenceNodeItem.addSelectionListener(new SelectionListener<MenuEvent>() {
            @Override
            public void componentSelected(MenuEvent ce)
            {
                displayForm(addSequenceNode);
            }
        });
        MenuItem addShotNodeItem = new MenuItem();
        addShotNodeItem.setText(titres[addShotNode]);
        addShotNodeItem.setIconStyle("shot");
        addShotNodeItem.addSelectionListener(new SelectionListener<MenuEvent>() {
            @Override
            public void componentSelected(MenuEvent ce)
            {
                displayForm(addShotNode);
            }
        });
        MenuItem addSubSequenceNodeItem = new MenuItem();
        addSubSequenceNodeItem.setText(titres[addSubSequenceNode]);
        addSubSequenceNodeItem.setIconStyle("add-sub-category");
        addSubSequenceNodeItem.addSelectionListener(new SelectionListener<MenuEvent>() {
            @Override
            public void componentSelected(MenuEvent ce)
            {
                displayForm(addSubSequenceNode);
            }
        });
        MenuItem renameSequenceNodeItem = new MenuItem();
        renameSequenceNodeItem.setText(titres[renameSequenceNode]);
        renameSequenceNodeItem.setIconStyle("rename");
        renameSequenceNodeItem.addSelectionListener(new SelectionListener<MenuEvent>() {
            @Override
            public void componentSelected(MenuEvent ce)
            {
                displayForm(renameSequenceNode);
            }
        });
        MenuItem deleteSequenceNodeItem = new MenuItem();
        deleteSequenceNodeItem.setText(titres[deleteSequenceNode]);
        deleteSequenceNodeItem.setIconStyle("delete");
        deleteSequenceNodeItem.addSelectionListener(new SelectionListener<MenuEvent>() {
            @Override
            public void componentSelected(MenuEvent ce)
            {
                deleteActionDialog(deleteSequenceNode);
            }
        });
        // deleteSequenceNodeItem.setEnabled(false);

        MenuItem cloneShotNodeItem = new MenuItem();
        // cloneShotNodeItem.setText(titres[cloneShotNode]);
        cloneShotNodeItem.setIconStyle("icon-add");
        cloneShotNodeItem.addSelectionListener(new SelectionListener<MenuEvent>() {
            @Override
            public void componentSelected(MenuEvent ce)
            {
                // displayForm(cloneShotNode);
            }
        });
        MenuItem renameShotNodeItem = new MenuItem();
        renameShotNodeItem.setText(titres[renameShotNode]);
        renameShotNodeItem.setIconStyle("rename");
        renameShotNodeItem.addSelectionListener(new SelectionListener<MenuEvent>() {
            @Override
            public void componentSelected(MenuEvent ce)
            {
                displayForm(renameShotNode);
            }
        });
        MenuItem deleteShotNodeItem = new MenuItem();
        deleteShotNodeItem.setText(titres[deleteShotNode]);
        deleteShotNodeItem.setIconStyle("delete");
        deleteShotNodeItem.addSelectionListener(new SelectionListener<MenuEvent>() {
            @Override
            public void componentSelected(MenuEvent ce)
            {
                deleteActionDialog(deleteShotNode);
            }
        });
        // deleteShotNodeItem.setEnabled(false);

        MenuItem refreshSequenceNodeItem = new MenuItem();
        refreshSequenceNodeItem.setText(titres[refreshSequenceNode]);
        refreshSequenceNodeItem.setIconStyle("refresh");

        final BaseTreeModel itemSel = item;

        refreshSequenceNodeItem.addSelectionListener(new SelectionListener<MenuEvent>() {

            @Override
            public void componentSelected(MenuEvent ce)
            {
                AppEvent event1 = new AppEvent(TreeEvents.REFRESH_TEMPORAL_NODE);
                event1.setData("ITEM", itemSel);
                EventDispatcher.forwardEvent(event1);
            }
        });

        String type = ((ModelData) item).get("type");
        if (type.compareTo("temporal") == 0)
        {
            // TODO
            // contextMenu.add(view);
            this.add(addSequenceNodeItem);
            this.add(refreshSequenceNodeItem);
        }
        else if (type.compareTo("sequence") == 0)
        {
            this.add(view2);
            this.add(addShotNodeItem);
            this.add(addSubSequenceNodeItem);
            this.add(renameSequenceNodeItem);
            this.add(deleteSequenceNodeItem);
            this.add(refreshSequenceNodeItem);
        }
        else
        {
            // TODO
            this.add(view);
            view.setEnabled(true);
            // this.add(cloneShotNodeItem);
            this.add(renameShotNodeItem);
            this.add(deleteShotNodeItem);
        }
    }

    private void displayForm(final int action)
    {
        final Window w = new Window();
        w.setResizable(true);
        w.setWidth(305);
        w.setHeight(105);
        w.setMonitorWindowResize(true);
        w.setModal(true);
        w.setHeaderVisible(true);
        w.setLayout(new BorderLayout());
        w.setHeading(titres[action]);
        w.setPlain(true);

        w.setPosition(211, 350);

        final FormPanel form2 = new FormPanel();
        form2.setHeaderVisible(false);
        form2.setBorders(false);
        form2.setBodyBorder(false);
        form2.setWidth(300);
        form2.setPadding(5);

        final TextField<String> field = new TextField<String>();
        field.setFieldLabel("Name");
        form2.add(field, new FormData("95%"));

        field.focus();

        switch (action)
        {
            case renameSequenceNode:
                field.setValue((String) item.get("label"));
                break;
            case renameShotNode:
                field.setValue((String) item.get("label"));
                break;
        }

        Button saveB = new Button("Save");
        w.addButton(saveB);

        field.addKeyListener(new KeyListener() {
            @Override
            public void handleEvent(ComponentEvent event)
            {
                if (event.getType() == Events.KeyPress && event.getKeyCode() == 13)
                {
                    String value = field.getValue();
                    if (value != null && value.length() != 0)
                    {
                        EventType command = null;
                        if (action == addSequenceNode)
                        {
                            command = TreeEvents.ADD_SEQUENCE_NODE;
                        }
                        else if (action == addShotNode)
                        {
                            command = TreeEvents.ADD_SHOT_NODE;
                        }
                        else if (action == addSubSequenceNode)
                        {
                            command = TreeEvents.ADD_SEQUENCE_NODE;
                        }
                        else if (action == renameSequenceNode)
                        {
                            command = TreeEvents.RENAME_SEQUENCE_NODE;
                        }
                        else if (action == cloneShotNode)
                        {
                            // cloneShotNode(item,value);
                        }
                        else if (action == renameShotNode)
                        {
                            command = TreeEvents.RENAME_SHOT_NODE;
                        }

                        AppEvent event1 = new AppEvent(TreeEvents.ACTION_IF_NOT_EXIST_IN_TEMPORAL);
                        event1.setData("VALUE", value);
                        event1.setData("ITEM", item);
                        event1.setData("ACTION", command);
                        EventDispatcher.forwardEvent(event1);
                        w.hide();
                    }
                }
            }
        });

        saveB.addListener(Events.OnClick, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                String value = field.getValue();
                if (value != null && value.length() != 0)
                {
                    EventType command = null;
                    if (action == addSequenceNode)
                    {
                        command = TreeEvents.ADD_SEQUENCE_NODE;
                    }
                    else if (action == addShotNode)
                    {
                        command = TreeEvents.ADD_SHOT_NODE;
                    }
                    else if (action == addSubSequenceNode)
                    {
                        command = TreeEvents.ADD_SEQUENCE_NODE;
                    }
                    else if (action == renameSequenceNode)
                    {
                        command = TreeEvents.RENAME_SEQUENCE_NODE;
                    }
                    else if (action == cloneShotNode)
                    {
                        // cloneShotNode(item,value);
                    }
                    else if (action == renameShotNode)
                    {
                        command = TreeEvents.RENAME_SHOT_NODE;
                    }

                    AppEvent event = new AppEvent(TreeEvents.ACTION_IF_NOT_EXIST_IN_TEMPORAL);
                    event.setData("VALUE", value);
                    event.setData("ITEM", item);
                    event.setData("ACTION", command);
                    EventDispatcher.forwardEvent(event);
                    w.hide();
                }
            }
        });
        Button closeB = new Button("Cancel");
        w.addButton(closeB);
        closeB.addListener(Events.OnClick, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                w.hide();
            }
        });
        w.add(form2);
        w.show();
    }

    private void createEvent(BaseTreeModel item, String value, EventType treeEvent)
    {

        AppEvent event = new AppEvent(treeEvent);
        event.setData("VALUE", value);
        event.setData("ITEM", item);
        EventDispatcher.forwardEvent(event);
    }

    private void deleteActionDialog(final int action)
    {

        final Dialog simple = new Dialog();
        simple.setHeading("Confirmation");
        simple.setButtons(Dialog.YESNO);
        simple.setBodyStyleName("pad-text");
        simple.setModal(true);
        simple.addText("Are you sure to delete : " + item.get("label") + " ?");
        simple.setScrollMode(Scroll.AUTO);
        simple.setHideOnButtonClick(true);

        simple.setPosition(211, 350);

        Button okButton = (Button) simple.getButtonBar().getItemByItemId(Dialog.YES);
        okButton.addListener(Events.Select, new SelectionListener<ComponentEvent>() {
            @Override
            public void componentSelected(ComponentEvent ce)
            {
                deleteAction(action);
            }
        });
        simple.show();
    }

    private void deleteAction(int action)
    {
        switch (action)
        {
            case deleteSequenceNode:
                createEvent(item, null, TreeEvents.DELETE_SEQUENCE_NODE);
                break;
            case deleteShotNode:
                createEvent(item, null, TreeEvents.DELETE_SHOT_NODE);
                break;
        }
    }
}
