package fr.hd3d.common.ui.client.widget.workobjecttree.model.callback;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.data.BaseTreeModel;
import com.extjs.gxt.ui.client.event.EventType;

import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.widget.workobjecttree.model.TreeTagModel;


public class CategoriesCallBackTreeTag extends BaseCallback
{
    private final TreeTagModel treeTagModel;
    private final BaseTreeModel currentItem;
    private final String urlTags;
    private final BaseTreeModel rootOfRecursion;
    private final EventType signal;

    public CategoriesCallBackTreeTag(TreeTagModel _treeTagModel, BaseTreeModel _currentItem, String _urlTags,
            BaseTreeModel _rootOfRecursion, EventType _signal)
    {
        this.treeTagModel = _treeTagModel;
        this.currentItem = _currentItem;
        this.urlTags = _urlTags;
        this.rootOfRecursion = _rootOfRecursion;
        this.signal = _signal;

        this.treeTagModel.increaseLoadingChildren(this.rootOfRecursion);
    }

    @Override
    public void onSuccess(Request request, Response response)
    {
    // JsonRepresentation representation = response.getEntityAsJson();
    // System.out.println("Debug RESPONSE: " + representation.getText());
    // ModelType tagCategoriesModelType = TagCategoryModelData.getModelType();
    // TagCategoryModelData loadConfigTagCategoriesModelData = new TagCategoryModelData();
    // TagCategoryReader jsonReader = new TagCategoryReader(tagCategoriesModelType);
    // ListLoadResult<TagCategoryModelData> result = jsonReader.read(loadConfigTagCategoriesModelData, representation
    // .getText());
    // List<TagCategoryModelData> lResult = result.getData();
    // TreeStore<BaseTreeModel> treeStore = this.treeTagModel.getTreeStore();
    // List<BaseTreeModel> lTreeModel = new ArrayList<BaseTreeModel>();
    // if (this.rootOfRecursion == null)
    // {
    // for (int i = 0; i < lResult.size(); i++)
    // {
    // TagCategoryModelData cmd = lResult.get(i);
    // BaseTreeModel baseTreeModel = new BaseTreeModel();
    // baseTreeModel.set("label", cmd.getName());
    // baseTreeModel.set("id", cmd.getId());
    // System.out.print("00 => " + cmd.getName() + " " + cmd.get("boundTags"));
    // baseTreeModel.set("boundTags", cmd.get("boundTags"));
    // baseTreeModel.set("type", "tagcategory");
    // lTreeModel.add(baseTreeModel);
    // }
    // }
    // else
    // {
    // for (int i = 0; i < lResult.size(); i++)
    // {
    // TagCategoryModelData cmd = lResult.get(i);
    // BaseTreeModel baseTreeModel = new BaseTreeModel();
    // baseTreeModel.set("label", cmd.getName());
    // baseTreeModel.set("id", cmd.getId());
    // System.out.print("11 => " + cmd.getName() + " " + cmd.get("boundTags"));
    // baseTreeModel.set("boundTags", cmd.get("boundTags"));
    // baseTreeModel.set("type", "tagcategory");
    // lTreeModel.add(baseTreeModel);
    // try
    // {
    // String urlTagCategories = Urls.urlTagCat + "/" + cmd.getId() + "/children" + "?orderBy=[\"name\"]";
    // String urlTags = Urls.urlTagCat + "/" + cmd.getId() + "/" + Urls.urlTags + "?orderBy=[\"name\"]";
    //
    // IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
    // requestHandler.handleRequest(Method.GET, urlTagCategories, null, new CategoriesCallBackTreeTag(
    // this.treeTagModel, baseTreeModel, urlTags, this.rootOfRecursion, this.signal));
    // }
    // catch (Exception e)
    // {}
    // }
    // }
    // this.treeTagModel.decreaseLoadingChildren(this.rootOfRecursion);
    // IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
    // if (this.urlTags.length() != 0)
    // {
    // try
    // {
    // requestHandler.handleRequest(Method.GET, this.urlTags, null, new TagsCallBackTreeTag(this.treeTagModel,
    // this.currentItem, lTreeModel, this.rootOfRecursion, this.signal));
    // }
    // catch (Exception e)
    // {
    // e.printStackTrace();
    // }
    // }
    // else
    // {
    // treeStore.removeAll(currentItem);
    // treeStore.add(currentItem, lTreeModel, true);
    // AppEvent event;
    // if (this.rootOfRecursion != null)
    // {
    // if (this.treeTagModel.getLoadingChildrenNumber(this.rootOfRecursion) != 0)
    // {
    // return;
    // }
    // // end of recursion
    // event = new AppEvent(this.signal, this.rootOfRecursion);
    // }
    // else
    // event = new AppEvent(this.signal, currentItem);
    // EventDispatcher.forwardEvent(event);
    // }
    }
}
