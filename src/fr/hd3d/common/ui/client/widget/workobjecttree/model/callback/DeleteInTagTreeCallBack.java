package fr.hd3d.common.ui.client.widget.workobjecttree.model.callback;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.data.BaseTreeModel;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.TreeStore;
import com.extjs.gxt.ui.client.widget.Info;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.widget.workobjecttree.Actions;
import fr.hd3d.common.ui.client.widget.workobjecttree.events.TreeEvents;


public class DeleteInTagTreeCallBack extends BaseCallback
{
    private final TreeStore<BaseTreeModel> treeStore;
    private final BaseTreeModel item;

    // private String value;

    public DeleteInTagTreeCallBack(TreeStore<BaseTreeModel> treeStore, BaseTreeModel item)
    {
        this.treeStore = treeStore;
        this.item = item;
    }

    @Override
    public void onSuccess(Request request, Response response)
    {
        Info.display("Delete Result", "The object have been correctly deleted");

        treeStore.remove(treeStore.getParent(item), item);

        // Update Explorator and identity sheet

        AppEvent event = new AppEvent(TreeEvents.REFRESH_VIEWS);
        event.setData("ACTION", Actions.REFRESH_AFTER_REMOVE);

        String type = item.get("type");
        if ("constituent".equals(type))
        {
            event.setData("TYPE", "Constituent");
        }
        else if ("shot".equals(type))
        {
            event.setData("TYPE", "Temporal");
        }
        EventDispatcher.forwardEvent(event);

        // Update trees

        if ("category".equals(type))
        {
            EventDispatcher.forwardEvent(new AppEvent(TreeEvents.REFRESH_TREE_TAG_CATEGORIES));
        }
        else if ("sequence".equals(type))
        {
            EventDispatcher.forwardEvent(new AppEvent(TreeEvents.REFRESH_TREE_TEMPORAL_SEQUENCES));
        }
    }
}
