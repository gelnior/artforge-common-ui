package fr.hd3d.common.ui.client.widget.workobjecttree.view.listeners;

import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.TreePanelEvent;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.widget.workobjecttree.events.TreeEvents;


/**
 * Event : Handle click on "new group" tool bar item . <br>
 * Effect : Create a new group in grid.
 * 
 * @author HD3D
 * */
public class ExpandTreeTagListener implements Listener<TreePanelEvent<?>>
{
    public void handleEvent(TreePanelEvent<?> te)
    {
        AppEvent event = new AppEvent(TreeEvents.TREE_TAG_EXPAND, te.getItem());
        EventDispatcher.forwardEvent(event);
    }
}
