package fr.hd3d.common.ui.client.widget.workobjecttree;

public class Actions
{
    public final static int REFRESH_AFTER_REMOVE = 0;
    public final static int REFRESH_AFTER_PUT = 1;
    public final static int REFRESH_AFTER_PUT_SHOT = 2;
    public final static int REFRESH_AFTER_POST = 3;
}
