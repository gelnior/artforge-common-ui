package fr.hd3d.common.ui.client.widget.workobjecttree.model.callback;

import org.restlet.client.Request;
import org.restlet.client.Response;

import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.widget.workobjecttree.model.TreeTagModel;


public class AllTagCategoriesCallBack extends BaseCallback
{
    private final TreeTagModel treeTagModel;

    public AllTagCategoriesCallBack(TreeTagModel _treeTagModel)
    {
        this.treeTagModel = _treeTagModel;

    }

    @Override
    public void onSuccess(Request request, Response response)
    {
    // JsonRepresentation representation = response.getEntityAsJson();
    // ModelType tagCategoriesModelType = TagCategoryModelData.getModelType();
    //
    // TagCategoryModelData loadConfigTagCategoriesModelData = new TagCategoryModelData();
    // TagCategoryReader jsonReader = new TagCategoryReader(tagCategoriesModelType);
    //
    // ListLoadResult<TagCategoryModelData> result = jsonReader.read(loadConfigTagCategoriesModelData, representation
    // .getText());
    // ListStore<TagCategoryModelData> treeStore = this.treeTagModel.getListTagCategories();
    // treeStore.removeAll();
    // treeStore.add(result.getData());
    }
}
