package fr.hd3d.common.ui.client.widget.workobjecttree;

import java.util.List;

import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.modeldata.reader.RecordReader;
import fr.hd3d.common.ui.client.modeldata.reader.SequenceReader;
import fr.hd3d.common.ui.client.service.proxy.TreeServicesProxy;
import fr.hd3d.common.ui.client.widget.basetree.BaseTree;
import fr.hd3d.common.ui.client.widget.basetree.BaseTreeDataModel;


/**
 * Shot tree allows user to browse shots and sequences of a given project.
 * 
 * @author HD3D
 */
public class SequenceTree extends BaseTree<ShotModelData, SequenceModelData>
{
    /** Constant strings to display : dialog messages, button label... */
    public static final CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    /** Listener used to dispatch events when category or constituent is clicked. */
    protected SequenceChangedListener listener = new SequenceChangedListener(this);

    /**
     * Default constructor, set reader and tree root title.
     */
    public SequenceTree()
    {
        this(CONSTANTS.All());
    }

    /**
     * Default constructor, set reader and tree root title with <i>rootName</i>.
     * 
     * @param rootName
     *            Tree root name.
     */
    public SequenceTree(String rootName)
    {
        super(rootName, null, new SequenceReader());
        this.tree.setIconProvider(new WorkObjectTreeIconProvider(model.getStore()));
        this.setSelectionChangedListener();
    }

    /**
     * Initialize tree model with proxy and specific data loader.
     */
    @Override
    protected void setModel(String rootName, IReader<ShotModelData> leafReader, IReader<SequenceModelData> parentReader)
    {
        TreeServicesProxy<RecordModelData> proxy = new TreeServicesProxy<RecordModelData>(new RecordReader());
        this.model = new BaseTreeDataModel<ShotModelData, SequenceModelData>(new SequenceTreeLoader(proxy));
        this.model.setRootName(rootName);
    }

    /**
     * Add a selection changed listener to send events to controllers when a constituent or category has been selected.
     */
    private void setSelectionChangedListener()
    {
        this.tree.getSelectionModel().addSelectionChangedListener(listener);
    }

    /**
     * When a sequence is clicked, the appropriate event is forwarded to controllers, locally or generally.
     * 
     * @param sequences
     *            The selected sequences.
     */
    public void onSequenceClicked(List<SequenceModelData> sequences)
    {
        AppEvent event = new AppEvent(CommonEvents.SEQUENCES_SELECTED);
        event.setData(sequences);
        event.setData("tree", "sequence");
        this.forwardEvent(event);
    }

}
