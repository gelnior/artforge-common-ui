package fr.hd3d.common.ui.client.widget.workobjecttree.view;

import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.widget.workobjecttree.controller.TreeTagController;
import fr.hd3d.common.ui.client.widget.workobjecttree.model.TreeTagModel;


public class TagsTreeView extends ContentPanel
{
    private TreeTagView _treeTag;
    private final FitLayout layoutTree = new FitLayout();

    public TreeTagView getTreeTag()
    {
        return _treeTag;
    }

    public void deselectAll()
    {
        if (_treeTag.getTree() != null)
        {
            _treeTag.getTree().getSelectionModel().deselectAll();
        }
    }

    public void setTreeTag(TreeTagView treeTag)
    {
        this._treeTag = treeTag;
    }

    public TagsTreeView()
    {
        super();
        final TreeTagModel treeTagModel = new TreeTagModel();

        _treeTag = new TreeTagView(treeTagModel, true);

        final TreeTagController treeTagController = new TreeTagController(_treeTag, treeTagModel);

        EventDispatcher dispatcher = EventDispatcher.get();
        dispatcher.addController(treeTagController);

        this.setLayout(layoutTree);
        this.setSize("100%", "100%");

        /*
         * BorderLayoutData northData = new BorderLayoutData(LayoutRegion.NORTH); northData.setSplit(true);
         * northData.setMargins(new Margins(5, 0, 0, 0));
         * 
         * BorderLayoutData southData = new BorderLayoutData(LayoutRegion.CENTER); southData.setSplit(true);
         * southData.setMargins(new Margins(5, 0, 0, 0));
         */

        this.add(this._treeTag);

        this.setBorders(false);
        this.setHeaderVisible(false);
        this.setFrame(false);
        this.setBodyBorder(false);
        this.setStyleAttribute("backgroundColor", "white");
        this.setBodyStyle("fontSize: 12px;");

    }
}
