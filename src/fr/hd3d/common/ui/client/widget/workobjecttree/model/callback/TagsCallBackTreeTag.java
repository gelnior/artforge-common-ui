package fr.hd3d.common.ui.client.widget.workobjecttree.model.callback;

import java.util.List;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.data.BaseTreeModel;
import com.extjs.gxt.ui.client.event.EventType;

import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.widget.workobjecttree.model.TreeTagModel;


public class TagsCallBackTreeTag extends BaseCallback
{
    private final TreeTagModel _treeTagModel;
    private final BaseTreeModel _currentItem;
    private final List<BaseTreeModel> _lst;
    private final BaseTreeModel _rootOfRecursion;
    private final EventType _signal;

    public TagsCallBackTreeTag(TreeTagModel treeTagModel, BaseTreeModel currentItem, List<BaseTreeModel> lst,
            BaseTreeModel rootOfRecursion, EventType signal)
    {
        _treeTagModel = treeTagModel;
        _currentItem = currentItem;
        _rootOfRecursion = rootOfRecursion;
        _signal = signal;
        _lst = lst;
        // GWT.log("Entering tag: [" + _currentItem.get("label") + "]", null);
        _treeTagModel.increaseLoadingChildren(_rootOfRecursion);
    }

    @Override
    public void onSuccess(Request request, Response response)
    {
    // JsonRepresentation representation = response.getEntityAsJson();
    // ModelType tagModelType = TagModelData.getModelType();
    // TagModelData loadConfigTagsModelData = new TagModelData();
    // TagReader jsonReader = new TagReader(tagModelType);
    // ListLoadResult<TagModelData> result = jsonReader.read(loadConfigTagsModelData, representation.getText());
    // List<TagModelData> lResult = result.getData();
    // TreeStore<BaseTreeModel> treeStore = _treeTagModel.getTreeStore();
    // for (int i = 0; i < lResult.size(); i++)
    // {
    // TagModelData cmd = lResult.get(i);
    // BaseTreeModel baseTreeModel = new BaseTreeModel();
    // baseTreeModel.set("label", cmd.getLabel());
    // baseTreeModel.set("id", cmd.getId());
    // baseTreeModel.set("type", "tag");
    // _lst.add(baseTreeModel);
    // }
    // treeStore.removeAll(_currentItem);
    // treeStore.add(_currentItem, _lst, true);
    //
    // _treeTagModel.decreaseLoadingChildren(_rootOfRecursion);
    // AppEvent event;
    // if (_rootOfRecursion != null)
    // {
    // if (_treeTagModel.getLoadingChildrenNumber(_rootOfRecursion) != 0)
    // {
    // return;
    // }
    // // end of recursion
    //
    // event = new AppEvent(_signal, _rootOfRecursion);
    // }
    // else
    // {
    // event = new AppEvent(_signal, _currentItem);
    // }
    // EventDispatcher.forwardEvent(event);
    }

    public boolean inBoolean(long data)
    {
        return data == 0 ? false : data == 1;
    }
}
