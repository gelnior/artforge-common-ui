package fr.hd3d.common.ui.client.widget.workobjecttree.model.callback;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.data.BaseTreeModel;

import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.widget.workobjecttree.model.TreeTemporalModel;


public class SequenceCallBackTreeSequence extends BaseCallback
{
    private final TreeTemporalModel treeSequenceModel;
    private final BaseTreeModel currentModelData;
    private final String urlShots;

    public SequenceCallBackTreeSequence(TreeTemporalModel _treeSequence, BaseTreeModel _currentModelData,
            String _urlShots)
    {

        this.treeSequenceModel = _treeSequence;
        this.currentModelData = _currentModelData;
        this.urlShots = _urlShots;
    }

    @Override
    public void onSuccess(Request request, Response response)
    {
    // JsonRepresentation representation = response.getEntityAsJson();
    // ModelType sequencesModelType = SequenceModelData.getModelType();
    //
    // SequenceModelData loadConfigSequenceModelData = new SequenceModelData();
    // SequenceReader jsonReader = new SequenceReader(sequencesModelType);
    // ListLoadResult<SequenceModelData> result = jsonReader.read(loadConfigSequenceModelData, representation
    // .getText());
    // List<SequenceModelData> lResult = result.getData();
    //
    // TreeStore<BaseTreeModel> treeStore = this.treeSequenceModel.getTreeStore();
    //
    // List<BaseTreeModel> lTreeModel = new ArrayList<BaseTreeModel>();
    //
    // for (int i = 0; i < lResult.size(); i++)
    // {
    // SequenceModelData cmd = lResult.get(i);
    //
    // BaseTreeModel baseTreeModel = new BaseTreeModel();
    // baseTreeModel.set("label", cmd.getName());
    // baseTreeModel.set("title", cmd.getTitle());
    // baseTreeModel.set("id", cmd.getId());
    // baseTreeModel.set("type", "sequence");
    // baseTreeModel.set("hook", cmd.getHook());
    //
    // lTreeModel.add(baseTreeModel);
    // }
    //
    // IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
    // if (this.urlShots.length() != 0)
    // {
    // try
    // {
    // requestHandler.handleRequest(Method.GET, this.urlShots, null, new ShotCallBackTreeSequence(
    // this.treeSequenceModel, this.currentModelData, lTreeModel));
    // }
    // catch (Exception e)
    // {
    // // TODO : Erreur
    // }
    // }
    // else
    // {
    //
    // treeStore.add(currentModelData, lTreeModel, true);
    // AppEvent event = new AppEvent(TreeEvents.SET_EXPAND_NODE_TEMPORAL, currentModelData);
    // EventDispatcher.forwardEvent(event);
    // }
    }
}
