package fr.hd3d.common.ui.client.widget.workobjecttree;

import java.util.List;
import java.util.Stack;

import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.event.LoadListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.reader.CategoryReader;
import fr.hd3d.common.ui.client.modeldata.reader.ConstituentReader;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.modeldata.reader.RecordReader;
import fr.hd3d.common.ui.client.service.proxy.TreeServicesProxy;
import fr.hd3d.common.ui.client.widget.basetree.BaseTree;
import fr.hd3d.common.ui.client.widget.basetree.BaseTreeDataModel;


/**
 * Constituent tree allows user to browse constituents and categories of a given project.
 * 
 * @author HD3D
 */
public class ConstituentTree extends BaseTree<ConstituentModelData, CategoryModelData>
{
    /** Constant strings to display : dialog messages, button label... */
    public static final CommonConstants CONSTANTS = GWT.create(CommonConstants.class);
    /** Listener used to dispatch events when category or constituent is clicked. */
    protected ConstituentChangedListener listener = new ConstituentChangedListener(this);

    public static String EVENT_DATA_CATEGORY = "CATEGORIES";
    public static String EVENT_DATA_CONSTITUENT = "CONSTITUENTS";

    /**
     * Default constructor, set reader and tree root title as "All".
     */
    public ConstituentTree()
    {
        this(CONSTANTS.All());
    }

    /**
     * Default constructor, set reader and tree root title with <i>rootName</i>.
     * 
     * @param rootName
     *            Tree root name.
     */
    public ConstituentTree(String rootName)
    {
        super(rootName, new ConstituentReader(), new CategoryReader());
        this.tree.setIconProvider(new WorkObjectTreeIconProvider(model.getStore()));
        this.setSelectionChangedListener();
    }

    /**
     * Initialize tree model with proxy and specific data loader.
     */
    @Override
    protected void setModel(String rootName, IReader<ConstituentModelData> leafReader,
            IReader<CategoryModelData> parentReader)
    {
        TreeServicesProxy<RecordModelData> proxy = new TreeServicesProxy<RecordModelData>(new RecordReader());
        this.model = new BaseTreeDataModel<ConstituentModelData, CategoryModelData>(new ConstituentTreeLoader(proxy));
        this.model.setRootName(rootName);
    }

    /**
     * Add a selection changed listener to send events to controllers when a constituent or category has been selected.
     */
    private void setSelectionChangedListener()
    {
        this.tree.getSelectionModel().addSelectionChangedListener(listener);
    }

    /**
     * When a constituent is clicked, the appropriate event is forwarded to controllers, locally or generally.
     * 
     * @param constituent
     *            The selected constituent.
     */
    public void onConstituentClicked(List<ConstituentModelData> constituent)
    {
        AppEvent event = new AppEvent(CommonEvents.CONSTITUENTS_SELECTED);
        event.setData(constituent);
        this.forwardEvent(event);
    }

    /**
     * When a category is clicked, the appropriate event is forwarded to controllers, locally or generally.
     * 
     * @param category
     *            The selected category.
     */
    public void onCategoryClicked(List<CategoryModelData> category)
    {
        AppEvent event = new AppEvent(CommonEvents.CATEGORIES_SELECTED);
        event.setData(category);
        this.forwardEvent(event);
    }

    public void selectCategory(final Stack<Long> ids)
    {
        Long id = ids.pop();

        RecordModelData category = this.getStore().findModel(Hd3dModelData.ID_FIELD, id);
        if (category != null)
        {
            if (ids.size() == 0)
            {
                this.tree.getSelectionModel().select(false, category);
            }
            else
            {
                if (this.tree.isExpanded(category))
                {
                    this.selectCategory(ids);
                }
                else
                {
                    LoadListener pathListener = new LoadListener() {
                        @Override
                        public void loaderLoad(LoadEvent le)
                        {
                            model.removeLoadListener(this);
                            selectCategory(ids);
                        }
                    };
                    this.model.addLoadListener(pathListener);
                    this.model.getLoader().loadChildren(category);
                }
            }
        }
    }

    // public void onConstituentNCategoryClicked(ArrayList<ConstituentModelData> constituents,
    // ArrayList<CategoryModelData> categories)
    // {
    // AppEvent event = new AppEvent(CommonEvents.CATEGORIES_N_CONSTITUENTS_SELECTED);
    // event.setData(EVENT_DATA_CATEGORY, categories);
    // event.setData(EVENT_DATA_CONSTITUENT, constituents);
    // this.forwardEvent(event);
    // }
}
