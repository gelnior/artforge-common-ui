package fr.hd3d.common.ui.client.widget.workobjecttree;

import java.util.ArrayList;

import com.extjs.gxt.ui.client.event.SelectionChangedEvent;
import com.extjs.gxt.ui.client.event.SelectionChangedListener;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;


public class SequenceChangedListener extends SelectionChangedListener<RecordModelData>
{
    private final SequenceTree tree;

    public SequenceChangedListener(SequenceTree shotTree)
    {
        this.tree = shotTree;
    }

    @Override
    public void selectionChanged(SelectionChangedEvent<RecordModelData> se)
    {
        RecordModelData firstSelection = se.getSelectedItem();

        if (firstSelection != null && firstSelection.getClassName().equals(SequenceModelData.CLASS_NAME))
        {

            ArrayList<SequenceModelData> sequenceList = new ArrayList<SequenceModelData>();
            for (RecordModelData item : se.getSelection())
            {
                if (item.getClassName().equals(SequenceModelData.CLASS_NAME))
                {
                    SequenceModelData sequence = new SequenceModelData();
                    Hd3dModelData.copy(item, sequence);
                    sequenceList.add(sequence);
                }
            }

            this.tree.onSequenceClicked(sequenceList);
        }
    }
}
