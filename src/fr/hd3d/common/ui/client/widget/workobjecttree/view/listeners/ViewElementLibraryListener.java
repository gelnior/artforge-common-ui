package fr.hd3d.common.ui.client.widget.workobjecttree.view.listeners;

import com.extjs.gxt.ui.client.event.MenuEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.widget.workobjecttree.events.TreeEvents;


/**
 * Event : Handle click on "OK" button from the rename dialog box. <br>
 * Effect : Redo last undid command.
 * 
 * @author HD3D
 * */
public class ViewElementLibraryListener<E extends MenuEvent> extends SelectionListener<E>
{
    /** Model to handle menu bar data */

    boolean displayTabPanel;

    /**
     * Default constructor.
     */
    public ViewElementLibraryListener(boolean displayTabPanel)
    {

        this.displayTabPanel = displayTabPanel;
    }

    @Override
    public void componentSelected(E ce)
    {
        AppEvent event = new AppEvent(TreeEvents.VIEW_LIBRARY_DETAILS);
        event.setData("DISPLAY_TAB_PANEL", this.displayTabPanel);
        EventDispatcher.forwardEvent(event);
    }
}
