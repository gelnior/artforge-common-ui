package fr.hd3d.common.ui.client.widget.workobjecttree;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.TreeLoadEvent;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.reader.CategoryReader;
import fr.hd3d.common.ui.client.modeldata.reader.ConstituentReader;
import fr.hd3d.common.ui.client.service.parameter.OrderBy;
import fr.hd3d.common.ui.client.service.proxy.TreeServicesProxy;
import fr.hd3d.common.ui.client.widget.basetree.BaseTreeDataLoader;


/**
 * Constituent tree loader specifies which URL should be used to load a specific node in constituent tree.
 * 
 * @author HD3D
 */
public class ConstituentTreeLoader extends BaseTreeDataLoader<ConstituentModelData, CategoryModelData>
{
    /**
     * Default constructor sets proxy, reader and default instance.
     * 
     * @param proxy
     *            The proxy needed to access to services.
     */
    public ConstituentTreeLoader(TreeServicesProxy<RecordModelData> proxy)
    {
        super(proxy, new ConstituentReader(), new CategoryReader(), new ConstituentModelData(), new CategoryModelData());
    }

    /**
     * Defines behavior to adopt when node children are successfully loaded.
     * 
     * @param loadConfig
     *            The loading configuration.
     * @param result
     *            list of children.
     */
    @Override
    protected void onLoadSuccess(Object loadConfig, List<RecordModelData> result)
    {
        if (loadConfig != null && children.contains(loadConfig))
        {
            RecordModelData parent = (RecordModelData) loadConfig;
            if (ConstituentModelData.CLASS_NAME.equals(parent.getClassName()))
            {
                TreeLoadEvent evt = new TreeLoadEvent(this, loadConfig, result);
                evt.parent = parent;
                fireEvent(Load, evt);
                children.remove(loadConfig);
            }
        }
        super.onLoadSuccess(loadConfig, result);
    }

    /**
     * Cancel the load success with null parent standard behavior.
     */
    @Override
    protected void onLoadSuccessWithNullParent(Object loadConfig, List<RecordModelData> result)
    {

    }

    /**
     * Update order by parameter because constituent name is contained in label field.
     */
    @Override
    protected void loadLeaves(RecordModelData parent)
    {
        servicesProxy.clearParameters();
        List<String> columns = new ArrayList<String>();
        columns.add(ConstituentModelData.CONSTITUENT_LABEL);
        servicesProxy.addParameter(new OrderBy(columns));

        super.loadLeaves(parent);
    }
}
