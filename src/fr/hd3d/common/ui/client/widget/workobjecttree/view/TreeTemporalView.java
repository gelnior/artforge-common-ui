package fr.hd3d.common.ui.client.widget.workobjecttree.view;

import java.util.List;

import org.restlet.client.data.Method;

import com.extjs.gxt.ui.client.Style.Scroll;
import com.extjs.gxt.ui.client.Style.SelectionMode;
import com.extjs.gxt.ui.client.data.BaseTreeModel;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.dnd.TreePanelDragSource;
import com.extjs.gxt.ui.client.dnd.TreePanelDropTarget;
import com.extjs.gxt.ui.client.dnd.DND.Feedback;
import com.extjs.gxt.ui.client.dnd.DND.Operation;
import com.extjs.gxt.ui.client.event.DNDEvent;
import com.extjs.gxt.ui.client.event.DNDListener;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.SelectionChangedEvent;
import com.extjs.gxt.ui.client.event.SelectionChangedListener;
import com.extjs.gxt.ui.client.event.TreePanelEvent;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.TreeStoreModel;
import com.extjs.gxt.ui.client.util.IconHelper;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.menu.Menu;
import com.extjs.gxt.ui.client.widget.treepanel.TreePanel;
import com.extjs.gxt.ui.client.widget.treepanel.TreePanel.TreeNode;
import com.google.gwt.user.client.Element;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.widget.workobjecttree.events.TreeEvents;
import fr.hd3d.common.ui.client.widget.workobjecttree.model.TreeTemporalModel;
import fr.hd3d.common.ui.client.widget.workobjecttree.model.callback.PutDragAndDropCallBack;
import fr.hd3d.common.ui.client.widget.workobjecttree.util.LoadHandle;
import fr.hd3d.common.ui.client.widget.workobjecttree.view.listeners.ExpandTreeTemporalListener;


public class TreeTemporalView extends ContentPanel
{

    private TreeTemporalModel treeTemporalModel = null;
    private boolean withDragAndDrop = false;
    private SelectionMode selectionMode = SelectionMode.SINGLE;
    private BaseTreeModel itemSel = null;
    private String url = "";
    private TreePanel<BaseTreeModel> tree = null;
    private LoadHandle loadHandle = null;
    private boolean bLoadingSequence = false;
    private Menu menuTreeTemporalView = null;

    public TreeTemporalView(TreeTemporalModel treeModel, boolean withDragAndDrop)
    {
        super();
        this.treeTemporalModel = treeModel;
        this.withDragAndDrop = withDragAndDrop;

        this.setId("temporal-tree");
    }

    public Menu getMenuTreeTemporalView()
    {
        return menuTreeTemporalView;
    }

    public void setMenuTreeTemporalView(Menu menuTreeTemporalView)
    {
        this.menuTreeTemporalView = menuTreeTemporalView;
    }

    public boolean isWithDragAndDrop()
    {
        return withDragAndDrop;
    }

    public void setWithDragAndDrop(boolean withDragAndDrop)
    {
        this.withDragAndDrop = withDragAndDrop;
    }

    public BaseTreeModel getItemSel()
    {
        return itemSel;
    }

    public void setItemSel()
    {
        itemSel = tree.getSelectionModel().getSelectedItem();
    }

    public TreePanel<BaseTreeModel> getTree()
    {
        return tree;
    }

    public TreeTemporalModel getTreeTemporalModel()
    {
        return treeTemporalModel;
    }

    public boolean isBLoadingSequence()
    {
        return bLoadingSequence;
    }

    public void setBLoadingSequence(boolean loadingSequence)
    {
        this.bLoadingSequence = loadingSequence;
    }

    public SelectionMode getSelectionMode()
    {
        return selectionMode;
    }

    public void setSelectionMode(SelectionMode selectionMode)
    {
        this.selectionMode = selectionMode;
    }

    public void setSelectionModeMulti()
    {
        this.selectionMode = SelectionMode.MULTI;
    }

    @Override
    protected void onRender(Element parent, int index)
    {
        super.onRender(parent, index);
        this.setLayout(new FitLayout());
        this.setScrollMode(Scroll.AUTO);
        this.setHeading("Temporal");
        this.setWidth("100%");
        this.setHeight("100%");
        this.tree = new TreePanel<BaseTreeModel>(this.treeTemporalModel.getTreeStore()) {
            @Override
            protected boolean hasChildren(BaseTreeModel model)
            {
                if (!"shot".equals(model.get("type")))
                {
                    return true;
                }
                return super.hasChildren(model);
            }
        };
        this.tree.setDisplayProperty("label");
        this.tree.setAutoLoad(true);
        this.tree.getStyle().setLeafIcon(IconHelper.createStyle("shot"));

        // this.tree.getStyle().setNodeOpenIcon(IconHelper.createPath("images/bobine.png"));
        // this.tree.getStyle().setNodeCloseIcon(IconHelper.createPath("images/bobine.png"));

        this.tree.setEnabled(false);
        this.tree.getSelectionModel().setSelectionMode(selectionMode);

        this.tree.addListener(Events.BeforeExpand, new ExpandTreeTemporalListener());

        this.tree.getSelectionModel().addSelectionChangedListener(new SelectionChangedListener<BaseTreeModel>() {
            @Override
            public void selectionChanged(SelectionChangedEvent<BaseTreeModel> se)
            {
                fireEvent(Events.SelectionChange, se);
                if (se.getSelection().size() > 0)
                {
                    AppEvent event = new AppEvent(TreeEvents.TREE_TEMPORAL_SELECTION, se.getSelection().get(0));
                    EventDispatcher.forwardEvent(event);
                    AppEvent event2 = new AppEvent(TreeEvents.TREE_TEMPORAL_SELECTION_MULTI, se.getSelection());
                    EventDispatcher.forwardEvent(event2);
                }
            }
        });

        this.tree.addListener(Events.Expand, new Listener<TreePanelEvent<?>>() {
            public void handleEvent(TreePanelEvent<?> te)
            {
                if (loadHandle != null && isTemporalItem((Hd3dModelData) te.getItem()) && bLoadingSequence == true)
                {
                    loadHandle.onLoadHandle();
                    bLoadingSequence = false;
                }
            }
        });

        tree.addListener(Events.OnClick, new Listener<TreePanelEvent<BaseTreeModel>>() {
            @SuppressWarnings("unchecked")
            public void handleEvent(TreePanelEvent<BaseTreeModel> be)
            {
                TreeNode node = be.getNode();
                if (node == null)
                {
                    tree.getSelectionModel().deselectAll();
                    setItemSel();
                    tree.setContextMenu(null);
                }
            }
        });

        // binder = new TreeBinder<BaseModelData>(tree, treeTemporalModel.getTreeStore()) {
        // @Override
        // public boolean hasChildren(BaseModelData parent)
        // {
        // if ("temporal".equals(parent.get("type")) || "sequence".equals(parent.get("type")))
        // {
        // return true;
        // }
        // return super.hasChildren(parent);
        // }
        // };
        // binder.setDisplayProperty("label");
        // binder.setAutoLoad(true);
        // binder.init();

        if (withDragAndDrop)
        {
            /*
             * DNDListener listener = new DNDListener() {
             * 
             * @Override public void dragDrop(DNDEvent e) { itemSel.getModel().set("parentLabel",
             * itemSel.getParentItem().getModel().get("label")); super.dragDrop(e); }
             * 
             * @Override public void dragStart(DNDEvent e) { TreeItem item = tree.findItem(e.getTarget()); if (item !=
             * null && item == tree.getRootItem().getItem(0) || !((Object) item.getModel().get("type")).equals("shot"))
             * { e.doit = false; e.status.setStatus(false); return; } super.dragStart(e); } };
             * 
             * TreeDragSource treeDragSource = new TreeDragSource(binder); treeDragSource.setGroup("shot");
             * treeDragSource.addDNDListener(listener);
             */

            TreePanelDragSource treeDragSource = new TreePanelDragSource(this.tree) {
                // @Override
                // public void onDragDrop(DNDEvent e)
                // {}
            };

            treeDragSource.addDNDListener(new DNDListener() {
                @Override
                public void dragStart(DNDEvent e)
                {
                    ModelData sel = tree.getSelectionModel().getSelectedItem();
                    if (sel != null && sel == tree.getStore().getRootItems().get(0))
                    {
                        e.setCancelled(true);
                        e.getStatus().setStatus(false);
                        return;
                    }

                    super.dragStart(e);
                }
            });

            treeDragSource.setGroup("shot");

            // Drop
            TreePanelDropTarget treeDropTarget = new TreePanelDropTarget(tree) {

                @SuppressWarnings("unchecked")
                @Override
                protected void showFeedback(DNDEvent event)
                {
                    super.showFeedback(event);
                    // TODO temporarily only to allow dropping items on leafs
                    // final TreeNode item = (() tree).findItem(event.getTarget());

                    final TreeNode item = tree.findNode(event.getTarget());

                    if (item == null
                            || (!(item.getModel()).get("type").equals("shot") && !(item.getModel()).get("type").equals(
                                    "sequence")))
                    {
                        event.getStatus().setStatus(false);
                        return;
                    }

                    boolean append = feedback == Feedback.APPEND || feedback == Feedback.BOTH;
                    if (append)
                    {
                        handleAppend(event, item);
                    }
                }

                @SuppressWarnings("unchecked")
                @Override
                protected void handleAppendDrop(DNDEvent event, TreeNode item)
                {

                    BaseTreeModel btm = (BaseTreeModel) item.getModel();

                    AppEvent event2 = new AppEvent(TreeEvents.DRAG_DROP_CASTING);
                    event2.setData("EVENT", event);
                    event2.setData("SHOT_ITEM", btm);
                    event2.setData("UPDATE", true);
                    EventDispatcher.forwardEvent(event2);

                    // breakdownSession.gridCastingConstituentData.UpdateCastingShotConstituentsDataBase(event, item,
                    // true);
                }
            };
            treeDropTarget.setGroup("constituent");
            treeDropTarget.setOperation(Operation.COPY);
            // treeDropTarget.addDNDListener(listener);

            TreePanelDropTarget treeInternDropTarget = new TreePanelDropTarget(this.tree) {
                @SuppressWarnings("unchecked")
                @Override
                public void onDragDrop(DNDEvent event)
                {
                    // Drag And Drop intern in tree
                    TreeNode item = tree.findNode(event.getTarget());
                    if (item == null)
                    {
                        return;
                    }
                    ModelData md = item.getModel();
                    Object type = md.get("type");
                    if (!type.equals("sequence"))
                    {
                        return;
                    }
                    final List<TreeStoreModel> listSel = (List<TreeStoreModel>) event.getData();

                    for (TreeStoreModel treeStoreModel : listSel)
                    {
                        final ModelData md2 = treeStoreModel.getModel();

                        // String value = md2.get("label");
                        Object id2 = md2.get("id");
                        String itemId = id2.toString();
                        Object id = md.get("id");
                        String parentId = id.toString();

                        String params = "{\"class\":\"fr.hd3d.model.lightweight.impl.LShot\", \"id\":" + itemId
                                + ", \"sequence\":" + parentId + "}";

                        url = "shots/" + itemId;

                        PutDragAndDropCallBack putDragAndDropCallBack = new PutDragAndDropCallBack(treeTemporalModel
                                .getTreeStore(), (BaseTreeModel) md2);

                        IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
                        try
                        {
                            requestHandler.handleRequest(Method.PUT, url, params, putDragAndDropCallBack);
                        }
                        catch (Exception exception)
                        {
                            // TODO : Erreur
                        }
                    }
                    super.onDragDrop(event);
                }
            };

            treeInternDropTarget.setAllowSelfAsSource(true);
            treeInternDropTarget.setFeedback(Feedback.APPEND);
            treeInternDropTarget.setOperation(Operation.MOVE);

            treeInternDropTarget.setGroup("shot");
            treeInternDropTarget.setAutoExpand(true);
            treeInternDropTarget.setAutoExpandDelay(800);
        }

        add(tree);
    }

    public void initExpand()
    {
        this.tree.setExpanded(this.tree.getStore().getAllItems().get(0), true, false);
    }

    public boolean isTemporalItem(Hd3dModelData tI)
    {
        String type = tI.get("type");
        return tI == null ? false : type.compareTo("temporal") == 0;
    }

    public void setLoadHandle(LoadHandle loadHandle)
    {
        this.loadHandle = loadHandle;
    }
}
