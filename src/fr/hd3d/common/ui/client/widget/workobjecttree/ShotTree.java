package fr.hd3d.common.ui.client.widget.workobjecttree;

import java.util.List;

import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.modeldata.reader.RecordReader;
import fr.hd3d.common.ui.client.modeldata.reader.SequenceReader;
import fr.hd3d.common.ui.client.modeldata.reader.ShotReader;
import fr.hd3d.common.ui.client.service.proxy.TreeServicesProxy;
import fr.hd3d.common.ui.client.widget.basetree.BaseTree;
import fr.hd3d.common.ui.client.widget.basetree.BaseTreeDataModel;


/**
 * Shot tree allows user to browse shots and sequences of a given project.
 * 
 * @author HD3D
 */
public class ShotTree extends BaseTree<ShotModelData, SequenceModelData>
{
    /** Constant strings to display : dialog messages, button label... */
    public static final CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    /** Listener used to dispatch events when category or constituent is clicked. */
    protected ShotChangedListener listener = new ShotChangedListener(this);

    /**
     * Default constructor, set reader and tree root title.
     */
    public ShotTree()
    {
        this(CONSTANTS.All());
    }

    /**
     * Default constructor, set reader and tree root title with <i>rootName</i>.
     * 
     * @param rootName
     *            Tree root name.
     */
    public ShotTree(String rootName)
    {
        super(rootName, new ShotReader(), new SequenceReader());
        this.tree.setIconProvider(new WorkObjectTreeIconProvider(model.getStore()));
        this.setSelectionChangedListener();
    }

    /**
     * Initialize tree model with proxy and specific data loader.
     */
    @Override
    protected void setModel(String rootName, IReader<ShotModelData> leafReader, IReader<SequenceModelData> parentReader)
    {
        TreeServicesProxy<RecordModelData> proxy = new TreeServicesProxy<RecordModelData>(new RecordReader());
        this.model = new BaseTreeDataModel<ShotModelData, SequenceModelData>(new ShotTreeLoader(proxy));
        this.model.setRootName(rootName);
    }

    /**
     * Add a selection changed listener to send events to controllers when a constituent or category has been selected.
     */
    private void setSelectionChangedListener()
    {
        this.tree.getSelectionModel().addSelectionChangedListener(listener);
    }

    /**
     * When a shot is clicked, the appropriate event is forwarded to controllers, locally or generally.
     * 
     * @param shots
     *            The selected shots.
     */
    public void onShotClicked(List<ShotModelData> shots)
    {
        AppEvent event = new AppEvent(CommonEvents.SHOTS_SELECTED);
        event.setData(shots);
        this.forwardEvent(event);
    }

    /**
     * When a category is clicked, the appropriate event is forwarded to controllers, locally or generally.
     * 
     * @param sequences
     *            The selected sequences.
     */
    public void onSequenceClicked(List<SequenceModelData> sequences)
    {
        AppEvent event = new AppEvent(CommonEvents.SEQUENCES_SELECTED);
        event.setData(sequences);
        this.forwardEvent(event);
    }

}
