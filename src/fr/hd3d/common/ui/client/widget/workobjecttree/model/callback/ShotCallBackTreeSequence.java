package fr.hd3d.common.ui.client.widget.workobjecttree.model.callback;

import java.util.List;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.data.BaseTreeModel;

import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.widget.workobjecttree.model.TreeTemporalModel;


public class ShotCallBackTreeSequence extends BaseCallback
{
    private final TreeTemporalModel treeSequence;
    private final BaseTreeModel currentModel;
    private final List<BaseTreeModel> lst;

    public ShotCallBackTreeSequence(TreeTemporalModel _treeTemporal, BaseTreeModel _currentModel,
            List<BaseTreeModel> lst)
    {
        this.treeSequence = _treeTemporal;
        this.currentModel = _currentModel;
        this.lst = lst;
    }

    @Override
    public void onSuccess(Request request, Response response)
    {
    // JsonRepresentation representation = response.getEntityAsJson();
    // ModelType shotsModelType = ShotModelData.getModelType();
    //
    // ShotModelData loadConfigShotsModelData = new ShotModelData();
    // ShotReader jsonReader = new ShotReader(shotsModelType);
    // ListLoadResult<ShotModelData> result = jsonReader.read(loadConfigShotsModelData, representation.getText());
    //
    // List<ShotModelData> lResult = result.getData();
    //
    // TreeStore<BaseTreeModel> treeStore = this.treeSequence.getTreeStore();
    //
    // for (int i = 0; i < lResult.size(); i++)
    // {
    // ShotModelData cmd = lResult.get(i);
    //
    // BaseTreeModel baseTreeModel = new BaseTreeModel();
    // baseTreeModel.set("label", cmd.getLabel());
    // baseTreeModel.set("id", cmd.getId());
    // baseTreeModel.set("type", "shot");
    // baseTreeModel.set("description", cmd.getDescription());
    // baseTreeModel.set("difficulty", cmd.getDifficulty());
    // baseTreeModel.set("hook", cmd.getHook());
    // baseTreeModel.set("mattePainting", cmd.getMattePainting());
    // baseTreeModel.set("layout", cmd.getLayout());
    // baseTreeModel.set("animation", cmd.getAnimation());
    // baseTreeModel.set("export", cmd.getExport());
    // baseTreeModel.set("tracking", cmd.getTracking());
    // baseTreeModel.set("dressing", cmd.getDressing());
    // baseTreeModel.set("lighting", cmd.getLighting());
    // baseTreeModel.set("rendering", cmd.getRendering());
    // baseTreeModel.set("compositing", cmd.getCompositing());
    // baseTreeModel.set("parentType", cmd.getParentLabel());
    // baseTreeModel.set("parentId", cmd.getSequence());
    // baseTreeModel.set("parentLabel", cmd.getParentLabel());
    //
    // lst.add(baseTreeModel);
    // }
    //
    // treeStore.add(currentModel, lst, false);
    // AppEvent event = new AppEvent(TreeEvents.SET_EXPAND_NODE_TEMPORAL, currentModel);
    // EventDispatcher.forwardEvent(event);
    }
}
