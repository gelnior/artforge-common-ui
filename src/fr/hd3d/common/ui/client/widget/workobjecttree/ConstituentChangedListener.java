package fr.hd3d.common.ui.client.widget.workobjecttree;

import java.util.ArrayList;

import com.extjs.gxt.ui.client.event.SelectionChangedEvent;
import com.extjs.gxt.ui.client.event.SelectionChangedListener;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;


/**
 * Selection changed listener that differentiate category from constituent and call right methods from constituent tree.
 * 
 * @author HD3D
 */
public class ConstituentChangedListener extends SelectionChangedListener<RecordModelData>
{

    /** Tree on which listener is set. */
    private final ConstituentTree constituentTree;

    public ConstituentChangedListener(ConstituentTree constituentTree)
    {
        this.constituentTree = constituentTree;
    }

    @Override
    public void selectionChanged(SelectionChangedEvent<RecordModelData> se)
    {
        // ArrayList<ConstituentModelData> constituents = new ArrayList<ConstituentModelData>();
        // ArrayList<CategoryModelData> categories = new ArrayList<CategoryModelData>();
        // for (RecordModelData item : se.getSelection())
        // {
        // if (item.getClassName().equals(ConstituentModelData.CLASS_NAME))
        // {
        // constituents.add((ConstituentModelData) item);
        // }
        // else if (item.getClassName().equals(CategoryModelData.CLASS_NAME))
        // {
        // categories.add((CategoryModelData) item);
        // }
        // }
        //
        // if (!constituents.isEmpty() && !categories.isEmpty())
        // {
        // this.constituentTree.onConstituentNCategoryClicked(constituents, categories);
        // }
        // else if (!constituents.isEmpty())
        // {
        // this.constituentTree.onConstituentClicked(constituents);
        // }
        // else if (!categories.isEmpty())
        // {
        // this.constituentTree.onCategoryClicked(categories);
        // }
        RecordModelData firstSelected = se.getSelectedItem();
        ArrayList<RecordModelData> dataToDeselected = new ArrayList<RecordModelData>();

        if (firstSelected != null && firstSelected.getClassName().equals(ConstituentModelData.CLASS_NAME))
        {
            ArrayList<ConstituentModelData> constituentList = new ArrayList<ConstituentModelData>();
            for (RecordModelData item : se.getSelection())
            {
                if (item.getClassName().equals(ConstituentModelData.CLASS_NAME))
                {
                    ConstituentModelData shot = new ConstituentModelData();
                    Hd3dModelData.copy(item, shot);
                    constituentList.add(shot);
                }
                else
                {
                    dataToDeselected.add(item);
                }
            }
            if (dataToDeselected.isEmpty())
            {
                this.constituentTree.onConstituentClicked(constituentList);
            }
            else
            {
                this.constituentTree.getTree().getSelectionModel().deselect(dataToDeselected);
            }
        }
        else if (firstSelected != null && firstSelected.getClassName().equals(CategoryModelData.CLASS_NAME))
        {
            ArrayList<CategoryModelData> categoryList = new ArrayList<CategoryModelData>();
            for (RecordModelData item : se.getSelection())
            {
                if (item.getClassName().equals(CategoryModelData.CLASS_NAME))
                {
                    CategoryModelData shot = new CategoryModelData();
                    Hd3dModelData.copy(item, shot);
                    categoryList.add(shot);
                }
                else
                {
                    dataToDeselected.add(item);
                }
            }
            if (dataToDeselected.isEmpty())
            {
                this.constituentTree.onCategoryClicked(categoryList);
            }
            else
            {
                this.constituentTree.getTree().getSelectionModel().deselect(dataToDeselected);
            }
        }
    }
}
