package fr.hd3d.common.ui.client.widget.workobjecttree.view;

import com.extjs.gxt.ui.client.Style.LayoutRegion;
import com.extjs.gxt.ui.client.util.Margins;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.layout.BorderLayout;
import com.extjs.gxt.ui.client.widget.layout.BorderLayoutData;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.widget.workobjecttree.controller.TreeLibraryController;
import fr.hd3d.common.ui.client.widget.workobjecttree.controller.TreeTemporalController;
import fr.hd3d.common.ui.client.widget.workobjecttree.model.TreeLibraryModel;
import fr.hd3d.common.ui.client.widget.workobjecttree.model.TreeTemporalModel;


public class TreesView extends ContentPanel
{
    private TreeLibraryView treeLibrary;
    private TreeTemporalView treeTemporal;
    private final BorderLayout layoutTree = new BorderLayout();

    public TreeLibraryView getTreeLibrary()
    {
        return treeLibrary;
    }

    public void deselectAll()
    {
        if (treeLibrary.getTree() != null)
        {
            treeLibrary.getTree().getSelectionModel().deselectAll();
        }
        if (treeTemporal.getTree() != null)
        {
            treeTemporal.getTree().getSelectionModel().deselectAll();
        }
    }

    public void setTreeLibrary(TreeLibraryView treeLibrary)
    {
        this.treeLibrary = treeLibrary;
    }

    public TreeTemporalView getTreeTemporal()
    {
        return treeTemporal;
    }

    public void setTreeTemporal(TreeTemporalView treeTemporal)
    {
        this.treeTemporal = treeTemporal;
    }

    public TreesView()
    {
        super();

        final TreeLibraryModel libraryModel = new TreeLibraryModel();
        final TreeTemporalModel temporalModel = new TreeTemporalModel();

        treeLibrary = new TreeLibraryView(libraryModel, true);
        treeLibrary.setHeaderVisible(false);
        treeTemporal = new TreeTemporalView(temporalModel, true);
        treeTemporal.setHeaderVisible(false);

        final TreeLibraryController libraryController = new TreeLibraryController(treeLibrary, libraryModel);
        final TreeTemporalController temporalController = new TreeTemporalController(treeTemporal, temporalModel);

        EventDispatcher dispatcher = EventDispatcher.get();
        dispatcher.addController(libraryController);
        dispatcher.addController(temporalController);

        this.setLayout(layoutTree);
        this.setSize("100%", "100%");

        BorderLayoutData northData = new BorderLayoutData(LayoutRegion.NORTH);
        northData.setSplit(true);
        northData.setMargins(new Margins(5, 0, 0, 0));

        BorderLayoutData southData = new BorderLayoutData(LayoutRegion.CENTER);
        southData.setSplit(true);
        southData.setMargins(new Margins(5, 0, 0, 0));

        this.add(this.treeLibrary, northData);
        this.add(this.treeTemporal, southData);

        this.setBorders(false);
        this.setHeaderVisible(false);
        this.setFrame(false);
        this.setBodyBorder(false);
        this.setStyleAttribute("backgroundColor", "white");
        this.setBodyStyle("fontSize: 12px;");
    }
}
