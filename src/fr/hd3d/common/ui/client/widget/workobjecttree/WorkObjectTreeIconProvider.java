package fr.hd3d.common.ui.client.widget.workobjecttree;

import com.extjs.gxt.ui.client.data.ModelIconProvider;
import com.extjs.gxt.ui.client.store.TreeStore;
import com.extjs.gxt.ui.client.util.IconHelper;
import com.google.gwt.user.client.ui.AbstractImagePrototype;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListRevisionGroupModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;


public class WorkObjectTreeIconProvider implements ModelIconProvider<RecordModelData>
{
    TreeStore<RecordModelData> store;

    public WorkObjectTreeIconProvider(TreeStore<RecordModelData> store)
    {
        this.store = store;
    }

    public AbstractImagePrototype getIcon(RecordModelData model)
    {
        if (model == null)
            return null;

        if (model.getId() == null || model.getId().longValue() == -1L)
        {
            if (CategoryModelData.CLASS_NAME.equals(model.getClassName())
                    || PlayListRevisionGroupModelData.CLASS_NAME.equals(model.getClassName()))
            {
                return IconHelper.createPath("images/folder_picture.png");
            }
            else
            {
                return IconHelper.createPath("images/bobine.png");
            }
        }
        else if (CategoryModelData.CLASS_NAME.equals(model.getClassName()))
        {
            return IconHelper.createPath("images/folder_picture.png");
        }
        else if (ConstituentModelData.CLASS_NAME.equals(model.getClassName()))
        {
            return IconHelper.createPath("images/workobjecttree/constituent.png");
        }
        else if (ShotModelData.CLASS_NAME.equals(model.getClassName()))
        {
            return IconHelper.createPath("images/workobjecttree/imovie.png");
        }
        else if (SequenceModelData.CLASS_NAME.equals(model.getClassName()))
        {
            return IconHelper.createPath("images/bobine.png");
        }
        else if (PlayListRevisionModelData.CLASS_NAME.equals(model.getClassName()))
        {
            return IconHelper.createPath("images/playlists/playlist.png");
        }
        else if (PlayListRevisionGroupModelData.CLASS_NAME.equals(model.getClassName()))
        {
            return IconHelper.createPath("images/suiviDeProduction/folder.png");
        }
        else
        {
            return IconHelper.createPath("images/asset.png");
        }
    }
}
