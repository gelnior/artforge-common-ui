package fr.hd3d.common.ui.client.widget.workobjecttree.controller;

import java.util.Iterator;
import java.util.List;

import org.restlet.client.data.Method;

import com.extjs.gxt.ui.client.data.BaseModelData;
import com.extjs.gxt.ui.client.data.BaseTreeModel;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.mvc.Controller;
import com.extjs.gxt.ui.client.widget.Info;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.widget.workobjecttree.Actions;
import fr.hd3d.common.ui.client.widget.workobjecttree.Urls;
import fr.hd3d.common.ui.client.widget.workobjecttree.events.TreeEvents;
import fr.hd3d.common.ui.client.widget.workobjecttree.model.TreeLibraryModel;
import fr.hd3d.common.ui.client.widget.workobjecttree.model.callback.AllCategoriesCallBack;
import fr.hd3d.common.ui.client.widget.workobjecttree.model.callback.DeleteInTreeCallBack;
import fr.hd3d.common.ui.client.widget.workobjecttree.model.callback.PostCallback;
import fr.hd3d.common.ui.client.widget.workobjecttree.model.callback.PutCallback;
import fr.hd3d.common.ui.client.widget.workobjecttree.view.TreeLibraryView;


/** Controller which handles grid events */
public class TreeLibraryController extends Controller
{
    /** Model which handles grid data */
    private final TreeLibraryModel model;
    /** View which handles widget */
    private final TreeLibraryView treeLibrary;

    /** Default constructor */
    public TreeLibraryController(TreeLibraryView treeLibrary, TreeLibraryModel model)
    {
        this.model = model;
        this.treeLibrary = treeLibrary;

        this.registerView();
        this.registerEvents();

        this.model.initTree();
    }

    /** Register all events the controller can handle. */
    private void registerEvents()
    {
        this.registerEventTypes(TreeEvents.TREE_LOAD_PROJECT);
        this.registerEventTypes(TreeEvents.TREE_LIBRARY_EXPAND);
        this.registerEventTypes(TreeEvents.ACTION_IF_NOT_EXIST_IN_LIBRARY);
        this.registerEventTypes(TreeEvents.VIEW_LIBRARY_DETAILS);
        this.registerEventTypes(TreeEvents.ADD_CATEGORY_NODE);
        this.registerEventTypes(TreeEvents.ADD_CONSTITUENT_NODE);
        this.registerEventTypes(TreeEvents.RENAME_CATEGORY_NODE);
        this.registerEventTypes(TreeEvents.RENAME_CONSTITUENT_NODE);
        this.registerEventTypes(TreeEvents.DELETE_CATEGORY_NODE);
        this.registerEventTypes(TreeEvents.DELETE_CONSTITUENT_NODE);
        this.registerEventTypes(TreeEvents.SET_EXPAND_NODE_LIBRARY);
        this.registerEventTypes(TreeEvents.CLEAR_TREE_LIBRARY_SELECTION);
        this.registerEventTypes(TreeEvents.REFRESH_TREE_LIBRARY_CATEGORIES);
        this.registerEventTypes(TreeEvents.REFRESH_LIBRARY_NODE);
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();

        if (type == TreeEvents.TREE_LOAD_PROJECT)
        {
            ProjectModelData projectModelData = (ProjectModelData) event.getData();
            this.model.setCurrentProject(projectModelData);
            this.onTreeLoadProject();
        }
        else if (type == TreeEvents.TREE_LIBRARY_EXPAND)
        {
            this.onExpandTree((BaseTreeModel) event.getData());
        }
        else if (type == TreeEvents.ACTION_IF_NOT_EXIST_IN_LIBRARY)
        {
            this.onActionIfNotExist(event);
        }
        else if (type == TreeEvents.ADD_CATEGORY_NODE)
        {
            this.onAddCategoryNode(event);
        }
        else if (type == TreeEvents.ADD_CONSTITUENT_NODE)
        {
            this.onAddConstituentNode(event);
        }
        else if (type == TreeEvents.RENAME_CATEGORY_NODE)
        {
            this.onRenameCategoryNode(event);
        }
        else if (type == TreeEvents.RENAME_CONSTITUENT_NODE)
        {
            this.onRenameConstituentNode(event);
        }
        else if (type == TreeEvents.DELETE_CATEGORY_NODE)
        {
            this.onDeleteCategoryNode(event);
        }
        else if (type == TreeEvents.DELETE_CONSTITUENT_NODE)
        {
            this.onDeleteConstituentNode(event);
        }
        else if (type == TreeEvents.SET_EXPAND_NODE_LIBRARY)
        {
            this.setExpandNode((BaseTreeModel) event.getData());
        }
        else if (type == TreeEvents.CLEAR_TREE_LIBRARY_SELECTION)
        {
            this.treeLibrary.getTree().getSelectionModel().deselectAll();
        }
        else if (type == TreeEvents.REFRESH_TREE_LIBRARY_CATEGORIES)
        {
            refreshCategories();
        }
        else if (type == TreeEvents.REFRESH_LIBRARY_NODE)
        {
            refreshNode(event);
        }
        else
        {
            this.forwardToChild(event);
        }
    }

    /** Retrieve data from XML configuration and sets static configuration variable. */
    private void onExpandTree(BaseTreeModel itemSel)
    {
        if (this.model.getListCategories().getCount() == 0)
        {
            refreshCategories();
        }
        if (this.treeLibrary.getTree().getStore().getChildren(itemSel) != null
                && this.treeLibrary.getTree().getStore().getChildren(itemSel).size() == 0)
        {
            this.model.populateItem(itemSel);
            this.treeLibrary.setBLoadingCategory(true);
        }
    }

    private void setExpandNode(BaseTreeModel modelData)
    {
        this.treeLibrary.getTree().setExpanded(modelData, true, false);
    }

    public void onAddCategoryNode(AppEvent event)
    {
        BaseTreeModel item = (BaseTreeModel) event.getData("ITEM");
        String value = event.getData("VALUE");
        String url = null;
        BaseTreeModel baseTreeModel = new BaseTreeModel();
        if (item != null)
        {
            baseTreeModel.set("label", value);
            baseTreeModel.set("type", "category");
            baseTreeModel.set("parentId", item.get("id"));
            baseTreeModel.set("parentLabel", item.get("label"));
            String params = "{\"class\":\"fr.hd3d.model.lightweight.impl.LCategory\", \"name\":\"" + value
                    + "\", \"project\":" + Urls.projectId + "}";

            Object obj = item.get("type");

            if (!"library".equals(obj.toString()))
            {
                Long id = item.get(CategoryModelData.ID_FIELD);
                String parentId = id.toString();
                params = "{\"class\":\"fr.hd3d.model.lightweight.impl.LCategory\", \"name\":\"" + value
                        + "\", \"parent\":" + parentId + ", \"project\":" + Urls.projectId + "}";
            }

            url = Urls.urlProject + Urls.urlCat;
            PostCallback postCallback = new PostCallback(model.getTreeStore(), baseTreeModel, item);

            IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
            try
            {
                requestHandler.handleRequest(Method.POST, url, params, postCallback);
            }
            catch (Exception e)
            {
                // TODO : Erreur
            }
        }
    }

    public void onAddConstituentNode(AppEvent event)
    {

        BaseTreeModel item = (BaseTreeModel) event.getData("ITEM");
        String value = event.getData("VALUE");
        String url = null;
        BaseTreeModel baseTreeModel = new BaseTreeModel();
        if (item != null)
        {
            ModelData modelItem = item;

            baseTreeModel.set("label", value);
            baseTreeModel.set("type", "constituent");
            baseTreeModel.set("parentId", modelItem.get("id"));
            Long id = modelItem.get(CategoryModelData.ID_FIELD);
            String parentId = id.toString();
            baseTreeModel.set("parentLabel", modelItem.get("label"));
            baseTreeModel.set("difficulty", 0);
            baseTreeModel.set("design", false);
            baseTreeModel.set("modeling", false);
            baseTreeModel.set("setup", false);
            baseTreeModel.set("texturing", false);
            baseTreeModel.set("shading", false);
            baseTreeModel.set("hairs", false);

            final String hook = model.getHook(baseTreeModel, item);

            baseTreeModel.set("description", Urls.noDescription);
            baseTreeModel.set("hook", hook);

            String params = "{\"class\":\"fr.hd3d.model.lightweight.impl.LConstituent\", \"label\":\"" + value
                    + "\", \"category\":" + parentId + ", \"description\":\"" + Urls.noDescription + "\", \"hook\":\""
                    + hook + "\", \"difficulty\":" + 0 + ", \"design\":" + false + ", \"modeling\":" + false
                    + ", \"setup\":" + false + ", \"texturing\":" + false + ", \"shading\":" + false + ", \"hairs\":"
                    + false + "}";
            url = Urls.urlProject + Urls.urlCat + "/" + parentId + Urls.urlConstituent;
            PostCallback postCallback = new PostCallback(model.getTreeStore(), baseTreeModel, item);

            IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
            try
            {
                requestHandler.handleRequest(Method.POST, url, params, postCallback);
            }
            catch (Exception e)
            {
                // TODO : Erreur
            }
        }
    }

    public void onRenameCategoryNode(AppEvent event)
    {
        BaseTreeModel item = (BaseTreeModel) event.getData("ITEM");
        String value = event.getData("VALUE");
        String url = null;
        if (item != null)
        {

            BaseTreeModel modelItem = item;
            Long id = modelItem.get(CategoryModelData.ID_FIELD);
            String itemId = id.toString();
            String params = null;

            String type = modelItem.get("type");
            if (!"library".equals(type))
            {
                BaseModelData parent = model.getTreeStore().getParent(item);
                String parentType = parent.get("type");
                if ("library".equals(parentType))
                {
                    params = "{\"class\":\"fr.hd3d.model.lightweight.impl.LCategory\", \"id\":" + itemId
                            + ", \"name\":\"" + value + "\", \"project\":" + Urls.projectId + "}";
                }
                else
                {
                    Long parentIdLong = parent.get("id");
                    String parentId = parentIdLong.toString();
                    params = "{\"class\":\"fr.hd3d.model.lightweight.impl.LCategory\", \"id\":" + itemId
                            + ", \"name\":\"" + value + "\", \"parent\":" + parentId + ", \"project\":"
                            + Urls.projectId + "}";
                }
                url = Urls.urlProject + Urls.urlCat + "/" + itemId;
                PutCallback putCallback = new PutCallback(model.getTreeStore(), item, value);

                IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
                try
                {
                    requestHandler.handleRequest(Method.PUT, url, params, putCallback);
                }

                catch (Exception e)
                {
                    // TODO : Erreur
                }
            }
        }
    }

    public String boolInIntString(Boolean boolVal)
    {
        if (!boolVal)
        {
            return "0";
        }
        else
        {
            return "1";
        }
    }

    public void onRenameConstituentNode(AppEvent event)
    {

        BaseTreeModel item = (BaseTreeModel) event.getData("ITEM");
        String value = event.getData("VALUE");
        String url = null;
        if (item != null)
        {
            // TODO PUT PARTIEL
            BaseTreeModel modelItem = item;

            Long itemIdLong = modelItem.get("id");
            String itemId = itemIdLong.toString();
            Long parentIdLong = model.getTreeStore().getParent(item).get("id");
            String parentId = parentIdLong.toString();

            String params = "{\"class\":\"fr.hd3d.model.lightweight.impl.LConstituent\", \"id\":" + itemId
                    + ", \"label\":\"" + value + "\"}";

            url = Urls.urlProject + Urls.urlCat + "/" + parentId + Urls.urlConstituent + "/" + itemId;
            PutCallback putCallback = new PutCallback(model.getTreeStore(), item, value);

            IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
            try
            {
                requestHandler.handleRequest(Method.PUT, url, params, putCallback);
            }
            catch (Exception e)
            {
                // TODO : Erreur
            }
        }
    }

    public void onDeleteCategoryNode(AppEvent event)
    {
        BaseTreeModel item = (BaseTreeModel) event.getData("ITEM");
        if (item != null)
        {
            ModelData modelItem = item;

            Long idLong = modelItem.get("id");
            String itemId = idLong.toString();

            String type = modelItem.get("type");
            if (!"library".equals(type))
            {
                String url = Urls.urlProject + Urls.urlCat + "/" + itemId;
                DeleteInTreeCallBack deleteInTreeCallBack = new DeleteInTreeCallBack(model.getTreeStore(), item);

                IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
                try
                {
                    requestHandler.handleRequest(Method.DELETE, url, null, deleteInTreeCallBack);
                }
                catch (Exception e)
                {
                    // TODO : Erreur
                }
            }
        }
    }

    public void onDeleteConstituentNode(AppEvent event)
    {
        BaseTreeModel item = (BaseTreeModel) event.getData("ITEM");
        if (item != null)
        {
            ModelData modelItem = item;
            Long idLong = modelItem.get("id");
            String itemId = idLong.toString();
            Long parentIdLong = model.getTreeStore().getParent(item).get("id");
            String parentId = parentIdLong.toString();

            if (!"library".equals(modelItem.get("type")))
            {
                String url = Urls.urlProject + Urls.urlCat + "/" + parentId + Urls.urlConstituent + "/" + itemId;
                DeleteInTreeCallBack deleteInTreeCallBack = new DeleteInTreeCallBack(model.getTreeStore(), item);
                IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
                try
                {
                    requestHandler.handleRequest(Method.DELETE, url, null, deleteInTreeCallBack);
                }
                catch (Exception e)
                {
                    // TODO : Erreur
                }
            }
        }
    }

    public void onActionIfNotExist(AppEvent event)
    {
        EventType action = event.getData("ACTION");
        BaseTreeModel item = (BaseTreeModel) event.getData("ITEM");
        String value = event.getData("VALUE");

        if (action == TreeEvents.RENAME_CONSTITUENT_NODE || action == TreeEvents.RENAME_CATEGORY_NODE)
        {
            if (isExist(model.getTreeStore().findModel("id", item.get("parentId")), value))
            {
                return;
            }
        }
        else
        {
            if (isExist(item, value))
            {
                return;
            }
        }
        createEvent(item, value, action);
    }

    public boolean isExist(BaseTreeModel item, String value)
    {
        if (item != null)
        {
            List<BaseTreeModel> lst = model.getTreeStore().getChildren(item);
            Iterator<BaseTreeModel> it = lst.iterator();
            while (it.hasNext())
            {
                ModelData md = it.next();
                String label = md.get("label");
                if (label.equals(value))
                {
                    Info.display("Warning", value + "  already exist in " + item.get("label"));
                    return true;
                }
            }
        }
        return false;
    }

    private void createEvent(BaseTreeModel item, String value, EventType treeEvent)
    {

        AppEvent event = new AppEvent(treeEvent);
        event.setData("VALUE", value);
        event.setData("ITEM", item);
        EventDispatcher.forwardEvent(event);
    }

    private void onTreeLoadProject()
    {
        this.treeLibrary.getTree().getSelectionModel().deselectAll();
        this.model.initTree();
        this.refreshCategories();
        this.treeLibrary.enable();
        this.treeLibrary.getTree().setExpanded(this.model.getTreeStore().getAllItems().get(0), true, false);
    }

    private void registerView()
    {}

    private void refreshCategories()
    {

        if (this.model.getCurrentProject() != null) // project selectionne
        {
            String urlCategories = Urls.urlProject + Urls.urlCat + "/all?orderBy=[\"name\"]";

            IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();

            try
            {
                requestHandler.handleRequest(Method.GET, urlCategories, null, new AllCategoriesCallBack(this.model));
            }
            catch (Exception e)
            {}
        }
    }

    private void refreshNode(AppEvent event)
    {

        BaseTreeModel item = (BaseTreeModel) event.getData("ITEM");
        this.treeLibrary.getTree().getStore().removeAll(item);
        this.onExpandTree(item);
        this.refreshCategories();
        AppEvent event2 = new AppEvent(TreeEvents.REFRESH_VIEWS);
        event2.setData("ACTION", Actions.REFRESH_AFTER_POST);

    }
}
