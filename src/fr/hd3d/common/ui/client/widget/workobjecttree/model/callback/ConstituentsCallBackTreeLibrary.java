package fr.hd3d.common.ui.client.widget.workobjecttree.model.callback;

import java.util.List;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.data.BaseTreeModel;

import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.widget.workobjecttree.model.TreeLibraryModel;


public class ConstituentsCallBackTreeLibrary extends BaseCallback
{
    private final TreeLibraryModel treeLibraryModel;
    private final BaseTreeModel currentItem;
    private final List<BaseTreeModel> lst;

    public ConstituentsCallBackTreeLibrary(TreeLibraryModel _treeLibraryModel, BaseTreeModel _currentItem,
            List<BaseTreeModel> lst)
    {
        this.treeLibraryModel = _treeLibraryModel;
        this.currentItem = _currentItem;
        this.lst = lst;
    }

    @Override
    public void onSuccess(Request request, Response response)
    {
    // JsonRepresentation representation = response.getEntityAsJson();
    // ModelType constituentModelType = ConstituentModelData.getModelType();
    //
    // ConstituentModelData loadConfigConstituentsModelData = new ConstituentModelData();
    // ConstituentReader jsonReader = new ConstituentReader(constituentModelType);
    // ListLoadResult<ConstituentModelData> result = jsonReader.read(loadConfigConstituentsModelData, representation
    // .getText());
    //
    // List<ConstituentModelData> lResult = result.getData();
    //
    // TreeStore<BaseTreeModel> treeStore = treeLibraryModel.getTreeStore();
    //
    // for (int i = 0; i < lResult.size(); i++)
    // {
    // ConstituentModelData cmd = lResult.get(i);
    //
    // BaseTreeModel baseTreeModel = new BaseTreeModel();
    // baseTreeModel.set("label", cmd.getLabel());
    // baseTreeModel.set("id", cmd.getId());
    // baseTreeModel.set("description", cmd.getDescription());
    // baseTreeModel.set("difficulty", cmd.getDifficulty());
    // baseTreeModel.set("hook", cmd.getHook());
    // baseTreeModel.set("trust", cmd.getTrust());
    // baseTreeModel.set("design", cmd.getDesign());
    // baseTreeModel.set("modeling", cmd.getModeling());
    // baseTreeModel.set("setup", cmd.getSetup());
    // baseTreeModel.set("texturing", cmd.getTexturing());
    // baseTreeModel.set("shading", cmd.getShading());
    // baseTreeModel.set("hairs", cmd.getHairs());
    // baseTreeModel.set("parentType", cmd.getParentLabel());
    // baseTreeModel.set("parentId", cmd.getCategory());
    // baseTreeModel.set("parentLabel", cmd.getParentLabel());
    // baseTreeModel.set("type", "constituent");
    //
    // lst.add(baseTreeModel);
    // }
    //
    // treeStore.add(currentItem, lst, false);
    // AppEvent event = new AppEvent(TreeEvents.SET_EXPAND_NODE_LIBRARY, currentItem);
    // EventDispatcher.forwardEvent(event);
    }

    public boolean inBoolean(long data)
    {
        return data == 0 ? false : data == 1;
    }
}
