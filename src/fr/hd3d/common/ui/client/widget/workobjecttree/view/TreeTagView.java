package fr.hd3d.common.ui.client.widget.workobjecttree.view;

import java.util.List;

import com.extjs.gxt.ui.client.Style.Scroll;
import com.extjs.gxt.ui.client.Style.SelectionMode;
import com.extjs.gxt.ui.client.data.BaseTreeModel;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.dnd.TreePanelDragSource;
import com.extjs.gxt.ui.client.dnd.TreePanelDropTarget;
import com.extjs.gxt.ui.client.event.DNDEvent;
import com.extjs.gxt.ui.client.event.DNDListener;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.SelectionChangedEvent;
import com.extjs.gxt.ui.client.event.SelectionChangedListener;
import com.extjs.gxt.ui.client.event.TreePanelEvent;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.TreeStore;
import com.extjs.gxt.ui.client.util.IconHelper;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.Info;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.menu.Menu;
import com.extjs.gxt.ui.client.widget.treepanel.TreePanel;
import com.extjs.gxt.ui.client.widget.treepanel.TreePanel.CheckCascade;
import com.google.gwt.user.client.Element;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.widget.workobjecttree.events.TreeEvents;
import fr.hd3d.common.ui.client.widget.workobjecttree.model.TreeTagModel;
import fr.hd3d.common.ui.client.widget.workobjecttree.util.LoadHandle;
import fr.hd3d.common.ui.client.widget.workobjecttree.view.listeners.ExpandTreeTagListener;


public class TreeTagView extends ContentPanel
{
    public class AsynchTreePanel extends TreePanel<BaseTreeModel>
    {

        public AsynchTreePanel(TreeStore<BaseTreeModel> store)
        {
            super(store);
            setSelectionMode(SelectionMode.MULTI);
        }

        @Override
        protected boolean hasChildren(BaseTreeModel model)
        {
            if (!"tag".equals(model.get("type")))
            {
                return true;
            }
            return super.hasChildren(model);
        }
    }

    private TreeTagModel treeTagModel = null;
    private boolean withDragAndDrop = false;
    private SelectionMode selectionMode = SelectionMode.SINGLE;
    // private final String url = "";
    private BaseTreeModel itemSel = null;
    private AsynchTreePanel tree = null;
    public boolean deleteItem = false;
    private LoadHandle loadHandle = null;
    private boolean bLoadingCategory = false;
    private Menu menuTreeTagView = null;

    public Menu getMenuTreeTagView()
    {
        return menuTreeTagView;
    }

    public void setMenuTreeTagView(Menu menuTreeTagView)
    {
        this.menuTreeTagView = menuTreeTagView;
    }

    public TreeTagView(TreeTagModel treeModel, boolean withDragAndDrop)
    {
        super();
        this.treeTagModel = treeModel;
        this.withDragAndDrop = withDragAndDrop;
        this.setHeaderVisible(false);

        // Completion filter
        // does not keep Checkbox selection !
        /*
         * StoreFilterField<BaseTreeModel> filter = new StoreFilterField<BaseTreeModel>() {
         * 
         * @Override protected boolean doSelect(Store<BaseTreeModel> store, BaseTreeModel parent, BaseTreeModel record,
         * String property, String filter) { String name = record.get("label"); name = name.toLowerCase(); if
         * (name.startsWith(filter.toLowerCase())) return true; return false; } };
         * filter.bind(treeModel.getTreeStore()); filter.setAutoWidth(true); filter.setEmptyText("Quick find");
         * setTopComponent(filter);
         */
    }

    public boolean isWithDragAndDrop()
    {
        return withDragAndDrop;
    }

    public void setWithDragAndDrop(boolean withDragAndDrop)
    {
        this.withDragAndDrop = withDragAndDrop;
    }

    public BaseTreeModel getItemSel()
    {
        return itemSel;
    }

    public void setItemSel()
    {
        itemSel = tree.getSelectionModel().getSelectedItem();
    }

    public TreePanel<BaseTreeModel> getTree()
    {
        return tree;
    }

    public TreeTagModel getTreeTagModel()
    {
        return treeTagModel;
    }

    public boolean isBLoadingCategory()
    {
        return bLoadingCategory;
    }

    public void setBLoadingCategory(boolean loadingCategory)
    {
        bLoadingCategory = loadingCategory;
    }

    public SelectionMode getSelectionMode()
    {
        return selectionMode;
    }

    public void setSelectionMode(SelectionMode selectionMode)
    {
        this.selectionMode = selectionMode;
    }

    public void setSelectionModeMulti()
    {
        this.selectionMode = SelectionMode.MULTI;
    }

    // private void expandAllChildren(TreeNode n)
    // {
    // tree.setExpanded((BaseTreeModel) n.getModel(), true, true);
    // }

    public void checkItemsRecursive(BaseTreeModel item, boolean checked)
    {
        List<BaseTreeModel> l = treeTagModel.getTreeStore().getChildren(item);
        for (BaseTreeModel m : l)
        {
            tree.setChecked(m, checked);
            checkItemsRecursive(m, checked);
        }
    }

    @Override
    protected void onRender(Element parent, int index)
    {
        super.onRender(parent, index);
        this.setLayout(new FitLayout());
        this.setScrollMode(Scroll.AUTO);
        this.setHeaderVisible(false);
        this.setWidth("100%");
        this.setHeight("100%");
        this.tree = new AsynchTreePanel(this.treeTagModel.getTreeStore());
        this.tree.setCheckable(true);
        this.tree.setCheckStyle(CheckCascade.NONE);
        this.tree.setDisplayProperty("label");
        this.tree.setAutoLoad(true);
        this.tree.getStyle().setLeafIcon(IconHelper.createStyle("tag"));
        this.tree.setEnabled(false);
        this.tree.getSelectionModel().setSelectionMode(selectionMode);

        this.tree.addListener(Events.BeforeExpand, new ExpandTreeTagListener());
        this.tree.addListener(Events.BeforeCheckChange, new Listener<TreePanelEvent<BaseTreeModel>>() {

            public void handleEvent(TreePanelEvent<BaseTreeModel> be)
            {
                BaseTreeModel item = be.getItem();
                if (item.get("type").equals("tags"))
                {
                    // on vient de cliquer sur le root (pour éviter une énorme cascade, on annule)
                    be.setCancelled(true);
                }
                else
                {
                    AppEvent event = new AppEvent(TreeEvents.TREE_TAG_CLICKED, item);
                    EventDispatcher.forwardEvent(event);
                }

                // tree.setExpanded(m, true, true);
                // tree.onCheckCascade(m, true);
                // tree.setChecked(m, true);
                // if (true)
                // return;
                // Vector<Long> selectedIds = new Vector<Long>();
                // List<BaseTreeModel> l;
                // l = tree.getCheckedSelection();
                // for (BaseTreeModel b : l)
                // {
                // selectedIds.add((Long) b.get("id"));
                // }
                // String toDisplay = "";
                // Iterator i = selectedIds.iterator();
                // while (i.hasNext())
                // {
                // toDisplay = toDisplay + i.next();
                // }
                // GWT.log(toDisplay, null);
            }
        });

        // tree.addListener(Events.OnDoubleClick, new Listener<TreePanelEvent<BaseTreeModel>>() {
        //
        // public void handleEvent(TreePanelEvent<BaseTreeModel> be)
        // {
        // TreeNode node = (TreeNode) be.getNode();
        // }
        // });

        // tree.addListener(Events.OnClick, new Listener<TreePanelEvent<BaseTreeModel>>() {
        //
        // public void handleEvent(TreePanelEvent<BaseTreeModel> be)
        // {
        // if (!be.isRightClick())
        // {
        // TreeNode node = be.getNode();
        // if (node != null)
        // {
        //
        // }
        // }
        // else
        // {
        // // RightClick
        // TreeNode node = be.getNode();
        // if (node != null)
        // {
        // // ContextMenu
        // BaseTreeModel btm = (BaseTreeModel) node.getModel();
        // AppEvent event = new AppEvent(TreeEvents.TREE_LIBRARY_MENU, btm);
        // EventDispatcher.forwardEvent(event);
        // }
        // }
        //
        // };
        // });

        this.tree.getSelectionModel().addSelectionChangedListener(new SelectionChangedListener<BaseTreeModel>() {
            @Override
            public void selectionChanged(SelectionChangedEvent<BaseTreeModel> se)
            {
                fireEvent(Events.SelectionChange, se);
                if (se.getSelection().size() > 0)
                {
                    AppEvent event = new AppEvent(TreeEvents.TREE_TAG_SELECTION, se.getSelection().get(0));
                    EventDispatcher.forwardEvent(event);
                    AppEvent event2 = new AppEvent(TreeEvents.TREE_TAG_SELECTION_MULTI, se.getSelection());
                    EventDispatcher.forwardEvent(event2);
                }
            }
        });

        this.tree.addListener(Events.Expand, new Listener<TreePanelEvent<ModelData>>() {
            public void handleEvent(TreePanelEvent<ModelData> te)
            {
                if (loadHandle != null && isTagItem((BaseTreeModel) te.getItem()) && bLoadingCategory == true)
                {
                    loadHandle.onLoadHandle();
                    bLoadingCategory = false;
                }
            }
        });

        //
        // DRAG AND DROP Support
        //
        if (!withDragAndDrop)
        {
            add(this.tree);
            return;
        }

        TreePanelDragSource treeDragSource = new TreePanelDragSource(this.tree);

        treeDragSource.addDNDListener(new DNDListener() {
            @Override
            public void dragStart(DNDEvent e)
            {
                try
                {
                    List<BaseTreeModel> l = tree.getSelectionModel().getSelectedItems();
                    if (l == null)
                    {
                        Info.display(":''(((", "");
                        e.setCancelled(true);
                        e.getStatus().setStatus(false);
                        return;
                    }
                    /*
                     * for (BaseTreeModel m : l) { if ("tagcategory".equals(m.get("type"))) { Long id = m.get("id");
                     * String ids = treeTagModel.getChildrenTagIdsRecursive(id.toString()); Info.display("TAGS!!", ids);
                     * } else { Long lo = m.get("id"); Info.display("TAGS..", lo.toString()); } }
                     */
                    e.setData(l);
                    super.dragStart(e);
                }
                catch (Exception ze)
                {
                    ze.printStackTrace();
                }
            }
        });

        treeDragSource.setGroup("tags");

        new TreePanelDropTarget(this.tree);
        // target.setAllowSelfAsSource(true);
        // target.setFeedback(Feedback.BOTH);

        // TreePanelDropTarget treeDropTarget = new TreePanelDropTarget(this.tree) {
        //
        // @Override
        // public void onDragDrop(DNDEvent event)
        // {
        // // Drag And Drop intern in tree
        // TreeNode item = tree.findNode(event.getTarget());
        //
        // if (item != null)
        // {
        //
        // final ModelData md2 = (ModelData) this.tree.getSelectionModel().getSelectedItem();
        //
        // ModelData md = item.getModel();
        //
        // String value = md2.get("label");
        // Long idLong = md2.get("id");
        //
        // String itemId = idLong.toString();
        //
        // ModelData parent = this.tree.getStore().getParent(md);
        // parent = md;
        //
        // Long parentIdLong = parent.get("id");
        // String parentId = parentIdLong.toString();
        //
        // String params = "{\"class\":\"fr.hd3d.model.lightweight.impl.LConstituent\", \"id\":" + itemId
        // + ", \"category\":" + parentId + "}";
        //
        // url = "constituents/" + itemId;
        //
        // PutDragAndDropCallBack putDragAndDropCallBack = new PutDragAndDropCallBack(treeLibraryModel
        // .getTreeStore(), md2, md);
        //
        // IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
        // try
        // {
        // requestHandler.handleRequest(Method.PUT, url, params, putDragAndDropCallBack);
        // }
        // catch (Exception exception)
        // {
        // // TODO : Erreur
        // }
        //
        // deleteItem = true;
        //
        // super.onDragDrop(event);
        //
        // // super.handleInsertDrop(event, itemExpand, index);
        // }
        // }
        // };
        //
        // treeDropTarget.setAllowSelfAsSource(true);
        // treeDropTarget.setFeedback(Feedback.BOTH);
        //
        // treeDropTarget.setGroup("constituent");
        // treeDropTarget.setAutoExpand(true);
        // treeDropTarget.setAutoExpandDelay(800);

        add(this.tree);
    }

    public void initExpand()
    {
        this.tree.setExpanded(this.tree.getStore().getAllItems().get(0), true, false);
    }

    public boolean isTagItem(BaseTreeModel tI)
    {
        String type = tI.get("type");
        return tI == null ? false : type.compareTo("tags") == 0;
    }

    public void setLoadHandle(LoadHandle loadHandle)
    {
        this.loadHandle = loadHandle;
    }
}
