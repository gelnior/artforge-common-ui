package fr.hd3d.common.ui.client.widget.workobjecttree.model.callback;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.data.BaseTreeModel;
import com.extjs.gxt.ui.client.store.TreeStore;
import com.extjs.gxt.ui.client.widget.Info;

import fr.hd3d.common.ui.client.service.callback.BaseCallback;


public class PutDragAndDropCallBack extends BaseCallback
{
    private final TreeStore<BaseTreeModel> treeStore;
    private final BaseTreeModel item;

    public PutDragAndDropCallBack(TreeStore<BaseTreeModel> treeStore, BaseTreeModel item)
    {
        this.treeStore = treeStore;
        this.item = item;
    }

    @Override
    public void onSuccess(Request request, Response response)
    {
        Info.display("Save Result " + item.get("label"), "The object have been correctly modified");

        BaseTreeModel parent = this.treeStore.getParent(item);
        item.set("parentLabel", parent.get("label"));
        this.treeStore.update(item);
    }

    // Sort and Insert NOT USED
    // private void insertSort(ModelData parent)
    // {
    //
    // this.treeStore.remove(parent, this.item);
    // this.treeStore.commitChanges();
    //
    // List<ModelData> children = treeStore.getChildren(parent, false);
    // for (int i = 0; i < children.size(); i++)
    // {
    // if ((children.get(i).get("label").toString()).compareTo(item.get("label").toString()) > 0)
    // {
    // treeStore.insert(item, parent, i, true);
    // return;
    // }
    // }
    // return;
    // }
}
