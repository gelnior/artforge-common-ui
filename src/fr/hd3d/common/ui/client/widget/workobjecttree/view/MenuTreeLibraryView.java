package fr.hd3d.common.ui.client.widget.workobjecttree.view;

import com.extjs.gxt.ui.client.Style.Scroll;
import com.extjs.gxt.ui.client.data.BaseTreeModel;
import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.KeyListener;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MenuEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.layout.BorderLayout;
import com.extjs.gxt.ui.client.widget.layout.FormData;
import com.extjs.gxt.ui.client.widget.menu.Menu;
import com.extjs.gxt.ui.client.widget.menu.MenuItem;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.widget.workobjecttree.events.TreeEvents;
import fr.hd3d.common.ui.client.widget.workobjecttree.view.listeners.ViewElementLibraryListener;


public class MenuTreeLibraryView extends Menu
{
    final static int viewDetails = 0;
    final static int addCategoryNode = 1;
    final static int addConstituentNode = 2;
    final static int addSubCategoryNode = 3;
    final static int renameCategoryNode = 4;
    final static int deleteCategoryNode = 5;
    final static int cloneConstituentNode = 6;
    final static int renameConstituentNode = 7;
    final static int deleteConstituentNode = 8;
    final static int viewCollection = 9;
    final static int refreshCategoryNode = 10;

    final String[] titres = { "View details", "New category", "New constituent", "New sub-category", "Rename",
            "Delete", "Clone constituent", "Rename", "Delete", "View collection", "Refresh" };

    BaseTreeModel item = null;

    public MenuTreeLibraryView(BaseTreeModel item)
    {

        super();
        this.setWidth(145);
        this.item = item;

        MenuItem view = new MenuItem();
        view.setText(titres[viewDetails]);
        view.setIconStyle("view-details");

        MenuItem view2 = new MenuItem();
        view2.setText(titres[viewCollection]);
        view2.setIconStyle("view-details");

        view.addSelectionListener(new ViewElementLibraryListener<MenuEvent>(true));
        view2.addSelectionListener(new ViewElementLibraryListener<MenuEvent>(true));

        MenuItem addCategoryNodeItem = new MenuItem();
        addCategoryNodeItem.setText(titres[addCategoryNode]);
        addCategoryNodeItem.setIconStyle("add-sub-category");
        addCategoryNodeItem.addSelectionListener(new SelectionListener<MenuEvent>() {
            @Override
            public void componentSelected(MenuEvent ce)
            {

                displayForm(addCategoryNode);
            }
        });
        MenuItem addConstituentNodeItem = new MenuItem();
        addConstituentNodeItem.setText(titres[addConstituentNode]);
        addConstituentNodeItem.setIconStyle("add-constituent");
        addConstituentNodeItem.addSelectionListener(new SelectionListener<MenuEvent>() {
            @Override
            public void componentSelected(MenuEvent ce)
            {
                displayForm(addConstituentNode);
            }
        });
        MenuItem addSubCategoryNodeItem = new MenuItem();
        addSubCategoryNodeItem.setText(titres[addSubCategoryNode]);
        addSubCategoryNodeItem.setIconStyle("add-sub-category");
        addSubCategoryNodeItem.addSelectionListener(new SelectionListener<MenuEvent>() {
            @Override
            public void componentSelected(MenuEvent ce)
            {
                displayForm(addSubCategoryNode);
            }
        });
        MenuItem renameCategoryNodeItem = new MenuItem();
        renameCategoryNodeItem.setText(titres[renameCategoryNode]);
        renameCategoryNodeItem.setIconStyle("rename");
        renameCategoryNodeItem.addSelectionListener(new SelectionListener<MenuEvent>() {
            @Override
            public void componentSelected(MenuEvent ce)
            {
                displayForm(renameCategoryNode);
            }
        });
        MenuItem deleteCategoryNodeItem = new MenuItem();
        deleteCategoryNodeItem.setText(titres[deleteCategoryNode]);
        deleteCategoryNodeItem.setIconStyle("delete");
        deleteCategoryNodeItem.addSelectionListener(new SelectionListener<MenuEvent>() {
            @Override
            public void componentSelected(MenuEvent ce)
            {
                deleteActionDialog(deleteCategoryNode);
            }
        });
        // deleteCategoryNodeItem.setEnabled(false);

        MenuItem cloneConstituentNodeItem = new MenuItem();
        cloneConstituentNodeItem.setText(titres[cloneConstituentNode]);
        cloneConstituentNodeItem.setIconStyle("icon-add");
        cloneConstituentNodeItem.addSelectionListener(new SelectionListener<MenuEvent>() {
            @Override
            public void componentSelected(MenuEvent ce)
            {
                displayForm(cloneConstituentNode);
            }
        });
        MenuItem renameConstituentNodeItem = new MenuItem();
        renameConstituentNodeItem.setText(titres[renameConstituentNode]);
        renameConstituentNodeItem.setIconStyle("rename");
        renameConstituentNodeItem.addSelectionListener(new SelectionListener<MenuEvent>() {
            @Override
            public void componentSelected(MenuEvent ce)
            {
                displayForm(renameConstituentNode);
            }
        });
        MenuItem deleteConstituentNodeItem = new MenuItem();
        deleteConstituentNodeItem.setText(titres[deleteConstituentNode]);
        deleteConstituentNodeItem.setIconStyle("delete");
        deleteConstituentNodeItem.addSelectionListener(new SelectionListener<MenuEvent>() {
            @Override
            public void componentSelected(MenuEvent ce)
            {
                deleteActionDialog(deleteConstituentNode);
            }
        });
        // deleteConstituentNodeItem.setEnabled(false);

        MenuItem refreshCategoryNodeItem = new MenuItem();
        refreshCategoryNodeItem.setText(titres[refreshCategoryNode]);
        refreshCategoryNodeItem.setIconStyle("refresh");

        final BaseTreeModel itemSel = item;

        refreshCategoryNodeItem.addSelectionListener(new SelectionListener<MenuEvent>() {

            @Override
            public void componentSelected(MenuEvent ce)
            {
                AppEvent event1 = new AppEvent(TreeEvents.REFRESH_LIBRARY_NODE);
                event1.setData("ITEM", itemSel);
                EventDispatcher.forwardEvent(event1);
            }
        });

        String type = item.get("type");
        if (type.compareTo("library") == 0)
        {
            // TODO
            // contextMenu.add(view);
            this.add(addCategoryNodeItem);
            this.add(refreshCategoryNodeItem);
        }
        else if (type.compareTo("category") == 0)
        {
            this.add(view2);
            this.add(addConstituentNodeItem);
            this.add(addSubCategoryNodeItem);
            this.add(renameCategoryNodeItem);
            this.add(deleteCategoryNodeItem);
            this.add(refreshCategoryNodeItem);
        }
        else
        {
            // TODO
            this.add(view);
            view.setEnabled(true);
            // this.add(cloneConstituentNodeItem);
            this.add(renameConstituentNodeItem);
            this.add(deleteConstituentNodeItem);
        }
    }

    private void displayForm(final int action)
    {
        final Window w = new Window();
        w.setResizable(true);
        w.setWidth(305);
        w.setHeight(105);
        w.setMonitorWindowResize(true);
        w.setModal(true);
        w.setHeaderVisible(true);
        w.setLayout(new BorderLayout());
        w.setHeading(titres[action]);
        w.setPlain(true);

        w.setPosition(211, 150);

        final FormPanel form2 = new FormPanel();
        form2.setHeaderVisible(false);
        form2.setBorders(false);
        form2.setBodyBorder(false);
        form2.setWidth(300);
        form2.setPadding(5);

        final TextField<String> field = new TextField<String>();
        field.setFieldLabel("Name");
        form2.add(field, new FormData("95%"));

        switch (action)
        {
            case renameCategoryNode:
                field.setValue((String) item.get("label"));
                break;
            case renameConstituentNode:
                field.setValue((String) item.get("label"));
                break;
        }

        field.focus();

        Button saveB = new Button("Save");
        w.addButton(saveB);

        field.addKeyListener(new KeyListener() {
            @Override
            public void handleEvent(ComponentEvent event)
            {
                if (event.getType() == Events.KeyPress && event.getKeyCode() == 13)
                {
                    String value = field.getValue();
                    if (value != null && value.length() != 0)
                    {
                        EventType command = null;
                        if (action == addCategoryNode)
                        {
                            command = TreeEvents.ADD_CATEGORY_NODE;
                        }
                        else if (action == addConstituentNode)
                        {
                            command = TreeEvents.ADD_CONSTITUENT_NODE;
                        }
                        else if (action == addSubCategoryNode)
                        {
                            command = TreeEvents.ADD_CATEGORY_NODE;
                        }
                        else if (action == renameCategoryNode)
                        {
                            command = TreeEvents.RENAME_CATEGORY_NODE;
                        }
                        else if (action == cloneConstituentNode)
                        {
                            // cloneConstituentNode(item,value);
                        }
                        else if (action == renameConstituentNode)
                        {
                            command = TreeEvents.RENAME_CONSTITUENT_NODE;
                        }
                        else if (action == addConstituentNode)
                        {}

                        AppEvent event1 = new AppEvent(TreeEvents.ACTION_IF_NOT_EXIST_IN_LIBRARY);
                        event1.setData("VALUE", value);
                        event1.setData("ITEM", item);
                        event1.setData("ACTION", command);
                        EventDispatcher.forwardEvent(event1);

                        w.hide();
                    }
                }
            }
        });

        saveB.addListener(Events.OnClick, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                String value = field.getValue();
                if (value != null && value.length() != 0)
                {
                    EventType command = null;
                    if (action == addCategoryNode)
                    {
                        command = TreeEvents.ADD_CATEGORY_NODE;
                    }
                    else if (action == addConstituentNode)
                    {
                        command = TreeEvents.ADD_CONSTITUENT_NODE;
                    }
                    else if (action == addSubCategoryNode)
                    {
                        command = TreeEvents.ADD_CATEGORY_NODE;
                    }
                    else if (action == renameCategoryNode)
                    {
                        command = TreeEvents.RENAME_CATEGORY_NODE;
                    }
                    else if (action == cloneConstituentNode)
                    {
                        // cloneConstituentNode(item,value);
                    }
                    else if (action == renameConstituentNode)
                    {
                        command = TreeEvents.RENAME_CONSTITUENT_NODE;
                    }
                    else if (action == addConstituentNode)
                    {}

                    AppEvent event = new AppEvent(TreeEvents.ACTION_IF_NOT_EXIST_IN_LIBRARY);
                    event.setData("VALUE", value);
                    event.setData("ITEM", item);
                    event.setData("ACTION", command);
                    EventDispatcher.forwardEvent(event);

                    w.hide();
                }
            }
        });
        Button closeB = new Button("Cancel");
        w.addButton(closeB);
        closeB.addListener(Events.OnClick, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                w.hide();
            }
        });
        w.add(form2);
        w.show();
    }

    private void createEvent(BaseTreeModel item, String value, EventType treeEvent)
    {

        AppEvent event = new AppEvent(treeEvent);
        event.setData("VALUE", value);
        event.setData("ITEM", item);
        EventDispatcher.forwardEvent(event);
    }

    private void deleteActionDialog(final int action)
    {

        final Dialog simple = new Dialog();
        simple.setHeading("Confirmation");
        simple.setButtons(Dialog.YESNO);
        simple.setBodyStyleName("pad-text");
        simple.setModal(true);
        simple.addText("Are you sure to delete : " + item.get("label") + " ?");
        simple.setScrollMode(Scroll.AUTO);
        simple.setHideOnButtonClick(true);

        simple.setPosition(211, 150);

        Button okButton = (Button) simple.getButtonBar().getItemByItemId(Dialog.YES);
        okButton.addListener(Events.Select, new SelectionListener<ComponentEvent>() {
            @Override
            public void componentSelected(ComponentEvent ce)
            {
                deleteAction(action);
            }
        });
        simple.show();
    }

    private void deleteAction(int action)
    {
        switch (action)
        {
            case deleteCategoryNode:
                createEvent(item, null, TreeEvents.DELETE_CATEGORY_NODE);
                break;
            case deleteConstituentNode:
                createEvent(item, null, TreeEvents.DELETE_CONSTITUENT_NODE);
                break;
        }
    }
}
