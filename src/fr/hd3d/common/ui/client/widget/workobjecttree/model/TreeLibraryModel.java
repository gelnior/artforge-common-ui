package fr.hd3d.common.ui.client.widget.workobjecttree.model;

import java.util.ArrayList;
import java.util.List;

import org.restlet.client.data.Method;

import com.extjs.gxt.ui.client.data.BaseTreeModel;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.store.TreeStore;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.parameter.OrderBy;
import fr.hd3d.common.ui.client.widget.workobjecttree.Urls;
import fr.hd3d.common.ui.client.widget.workobjecttree.model.callback.CategoriesCallBackTreeLibrary;


public class TreeLibraryModel
{
    private static final long serialVersionUID = 5716548900553775333L;
    private TreeStore<BaseTreeModel> treeStore = null;
    private ProjectModelData currentProject = null;
    private BaseTreeModel itemExpand = null;
    // private BaseTreeModel itemCurrent = null;
    private ListStore<CategoryModelData> listCategories = new ListStore<CategoryModelData>();
    public BaseTreeModel root = null;

    public TreeLibraryModel()
    {
        treeStore = new TreeStore<BaseTreeModel>();

        // STORESORTER<BASETREEMODEL> SORTER = NEW STORESORTER<BASETREEMODEL>() {
        //
        // @OVERRIDE
        // PUBLIC INT COMPARE(STORE STORE, BASETREEMODEL M1, BASETREEMODEL M2, STRING PROPERTY)
        // {
        // STRING T1 = M1.GET("TYPE");
        // STRING T2 = M2.GET("TYPE");
        //
        // // /*
        // // * IF (M1FOLDER && !M2FOLDER) { # RETURN -1; # } ELSE IF (!M1FOLDER && M2FOLDER) { # RETURN 1; # }
        // // */
        //
        // IF (T1.COMPARETO(T2) == 0)
        // {
        // RETURN SUPER.COMPARE(STORE, M1, M2, "LABEL");
        // }
        // RETURN -1;
        // }
        // };

        // TODO Ne fonctionne pas ????
        // treeStore.setStoreSorter(sorter);
    }

    public TreeStore<BaseTreeModel> getTreeStore()
    {
        return treeStore;
    }

    public void setTreeStore(TreeStore<BaseTreeModel> treeStore)
    {
        this.treeStore = treeStore;
    }

    public ProjectModelData getCurrentProject()
    {
        return currentProject;
    }

    public void setCurrentProject(ProjectModelData currentProject)
    {
        this.currentProject = currentProject;
    }

    public BaseTreeModel getItemExpand()
    {
        return itemExpand;
    }

    public void setItemExpand(BaseTreeModel itemExpand)
    {
        this.itemExpand = itemExpand;
    }

    public ListStore<CategoryModelData> getListCategories()
    {
        return listCategories;
    }

    public void setListCategories(ListStore<CategoryModelData> listCategories)
    {
        this.listCategories = listCategories;
    }

    public void initTree()
    {
        this.treeStore.removeAll();

        // on insert le premier element
        root = new BaseTreeModel();
        root.set("label", "Library");
        root.set("id", Long.valueOf(-1));
        root.set("type", "library");
        this.treeStore.add(root, true);
    }

    public boolean isLibraryItem(BaseTreeModel tI)
    {
        String type = tI.get("type");
        return tI == null ? false : type.compareTo("library") == 0;
    }

    public boolean isCategoryItem(BaseTreeModel tI)
    {
        String type = tI.get("type");
        return tI == null ? false : type.compareTo("category") == 0;
    }

    public boolean isConstituentItem(BaseTreeModel tI)
    {
        String type = tI.get("type");
        return tI == null ? false : type.compareTo("constituent") == 0;
    }

    public void populateItem(BaseTreeModel currentItem)
    {
        /*
         * if (itemExpand == null) { itemExpand = currentItem; return; }
         * 
         * itemExpand = null;
         */

        if (currentItem != null)
        {
            if (currentItem.isLeaf()) // si on ne l a pas deja remplis
            {
                if (isLibraryItem(currentItem)) // si c est le root
                {
                    populateTree(currentItem, true);
                    // this.itemCurrent = currentItem;
                }
                else
                {
                    if (isCategoryItem(currentItem))// si c est une category on populate
                    {
                        populateTree(currentItem, false);
                        // this.itemCurrent = currentItem;
                    }
                    else
                    // c est un constituent... pas normal :)
                    {

                    }
                }
            }
        }
    }

    private void populateTree(BaseTreeModel currentModel, boolean isRoot)
    {
        if (this.currentProject != null) // project selectionne
        {
            String urlProject = Urls.urlProject;
            String urlCategories = "";
            String urlConstituents = "";
            if (isRoot)
            {
                OrderBy orderBy = new OrderBy(RecordModelData.NAME_FIELD);
                urlCategories = urlProject + Urls.urlCat + "?" + orderBy.toString();
                urlConstituents = "";
            }
            else
            {
                long currentCategory = (Long) currentModel.get("id");
                urlCategories = urlProject + Urls.urlCat + "/" + currentCategory + "/children" + "?orderBy=[\"name\"]";
                urlConstituents = urlProject + Urls.urlCat + "/" + currentCategory + Urls.urlConstituent
                        + "?orderBy=[\"label\"]";
            }

            IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();

            /*
             * if (urlConstituents.length() != 0) { try { requestHandler.handleRequest(Method.GET, urlConstituents,
             * null, new ConstituentsCallBackTreeLibrary(this, currentModel)); } catch (Exception e) { // TODO : Erreur
             * } }
             */

            try
            {
                requestHandler.handleRequest(Method.GET, urlCategories, null, new CategoriesCallBackTreeLibrary(this,
                        currentModel, urlConstituents));
            }
            catch (Exception e)
            {
                // TODO : Erreur
            }

        }
    }

    public String getHook(BaseTreeModel baseTreeModel, BaseTreeModel item)
    {
        String hook = baseTreeModel.get("label");
        BaseTreeModel baseTreeParent = item;

        Long idLong = item.get("id");
        String id = idLong.toString();
        while (!id.equals("-1"))
        {
            hook = baseTreeParent.get("label") + "_" + hook;
            baseTreeParent = this.treeStore.getParent(baseTreeParent);

            Long idParent = baseTreeParent.get("id");
            id = idParent.toString();
        }

        return hook;
    }

    public List<Long> getChildrenCategoriesId(String idString)
    {
        List<Long> lst = new ArrayList<Long>();
        Long id = null;
        if (!"-1".equals(idString))
        {
            id = Long.decode(idString);
            lst.add(id);
        }
        else
        {
            for (CategoryModelData md : this.listCategories.getModels())
            {
                lst.add(md.getId());
            }

            return lst;
        }

        List<CategoryModelData> children = this.listCategories.findModels("parent", id);

        for (CategoryModelData child : children)
        {
            lst.addAll(getChildrenCategoriesId(child.getId().toString()));
        }

        return lst;
    }

}
