package fr.hd3d.common.ui.client.widget.workobjecttree.events;

import com.extjs.gxt.ui.client.event.EventType;


/**
 * BreakdownEvents contains all event code values to send to the dispatcher.
 * 
 * @author HD3D
 */
public class TreeEvents
{
    public static final int START_EVENT = 15000;

    public static final EventType ACTION_IF_NOT_EXIST_IN_LIBRARY = new EventType();
    public static final EventType ACTION_IF_NOT_EXIST_IN_TEMPORAL = new EventType();

    public static final EventType TREE_LOAD_PROJECT = new EventType();

    public static final EventType DRAG_DROP_CASTING = new EventType();
    public static final EventType REFRESH_VIEWS = new EventType();

    public static final EventType TREE_LIBRARY_EXPAND = new EventType();
    public static final EventType TREE_TEMPORAL_EXPAND = new EventType();

    public static final EventType TREE_LIBRARY_SELECTION = new EventType();
    public static final EventType TREE_LIBRARY_SELECTION_MULTI = new EventType();
    public static final EventType TREE_TEMPORAL_SELECTION = new EventType();
    public static final EventType TREE_TEMPORAL_SELECTION_MULTI = new EventType();
    public static final EventType SET_EXPAND_NODE_LIBRARY = new EventType();
    public static final EventType SET_EXPAND_NODE_TEMPORAL = new EventType();

    public static final EventType VIEW_LIBRARY_DETAILS = new EventType();
    public static final EventType ADD_CATEGORY_NODE = new EventType();
    public static final EventType ADD_CONSTITUENT_NODE = new EventType();
    public static final EventType RENAME_CATEGORY_NODE = new EventType();
    public static final EventType DELETE_CATEGORY_NODE = new EventType();
    public static final EventType RENAME_CONSTITUENT_NODE = new EventType();
    public static final EventType DELETE_CONSTITUENT_NODE = new EventType();
    public static final EventType REFRESH_LIBRARY_NODE = new EventType();

    public static final EventType VIEW_TEMPORAL_DETAILS = new EventType();
    public static final EventType ADD_SEQUENCE_NODE = new EventType();
    public static final EventType ADD_SHOT_NODE = new EventType();
    public static final EventType RENAME_SEQUENCE_NODE = new EventType();
    public static final EventType DELETE_SEQUENCE_NODE = new EventType();
    public static final EventType RENAME_SHOT_NODE = new EventType();
    public static final EventType DELETE_SHOT_NODE = new EventType();
    public static final EventType REFRESH_TEMPORAL_NODE = new EventType();

    public static final EventType SELECT_TAB_ITEM = new EventType();

    public static final EventType CLEAR_TREE_LIBRARY_SELECTION = new EventType();
    public static final EventType CLEAR_TREE_TEMPORAL_SELECTION = new EventType();

    public static final EventType REFRESH_TREE_LIBRARY_CATEGORIES = new EventType();
    public static final EventType REFRESH_TREE_TEMPORAL_SEQUENCES = new EventType();

    public static final EventType REFRESH_TREE_TAG_CATEGORIES = new EventType();
    public static final EventType TREE_LIBRARY_MENU = new EventType();

    public static final EventType TREE_TAG_EXPAND = new EventType(START_EVENT + 600);
    public static final EventType TREE_TAG_SELECTION = new EventType(START_EVENT + 601);
    public static final EventType TREE_TAG_SELECTION_MULTI = new EventType(START_EVENT + 602);
    public static final EventType SET_EXPAND_NODE_TAG = new EventType(START_EVENT + 603);
    public static final EventType ACTION_IF_NOT_EXIST_IN_TAGS = new EventType(START_EVENT + 604);
    public static final EventType ADD_TAG_NODE = new EventType(START_EVENT + 605);
    public static final EventType DELETE_TAGCATEGORY_NODE = new EventType(START_EVENT + 606);
    public static final EventType DELETE_TAG_NODE = new EventType(START_EVENT + 607);
    public static final EventType CLEAR_TREE_TAG_SELECTION = new EventType(START_EVENT + 608);
    public static final EventType ADD_TAGCATEGORY_NODE = new EventType(START_EVENT + 609);
    public static final EventType RENAME_TAGCATEGORY_NODE = new EventType(START_EVENT + 610);
    public static final EventType RENAME_TAG_NODE = new EventType(START_EVENT + 611);
    public static final EventType VIEW_TAG_DETAILS = new EventType(START_EVENT + 612);
    public static final EventType RECURSE_AND_SET_CHECKED_TREE_TAG = new EventType(START_EVENT + 613);
    public static final EventType SET_TREE_TAG_NODE_CHECKED = new EventType(START_EVENT + 614);
    public static final EventType TREE_TAG_CLICKED = new EventType(START_EVENT + 615);
    public static final EventType PREPARE_TAG_LIST_MSG = new EventType(START_EVENT + 616);
    public static final EventType SEND_TREE_TAG_CHECKED_LIST = new EventType(START_EVENT + 617);
    public static final EventType TREE_TAG_CHECKED_LIST_CHANGED = new EventType(START_EVENT + 618);
}
