package fr.hd3d.common.ui.client.widget.workobjecttree.model;

import java.util.ArrayList;
import java.util.List;

import org.restlet.client.data.Method;

import com.extjs.gxt.ui.client.data.BaseTreeModel;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.store.TreeStore;

import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.widget.workobjecttree.Urls;
import fr.hd3d.common.ui.client.widget.workobjecttree.model.callback.SequenceCallBackTreeSequence;


public class TreeTemporalModel
{

    private static final long serialVersionUID = 5716548900553775333L;
    private TreeStore<BaseTreeModel> treeStore = null;
    private ListStore<SequenceModelData> listSequences = new ListStore<SequenceModelData>();
    private ProjectModelData currentProject = null;
    private BaseTreeModel itemExpand = null;
    // private BaseTreeModel itemCurrent = null;
    public BaseTreeModel root = null;

    public TreeTemporalModel()
    {
        treeStore = new TreeStore<BaseTreeModel>();

        // StoreSorter<Hd3dModelData> sorter = new StoreSorter<Hd3dModelData>() {
        // @Override
        // public int compare(Store<Hd3dModelData> store, Hd3dModelData m1, Hd3dModelData m2, String property)
        // {
        // String t1 = m1.get("type");
        // String t2 = m2.get("type");
        //
        // /*
        // * if (m1Folder && !m2Folder) { # return -1; # } else if (!m1Folder && m2Folder) { # return 1; # }
        // */
        //
        // if (t1.compareTo(t2) == 0)
        // {
        // return super.compare(store, m1, m2, "label");
        // }
        // return -1;
        // }
        // };

        // TODO Ne fonctionne pas ????
        // treeStore.setStoreSorter(sorter);
    }

    public TreeStore<BaseTreeModel> getTreeStore()
    {
        return treeStore;
    }

    public void setTreeStore(TreeStore<BaseTreeModel> treeStore)
    {
        this.treeStore = treeStore;
    }

    public BaseTreeModel getItemExpand()
    {
        return itemExpand;
    }

    public void setItemExpand(BaseTreeModel itemExpand)
    {
        this.itemExpand = itemExpand;
    }

    public ProjectModelData getCurrentProject()
    {
        return currentProject;
    }

    public void setCurrentProject(ProjectModelData currentProject)
    {
        this.currentProject = currentProject;
    }

    public ListStore<SequenceModelData> getListSequences()
    {
        return listSequences;
    }

    public void setListSequences(ListStore<SequenceModelData> listSequences)
    {
        this.listSequences = listSequences;
    }

    public void initTree()
    {
        this.treeStore.removeAll();

        // on insert le premier element
        root = new BaseTreeModel();
        root.set("label", "Temporal");
        root.set("id", Long.valueOf(-1));
        root.set("type", "temporal");
        this.treeStore.add(root, true);
    }

    public boolean isTemporalItem(BaseTreeModel tI)
    {
        String type = tI.get("type");
        return tI == null ? false : type.compareTo("temporal") == 0;
    }

    public boolean isSequenceItem(BaseTreeModel tI)
    {
        String type = tI.get("type");
        return tI == null ? false : type.compareTo("sequence") == 0;
    }

    public boolean isShotItem(BaseTreeModel tI)
    {
        String type = tI.get("type");
        return tI == null ? false : type.compareTo("shot") == 0;
    }

    public void populateItem(BaseTreeModel currentItem)
    {
        /*
         * if (itemExpand == null) { itemExpand = currentItem; return; }
         * 
         * itemExpand = null;
         */
        if (currentItem != null)
        {
            if (currentItem.isLeaf()) // si on ne la pas deja remplis
            {
                if (isTemporalItem(currentItem)) // si c est le root
                {
                    populateTree(currentItem, true);
                    // this.itemCurrent = currentItem;
                }
                else
                {
                    if (isSequenceItem(currentItem))// si c est une
                    // sequence on populate
                    {
                        populateTree(currentItem, false);
                        // this.itemCurrent = currentItem;
                    }
                    else
                    // c est un shot... pas normal :)
                    {

                    }
                }
            }
        }
    }

    private void populateTree(BaseTreeModel currentModel, boolean isRoot)
    {
        if (this.currentProject != null) // project selectionne
        {
            String urlProject = Urls.urlProject;
            String urlSequences = "";
            String urlShots = "";
            if (isRoot)
            {
                urlSequences = urlProject + Urls.urlSeq + "?orderBy=[\"name\"]";
                urlShots = "";
            }
            else
            {
                long currentSeq = (Long) currentModel.get("id");
                urlSequences = urlProject + Urls.urlSeq + "/" + currentSeq + "/children" + "?orderBy=[\"name\"]";
                urlShots = urlProject + Urls.urlSeq + "/" + currentSeq + Urls.urlShot + "?orderBy=[\"label\"]";
            }

            IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
            try
            {
                requestHandler.handleRequest(Method.GET, urlSequences, null, new SequenceCallBackTreeSequence(this,
                        currentModel, urlShots));
            }
            catch (Exception e)
            {
                // TODO : Erreur
            }
            /*
             * if (urlShots.length() != 0) { try { requestHandler.handleRequest(Method.GET, urlShots, null, new
             * ShotCallBackTreeSequence(this, currentModel)); } catch (Exception e) { // TODO : Erreur } }
             */
        }
    }

    public String getHook(BaseTreeModel baseTreeModel, BaseTreeModel item)
    {
        String hook = baseTreeModel.get("label");
        BaseTreeModel baseTreeParent = item;

        Long idLong = item.get("id");
        String id = idLong.toString();
        while (!id.equals("-1"))
        {
            hook = baseTreeParent.get("label") + "_" + hook;
            baseTreeParent = this.treeStore.getParent(baseTreeParent);

            Long idParent = baseTreeParent.get("id");
            id = idParent.toString();
        }

        return hook;
    }

    public List<Long> getChildrenSequencesId(String idString)
    {
        List<Long> lst = new ArrayList<Long>();
        Long id = null;
        if (!"-1".equals(idString))
        {
            id = Long.decode(idString);
            lst.add(id);
        }
        else
        {
            for (SequenceModelData md : this.listSequences.getModels())
            {
                lst.add(md.getId());
            }

            return lst;
        }

        List<SequenceModelData> children = this.listSequences.findModels("parent", id);

        for (SequenceModelData child : children)
        {
            lst.addAll(getChildrenSequencesId(child.getId().toString()));
        }

        return lst;
    }
}
