package fr.hd3d.common.ui.client.widget.workobjecttree;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.TreeLoadEvent;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.modeldata.reader.SequenceReader;
import fr.hd3d.common.ui.client.modeldata.reader.ShotReader;
import fr.hd3d.common.ui.client.service.parameter.OrderBy;
import fr.hd3d.common.ui.client.service.proxy.TreeServicesProxy;
import fr.hd3d.common.ui.client.widget.basetree.BaseTreeDataLoader;


/**
 * Shot tree loader specifies which URL should be used to load a specific node in shot tree.
 * 
 * @author HD3D
 */
public class ShotTreeLoader extends BaseTreeDataLoader<ShotModelData, SequenceModelData>
{
    /**
     * Default constructor sets proxy, reader and default instance.
     * 
     * @param proxy
     *            The proxy needed to access to services.
     */
    public ShotTreeLoader(TreeServicesProxy<RecordModelData> proxy)
    {
        super(proxy, new ShotReader(), new SequenceReader(), new ShotModelData(), new SequenceModelData());
    }

    /**
     * Defines behavior to adopt when node children are successfully loaded.
     * 
     * @param loadConfig
     *            The loading configuration.
     * @param result
     *            list of children.
     */
    @Override
    protected void onLoadSuccess(Object loadConfig, List<RecordModelData> result)
    {
        if (loadConfig != null && children.contains(loadConfig))
        {
            RecordModelData parent = (RecordModelData) loadConfig;
            if (ShotModelData.CLASS_NAME.equals(parent.getClassName()))
            {
                TreeLoadEvent evt = new TreeLoadEvent(this, loadConfig, result);
                evt.parent = parent;
                fireEvent(Load, evt);
                children.remove(loadConfig);
            }
        }
        super.onLoadSuccess(loadConfig, result);
    }

    /**
     * Update order by parameter because constituent name is contained in label field.
     */
    @Override
    protected void loadLeaves(RecordModelData parent)
    {
        servicesProxy.clearParameters();
        List<String> columns = new ArrayList<String>();
        columns.add(ShotModelData.SHOT_LABEL);
        servicesProxy.addParameter(new OrderBy(columns));

        super.loadLeaves(parent);
    }
}
