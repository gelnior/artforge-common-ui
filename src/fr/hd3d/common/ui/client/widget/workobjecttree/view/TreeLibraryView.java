package fr.hd3d.common.ui.client.widget.workobjecttree.view;

import java.util.List;

import org.restlet.client.data.Method;

import com.extjs.gxt.ui.client.Style.Scroll;
import com.extjs.gxt.ui.client.Style.SelectionMode;
import com.extjs.gxt.ui.client.data.BaseTreeModel;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.dnd.TreePanelDragSource;
import com.extjs.gxt.ui.client.dnd.TreePanelDropTarget;
import com.extjs.gxt.ui.client.dnd.DND.Feedback;
import com.extjs.gxt.ui.client.dnd.DND.Operation;
import com.extjs.gxt.ui.client.event.DNDEvent;
import com.extjs.gxt.ui.client.event.DNDListener;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.SelectionChangedEvent;
import com.extjs.gxt.ui.client.event.SelectionChangedListener;
import com.extjs.gxt.ui.client.event.TreePanelEvent;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.TreeStoreModel;
import com.extjs.gxt.ui.client.util.IconHelper;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.menu.Menu;
import com.extjs.gxt.ui.client.widget.treepanel.TreePanel;
import com.extjs.gxt.ui.client.widget.treepanel.TreePanel.TreeNode;
import com.google.gwt.user.client.Element;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.widget.workobjecttree.events.TreeEvents;
import fr.hd3d.common.ui.client.widget.workobjecttree.model.TreeLibraryModel;
import fr.hd3d.common.ui.client.widget.workobjecttree.model.callback.PutDragAndDropCallBack;
import fr.hd3d.common.ui.client.widget.workobjecttree.util.LoadHandle;
import fr.hd3d.common.ui.client.widget.workobjecttree.view.listeners.ExpandTreeLibraryListener;


public class TreeLibraryView extends ContentPanel
{
    private TreeLibraryModel treeLibraryModel = null;
    private boolean withDragAndDrop = false;
    private SelectionMode selectionMode = SelectionMode.SINGLE;
    private String url = "";
    private BaseTreeModel itemSel = null;
    private TreePanel<BaseTreeModel> tree = null;
    public boolean deleteItem = false;
    private LoadHandle loadHandle = null;
    private boolean bLoadingCategory = false;
    private Menu menuTreeLibraryView = null;

    public TreeLibraryView(TreeLibraryModel treeModel, boolean withDragAndDrop)
    {
        super();
        this.treeLibraryModel = treeModel;
        this.withDragAndDrop = withDragAndDrop;

        this.setId("library-tree");
    }

    public boolean isWithDragAndDrop()
    {
        return withDragAndDrop;
    }

    public Menu getMenuTreeLibraryView()
    {
        return menuTreeLibraryView;
    }

    public void setMenuTreeLibraryView(Menu menuTreeLibraryView)
    {
        this.menuTreeLibraryView = menuTreeLibraryView;
    }

    public void setWithDragAndDrop(boolean withDragAndDrop)
    {
        this.withDragAndDrop = withDragAndDrop;
    }

    public BaseTreeModel getItemSel()
    {
        return itemSel;
    }

    public void setItemSel()
    {
        itemSel = tree.getSelectionModel().getSelectedItem();
    }

    public TreePanel<BaseTreeModel> getTree()
    {
        return tree;
    }

    public TreeLibraryModel getTreeLibraryModel()
    {
        return treeLibraryModel;
    }

    public boolean isBLoadingCategory()
    {
        return bLoadingCategory;
    }

    public void setBLoadingCategory(boolean loadingCategory)
    {
        bLoadingCategory = loadingCategory;
    }

    public SelectionMode getSelectionMode()
    {
        return selectionMode;
    }

    public void setSelectionMode(SelectionMode selectionMode)
    {
        this.selectionMode = selectionMode;
    }

    public void setSelectionModeMulti()
    {
        this.selectionMode = SelectionMode.MULTI;
    }

    @Override
    protected void onRender(Element parent, int index)
    {
        super.onRender(parent, index);
        this.setLayout(new FitLayout());
        this.setScrollMode(Scroll.AUTO);
        this.setHeading("<strong>Library</strong>");
        this.setWidth("100%");
        this.setHeight("100%");
        this.tree = new TreePanel<BaseTreeModel>(this.treeLibraryModel.getTreeStore()) {
            @Override
            protected boolean hasChildren(BaseTreeModel model)
            {
                if (!"constituent".equals(model.get("type")))
                {
                    return true;
                }
                return super.hasChildren(model);
            }
        };
        this.tree.setDisplayProperty("label");
        this.tree.setAutoLoad(true);
        this.tree.getStyle().setLeafIcon(IconHelper.createStyle("add-constituent"));

        // this.tree.getStyle().setNodeOpenIcon(IconHelper.createPath("images/folder_picture.png"));
        // this.tree.getStyle().setNodeCloseIcon(IconHelper.createPath("images/folder_picture.png"));

        this.tree.setEnabled(false);
        this.tree.getSelectionModel().setSelectionMode(selectionMode);

        this.tree.addListener(Events.BeforeExpand, new ExpandTreeLibraryListener());
        //
        // tree.addListener(Events.OnDoubleClick, new Listener<TreePanelEvent<BaseTreeModel>>() {
        //
        // public void handleEvent(TreePanelEvent<BaseTreeModel> be)
        // {
        // TreeNode node = (TreeNode) be.getNode();
        // }
        // });

        // tree.addListener(Events.OnClick, new Listener<TreePanelEvent<BaseTreeModel>>() {
        //
        // public void handleEvent(TreePanelEvent<BaseTreeModel> be)
        // {
        // if (!be.isRightClick())
        // {
        // TreeNode node = be.getNode();
        // if (node != null)
        // {
        //
        // }
        // }
        // else
        // {
        // // RightClick
        // TreeNode node = be.getNode();
        // if (node != null)
        // {
        // // ContextMenu
        // BaseTreeModel btm = (BaseTreeModel) node.getModel();
        // AppEvent event = new AppEvent(TreeEvents.TREE_LIBRARY_MENU, btm);
        // EventDispatcher.forwardEvent(event);
        // }
        // }
        //
        // };
        // });

        tree.addListener(Events.OnClick, new Listener<TreePanelEvent<BaseTreeModel>>() {
            @SuppressWarnings("unchecked")
            public void handleEvent(TreePanelEvent<BaseTreeModel> be)
            {
                TreeNode node = be.getNode();
                if (node == null)
                {
                    tree.getSelectionModel().deselectAll();
                    setItemSel();
                    tree.setContextMenu(null);
                }
            }
        });

        this.tree.getSelectionModel().addSelectionChangedListener(new SelectionChangedListener<BaseTreeModel>() {
            @Override
            public void selectionChanged(SelectionChangedEvent<BaseTreeModel> se)
            {
                fireEvent(Events.SelectionChange, se);
                if (se.getSelection().size() > 0)
                {
                    AppEvent event = new AppEvent(TreeEvents.TREE_LIBRARY_SELECTION, se.getSelection().get(0));
                    EventDispatcher.forwardEvent(event);
                    AppEvent event2 = new AppEvent(TreeEvents.TREE_LIBRARY_SELECTION_MULTI, se.getSelection());
                    EventDispatcher.forwardEvent(event2);
                }
            }
        });

        this.tree.addListener(Events.Expand, new Listener<TreePanelEvent<ModelData>>() {
            public void handleEvent(TreePanelEvent<ModelData> te)
            {
                if (loadHandle != null && isLibraryItem((Hd3dModelData) te.getItem()) && bLoadingCategory == true)
                {
                    loadHandle.onLoadHandle();
                    bLoadingCategory = false;
                }
            }
        });

        if (withDragAndDrop)
        {

            TreePanelDragSource treeDragSource = new TreePanelDragSource(this.tree) {
                // @Override
                // public void onDragDrop(DNDEvent e)
                // {}
            };

            treeDragSource.addDNDListener(new DNDListener() {
                @Override
                public void dragStart(DNDEvent e)
                {
                    ModelData sel = tree.getSelectionModel().getSelectedItem();
                    if (sel != null && sel == tree.getStore().getRootItems().get(0))
                    {
                        e.setCancelled(true);
                        e.getStatus().setStatus(false);
                        return;
                    }

                    super.dragStart(e);
                }
            });

            treeDragSource.setGroup("constituent");

            TreePanelDropTarget treeDropTarget = new TreePanelDropTarget(this.tree) {
                @SuppressWarnings("unchecked")
                @Override
                public void onDragDrop(DNDEvent event)
                {
                    // Drag And Drop intern in tree
                    TreeNode item = tree.findNode(event.getTarget());
                    if (item == null)
                    {
                        return;
                    }
                    ModelData md = item.getModel();
                    Object type = md.get("type");
                    if (!type.equals("category"))
                    {
                        return;
                    }
                    final List<TreeStoreModel> listSel = (List<TreeStoreModel>) event.getData();

                    for (TreeStoreModel treeStoreModel : listSel)
                    {
                        final ModelData md2 = treeStoreModel.getModel();

                        // String value = md2.get("label");
                        Object id2 = md2.get("id");
                        String itemId = id2.toString();
                        Object id = md.get("id");
                        String parentId = id.toString();

                        String params = "{\"class\":\"fr.hd3d.model.lightweight.impl.LConstituent\", \"id\":" + itemId
                                + ", \"category\":" + parentId + "}";

                        url = "constituents/" + itemId;

                        PutDragAndDropCallBack putDragAndDropCallBack = new PutDragAndDropCallBack(treeLibraryModel
                                .getTreeStore(), (BaseTreeModel) md2);

                        IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
                        try
                        {
                            requestHandler.handleRequest(Method.PUT, url, params, putDragAndDropCallBack);
                        }
                        catch (Exception exception)
                        {
                            // TODO : Erreur
                        }
                    }
                    super.onDragDrop(event);
                }
            };

            treeDropTarget.setAllowSelfAsSource(true);
            treeDropTarget.setFeedback(Feedback.APPEND);
            treeDropTarget.setOperation(Operation.MOVE);

            treeDropTarget.setGroup("constituent");
            treeDropTarget.setAutoExpand(true);
            treeDropTarget.setAutoExpandDelay(800);
        }
        add(this.tree);
    }

    public void initExpand()
    {
        this.tree.setExpanded(this.tree.getStore().getAllItems().get(0), true, false);
    }

    public boolean isLibraryItem(Hd3dModelData tI)
    {
        String type = tI.get("type");
        return tI == null ? false : type.compareTo("library") == 0;
    }

    public void setLoadHandle(LoadHandle loadHandle)
    {
        this.loadHandle = loadHandle;
    }
}
