package fr.hd3d.common.ui.client.widget.workobjecttree.model.callback;

import org.restlet.client.Request;
import org.restlet.client.Response;

import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.widget.workobjecttree.model.TreeTemporalModel;


public class AllSequencesCallBack extends BaseCallback
{
    private final TreeTemporalModel treeTemporalModel;

    public AllSequencesCallBack(TreeTemporalModel treeTemporalModel)
    {
        this.treeTemporalModel = treeTemporalModel;

    }

    @Override
    public void onSuccess(Request request, Response response)
    {
    // JsonRepresentation representation = response.getEntityAsJson();
    // ModelType sequenceModelType = SequenceModelData.getModelType();
    //
    // SequenceModelData loadConfigSequencesModelData = new SequenceModelData();
    // SequenceReader jsonReader = new SequenceReader(sequenceModelType);
    // ListLoadResult<SequenceModelData> result = jsonReader.read(loadConfigSequencesModelData, representation
    // .getText());
    //
    // ListStore<SequenceModelData> treeStore = this.treeTemporalModel.getListSequences();
    // treeStore.removeAll();
    // treeStore.add(result.getData());
    }
}
