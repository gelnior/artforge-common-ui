package fr.hd3d.common.ui.client.widget.workobjecttree;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.modeldata.reader.SequenceReader;
import fr.hd3d.common.ui.client.service.proxy.TreeServicesProxy;
import fr.hd3d.common.ui.client.widget.basetree.BaseTreeDataLoader;


/**
 * Sequence tree loader specifies which URL should be used to load a specific node in sequence tree.
 * 
 * @author HD3D
 */
public class SequenceTreeLoader extends BaseTreeDataLoader<ShotModelData, SequenceModelData>
{
    /**
     * Default constructor sets proxy, reader and default instance.
     * 
     * @param proxy
     *            The proxy needed to access to services.
     */
    public SequenceTreeLoader(TreeServicesProxy<RecordModelData> proxy)
    {
        super(proxy, null, new SequenceReader(), null, new SequenceModelData());
    }
}
