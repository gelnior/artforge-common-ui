package fr.hd3d.common.ui.client.widget.workobjecttree.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.restlet.client.data.Method;

import com.extjs.gxt.ui.client.data.BaseModelData;
import com.extjs.gxt.ui.client.data.BaseTreeModel;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.mvc.Controller;
import com.extjs.gxt.ui.client.util.DelayedTask;
import com.extjs.gxt.ui.client.widget.Info;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.widget.workobjecttree.Urls;
import fr.hd3d.common.ui.client.widget.workobjecttree.events.TreeEvents;
import fr.hd3d.common.ui.client.widget.workobjecttree.model.TreeTagModel;
import fr.hd3d.common.ui.client.widget.workobjecttree.model.callback.AllTagCategoriesCallBack;
import fr.hd3d.common.ui.client.widget.workobjecttree.model.callback.DeleteInTagTreeCallBack;
import fr.hd3d.common.ui.client.widget.workobjecttree.model.callback.PostCallback;
import fr.hd3d.common.ui.client.widget.workobjecttree.model.callback.PutCallback;
import fr.hd3d.common.ui.client.widget.workobjecttree.view.TreeTagView;


/** Controller which handles grid events */
public class TreeTagController extends Controller
{

    boolean _ignoreCheckChange = false;

    /** Model which handles grid data */
    private final TreeTagModel model;
    /** View which handles widget */
    private final TreeTagView treeTag;
    private final DelayedTask _dt_taglistmsg = new DelayedTask(new Listener<BaseEvent>() {
        public void handleEvent(final BaseEvent be)
        {
            EventDispatcher.forwardEvent(TreeEvents.SEND_TREE_TAG_CHECKED_LIST);
        }
    });;

    /** Default constructor */
    public TreeTagController(TreeTagView treeTag, TreeTagModel model)
    {
        this.model = model;
        this.treeTag = treeTag;

        this.registerView();
        this.registerEvents();

        this.model.initTree();
    }

    /** Register all events the controller can handle. */
    private void registerEvents()
    {
        this.registerEventTypes(TreeEvents.TREE_LOAD_PROJECT);
        this.registerEventTypes(TreeEvents.TREE_TAG_EXPAND);
        this.registerEventTypes(TreeEvents.ACTION_IF_NOT_EXIST_IN_TAGS);
        this.registerEventTypes(TreeEvents.VIEW_LIBRARY_DETAILS);
        this.registerEventTypes(TreeEvents.ADD_TAGCATEGORY_NODE);
        this.registerEventTypes(TreeEvents.ADD_TAG_NODE);
        this.registerEventTypes(TreeEvents.RENAME_TAGCATEGORY_NODE);
        this.registerEventTypes(TreeEvents.RENAME_TAG_NODE);
        this.registerEventTypes(TreeEvents.DELETE_TAGCATEGORY_NODE);
        this.registerEventTypes(TreeEvents.DELETE_TAG_NODE);
        this.registerEventTypes(TreeEvents.SET_EXPAND_NODE_TAG);
        this.registerEventTypes(TreeEvents.CLEAR_TREE_TAG_SELECTION);
        this.registerEventTypes(TreeEvents.REFRESH_TREE_TAG_CATEGORIES);
        this.registerEventTypes(TreeEvents.RECURSE_AND_SET_CHECKED_TREE_TAG);
        this.registerEventTypes(TreeEvents.SET_TREE_TAG_NODE_CHECKED);
        this.registerEventTypes(TreeEvents.TREE_TAG_CLICKED);
        this.registerEventTypes(TreeEvents.PREPARE_TAG_LIST_MSG);
        this.registerEventTypes(TreeEvents.SEND_TREE_TAG_CHECKED_LIST);
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();

        if (type == TreeEvents.SEND_TREE_TAG_CHECKED_LIST)
        {
            onSendCheckedList();
        }
        else if (type == TreeEvents.PREPARE_TAG_LIST_MSG)
        {
            _dt_taglistmsg.delay(20);
        }
        else if (type == TreeEvents.TREE_TAG_CLICKED)
        {
            onTreeTagClicked(event);
        }
        else if (type == TreeEvents.SET_TREE_TAG_NODE_CHECKED)
        {
            BaseTreeModel item = (BaseTreeModel) event.getData();
            treeTag.checkItemsRecursive(item, true);
            _ignoreCheckChange = false;
        }
        else if (type == TreeEvents.RECURSE_AND_SET_CHECKED_TREE_TAG)
        {
            if (!_ignoreCheckChange)
            {
                _ignoreCheckChange = true;
                this.model.populateItemRecursive((BaseTreeModel) event.getData(), TreeEvents.SET_TREE_TAG_NODE_CHECKED);
            }
        }
        else if (type == TreeEvents.TREE_LOAD_PROJECT)
        {
            ProjectModelData projectModelData = (ProjectModelData) event.getData();
            this.model.setCurrentProject(projectModelData);
            this.onTreeLoadProject();
        }
        else if (type == TreeEvents.TREE_TAG_EXPAND)
        {
            this.onExpandTree((BaseTreeModel) event.getData());
        }
        else if (type == TreeEvents.ACTION_IF_NOT_EXIST_IN_TAGS)
        {
            this.onActionIfNotExist(event);
        }
        else if (type == TreeEvents.ADD_TAGCATEGORY_NODE)
        {
            this.onAddTagCategoryNode(event);
        }
        else if (type == TreeEvents.ADD_TAG_NODE)
        {
            this.onAddTagNode(event);
        }
        else if (type == TreeEvents.RENAME_TAGCATEGORY_NODE)
        {
            this.onRenameCategoryNode(event);
        }
        else if (type == TreeEvents.RENAME_TAG_NODE)
        {
            this.onRenameTagNode(event);
        }
        else if (type == TreeEvents.DELETE_TAGCATEGORY_NODE)
        {
            this.onDeleteCategoryNode(event);
        }
        else if (type == TreeEvents.DELETE_TAG_NODE)
        {
            this.onDeleteConstituentNode(event);
        }
        else if (type == TreeEvents.SET_EXPAND_NODE_TAG)
        {
            this.setExpandNode((BaseTreeModel) event.getData());
        }
        else if (type == TreeEvents.CLEAR_TREE_TAG_SELECTION)
        {
            this.treeTag.getTree().getSelectionModel().deselectAll();
        }
        else if (type == TreeEvents.REFRESH_TREE_TAG_CATEGORIES)
        {
            refreshTagCategories();
        }
        else
        {
            this.forwardToChild(event);
        }
    }

    private void onSendCheckedList()
    {
        List<Long> data = new ArrayList<Long>();
        List<BaseTreeModel> l = this.treeTag.getTree().getCheckedSelection();
        for (BaseTreeModel item : l)
        {
            Object type = item.get("type");
            if (type.equals("tag"))
            {
                data.add((Long) item.get("id"));
            }
        }

        AppEvent newevent = new AppEvent(TreeEvents.TREE_TAG_CHECKED_LIST_CHANGED, data);
        EventDispatcher.forwardEvent(newevent);
    }

    private void onTreeTagClicked(AppEvent event)
    {
        BaseTreeModel item = (BaseTreeModel) event.getData();
        if (!treeTag.getTree().isChecked(item))
        {
            Object type = item.get("type");
            if (type.equals("tagcategory"))
            {
                if (model.getTreeStore().getChildCount(item) == 0)
                {
                    AppEvent newevent = new AppEvent(TreeEvents.RECURSE_AND_SET_CHECKED_TREE_TAG, item);
                    EventDispatcher.forwardEvent(newevent);
                }
                else
                {
                    treeTag.checkItemsRecursive(item, true);
                }
            }
            else
            {
                AppEvent newevent = new AppEvent(TreeEvents.PREPARE_TAG_LIST_MSG, item);
                EventDispatcher.forwardEvent(newevent);
            }
        }
        else
        {
            Object type = item.get("type");
            if (type.equals("tagcategory"))
            {
                treeTag.checkItemsRecursive(item, false);
            }
            else
            {
                AppEvent newevent = new AppEvent(TreeEvents.PREPARE_TAG_LIST_MSG, item);
                EventDispatcher.forwardEvent(newevent);
            }
        }
    }

    /** Retrieve data from XML configuration and sets static configuration variable. */
    private void onExpandTree(BaseTreeModel itemSel)
    {
        if (this.treeTag.getTree().getStore().getChildren(itemSel) != null
                && this.treeTag.getTree().getStore().getChildren(itemSel).size() == 0)
        {
            this.model.populateItem(itemSel, TreeEvents.SET_EXPAND_NODE_TAG);
            this.treeTag.setBLoadingCategory(true);
        }
    }

    // private void setExpandNodeRecursive(BaseTreeModel modelData)
    // {
    // this.treeTag.getTree().setExpanded(modelData, true, true);
    // }

    private void setExpandNode(BaseTreeModel modelData)
    {
        this.treeTag.getTree().setExpanded(modelData, true, false);
    }

    public void onAddTagCategoryNode(AppEvent event)
    {
        BaseTreeModel parent = (BaseTreeModel) event.getData("ITEM");
        if (parent == null)
        {
            return;
        }
        String value = event.getData("VALUE");
        BaseTreeModel baseTreeModel = new BaseTreeModel();

        baseTreeModel.set("name", value);
        baseTreeModel.set("label", value);
        baseTreeModel.set("type", "tagcategory");
        baseTreeModel.set("parentId", parent.get("id"));
        baseTreeModel.set("parentBoundTags", parent.get("boundTags"));
        String params = "{\"class\":\"fr.hd3d.model.lightweight.impl.LTagCategory\", \"name\":\"" + value + "\"}";

        Object obj = parent.get("type");

        if (!"tags".equals(obj.toString()))
        {
            Long id = parent.get(CategoryModelData.ID_FIELD);
            String parentId = id.toString();
            params = "{\"class\":\"fr.hd3d.model.lightweight.impl.LTagCategory\", \"name\":\"" + value
                    + "\", \"parent\":" + parentId + "}";
        }

        String url = Urls.urlTagCat;
        PostCallback postCallback = new PostCallback(model.getTreeStore(), baseTreeModel, parent);

        IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
        try
        {
            requestHandler.handleRequest(Method.POST, url, params, postCallback);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            // TODO : Erreur
        }
    }

    public void onAddTagNode(AppEvent event)
    {
        BaseTreeModel parent = (BaseTreeModel) event.getData("ITEM");
        String value = event.getData("VALUE");
        String url = null;
        BaseTreeModel baseTreeModel = new BaseTreeModel();
        if (parent == null)
        {
            return;
        }
        baseTreeModel.set("name", value);
        baseTreeModel.set("label", value);
        baseTreeModel.set("parentTags", parent.get("boundTags"));
        baseTreeModel.set("parentLabel", parent.get("label"));
        baseTreeModel.set("parentId", parent.get("id"));
        baseTreeModel.set("type", "tag");
        // Long id = parent.get(CategoriesModelData.ID_FIELD);
        //
        // final String hook = model.getHook(baseTreeModel, parent);

        String params = "{\"class\":\"fr.hd3d.model.lightweight.impl.LTag\",\"name\":\"" + value + "\"}";
        url = Urls.urlTags;
        PostCallback postCallback = new PostCallback(model.getTreeStore(), baseTreeModel, parent);

        IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
        try
        {
            System.out.println("POST TAG: " + params);
            requestHandler.handleRequest(Method.POST, url, params, postCallback);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void onRenameCategoryNode(AppEvent event)
    {
        BaseTreeModel item = (BaseTreeModel) event.getData("ITEM");
        String value = event.getData("VALUE");
        String url = null;
        if (item != null)
        {

            BaseTreeModel modelItem = item;
            Long id = modelItem.get(CategoryModelData.ID_FIELD);
            String itemId = id.toString();
            String params = null;

            String type = modelItem.get("type");
            if ("tags".equals(type))
            {
                return;
            }
            BaseModelData parent = model.getTreeStore().getParent(item);
            String parentType = parent.get("type");
            if ("tags".equals(parentType))
            {
                params = "{\"class\":\"fr.hd3d.model.lightweight.impl.LTagCategory\", \"id\":" + itemId
                        + ", \"name\":\"" + value + "\"" + "}";
                // TODO parent = 0 ?
            }
            else
            {
                Long parentIdLong = parent.get("id");
                String parentId = parentIdLong.toString();
                params = "{\"class\":\"fr.hd3d.model.lightweight.impl.LTagCategory\", \"id\":" + itemId
                        + ", \"name\":\"" + value + "\", \"parent\":" + parentId + "}";
            }
            url = Urls.urlTagCat + "/" + itemId;
            PutCallback putCallback = new PutCallback(model.getTreeStore(), item, value);

            IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
            try
            {
                requestHandler.handleRequest(Method.PUT, url, params, putCallback);
            }

            catch (Exception e)
            {
                // TODO : Erreur
            }
        }
    }

    public String boolInIntString(Boolean boolVal)
    {
        if (!boolVal)
        {
            return "0";
        }
        else
        {
            return "1";
        }
    }

    public void onRenameTagNode(AppEvent event)
    {

        BaseTreeModel item = (BaseTreeModel) event.getData("ITEM");
        String value = event.getData("VALUE");
        String url = null;
        if (item == null)
        {
            return;
        }
        BaseTreeModel modelItem = item;

        Long itemIdLong = modelItem.get("id");
        String itemId = itemIdLong.toString();
        // Long parentIdLong = model.getTreeStore().getParent(item).get("id");
        // String parentId = parentIdLong.toString();

        String params = "{\"class\":\"fr.hd3d.model.lightweight.impl.LTag\", \"id\":" + itemId + ", \"name\":\""
                + value + "\"}";

        url = Urls.urlTags + "/" + itemId;
        PutCallback putCallback = new PutCallback(model.getTreeStore(), item, value);

        IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
        try
        {
            requestHandler.handleRequest(Method.PUT, url, params, putCallback);
        }
        catch (Exception e)
        {
            // TODO : Erreur
        }
    }

    public void onDeleteCategoryNode(AppEvent event)
    {
        BaseTreeModel item = (BaseTreeModel) event.getData("ITEM");
        if (item != null)
        {
            ModelData modelItem = item;

            Long idLong = modelItem.get("id");
            String itemId = idLong.toString();

            String type = modelItem.get("type");
            if (!"tags".equals(type))
            {
                String url = Urls.urlTagCat + "/" + itemId;
                DeleteInTagTreeCallBack deleteInTagTreeCallBack = new DeleteInTagTreeCallBack(model.getTreeStore(),
                        item);

                IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
                try
                {
                    requestHandler.handleRequest(Method.DELETE, url, null, deleteInTagTreeCallBack);
                }
                catch (Exception e)
                {
                    // TODO : Erreur
                }
            }
        }
    }

    public void onDeleteConstituentNode(AppEvent event)
    {
        BaseTreeModel item = (BaseTreeModel) event.getData("ITEM");
        if (item != null)
        {
            ModelData modelItem = item;
            Long idLong = modelItem.get("id");
            String itemId = idLong.toString();
            // Long parentIdLong = model.getTreeStore().getParent(item).get("id");
            // String parentId = parentIdLong.toString();

            if (!"tags".equals(modelItem.get("type")))
            {
                String url = Urls.urlTags + "/" + itemId;
                DeleteInTagTreeCallBack deleteInTagTreeCallBack = new DeleteInTagTreeCallBack(model.getTreeStore(),
                        item);
                IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
                try
                {
                    requestHandler.handleRequest(Method.DELETE, url, null, deleteInTagTreeCallBack);
                }
                catch (Exception e)
                {
                    // TODO : Erreur
                }
            }
        }
    }

    public void onActionIfNotExist(AppEvent event)
    {
        EventType action = event.getData("ACTION");
        BaseTreeModel item = (BaseTreeModel) event.getData("ITEM");
        String value = event.getData("VALUE");

        if (isExist(item, value))
        {
            return;
        }
        createEvent(item, value, action);
    }

    public boolean isExist(BaseTreeModel item, String value)
    {
        if (item != null)
        {
            List<BaseTreeModel> lst = model.getTreeStore().getChildren(item);
            Iterator<BaseTreeModel> it = lst.iterator();
            while (it.hasNext())
            {
                ModelData md = it.next();
                String label = md.get("label");
                if (label.equals(value))
                {
                    Info.display("Warning", value + "  already exist in " + item);
                    return true;
                }
            }
        }
        return false;
    }

    private void createEvent(BaseTreeModel item, String value, EventType treeEvent)
    {
        AppEvent event = new AppEvent(treeEvent);
        event.setData("VALUE", value);
        event.setData("ITEM", item);
        EventDispatcher.forwardEvent(event);
    }

    private void onTreeLoadProject()
    {
        this.treeTag.getTree().getSelectionModel().deselectAll();
        this.model.initTree();
        this.refreshTagCategories();

        this.treeTag.enable();
        this.treeTag.getTree().setExpanded(this.model.getTreeStore().getAllItems().get(0), true, false);
    }

    private void registerView()
    {}

    private void refreshTagCategories()
    {
        if (this.model.getCurrentProject() != null) // project selectionne
        {
            String urlCategories = Urls.urlTagCat + "?orderBy=[\"name\"]";

            IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
            try
            {
                requestHandler.handleRequest(Method.GET, urlCategories, null, new AllTagCategoriesCallBack(this.model));
            }
            catch (Exception e)
            {
                // TODO : Erreur
            }
        }
    }

}
