package fr.hd3d.common.ui.client.widget.workobjecttree;

import java.util.ArrayList;

import com.extjs.gxt.ui.client.event.SelectionChangedEvent;
import com.extjs.gxt.ui.client.event.SelectionChangedListener;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;


public class ShotChangedListener extends SelectionChangedListener<RecordModelData>
{
    private final ShotTree shotTree;

    public ShotChangedListener(ShotTree shotTree)
    {
        this.shotTree = shotTree;
    }

    @Override
    public void selectionChanged(SelectionChangedEvent<RecordModelData> se)
    {
        RecordModelData firstSelection = se.getSelectedItem();

        if (firstSelection != null && firstSelection.getClassName().equals(ShotModelData.CLASS_NAME))
        {

            ArrayList<ShotModelData> shotList = new ArrayList<ShotModelData>();
            for (RecordModelData item : se.getSelection())
            {
                if (item.getClassName().equals(ShotModelData.CLASS_NAME))
                {
                    ShotModelData shot = new ShotModelData();
                    Hd3dModelData.copy(item, shot);
                    shotList.add(shot);
                }
            }

            this.shotTree.onShotClicked(shotList);
        }

        if (firstSelection != null && firstSelection.getClassName().equals(SequenceModelData.CLASS_NAME))
        {

            ArrayList<SequenceModelData> sequenceList = new ArrayList<SequenceModelData>();
            for (RecordModelData item : se.getSelection())
            {
                if (item.getClassName().equals(SequenceModelData.CLASS_NAME))
                {
                    SequenceModelData sequence = new SequenceModelData();
                    Hd3dModelData.copy(item, sequence);
                    sequenceList.add(sequence);
                }
            }

            this.shotTree.onSequenceClicked(sequenceList);
        }
    }
}
