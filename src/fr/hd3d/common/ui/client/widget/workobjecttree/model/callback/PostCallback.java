package fr.hd3d.common.ui.client.widget.workobjecttree.model.callback;

import java.util.ArrayList;
import java.util.List;

import org.restlet.client.Request;
import org.restlet.client.Response;
import org.restlet.client.data.Method;

import com.extjs.gxt.ui.client.data.BaseTreeModel;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.store.TreeStore;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.widget.workobjecttree.Actions;
import fr.hd3d.common.ui.client.widget.workobjecttree.Urls;
import fr.hd3d.common.ui.client.widget.workobjecttree.events.TreeEvents;


public class PostCallback extends BaseCallback
{
    private final TreeStore<BaseTreeModel> treeStore;
    private final BaseTreeModel _baseTreeModel;
    private final BaseTreeModel parent;

    public PostCallback(TreeStore<BaseTreeModel> treeStore, BaseTreeModel baseTreeModel, BaseTreeModel parent)
    {
        this.treeStore = treeStore;
        this._baseTreeModel = baseTreeModel;
        this.parent = parent;
    }

    @Override
    public void onSuccess(Request request, Response response)
    {

        String identifier = response.getEntity().getIdentifier().toString();
        String id = identifier.substring(identifier.lastIndexOf("/") + 1);

        _baseTreeModel.set("id", new Long(id));

        // Info.display("Save Result", "The object " + _baseTreeModel.get("label") +
        // " have been correctly created");

        // treeStore.add(item, _baseTreeModel, true);
        int nbChildren = insertSort();

        // BUG : treeStore.sort("label", SortDir.ASC);

        if ("category".equals(parent.get("type")) || "library".equals(parent.get("type")))
        {
            EventDispatcher.forwardEvent(new AppEvent(TreeEvents.REFRESH_TREE_LIBRARY_CATEGORIES));
            if (nbChildren == 1)
            {
                treeStore.removeAll(parent);
                AppEvent event1 = new AppEvent(TreeEvents.TREE_LIBRARY_EXPAND, parent);
                EventDispatcher.forwardEvent(event1);
            }
        }
        else if ("tagcategory".equals(parent.get("type")))
        {
            if ("tag".equals(_baseTreeModel.get("type")))
            {
                updateParentTagCategory();
            }

            EventDispatcher.forwardEvent(new AppEvent(TreeEvents.REFRESH_TREE_TAG_CATEGORIES));
            AppEvent event = new AppEvent(TreeEvents.SET_EXPAND_NODE_TAG, parent);
            EventDispatcher.forwardEvent(event);
        }
        else
        {
            EventDispatcher.forwardEvent(new AppEvent(TreeEvents.REFRESH_TREE_TEMPORAL_SEQUENCES));
            if (nbChildren == 1)
            {
                treeStore.removeAll(parent);
                AppEvent event2 = new AppEvent(TreeEvents.TREE_TEMPORAL_EXPAND, parent);
                EventDispatcher.forwardEvent(event2);
            }
        }

        AppEvent event = new AppEvent(TreeEvents.REFRESH_VIEWS);
        event.setData("ACTION", Actions.REFRESH_AFTER_POST);

        if ("shot".equals(_baseTreeModel.get("type")) || "sequence".equals(_baseTreeModel.get("type")))
        {
            event.setData("TYPE", "Temporal");
        }
        else
        {
            event.setData("TYPE", "Constituent");
        }
        EventDispatcher.forwardEvent(event);
    }

    private void updateParentTagCategory()
    {
        Long parentId = parent.get("id");
        List<Long> ltags = parent.get("boundTags");
        String tags = "";
        String sep = "";
        if (ltags == null)
        {
            ltags = new ArrayList<Long>();
        }
        for (Long tagId : ltags)
        {
            tags = tags + sep + tagId;
            sep = ",";
        }

        Long newId = _baseTreeModel.get("id");
        ltags.add(newId);
        parent.set("boundTags", ltags);
        tags = "[" + tags + sep + newId + "]";
        // System.out.println("PARENT TAGS: " + tags);
        String label = _baseTreeModel.get("parentLabel");
        String params = "{\"class\":\"fr.hd3d.model.lightweight.impl.LTagCategory\",\"id\":" + parentId
                + ",\"boundTags\":" + tags + "}";

        String url = Urls.urlTagCat + "/" + parentId;
        // System.out.println("=> " + url);
        // System.out.println("=> " + params);
        PutCallback putCallback = new PutCallback(this.treeStore, parent, label);

        IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
        try
        {
            requestHandler.handleRequest(Method.PUT, url, params, putCallback);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    // Sort and Insert return nbChildren
    private int insertSort()
    {
        List<BaseTreeModel> children = treeStore.getChildren(parent, false);
        if (children.size() != 0)
        {
            for (int i = 0; i < children.size(); i++)
            {
                if ((children.get(i).get("label").toString()).compareTo(_baseTreeModel.get("label").toString()) > 0)
                {
                    treeStore.insert(parent, _baseTreeModel, i, true);
                    return children.size() + 1;
                }
            }
        }

        treeStore.add(parent, _baseTreeModel, true);

        return 1;
    }
}
