package fr.hd3d.common.ui.client.widget.workobjecttree;

import fr.hd3d.common.client.Constraint;
import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.service.ServicesPath;


public class Urls
{
    public static void setProjectId(String id)
    {
        projectId = id;
        urlProject = ServicesPath.PROJECTS + projectId;
    }

    public static String projectId = "";
    public static final String urlProj = ServicesURI.PROJECTS;
    public static String urlProject = ServicesPath.PROJECTS + projectId;
    public static String urlTagCat = ServicesURI.TAG_CATEGORIES;
    public static String urlTags = ServicesURI.TAGS;
    public static final String urlCat = '/' + ServicesURI.CATEGORIES;
    public static final String urlSeq = '/' + ServicesURI.SEQUENCES;
    public static final String urlConstituent = '/' + ServicesURI.CONSTITUENTS;
    public static final String urlShot = '/' + ServicesURI.SHOTS;
    // public static final String urlContentUnit = "/contentunits";
    public static final String urlPerson = ServicesURI.PERSONS;
    public static final String urlGroup = ServicesURI.RESOURCEGROUPS;
    public static final String urlCasting = '/' + ServicesURI.CASTING;
    public static final String urlComposition = '/' + ServicesURI.COMPOSITIONS;
    public static final String urlTask = '/' + ServicesURI.TASKS;
    public static final String urlEntityTaskLinks = ServicesURI.ENTITYTASKLINKS;
    public static final String urlSheetTask = ServicesURI.SHEETS + '?' + Constraint.ORDERBY + "=[\"name\"]&"
            + Constraint.CONSTRAINT + "=[{\"" + Constraint.COLUMN_MAP_FIELD + "\":\"boundClassName\",\""
            + Constraint.TYPE_MAP_FIELD + "\":\"" + EConstraintOperator.eq + "\",\"" + Constraint.VALUE_MAP_FIELD
            + "\":\"Task\"}]";
    public static final String noDescription = "No description available";
    public static final String urlProjectOpen = ServicesURI.PROJECTS + '?' + Constraint.ORDERBY + "=[\"name\"]&"
            + Constraint.CONSTRAINT + "=[{\"" + Constraint.COLUMN_MAP_FIELD + "\":\"status\",\""
            + Constraint.TYPE_MAP_FIELD + "\":\"" + EConstraintOperator.eq + "\",\"" + Constraint.VALUE_MAP_FIELD
            + "\":\"open\"}]";
}
