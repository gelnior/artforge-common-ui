package fr.hd3d.common.ui.client.widget.workobjecttree.view;

import com.extjs.gxt.ui.client.Style.Scroll;
import com.extjs.gxt.ui.client.data.BaseTreeModel;
import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.KeyListener;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MenuEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.Window;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.FormPanel;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.layout.BorderLayout;
import com.extjs.gxt.ui.client.widget.layout.FormData;
import com.extjs.gxt.ui.client.widget.menu.Menu;
import com.extjs.gxt.ui.client.widget.menu.MenuItem;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.widget.workobjecttree.events.TreeEvents;
import fr.hd3d.common.ui.client.widget.workobjecttree.view.listeners.ViewElementLibraryListener;


public class MenuTreeTagView extends Menu
{
    final static int addTagCategoryNode = 0;
    final static int addTagNode = 1;
    final static int renameTagCategoryNode = 2;
    final static int deleteTagCategoryNode = 3;
    final static int renameTagNode = 4;
    final static int deleteTagNode = 5;

    final String[] _titres = { "Add sub-category", "Add tag", "Rename category", "Delete category", "Rename tag",
            "Delete tag" };

    BaseTreeModel item = null;

    MenuItem createMenuItem(final int itemIndex, final String style)
    {
        MenuItem ret = new MenuItem();
        ret.setText(_titres[itemIndex]);
        ret.setIconStyle(style);
        ret.addSelectionListener(new SelectionListener<MenuEvent>() {
            @Override
            public void componentSelected(MenuEvent ce)
            {
                displayForm(itemIndex);
            }
        });
        return ret;
    }

    public MenuTreeTagView(BaseTreeModel item)
    {
        super();
        this.setWidth(145);
        this.item = item;

        MenuItem addTagCategoryNodeItem = createMenuItem(addTagCategoryNode, "add-sub-category");
        MenuItem addTagNodeItem = createMenuItem(addTagNode, "add-constituent");
        MenuItem renameTagCategoryNodeItem = createMenuItem(renameTagCategoryNode, "rename");
        MenuItem deleteTagCategoryNodeItem = createMenuItem(deleteTagCategoryNode, "delete");
        deleteTagCategoryNodeItem.setEnabled(false);
        MenuItem renameTagNodeItem = createMenuItem(renameTagNode, "rename");
        MenuItem deleteTagNodeItem = createMenuItem(deleteTagNode, "delete");
        deleteTagNodeItem.removeAllListeners();
        deleteTagNodeItem.addSelectionListener(new SelectionListener<MenuEvent>() {
            @Override
            public void componentSelected(MenuEvent ce)
            {
                deleteActionDialog(deleteTagNode);
            }
        });

        String type = item.get("type");
        if (type.compareTo("tags") == 0)
        {
            // TODO
            // contextMenu.add(view);
            this.add(addTagCategoryNodeItem);
        }
        else if (type.compareTo("tagcategory") == 0)
        {
            this.add(addTagNodeItem);
            this.add(addTagCategoryNodeItem);
            this.add(renameTagCategoryNodeItem);
            this.add(deleteTagCategoryNodeItem);
        }
        else
        {
            this.add(renameTagNodeItem);
            this.add(deleteTagNodeItem);
        }
        ((MenuItem) getItem(0)).addSelectionListener(new ViewElementLibraryListener<MenuEvent>(true));
    }

    private void displayForm(final int action)
    {
        final Window w = new Window();
        w.setResizable(true);
        w.setWidth(305);
        w.setHeight(105);
        w.setMonitorWindowResize(true);
        w.setModal(true);
        w.setHeaderVisible(true);
        w.setLayout(new BorderLayout());
        w.setHeading(_titres[action]);
        w.setPlain(true);

        w.setPosition(211, 150);

        final FormPanel form2 = new FormPanel();
        form2.setHeaderVisible(false);
        form2.setBorders(false);
        form2.setBodyBorder(false);
        form2.setWidth(300);
        form2.setPadding(5);

        final TextField<String> field = new TextField<String>();
        field.setFieldLabel("Name");
        form2.add(field, new FormData("95%"));

        switch (action)
        {
            case renameTagCategoryNode:
                field.setValue((String) item.get("label"));
                break;
            case renameTagNode:
                field.setValue((String) item.get("label"));
                break;
        }

        Button saveB = new Button("Save");
        w.addButton(saveB);

        field.addKeyListener(new KeyListener() {
            @Override
            public void handleEvent(ComponentEvent event)
            {
                if (event.getType() == Events.KeyPress && event.getKeyCode() == 13)
                {
                    String value = field.getValue();
                    if (value.length() != 0)
                    {
                        EventType command = null;
                        if (action == addTagCategoryNode)
                        {
                            command = TreeEvents.ADD_TAGCATEGORY_NODE;
                        }
                        else if (action == addTagNode)
                        {
                            command = TreeEvents.ADD_TAG_NODE;
                        }
                        else if (action == renameTagCategoryNode)
                        {
                            command = TreeEvents.RENAME_TAGCATEGORY_NODE;
                        }
                        else if (action == renameTagNode)
                        {
                            command = TreeEvents.RENAME_TAG_NODE;
                        }
                        else if (action == addTagNode)
                        {}

                        AppEvent event1 = new AppEvent(TreeEvents.ACTION_IF_NOT_EXIST_IN_TAGS);
                        event1.setData("VALUE", value);
                        event1.setData("ITEM", item);
                        event1.setData("ACTION", command);
                        EventDispatcher.forwardEvent(event1);

                        w.hide();
                    }
                }
            }
        });

        saveB.addListener(Events.OnClick, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                String value = field.getValue();
                if (value.length() != 0)
                {
                    EventType command = null;
                    if (action == addTagCategoryNode)
                    {
                        command = TreeEvents.ADD_TAGCATEGORY_NODE;
                    }
                    else if (action == addTagNode)
                    {
                        command = TreeEvents.ADD_TAG_NODE;
                    }
                    else if (action == renameTagCategoryNode)
                    {
                        command = TreeEvents.RENAME_TAGCATEGORY_NODE;
                    }
                    else if (action == renameTagNode)
                    {
                        command = TreeEvents.RENAME_TAG_NODE;
                    }
                    else if (action == addTagNode)
                    {}

                    AppEvent event = new AppEvent(TreeEvents.ACTION_IF_NOT_EXIST_IN_TAGS);
                    event.setData("VALUE", value);
                    event.setData("ITEM", item);
                    event.setData("ACTION", command);
                    EventDispatcher.forwardEvent(event);

                    w.hide();
                }
            }
        });
        Button closeB = new Button("Cancel");
        w.addButton(closeB);
        closeB.addListener(Events.OnClick, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                w.hide();
            }
        });
        w.add(form2);
        w.show();
    }

    private void createEvent(BaseTreeModel item, String value, EventType treeEvent)
    {

        AppEvent event = new AppEvent(treeEvent);
        event.setData("VALUE", value);
        event.setData("ITEM", item);
        EventDispatcher.forwardEvent(event);
    }

    private void deleteActionDialog(final int action)
    {

        final Dialog simple = new Dialog();
        simple.setHeading("Confirmation");
        simple.setButtons(Dialog.YESNO);
        simple.setBodyStyleName("pad-text");
        simple.setModal(true);
        simple.addText("Are you sure to delete : " + item.get("label") + " ?");
        simple.setScrollMode(Scroll.AUTO);
        simple.setHideOnButtonClick(true);

        simple.setPosition(211, 150);

        Button okButton = (Button) simple.getButtonBar().getItemByItemId(Dialog.YES);
        okButton.addListener(Events.Select, new SelectionListener<ComponentEvent>() {
            @Override
            public void componentSelected(ComponentEvent ce)
            {
                deleteAction(action);
            }
        });
        simple.show();
    }

    private void deleteAction(int action)
    {
        switch (action)
        {
            case deleteTagCategoryNode:
                createEvent(item, null, TreeEvents.DELETE_TAGCATEGORY_NODE);
                break;
            case deleteTagNode:
                createEvent(item, null, TreeEvents.DELETE_TAG_NODE);
                break;
        }
    }
}
