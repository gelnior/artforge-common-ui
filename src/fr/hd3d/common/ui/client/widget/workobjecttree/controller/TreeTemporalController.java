package fr.hd3d.common.ui.client.widget.workobjecttree.controller;

import java.util.Iterator;
import java.util.List;

import org.restlet.client.data.Method;

import com.extjs.gxt.ui.client.data.BaseTreeModel;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.mvc.Controller;
import com.extjs.gxt.ui.client.widget.Info;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.widget.workobjecttree.Actions;
import fr.hd3d.common.ui.client.widget.workobjecttree.Urls;
import fr.hd3d.common.ui.client.widget.workobjecttree.events.TreeEvents;
import fr.hd3d.common.ui.client.widget.workobjecttree.model.TreeTemporalModel;
import fr.hd3d.common.ui.client.widget.workobjecttree.model.callback.AllSequencesCallBack;
import fr.hd3d.common.ui.client.widget.workobjecttree.model.callback.DeleteInTreeCallBack;
import fr.hd3d.common.ui.client.widget.workobjecttree.model.callback.PostCallback;
import fr.hd3d.common.ui.client.widget.workobjecttree.model.callback.PutCallback;
import fr.hd3d.common.ui.client.widget.workobjecttree.view.TreeTemporalView;


/** Controller which handles grid events */
public class TreeTemporalController extends Controller
{
    /** Model which handles grid data */
    private final TreeTemporalModel model;
    /** View which handles widget */
    private final TreeTemporalView treeTemporal;

    /** Default constructor */
    public TreeTemporalController(TreeTemporalView treeTemporal, TreeTemporalModel model)
    {
        this.model = model;
        this.treeTemporal = treeTemporal;

        this.registerView();
        this.registerEvents();

        this.model.initTree();
    }

    /** Register all events the controller can handle. */
    private void registerEvents()
    {
        this.registerEventTypes(TreeEvents.TREE_LOAD_PROJECT);
        this.registerEventTypes(TreeEvents.TREE_TEMPORAL_EXPAND);
        this.registerEventTypes(TreeEvents.ACTION_IF_NOT_EXIST_IN_TEMPORAL);
        this.registerEventTypes(TreeEvents.VIEW_TEMPORAL_DETAILS);
        this.registerEventTypes(TreeEvents.ADD_SEQUENCE_NODE);
        this.registerEventTypes(TreeEvents.ADD_SHOT_NODE);
        this.registerEventTypes(TreeEvents.RENAME_SEQUENCE_NODE);
        this.registerEventTypes(TreeEvents.RENAME_SHOT_NODE);
        this.registerEventTypes(TreeEvents.DELETE_SEQUENCE_NODE);
        this.registerEventTypes(TreeEvents.DELETE_SHOT_NODE);
        this.registerEventTypes(TreeEvents.SET_EXPAND_NODE_TEMPORAL);
        this.registerEventTypes(TreeEvents.CLEAR_TREE_TEMPORAL_SELECTION);
        this.registerEventTypes(TreeEvents.REFRESH_TREE_TEMPORAL_SEQUENCES);
        this.registerEventTypes(TreeEvents.REFRESH_TEMPORAL_NODE);
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();

        if (type == TreeEvents.TREE_LOAD_PROJECT)
        {
            ProjectModelData projectModelData = (ProjectModelData) event.getData();
            this.model.setCurrentProject(projectModelData);
            this.onTreeLoadProject();
        }
        else if (type == TreeEvents.TREE_TEMPORAL_EXPAND)
        {
            this.onExpandTree((BaseTreeModel) event.getData());
        }
        else if (type == TreeEvents.ACTION_IF_NOT_EXIST_IN_TEMPORAL)
        {
            this.onActionIfNotExist(event);
        }
        else if (type == TreeEvents.ADD_SEQUENCE_NODE)
        {
            this.onAddSequenceNode(event);
        }
        else if (type == TreeEvents.ADD_SHOT_NODE)
        {
            this.onAddShotNode(event);
        }
        else if (type == TreeEvents.RENAME_SEQUENCE_NODE)
        {
            this.onRenameSequenceNode(event);
        }
        else if (type == TreeEvents.RENAME_SHOT_NODE)
        {
            this.onRenameShotNode(event);
        }
        else if (type == TreeEvents.DELETE_SEQUENCE_NODE)
        {
            this.onDeleteSequenceNode(event);
        }
        else if (type == TreeEvents.DELETE_SHOT_NODE)
        {
            this.onDeleteShotNode(event);
        }
        else if (type == TreeEvents.SET_EXPAND_NODE_TEMPORAL)
        {
            this.setExpandNode((BaseTreeModel) event.getData());
        }
        else if (type == TreeEvents.CLEAR_TREE_TEMPORAL_SELECTION)
        {
            this.treeTemporal.getTree().getSelectionModel().deselectAll();
        }
        else if (type == TreeEvents.REFRESH_TREE_TEMPORAL_SEQUENCES)
        {
            refreshSequences();
        }
        else if (type == TreeEvents.REFRESH_TEMPORAL_NODE)
        {
            refreshNode(event);
        }
        else
        {
            this.forwardToChild(event);
        }
    }

    /** Retrieve data from XML configuration and sets static configuration variable. */
    private void onExpandTree(BaseTreeModel itemSel)
    {
        if (this.model.getListSequences().getCount() <= 1)
        {
            refreshSequences();
        }
        if (this.treeTemporal.getTree().getStore().getChildren(itemSel) != null
                && this.treeTemporal.getTree().getStore().getChildren(itemSel).size() == 0)
        {
            this.model.populateItem(itemSel);
            this.treeTemporal.setBLoadingSequence(true);
        }
    }

    private void setExpandNode(BaseTreeModel modelData)
    {
        this.treeTemporal.getTree().setExpanded(modelData, true, false);
    }

    public void onAddSequenceNode(AppEvent event)
    {

        BaseTreeModel item = (BaseTreeModel) event.getData("ITEM");
        String value = event.getData("VALUE");
        String url = null;
        BaseTreeModel baseTreeModel = new BaseTreeModel();
        if (item != null)
        {
            // BaseTreeModel modelItem = item;

            baseTreeModel.set("label", value);
            baseTreeModel.set("type", "sequence");
            baseTreeModel.set("parentId", item.get("id"));
            baseTreeModel.set("parentLabel", item.get("label"));
            baseTreeModel.set("hook", item.get("hook") + "_" + value);
            String params = "{\"class\":\"fr.hd3d.model.lightweight.impl.LSequence\", \"name\":\"" + value
                    + "\", \"project\":" + Urls.projectId + "}";
            if (!"temporal".equals(item.get("type")))
            {
                Long parentIdLong = item.get("id");
                String parentId = parentIdLong.toString();
                String hook = model.getHook(baseTreeModel, item);
                params = "{\"class\":\"fr.hd3d.model.lightweight.impl.LSequence\", \"name\":\"" + value
                        + "\", \"parent\":" + parentId + ", \"hook\":\"" + hook + "\", \"project\":" + Urls.projectId
                        + "}";
            }

            url = Urls.urlProject + Urls.urlSeq;
            PostCallback postCallback = new PostCallback(model.getTreeStore(), baseTreeModel, item);

            IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
            try
            {
                requestHandler.handleRequest(Method.POST, url, params, postCallback);
            }
            catch (Exception e)
            {
                // TODO : Erreur
            }
        }
    }

    public void onAddShotNode(AppEvent event)
    {

        BaseTreeModel item = (BaseTreeModel) event.getData("ITEM");
        String value = event.getData("VALUE");
        String url = null;
        BaseTreeModel baseTreeModel = new BaseTreeModel();
        if (item != null)
        {
            ModelData modelItem = item;

            baseTreeModel.set("label", value);
            baseTreeModel.set("type", "shot");
            baseTreeModel.set("parentId", modelItem.get("id"));
            Long parentIdLong = modelItem.get("id");
            String parentId = parentIdLong.toString();
            baseTreeModel.set("parentLabel", modelItem.get("label"));
            baseTreeModel.set("difficulty", 0);
            baseTreeModel.set("mattePainting", false);
            baseTreeModel.set("layout", false);
            baseTreeModel.set("animation", false);
            baseTreeModel.set("export", false);
            baseTreeModel.set("tracking", false);
            baseTreeModel.set("dressing", false);
            baseTreeModel.set("lighting", false);
            baseTreeModel.set("rendering", false);
            baseTreeModel.set("compositing", false);

            final String hook = model.getHook(baseTreeModel, item);

            baseTreeModel.set("description", Urls.noDescription);
            baseTreeModel.set("hook", hook);

            String params = "{\"class\":\"fr.hd3d.model.lightweight.impl.LShot\", \"label\":\"" + value
                    + "\", \"sequence\":" + parentId + ", \"description\":\"" + Urls.noDescription + "\", \"hook\":\""
                    + hook + "\", \"difficulty\":" + 0 + ", \"mattePainting\":" + false + ", \"layout\":" + false
                    + ", \"animation\":" + false + ", \"export\":" + false + ", \"tracking\":" + false
                    + ", \"dressing\":" + false + ", \"lighting\":" + false + ", \"rendering\":" + false
                    + ", \"compositing\":" + false + "}";
            url = Urls.urlProject + Urls.urlSeq + "/" + parentId + Urls.urlShot;
            PostCallback postCallback = new PostCallback(model.getTreeStore(), baseTreeModel, item);

            IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
            try
            {
                requestHandler.handleRequest(Method.POST, url, params, postCallback);
            }
            catch (Exception e)
            {
                // TODO : Erreur
            }
        }
    }

    public void onRenameSequenceNode(AppEvent event)
    {
        BaseTreeModel item = (BaseTreeModel) event.getData("ITEM");
        String value = event.getData("VALUE");
        String url = null;
        if (item != null)
        {
            Long id = item.get("id");
            String itemId = id.toString();
            String params = null;

            if (!"temporal".equals(item.get("type")))
            {
                BaseTreeModel parent = model.getTreeStore().getParent(item);
                String parentType = parent.get("type");
                if ("temporal".equals(parentType))
                {
                    params = "{\"class\":\"fr.hd3d.model.lightweight.impl.LSequence\", \"id\":" + itemId
                            + ", \"name\":\"" + value + "\", \"project\":" + Urls.projectId + "}";
                }
                else
                {
                    Long parentIdLong = parent.get("id");
                    String parentId = parentIdLong.toString();
                    params = "{\"class\":\"fr.hd3d.model.lightweight.impl.LSequence\", \"id\":" + itemId
                            + ", \"name\":\"" + value + "\", \"parent\":" + parentId + ", \"project\":"
                            + Urls.projectId + "}";
                }
                url = Urls.urlProject + Urls.urlSeq + "/" + itemId;
                PutCallback putCallback = new PutCallback(model.getTreeStore(), item, value);
                IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
                try
                {
                    requestHandler.handleRequest(Method.PUT, url, params, putCallback);
                }
                catch (Exception e)
                {
                    // TODO : Erreur
                }
            }
        }
    }

    public String boolInIntString(Boolean boolVal)
    {
        if (!boolVal)
        {
            return "0";
        }
        else
        {
            return "1";
        }
    }

    public void onRenameShotNode(AppEvent event)
    {

        BaseTreeModel item = (BaseTreeModel) event.getData("ITEM");
        String value = event.getData("VALUE");
        String url = null;
        if (item != null)
        {
            // TODO PUT PARTIEL
            BaseTreeModel modelItem = item;

            Long itemIdLong = modelItem.get("id");
            String itemId = itemIdLong.toString();
            Long parentIdLong = model.getTreeStore().getParent(item).get("id");
            String parentId = parentIdLong.toString();
            // String description = modelItem.get("description");
            // String hook = modelItem.get("hook");
            // Integer difficultyInteger = modelItem.get("difficulty");
            // String difficulty = difficultyInteger.toString();

            String params = "{\"class\":\"fr.hd3d.model.lightweight.impl.LShot\", \"id\":" + itemId + ", \"label\":\""
                    + value + "\"}";

            url = Urls.urlProject + Urls.urlSeq + "/" + parentId + Urls.urlShot + "/" + itemId;
            PutCallback putCallback = new PutCallback(model.getTreeStore(), item, value);

            IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
            try
            {
                requestHandler.handleRequest(Method.PUT, url, params, putCallback);

            }
            catch (Exception e)
            {
                // TODO : Erreur
            }
        }
    }

    public void onDeleteSequenceNode(AppEvent event)
    {
        BaseTreeModel item = (BaseTreeModel) event.getData("ITEM");
        if (item != null)
        {
            ModelData modelItem = item;

            Long id = modelItem.get("id");
            String itemId = id.toString();

            if (!"temporal".equals(modelItem.get("type")))
            {
                String url = Urls.urlProject + Urls.urlSeq + "/" + itemId;
                DeleteInTreeCallBack deleteInTreeCallBack = new DeleteInTreeCallBack(model.getTreeStore(), item);

                IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
                try
                {
                    requestHandler.handleRequest(Method.DELETE, url, null, deleteInTreeCallBack);
                }
                catch (Exception e)
                {
                    // TODO : Erreur
                }
            }
        }
    }

    public void onDeleteShotNode(AppEvent event)
    {
        BaseTreeModel item = (BaseTreeModel) event.getData("ITEM");
        if (item != null)
        {
            ModelData modelItem = item;

            Long idLong = modelItem.get("id");
            String itemId = idLong.toString();
            Long parentIdLong = modelItem.get("id");
            String parentId = parentIdLong.toString();

            if (!"temporal".equals(modelItem.get("type")))
            {
                String url = Urls.urlProject + Urls.urlSeq + "/" + parentId + Urls.urlShot + "/" + itemId;
                DeleteInTreeCallBack deleteInTreeCallBack = new DeleteInTreeCallBack(model.getTreeStore(), item);
                IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
                try
                {
                    requestHandler.handleRequest(Method.DELETE, url, null, deleteInTreeCallBack);
                }
                catch (Exception e)
                {
                    // TODO : Erreur
                }
            }
        }
    }

    public void onActionIfNotExist(AppEvent event)
    {
        EventType action = event.getData("ACTION");
        BaseTreeModel item = (BaseTreeModel) event.getData("ITEM");
        String value = event.getData("VALUE");

        if (action == TreeEvents.RENAME_SHOT_NODE || action == TreeEvents.RENAME_SEQUENCE_NODE)
        {
            if (isExist(model.getTreeStore().findModel("id", item.get("parentId")), value))
            {
                return;
            }
        }
        else
        {
            if (isExist(item, value))
            {
                return;
            }
        }
        createEvent(item, value, action);
    }

    public boolean isExist(BaseTreeModel item, String value)
    {
        if (item != null)
        {
            List<BaseTreeModel> lst = model.getTreeStore().getChildren(item);
            Iterator<BaseTreeModel> it = lst.iterator();
            while (it.hasNext())
            {
                ModelData md = it.next();
                String label = md.get("label");
                if (label.equals(value))
                {
                    Info.display("Warning", value + "  already exist in " + item.get("label"));
                    return true;
                }
            }
        }
        return false;
    }

    private void createEvent(BaseTreeModel item, String value, EventType treeEvent)
    {

        AppEvent event = new AppEvent(treeEvent);
        event.setData("VALUE", value);
        event.setData("ITEM", item);
        EventDispatcher.forwardEvent(event);
    }

    private void onTreeLoadProject()
    {
        this.treeTemporal.getTree().getSelectionModel().deselectAll();
        this.model.initTree();
        this.refreshSequences();
        this.treeTemporal.enable();
        this.treeTemporal.getTree().setExpanded(this.model.getTreeStore().getAllItems().get(0), true, false);
    }

    // ** Registers dialog box to view registry. *//*
    private void registerView()
    {}

    private void refreshSequences()
    {

        if (this.model.getCurrentProject() != null) // project selectionne
        {
            String urlSequences = Urls.urlProject + Urls.urlSeq + "/children?orderBy=[\"name\"]";

            IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();

            try
            {
                requestHandler.handleRequest(Method.GET, urlSequences, null, new AllSequencesCallBack(this.model));
            }
            catch (Exception e)
            {
                // TODO : Erreur
            }
        }
    }

    private void refreshNode(AppEvent event)
    {

        BaseTreeModel item = (BaseTreeModel) event.getData("ITEM");
        this.treeTemporal.getTree().getStore().removeAll(item);
        this.onExpandTree(item);
        this.refreshSequences();
        AppEvent event2 = new AppEvent(TreeEvents.REFRESH_VIEWS);
        event2.setData("ACTION", Actions.REFRESH_AFTER_POST);

    }
}
