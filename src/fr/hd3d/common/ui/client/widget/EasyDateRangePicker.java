package fr.hd3d.common.ui.client.widget;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.MenuEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.util.DateWrapper;
import com.extjs.gxt.ui.client.widget.DatePicker;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.Text;
import com.extjs.gxt.ui.client.widget.button.SplitButton;
import com.extjs.gxt.ui.client.widget.layout.ColumnLayout;
import com.extjs.gxt.ui.client.widget.menu.Menu;
import com.extjs.gxt.ui.client.widget.menu.MenuItem;
import com.extjs.gxt.ui.client.widget.menu.SeparatorMenuItem;

import fr.hd3d.common.ui.client.calendar.DateFormat;
import fr.hd3d.common.ui.client.config.CommonConfig;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.util.CSSUtils;


/**
 * Little widget that lets user easily defines a date range via menu buttons. It allows to select common date ranged via
 * date shortcuts. EasyDateRangePicker allows also to select dates from date pickers.
 * 
 * @author HD3D
 */
public class EasyDateRangePicker extends LayoutContainer
{
    /** Button that displays start date and that contains menu needed to change start date. */
    protected SplitButton startDateField = new SplitButton();
    /** The menu used to select start date via shortcuts. */
    protected Menu startDateMenu = new Menu();
    /** The date picked used to select start date via direct selection. */
    protected DatePicker startDatePicker = new DatePicker();

    /** Button that displays end date and that contains menu needed to change end date. */
    protected SplitButton endDateField = new SplitButton();
    /** The menu used to select end date via shortcuts. */
    protected Menu endDateMenu = new Menu();
    /** The date picked used to select end date via direct selection. */
    protected DatePicker endDatePicker = new DatePicker();

    /** Current start date. */
    protected DateWrapper startDate;
    /** Current end date. */
    protected DateWrapper endDate;

    /** Event type to forward when change occurs. */
    protected EventType eventType;

    /** Short cut used to select */
    protected FieldModelData currentWeek = new FieldModelData(0, "First day of current week",
            new DateWrapper().addDays(-1 * (new DateWrapper().getDayInWeek())));
    protected FieldModelData currentMonth = new FieldModelData(1, "First day of current month",
            new DateWrapper().getFirstDayOfMonth());
    protected FieldModelData currentYear = new FieldModelData(1, "First day of current year", new DateWrapper(
            new DateWrapper().getFullYear(), 0, 1));

    /**
     * Constructor : build fields, their menu and their labels.
     */
    public EasyDateRangePicker()
    {
        this.setLayout(new ColumnLayout());

        Text fromLabel = new Text("From: ");
        fromLabel.setStyleAttribute("padding", "6px 4px 3px 4px");
        this.add(fromLabel);
        this.setStartDateField();
        Text toLabel = new Text("Until: ");
        toLabel.setStyleAttribute("padding", "6px 4px 3px 4px");
        this.add(toLabel);
        this.setEndDateField();

        this.setStartDate((DateWrapper) currentMonth.getValue(), false);
        this.setEndDate(getStartDate().addMonths(1), true);
    }

    /**
     * @return The current start date.
     */
    public DateWrapper getStartDate()
    {
        return this.startDate;
    }

    /**
     * Set current start date without firing change event.
     * 
     * @param date
     *            The date to set.
     */
    public void setStartDate(DateWrapper date)
    {
        setStartDate(date, false);
    }

    /**
     * Set start date. It forwards to event dispatcher the change event if <i>fireEvents</i> is set to true.
     * 
     * @param date
     *            The date to set as start date.
     * @param fireEvents
     *            True to forward change event after start date has changed.
     */
    public void setStartDate(DateWrapper date, Boolean fireEvents)
    {
        this.startDateField.setText(DateFormat.FRENCH_DATE.format(date.asDate()));
        this.startDate = date;

        if (date != null)
        {
            startDatePicker.disableEvents(true);
            startDatePicker.setValue(getStartDate().asDate());
            startDatePicker.disableEvents(false);
        }

        if (fireEvents && this.eventType != null)
            this.fireEvent();
    }

    /**
     * @return The current end date.
     */
    public DateWrapper getEndDate()
    {
        return this.endDate;
    }

    /**
     * @param date
     *            The date to set. Set current end date without firing change event.
     */
    public void setEndDate(DateWrapper date)
    {
        this.setEndDate(date, false);
    }

    /**
     * Set end date. It forwards to event dispatcher the change event if <i>fireEvents</i> is set to true.
     * 
     * @param date
     *            The date to set as end date.
     * @param fireEvents
     *            True to forward change event after end date has changed.
     */
    public void setEndDate(DateWrapper date, Boolean fireEvents)
    {
        this.endDateField.setText(DateFormat.FRENCH_DATE.format(date.asDate()));
        this.endDate = date;

        if (date != null)
        {
            endDatePicker.disableEvents(true);
            endDatePicker.setValue(getEndDate().asDate());
            endDatePicker.disableEvents(false);
        }

        if (fireEvents && this.eventType != null)
            EventDispatcher.forwardEvent(eventType);
    }

    /**
     * Register event to forward when a change occurs.
     * 
     * @param event
     *            The event to forward.
     */
    public void setEvent(EventType event)
    {
        this.eventType = event;
    }

    /**
     * Build start date field and its menu. The menu contains date shortcut (like setting start date to the beginning of
     * the month) and a date picker.
     */
    private void setStartDateField()
    {
        this.add(startDateField);
        this.startDateField.addStyleName(CSSUtils.FLAT_NAME_STYLE);

        MenuItem item = new MenuItem(currentWeek.getName());
        item.addSelectionListener(new StartDateItemSelectionListener((DateWrapper) currentWeek.getValue()));
        this.startDateMenu.add(item);

        item = new MenuItem(currentMonth.getName());
        item.addSelectionListener(new StartDateItemSelectionListener((DateWrapper) currentMonth.getValue()));
        this.startDateMenu.add(item);

        item = new MenuItem(currentYear.getName());
        item.addSelectionListener(new StartDateItemSelectionListener((DateWrapper) currentYear.getValue()));
        this.startDateMenu.add(item);

        this.startDatePicker.addListener(Events.Select, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                onStartDatePickerChanged();
            }
        });
        this.startDateMenu.add(new SeparatorMenuItem());
        this.startDateMenu.add(startDatePicker);

        this.startDateField.setMenu(startDateMenu);
    }

    /**
     * When date picker value changed, it checks if start date and end dates are valid (start date before end date) then
     * it sets the new start value with date picker value and fire change events.
     */
    private void onStartDatePickerChanged()
    {
        if (getEndDate().asDate().after(startDatePicker.getValue()))
        {
            setStartDate(new DateWrapper(startDatePicker.getValue()), true);
            startDateMenu.hide();
        }
        else
        {
            MessageBox.info("Error", "You must not select an end date that is before the start date", null);
        }
    }

    /**
     * Build end date field and its menu. The menu contains date shortcut (like setting end date one month after start
     * date) and a date picker.
     */
    private void setEndDateField()
    {
        this.add(endDateField);
        this.endDateField.addStyleName(CSSUtils.FLAT_NAME_STYLE);

        MenuItem item = new MenuItem("1 week");
        item.addSelectionListener(new EndDateWeekItemSelectionListener(1));
        endDateMenu.add(item);
        item = new MenuItem("2 weeks");
        item.addSelectionListener(new EndDateWeekItemSelectionListener(2));
        endDateMenu.add(item);
        item = new MenuItem("3 weeks");
        item.addSelectionListener(new EndDateWeekItemSelectionListener(3));
        endDateMenu.add(item);

        endDateMenu.add(new SeparatorMenuItem());
        item = new MenuItem("1 month");
        item.addSelectionListener(new EndDateMonthItemSelectionListener(1));
        endDateMenu.add(item);
        item = new MenuItem("2 months");
        item.addSelectionListener(new EndDateMonthItemSelectionListener(2));
        endDateMenu.add(item);
        item = new MenuItem("3 months");
        item.addSelectionListener(new EndDateMonthItemSelectionListener(3));
        endDateMenu.add(item);
        item = new MenuItem("6 months");
        item.addSelectionListener(new EndDateMonthItemSelectionListener(6));
        endDateMenu.add(item);

        endDateMenu.add(new SeparatorMenuItem());
        item = new MenuItem("1 year");
        item.addSelectionListener(new EndDateMonthItemSelectionListener(12));
        endDateMenu.add(item);
        item = new MenuItem("2 years");
        item.addSelectionListener(new EndDateMonthItemSelectionListener(24));
        endDateMenu.add(item);
        item = new MenuItem("3 years");
        item.addSelectionListener(new EndDateMonthItemSelectionListener(36));
        endDateMenu.add(item);
        endDateMenu.add(new SeparatorMenuItem());

        endDatePicker.addListener(Events.Select, new Listener<BaseEvent>() {
            public void handleEvent(BaseEvent be)
            {
                onEndDatePickerChanged();
            }
        });
        endDateMenu.add(endDatePicker);

        endDateField.setMenu(endDateMenu);
    }

    /**
     * When date picker value changed, it checks if start date and end dates are valid (start date before end date) then
     * it sets the new end value with date picker value and fire change events.
     */
    private void onEndDatePickerChanged()
    {
        if (getStartDate().asDate().before(endDatePicker.getValue()))
        {
            setEndDate(new DateWrapper(endDatePicker.getValue()), true);
            endDateMenu.hide();
        }
        else
        {
            MessageBox.info("Error", "You must not select an end date that is before the start date", null);
        }
    }

    /**
     * Forward registered event type, with start date and end date as date. To retrieve this data from event data map,
     * use CommonConfig.START_DATE_EVENT_VAR_NAME and CommonConfig.START_DATE_EVENT_VAR_NAME as keys.
     */
    private void fireEvent()
    {
        AppEvent mvcEvent = new AppEvent(eventType);
        mvcEvent.setData(CommonConfig.START_DATE_EVENT_VAR_NAME, eventType);
        mvcEvent.setData(CommonConfig.END_DATE_EVENT_VAR_NAME, eventType);

        EventDispatcher.forwardEvent(mvcEvent);
    }

    /**
     * Update start date field value with date linked to the listener via the constructor.
     * 
     * @author HD3D
     */
    private class StartDateItemSelectionListener extends SelectionListener<MenuEvent>
    {
        private final DateWrapper date;

        public StartDateItemSelectionListener(DateWrapper date)
        {
            this.date = date;
        }

        @Override
        public void componentSelected(MenuEvent ce)
        {
            setStartDate(this.date, true);
        }
    }

    /**
     * Set end date as start date + <i>weekToAdd</i> f weeks.
     * 
     * @author HD3D
     */
    private class EndDateWeekItemSelectionListener extends SelectionListener<MenuEvent>
    {
        private final Integer weekToAdd;

        public EndDateWeekItemSelectionListener(Integer weekToAdd)
        {
            this.weekToAdd = weekToAdd;
        }

        @Override
        public void componentSelected(MenuEvent ce)
        {
            setEndDate(getStartDate().addDays(7 * weekToAdd), true);
        }
    }

    /**
     * Set end date as start date + <i>monthToAdd</i>months.
     * 
     * @author HD3D
     */
    private class EndDateMonthItemSelectionListener extends SelectionListener<MenuEvent>
    {
        private final Integer monthToAdd;

        public EndDateMonthItemSelectionListener(Integer monthToAdd)
        {
            this.monthToAdd = monthToAdd;
        }

        @Override
        public void componentSelected(MenuEvent ce)
        {
            setEndDate(getStartDate().addMonths(monthToAdd), true);
        }
    }
}
