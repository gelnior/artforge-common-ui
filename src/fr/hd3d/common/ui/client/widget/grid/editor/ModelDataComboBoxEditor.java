package fr.hd3d.common.ui.client.widget.grid.editor;

import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.google.gwt.json.client.JSONObject;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.writer.JsonEncoder;
import fr.hd3d.common.ui.client.widget.ModelDataComboBox;


/**
 * This editor is based upon a model data combo box. Useful for selection of one-to-many value.
 * 
 * @author HD3D
 * 
 * @param <C>
 *            A record model data object.
 */
public class ModelDataComboBoxEditor<C extends RecordModelData> extends CellEditor
{
    /** The field combo box which act as the editor. */
    private final ModelDataComboBox<C> combo;

    protected Long id = 0L;

    /** Default Constructor. */
    public ModelDataComboBoxEditor(ModelDataComboBox<C> combo)
    {
        super(combo);
        this.combo = combo;
    }

    @Override
    public Object preProcessValue(Object value)
    {
        C object;

        if (value == null)
        {
            return null;
        }
        else
        {
            String name = ((JSONObject) value).get(RecordModelData.NAME_FIELD).toString();
            this.id = Long.parseLong(((JSONObject) value).get(RecordModelData.ID_FIELD).toString());

            object = combo.getValue(name);

            return object;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public Object postProcessValue(Object value)
    {
        C object = (C) value;

        if (value == null || object.getId().longValue() == id.longValue())
        {
            return null;
        }
        else
        {
            JSONObject result = JsonEncoder.encode(object);
            return result;
        }
    }
}
