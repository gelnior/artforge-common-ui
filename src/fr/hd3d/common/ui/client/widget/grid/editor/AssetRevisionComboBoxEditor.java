package fr.hd3d.common.ui.client.widget.grid.editor;

import com.extjs.gxt.ui.client.widget.grid.CellEditor;

import fr.hd3d.common.ui.client.widget.AutoAssetRevisionComboBox;


public class AssetRevisionComboBoxEditor extends CellEditor
{
    /** The field combo box which act as the editor. */
    protected final AutoAssetRevisionComboBox combo;

    /** Default Constructor. */
    public AssetRevisionComboBoxEditor()
    {
        this(false);
    }

    /** Default Constructor. */
    public AssetRevisionComboBoxEditor(boolean blankField)
    {
        super(new AutoAssetRevisionComboBox(blankField));
        this.combo = (AutoAssetRevisionComboBox) this.getField();
    }

    @Override
    public Object preProcessValue(Object value)
    {
        if (value == null)
        {
            return value;
        }

        return combo.getValueById((Long) value);
    }

    @Override
    public Object postProcessValue(Object value)
    {
        if (value == null)
        {
            return value;
        }
        return (value);
    }
}
