package fr.hd3d.common.ui.client.widget.grid;

import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.EditorGrid;
import com.extjs.gxt.ui.client.widget.grid.GroupingView;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.service.store.BaseStore;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.widget.EasyMenu;
import fr.hd3d.common.ui.client.widget.RefreshMenuItem;


/**
 * Preconfigured GXT editable grid.
 * 
 * @author HD3D
 * 
 * @param <M>
 *            Type of data displayed by the grid.
 */
public class BaseEditorGrid<M extends Hd3dModelData> extends EditorGrid<M>
{
    /** Grid context menu. */
    protected EasyMenu menu = new EasyMenu();
    /** Refresh menu item to allow user to reload data from this grid. */
    protected RefreshMenuItem<M> refreshItem = new RefreshMenuItem<M>(null);

    public BaseEditorGrid(BaseStore<M> baseStore, ColumnModel cm, EventType selectionEvent)
    {
        super(baseStore, cm);

        this.refreshItem.setStore(baseStore);
        this.menu.add(refreshItem);
        this.setContextMenu(menu);

        SelectableGridView gridView = new SelectableGridView();
        this.setView(gridView);
        this.setSelectionModel(new SimpleLineCellSelectionModel<M>(gridView, selectionEvent));
    }

    public BaseEditorGrid(BaseStore<M> baseStore, ColumnModel cm)
    {
        this(baseStore, cm, null);
    }

    /**
     * @return Store directly cast as an Artforge store.
     */
    public BaseStore<M> getServiceStore()
    {
        return (BaseStore<M>) this.store;
    }

    /**
     * @return Context menu.
     */
    public EasyMenu getMenu()
    {
        return this.menu;
    }

    @Override
    public void reconfigure(ListStore<M> store, ColumnModel cm)
    {
        super.reconfigure(store, cm);
        this.refreshItem.setStore((BaseStore<M>) store);
    }

    /**
     * Display grid headers.
     */
    public void showHeaders()
    {
        if (this.getView() instanceof BaseGroupingView)
        {
            ((BaseGroupingView) this.getView()).showHeaders();
        }
        else if (this.getView() instanceof BaseGridView)
        {
            ((BaseGridView) this.getView()).showHeaders();
        }
    }

    /**
     * Hide grid headers.
     */
    public void hideHeaders()
    {
        if (this.getView() instanceof BaseGroupingView)
        {
            ((BaseGroupingView) this.getView()).hideHeaders();
        }
        else if (this.getView() instanceof BaseGridView)
        {
            ((BaseGridView) this.getView()).hideHeaders();
        }
    }

    /** Clear current selection. */
    public void resetSelection()
    {
        ((SimpleLineCellSelectionModel<M>) this.getSelectionModel()).reset();
    }

    /**
     * @return Currently selected models.
     */
    public List<M> getSelection()
    {
        return this.getSelectionModel().getSelectedItems();
    }

    /**
     * @return The first element selected by user.
     */
    public M getFirstSelected()
    {
        List<M> selectedModels = getSelection();
        if (CollectionUtils.isNotEmpty(selectedModels))
        {
            return selectedModels.get(0);
        }
        return null;
    }

    /**
     * Enable grouping for this grid.
     */
    public void setGroupingOn()
    {
        GroupingView view = new GroupingView();
        view.setShowGroupedColumn(false);
        view.setEnableGroupingMenu(false);
        this.setView(view);
    }

}
