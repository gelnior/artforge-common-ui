package fr.hd3d.common.ui.client.widget.grid.renderer;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.DOM;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.util.HtmlUtils;


/**
 * Long text renderer transforms carriage return in HTML carriage return. Moreover if text width is bigger than column
 * width, it makes automatic carriage returns (set text-wrap CSS properties to normal via cell-long-text CSS class).
 * 
 * @author HD3D
 * 
 * @param <M>
 *            Type of rendered record.
 */
public class LongTextRenderer<M extends Hd3dModelData> implements GridCellRenderer<M>
{
    public Object render(M model, String property, ColumnData config, int rowIndex, int colIndex, ListStore<M> store,
            Grid<M> grid)
    {
        Element div = DOM.createDiv();
        String text = ((String) model.get(property));
        if (text != null)
        {
            div.setInnerHTML(HtmlUtils.changeToHyperText(text.replace("\n", "<br />")));
        }
        div.addClassName(" cell-long-text");

        return div.getString();
    }
}
