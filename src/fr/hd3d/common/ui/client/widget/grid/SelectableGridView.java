package fr.hd3d.common.ui.client.widget.grid;

import com.extjs.gxt.ui.client.Style.SortDir;


public class SelectableGridView extends BaseGroupingView implements ISelectableGridView
{
    @Override
    public void onCellSelect(int row, int col)
    {
        super.onCellSelect(row, col);
    }

    @Override
    public void onCellDeselect(int row, int col)
    {
        super.onCellDeselect(row, col);
    }

    @Override
    public void updateSortIcon(int colIndex, SortDir dir)
    {
        header.updateSortIcon(colIndex, dir);
    }
}
