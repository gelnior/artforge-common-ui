package fr.hd3d.common.ui.client.widget.grid;

import com.extjs.gxt.ui.client.Style.SortDir;


public interface ISelectableGridView
{

    public void onCellSelect(int row, int col);

    public void onCellDeselect(int row, int col);

    public void updateSortIcon(int colIndex, SortDir dir);
}
