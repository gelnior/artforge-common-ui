package fr.hd3d.common.ui.client.widget.grid.renderer;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;
import com.extjs.gxt.ui.client.widget.tips.QuickTip;
import com.extjs.gxt.ui.client.widget.tips.ToolTipConfig;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.widget.grid.BaseGrid;


/**
 * Displays comment with a small padding. When user put his mouse over the cell, it displays a tooltip containing the
 * full comment.
 * 
 * @author HD3D
 * 
 * @param <T>
 *            Type of the data containing the field to render
 */
public class CommentRenderer<T extends Hd3dModelData> implements GridCellRenderer<T>
{
    /**
     * Constructor, initializes tool tip feature.
     * 
     * @param taskGrid
     *            The grid on which tool tip will be displayed.
     */
    public CommentRenderer(BaseGrid<T> taskGrid)
    {
        this.setQuickTip(taskGrid);
    }

    /** Tool tip configuration. */
    private final ToolTipConfig config = new ToolTipConfig();
    /** Tool tip displayed on each cell. */
    private QuickTip quickTip;

    public Object render(T model, String property, ColumnData config, int rowIndex, int colIndex, ListStore<T> store,
            Grid<T> grid)
    {
        this.quickTip.initTarget(grid);

        String value = model.get(property);
        if (value == null)
            value = "";
        String html = "<div qtip='" + value.replace("'", "&#39;").replace("\n", "<br />")
                + "' qtitle='' style=\"padding: 3px;\">";
        html += value;
        html += "</div>";
        return html;
    }

    /**
     * Set tool tip to display comment.
     * 
     * @param grid
     *            The grid on which tool tip should be set.
     */
    private void setQuickTip(Grid<T> grid)
    {
        quickTip = new QuickTip(grid);

        config.setShowDelay(200);
        config.setHideDelay(0);
        quickTip.setToolTip(config);
        quickTip.setInterceptTitles(false);
        quickTip.setShadow(false);
        quickTip.setBodyBorder(false);
        quickTip.setBorders(false);
    }
}
