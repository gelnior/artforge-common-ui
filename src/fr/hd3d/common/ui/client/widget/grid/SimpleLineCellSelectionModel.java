package fr.hd3d.common.ui.client.widget.grid;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.grid.EditorGrid;
import com.extjs.gxt.ui.client.widget.grid.GridSelectionModel;
import com.google.gwt.core.client.JavaScriptException;
import com.google.gwt.event.dom.client.KeyCodes;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;


/**
 * Selection model for handling both cell and row selection.
 * 
 * @author HD3D
 * 
 * @param <M>
 *            Type of object displayed by grid.
 */
public class SimpleLineCellSelectionModel<M extends Hd3dModelData> extends GridSelectionModel<M>
{
    protected static final int START_COLUMN = 0;

    protected Cell selectedCell = null;

    /** Grid view needed to display section. */
    protected ISelectableGridView view;

    protected final EventType eventType;

    public SimpleLineCellSelectionModel(ISelectableGridView view, EventType eventType)
    {
        this.view = view;
        this.eventType = eventType;

    }

    /**
     * Reset all selection, deselect everything and set to -1 actual column/row and previously selected column/row
     * index.
     */
    public void reset()
    {
        this.deselectAll();
        this.forwardSelectChangeEvent(null);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void handleEvent(BaseEvent e)
    {
        EventType type = e.getType();

        if (type == Events.RowMouseDown)
        {
            handleMouseDown((GridEvent<M>) e);
        }
        else if (type == Events.ContextMenu)
        {
            handleMouseDown((GridEvent<M>) e);
        }
        else if (type == Events.Refresh)
        {
            refresh();
        }
        else if (type == Events.Remove)
        {
            Logger.display("remove");
        }
    }

    @Override
    protected void onKeyPress(GridEvent<M> e)
    {
        int k = e.getKeyCode();
        switch (k)
        {
            case KeyCodes.KEY_ENTER:
                this.onKeyEnter();
                break;
        }
        super.onKeyPress(e);
    }

    /**
     * When enter key is pressed, if there is one selected cell, edition mode is activated.
     */
    protected void onKeyEnter()
    {
        if (this.selectedCell != null)
        {
            ((EditorGrid<M>) grid).startEditing(selectedCell.row, selectedCell.cell);
        }
    }

    /**
     * When down key is pressed, the cell above the currently selected cell and its row are selected. If shift key is
     * not pressed, previously selected cell and its row are unselected.
     * 
     * @param e
     *            Key press event.
     */
    @Override
    protected void onKeyDown(GridEvent<M> e)
    {
        e.preventDefault();
        moveSelection(1, 0);
    }

    /**
     * Moves the selected cell.
     * 
     * @param moveRow
     *            the number of step in up/down
     * @param moveCol
     *            the number of step in left/right
     */
    protected void moveSelection(int moveRow, int moveCol)
    {
        if (selectedCell == null)
        {
            return;
        }

        // If selection model reaches the bottom of the grid, key down does not move the cell selection.
        Cell newCell = getValidateCell(selectedCell.row + moveRow, selectedCell.cell + moveCol);

        selectRowAndCell(newCell.row, newCell.cell, false);
    }

    /**
     * Get a validate cell according with the given row and the given cell.
     * 
     * @param row
     *            the given row index
     * @param cell
     *            the given cell index
     * @return the validate cell
     */
    protected Cell getValidateCell(int row, int cell)
    {
        Cell newCell = new Cell(row, cell);
        int clen = grid.getColumnModel().getColumnCount();
        if (newCell.cell < 0)
        {
            newCell.cell = clen - 1;
            newCell.row = newCell.row - 1;
        }
        else if (newCell.cell >= clen)
        {
            newCell.cell = 0;
            newCell.row = newCell.row + 1;
        }

        if (newCell.row < 0)
        {
            newCell.row = grid.getStore().getCount() - 1;
        }
        else if (newCell.row >= grid.getStore().getCount())
        {
            newCell.row = 0;
        }
        return newCell;
    }

    /**
     * When up key is pressed, the cell over the currently selected cell and its row are selected. If shift key is not
     * pressed, previously selected cell and its row are unselected.<br>
     * <br>
     * SELECTION_CHANGE event is raised.
     * 
     * @param e
     *            Key press event.
     */
    @Override
    protected void onKeyUp(GridEvent<M> e)
    {
        e.preventDefault();
        moveSelection(-1, 0);
    }

    /**
     * When left key is pressed, the cell at the left of the currently selected cell is selected. If currently selected
     * cell is at the extreme left.
     */
    @Override
    protected void onKeyLeft(GridEvent<M> ce)
    {
        ce.preventDefault();
        moveSelection(0, -1);
    }

    @Override
    protected void onKeyRight(GridEvent<M> ce)
    {
        ce.preventDefault();
        moveSelection(0, 1);
    }

    @Override
    protected void handleMouseDown(GridEvent<M> e)
    {
        if (!isLocked())
        {
            M selectedModel = listStore.getAt(e.getRowIndex());

            if (!(e.isRightClick() && this.isSelected(selectedModel)))
            {
                this.onCellClick(e, selectedModel);
            }
        }
    }

    /**
     * Handler to the case with a click on a cell.
     * 
     * @param event
     *            the event.
     * @param selectedModel
     *            the selected model.
     */
    protected void onCellClick(GridEvent<M> event, M selectedModel)
    {
        try
        {
            this.onSimpleClick(event, selectedModel);
        }
        catch (JavaScriptException exception)
        {
            this.onJavascriptError();
        }
    }

    /**
     * When javascript error occurs, everything is unselected.
     */
    protected void onJavascriptError()
    {
        deselectAll();
    }

    /**
     * Forward to event dispatcher the fact that another record (row) is selected and attach the record to the event.
     * 
     * @param selectedModel
     */
    protected void forwardSelectChangeEvent(M selectedModel)
    {
        if (eventType != null)
        {
            AppEvent event = new AppEvent(eventType);
            event.setData(selectedModel);
            EventDispatcher.forwardEvent(event);
        }
    }

    /**
     * Return the list of selected cells.
     * 
     * @return the list of selected cells.
     */
    public List<Cell> getSeletedCells()
    {
        ArrayList<Cell> cells = new ArrayList<Cell>();
        cells.add(selectedCell);
        return cells;
    }

    /**
     * Handler to the case with simple click.
     * 
     * @param event
     *            the event
     * @param selectedModel
     *            the selected model
     */
    protected void onSimpleClick(GridEvent<M> event, M selectedModel)
    {
        if (selectedCell == null
                || !(selectedCell.row == event.getRowIndex() && selectedCell.cell == event.getColIndex()))
        {
            selectRowAndCell(event.getRowIndex(), event.getColIndex(), false);
        }
    }

    /**
     * Select the row and the cell.
     * 
     * @param rowIndex
     *            the row index
     * @param colIndex
     *            the cell index
     * @param keepExisting
     *            keep the before selection
     */
    public void selectRowAndCell(int rowIndex, int colIndex, boolean keepExisting)
    {
        if (selectedCell == null || !(selectedCell.row == rowIndex && selectedCell.cell == colIndex))
        {
            if (rowIndex != -1 && colIndex != -1)
            {
                if (!keepExisting)
                {
                    deselectAll();
                }

                select(this.listStore.getAt(rowIndex), keepExisting);
                selectedCell = new Cell(rowIndex, colIndex);
                this.view.onCellSelect(selectedCell.row, selectedCell.cell);

                this.forwardSelectChangeEvent(listStore.getAt(rowIndex));
            }
        }
    }

    @Override
    public void deselectAll()
    {
        if (selectedCell != null)
        {
            deselect(selectedCell.row);
            this.view.onCellDeselect(selectedCell.row, selectedCell.cell);
            selectedCell = null;
        }
    }

    /**
     * Deselect the row and the cell
     * 
     * @param cell
     *            the cell containing the row index and the cell index to deselect.
     */
    public void deselectRowAndCell(Cell cell)
    {
        if (cell == null || cell.row == -1 || cell.cell == -1)
        {
            return;
        }
        deselect(cell.row);
        this.view.onCellDeselect(cell.row, cell.cell);
        selectedCell = null;
    };
}
