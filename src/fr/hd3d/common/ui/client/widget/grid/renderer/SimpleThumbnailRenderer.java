package fr.hd3d.common.ui.client.widget.grid.renderer;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.widget.thumbnaileditor.ThumbnailPath;


public class SimpleThumbnailRenderer<M extends Hd3dModelData> implements GridCellRenderer<M>
{
    public Object render(M model, String property, ColumnData config, int rowIndex, int colIndex, ListStore<M> store,
            Grid<M> grid)
    {
        String path = ThumbnailPath.getPath(model);

        return getImageHtmlCode(path);
    }

    protected Object getImageHtmlCode(String path)
    {
        return "<div style=\"text-align: center; width: 100%;\"><img height=\"40\" src=\"" + path + "\" /></div>";
    }
}
