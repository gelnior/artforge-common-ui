package fr.hd3d.common.ui.client.widget.grid.renderer;

import java.util.List;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GroupColumnData;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONValue;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.IExplorerCellRenderer;


/**
 * Displays name of the record model data in the cell instead of its JSON representation.
 * 
 * @author HD3D
 */
public class RecordModelDataRenderer implements IExplorerCellRenderer<Hd3dModelData>
{
    /** Separator used for list rendering. */
    private String separator = ", ";

    /**
     * Set separator used for list rendering.
     * 
     * @param separator
     *            The separator to set.
     */
    public void setSeparator(String separator)
    {
        this.separator = separator;
    }

    public Object render(Hd3dModelData model, String property, ColumnData config, int rowIndex, int colIndex,
            ListStore<Hd3dModelData> store, Grid<Hd3dModelData> grid)
    {
        Object obj = model.get(property);
        return this.getValueFromObject(obj);
    }

    public String render(GroupColumnData data)
    {
        return this.getValueFromObject(data.gvalue);
    }

    protected String getValueFromObject(Object obj)
    {
        StringBuilder stringValue = new StringBuilder();
        if (obj instanceof JSONObject)
        {
            stringValue.append(getNameFromObject(obj));
        }
        else if (obj instanceof List<?>)
        {
            @SuppressWarnings("unchecked")
            List<JSONValue> records = (List<JSONValue>) obj;
            for (JSONValue value : records)
            {
                if (stringValue.length() > 0)
                {
                    stringValue.append(separator);
                }
                stringValue.append(getNameFromObject(value));
            }
        }
        return stringValue.toString();
    }

    protected String getNameFromObject(Object obj)
    {
        JSONObject json = (JSONObject) obj;
        JSONValue name = json.get(RecordModelData.NAME_FIELD);
        return name.isString().stringValue();
    }
}
