package fr.hd3d.common.ui.client.widget.grid.renderer;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;

import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.IExplorerCellRenderer;


public class BigTextRenderer<M extends ModelData> implements IExplorerCellRenderer<M>
{

    public Object render(M model, String property, ColumnData config, int rowIndex, int colIndex, ListStore<M> store,
            Grid<M> grid)
    {
        Object obj = model.get(property);
        return getBoldRenderedValue(obj);
    }

    private String getBoldRenderedValue(Object gValue)
    {
        String stringValue = "";
        if (gValue != null)
        {
            stringValue = "<div style= 'padding: 3px; font-size: 16px; font-weight: bold;'>" + (String) gValue
                    + "</div>";
        }

        return stringValue;
    }
}
