package fr.hd3d.common.ui.client.widget.grid.editor;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.widget.form.SimpleComboBox;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;


/**
 * Cell Editor based on a simple combo box. Useful when display data are the same as real values.
 * 
 * @author HD3D
 */
public class SimpleComboBoxEditor extends CellEditor
{
    /** The simple combo box which acts as the editor. */
    private final SimpleComboBox<String> combo;

    /** Default Constructor. */
    public SimpleComboBoxEditor(SimpleComboBox<String> combo)
    {
        super(combo);
        this.combo = combo;
    }

    @Override
    public Object preProcessValue(Object value)
    {
        if (value == null)
        {
            return value;
        }
        return this.combo.findModel(value.toString());
    }

    @Override
    public Object postProcessValue(Object value)
    {
        if (value == null)
        {
            return value;
        }
        return ((ModelData) value).get("value");
    }
}
