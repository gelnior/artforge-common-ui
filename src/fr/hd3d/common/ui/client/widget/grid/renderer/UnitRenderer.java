package fr.hd3d.common.ui.client.widget.grid.renderer;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GroupColumnData;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.IExplorerCellRenderer;


/**
 * Field combo cell renderer for GXT grids : display the field name and order fields in combo by id.
 * 
 * @author HD3D
 */
public class UnitRenderer implements IExplorerCellRenderer<Hd3dModelData>
{
    private final String unit;

    /** Default Constructor. */
    public UnitRenderer(String unit)
    {
        this.unit = unit;
    }

    /** @return The value to render. */
    public Object render(Hd3dModelData model, String property, ColumnData config, int rowIndex, int colIndex,
            ListStore<Hd3dModelData> store, Grid<Hd3dModelData> grid)
    {
        Object obj = model.get(property);

        if (obj != null)
        {
            if (obj instanceof Double)
            {
                return ((Double) obj).intValue() + " " + unit;
            }

            String value = obj.toString();
            return value + " " + unit;
        }
        return "0 " + unit;
    }

    public String render(GroupColumnData data)
    {
        return data.gvalue.toString();
    }
}
