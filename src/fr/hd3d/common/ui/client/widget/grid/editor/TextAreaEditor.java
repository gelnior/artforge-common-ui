package fr.hd3d.common.ui.client.widget.grid.editor;

import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.FieldEvent;
import com.extjs.gxt.ui.client.widget.form.TextArea;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.google.gwt.event.dom.client.KeyCodes;


/**
 * This is a text area grid editor. It allows user to make carriage return inside its text field.
 * 
 * @author HD3D
 */
public class TextAreaEditor extends CellEditor
{
    public static final String CR_STRING = "\n";
    public static final String CR_HTML = "<br />";

    /**
     * Constructor : automatically set TextArea as field.
     */
    public TextAreaEditor()
    {
        super(new TextArea());
    }

    /**
     * Before displaying value for edition it replaces every HTML carriage return by string carriage return to
     * facilitate readability.
     * 
     * @param value
     *            The value to pre-process.
     */
    @Override
    public Object preProcessValue(Object value)
    {
        if (value == null)
        {
            return value;
        }
        return ((String) value).replace(CR_HTML, CR_STRING);
    }

    /**
     * Before setting value after edition it replaces every string carriage return by HTML carriage return to facilitate
     * readability inside cell.
     * 
     * @param value
     *            The value to post-process.
     */
    @Override
    public Object postProcessValue(Object value)
    {
        if (value == null)
        {
            return null;
        }
        return ((String) value).replace(CR_STRING, CR_HTML);
    }

    /**
     * When special key event occurs, it keeps normal behavior except for shift+enter event where it stops normal event
     * behavior (no end edition) and add string carriage return to current field value.
     */
    @Override
    protected void onSpecialKey(FieldEvent fe)
    {
        int key = fe.getKeyCode();
        if (fe.isShiftKey() && key == KeyCodes.KEY_ENTER)
        {
            fe.stopEvent();
            TextArea area = ((TextArea) getField());
            String value = ((String) getField().getValue());
            int cursorPosition = area.getCursorPos();
            if (value == null)
            {
                value = "";
            }
            value = value.substring(0, cursorPosition) + CR_STRING + value.substring(cursorPosition);

            area.setValue(value);
            area.setCursorPos(cursorPosition + 1);
        }
        else if (isCompleteOnEnter() && key == KeyCodes.KEY_ENTER)
        {
            fe.stopEvent();
            completeEdit();
        }
        else if (isCancelOnEsc() && key == KeyCodes.KEY_ESCAPE)
        {
            cancelEdit();
        }
        else
        {
            fireEvent(Events.SpecialKey, fe);
        }
    }
}
