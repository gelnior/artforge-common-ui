package fr.hd3d.common.ui.client.widget.grid.editor;

import com.extjs.gxt.ui.client.widget.grid.CellEditor;

import fr.hd3d.common.ui.client.widget.AutoTaskTypeComboBox;


/**
 * Editor based on task type combo box.
 * 
 * @author HD3D
 */
public class TaskTypeComboBoxEditor extends CellEditor
{
    /** The field combo box which act as the editor. */
    protected final AutoTaskTypeComboBox combo;

    /** Default Constructor. */
    public TaskTypeComboBoxEditor()
    {
        this(true);
    }

    /** Default Constructor. */
    public TaskTypeComboBoxEditor(boolean blankField)
    {
        super(new AutoTaskTypeComboBox(blankField));
        this.combo = (AutoTaskTypeComboBox) this.getField();
    }

    @Override
    public Object preProcessValue(Object value)
    {
        if (value == null)
        {
            return value;
        }

        return combo.getValueById((Long) value);
    }

    @Override
    public Object postProcessValue(Object value)
    {
        if (value == null)
        {
            return value;
        }
        return (value);
    }
}
