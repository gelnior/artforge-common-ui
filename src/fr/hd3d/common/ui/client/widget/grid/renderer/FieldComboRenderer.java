package fr.hd3d.common.ui.client.widget.grid.renderer;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GroupColumnData;

import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.IExplorerCellRenderer;


/**
 * Field combo cell renderer for GXT grids : display the field name and order fields by id.
 * 
 * @author HD3D
 */
public class FieldComboRenderer implements IExplorerCellRenderer<Hd3dModelData>
{
    /** Map containing combo field, indexed by their value. */
    protected final FastMap<FieldModelData> map;

    /** Default Constructor. */
    public FieldComboRenderer(FastMap<FieldModelData> map)
    {
        this.map = map;
    }

    /** @return The value to render. */
    public Object render(Hd3dModelData model, String property, ColumnData config, int rowIndex, int colIndex,
            ListStore<Hd3dModelData> store, Grid<Hd3dModelData> grid)
    {
        String obj = model.get(property);
        return this.getValueFromObject(obj);
    }

    protected String getValueFromObject(Object obj)
    {
        if (obj == null)
        {
            return null;
        }
        else
        {
            String stringValue = (String) obj;
            FieldModelData field = map.get(stringValue);

            if (field != null)
            {
                return field.getName();
            }
            else
            {
                return null;
            }
        }
    }

    public String render(GroupColumnData data)
    {
        return this.getValueFromObject(data.gvalue);
    }
}
