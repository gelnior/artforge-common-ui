package fr.hd3d.common.ui.client.widget.grid;

import com.extjs.gxt.ui.client.widget.grid.GroupingView;


public class BaseGroupingView extends GroupingView
{
    public void hideHeaders()
    {
        this.mainHd.setVisible(false);
        this.header.setVisible(false);
    }

    public void showHeaders()
    {
        this.mainHd.setVisible(true);
        this.header.setVisible(true);
    }

}
