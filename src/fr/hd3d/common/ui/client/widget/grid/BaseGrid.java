package fr.hd3d.common.ui.client.widget.grid;

import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.grid.GroupingView;

import fr.hd3d.common.ui.client.listener.EventSelectionChangedListener;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.service.store.BaseStore;
import fr.hd3d.common.ui.client.widget.EasyMenu;
import fr.hd3d.common.ui.client.widget.RefreshMenuItem;


public class BaseGrid<M extends Hd3dModelData> extends Grid<M>
{
    protected EasyMenu menu = new EasyMenu();
    protected RefreshMenuItem<M> refreshItem = new RefreshMenuItem<M>(null);

    public BaseGrid(BaseStore<M> baseStore, ColumnModel cm)
    {
        super(baseStore, cm);
        this.setView(new BaseGridView());
        this.refreshItem.setStore(baseStore);
        this.menu.add(refreshItem);
        this.setContextMenu(menu);
        this.setLoadMask(true);
    }

    public EasyMenu getMenu()
    {
        return this.menu;
    }

    @Override
    public void reconfigure(ListStore<M> store, ColumnModel cm)
    {
        super.reconfigure(store, cm);
        this.refreshItem.setStore((BaseStore<M>) store);
    }

    public void addEventSelectionChangedListener(EventType event)
    {
        this.getSelectionModel().addSelectionChangedListener(new EventSelectionChangedListener<M>(event));
    }

    public void setGroupingOn()
    {
        GroupingView view = new GroupingView();
        view.setShowGroupedColumn(false);
        view.setEnableGroupingMenu(false);
        this.setView(view);
    }

    public void showHeaders()
    {
        if (this.getView() instanceof BaseGroupingView)
        {
            ((BaseGroupingView) this.getView()).showHeaders();
        }
        else if (this.getView() instanceof BaseGridView)
        {
            ((BaseGridView) this.getView()).showHeaders();
        }
    }

    public void hideHeaders()
    {
        if (this.getView() instanceof BaseGroupingView)
        {
            ((BaseGroupingView) this.getView()).hideHeaders();
        }
        else if (this.getView() instanceof BaseGridView)
        {
            ((BaseGridView) this.getView()).hideHeaders();
        }
    }

    /**
     * @return Currently selected models.
     */
    public List<M> getSelection()
    {
        return this.getSelectionModel().getSelectedItems();
    }
}
