package fr.hd3d.common.ui.client.widget.grid.renderer;

import java.util.Date;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;

import fr.hd3d.common.ui.client.calendar.DateFormat;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.IExplorerCellRenderer;


/**
 * 
 * Displays text with a small padding around it (cosmectic purpose).
 * 
 * @author HD3D
 * 
 * @param <M>
 *            Type of the model of which field must be rendered.
 */
public class PaddingTextRenderer<M extends ModelData> implements IExplorerCellRenderer<M>
{

    public Object render(M model, String property, ColumnData config, int rowIndex, int colIndex, ListStore<M> store,
            Grid<M> grid)
    {
        Object obj = model.get(property);

        String stringValue = "";
        if (obj != null)
        {
            if (obj instanceof Date)
            {
                Date date = (Date) obj;
                obj = DateFormat.FRENCH_DATE.format(date);
            }
            stringValue = "<div style= 'padding: 3px;'>" + obj + "</div>";
        }

        return stringValue;
    }

}
