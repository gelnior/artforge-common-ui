package fr.hd3d.common.ui.client.widget.grid.editor;

import com.extjs.gxt.ui.client.widget.form.Field;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.StepModelData;


public class StepDurationEditor extends CellEditor
{
    /** Object edited by editor needed to update it after combo value selection. */
    protected StepModelData step;

    public StepDurationEditor(Field<Number> field)
    {
        super(field);
    }

    @Override
    public Object preProcessValue(Object value)
    {
        Long duration = 0L;
        if (value == null)
        {
            step = null;
        }
        else if (value instanceof Long)
        {
            step = null;
            duration = (Long) value;
        }
        else if (value instanceof StepModelData)
        {
            step = (StepModelData) value;
            duration = step.getEstimatedDuration();
        }
        return duration;
    }

    @Override
    public Object postProcessValue(Object value)
    {
        Object returnedValue = null;
        if (value == null)
        {
            returnedValue = null;
        }
        if (step == null)
        {
            returnedValue = getField().getValue();
        }
        else
        {
            Long estimatedDuration = (Long) getField().getValue();

            StepModelData newStep = new StepModelData();
            Hd3dModelData.copy(step, newStep);
            newStep.setEstimatedDuration(estimatedDuration);

            returnedValue = newStep;
        }
        return returnedValue;
    }
}
