package fr.hd3d.common.ui.client.widget.grid.editor;

import com.extjs.gxt.ui.client.widget.grid.CellEditor;

import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.widget.FieldComboBox;


/**
 * Cell Editor based on a field combo box. Useful when display data are not the same as real values.
 * 
 * @author HD3D
 */
public class FieldComboBoxEditor extends CellEditor
{
    /** The field combo box which act as the editor. */
    protected final FieldComboBox combo;

    /** Default Constructor. */
    public FieldComboBoxEditor(FieldComboBox combo)
    {
        super(combo);
        this.combo = combo;
    }

    @Override
    public Object preProcessValue(Object value)
    {
        if (value == null)
        {
            return value;
        }

        return combo.getValue(value);
    }

    @Override
    public Object postProcessValue(Object value)
    {
        if (value == null)
        {
            return value;
        }
        return ((FieldModelData) value).getValue();
    }
}
