package fr.hd3d.common.ui.client.widget.grid;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.google.gwt.core.client.JavaScriptException;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;


/**
 * Selection model for handling both cell and row selection.
 * 
 * @author HD3D
 * 
 * @param <M>
 *            Type of object displayed by grid.
 */
public class MultiLineCellSelectionModel<M extends Hd3dModelData> extends SimpleLineCellSelectionModel<M>
{
    private final ArrayList<Cell> selectedCells = new ArrayList<Cell>();

    public MultiLineCellSelectionModel(SelectableGridView view, EventType eventType)
    {
        super(view, eventType);
    }

    /**
     * When down key is pressed, the cell above the currently selected cell and its row are selected. If shift key is
     * not pressed, previously selected cell and its row are unselected.
     * 
     * @param e
     *            Key press event.
     */
    @Override
    protected void onKeyDown(GridEvent<M> e)
    {
        if (e.isShiftKey())
        {
            moveSelectionShift(1, 0);
        }
        else
        {
            super.onKeyDown(e);
        }
    }

    /**
     * When up key is pressed, the cell over the currently selected cell and its row are selected. If shift key is not
     * pressed, previously selected cell and its row are unselected.<br>
     * <br>
     * SELECTION_CHANGE event is raised.
     * 
     * @param e
     *            Key press event.
     */
    @Override
    protected void onKeyUp(GridEvent<M> e)
    {
        if (e.isShiftKey())
        {
            moveSelectionShift(-1, 0);
        }
        else
        {
            super.onKeyUp(e);
        }
    }

    private void moveSelectionShift(int moveRow, int moveCol)
    {
        if (selectedCell == null)
        {
            return;
        }

        int min = selectedCell.row;
        int max = selectedCell.row;
        for (Cell cell : selectedCells)
        {
            if (cell.row < min)
            {
                min = cell.row;
            }
            else if (cell.row > max)
            {
                max = cell.row;
            }
        }
        int row = selectedCell.row;
        if (moveRow < 0)
        {
            row = min;
        }
        else if (moveRow > 0)
        {
            row = max;
        }
        Cell newCell = super.getValidateCell(row + moveRow, selectedCell.cell + moveCol);

        selectRowAndCell(newCell.row, newCell.cell, true);
    }

    @Override
    protected void onCellClick(GridEvent<M> event, M selectedModel)
    {
        try
        {
            if (event.isShiftKey())
            {
                this.onShiftKey(event, selectedModel);
            }
            else if (event.isControlKey())
            {
                this.onControlClick(event, selectedModel);
            }
            else
            {
                this.onSimpleClick(event, selectedModel);
            }
        }
        catch (JavaScriptException exception)
        {
            this.onJavascriptError();
        }
    }

    @Override
    public void deselectAll()
    {
        for (Cell cell : selectedCells)
        {
            super.deselectRowAndCell(cell);
        }
        selectedCells.clear();
    }

    /**
     * Return if the row and the cell index is already selected.
     * 
     * @param selectedCell
     *            the cell containing the row and cell index
     * @return
     */
    private boolean isSelected(Cell selectedCell)
    {
        for (Cell cell : selectedCells)
        {
            if (cell.row == selectedCell.row && cell.cell == selectedCell.cell)
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Handler to the case with shift key pressed.
     * 
     * @param event
     *            the grid event
     * @param selectedModel
     *            the selected model
     */
    protected void onShiftKey(GridEvent<M> event, M selectedModel)
    {
        if (selectedCell == null)
        {
            selectRowAndCell(event.getRowIndex(), event.getColIndex(), false);
        }
        else if (event.getRowIndex() != -1 && event.getColIndex() != -1)
        {
            Cell beginCell = selectedCell;

            // we only select the same column index
            Cell endCell = new Cell(event.getRowIndex(), beginCell.cell);

            if (beginCell.row > endCell.row)
            {
                Cell tmp = endCell;
                endCell = beginCell;
                beginCell = tmp;
                if (!event.isControlKey())
                {
                    deselectAll();
                }
            }

            selectRowAndCell(beginCell, endCell, true);
            selectedCell = beginCell;
        }
    }

    /**
     * Select most rows and cells.
     * 
     * @param beginCell
     *            the first index
     * @param endCell
     *            the last index
     * @param keepExisting
     *            if keep selected existing.
     */
    private void selectRowAndCell(Cell beginCell, Cell endCell, boolean keepExisting)
    {
        for (int i = beginCell.row; i <= endCell.row; i++)
        {
            selectRowAndCell(i, endCell.cell, true);
        }
    }

    @Override
    public void selectRowAndCell(int rowIndex, int colIndex, boolean keepExisting)
    {
        Cell cell = new Cell(rowIndex, colIndex);
        if (!isSelected(cell) && cell.row != -1 && cell.cell != -1)
        {
            super.selectRowAndCell(rowIndex, colIndex, keepExisting);
            selectedCells.add(cell);
        }
    };

    /**
     * Handler to the case with control key pressed.
     * 
     * @param event
     *            the grid event
     * @param selectedModel
     *            the selected model
     */
    protected void onControlClick(GridEvent<M> event, M selectedModel)
    {
        if (isSelected(selectedModel))
        {
            this.deselectRowAndCell(event.getRowIndex(), event.getColIndex());
        }
        else
        {
            selectRowAndCell(event.getRowIndex(), event.getColIndex(), true);
        }
    }

    /**
     * Deselect the given row and the given cell.
     * 
     * @param rowIndex
     *            the given row
     * @param colIndex
     *            the given cell
     */
    public void deselectRowAndCell(int rowIndex, int colIndex)
    {
        Cell cell = removeCell(rowIndex, colIndex);
        super.deselectRowAndCell(cell);
    }

    /**
     * Remove the cell with the row index and the cell index in the selected cells.
     * 
     * @param rowIndex
     *            the row index
     * @param colIndex
     *            the cell index
     * @return the removed cell
     */
    private Cell removeCell(int rowIndex, int colIndex)
    {
        Cell toRemove = null;
        for (Cell cell : selectedCells)
        {
            if (cell.row == rowIndex && cell.cell == colIndex)
            {
                toRemove = cell;
                break;
            }
        }
        if (toRemove != null)
        {
            selectedCells.remove(toRemove);
            return toRemove;
        }
        return null;
    }

    /**
     * Move the selected cells to step.
     * 
     * @param i
     *            the step
     */
    public void moveSelectedCells(int i)
    {
        for (Cell c : selectedCells)
        {
            c.row = c.row + 1;
        }
    }

    /**
     * Return the list of selected cells.
     * 
     * @return the list of selected cells.
     */
    @Override
    public List<Cell> getSeletedCells()
    {
        return selectedCells;
    }

}
