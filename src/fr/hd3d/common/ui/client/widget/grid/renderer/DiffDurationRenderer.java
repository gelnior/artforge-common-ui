package fr.hd3d.common.ui.client.widget.grid.renderer;

import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.ColumnData;
import com.extjs.gxt.ui.client.widget.grid.Grid;

import fr.hd3d.common.ui.client.calendar.DatetimeUtil;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.IAggregationRenderer;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.IExplorerCellRenderer;


public class DiffDurationRenderer implements IExplorerCellRenderer<Hd3dModelData>, IAggregationRenderer
{
    static boolean negatif = false;

    public Object render(Hd3dModelData model, String property, ColumnData config, int rowIndex, int colIndex,
            ListStore<Hd3dModelData> store, Grid<Hd3dModelData> grid)
    {
        negatif = false;
        Long duration = model.get(property);
        String durationString = getDurationString(duration);
        
        String borderColor = "green";
        if(negatif)
        {
            borderColor = "red";
        }
        
        
        return "<div style='white-space: normal; padding: 4px;" 
        + " border: 2px solid " + borderColor + "';>" + durationString + "</div>";

        //return "<div style='padding: 3px;'>" + durationString + "</div>";
    }

    public static String getDurationString(Long duration)
    {
        String durationString = "";
        if (duration != null)
        {
            if (duration < 0) 
            { 
                durationString = "- ";
                duration = - duration;
                negatif = true;
            }
            
            float nbDay = duration / DatetimeUtil.DAY_SECONDS;
            int days = (int) Math.floor(nbDay);
            duration = duration % DatetimeUtil.DAY_SECONDS;
            float nbHours = duration / DatetimeUtil.HOUR_SECONDS;
            int hours = (int) Math.floor(nbHours);
            duration = duration % DatetimeUtil.HOUR_SECONDS;
            float nbMinutes = duration / DatetimeUtil.MINUTE_SECONDS;
            int minutes = (int) Math.floor(nbMinutes);

            if (days > 0)
            {
                durationString += days + "d ";
            }
            if (hours > 0)
            {
                durationString += hours + "h ";
            }
            if (minutes > 0)
            {
                durationString += minutes + "min ";
            }
        }

        return durationString;
    }

    public Object renderAggregate(Object value)
    {
        return getDurationString((Long) value);
    }

};
