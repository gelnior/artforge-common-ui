package fr.hd3d.common.ui.client.widget;

import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.google.gwt.user.client.ui.AbstractImagePrototype;

import fr.hd3d.common.ui.client.listener.ButtonClickListener;


/**
 * The tool bar button already sets an icon and a listener that dispactches to controllers the event given in the
 * constructor.
 * 
 * @author HD3D
 */
public class ToolBarButton extends Button
{
    /**
     * Default Constructor.
     * 
     * @param iconStyle
     *            Icon at CCS format.
     * @param toolTip
     *            Tool tip to display.
     * @param eventType
     *            Event to forward on click.
     */
    public ToolBarButton(String iconStyle, String toolTip, EventType eventType)
    {
        super();
        this.setStyle(iconStyle, toolTip);
        this.registerListener(eventType);
    }

    /**
     * Alternative Constructor.
     * 
     * @param icon
     *            Icon at GXT format.
     * @param toolTip
     *            Tool tip to display.
     * @param eventType
     *            Event to forward on click.
     */
    public ToolBarButton(AbstractImagePrototype icon, String toolTip, EventType eventType)
    {
        super();
        this.setStyle(icon, toolTip);
        this.registerListener(eventType);
    }

    /**
     * Alternative Constructor.
     * 
     * @param icon
     *            Icon at GXT format.
     * @param toolTip
     *            Tool tip to display.
     * @param listener
     *            listener to forward on click.
     */
    public ToolBarButton(AbstractImagePrototype icon, SelectionListener<ButtonEvent> listener, String toolTip)
    {
        super();
        this.setStyle(icon, toolTip);
        this.addSelectionListener(listener);
    }

    /**
     * Set buttons styles.
     * 
     * @param iconStyle
     *            Icon at GXT format.
     * @param toolTip
     *            Tool tip to display.
     */
    private void setStyle(AbstractImagePrototype icon, String toolTip)
    {
        this.setIcon(icon);
        this.setToolTip(toolTip);
    }

    /**
     * Register on tool item click event. When the button is clicked, it raise the evenType event.
     */
    protected void registerListener(EventType eventType)
    {
        if (eventType != null)
        {
            this.addSelectionListener(new ButtonClickListener(eventType));
        }
    }

    /**
     * Sets icon and tool tip.
     */
    protected void setStyle(String iconStyle, String toolTip)
    {
        this.setIconStyle(iconStyle);
        this.setToolTip(toolTip);
    }
}
