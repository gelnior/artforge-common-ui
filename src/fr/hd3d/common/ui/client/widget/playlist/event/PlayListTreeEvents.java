package fr.hd3d.common.ui.client.widget.playlist.event;

import com.extjs.gxt.ui.client.event.EventType;


public class PlayListTreeEvents
{
    public static final int START_EVENT = 16000;

    public static final EventType PLAYLISTREVISION_SELECTED = new EventType();
    public static final EventType PLAYLISTREVISION_GROUP_SELECTED = new EventType();
}
