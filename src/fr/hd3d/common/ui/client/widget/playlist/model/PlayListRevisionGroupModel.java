package fr.hd3d.common.ui.client.widget.playlist.model;

import java.util.List;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.model.TreeModel;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListRevisionGroupModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListRevisionModelData;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.LuceneConstraint;


public class PlayListRevisionGroupModel extends TreeModel
{

    /** LuceneConstraint to reload all shots from list of sequences ( included the sub sequences ). */
    private final LuceneConstraint groupConstraintId = new LuceneConstraint(EConstraintOperator.in,
            "playlistrevisiongroup");

    private final Constraint playlistConstraintId = new Constraint(EConstraintOperator.in, "id");

    public void setConstraintPlayListRevisionGroup(List<PlayListRevisionGroupModelData> playListRevisionGroups)
    {
        this.setConstraints(playListRevisionGroups, groupConstraintId);
    }

    public void setConstraintPlayListRevision(List<PlayListRevisionModelData> playListRevisions)
    {
        this.setConstraints(playListRevisions, playlistConstraintId);
    }

    public void removePlayListRevisionGroupConstraint()
    {
        this.store.removeParameter(groupConstraintId);
    }

    public void removePlayListRevisionConstraint()
    {
        this.getFilter().setRightMember(null);
    }

}
