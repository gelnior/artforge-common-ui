package fr.hd3d.common.ui.client.widget.playlist.view;

import java.util.List;

import com.extjs.gxt.ui.client.store.TreeStore;
import com.extjs.gxt.ui.client.widget.LayoutContainer;
import com.extjs.gxt.ui.client.widget.layout.FlowLayout;
import com.extjs.gxt.ui.client.widget.treepanel.TreePanel;
import com.google.gwt.user.client.Element;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListRevisionGroupModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.reader.RecordReader;
import fr.hd3d.common.ui.client.service.proxy.TreeServicesProxy;
import fr.hd3d.common.ui.client.widget.basetree.BaseTreeDataModel;
import fr.hd3d.common.ui.client.widget.playlist.model.PlayListRevisionTreeLoader;


public class PlayListRevisionGroupTreeTesting extends LayoutContainer
{

    @Override
    protected void onRender(Element parent, int index)
    {
        super.onRender(parent, index);

        TreeServicesProxy<RecordModelData> proxy = new TreeServicesProxy<RecordModelData>(new RecordReader());
        BaseTreeDataModel<PlayListRevisionModelData, PlayListRevisionGroupModelData> baseTreeDataModel = new BaseTreeDataModel<PlayListRevisionModelData, PlayListRevisionGroupModelData>(
                new PlayListRevisionTreeLoader(proxy));
        List<RecordModelData> models = baseTreeDataModel.getStore().getModels();

        final TreeStore<RecordModelData> store = new TreeStore<RecordModelData>();
        store.add(models, true);
        final TreePanel<RecordModelData> tree = new TreePanel<RecordModelData>(store);
        tree.setDisplayProperty(PlayListRevisionGroupModelData.NAME_FIELD);

        setLayout(new FlowLayout());
        add(tree);
    }

}
