package fr.hd3d.common.ui.client.widget.playlist.view;

import java.util.Stack;

import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.event.LoadListener;
import com.extjs.gxt.ui.client.store.Store;
import com.extjs.gxt.ui.client.store.TreeStore;
import com.extjs.gxt.ui.client.widget.form.StoreFilterField;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListRevisionGroupModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.modeldata.reader.RecordReader;
import fr.hd3d.common.ui.client.modeldata.reader.playlist.PlayListRevisionGroupReader;
import fr.hd3d.common.ui.client.modeldata.reader.playlist.PlayListRevisionReader;
import fr.hd3d.common.ui.client.service.proxy.TreeServicesProxy;
import fr.hd3d.common.ui.client.widget.basetree.BaseTree;
import fr.hd3d.common.ui.client.widget.basetree.BaseTreeDataModel;
import fr.hd3d.common.ui.client.widget.playlist.event.PlayListRevisionChangedListener;
import fr.hd3d.common.ui.client.widget.playlist.model.PlayListRevisionTreeLoader;
import fr.hd3d.common.ui.client.widget.workobjecttree.WorkObjectTreeIconProvider;


/**
 * PlayListRevisionTree tree allows user to browse PlayListRevisionGroups and PlayListRevision of a given project.
 * 
 * It displays the following hierarchy : <br>
 * - PlayListRevisionGroupModelData<br>
 * | - PlayListRevisionModelData<br>
 * | | - Revision (PlayListRevisionModelData)<br>
 * | | - Revision (PlayListRevisionModelData)<br>
 * | - PlayListRevisionModelData<br>
 * | | - Revision (PlayListRevisionModelData)<br>
 * | - PlayListRevisionModelData<br>
 * | | - Revision (PlayListRevisionModelData)<br>
 * | | - Revision (PlayListRevisionModelData)<br>
 * | | - Revision (PlayListRevisionModelData)<br>
 * - PlayListRevisionGroupModelData<br>
 * ...
 * 
 */
public class PlayListRevisionTree extends BaseTree<PlayListRevisionModelData, PlayListRevisionGroupModelData>
{
    /** Constant strings to display : dialog messages, button label... */
    public static final CommonConstants CONSTANTS = GWT.create(CommonConstants.class);
    /** Listener used to dispatch events when category or constituent is clicked. */
    protected PlayListRevisionChangedListener listener = new PlayListRevisionChangedListener(this);
    /** Filter used as quick access feature */
    protected StoreFilterField<RecordModelData> filter;

    /**
     * Default constructor, set reader and tree root title.
     */
    public PlayListRevisionTree()
    {
        this(CONSTANTS.Playlists());
    }

    /**
     * Default constructor, set reader and tree root title with @rootName.
     * 
     * @param rootName
     *            Tree root name.
     */
    public PlayListRevisionTree(String rootName)
    {
        super(rootName, new PlayListRevisionReader(), new PlayListRevisionGroupReader());
        this.tree.setIconProvider(new WorkObjectTreeIconProvider(model.getStore()));
        setSelectionChangedListener();
        enableDragNDrop();
        tree.setDisplayProperty(PlayListRevisionModelData.ENHANCED_NAME);
    }

    public void setRootName(String rootName)
    {
        this.model.setRootName(rootName);
    }

    /**
     * Returns the filter Field attached to the current tree store.
     * 
     * @return filter Field attached to the current tree store
     */
    public StoreFilterField<RecordModelData> getFilter()
    {
        return enableFiltering();
    }

    /**
     * Return the store associated with the tree.
     * 
     * @return TreeStore associated to the current tree
     */
    @Override
    public TreeStore<RecordModelData> getStore()
    {
        return tree == null ? null : tree.getStore();
    }

    /**
     * <p>
     * Warning : this will work only if the content of the tree is loaded !
     * </p>
     * <p>
     * Sets up the filter Field attached to the current tree store.
     * </p>
     * 
     * @return filter Field attached to the current tree store
     */
    private StoreFilterField<RecordModelData> enableFiltering()
    {
        if (tree == null || tree.getStore() == null)
            return null;

        if (this.filter != null)
            return this.filter;

        this.filter = new StoreFilterField<RecordModelData>() {
            @Override
            protected boolean doSelect(Store<RecordModelData> store, RecordModelData parent, RecordModelData record,
                    String property, String filter)
            {
                if (!PlayListRevisionModelData.SIMPLE_CLASS_NAME.equals(record.getSimpleClassName()))
                {
                    return false;
                }
                String name = record.getName().toLowerCase();
                if (name.startsWith(filter.toLowerCase()))
                {
                    return true;
                }
                return false;
            }
        };
        this.filter.bind(this.tree.getStore());
        return this.filter;
    }

    /**
     * Enables DragNDrop feature as described on PlayListTreePanelDragSource.
     */
    protected void enableDragNDrop()
    {
        new PlayListTreePanelDragSource(tree, this.getId());
    }

    @Override
    protected void setModel(String rootName, IReader<PlayListRevisionModelData> leafReader,
            IReader<PlayListRevisionGroupModelData> parentReader)
    {

        TreeServicesProxy<RecordModelData> proxy = new TreeServicesProxy<RecordModelData>(new RecordReader());
        this.model = new BaseTreeDataModel<PlayListRevisionModelData, PlayListRevisionGroupModelData>(
                new PlayListRevisionTreeLoader(proxy));
        this.model.setRootName(rootName);
    }

    @Override
    @SuppressWarnings("unchecked")
    public BaseTreeDataModel<PlayListRevisionModelData, PlayListRevisionGroupModelData> getModel()
    {
        return this.model;
    }

    /**
     * Add a selection changed listener to send events to controllers when a constituent or category has been selected.
     */
    private void setSelectionChangedListener()
    {
        this.tree.getSelectionModel().addSelectionChangedListener(listener);
    }

    /**
     * Used to set the selection and load children of ids nodes.
     * 
     * @param ids
     *            nodes to be selected
     */
    public void selectPlayListRevisionGroup(final Stack<Long> ids)
    {
        Long id = ids.pop();

        RecordModelData playListRevisionGroup = this.getStore().findModel(Hd3dModelData.ID_FIELD, id);
        if (playListRevisionGroup != null)
        {
            if (ids.size() == 0)
            {
                this.tree.getSelectionModel().select(false, playListRevisionGroup);
            }
            else
            {
                if (this.tree.isExpanded(playListRevisionGroup))
                {
                    this.selectPlayListRevisionGroup(ids);
                }
                else
                {
                    LoadListener pathListener = new LoadListener() {
                        @Override
                        public void loaderLoad(LoadEvent le)
                        {
                            model.removeLoadListener(this);
                            selectPlayListRevisionGroup(ids);
                        }
                    };
                    this.model.addLoadListener(pathListener);
                    this.model.getLoader().loadChildren(playListRevisionGroup);
                }
            }
        }
    }

}
