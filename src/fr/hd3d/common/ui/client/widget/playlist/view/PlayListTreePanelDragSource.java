package fr.hd3d.common.ui.client.widget.playlist.view;

import com.extjs.gxt.ui.client.dnd.TreePanelDragSource;
import com.extjs.gxt.ui.client.dnd.TreePanelDropTarget;
import com.extjs.gxt.ui.client.dnd.DND.Feedback;
import com.extjs.gxt.ui.client.widget.treepanel.TreePanel;


/**
 * When linked to a TreePanel object, allows drag and drop operations on it. The TreePanel object is both source and
 * target of the DND.
 * 
 */
public class PlayListTreePanelDragSource extends TreePanelDragSource
{

    private final String scrollElementId;

    @SuppressWarnings("unchecked")
    public PlayListTreePanelDragSource(TreePanel tree, String scrollElementId)
    {
        super(tree);
        this.scrollElementId = scrollElementId;
        this.initialize();
    }

    @SuppressWarnings("unchecked")
    public PlayListTreePanelDragSource(TreePanel tree)
    {
        this(tree, null);
    }

    private void initialize()
    {
        TreePanelDropTarget target = new TreePanelDropTarget(tree);
        target.setAllowSelfAsSource(true);
        target.setFeedback(Feedback.BOTH);
        if (scrollElementId != null)
            target.setScrollElementId(this.scrollElementId);
        target.setAllowDropOnLeaf(false);
    }

}
