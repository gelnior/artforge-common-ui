package fr.hd3d.common.ui.client.widget.playlist.event;

import java.util.ArrayList;

import com.extjs.gxt.ui.client.event.SelectionChangedEvent;
import com.extjs.gxt.ui.client.event.SelectionChangedListener;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListRevisionGroupModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListRevisionModelData;
import fr.hd3d.common.ui.client.widget.playlist.view.PlayListRevisionTree;


/**
 * Listener used to handle selection change events on a PlayListRevisionTree. This will thus listen to changes on
 * selection of both PlayListRevisionGroup and PlayListRevision items.
 * 
 * @author guillaume-maucomble
 * @version 1.0
 * @since 1.0
 * 
 */
public class PlayListRevisionChangedListener extends SelectionChangedListener<RecordModelData>
{

    /** Original Tree */
    private final PlayListRevisionTree playListRevisionTree;

    /**
     * Default constructor.
     * 
     * @param playListRevisionTree
     */
    public PlayListRevisionChangedListener(PlayListRevisionTree playListRevisionTree)
    {
        this.playListRevisionTree = playListRevisionTree;
    }

    @Override
    public void selectionChanged(SelectionChangedEvent<RecordModelData> se)
    {
        RecordModelData firstSelection = se.getSelectedItem();

        if (firstSelection != null && firstSelection.getClassName().equals(PlayListRevisionModelData.CLASS_NAME))
        {

            ArrayList<PlayListRevisionModelData> playListList = new ArrayList<PlayListRevisionModelData>();
            for (RecordModelData item : se.getSelection())
            {
                if (item.getClassName().equals(PlayListRevisionModelData.CLASS_NAME))
                {
                    PlayListRevisionModelData playListRevision = new PlayListRevisionModelData();
                    Hd3dModelData.copy(item, playListRevision);
                    playListList.add(playListRevision);
                }
            }

            // this.playListRevisionTree.onPlayListRevisionClicked(playListList);
        }

        if (firstSelection != null && firstSelection.getClassName().equals(PlayListRevisionGroupModelData.CLASS_NAME))
        {

            ArrayList<PlayListRevisionGroupModelData> playListRevisionGroupList = new ArrayList<PlayListRevisionGroupModelData>();
            for (RecordModelData item : se.getSelection())
            {
                if (item.getClassName().equals(PlayListRevisionGroupModelData.CLASS_NAME))
                {
                    PlayListRevisionGroupModelData playListRevisionGroup = new PlayListRevisionGroupModelData();
                    Hd3dModelData.copy(item, playListRevisionGroup);
                    playListRevisionGroupList.add(playListRevisionGroup);
                }
            }

            // this.playListRevisionTree.onPlayListRevisionGroupClicked(playListRevisionGroupList);
        }
    }

}
