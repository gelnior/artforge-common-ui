package fr.hd3d.common.ui.client.widget.playlist.model;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.TreeLoadEvent;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListRevisionGroupModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.reader.playlist.PlayListRevisionGroupReader;
import fr.hd3d.common.ui.client.modeldata.reader.playlist.PlayListRevisionReader;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.EqConstraint;
import fr.hd3d.common.ui.client.service.parameter.LuceneConstraint;
import fr.hd3d.common.ui.client.service.parameter.OrderBy;
import fr.hd3d.common.ui.client.service.proxy.TreeServicesProxy;
import fr.hd3d.common.ui.client.widget.basetree.BaseTreeDataLoader;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;


public class PlayListRevisionTreeLoader extends
        BaseTreeDataLoader<PlayListRevisionModelData, PlayListRevisionGroupModelData>
{

    /**
     * Default constructor sets proxy, reader and default instance.
     * 
     * @param proxy
     *            The proxy needed to access to services.
     */
    public PlayListRevisionTreeLoader(TreeServicesProxy<RecordModelData> proxy)
    {
        super(proxy, new PlayListRevisionReader(), new PlayListRevisionGroupReader(), new PlayListRevisionModelData(),
                new PlayListRevisionGroupModelData());
    }

    // @Override
    // public boolean loadChildren(RecordModelData parent)
    // {
    // PlayListRevisionGroupModelData instance = getParentInstance();
    // PlayListRevisionModelData lInstance = getLeafInstance();
    // servicesProxy.clearParameters();
    // servicesProxy.addParameter(new OrderBy(RecordModelData.NAME_FIELD));
    // initReader();
    // // servicesProxy.setReader((IReader<RecordModelData>) parentReader);
    //
    // if (parent == null || parent.getId().longValue() == -1L)
    // {
    // return (this.loadNullRoot(instance.getSimpleClassName()) && this.loadNullRoot(lInstance
    // .getSimpleClassName()));
    // }
    // else if (instance.getClassName().equals(parent.getClassName()))
    // {
    // return this.loadParent(parent);
    // }
    // else
    // {
    // return false;
    // }
    // }

    /**
     * Returns a String representation of the current path for the simpleClassName within project context in the form
     * /projects/ {id}/simpleClassName/
     * 
     * @param simpleClassName
     *            String representation of the class to be loaded
     * @return String representation of the path
     */
    private String getPath(String simpleClassName)
    {
        String path = "";
        currentProjectId = MainModel.getCurrentProject().getId();
        if (currentProjectId != null)
        {
            path = ServicesPath.getPath(ProjectModelData.SIMPLE_CLASS_NAME);
            path += currentProjectId + "/";
        }
        path += ServicesPath.getPath(simpleClassName);

        return path;
    }

    /**
     * Load children for a treen node (parent). If parent has no ID, it considers that it has no children, and add
     * nothing to the tree but considers that loading succeeds.
     * 
     * @param parent
     *            The node of which children to load.
     * @return true if loading succeeds.
     */
    protected boolean loadParent(RecordModelData parent)
    {
        if (parent.getId() != null)
        {
            String path = getPath(parent.getSimpleClassName()) + parent.getId() + ServicesPath.CHILDREN;
            servicesProxy.clearParameters();
            servicesProxy.setPath(path);
            this.children.add(parent);
        }
        else
        {
            this.onLoadSuccess(null, new ArrayList<RecordModelData>());

        }
        return load(parent);
    }

    /**
     * Load tree nodes which has no parent (root nodes). If a project is set it retrieves only nodes of this project.<br>
     * It loads only parents, not leaves.
     * 
     * @param simpleClassName
     *            Class name of data to retrieve.
     * @return True if succeeds.
     */
    protected boolean loadNullRoot(String simpleClassName)
    {
        servicesProxy.clearParameters();
        servicesProxy.setPath(getPath(simpleClassName));
        return load(null) && loadNullRootLeaves(leafInstance.getSimpleClassName());
    }

    protected boolean loadNullRootLeaves(String simpleClassName)
    {
        servicesProxy.clearParameters();
        servicesProxy.setPath(ServicesPath.getPath(simpleClassName));
        servicesProxy.addParameter(new EqConstraint("project", MainModel.getCurrentProject().getId()));
        return load(null);
    }

    /**
     * Load tree nodes which has for parent the root node. If a project is set it retrieves only nodes of this project..<br>
     * It loads only parents, not leaves.
     * 
     * @param parent
     *            The parent root.
     * @param simpleClassName
     *            Class name of data to retrieve.
     * @return True if succeeds.
     */
    protected boolean loadRoot(RecordModelData parent, String simpleClassName)
    {
        servicesProxy.setPath(getPath(simpleClassName));
        servicesProxy.clearParameters();
        this.children.add(parent);

        return load(parent);
    }

    /**
     * Defines behavior to adopt when node children are successfully loaded.
     * 
     * @param loadConfig
     *            The loading configuration.
     * @param result
     *            list of children.
     */
    @Override
    protected void onLoadSuccess(Object loadConfig, List<RecordModelData> result)
    {
        if (loadConfig != null && children.contains(loadConfig))
        {
            RecordModelData parent = (RecordModelData) loadConfig;
            if (PlayListRevisionModelData.CLASS_NAME.equals(parent.getClassName()))
            {
                TreeLoadEvent evt = new TreeLoadEvent(this, loadConfig, result);
                evt.parent = parent;
                fireEvent(Load, evt);
                children.remove(loadConfig);
            }
        }
        super.onLoadSuccess(loadConfig, result);
    }

    /**
     * Update order by name and revision.
     */
    @Override
    protected void loadLeaves(RecordModelData parent)
    {
        servicesProxy.clearParameters();
        List<String> columns = new ArrayList<String>();
        columns.add(PlayListRevisionModelData.NAME_FIELD);
        columns.add(PlayListRevisionModelData.REVISION_FIELD);
        servicesProxy.addParameter(new OrderBy(columns));
        PlayListRevisionModelData instance = getLeafInstance();

        String path = getPath(parent.getSimpleClassName()) + parent.getId() + "/"
                + ServicesPath.getPath(instance.getSimpleClassName());
        servicesProxy.setPath(path);
        setLeafReader();

        load(parent);
    }

    protected void loadRevisions(PlayListRevisionModelData parent)
    {
        servicesProxy.clearParameters();
        List<String> columns = new ArrayList<String>();
        columns.add(PlayListRevisionModelData.REVISION_FIELD);
        servicesProxy.addParameter(new OrderBy(columns));

        // Create a Lucene constraint ?

        final LuceneConstraint nameConstraint = new LuceneConstraint(EConstraintOperator.eq,
                PlayListRevisionModelData.NAME_FIELD, "");

    }

}
