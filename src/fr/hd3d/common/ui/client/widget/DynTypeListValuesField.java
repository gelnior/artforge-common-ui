package fr.hd3d.common.ui.client.widget;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.restlet.client.Request;
import org.restlet.client.Response;
import org.restlet.client.ext.json.JsonRepresentation;

import com.extjs.gxt.ui.client.Style.Orientation;
import com.extjs.gxt.ui.client.Style.SelectionMode;
import com.extjs.gxt.ui.client.dnd.GridDragSource;
import com.extjs.gxt.ui.client.dnd.GridDropTarget;
import com.extjs.gxt.ui.client.dnd.DND.Feedback;
import com.extjs.gxt.ui.client.event.ButtonEvent;
import com.extjs.gxt.ui.client.event.DNDEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.GridEvent;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.HorizontalPanel;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.form.AdapterField;
import com.extjs.gxt.ui.client.widget.form.MultiField;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.CheckBoxSelectionModel;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.grid.EditorGrid;
import com.extjs.gxt.ui.client.widget.grid.EditorGrid.ClicksToEdit;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Overflow;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.modeldata.reader.ListValuesReader;
import fr.hd3d.common.ui.client.modeldata.technical.ClassDynModelData;
import fr.hd3d.common.ui.client.modeldata.technical.DynTypeModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ListValuesModelData;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.EqConstraint;
import fr.hd3d.common.ui.client.util.GridUtils;
import fr.hd3d.common.ui.client.widget.attributeeditor.event.ExtendedAttributeEditorEvents;
import fr.hd3d.common.ui.client.widget.attributeeditor.model.ExtendedAttributeEditorModel;


public class DynTypeListValuesField extends MultiField<FieldModelData>
{
    private static final int HEIGHT = 300;
    private static final int WIDTH = 210;
    private static final String NEW_VALUE = "New Value";
    private final Button addButton;
    private final Button deleteButton;
    private EditorGrid<FieldModelData> editableNameGrid;
    private ListStore<FieldModelData> listStore = new ListStore<FieldModelData>();
    private ListValuesModelData listValues;
    private String defaultListValueString;
    public static final CommonConstants CONSTANTS = GWT.create(CommonConstants.class);
    private boolean isNewValue = false;
    private boolean hasChanges = false;
    private final ExtendedAttributeEditorModel<ClassDynModelData, DynTypeModelData> model;

    public DynTypeListValuesField(DynTypeModelData associatedDynTypeModelData,
            ExtendedAttributeEditorModel<ClassDynModelData, DynTypeModelData> model)
    {

        this.model = model;
        this.orientation = Orientation.VERTICAL;
        this.addButton = new Button();
        this.deleteButton = new Button();

        this.addButton.setIcon(Hd3dImages.getAddIcon());
        this.deleteButton.setIcon(Hd3dImages.getDeleteIcon());
        this.listStore.setMonitorChanges(true);

        HorizontalPanel horizontalPanel = new HorizontalPanel();
        horizontalPanel.add(addButton);
        horizontalPanel.add(deleteButton);

        AdapterField adapterField = new AdapterField(horizontalPanel);
        add(adapterField);
        configureAndAddValueGrid();

        this.setSize(WIDTH, HEIGHT);

        addButton.addSelectionListener(new SelectionListener<ButtonEvent>() {
            @Override
            public void componentSelected(ButtonEvent ce)
            {
                if (!isNewValue)
                {
                    FieldModelData fieldModelData = new FieldModelData(listStore.getCount(), "", NEW_VALUE);
                    editableNameGrid.stopEditing();
                    listStore.add(fieldModelData);
                    editableNameGrid.startEditing(listStore.indexOf(fieldModelData), 1);
                    isNewValue = true;
                }
            }
        });
        deleteButton.addSelectionListener(new SelectionListener<ButtonEvent>() {
            @Override
            public void componentSelected(ButtonEvent ce)
            {
                FieldModelData selectedItem = editableNameGrid.getSelectionModel().getSelectedItem();
                if (selectedItem != null)
                {
                    String values = listValues.getValues();
                    listStore.remove(selectedItem);
                    values = values.replaceFirst(selectedItem.getName(), "");
                    values = values.replaceFirst(listValues.getSeparator() + listValues.getSeparator(),
                            listValues.getSeparator());
                    listValues.setValues(values);
                    setHasChanges();
                }
            }
        });
        if (associatedDynTypeModelData != null)
        {
            getDynTypeValues(associatedDynTypeModelData);
        }
    }

    private void configureAndAddValueGrid()
    {
        final List<ColumnConfig> columns = new ArrayList<ColumnConfig>();
        CheckBoxSelectionModel<FieldModelData> gridSelectionModel = new CheckBoxSelectionModel<FieldModelData>();
        gridSelectionModel.setSelectionMode(SelectionMode.SINGLE);
        ColumnConfig nameColumn = GridUtils.addColumnConfig(columns, FieldModelData.NAME_FIELD, CONSTANTS.Name(), 150);
        nameColumn.setResizable(false);
        nameColumn.setEditor(new CellEditor(new TextField<String>()));
        ColumnModel columnModel = new ColumnModel(columns);
        editableNameGrid = new EditorGrid<FieldModelData>(this.listStore, columnModel);
        editableNameGrid.setSelectionModel(gridSelectionModel);
        editableNameGrid.setSize(WIDTH, HEIGHT - 30);
        editableNameGrid.setClicksToEdit(ClicksToEdit.TWO);
        editableNameGrid.setAutoExpandColumn(FieldModelData.NAME_FIELD);
        editableNameGrid.addListener(Events.AfterEdit, new Listener<GridEvent<FieldModelData>>() {

            public void handleEvent(GridEvent<FieldModelData> be)
            {
                if (be.getProperty().equals(FieldModelData.NAME_FIELD))
                {
                    if (!"".equals(be.getValue()) && be.getValue() != null)
                    {
                        isNewValue = false;
                        if (listValues.getValues() != null)
                        {
                            listValues.setValues(listValues.getValues() + listValues.getSeparator() + be.getValue());
                        }
                        else
                        {
                            listValues.setValues(be.getValue().toString());
                        }
                        listStore.commitChanges();
                        setHasChanges();
                    }
                }
            }
        });
        new GridDragSource(editableNameGrid);
        GridDropTarget target = new GridDropTarget(editableNameGrid) {
            @Override
            protected void onDragDrop(DNDEvent e)
            {
                super.onDragDrop(e);
                resetValues();
            }
        };
        target.setAllowSelfAsSource(true);
        target.setFeedback(Feedback.INSERT);
        AdapterField adapterField = new AdapterField(editableNameGrid);
        adapterField.getElement().getStyle().setOverflow(Overflow.HIDDEN);
        add(adapterField);
    }

    protected void resetValues()
    {
        StringBuffer values = new StringBuffer();

        for (FieldModelData fieldModelData : this.editableNameGrid.getStore().getModels())
        {
            if (values.length() > 0)
                values.append(listValues.getSeparator());
            values.append(fieldModelData.getName());
        }

        this.listValues.setValues(values.toString());
        this.setHasChanges();
    }

    private void getDynTypeValues(final DynTypeModelData associatedDynTypeModelData)
    {
        if (associatedDynTypeModelData.getStructId() != null && associatedDynTypeModelData.getStructId().intValue() > 0)
        {
            String path = ServicesPath.getPath(ListValuesModelData.SIMPLE_CLASS_NAME);
            Constraint constraint = new EqConstraint(ListValuesModelData.ID_FIELD,
                    associatedDynTypeModelData.getStructId());
            path += "?" + constraint.toString();
            RestRequestHandlerSingleton.getInstance().getRequest(path, new BaseCallback() {
                @Override
                protected void onSuccess(Request request, Response response)
                {
                    JsonRepresentation representation = new JsonRepresentation(response.getEntity());
                    ListValuesReader reader = new ListValuesReader();
                    try
                    {
                        List<ListValuesModelData> readData = reader.readData(new ListValuesModelData(),
                                representation.getText());
                        if (readData.size() == 1)
                        {
                            listValues = readData.get(0);
                            associatedDynTypeModelData.setStructId(listValues.getId());
                            defaultListValueString = listValues.getValues();
                            if (!defaultListValueString.isEmpty())
                            {
                                String[] values = defaultListValueString.split(listValues.getSeparator());
                                int index = 0;
                                for (String value : values)
                                {
                                    FieldModelData fieldModelData = new FieldModelData(index++, value, value);
                                    listStore.add(fieldModelData);
                                    EventDispatcher.forwardEvent(ExtendedAttributeEditorEvents.LIST_VALUES_VALUES_LOADED, associatedDynTypeModelData);
                                }
                            }
                        }
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }
            });
        }
        else
        {
            listValues = model.getListValueModelDataByStructId(associatedDynTypeModelData.getStructId());
        }
    }

    public ListValuesModelData getListValues()
    {
        return listValues;
    }

    public FieldModelData getDefaultValue()
    {
        return editableNameGrid.getSelectionModel().getSelectedItem();
    }

    public boolean isHasChanges()
    {
        return hasChanges;
    }

    private void setHasChanges()
    {
        hasChanges = !(listValues.getValues().equals(defaultListValueString));
        if (hasChanges)
        {
            if (listValues.getId().intValue() <= 0)
            {
                model.getAddListValuesMap().put(listValues.getId().toString(), listValues);
            }
            else
            {
                model.getUpdateListValuesMap().put(listValues.getId().toString(), listValues);
            }
        }
    }
    
    public ListStore<FieldModelData> getListStore()
    {
        return listStore;
    }
}
