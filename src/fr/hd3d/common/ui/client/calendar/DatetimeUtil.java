package fr.hd3d.common.ui.client.calendar;

import java.util.Date;

import com.extjs.gxt.ui.client.util.DateWrapper;

import fr.hd3d.common.ui.client.modeldata.writer.ClientDateWriter;
import fr.hd3d.common.ui.client.modeldata.writer.IDateWriter;


/**
 * Utility functions and constants for date management.
 * 
 * @author HD3D
 */
public class DatetimeUtil
{
    public static final int HOUR_SECONDS = 3600;
    public static final int QUARTER_HOUR_SECONDS = 900;
    public static final int MINUTE_SECONDS = 60;
    public static final int DAY_SECONDS = 28800;
    public static final int DAY_MILLISECONDS = DAY_SECONDS * 1000;

    /**
     * @return Return current day with hours and seconds set at 0.
     */
    public static Date today()
    {
        return new DateWrapper().clearTime().asDate();
    }

    /**
     * @return The first day of currentMonth with hours and seconds set at 0.
     */
    public static Date getFirstDayOfCurrentMonth()
    {
        return new DateWrapper().clearTime().getFirstDayOfMonth().asDate();
    }

    /**
     * @return Current date as a string formatted like this "yyyy-MM-dd HH:mm:ss".
     */
    public static String todayAsString()
    {
        return DateFormat.DATE_TIME.format(today());
    }

    /**
     * @param nbDays
     *            The number of day separating returned date from current day.
     * @return Date corresponding to the day which precedes current day from <i>nbDays</i>.
     */
    public static Date daysBeforeToday(int nbDays)
    {
        return new DateWrapper().clearTime().addDays(-nbDays).asDate();
    }

    /**
     * @return Date corresponding to the day that follows current day.
     */
    public static Date tomorrow()
    {
        DateWrapper wrapper = new DateWrapper(new Date()).clearTime();
        return wrapper.addDays(1).asDate();
    }

    /**
     * @return Date corresponding to the day that follows current day as a string "yyyy-MM-dd HH:mm:ss".
     */
    public static String tomorrowAsString()
    {
        return DateFormat.DATE_TIME.format(tomorrow());
    }

    /**
     * @param date
     *            The date of which number of seconds is requested.
     * @return For a given, the number seconds set in its hours and minutes fields.
     */
    public static Long getDaySeconds(Date date)
    {
        DateWrapper wrapper = new DateWrapper(date);
        Long seconds = 0L;
        seconds += wrapper.getHours() * HOUR_SECONDS;
        seconds += wrapper.getMinutes() * MINUTE_SECONDS;

        return seconds;
    }

    /**
     * @param date
     *            First date of comparison
     * @param date2
     *            First date of comparison
     * @return True if time of <i>date</i> is lesser than time of <i>date2</i>.
     */
    public static Boolean isTimeBefore(Date date, Date date2)
    {
        return getDaySeconds(date) <= getDaySeconds(date2);
    }

    /**
     * @param hours
     *            Number of hours to set (max 24).
     * @param minutes
     *            Number of minutes to set (max 60).
     * @return Today date with hours and time given in parameters.
     */
    public static Date getTodayWithTime(int hours, int minutes)
    {
        return getTodayWithTime(hours, minutes, 0);
    }

    /**
     * @param hours
     *            Number of hours to set (max 24).
     * @param minutes
     *            Number of minutes to set (max 60).
     * @param seconds
     *            Number of seconds to set (max 60).
     * @return Today date with hours and time given in parameters.
     */
    public static Date getTodayWithTime(int hours, int minutes, int seconds)
    {
        return new DateWrapper().clearTime().addHours(hours).addMinutes(minutes).addSeconds(seconds).asDate();
    }

    /**
     * @param dayInWeek
     *            The day of which string representation is requested.
     * @return For a day number returns its name in the week (ex: 1 returns monday, 2 returns tuesday...).
     */
    public static String getDayFromDayInWeek(int dayInWeek)
    {
        switch (dayInWeek)
        {
            case 0:
                return "sunday";
            case 1:
                return "monday";
            case 2:
                return "tuesday";
            case 3:
                return "wednesday";
            case 4:
                return "thursday";
            case 5:
                return "friday";
            case 6:
                return "saturday";
            default:
                ;
        }
        return null;
    }

    /**
     * @param month
     *            The month of which string representation is requested.
     * @return For a month number returns its name in the year (ex: 1 returns january, 2 returns february...).
     */
    public static String getMonthString(int month)
    {
        switch (month)
        {
            case 0:
                return "January";
            case 1:
                return "February";
            case 2:
                return "March";
            case 3:
                return "April";
            case 4:
                return "May";
            case 5:
                return "June";
            case 6:
                return "July";
            case 7:
                return "August";
            case 8:
                return "September";
            case 9:
                return "October";
            case 10:
                return "November";
            case 11:
                return "December";
            default:
                ;
        }
        return null;
    }

    /**
     * @param startDate
     *            Start date of period to analyze.
     * @param endDate
     *            End date of period to analyze.
     * @return The number of day corresponding to saturday or sunday between start date and end date.
     */
    public static Integer getNbWeekEndDayBetween(DateWrapper startDate, DateWrapper endDate)
    {
        DateWrapper currentDate = startDate;
        int weekEndCounter = 0;
        while (currentDate.before(endDate))
        {
            if (isWeekend(currentDate))
                weekEndCounter++;
            currentDate = currentDate.addDays(1);
        }
        return weekEndCounter;
    }

    /**
     * @param date
     *            The date to change.
     * @return A new date that is first next monday after <i>date</i>, if <i>date</i> corresponds to a week-end day.
     */
    public static DateWrapper skipWeekend(DateWrapper date)
    {
        DateWrapper newDate = date;
        if (isSaturday(newDate))
            newDate = newDate.addDays(2);
        if (isSunday(newDate))
            newDate = newDate.addDays(1);

        return newDate;
    }

    /**
     * @param startDate
     *            Start date of period to analyze.
     * @param endDate
     *            End date of period to analyze.
     * @return The number of day different from saturday and sunday between start date and end date.
     */
    public static Integer getNbOpenDayBetween(DateWrapper startDate, DateWrapper endDate)
    {
        DateWrapper currentDate = startDate;
        int counter = 0;
        while (currentDate.before(endDate))
        {
            if (!isWeekend(currentDate))
                counter++;
            currentDate = currentDate.addDays(1);
        }
        return counter;
    }

    /**
     * @param day
     *            The day to test.
     * @return True if day corresponds to saturday or sunday.
     */
    public static boolean isWeekend(DateWrapper day)
    {
        int nbDay = day.getDayInWeek();
        boolean isWeekend = false;
        if (nbDay == 6 || nbDay == 0)
            isWeekend = true;
        return isWeekend;
    }

    /**
     * @param day
     *            The day to test.
     * @return True if day corresponds to monday.
     */
    public static boolean isMonday(DateWrapper day)
    {
        int nbDay = day.getDayInWeek();
        return nbDay == 1;
    }

    /**
     * @param day
     *            The day to test.
     * @return True if day corresponds to tuesday.
     */
    public static boolean isTuesday(DateWrapper day)
    {
        int nbDay = day.getDayInWeek();
        return nbDay == 2;
    }

    /**
     * @param day
     *            The day to test.
     * @return True if day corresponds to wednesday.
     */
    public static boolean isWednesday(DateWrapper day)
    {
        int nbDay = day.getDayInWeek();
        return nbDay == 3;
    }

    /**
     * @param day
     *            The day to test.
     * @return True if day corresponds to thursday.
     */
    public static boolean isThursday(DateWrapper day)
    {
        int nbDay = day.getDayInWeek();
        return nbDay == 4;
    }

    /**
     * @param day
     *            The day to test.
     * @return True if day corresponds to friday.
     */
    public static boolean isFriday(DateWrapper day)
    {
        int nbDay = day.getDayInWeek();
        return nbDay == 5;
    }

    /**
     * @param day
     *            The day to test.
     * @return True if day corresponds to saturday.
     */
    public static boolean isSaturday(DateWrapper day)
    {
        int nbDay = day.getDayInWeek();
        return nbDay == 6;
    }

    /**
     * @param day
     *            The day to test.
     * @return True if day corresponds to sunday.
     */
    public static boolean isSunday(DateWrapper day)
    {
        int nbDay = day.getDayInWeek();
        return nbDay == 0;
    }

    private static IDateWriter dateWriter;

    public static IDateWriter getDateWriter()
    {
        if (dateWriter == null)
            dateWriter = new ClientDateWriter();
        return dateWriter;
    }

    public static void setDateWriter(IDateWriter writerMock)
    {
        dateWriter = writerMock;
    }

    public static String formatDate(String dateTimeString, Date date)
    {
        return getDateWriter().write(dateTimeString, date);
    }
}
