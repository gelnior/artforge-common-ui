package fr.hd3d.common.ui.client.calendar;

import com.google.gwt.i18n.client.DateTimeFormat;


/**
 * DateFormat stores all classic date format needed.
 * 
 * @author HD3D
 */
public class DateFormat
{
    /** Display format for date value : "dd-MM-yyyy". */
    public static final DateTimeFormat DATE = DateTimeFormat.getFormat(fr.hd3d.common.client.DateFormat.DATE_STRING);

    /** Display format for date value : "dd/MM/yyyy". */
    public static final DateTimeFormat FRENCH_DATE = DateTimeFormat
            .getFormat(fr.hd3d.common.client.DateFormat.FRENCH_DATE_STRING);

    /** Display format for date-time value : "yyyy-MM-dd HH:mm:ss". */
    public static final DateTimeFormat DATE_TIME = DateTimeFormat
            .getFormat(fr.hd3d.common.client.DateFormat.DATE_TIME_STRING);

    /** Display format for date-time value : "dd/MM/yyyy HH:mm:ss". */
    public static final DateTimeFormat FRENCH_DATE_TIME = DateTimeFormat
            .getFormat(fr.hd3d.common.client.DateFormat.FRENCH_DATE_TIME_STRING);

    /** Display format for date value : "yyyy/MM/dd". */
    public static final DateTimeFormat REVERSED_FRENCH_DATE = DateTimeFormat
            .getFormat(fr.hd3d.common.client.DateFormat.REVERSED_FRENCH_STRING);

    /** Display format for date value : "yyyy-MM-dd (EEE)". */
    public static final DateTimeFormat DATE_AND_DAY = DateTimeFormat
            .getFormat(fr.hd3d.common.client.DateFormat.DATE_AND_DAY_STRING);

    /** Display format for time value : "HH:mm". */
    public static final DateTimeFormat TIME = DateTimeFormat.getFormat(fr.hd3d.common.client.DateFormat.TIME_STRING);

    /** Display format for date value : "yyyy-MM-dd". */
    public static final DateTimeFormat DATESTAMP = DateTimeFormat
            .getFormat(fr.hd3d.common.client.DateFormat.DATESTAMP_STRING);

    /** Display format for time stamp value : "yyyy-MM-dd HH:mm:ss.S". */
    public static final DateTimeFormat TIMESTAMP_FORMAT = DateTimeFormat
            .getFormat(fr.hd3d.common.client.DateFormat.TIMESTAMP_FORMAT_STRING);
}
