package fr.hd3d.common.ui.client.calendar;

import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;


/**
 * For a given number MonthTranslation returns the full name of a month translated to the current locale.
 * 
 * @author HD3D
 */
public class MonthTranslation
{
    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants CONSTANTS = GWT.create(CommonConstants.class);

    /**
     * @param month
     *            The month number.
     * @return Month string corresponding to month number given in parameter (ex: 3 return "march").
     */
    public String getMonth(int month)
    {
        switch (month)
        {
            // case 1:
            // return CONSTANTS.January();
            // case 2:
            // return CONSTANTS.February();
            // case 3:
            // return CONSTANTS.March();
            // case 4:
            // return CONSTANTS.April();
            // case 5:
            // return CONSTANTS.May();
            // case 6:
            // return CONSTANTS.June();
            // case 7:
            // return CONSTANTS.July();
            // case 8:
            // return CONSTANTS.August();
            // case 9:
            // return CONSTANTS.September();
            // case 10:
            // return CONSTANTS.October();
            // case 11:
            // return CONSTANTS.November();
            // case 12:
            // return CONSTANTS.December();
            default:
                return "";
        }
    }
}
