package fr.hd3d.common.ui.client.cookie;

import java.util.Date;
import java.util.Map;

import com.extjs.gxt.ui.client.core.FastMap;
import com.google.gwt.user.client.Cookies;

import fr.hd3d.common.ui.client.config.CommonConfig;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;


/**
 * FavoriteCookie is made for easy favorite cookie manipulation. It lets you add or get parameter value in the HD3D
 * favorite cookie.<br>
 * <br>
 * FavoriteCookie behaves like a map.
 * 
 * @author HD3D
 */
public class FavoriteCookie
{

    /**
     * Set favorite cookie value.
     * 
     * @param value
     *            Value to set.
     */
    public static void setFavCookie(String value)
    {
        Date expireDate = new Date();
        expireDate = new Date(expireDate.getTime() + 2 * 24 * 60 * 60 * 1000);
        Cookies.setCookie(CommonConfig.FAV_COOKIE_NAME, value, expireDate, null, "/", false);
    }

    /**
     * Set favorite cookie value with value contained in the map given in parameter. Data appears in this way :
     * "key01:value01,key02:value02...key0n:value0n".
     * 
     * @param map
     *            Map containing value to set in the cookie.
     */
    public static void setFavCookie(Map<String, String> map)
    {
        StringBuilder cookieValue = new StringBuilder();
        for (Map.Entry<String, String> entry : map.entrySet())
        {
            if (cookieValue.length() > 0)
            {
                cookieValue.append(",");
            }

            cookieValue.append(entry.getKey());
            cookieValue.append(":");
            cookieValue.append(entry.getValue());
        }

        setFavCookie(cookieValue.toString());
    }

    /**
     * Get favorite cookie value as String.
     */
    public static String getFavCookie()
    {
        return Cookies.getCookie(CommonConfig.FAV_COOKIE_NAME);
    }

    /**
     * Get favorite cookie value as map.
     */
    public static Map<String, String> getFavCookieAsMap()
    {
        Map<String, String> map = new FastMap<String>();
        String cookieValue = Cookies.getCookie(CommonConfig.FAV_COOKIE_NAME);

        if (cookieValue != null)
        {
            String[] keysAndValues = cookieValue.split(",");
            for (String keyAndValue : keysAndValues)
            {
                String[] entry = keyAndValue.split(":");
                if (entry.length == 2)
                    map.put(entry[0], entry[1]);
            }
        }

        return map;
    }

    /**
     * @param key
     *            The key of the parameter.
     * @return The parameter value corresponding to the key given in parameter.
     */
    public static String getFavParameterValue(String key)
    {
        Map<String, String> map = getFavCookieAsMap();

        return map.get(key);
    }

    /**
     * Set the parameter value corresponding to the key given in parameter.
     * 
     * @param key
     *            the key of the parameter to set.
     */
    public static void putFavValue(String key, String value)
    {
        Map<String, String> map = getFavCookieAsMap();

        map.put(key, value);

        setFavCookie(map);
    }

    /**
     * Remove from the cookie the parameter with <i>key</i> as key.
     * 
     * @param key
     *            The parameter key.
     */
    public static void removeValue(String key)
    {
        Map<String, String> map = getFavCookieAsMap();
        map.remove(key);

        setFavCookie(map);
    }

    /**
     * Remove all values from favorite cookie.
     */
    public static void reset()
    {
        setFavCookie("");
    }

    /**
     * @return Id of current project set inside favorite cookie.
     */
    public static Long getCurrentProjectId()
    {
        String projectId = getFavParameterValue(CommonConfig.COOKIE_VAR_PROJECT);
        if (projectId != null)
            return Long.valueOf(projectId);
        else
            return null;
    }

    /**
     * Set project as current project inside favorite cookie.
     * 
     * @param project
     *            The project to set.
     */
    public static void setCurrentProject(ProjectModelData project)
    {
        if (project != null && project.getId() != null)
            putFavValue(CommonConfig.COOKIE_VAR_PROJECT, project.getId().toString());
        else
            putFavValue(CommonConfig.COOKIE_VAR_PROJECT, "0");
    }

}
