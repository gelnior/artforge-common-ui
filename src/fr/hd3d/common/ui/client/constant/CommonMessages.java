package fr.hd3d.common.ui.client.constant;

/**
 * Interface to represent the constants contained in resource bundle:
 * '/home/frank.rousseau/workspace/Hd3dUILibrary/src/fr/hd3d/common/ui/client/constant/CommonMessages.properties'.
 */
public interface CommonMessages extends com.google.gwt.i18n.client.Messages
{

    /**
     * Translated "InventoryMessages".
     * 
     * @return translated "InventoryMessages"
     */
    @DefaultMessage("CommonMessages")
    @Key("ClassName")
    String ClassName();

    /**
     * Translated
     * "This proxy type is not supported by your browser. You can donwload this proxy <a href=\"{0}\">here</a>".
     * 
     * @return translated
     *         "This proxy type is not supported by your browser. You can donwload this proxy <a href=\"{0}\">here</a>"
     */
    @DefaultMessage("This proxy type is not supported by your browser. You can donwload this proxy <a href=\"{0}\">here</a>")
    @Key("downloadProxy")
    String downloadProxy(String src);

    /**
     * Translated
     * "This proxy type is not supported for annotation by your browser. You can donwload this proxy <a href=\"{0}\">here</a>".
     * 
     * @return translated
     *         "This proxy type is not supported for annotation by your browser. You can donwload this proxy <a href=\"{0}\">here</a>"
     */
    @DefaultMessage("This proxy type is not supported for annotation by your browser. You can donwload this proxy <a href=\"{0}\">here</a>")
    @Key("downloadProxyAnnotation")
    String downloadProxyAnnotation(String src);

    /**
     * Translated "Script finished successfully".
     * 
     * @return translated "Script finished successfully"
     */
    @DefaultMessage("Script finished successfully")
    @Key("scriptFinished")
    String scriptFinished();

    /**
     * Translated "These users already exist : <br/>
     * {0}".
     * 
     * @return translated "These users already exist : <br/>
     *         {0}"
     */
    @DefaultMessage("These users already exist : <br/>{0}")
    @Key("userAlreadyExist")
    String userAlreadyExist(String message);

}
