package fr.hd3d.common.ui.client.constant;

/**
 * Interface to represent the constants contained in resource bundle:
 * 	'/home/frank.rousseau/workspace/Hd3dUILibrary/src/fr/hd3d/common/ui/client/constant/CommonConstants.properties'.
 */
public interface CommonConstants extends com.google.gwt.i18n.client.Constants {
  
  /**
   * Translated "Activities".
   * 
   * @return translated "Activities"
   */
  @DefaultStringValue("Activities")
  @Key("Activities")
  String Activities();

  /**
   * Translated "Add".
   * 
   * @return translated "Add"
   */
  @DefaultStringValue("Add")
  @Key("Add")
  String Add();

  /**
   * Translated "Add filter".
   * 
   * @return translated "Add filter"
   */
  @DefaultStringValue("Add filter")
  @Key("AddFilter")
  String AddFilter();

  /**
   * Translated "Add milestone".
   * 
   * @return translated "Add milestone"
   */
  @DefaultStringValue("Add milestone")
  @Key("AddMileStone")
  String AddMileStone();

  /**
   * Translated "Set relations on selected rows".
   * 
   * @return translated "Set relations on selected rows"
   */
  @DefaultStringValue("Set relations on selected rows")
  @Key("AddRelation")
  String AddRelation();

  /**
   * Translated "Add row to the list".
   * 
   * @return translated "Add row to the list"
   */
  @DefaultStringValue("Add row to the list")
  @Key("AddRow")
  String AddRow();

  /**
   * Translated "Additional informations...".
   * 
   * @return translated "Additional informations..."
   */
  @DefaultStringValue("Additional informations...")
  @Key("AdditionalInformations")
  String AdditionalInformations();

  /**
   * Translated "Alignment".
   * 
   * @return translated "Alignment"
   */
  @DefaultStringValue("Alignment")
  @Key("Alignment")
  String Alignment();

  /**
   * Translated "All".
   * 
   * @return translated "All"
   */
  @DefaultStringValue("All")
  @Key("All")
  String All();

  /**
   * Translated "Apply filter".
   * 
   * @return translated "Apply filter"
   */
  @DefaultStringValue("Apply filter")
  @Key("ApplyFilter")
  String ApplyFilter();

  /**
   * Translated "Approval notes".
   * 
   * @return translated "Approval notes"
   */
  @DefaultStringValue("Approval notes")
  @Key("ApprovalNotes")
  String ApprovalNotes();

  /**
   * Translated "Asset".
   * 
   * @return translated "Asset"
   */
  @DefaultStringValue("Asset")
  @Key("Asset")
  String Asset();

  /**
   * Translated "Auto save".
   * 
   * @return translated "Auto save"
   */
  @DefaultStringValue("Auto save")
  @Key("AutoSave")
  String AutoSave();

  /**
   * Translated "Available properties".
   * 
   * @return translated "Available properties"
   */
  @DefaultStringValue("Available properties")
  @Key("AvailableProperties")
  String AvailableProperties();

  /**
   * Translated "Available relations".
   * 
   * @return translated "Available relations"
   */
  @DefaultStringValue("Available relations")
  @Key("AvailableRelations")
  String AvailableRelations();

  /**
   * Translated "CSV Export".
   * 
   * @return translated "CSV Export"
   */
  @DefaultStringValue("CSV Export")
  @Key("CSVExport")
  String CSVExport();

  /**
   * Translated "Cancel".
   * 
   * @return translated "Cancel"
   */
  @DefaultStringValue("Cancel")
  @Key("Cancel")
  String Cancel();

  /**
   * Translated "Cancelled".
   * 
   * @return translated "Cancelled"
   */
  @DefaultStringValue("Cancelled")
  @Key("Cancelled")
  String Cancelled();

  /**
   * Translated "Casting".
   * 
   * @return translated "Casting"
   */
  @DefaultStringValue("Casting")
  @Key("Casting")
  String Casting();

  /**
   * Translated "Center".
   * 
   * @return translated "Center"
   */
  @DefaultStringValue("Center")
  @Key("Center")
  String Center();

  /**
   * Translated "CommonConstants".
   * 
   * @return translated "CommonConstants"
   */
  @DefaultStringValue("CommonConstants")
  @Key("ClassName")
  String ClassName();

  /**
   * Translated "Clear".
   * 
   * @return translated "Clear"
   */
  @DefaultStringValue("Clear")
  @Key("Clear")
  String Clear();

  /**
   * Translated "Close".
   * 
   * @return translated "Close"
   */
  @DefaultStringValue("Close")
  @Key("Close")
  String Close();

  /**
   * Translated "Closed".
   * 
   * @return translated "Closed"
   */
  @DefaultStringValue("Closed")
  @Key("Closed")
  String Closed();

  /**
   * Translated "Comment".
   * 
   * @return translated "Comment"
   */
  @DefaultStringValue("Comment")
  @Key("Comment")
  String Comment();

  /**
   * Translated "Computers".
   * 
   * @return translated "Computers"
   */
  @DefaultStringValue("Computers")
  @Key("Computers")
  String Computers();

  /**
   * Translated "Confirm".
   * 
   * @return translated "Confirm"
   */
  @DefaultStringValue("Confirm")
  @Key("Confirm")
  String Confirm();

  /**
   * Translated "Do you really want to delete current view ?".
   * 
   * @return translated "Do you really want to delete current view ?"
   */
  @DefaultStringValue("Do you really want to delete current view ?")
  @Key("ConfirmDeleteSheet")
  String ConfirmDeleteSheet();

  /**
   * Translated "Constituents".
   * 
   * @return translated "Constituents"
   */
  @DefaultStringValue("Constituents")
  @Key("Constituents")
  String Constituents();

  /**
   * Translated "Contracts".
   * 
   * @return translated "Contracts"
   */
  @DefaultStringValue("Contracts")
  @Key("Contracts")
  String Contracts();

  /**
   * Translated "Cores".
   * 
   * @return translated "Cores"
   */
  @DefaultStringValue("Cores")
  @Key("Cores")
  String Cores();

  /**
   * Translated "Create new attribute".
   * 
   * @return translated "Create new attribute"
   */
  @DefaultStringValue("Create new attribute")
  @Key("CreateNewAttribute")
  String CreateNewAttribute();

  /**
   * Translated "Create new group".
   * 
   * @return translated "Create new group"
   */
  @DefaultStringValue("Create new group")
  @Key("CreateNewGroup")
  String CreateNewGroup();

  /**
   * Translated "Create a new view".
   * 
   * @return translated "Create a new view"
   */
  @DefaultStringValue("Create a new view")
  @Key("CreateSheet")
  String CreateSheet();

  /**
   * Translated "Creator".
   * 
   * @return translated "Creator"
   */
  @DefaultStringValue("Creator")
  @Key("Creator")
  String Creator();

  /**
   * Translated "Date".
   * 
   * @return translated "Date"
   */
  @DefaultStringValue("Date")
  @Key("Date")
  String Date();

  /**
   * Translated "Date Format".
   * 
   * @return translated "Date Format"
   */
  @DefaultStringValue("Date Format")
  @Key("DateFormat")
  String DateFormat();

  /**
   * Translated "Delete".
   * 
   * @return translated "Delete"
   */
  @DefaultStringValue("Delete")
  @Key("Delete")
  String Delete();

  /**
   * Translated "Delete current view".
   * 
   * @return translated "Delete current view"
   */
  @DefaultStringValue("Delete current view")
  @Key("DeleteCurrentSheet")
  String DeleteCurrentSheet();

  /**
   * Translated "Delete group".
   * 
   * @return translated "Delete group"
   */
  @DefaultStringValue("Delete group")
  @Key("DeleteGroup")
  String DeleteGroup();

  /**
   * Translated "Delete milestone".
   * 
   * @return translated "Delete milestone"
   */
  @DefaultStringValue("Delete milestone")
  @Key("DeleteMileStone")
  String DeleteMileStone();

  /**
   * Translated "Delete selected rows".
   * 
   * @return translated "Delete selected rows"
   */
  @DefaultStringValue("Delete selected rows")
  @Key("DeleteRow")
  String DeleteRow();

  /**
   * Translated "Delete selected elements".
   * 
   * @return translated "Delete selected elements"
   */
  @DefaultStringValue("Delete selected elements")
  @Key("DeleteSelectedElements")
  String DeleteSelectedElements();

  /**
   * Translated "Description".
   * 
   * @return translated "Description"
   */
  @DefaultStringValue("Description")
  @Key("Description")
  String Description();

  /**
   * Translated "Devices".
   * 
   * @return translated "Devices"
   */
  @DefaultStringValue("Devices")
  @Key("Devices")
  String Devices();

  /**
   * Translated "Dispatcher allowed".
   * 
   * @return translated "Dispatcher allowed"
   */
  @DefaultStringValue("Dispatcher allowed")
  @Key("DispatcherAllowed")
  String DispatcherAllowed();

  /**
   * Translated "Display".
   * 
   * @return translated "Display"
   */
  @DefaultStringValue("Display")
  @Key("Display")
  String Display();

  /**
   * Translated "DNS Name".
   * 
   * @return translated "DNS Name"
   */
  @DefaultStringValue("DNS Name")
  @Key("DnsName")
  String DnsName();

  /**
   * Translated "Done".
   * 
   * @return translated "Done"
   */
  @DefaultStringValue("Done")
  @Key("Done")
  String Done();

  /**
   * Translated "Duration".
   * 
   * @return translated "Duration"
   */
  @DefaultStringValue("Duration")
  @Key("Duration")
  String Duration();

  /**
   * Translated "Select or enter name".
   * 
   * @return translated "Select or enter name"
   */
  @DefaultStringValue("Select or enter name")
  @Key("DynTypeNameEmptyText")
  String DynTypeNameEmptyText();

  /**
   * Translated "Value(s) type".
   * 
   * @return translated "Value(s) type"
   */
  @DefaultStringValue("Value(s) type")
  @Key("DynTypeValuesType")
  String DynTypeValuesType();

  /**
   * Translated "Edit approval note types".
   * 
   * @return translated "Edit approval note types"
   */
  @DefaultStringValue("Edit approval note types")
  @Key("EditApprovalNoteType")
  String EditApprovalNoteType();

  /**
   * Translated "Edit current view".
   * 
   * @return translated "Edit current view"
   */
  @DefaultStringValue("Edit current view")
  @Key("EditCurrentSheet")
  String EditCurrentSheet();

  /**
   * Translated "Edit group".
   * 
   * @return translated "Edit group"
   */
  @DefaultStringValue("Edit group")
  @Key("EditGroup")
  String EditGroup();

  /**
   * Translated "Edit milestone".
   * 
   * @return translated "Edit milestone"
   */
  @DefaultStringValue("Edit milestone")
  @Key("EditMileStone")
  String EditMileStone();

  /**
   * Translated "Edition".
   * 
   * @return translated "Edition"
   */
  @DefaultStringValue("Edition")
  @Key("Edition")
  String Edition();

  /**
   * Translated "Editor".
   * 
   * @return translated "Editor"
   */
  @DefaultStringValue("Editor")
  @Key("Editor")
  String Editor();

  /**
   * Translated "End date".
   * 
   * @return translated "End date"
   */
  @DefaultStringValue("End date")
  @Key("EndDate")
  String EndDate();

  /**
   * Translated "Anglais".
   * 
   * @return translated "Anglais"
   */
  @DefaultStringValue("Anglais")
  @Key("English")
  String English();

  /**
   * Translated "Entity ID".
   * 
   * @return translated "Entity ID"
   */
  @DefaultStringValue("Entity ID")
  @Key("EntityId")
  String EntityId();

  /**
   * Translated "Entity Name".
   * 
   * @return translated "Entity Name"
   */
  @DefaultStringValue("Entity Name")
  @Key("EntityName")
  String EntityName();

  /**
   * Translated "Error".
   * 
   * @return translated "Error"
   */
  @DefaultStringValue("Error")
  @Key("Error")
  String Error();

  /**
   * Translated "Export to CSV".
   * 
   * @return translated "Export to CSV"
   */
  @DefaultStringValue("Export to CSV")
  @Key("ExportToCsv")
  String ExportToCsv();

  /**
   * Translated "Export to ODS".
   * 
   * @return translated "Export to ODS"
   */
  @DefaultStringValue("Export to ODS")
  @Key("ExportToOds")
  String ExportToOds();

  /**
   * Translated "File Name".
   * 
   * @return translated "File Name"
   */
  @DefaultStringValue("File Name")
  @Key("FileName")
  String FileName();

  /**
   * Translated "File Revision".
   * 
   * @return translated "File Revision"
   */
  @DefaultStringValue("File Revision")
  @Key("FileRevision")
  String FileRevision();

  /**
   * Translated "File Revisions".
   * 
   * @return translated "File Revisions"
   */
  @DefaultStringValue("File Revisions")
  @Key("FileRevisions")
  String FileRevisions();

  /**
   * Translated "First name".
   * 
   * @return translated "First name"
   */
  @DefaultStringValue("First name")
  @Key("FirstName")
  String FirstName();

  /**
   * Translated "French".
   * 
   * @return translated "French"
   */
  @DefaultStringValue("French")
  @Key("French")
  String French();

  /**
   * Translated "Frequency".
   * 
   * @return translated "Frequency"
   */
  @DefaultStringValue("Frequency")
  @Key("Frequency")
  String Frequency();

  /**
   * Translated "Group".
   * 
   * @return translated "Group"
   */
  @DefaultStringValue("Group")
  @Key("Group")
  String Group();

  /**
   * Translated "Nom du groupe :".
   * 
   * @return translated "Nom du groupe :"
   */
  @DefaultStringValue("Nom du groupe :")
  @Key("GroupName")
  String GroupName();

  /**
   * Translated "Groups".
   * 
   * @return translated "Groups"
   */
  @DefaultStringValue("Groups")
  @Key("Groups")
  String Groups();

  /**
   * Translated "Hide".
   * 
   * @return translated "Hide"
   */
  @DefaultStringValue("Hide")
  @Key("Hide")
  String Hide();

  /**
   * Translated "History".
   * 
   * @return translated "History"
   */
  @DefaultStringValue("History")
  @Key("History")
  String History();

  /**
   * Translated "Infos".
   * 
   * @return translated "Infos"
   */
  @DefaultStringValue("Infos")
  @Key("Information")
  String Information();

  /**
   * Translated "Inventory ID".
   * 
   * @return translated "Inventory ID"
   */
  @DefaultStringValue("Inventory ID")
  @Key("InventoryId")
  String InventoryId();

  /**
   * Translated "Inventory Status".
   * 
   * @return translated "Inventory Status"
   */
  @DefaultStringValue("Inventory Status")
  @Key("InventoryStatus")
  String InventoryStatus();

  /**
   * Translated "I.P. Address".
   * 
   * @return translated "I.P. Address"
   */
  @DefaultStringValue("I.P. Address")
  @Key("IpAdress")
  String IpAdress();

  /**
   * Translated "Key".
   * 
   * @return translated "Key"
   */
  @DefaultStringValue("Key")
  @Key("Key")
  String Key();

  /**
   * Translated "Set last filter applied".
   * 
   * @return translated "Set last filter applied"
   */
  @DefaultStringValue("Set last filter applied")
  @Key("LastFilterApplied")
  String LastFilterApplied();

  /**
   * Translated "Last name".
   * 
   * @return translated "Last name"
   */
  @DefaultStringValue("Last name")
  @Key("LastName")
  String LastName();

  /**
   * Translated "Last User".
   * 
   * @return translated "Last User"
   */
  @DefaultStringValue("Last User")
  @Key("LastUser")
  String LastUser();

  /**
   * Translated "Left".
   * 
   * @return translated "Left"
   */
  @DefaultStringValue("Left")
  @Key("Left")
  String Left();

  /**
   * Translated "Library".
   * 
   * @return translated "Library"
   */
  @DefaultStringValue("Library")
  @Key("Library")
  String Library();

  /**
   * Translated "Licenses".
   * 
   * @return translated "Licenses"
   */
  @DefaultStringValue("Licenses")
  @Key("Licenses")
  String Licenses();

  /**
   * Translated "List Name".
   * 
   * @return translated "List Name"
   */
  @DefaultStringValue("List Name")
  @Key("ListName")
  String ListName();

  /**
   * Translated "Loading...".
   * 
   * @return translated "Loading..."
   */
  @DefaultStringValue("Loading...")
  @Key("Loading")
  String Loading();

  /**
   * Translated "Login".
   * 
   * @return translated "Login"
   */
  @DefaultStringValue("Login")
  @Key("Login")
  String Login();

  /**
   * Translated "MAC Address".
   * 
   * @return translated "MAC Address"
   */
  @DefaultStringValue("MAC Address")
  @Key("MacAddress")
  String MacAddress();

  /**
   * Translated "Meeting".
   * 
   * @return translated "Meeting"
   */
  @DefaultStringValue("Meeting")
  @Key("Meeting")
  String Meeting();

  /**
   * Translated "Model".
   * 
   * @return translated "Model"
   */
  @DefaultStringValue("Model")
  @Key("Model")
  String Model();

  /**
   * Translated "Modification".
   * 
   * @return translated "Modification"
   */
  @DefaultStringValue("Modification")
  @Key("Modification")
  String Modification();

  /**
   * Translated "Name".
   * 
   * @return translated "Name"
   */
  @DefaultStringValue("Name")
  @Key("Name")
  String Name();

  /**
   * Translated "New".
   * 
   * @return translated "New"
   */
  @DefaultStringValue("New")
  @Key("New")
  String New();

  /**
   * Translated "New Item".
   * 
   * @return translated "New Item"
   */
  @DefaultStringValue("New Item")
  @Key("NewItem")
  String NewItem();

  /**
   * Translated "Number".
   * 
   * @return translated "Number"
   */
  @DefaultStringValue("Number")
  @Key("Number")
  String Number();

  /**
   * Translated "OK".
   * 
   * @return translated "OK"
   */
  @DefaultStringValue("OK")
  @Key("Ok")
  String Ok();

  /**
   * Translated "Operation".
   * 
   * @return translated "Operation"
   */
  @DefaultStringValue("Operation")
  @Key("Operation")
  String Operation();

  /**
   * Translated "Other".
   * 
   * @return translated "Other"
   */
  @DefaultStringValue("Other")
  @Key("Other")
  String Other();

  /**
   * Translated "Persons".
   * 
   * @return translated "Persons"
   */
  @DefaultStringValue("Persons")
  @Key("Persons")
  String Persons();

  /**
   * Translated "Planning".
   * 
   * @return translated "Planning"
   */
  @DefaultStringValue("Planning")
  @Key("Planning")
  String Planning();

  /**
   * Translated "Playlist".
   * 
   * @return translated "Playlist"
   */
  @DefaultStringValue("Playlist")
  @Key("Playlist")
  String Playlist();

  /**
   * Translated "Playlists".
   * 
   * @return translated "Playlists"
   */
  @DefaultStringValue("Playlists")
  @Key("Playlists")
  String Playlists();

  /**
   * Translated "Pools".
   * 
   * @return translated "Pools"
   */
  @DefaultStringValue("Pools")
  @Key("Pools")
  String Pools();

  /**
   * Translated "Preview".
   * 
   * @return translated "Preview"
   */
  @DefaultStringValue("Preview")
  @Key("Preview")
  String Preview();

  /**
   * Translated "Print".
   * 
   * @return translated "Print"
   */
  @DefaultStringValue("Print")
  @Key("Print")
  String Print();

  /**
   * Translated "Processors".
   * 
   * @return translated "Processors"
   */
  @DefaultStringValue("Processors")
  @Key("Processors")
  String Processors();

  /**
   * Translated "Project".
   * 
   * @return translated "Project"
   */
  @DefaultStringValue("Project")
  @Key("Project")
  String Project();

  /**
   * Translated "Project Type".
   * 
   * @return translated "Project Type"
   */
  @DefaultStringValue("Project Type")
  @Key("ProjectType")
  String ProjectType();

  /**
   * Translated "Projects".
   * 
   * @return translated "Projects"
   */
  @DefaultStringValue("Projects")
  @Key("Projects")
  String Projects();

  /**
   * Translated "Purchase date".
   * 
   * @return translated "Purchase date"
   */
  @DefaultStringValue("Purchase date")
  @Key("PurchaseDate")
  String PurchaseDate();

  /**
   * Translated "Qualification".
   * 
   * @return translated "Qualification"
   */
  @DefaultStringValue("Qualification")
  @Key("Qualification")
  String Qualification();

  /**
   * Translated "RAM".
   * 
   * @return translated "RAM"
   */
  @DefaultStringValue("RAM")
  @Key("Ram")
  String Ram();

  /**
   * Translated "Rec In".
   * 
   * @return translated "Rec In"
   */
  @DefaultStringValue("Rec In")
  @Key("RecIn")
  String RecIn();

  /**
   * Translated "Rec Out".
   * 
   * @return translated "Rec Out"
   */
  @DefaultStringValue("Rec Out")
  @Key("RecOut")
  String RecOut();

  /**
   * Translated "Reference".
   * 
   * @return translated "Reference"
   */
  @DefaultStringValue("Reference")
  @Key("Reference")
  String Reference();

  /**
   * Translated "Refresh".
   * 
   * @return translated "Refresh"
   */
  @DefaultStringValue("Refresh")
  @Key("Refresh")
  String Refresh();

  /**
   * Translated "Relation tool".
   * 
   * @return translated "Relation tool"
   */
  @DefaultStringValue("Relation tool")
  @Key("RelationTool")
  String RelationTool();

  /**
   * Translated "Relations to set".
   * 
   * @return translated "Relations to set"
   */
  @DefaultStringValue("Relations to set")
  @Key("RelationsToSet")
  String RelationsToSet();

  /**
   * Translated "Remove all buttons".
   * 
   * @return translated "Remove all buttons"
   */
  @DefaultStringValue("Remove all buttons")
  @Key("RemoveAllButtons")
  String RemoveAllButtons();

  /**
   * Translated "Remove Filter".
   * 
   * @return translated "Remove Filter"
   */
  @DefaultStringValue("Remove Filter")
  @Key("RemoveFilter")
  String RemoveFilter();

  /**
   * Translated "Rename".
   * 
   * @return translated "Rename"
   */
  @DefaultStringValue("Rename")
  @Key("Rename")
  String Rename();

  /**
   * Translated "Render licenses".
   * 
   * @return translated "Render licenses"
   */
  @DefaultStringValue("Render licenses")
  @Key("RenderLicenses")
  String RenderLicenses();

  /**
   * Translated "Renderer".
   * 
   * @return translated "Renderer"
   */
  @DefaultStringValue("Renderer")
  @Key("Renderer")
  String Renderer();

  /**
   * Translated "Groups".
   * 
   * @return translated "Groups"
   */
  @DefaultStringValue("Groups")
  @Key("ResourceGroups")
  String ResourceGroups();

  /**
   * Translated "Resources".
   * 
   * @return translated "Resources"
   */
  @DefaultStringValue("Resources")
  @Key("Resources")
  String Resources();

  /**
   * Translated "Retake".
   * 
   * @return translated "Retake"
   */
  @DefaultStringValue("Retake")
  @Key("Retake")
  String Retake();

  /**
   * Translated "Revision".
   * 
   * @return translated "Revision"
   */
  @DefaultStringValue("Revision")
  @Key("Revision")
  String Revision();

  /**
   * Translated "Right".
   * 
   * @return translated "Right"
   */
  @DefaultStringValue("Right")
  @Key("Right")
  String Right();

  /**
   * Translated "Roles".
   * 
   * @return translated "Roles"
   */
  @DefaultStringValue("Roles")
  @Key("Roles")
  String Roles();

  /**
   * Translated "Room ".
   * 
   * @return translated "Room "
   */
  @DefaultStringValue("Room ")
  @Key("Room")
  String Room();

  /**
   * Translated "Save".
   * 
   * @return translated "Save"
   */
  @DefaultStringValue("Save")
  @Key("Save")
  String Save();

  /**
   * Translated "Save all".
   * 
   * @return translated "Save all"
   */
  @DefaultStringValue("Save all")
  @Key("SaveAll")
  String SaveAll();

  /**
   * Translated "Save as".
   * 
   * @return translated "Save as"
   */
  @DefaultStringValue("Save as")
  @Key("SaveAs")
  String SaveAs();

  /**
   * Translated "Saving...".
   * 
   * @return translated "Saving..."
   */
  @DefaultStringValue("Saving...")
  @Key("Saving")
  String Saving();

  /**
   * Translated "Error occurs while saving data. View could be corrupted.".
   * 
   * @return translated "Error occurs while saving data. View could be corrupted."
   */
  @DefaultStringValue("Error occurs while saving data. View could be corrupted.")
  @Key("SavingErrorSheet")
  String SavingErrorSheet();

  /**
   * Translated "Screens".
   * 
   * @return translated "Screens"
   */
  @DefaultStringValue("Screens")
  @Key("Screens")
  String Screens();

  /**
   * Translated "Script failed".
   * 
   * @return translated "Script failed"
   */
  @DefaultStringValue("Script failed")
  @Key("ScriptFailed")
  String ScriptFailed();

  /**
   * Translated "Script running ".
   * 
   * @return translated "Script running "
   */
  @DefaultStringValue("Script running ")
  @Key("ScriptRunning")
  String ScriptRunning();

  /**
   * Translated "Please wait while script is running ...".
   * 
   * @return translated "Please wait while script is running ..."
   */
  @DefaultStringValue("Please wait while script is running ...")
  @Key("ScriptRunningMessage")
  String ScriptRunningMessage();

  /**
   * Translated "Select an entity type".
   * 
   * @return translated "Select an entity type"
   */
  @DefaultStringValue("Select an entity type")
  @Key("SelectEntityType")
  String SelectEntityType();

  /**
   * Translated "Select project".
   * 
   * @return translated "Select project"
   */
  @DefaultStringValue("Select project")
  @Key("SelectProject")
  String SelectProject();

  /**
   * Translated "Select view".
   * 
   * @return translated "Select view"
   */
  @DefaultStringValue("Select view")
  @Key("SelectSheet")
  String SelectSheet();

  /**
   * Translated "Selected properties".
   * 
   * @return translated "Selected properties"
   */
  @DefaultStringValue("Selected properties")
  @Key("SelectedProperties")
  String SelectedProperties();

  /**
   * Translated "Sequence".
   * 
   * @return translated "Sequence"
   */
  @DefaultStringValue("Sequence")
  @Key("Sequence")
  String Sequence();

  /**
   * Translated "Sequence Casting".
   * 
   * @return translated "Sequence Casting"
   */
  @DefaultStringValue("Sequence Casting")
  @Key("SequenceCasting")
  String SequenceCasting();

  /**
   * Translated "Sequences".
   * 
   * @return translated "Sequences"
   */
  @DefaultStringValue("Sequences")
  @Key("Sequences")
  String Sequences();

  /**
   * Translated "Serial".
   * 
   * @return translated "Serial"
   */
  @DefaultStringValue("Serial")
  @Key("Serial")
  String Serial();

  /**
   * Translated "Please set a view name before saving".
   * 
   * @return translated "Please set a view name before saving"
   */
  @DefaultStringValue("Please set a view name before saving")
  @Key("SetASheetName")
  String SetASheetName();

  /**
   * Translated "View Editor".
   * 
   * @return translated "View Editor"
   */
  @DefaultStringValue("View Editor")
  @Key("SheetEditor")
  String SheetEditor();

  /**
   * Translated "View Name".
   * 
   * @return translated "View Name"
   */
  @DefaultStringValue("View Name")
  @Key("SheetName")
  String SheetName();

  /**
   * Translated "A view with the same name already exists.<br /> Please, change the view name.".
   * 
   * @return translated "A view with the same name already exists.<br /> Please, change the view name."
   */
  @DefaultStringValue("A view with the same name already exists.<br /> Please, change the view name.")
  @Key("SheetNameAlreadyExist")
  String SheetNameAlreadyExist();

  /**
   * Translated "Shot".
   * 
   * @return translated "Shot"
   */
  @DefaultStringValue("Shot")
  @Key("Shot")
  String Shot();

  /**
   * Translated "Shots".
   * 
   * @return translated "Shots"
   */
  @DefaultStringValue("Shots")
  @Key("Shots")
  String Shots();

  /**
   * Translated "Since".
   * 
   * @return translated "Since"
   */
  @DefaultStringValue("Since")
  @Key("Since")
  String Since();

  /**
   * Translated "Size".
   * 
   * @return translated "Size"
   */
  @DefaultStringValue("Size")
  @Key("Size")
  String Size();

  /**
   * Translated "Software".
   * 
   * @return translated "Software"
   */
  @DefaultStringValue("Software")
  @Key("Software")
  String Software();

  /**
   * Translated "Softwares".
   * 
   * @return translated "Softwares"
   */
  @DefaultStringValue("Softwares")
  @Key("Softwares")
  String Softwares();

  /**
   * Translated "Src In".
   * 
   * @return translated "Src In"
   */
  @DefaultStringValue("Src In")
  @Key("SrcIn")
  String SrcIn();

  /**
   * Translated "Src Out".
   * 
   * @return translated "Src Out"
   */
  @DefaultStringValue("Src Out")
  @Key("SrcOut")
  String SrcOut();

  /**
   * Translated "Stand by".
   * 
   * @return translated "Stand by"
   */
  @DefaultStringValue("Stand by")
  @Key("StandBy")
  String StandBy();

  /**
   * Translated "Start date".
   * 
   * @return translated "Start date"
   */
  @DefaultStringValue("Start date")
  @Key("StartDate")
  String StartDate();

  /**
   * Translated "Status".
   * 
   * @return translated "Status"
   */
  @DefaultStringValue("Status")
  @Key("Status")
  String Status();

  /**
   * Translated "Task".
   * 
   * @return translated "Task"
   */
  @DefaultStringValue("Task")
  @Key("Task")
  String Task();

  /**
   * Translated "Task Duration".
   * 
   * @return translated "Task Duration"
   */
  @DefaultStringValue("Task Duration")
  @Key("TaskDuration")
  String TaskDuration();

  /**
   * Translated "TaskType".
   * 
   * @return translated "TaskType"
   */
  @DefaultStringValue("TaskType")
  @Key("TaskType")
  String TaskType();

  /**
   * Translated "Tasks".
   * 
   * @return translated "Tasks"
   */
  @DefaultStringValue("Tasks")
  @Key("Tasks")
  String Tasks();

  /**
   * Translated "Technical Incident".
   * 
   * @return translated "Technical Incident"
   */
  @DefaultStringValue("Technical Incident")
  @Key("TechnicalIncident")
  String TechnicalIncident();

  /**
   * Translated "Title".
   * 
   * @return translated "Title"
   */
  @DefaultStringValue("Title")
  @Key("Title")
  String Title();

  /**
   * Translated "To do".
   * 
   * @return translated "To do"
   */
  @DefaultStringValue("To do")
  @Key("Todo")
  String Todo();

  /**
   * Translated "Type".
   * 
   * @return translated "Type"
   */
  @DefaultStringValue("Type")
  @Key("Type")
  String Type();

  /**
   * Translated "Type the record name...".
   * 
   * @return translated "Type the record name..."
   */
  @DefaultStringValue("Type the record name...")
  @Key("TypeRecordName")
  String TypeRecordName();

  /**
   * Translated "< Type your comment ... >".
   * 
   * @return translated "< Type your comment ... >"
   */
  @DefaultStringValue("< Type your comment ... >")
  @Key("TypeYourComment")
  String TypeYourComment();

  /**
   * Translated "User".
   * 
   * @return translated "User"
   */
  @DefaultStringValue("User")
  @Key("User")
  String User();

  /**
   * Translated "User licenses".
   * 
   * @return translated "User licenses"
   */
  @DefaultStringValue("User licenses")
  @Key("UserLicenses")
  String UserLicenses();

  /**
   * Translated "Validity".
   * 
   * @return translated "Validity"
   */
  @DefaultStringValue("Validity")
  @Key("Validity")
  String Validity();

  /**
   * Translated "Variation".
   * 
   * @return translated "Variation"
   */
  @DefaultStringValue("Variation")
  @Key("Variation")
  String Variation();

  /**
   * Translated "Waiting Approval".
   * 
   * @return translated "Waiting Approval"
   */
  @DefaultStringValue("Waiting Approval")
  @Key("WaitApproval")
  String WaitApproval();

  /**
   * Translated "Please wait while saving...".
   * 
   * @return translated "Please wait while saving..."
   */
  @DefaultStringValue("Please wait while saving...")
  @Key("WaitSaving")
  String WaitSaving();

  /**
   * Translated "Warranty End".
   * 
   * @return translated "Warranty End"
   */
  @DefaultStringValue("Warranty End")
  @Key("WarrantyEnd")
  String WarrantyEnd();

  /**
   * Translated "Work comments submission.".
   * 
   * @return translated "Work comments submission."
   */
  @DefaultStringValue("Work comments submission.")
  @Key("WorkCommentsSubmission")
  String WorkCommentsSubmission();

  /**
   * Translated "Work In Progress".
   * 
   * @return translated "Work In Progress"
   */
  @DefaultStringValue("Work In Progress")
  @Key("WorkInProgress")
  String WorkInProgress();

  /**
   * Translated "Work Object".
   * 
   * @return translated "Work Object"
   */
  @DefaultStringValue("Work Object")
  @Key("WorkObject")
  String WorkObject();

  /**
   * Translated "Worker".
   * 
   * @return translated "Worker"
   */
  @DefaultStringValue("Worker")
  @Key("Worker")
  String Worker();

  /**
   * Translated "Worker Status".
   * 
   * @return translated "Worker Status"
   */
  @DefaultStringValue("Worker Status")
  @Key("WorkerStatus")
  String WorkerStatus();

  /**
   * Translated "You are going to delete data. Do you confirm data suppression ?".
   * 
   * @return translated "You are going to delete data. Do you confirm data suppression ?"
   */
  @DefaultStringValue("You are going to delete data. Do you confirm data suppression ?")
  @Key("YouAreGoingToDeleteData")
  String YouAreGoingToDeleteData();

  /**
   * Translated "Add persons".
   * 
   * @return translated "Add persons"
   */
  @DefaultStringValue("Add persons")
  @Key("addPersonButtonToolTip")
  String addPersonButtonToolTip();

  /**
   * Translated "Add persons".
   * 
   * @return translated "Add persons"
   */
  @DefaultStringValue("Add persons")
  @Key("addPersons")
  String addPersons();

  /**
   * Translated "User selection dialog".
   * 
   * @return translated "User selection dialog"
   */
  @DefaultStringValue("User selection dialog")
  @Key("addUserDialogTitle")
  String addUserDialogTitle();

  /**
   * Translated "There was a problem while retrieving general configuration file. Ensure connection to server is still available.".
   * 
   * @return translated "There was a problem while retrieving general configuration file. Ensure connection to server is still available."
   */
  @DefaultStringValue("There was a problem while retrieving general configuration file. Ensure connection to server is still available.")
  @Key("configurationFileError")
  String configurationFileError();

  /**
   * Translated "Are you sure you want to delete this note ?".
   * 
   * @return translated "Are you sure you want to delete this note ?"
   */
  @DefaultStringValue("Are you sure you want to delete this note ?")
  @Key("confirmDeleteApprovalNote")
  String confirmDeleteApprovalNote();

  /**
   * Translated "The application cannot establish connection to the data server.".
   * 
   * @return translated "The application cannot establish connection to the data server."
   */
  @DefaultStringValue("The application cannot establish connection to the data server.")
  @Key("dataServerError")
  String dataServerError();

  /**
   * Translated "There was a problem while retrieving a configuration file. Ensure connection to server is still available.".
   * 
   * @return translated "There was a problem while retrieving a configuration file. Ensure connection to server is still available."
   */
  @DefaultStringValue("There was a problem while retrieving a configuration file. Ensure connection to server is still available.")
  @Key("dataTypeFileError")
  String dataTypeFileError();

  /**
   * Translated "S,M,T,W,T,F,S".
   * 
   * @return translated "S,M,T,W,T,F,S"
   */
  @DefaultStringValue("S,M,T,W,T,F,S")
  @Key("daysFirstLetterArray")
  String daysFirstLetterArray();

  /**
   * Translated "Delete person".
   * 
   * @return translated "Delete person"
   */
  @DefaultStringValue("Delete person")
  @Key("deletePersonButtonToolTip")
  String deletePersonButtonToolTip();

  /**
   * Translated "from".
   * 
   * @return translated "from"
   */
  @DefaultStringValue("from")
  @Key("from")
  String from();

  /**
   * Translated "Configuration file seems malformed. Please, correct it.".
   * 
   * @return translated "Configuration file seems malformed. Please, correct it."
   */
  @DefaultStringValue("Configuration file seems malformed. Please, correct it.")
  @Key("malformedConfigurationFileError")
  String malformedConfigurationFileError();

  /**
   * Translated "January,February,March,April,May,June,July,August,September,October,November,December".
   * 
   * @return translated "January,February,March,April,May,June,July,August,September,October,November,December"
   */
  @DefaultStringValue("January,February,March,April,May,June,July,August,September,October,November,December")
  @Key("monthNameArray")
  String monthNameArray();

  /**
   * Translated "No comment".
   * 
   * @return translated "No comment"
   */
  @DefaultStringValue("No comment")
  @Key("noComment")
  String noComment();

  /**
   * Translated "You do not have the permission to execute this request.".
   * 
   * @return translated "You do not have the permission to execute this request."
   */
  @DefaultStringValue("You do not have the permission to execute this request.")
  @Key("noPermission")
  String noPermission();

  /**
   * Translated "You are not authenticated, your request cannot be handled.".
   * 
   * @return translated "You are not authenticated, your request cannot be handled."
   */
  @DefaultStringValue("You are not authenticated, your request cannot be handled.")
  @Key("notAuthenticated")
  String notAuthenticated();

  /**
   * Translated "Person planning".
   * 
   * @return translated "Person planning"
   */
  @DefaultStringValue("Person planning")
  @Key("personPlanningHeading")
  String personPlanningHeading();

  /**
   * Translated "d".
   * 
   * @return translated "d"
   */
  @DefaultStringValue("d")
  @Key("smallDayInDate")
  String smallDayInDate();

  /**
   * Translated "Unknown".
   * 
   * @return translated "Unknown"
   */
  @DefaultStringValue("Unknown")
  @Key("unKnown")
  String unKnown();

  /**
   * Translated "User exist".
   * 
   * @return translated "User exist"
   */
  @DefaultStringValue("User exist")
  @Key("userExistMessageBoxTitle")
  String userExistMessageBoxTitle();

  /**
   * Translated "Warning".
   * 
   * @return translated "Warning"
   */
  @DefaultStringValue("Warning")
  @Key("warning")
  String warning();

  /**
   * Translated "With comment".
   * 
   * @return translated "With comment"
   */
  @DefaultStringValue("With comment")
  @Key("withComment")
  String withComment();
}
