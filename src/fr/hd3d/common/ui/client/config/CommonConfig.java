package fr.hd3d.common.ui.client.config;

/**
 * Common static string needed by library tools and HD3D applications.
 * 
 * @author HD3D
 */
public class CommonConfig
{
    /** Services configuration file path */
    public final static String CONFIG_FILE_PATH = "config/config.xml";
    /** Sheet editor configuration file path */
    public final static String CONFIG_DATA_TYPE_FILE_PATH = "config/dataType.json";

    /** Error event variable name for event dispatcher. */
    public static final String ERROR_EVENT_VAR_NAME = "error_event";
    /** Error message variable name for event dispatcher. */
    public static final String ERROR_MSG_EVENT_VAR_NAME = "error_msg";
    /** Error technical message variable name for event dispatcher. */
    public static final String ERROR_TECH_MSG_EVENT_VAR_NAME = "error-tech-msg";

    /** Class variable name for event dispatcher. */
    public static final String CLASS_EVENT_VAR_NAME = "class_event";
    /** Id variable name for event dispatcher. */
    public static final String ID_EVENT_VAR_NAME = "id_event";
    /** Model data variable name for event dispatcher. */
    public static final String MODEL_EVENT_VAR_NAME = "model";
    /** Start date variable name for event dispatcher. */
    public static final String START_DATE_EVENT_VAR_NAME = "startDate";
    /** End date variable name for event dispatcher. */
    public static final String END_DATE_EVENT_VAR_NAME = "endDate";

    /** Name of the cookie containing all favorite value (sheet, project...) */
    public static final String FAV_COOKIE_NAME = "hd3d_favorite_cookie";

    /** Project variable name for favorite cookie map. */
    public static final String COOKIE_VAR_PROJECT = "current-project";
    /** Sheet variable name for favorite cookie map. */
    public static final String COOKIE_VAR_SHEET = "-sheet";
    /** Group variable name for favorite cookie map. */
    public static final String COOKIE_VAR_GROUP = "current-group";
    /** Name for cookie variable that describes a tree path (for auto-selecting shot or constituent). */
    public static final String COOKIE_VAR_TREE_PATH = "tree-path";

    public static final String SETTING_PAGINATION_QUANTITY = "explorer-pagination-quantity";

    /** Number of page displayed by default in a paging grid. */
    public static final String PROJECT_HISTORY_VAR = "project";

    /** Default page size in paginated grid. */
    public static final int PAGINATION_DEFAULT_SIZE = 50;

}
