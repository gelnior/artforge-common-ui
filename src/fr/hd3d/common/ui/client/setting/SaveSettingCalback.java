package fr.hd3d.common.ui.client.setting;

import org.restlet.client.Request;
import org.restlet.client.Response;

import fr.hd3d.common.ui.client.modeldata.technical.SettingModelData;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;


/**
 * Callback saving setting to server.
 * 
 * @author HD3D
 */
public class SaveSettingCalback extends BaseCallback
{
    /** Setting saved by the request calling this callback. */
    protected SettingModelData setting;

    /**
     * Default constructor.
     * 
     * @param setting
     *            The saved setting.
     */
    public SaveSettingCalback(SettingModelData setting)
    {
        this.setting = setting;
    }

    /**
     * When setting is correctly saved on server, it is saved locally.
     */
    @Override
    protected void onSuccess(Request request, Response response)
    {
        UserSettings.setSetting(setting);
    }

}
