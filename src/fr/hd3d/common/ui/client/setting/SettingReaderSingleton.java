package fr.hd3d.common.ui.client.setting;

import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.modeldata.reader.SettingReader;
import fr.hd3d.common.ui.client.modeldata.technical.SettingModelData;


/**
 * Singleton that provides the reader set by user. By this reader is GXT SettingReader.
 * 
 * @author HD3D
 */
public class SettingReaderSingleton
{
    /** The reader instance. */
    private static IReader<SettingModelData> reader;

    /**
     * @return default reader instance if reader is set to null else it returns the reader.
     */
    public final static IReader<SettingModelData> getInstance()
    {
        if (reader == null)
        {
            reader = new SettingReader();
        }
        return reader;
    }

    /**
     * Set the reader.
     * 
     * @param mock
     *            The new version of the reader to set.
     */
    public final static void setInstanceAsMock(IReader<SettingModelData> mock)
    {
        reader = mock;
    }
}
