package fr.hd3d.common.ui.client.setting;

import java.util.List;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.util.Util;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.modeldata.technical.SettingModelData;


/**
 * Class handling current user setting preferences.
 * 
 * @author HD3D
 */
public class UserSettings
{
    /** Map containing all user preferences. */
    protected final static FastMap<SettingModelData> settingMap = new FastMap<SettingModelData>();

    /**
     * @param key
     *            Setting key.
     * @return The setting associated to <i>key</i>..
     */
    public static String getSetting(String key)
    {
        SettingModelData setting = settingMap.get(key);
        if (setting == null)
        {
            return null;
        }
        else
        {
            return setting.getValue();
        }
    }

    /**
     * Set a new setting or replace existing one : associate a key to its value.
     * 
     * @param key
     *            The key of the new setting.
     * @param value
     *            The value of the new setting.
     */
    public static void setSetting(final String key, final String value)
    {
        SettingModelData setting = settingMap.get(key);

        if (!Util.isEmptyString(value))
        {
            if (setting == null)
            {
                setting = new SettingModelData(key, value);
                setSetting(setting);
            }

            setting.setValue(value);
            setting.save(CommonEvents.SETTING_SAVED);
        }
        else
        {
            if (setting != null && setting.getId() != null)
                setting.delete(CommonEvents.SETTING_SAVED);
        }

    }

    /**
     * Set a new setting or replace existing one : associate a key to its value.
     * 
     * @param setting
     *            The setting to set.
     */
    public static void setSetting(SettingModelData setting)
    {
        settingMap.put(setting.getKey(), setting);
    }

    /**
     * Refresh settings with settings given in parameter.
     * 
     * @param settings
     *            Settings to put in the settings map.
     */
    public static void refreshSettings(List<SettingModelData> settings)
    {
        for (SettingModelData setting : settings)
        {
            if (setting.getKey() != null)
            {
                settingMap.put(setting.getKey(), setting);
            }
        }
    }

    /**
     * @param key
     *            Setting key.
     * @return The setting associated to <i>key</i> as a Boolean. If string value from setting cannot be converted to
     *         boolean, null is returned.
     */
    public static Boolean getSettingAsBoolean(String key)
    {
        String setting = getSetting(key);
        if (setting != null)
        {
            try
            {
                return Boolean.parseBoolean(setting);
            }
            catch (Exception e)
            {
                return null;
            }
        }
        return null;
    }

    /**
     * @param key
     *            Setting key.
     * @return The setting associated to <i>key</i> as a Integer. If string value from setting cannot be converted to
     *         Integer, null is returned.
     */
    public static Integer getSettingAsInteger(String key)
    {
        String setting = getSetting(key);
        if (setting != null)
        {
            try
            {
                return Integer.parseInt(setting);
            }
            catch (Exception e)
            {
                return null;
            }
        }
        return null;
    }
}
