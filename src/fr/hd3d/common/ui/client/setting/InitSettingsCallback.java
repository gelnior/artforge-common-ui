package fr.hd3d.common.ui.client.setting;

import java.util.List;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.data.ListLoadResult;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.modeldata.technical.SettingModelData;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;


/**
 * Callback needed by UserSettings to initialize settings locally.
 * 
 * @author HD3D
 */
public class InitSettingsCallback extends BaseCallback
{
    /**
     * When request succeeds, it load settings in the UserSettings class.
     * 
     */
    @Override
    public void onSuccess(Request request, Response response)
    {
        try
        {
            this.loadSettings(response.getEntity().getText());

            EventDispatcher.forwardEvent(CommonEvents.SETTINGS_INITIALIZED);
        }
        catch (Exception e)
        {
            GWT.log("Error occurs while parsing settings", e);
        }
    }

    /**
     * Load in UserSettings settings described in the <i>json</i> string.
     * 
     * @param json
     *            String containing settings at JSON format.
     */
    private void loadSettings(String json)
    {
        IReader<SettingModelData> reader = SettingReaderSingleton.getInstance();

        ListLoadResult<SettingModelData> result = reader.read(json);
        List<SettingModelData> settings = result.getData();

        UserSettings.refreshSettings(settings);
    }
}
