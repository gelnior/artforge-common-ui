package fr.hd3d.common.ui.client.event;

import com.extjs.gxt.ui.client.event.BaseEvent;
import com.extjs.gxt.ui.client.mvc.AppEvent;


/**
 * Event dispatched by the dispatcher to controllers.
 * 
 * @author HD3D
 */
public class DispatcherEvent extends BaseEvent
{
    /**
     * The dispatcher.
     */
    public EventDispatcher dispatcher;

    /**
     * The application event.
     */
    public AppEvent appEvent;

    /**
     * The event name.
     */
    public String name;

    /**
     * Creates a new Dispatcher event.
     * 
     * @param dispatcher
     *            The dispatcher.
     * @param appEvent
     *            The application event to dispatch.
     */
    public DispatcherEvent(EventDispatcher dispatcher, AppEvent appEvent)
    {
        super(null);
        this.dispatcher = dispatcher;
        this.appEvent = appEvent;
    }

    /**
     * Returns the application event.
     * 
     * @return the application event
     */
    public AppEvent getAppEvent()
    {
        return appEvent;
    }

    /**
     * Returns the dispatcher.
     * 
     * @return the dispatcher
     */
    public EventDispatcher getDispatcher()
    {
        return dispatcher;
    }

    /**
     * Returns the name.
     * 
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * Sets the application event.
     * 
     * @param appEvent
     *            the application event
     */
    public void setAppEvent(AppEvent appEvent)
    {
        this.appEvent = appEvent;
    }

    /**
     * Sets the dispatcher.
     * 
     * @param dispatcher
     *            the dispatcher
     */
    public void setDispatcher(EventDispatcher dispatcher)
    {
        this.dispatcher = dispatcher;
    }

    /**
     * Sets the name.
     * 
     * @param name
     *            the name
     */
    public void setName(String name)
    {
        this.name = name;
    }
}
