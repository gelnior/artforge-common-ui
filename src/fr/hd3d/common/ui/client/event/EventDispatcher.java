package fr.hd3d.common.ui.client.event;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.event.BaseObservable;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.mvc.Controller;
import com.extjs.gxt.ui.client.mvc.DispatcherListener;


/**
 * Dispatchers are responsible for dispatching application events to controllers.
 * 
 * <dl>
 * <dt><b>Events:</b></dt>
 * 
 * <dd><b>Dispatcher.BeforeDispatch</b> : DispatcherEvent(dispatcher, appEvent)<br>
 * <div>Fires before an event is dispatched. Listeners can set the <code>doit</code> field to <code>false</code> to
 * cancel the action.</div>
 * <ul>
 * <li>dispatcher : this</li>
 * <li>appEvent : the app event</li>
 * </ul>
 * </dd>
 * 
 * <dd><b>Dispatcher.AfterDispatch</b> : DispatcherEvent(dispatcher, appEvent)<br>
 * <div>Fires after an event has been dispatched.</div>
 * <ul>
 * <li>dispatcher : this</li>
 * <li>appEvent : the app event</li>
 * </ul>
 * </dd>
 * 
 * </dl>
 * 
 * @see DispatcherListener
 */
public class EventDispatcher extends BaseObservable
{
    /**
     * Fires before an event is dispatched.
     */
    public static final EventType BeforeDispatch = new EventType();

    /**
     * Fires after an event has been dispatched.
     */
    public static final EventType AfterDispatch = new EventType();

    private static EventDispatcher instance = new EventDispatcher();

    /**
     * Forwards an application event to the dispatcher.
     * 
     * @param event
     *            the application event
     */
    public static void forwardEvent(AppEvent event)
    {
        instance.dispatch(event);
    }

    /**
     * Creates and forwards an application event to the dispatcher.
     * 
     * @param eventType
     *            the application event type
     */
    public static void forwardEvent(EventType eventType)
    {
        instance.dispatch(eventType);
    }

    /**
     * Creates and forwards an application event to the dispatcher.
     * 
     * @param eventType
     *            the application event type
     * @param data
     *            the event data
     */
    public static void forwardEvent(EventType eventType, Object data)
    {
        instance.dispatch(new AppEvent(eventType, data));
    }

    /**
     * Creates and forwards an application event to the dispatcher.
     * 
     * @param eventType
     *            the application event type
     * @param data
     *            the event data
     * @param historyEvent
     *            true to mark event as a history event
     */
    public static void forwardEvent(EventType eventType, Object data, boolean historyEvent)
    {
        AppEvent ae = new AppEvent(eventType, data);
        ae.setHistoryEvent(historyEvent);
        instance.dispatch(ae);
    }

    /**
     * Returns the singleton instance.
     * 
     * @return the dispatcher
     */
    public static EventDispatcher get()
    {
        return instance;
    }

    private final List<Controller> controllers;

    private EventDispatcher()
    {
        controllers = new ArrayList<Controller>();
    }

    /**
     * Adds a controller.
     * 
     * @param controller
     *            the controller to be added
     */
    public void addController(Controller controller)
    {
        if (!controllers.contains(controller))
        {
            controllers.add(controller);
        }
    }

    /**
     * Adds a listener to receive dispatch events.
     * 
     * @param listener
     *            the listener to add
     */
    public void addDispatcherListener(DispatcherListener listener)
    {
        addListener(BeforeDispatch, listener);
        addListener(AfterDispatch, listener);
    }

    /**
     * The dispatcher will query its controllers and pass the application event to controllers that can handle the
     * particular event type.
     * 
     * @param type
     *            the event type
     */
    public void dispatch(EventType type)
    {
        dispatch(new AppEvent(type));
    }

    /**
     * The dispatcher will query its controllers and pass the application event to controllers that can handle the
     * particular event type.
     * 
     * @param type
     *            the event type
     * @param data
     *            the app event data
     */
    public void dispatch(EventType type, Object data)
    {
        dispatch(new AppEvent(type, data));
    }

    /**
     * Returns all controllers.
     * 
     * @return the list of controllers
     */
    public List<Controller> getControllers()
    {
        return controllers;
    }

    /**
     * Removes a controller.
     * 
     * @param controller
     *            the controller to be removed
     */
    public void removeController(Controller controller)
    {
        controllers.remove(controller);
    }

    /**
     * Removes a previously added listener.
     * 
     * @param listener
     *            the listener to be removed
     */
    public void removeDispatcherListener(DispatcherListener listener)
    {
        removeListener(BeforeDispatch, listener);
        removeListener(AfterDispatch, listener);
    }

    private void dispatch(AppEvent event)
    {
        DispatcherEvent e = new DispatcherEvent(this, event);
        e.setAppEvent(event);
        if (fireEvent(BeforeDispatch, e))
        {
            List<Controller> copy = new ArrayList<Controller>(controllers);
            for (Controller controller : copy)
            {
                if (controller.canHandle(event))
                {
                    controller.handleEvent(event);
                }
            }
            fireEvent(AfterDispatch, e);
        }
    }
}
