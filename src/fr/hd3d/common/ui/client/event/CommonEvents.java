package fr.hd3d.common.ui.client.event;

import com.extjs.gxt.ui.client.event.EventType;


/**
 * Event types for most commen event types.
 * 
 * @author HD3D
 */
public class CommonEvents
{
    public static final int FIRST_EVENT_NUMBER = 10000;

    public static final EventType ERROR = new EventType(FIRST_EVENT_NUMBER + 0);
    public static final EventType START = new EventType(FIRST_EVENT_NUMBER + 1);
    public static final EventType CONFIG_INITIALIZED = new EventType(FIRST_EVENT_NUMBER + 2);
    public static final EventType PERMISSION_INITIALIZED = new EventType(FIRST_EVENT_NUMBER + 3);
    public static final EventType PERSON_INITIALIZED = new EventType(FIRST_EVENT_NUMBER + 110);

    public static final EventType MODEL_DATA_CREATION_SUCCESS = new EventType(FIRST_EVENT_NUMBER + 101);
    public static final EventType MODEL_DATA_UPDATE_SUCCESS = new EventType(FIRST_EVENT_NUMBER + 102);
    public static final EventType MODEL_DATA_DELETE_SUCCESS = new EventType(FIRST_EVENT_NUMBER + 103);
    public static final EventType MODEL_DATA_REFRESH_SUCCESS = new EventType(FIRST_EVENT_NUMBER + 104);

    public static final EventType SETTINGS_INITIALIZED = new EventType(FIRST_EVENT_NUMBER + 105);
    public static final EventType SETTINGS_RETRIEVED = new EventType(FIRST_EVENT_NUMBER + 106);
    public static final EventType SETTING_SAVED = new EventType(FIRST_EVENT_NUMBER + 107);

    public static final EventType USER_INITIALIZED = new EventType(FIRST_EVENT_NUMBER + 108);

    public static final EventType RECORD_RENAMED = new EventType(FIRST_EVENT_NUMBER + 109);

    public static final EventType CATEGORY_CHILDREN_LOADED = new EventType(FIRST_EVENT_NUMBER + 110);
    public static final EventType SEQUENCE_CHILDREN_LOADED = new EventType(FIRST_EVENT_NUMBER + 111);

    public static final EventType CONSTITUENTS_SELECTED = new EventType(FIRST_EVENT_NUMBER + 112);
    public static final EventType CATEGORIES_SELECTED = new EventType(FIRST_EVENT_NUMBER + 113);
    public static final EventType SHOTS_SELECTED = new EventType(FIRST_EVENT_NUMBER + 114);
    public static final EventType SEQUENCES_SELECTED = new EventType(FIRST_EVENT_NUMBER + 115);

    public static final EventType NAME_CHANGED = new EventType(FIRST_EVENT_NUMBER + 116);

    public static final EventType PROJECT_CHANGED = new EventType(FIRST_EVENT_NUMBER + 117);
    public static final EventType HISTORY_CHANGED = new EventType(FIRST_EVENT_NUMBER + 118);
    public static final EventType IS_EXIST_SUCCESS = new EventType(FIRST_EVENT_NUMBER + 119);

    public static final EventType TASK_TYPE_BUTTON_CLICKED = new EventType(FIRST_EVENT_NUMBER + 120);

    // public static final EventType CATEGORIES_N_CONSTITUENTS_SELECTED = new EventType(FIRST_EVENT_NUMBER + 121);

    public static final EventType SERVER_TIME_REFRESHED = new EventType(FIRST_EVENT_NUMBER + 122);

    public static final EventType SCRIPT_EXECUTION_STARTED = new EventType(FIRST_EVENT_NUMBER + 123);
    public static final EventType SCRIPT_EXECUTION_FINISHED = new EventType(FIRST_EVENT_NUMBER + 124);

    public static final EventType LOG_OUT_AFTER_COOKIE_EXPIRED = new EventType(FIRST_EVENT_NUMBER + 125);
    public static final EventType LOG_OUT = new EventType(FIRST_EVENT_NUMBER + 126);

    public static final EventType TASK_TYPE_EDITOR_SHOWN = new EventType();
    public static final EventType TASK_TYPE_EDITOR_HIDDEN = new EventType();

    public static final EventType PLAYLIST_CREATION_REQUIRED = new EventType(FIRST_EVENT_NUMBER + 126);
    public static final EventType PLAYLIST_CREATION_SUCCESS = new EventType(FIRST_EVENT_NUMBER + 127);
    public static final EventType PLAYLIST_CREATION_FAILURE = new EventType(FIRST_EVENT_NUMBER + 128);

    public static final EventType PLAYLISTREVISIONGROUP_CHILDREN_LOADED = new EventType(FIRST_EVENT_NUMBER + 129);
    public static final EventType PLAYLISTREVISIONGROUP_SELECTED = new EventType(FIRST_EVENT_NUMBER + 130);

    public static final EventType CREATE_STEP_VARIANTS_CLICKED = new EventType(FIRST_EVENT_NUMBER + 131);
    public static final EventType CREATE_STEP_VARIANTS_CONFIRMED = new EventType(FIRST_EVENT_NUMBER + 132);
}
