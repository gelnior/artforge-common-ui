package fr.hd3d.common.ui.client.history;

import java.util.Map;

import com.extjs.gxt.ui.client.core.FastMap;
import com.google.gwt.user.client.History;


/**
 * HistoryUtils is made for easy history token handling. It lets you to get parameter value in the URL history (behind #
 * token) and set these values.<br>
 * Each modification add an history URL to the navigator history list.<br>
 * <br>
 * History behaves like a map, you add, modify or remove key-value couples.
 * 
 * @author HD3D
 */
public class HistoryUtils
{
    private static final String KEY_VALUE_SEPARATOR = ":";
    private static final String COUPLE_SEPARATOR = ",";

    /**
     * Get history parameters as string.
     */
    public static String getHistoryToken()
    {
        return History.getToken();
    }

    /**
     * Get history parameters cookie value as map.
     */
    public static Map<String, String> getAsMap()
    {
        Map<String, String> map = new FastMap<String>();
        String history = getHistoryToken();

        if (history != null)
        {
            String[] keysAndValues = history.split(COUPLE_SEPARATOR);
            for (String keyAndValue : keysAndValues)
            {
                String[] entry = keyAndValue.split(KEY_VALUE_SEPARATOR);
                if (entry.length == 2)
                    map.put(entry[0], entry[1]);
            }
        }

        return map;
    }

    /**
     * @param key
     *            The key of the parameter.
     * @return The parameter value corresponding to the key given in parameter.
     */
    public static String getValue(String key)
    {
        Map<String, String> map = getAsMap();

        return map.get(key);
    }

    /**
     * Change inside the history token, the value of <i>key</i> with <i>value</i>. If key does not exist, it is added.
     * 
     * @param key
     *            The key of the value to set.
     * @param value
     *            The value to set.
     */
    public static void putValue(String key, String value)
    {
        Map<String, String> map = getAsMap();

        map.put(key, value);
        setHistoryToken(map);
    }

    /**
     * Convert map into string like : "key1:value1,key2:value2,...,keyn:valuen", then set it as history token.
     * 
     * @param map
     *            The map to set as history token.
     */
    private static void setHistoryToken(Map<String, String> map)
    {
        StringBuilder builder = new StringBuilder();
        for (Map.Entry<String, String> entry : map.entrySet())
        {
            if (builder.length() > 0)
            {
                builder.append(COUPLE_SEPARATOR);
            }
            builder.append(entry.getKey() + KEY_VALUE_SEPARATOR + entry.getValue());
        }
        History.newItem(builder.toString());
    }

    /**
     * Remove a key and its value from the history token.
     * 
     * @param key
     *            The key to remove.
     */
    public static void removeValue(String key)
    {
        Map<String, String> map = getAsMap();
        map.keySet().remove(key);
        setHistoryToken(map);
    }
}
