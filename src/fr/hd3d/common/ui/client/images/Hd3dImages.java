package fr.hd3d.common.ui.client.images;

import com.extjs.gxt.ui.client.util.IconHelper;
import com.google.gwt.user.client.ui.AbstractImagePrototype;
import com.google.gwt.user.client.ui.Image;


/**
 * Hd3dImages provides standard Images used by Hd3d applications.
 * 
 * @author HD3D
 */
public class Hd3dImages
{
    public final static String URL_SYMBOL = "images/skreeneo_logo_150_90.png";
    public final static String URL_SYMBOL_SMALL = "images/skreeneo_logo_60_20.png";
    public final static String URL_SYMBOL_TINY = "images/skreeneo_logo_25_20.png";

    public final static String LOADING_BALLS = "images/explorateur/loading-balls.gif";
    public final static String LARGE_LOADING = "gxt/images/gxt/shared/large-loading.gif";

    public static final String TASK_TYPE_COLOR_PATH = "images/planning/task_type_color.png";
    public static final String TASK_TYPE_CONSTITUENT_PATH = "images/planning/task_type_constituent.png";
    public static final String TASK_TYPE_SHOT_PATH = "images/planning/task_type_shot.png";
    public static final String CATEGORY_PATH = "images/folder_picture.png";
    public static final String SEQUENCE_PATH = "images/bobine.png";
    public static final String PERSON_PATH = "images/planning/user.png";
    public static final String CONSTITUENT_PATH = "images/workobjecttree/constituent.png";
    public static final String SHOT_PATH = "images/workobjecttree/imovie.png";
    public static final String PRINTER = "images/printer.png";

    /** @return an Image that represents the HD3D symbol (150x90 pixels) */
    public final static Image hd3dSymbol()
    {
        Image hd3dLogoImage = new Image();
        hd3dLogoImage.setUrl(URL_SYMBOL);

        return hd3dLogoImage;
    }

    /** @return an Image that represents the HD3D symbol (60x20 pixels) */
    public final static Image hd3dSmallSymbol()
    {
        Image hd3dLogoImage = new Image();
        hd3dLogoImage.setUrl(URL_SYMBOL_SMALL);

        return hd3dLogoImage;
    }

    /** @return an Image that represents the HD3D symbol (20x15 pixels) */
    public final static Image hd3dTinySymbol()
    {
        Image hd3dLogoImage = new Image();
        hd3dLogoImage.setUrl(URL_SYMBOL_TINY);

        return hd3dLogoImage;
    }

    /** @return round loading image (32x32 pixels) */
    public final static Image largeLoading()
    {
        Image largeLoadingImage = new Image(LARGE_LOADING);

        return largeLoadingImage;
    }

    public static AbstractImagePrototype getHd3dTinySymbol()
    {
        return IconHelper.create(URL_SYMBOL_TINY);
    }

    public static AbstractImagePrototype getStatsIcon()
    {
        return IconHelper.create("images/chart_bar.png");
    }

    public static AbstractImagePrototype getStatDetailsIcon()
    {
        return IconHelper.create("images/chart_bar_details.png");
    }

    public static AbstractImagePrototype getAddIcon()
    {
        return IconHelper.create("images/explorateur/add.png");
    }

    public static AbstractImagePrototype getMediumAddIcon()
    {
        return IconHelper.create("images/explorateur/add_medium.png", 24, 24);
    }

    public static AbstractImagePrototype getRefreshPageIcon()
    {
        return IconHelper.create("images/explorateur/table_refresh.png");
    }

    public static AbstractImagePrototype getEditIcon()
    {
        return IconHelper.create("images/workobjecttree/pencil.png");
    }

    public static AbstractImagePrototype getDeleteIcon()
    {
        return IconHelper.create("images/remove.png");
    }

    public static AbstractImagePrototype getCsvExportIcon()
    {
        return IconHelper.create("images/explorateur/export_csv.png");
    }

    public static AbstractImagePrototype getAddTagIcon()
    {
        return IconHelper.create("images/tag_add.png");
    }

    public static AbstractImagePrototype getRemoveAllFiltersIcon()
    {
        return IconHelper.create("images/cancel.png");
    }

    public static AbstractImagePrototype getUndoIcon()
    {
        return IconHelper.create("images/undo-icon.png");
    }

    public static AbstractImagePrototype getProjectIcon()
    {
        return IconHelper.create("images/bricks.png");
    }

    public static AbstractImagePrototype getEditProjectTypeIcon()
    {
        return IconHelper.create("images/project_type_edit.png");
    }

    public static AbstractImagePrototype getImportIcon()
    {
        return IconHelper.create("images/database_add.png");
    }

    public static AbstractImagePrototype getAssetIcon()
    {
        return IconHelper.create("images/asset.png");
    }

    public static AbstractImagePrototype getTaskTypeIcon()
    {
        return IconHelper.create("images/shape_square_add.png");
    }

    public static AbstractImagePrototype getTaskTypeEditIcon()
    {
        return IconHelper.create("images/shape_square_edit.png");
    }

    public static AbstractImagePrototype getRefreshIcon()
    {
        return IconHelper.create("gxt/images/default/grid/refresh.png");
    }

    public static AbstractImagePrototype getSaveIcon()
    {
        return IconHelper.create("images/explorateur/disk.png");
    }

    public static AbstractImagePrototype getMediumSaveIcon()
    {
        return IconHelper.create("images/explorateur/disk_medium.png", 24, 24);
    }

    public static AbstractImagePrototype getAutoSaveIcon()
    {
        return IconHelper.create("images/explorateur/auto_disk.png");
    }

    public static AbstractImagePrototype getLockIcon()
    {
        return IconHelper.create("images/lock.png");
    }

    public static AbstractImagePrototype getUnlockIcon()
    {
        return IconHelper.create("images/lock_open.png");
    }

    public static AbstractImagePrototype getFilterIcon()
    {
        return IconHelper.create("images/explorateur/funnel.png");
    }

    public static AbstractImagePrototype getMosaicIcon()
    {
        return IconHelper.create("images/explorateur/mosaic.png");
    }

    public static AbstractImagePrototype getShotIcon()
    {
        return IconHelper.create(SHOT_PATH);
    }

    public static AbstractImagePrototype getConsituentIcon()
    {
        return IconHelper.create("images/workobjecttree/constituent.png");
    }

    public static AbstractImagePrototype getCategoryIcon()
    {
        return IconHelper.createPath(CATEGORY_PATH);
    }

    public static AbstractImagePrototype getSequenceIcon()
    {
        return IconHelper.createPath(SEQUENCE_PATH);
    }

    public static AbstractImagePrototype getConstituentIcon()
    {
        return IconHelper.createPath(CONSTITUENT_PATH);
    }

    public static AbstractImagePrototype getTodayIcon()
    {
        return IconHelper.createPath("images/calendar_view_day.png");
    }

    public static AbstractImagePrototype getListDetail()
    {
        return IconHelper.createPath("images/detail_list.png");
    }

    public static AbstractImagePrototype getTaskGroupIcon()
    {
        return IconHelper.createPath("images/planning/task_group.gif");
    }

    public static AbstractImagePrototype getTaskGroupAutoResizeIcon()
    {
        return IconHelper.createPath("images/planning/task_group_wand.gif");
    }

    public static AbstractImagePrototype getTaskTypeColorIcon()
    {
        return IconHelper.createPath(TASK_TYPE_COLOR_PATH);
    }

    public static AbstractImagePrototype getTaskTypeConstituentIcon()
    {
        return IconHelper.createPath(TASK_TYPE_CONSTITUENT_PATH);
    }

    public static AbstractImagePrototype getTaskTypeShotIcon()
    {
        return IconHelper.createPath(TASK_TYPE_SHOT_PATH);
    }

    public static AbstractImagePrototype getTaskStatusColorIcon()
    {
        return IconHelper.createPath("images/planning/task_status_color.png");
    }

    public static AbstractImagePrototype getTaskLateColorIcon()
    {
        return IconHelper.createPath("images/planning/task_late_color.png");
    }

    public static AbstractImagePrototype getPlanningBigZoomIcon()
    {
        return IconHelper.createPath("images/planning/zoom_big.png");
    }

    public static AbstractImagePrototype getPlanningNormalZoomIcon()
    {
        return IconHelper.createPath("images/planning/zoom_normal.png");
    }

    public static AbstractImagePrototype getPlanningSmallZoomIcon()
    {
        return IconHelper.createPath("images/planning/zoom_small.png");
    }

    public static AbstractImagePrototype getAddAttributeIcon()
    {
        return IconHelper.createPath("images/explorateur/column_add.png");
    }

    public static AbstractImagePrototype getPrinterIcon()
    {
        return IconHelper.create("images/printer.png");
    }

    public static AbstractImagePrototype getGroupIcon()
    {
        return IconHelper.createPath("images/group.png");
    }

    public static AbstractImagePrototype getPersonIcon()
    {
        return IconHelper.createPath("images/person.png");
    }

    public static AbstractImagePrototype getMoveUpIcon()
    {
        return IconHelper.createPath("gxt/images/default/grid/col-move-bottom.gif");
    }

    public static AbstractImagePrototype getExclamationIcon()
    {
        return IconHelper.createPath("gxt/images/default/form/exclamation.gif");
    }

    public static AbstractImagePrototype getWarningIcon()
    {
        return IconHelper.createPath("gxt/images/default/shared/warning.gif");
    }

    public static AbstractImagePrototype getMoveDownIcon()
    {
        return IconHelper.createPath("gxt/images/default/grid/col-move-top.gif");
    }

    public static AbstractImagePrototype getFileIcon()
    {
        return IconHelper.createPath("images/explorateur/file.png");
    }

    public static AbstractImagePrototype getCheckIcon()
    {
        return IconHelper.createPath("gxt/images/default/tree/drop-yes.gif");
    }

    public static AbstractImagePrototype getConnectIcon()
    {
        return IconHelper.createPath("images/explorateur/connect.png");
    }
}
