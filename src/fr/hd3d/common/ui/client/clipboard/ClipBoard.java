package fr.hd3d.common.ui.client.clipboard;

import java.util.ArrayList;
import java.util.List;


/**
 * ClipBoard class allow to store the data to copy and get this data to paste. This class is singleton pattern. To get,
 * use the static method get(). It is lazy load.
 * 
 * @author HD3D
 * 
 */
public class ClipBoard
{
    /** Instance of singleton. */
    private static ClipBoard instance = null;

    private Object copy = null;

    public static ClipBoard get()
    {
        if (instance == null)
        {
            instance = new ClipBoard();
        }
        return instance;
    }

    /**
     * Private constructor.
     */
    private ClipBoard()
    {}

    /**
     * Store data to copy. The data can be any objects. The method 'copy' have to implement in the object to copy else
     * the behavior is undefined.
     * 
     * @param objToCopy
     *            the data to copy.
     */
    public void copy(Copyable objToCopy)
    {
        if (objToCopy == null)
        {
            copy = null;
            return;
        }

        copy = objToCopy.copy();
    }

    /**
     * Store data to copy. The data can be any objects.
     * 
     * @param objsToCopy
     *            the data to copy.
     */
    public void copy(List<? extends Copyable> objsToCopy)
    {
        if (objsToCopy == null)
        {
            copy = null;
            return;
        }
        ArrayList<Copyable> dataList = new ArrayList<Copyable>();
        for (Copyable obj : objsToCopy)
        {
            Copyable data = obj.copy();
            dataList.add(data);
        }
        copy = dataList;
    }

    /**
     * Get the data to copy.The data can be any objects.
     * 
     * @return the data to copy.
     */
    public Object paste()
    {
        return copy;
    }

    /**
     * Cancel the copy.
     */
    public void reset()
    {
        copy = null;
    }
}
