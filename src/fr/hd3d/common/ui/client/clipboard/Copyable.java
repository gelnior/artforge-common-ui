package fr.hd3d.common.ui.client.clipboard;

public interface Copyable
{
    /**
     * Return a copy on the instance.
     * 
     * @return a copy
     */
    public Copyable copy();
}
