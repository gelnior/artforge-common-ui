package fr.hd3d.common.ui.client.service.callback;

import java.util.List;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.data.ListLoadResult;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.modeldata.reader.TagReaderSingleton;
import fr.hd3d.common.ui.client.modeldata.technical.TagModelData;
import fr.hd3d.common.ui.client.widget.tageditor.TagEditorEvents;


/**
 * Update tag with first retrieved tag data.
 * 
 * @author HD3D
 */
public class TagRefreshCallback extends BaseCallback
{
    /** Tag that is going to be refreshed. */
    protected TagModelData tag;

    /**
     * Default constructor.
     * 
     * @param tag
     *            Tag that is going to be refreshed.
     */
    public TagRefreshCallback(TagModelData tag)
    {
        this.tag = tag;
    }

    /**
     * When tag is refreshed, the name and id of the tag are updated, then TAG_REFRESHED event is raised.
     */
    @Override
    protected void onSuccess(Request request, Response response)
    {
        try
        {
            String json = response.getEntity().getText();
            IReader<TagModelData> reader = TagReaderSingleton.getInstance();

            ListLoadResult<TagModelData> result = reader.read(json);
            List<TagModelData> tags = result.getData();

            if (tags.size() > 0)
            {
                this.tag.setId(tags.get(0).getId());
                this.tag.setName(tags.get(0).getName());
            }

            AppEvent event = new AppEvent(TagEditorEvents.TAG_REFRESHED, tag);
            EventDispatcher.forwardEvent(event);
        }
        catch (Exception e)
        {
            GWT.log("Tag refreshing failed, server response is empty (null).", e);
        }
    }
}
