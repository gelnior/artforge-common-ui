package fr.hd3d.common.ui.client.service.callback;

import java.io.IOException;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.logs.Logger;


public class MaxRecordCallback extends BaseCallback
{
    private final AppEvent event;

    public final static String MAX_RECORD = "max-record";

    public MaxRecordCallback(EventType event)
    {
        this(new AppEvent(event));
    }

    public MaxRecordCallback(AppEvent event)
    {
        this.event = event;
    }

    @Override
    protected void onSuccess(Request request, Response response)
    {
        try
        {
            long maxRecord = getMaxRecord(response);
            event.setData(MAX_RECORD, maxRecord);
            EventDispatcher.forwardEvent(event);

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Return the max record in the response.
     * 
     * @param response
     *            the response
     * @return the max record. Returns -1 if there are errors;
     * @throws IOException
     */

    protected long getMaxRecord(Response response) throws IOException
    {
        String maxRecordString = null;
        long maxRecord = -1;
        try
        {
            maxRecordString = response.getEntity().getText();
            maxRecord = Long.parseLong(maxRecordString);
        }
        catch (NumberFormatException nfe)
        {
            Logger.error("MaxCallback : the max record is not a long : max record : " + maxRecordString, nfe);
            // throw new IOException(nfe);
        }
        return maxRecord;
    }
}
