package fr.hd3d.common.ui.client.service.callback;

import java.io.IOException;

import org.restlet.client.Request;
import org.restlet.client.Response;
import org.restlet.client.data.Status;

import fr.hd3d.common.client.ScriptMessageGenerator;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;

public class RunScriptCallBack extends BaseCallback {

	@Override
	protected void onSuccess(Request request, Response response) {
		ScriptMessageGenerator errorMessageGenerator = null;
        try
        {
            if (Status.SUCCESS_OK.equals(response.getStatus()))
            {
                EventDispatcher.forwardEvent(CommonEvents.SCRIPT_EXECUTION_FINISHED, response.getEntity()
                        .getText());
            }
            else if (Status.SUCCESS_NO_CONTENT.equals(response.getStatus()))
            {
                errorMessageGenerator = new ScriptMessageGenerator(1, "No script found", "");
                EventDispatcher.forwardEvent(CommonEvents.SCRIPT_EXECUTION_FINISHED, errorMessageGenerator.errorMessageToJsonString());
            }
        }
        catch (IOException e)
        {
            errorMessageGenerator = new ScriptMessageGenerator(0, "Script finished succefully", "");
            EventDispatcher.forwardEvent(CommonEvents.SCRIPT_EXECUTION_FINISHED, errorMessageGenerator.errorMessageToJsonString());
        }
	}

}
