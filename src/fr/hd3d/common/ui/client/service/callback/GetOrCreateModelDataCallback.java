package fr.hd3d.common.ui.client.service.callback;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;


/**
 * Callback that updates fields of given record. If no data are returned by server, the record is created.
 * 
 * @author HD3D
 */
public class GetOrCreateModelDataCallback extends GetModelDataCallback
{

    public GetOrCreateModelDataCallback(Hd3dModelData record, AppEvent event)
    {
        super(record, event);
    }

    @Override
    protected void onNoRecordReturned()
    {
        record.save(event);
    }
}
