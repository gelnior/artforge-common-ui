package fr.hd3d.common.ui.client.service.callback;

import java.io.IOException;
import java.util.List;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.data.ListLoadResult;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.error.ErrorDispatcher;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.reader.Hd3dModelDataReaderSingleton;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;


/**
 * Callback that Check if the record is already exist in server data. Forward the given event type with the boolean
 * corresponding to if exist.
 * 
 * @author HD3D
 */
public class IsExistCallBack extends BaseCallback
{
    /** Record to update with its new ID. */
    protected final Hd3dModelData record;
    /** Event Type to raise when data are successfully updated. */
    protected AppEvent event = new AppEvent(CommonEvents.IS_EXIST_SUCCESS);

    protected final String[] properties;

    /**
     * Default constructor.
     * 
     * @param record
     *            Record to update with its new data.
     * @param event
     *            The event type to raise when update is complete.
     */
    public IsExistCallBack(Hd3dModelData record, final AppEvent event, String[] properties)
    {
        this.record = record;
        if (event != null && event.getType() != null)
        {
            this.event = event;
        }
        if (properties == null)
        {
            this.properties = new String[0];
        }
        else
        {
            this.properties = properties;
        }
    }

    /**
     * Alternative constructor. Do no send event when succeeds.
     * 
     * @param record
     *            Record to update with its new data.
     */
    public IsExistCallBack(Hd3dModelData record)
    {
        this(record, null, null);
    }

    /**
     * The callback check if the record is already exist.
     */
    @Override
    public void onSuccess(Request request, Response response)
    {
        try
        {
            String json = response.getEntity().getText();
            if (json != null && json.length() > 0)
            {
                IReader<Hd3dModelData> reader = Hd3dModelDataReaderSingleton.getInstance();
                Hd3dModelDataReaderSingleton.setModelType(this.record.getSimpleClassName());

                ListLoadResult<Hd3dModelData> result = reader.read(json);
                List<Hd3dModelData> records = result.getData();

                if (records.size() > 0)
                {
                    this.onExist(records);
                }
                else
                {
                    this.onNotExist();
                }
            }
            else
            {
                ErrorDispatcher.sendError(CommonErrors.RECORD_NOT_ON_SERVER);
            }
        }
        catch (IOException e)
        {
            GWT.log("Json data cannot be retrieved.", e);
            ErrorDispatcher.sendError(CommonErrors.RECORD_NOT_ON_SERVER);
        }

    }

    /**
     * Method called when records are returned. Forward the event with the boolean true.
     * 
     * @param records
     *            The returned records.
     */
    protected void onExist(List<Hd3dModelData> records)
    {
        boolean equal = true;

        for (Hd3dModelData model : records)
        {
            equal = true;
            for (String property : properties)
            {
                Object propertyModel = model.get(property);
                Object propertyRecord = record.get(property);

                if ((propertyModel == null && propertyRecord != null)
                        || (propertyModel != null && !propertyModel.equals(propertyRecord)))
                {
                    equal = false;
                    break;
                }

            }
            if (equal)
            {
                break;
            }
        }
        event.setData(Boolean.valueOf(equal));
        EventDispatcher.forwardEvent(event);
    }

    /**
     * Method called when no record is returned. Forward the event with the boolean false.
     */
    protected void onNotExist()
    {
        event.setData(Boolean.valueOf(false));
        EventDispatcher.forwardEvent(event);
    }
}
