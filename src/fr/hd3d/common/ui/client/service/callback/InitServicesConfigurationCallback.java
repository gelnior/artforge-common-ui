package fr.hd3d.common.ui.client.service.callback;

import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.Response;
import com.google.gwt.xml.client.Document;
import com.google.gwt.xml.client.Node;
import com.google.gwt.xml.client.NodeList;
import com.google.gwt.xml.client.XMLParser;

import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.error.ErrorDispatcher;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
import fr.hd3d.common.ui.client.restlet.RestRequestHandler;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.widget.mainview.CustomButtonInfos;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;


/**
 * When initialization response is received, it parses the retrieved config.xml to get the services path element and
 * then sets the requestHandler services path with it. Then it warns the controller that services configuration is done.
 * 
 * @author HD3D
 */
public class InitServicesConfigurationCallback implements RequestCallback
{
    /** Model to configure with data retrieved. */
    protected MainModel model;

    /**
     * Default constructor.
     * 
     * @param model
     *            Main model to configure with data retrieved.
     */
    public InitServicesConfigurationCallback(MainModel model)
    {
        this.model = model;
    }

    /**
     * When error occurs, a "No configuration file found" error is sent.
     */
    public void onError(Request request, Throwable exception)
    {
        ErrorDispatcher.sendError(CommonErrors.NO_CONFIG_FILE);
    }

    /**
     * @see com.google.gwt.http.client.RequestCallback#onResponseReceived(com.google.gwt.http.client.Request,
     *      com.google.gwt.http.client.Response)
     */
    public void onResponseReceived(Request request, Response response)
    {
        try
        {
            Document configDocument = XMLParser.parse(response.getText());
            this.onParsingSucceed(configDocument);
        }
        catch (Exception e)
        {
            ErrorDispatcher.sendError(CommonErrors.BAD_CONFIG_FILE);
        }

        EventDispatcher.forwardEvent(CommonEvents.CONFIG_INITIALIZED);
    }

    /**
     * When parsing succeeds, it reads documents for services path and sets the services path into the REST request
     * handler. Then it parses custom buttons informations should be registered. Custom buttons are additional buttons
     * calling script set by user.
     * 
     * @param configDocument
     *            The document parsed.
     * @throws Exception
     *             exception raised if an element does not exist.
     */
    protected void onParsingSucceed(Document configDocument) throws Exception
    {
        this.parseServicesPath(configDocument);
        this.parseCustomButtons(configDocument);
    }

    /**
     * Retrieve services path from configuration file and set it to the REST request handler.
     * 
     * @param configDocument
     *            the document to parse.
     */
    protected void parseServicesPath(Document configDocument)
    {
        Node urlNode = configDocument.getElementsByTagName(RestRequestHandler.XML_ELEMENT_SERVICES_PATH).item(0);
        String servicePath = urlNode.getChildNodes().item(0).getNodeValue();

        IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
        requestHandler.setServicePath(servicePath);
    }

    /**
     * Retrieve custom buttons data from configuration file by extracting child from \<buttons\> tag.
     * 
     * @param configDocument
     *            the document to parse.
     */
    protected void parseCustomButtons(Document configDocument)
    {
        NodeList buttonsNode = configDocument.getElementsByTagName(MainModel.BUTTONS_XML_TAG);

        for (int i = 0; i < buttonsNode.getLength(); i++)
        {
            Node node = buttonsNode.item(i);
            Node nameNode = node.getChildNodes().item(1);
            Node scriptNode = node.getChildNodes().item(3);
            String buttonName = nameNode.getFirstChild().getNodeValue();
            String buttonScript = scriptNode.getFirstChild().getNodeValue();

            CustomButtonInfos button = new CustomButtonInfos(buttonName, buttonScript);
            this.model.addCustomButton(button);
        }
    }
}
