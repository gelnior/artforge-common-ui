package fr.hd3d.common.ui.client.service.callback;

import java.io.IOException;
import java.util.Date;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.logs.Logger;


public class ServerTimeCallback extends BaseCallback
{
    private final static AppEvent default_event = new AppEvent(CommonEvents.SERVER_TIME_REFRESHED);

    private final AppEvent event;
    private final Date serverTime;

    public ServerTimeCallback(Date serverTime)
    {
        this(serverTime, default_event);
    }

    public ServerTimeCallback(Date serverTime, AppEvent event)
    {
        if (serverTime == null)
        {
            throw new IllegalArgumentException("ServerTimeCallback - Constructor -> The date must not be null!!");
        }
        this.serverTime = serverTime;
        this.event = event;
    }

    @Override
    protected void onSuccess(Request request, Response response)
    {
        String timeStampString = null;
        try
        {
            timeStampString = response.getEntity().getText();
            long timeStamp = Long.parseLong(timeStampString);
            serverTime.setTime(timeStamp);
            EventDispatcher.forwardEvent(event);
        }
        catch (NumberFormatException nfe)
        {
            Logger.error("ServerTimeCallback : the timestamp is not a long : timestamp: " + timeStampString, nfe);
        }
        catch (IOException e)
        {
            Logger.error("While parsing time service response.", e);
        }
    }
}
