package fr.hd3d.common.ui.client.service.callback;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.config.CommonConfig;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;


/**
 * Callback that sets the right ID after an instance creation on server. If ID update succeeds, it raises an event set
 * by user (MODEL_DATA_CREATION_SUCCESS by default).
 * 
 * @author HD3D
 */
public class PostModelDataCallback extends BaseCallback
{
    /** Record to update with its new ID. */
    protected final Hd3dModelData record;
    /** The event type to raise after setting the new ID. */
    protected AppEvent event = new AppEvent(CommonEvents.MODEL_DATA_CREATION_SUCCESS);

    /**
     * Default constructor.
     * 
     * @param record
     *            Record to update with its new ID.
     */
    public PostModelDataCallback(Hd3dModelData record, AppEvent event)
    {
        this.record = record;
        if (event != null && event.getType() != null)
        {
            this.event = event;
        }
    }

    public PostModelDataCallback(Hd3dModelData record)
    {
        this(record, null);
    }

    /**
     * It sets the right ID after an instance creation on server. If ID update succeeds, an event set by user
     * (MODEL_DATA_CREATION_SUCCESS by default).
     */
    @Override
    protected void onSuccess(Request request, Response response)
    {
        this.setNewId(response);

        event.setData(CommonConfig.CLASS_EVENT_VAR_NAME, this.record.getSimpleClassName());
        event.setData(CommonConfig.ID_EVENT_VAR_NAME, this.record.getId());
        event.setData(CommonConfig.MODEL_EVENT_VAR_NAME, this.record);

        EventDispatcher.forwardEvent(event);
    }

    protected void setNewId(Response response)
    {
        if (response.getEntity() != null && response.getEntity().getLocationRef() != null)
        {
            String url = response.getEntity().getLocationRef().toString();
            if (url != null && url.length() > 0)
            {
                long id = new Long(url.substring(url.lastIndexOf('/') + 1));
                this.record.setId(id);

                String defaultPath = url.substring(RestRequestHandlerSingleton.getInstance().getServicesUrl().length());
                this.record.setDefaultPath(defaultPath);
            }
        }
    }

}
