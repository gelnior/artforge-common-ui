package fr.hd3d.common.ui.client.service.callback;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.EventDispatcher;


/**
 * Simple callback that forwards the event given in parameter.
 * 
 * @author HD3D
 */
public class EventCallback extends BaseCallback
{
    /** The event to forward. */
    protected final AppEvent event;

    /**
     * Default constructor.Build a new event from event type.
     * 
     * @param event
     *            The event type to forward.
     */
    public EventCallback(EventType event)
    {
        this.event = new AppEvent(event);
    }

    /**
     * Alternative constructor.
     * 
     * @param event
     *            The event to forward.
     */
    public EventCallback(AppEvent event)
    {
        this.event = event;
    }

    /**
     * On request success, it forwards the event given in parameter.
     */
    @Override
    public void onSuccess(Request request, Response response)
    {
        EventDispatcher.forwardEvent(event);
    }
}
