package fr.hd3d.common.ui.client.service.callback;

import java.util.ArrayList;
import java.util.List;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;


public class BaseBulkCallback extends BaseCallback
{
    protected List<? extends Hd3dModelData> modelsSaved = new ArrayList<Hd3dModelData>();
    private AppEvent event;

    public BaseBulkCallback(List<? extends Hd3dModelData> modelsSaved, AppEvent event)
    {
        this.modelsSaved = modelsSaved;
        this.event = event;
    }

    public BaseBulkCallback(List<Hd3dModelData> modelsSaved, EventType eventType)
    {
        this(modelsSaved, new AppEvent(eventType));
    }

    public BaseBulkCallback(List<? extends Hd3dModelData> modelsSaved)
    {
        this.modelsSaved = modelsSaved;
    }

    @Override
    protected void onSuccess(Request request, Response response)
    {
        if (response.getEntity().getLocationRef() != null)
        {
            String identifiers = response.getEntity().getLocationRef().toString();
            Logger.log(identifiers);

            String[] urls = identifiers.substring(1, identifiers.length() - 1).split(",");

            for (int i = 0; i < urls.length; i++)
            {
                String url = urls[i];
                if (url != null && url.length() > 0)
                {
                    long id = new Long(url.substring(url.lastIndexOf('/') + 1));
                    modelsSaved.get(i).setId(id);
                }
            }
        }
        this.onIdsUpdated();
    }

    protected void onIdsUpdated()
    {
        if (event != null && event.getType() != null)
        {
            event.setData(modelsSaved);
            EventDispatcher.forwardEvent(event);
        }
    }
}
