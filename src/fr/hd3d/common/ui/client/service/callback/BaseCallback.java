package fr.hd3d.common.ui.client.service.callback;

import java.io.IOException;

import org.restlet.client.Request;
import org.restlet.client.Response;
import org.restlet.client.Uniform;
import org.restlet.client.data.Status;

import com.google.gwt.core.client.GWT;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONString;

import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.error.ErrorDispatcher;
import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.restlet.RestRequestHandler;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;


/**
 * Callback that handles common errors like authentication, connection, permission and server errors.<br>
 * <br>
 * This class contains the abstract method onSucess that should be override to define which behavior to adopt when the
 * request succeeds.
 * 
 * @author HD3D
 */
public abstract class BaseCallback implements Uniform
{
    /**
     * When server encounters some types of errors, it sends a redirection response. The default behavior for the client
     * is to send again the sent request. To avoid infinite loop case, a redirection counter is set, to limit to 3 the
     * number of sending retries .
     */
    int nbRedirection = 0;

    public void handle(Request request, Response response)
    {
        if (response.getStatus().isSuccess())
        {
            this.onSuccess(request, response);
        }
        else if (response.getStatus().isConnectorError())
        {
            Logger.log("Restlet connector error.", response.getStatus().getThrowable());
            this.onServerError(request, response);
            // this.onSuccess(request, response);
        }
        else if (response.getStatus().getCode() == 401)
        {
            this.onAuthenticationError();
        }
        else if (response.getStatus().getCode() == 403)
        {
            this.onPermissionError();
        }
        else if (response.getStatus().isClientError())
        {
            this.onServerError(request, response);
        }
        else if (response.getStatus().isServerError())
        {
            this.onServerError(request, response);
        }
        else if (response.getStatus().getCode() == Status.REDIRECTION_TEMPORARY.getCode()
                || response.getStatus().getCode() == Status.REDIRECTION_FOUND.getCode())
        {
            this.onRedirection(request, response);
        }
        else if (response.getStatus().getCode() == 204)
        {
            this.onNoRecordError();
        }
        else
        {
            this.onNoServiceError();
        }
    }

    protected abstract void onSuccess(Request request, Response response);

    /**
     * Method called every time an error occurs. Has no effect but is useful for overriding.
     */
    protected void onError()
    {}

    /**
     * When the request has been refused because the user does not have the permission, it forwards the NO_PERMISSION
     * error.
     */
    protected void onPermissionError()
    {
        this.onError();
        ErrorDispatcher.sendError(CommonErrors.NO_PERMISSION);
    }

    /**
     * When the request has been refused because the user is not authenticated, it forwards the NOT_AUTHENTIFIED error.
     */
    protected void onAuthenticationError()
    {
        this.onError();
        ErrorDispatcher.sendError(CommonErrors.NOT_AUTHENTIFIED);
    }

    /**
     * When the request has been refused because the server encountered a internal error, it forwards the SERVER_ERROR
     * error. For better view. User message and stack error message are extracted from server response.
     */
    protected void onServerError(Request request, Response response)
    {
        this.onError();

        try
        {
            if (GWT.isScript())
            {
                JSONObject object = (JSONObject) JSONParser.parse(response.getEntity().getText());
                JSONArray array = (JSONArray) ((JSONArray) object.get("records")).get(0);
                String usrMsg = ((JSONString) array.get(0)).stringValue();
                String stack = ((JSONString) array.get(1)).stringValue();

                ErrorDispatcher.sendError(CommonErrors.SERVER_ERROR, usrMsg, stack);
            }
            else
            {
                ErrorDispatcher.sendError(CommonErrors.SERVER_ERROR, response.getEntity().getText(), "");
                GWT.log("BaseCallback error occured");
            }
        }
        catch (Exception e)
        {
            try
            {
                if (response != null && response.getEntity() != null)
                    ErrorDispatcher.sendError(CommonErrors.SERVER_ERROR, response.getEntity().getText());
                else
                    ErrorDispatcher.sendError(CommonErrors.SERVER_ERROR);
            }
            catch (IOException e1)
            {
                GWT.log("An error occurs on server, but server error parsing failed. No message can be displayed.", e1);
            }
        }
    }

    /**
     * When a redirection response is received, it sends the request again.
     */
    private void onRedirection(Request request, Response response)
    {
        if (nbRedirection < 3)
        {
            RestRequestHandler requestHandler = (RestRequestHandler) RestRequestHandlerSingleton.getInstance();

            Logger.log("Redirection found, resend request.");
            requestHandler.sendRequest(request, this);

            nbRedirection++;
        }
        else
        {
            nbRedirection = 0;
            this.onServerError(request, response);
        }
    }

    /**
     * When the request has been refused because the server encountered no content error, it forwards the
     * RECORD_NOT_ON_SERVER error.
     */
    protected void onNoRecordError()
    {
        this.onError();
        ErrorDispatcher.sendError(CommonErrors.RECORD_NOT_ON_SERVER);
    }

    /**
     * When the request has been refuses because the service has not been found, it forwards the NO_SERVICE_CONNECTION
     * error.
     */
    protected void onNoServiceError()
    {
        this.onError();
        ErrorDispatcher.sendError(CommonErrors.NO_SERVICE_CONNECTION);
    }
}
