package fr.hd3d.common.ui.client.service.callback;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.config.CommonConfig;
import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.error.ErrorDispatcher;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;


/**
 * Callback that raises the event set by the user (MODEL_DATA_DELETE_SUCCESS by default) when delete operation succeeds.
 * 
 * @author HD3D
 */
public class DeleteModelDataCallback extends BaseCallback
{
    /** Record to update with its new ID. */
    protected final Hd3dModelData record;
    /** The event type to raise when success. */
    protected AppEvent appEvent = new AppEvent(CommonEvents.MODEL_DATA_DELETE_SUCCESS);

    /**
     * Default constructor.
     * 
     * @param record
     *            Record to update with its new ID.
     */
    public DeleteModelDataCallback(Hd3dModelData record, EventType eventType)
    {
        this(record, new AppEvent(eventType));
    }

    /**
     * Default constructor.
     * 
     * @param record
     *            Record to update with its new ID.
     */
    public DeleteModelDataCallback(Hd3dModelData record, AppEvent appEvent)
    {
        this.record = record;
        if (appEvent != null)
        {
            this.appEvent = appEvent;
        }
    }

    /**
     * Raise the event set by the user (MODEL_DATA_DELETE_SUCCESS by default) when delete operation succeeds.
     */
    @Override
    public void onSuccess(Request request, Response response)
    {
        if (response.getStatus().isSuccess())
        {
            appEvent.setData(CommonConfig.CLASS_EVENT_VAR_NAME, this.record.getSimpleClassName());
            appEvent.setData(CommonConfig.ID_EVENT_VAR_NAME, this.record.getId());
            appEvent.setData(CommonConfig.MODEL_EVENT_VAR_NAME, this.record);
            EventDispatcher.forwardEvent(appEvent);
        }
        else if (response.getStatus().isServerError())
        {
            ErrorDispatcher.sendError(CommonErrors.SERVER_ERROR);
        }
        else
        {
            ErrorDispatcher.sendError(CommonErrors.NO_SERVICE_CONNECTION);
        }
    }
}
