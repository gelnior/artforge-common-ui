package fr.hd3d.common.ui.client.service.callback;

import java.io.IOException;
import java.util.List;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.data.ListLoadResult;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.config.CommonConfig;
import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.error.ErrorDispatcher;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.reader.Hd3dModelDataReaderSingleton;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;


/**
 * Callback that refreshes object data with the one retrieved from server. Then it sends an event with entity class name
 * and object id to the event dispatcher.
 * 
 * @author HD3D
 */
public class GetModelDataCallback extends BaseCallback
{
    /** Record to update with its new ID. */
    protected final Hd3dModelData record;
    /** Event Type to raise when data are successfully updated. */
    protected AppEvent event = new AppEvent(CommonEvents.MODEL_DATA_REFRESH_SUCCESS);

    /**
     * Default constructor.
     * 
     * @param record
     *            Record to update with its new data.
     * @param event
     *            The event type to raise when update is complete.
     */
    public GetModelDataCallback(Hd3dModelData record, AppEvent event)
    {
        this.record = record;
        if (event != null && event.getType() != null)
        {
            this.event = event;
        }
    }

    /**
     * Alternative constructor. Do no send event when succeeds.
     * 
     * @param record
     *            Record to update with its new data.
     */
    public GetModelDataCallback(Hd3dModelData record)
    {
        this(record, null);
    }

    /**
     * The callback refreshes object data with the one retrieved from server. Then it sends an event with entity class
     * name and object id to the event dispatcher.
     */
    @Override
    public void onSuccess(Request request, Response response)
    {
        String json;
        try
        {
            json = response.getEntity().getText();
            if (json != null && json.length() > 0)
            {
                IReader<Hd3dModelData> reader = Hd3dModelDataReaderSingleton.getInstance();
                Hd3dModelDataReaderSingleton.setModelType(this.record.getSimpleClassName());

                ListLoadResult<Hd3dModelData> result = reader.read(json);
                List<Hd3dModelData> records = result.getData();

                if (records.size() > 0)
                {
                    this.onRecordReturned(records);
                }
                else
                {
                    this.onNoRecordReturned();
                }
            }
            else
            {
                ErrorDispatcher.sendError(CommonErrors.RECORD_NOT_ON_SERVER);
            }
        }
        catch (IOException e)
        {
            Logger.log("Json data cannot be retrieved.", e);
            ErrorDispatcher.sendError(CommonErrors.RECORD_NOT_ON_SERVER);
        }

    }

    /**
     * Method called when records are returned. By default update registered record with first returned record data.
     * 
     * @param records
     *            The returned records.
     */
    protected void onRecordReturned(List<Hd3dModelData> records)
    {
        Hd3dModelData newRecord = records.get(0);
        this.record.updateFromModelData(newRecord);

        event.setData(CommonConfig.CLASS_EVENT_VAR_NAME, this.record.getSimpleClassName());
        event.setData(CommonConfig.ID_EVENT_VAR_NAME, this.record.getId());
        event.setData(CommonConfig.MODEL_EVENT_VAR_NAME, this.record);
        EventDispatcher.forwardEvent(event);
    }

    /**
     * Method called when no record is returned. Send a RECORD_NOT_ON_SERVER error by default.
     */
    protected void onNoRecordReturned()
    {
        ErrorDispatcher.sendError(CommonErrors.RECORD_NOT_ON_SERVER);
    }
}
