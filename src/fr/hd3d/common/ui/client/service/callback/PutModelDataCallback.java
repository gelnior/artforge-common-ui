package fr.hd3d.common.ui.client.service.callback;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.config.CommonConfig;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;


/**
 * Callback that raises the event set by the user (MODEL_DATA_UPDATE_SUCCESS by default) when update operation succeeds.
 * 
 * @author HD3D
 */
public class PutModelDataCallback extends BaseCallback
{
    /** Record that have been saved to server. */
    protected final Hd3dModelData record;
    /** The event type to raise when success. */
    protected AppEvent event = new AppEvent(CommonEvents.MODEL_DATA_UPDATE_SUCCESS);

    /**
     * Default constructor.
     * 
     * @param record
     *            Record to update with its new ID.
     */
    public PutModelDataCallback(Hd3dModelData record, AppEvent newEvent)
    {
        this.record = record;
        if (event != null)
        {
            this.event = newEvent;
        }
    }

    /**
     * Raise the event set by the user (MODEL_DATA_UPDATE_SUCCESS by default) when delete operation succeeds.
     */
    @Override
    protected void onSuccess(Request request, Response response)
    {
        event.setData(CommonConfig.CLASS_EVENT_VAR_NAME, this.record.getSimpleClassName());
        event.setData(CommonConfig.ID_EVENT_VAR_NAME, this.record.getId());
        event.setData(CommonConfig.MODEL_EVENT_VAR_NAME, this.record);
        event.setData(this.record.getId());

        EventDispatcher.forwardEvent(event);
    }
}
