package fr.hd3d.common.ui.client.service;

import java.util.HashMap;

import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerTypeModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.DeviceModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.DeviceModelModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.DeviceTypeModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.LicenseModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ManufacturerModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.PoolModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ProcessorModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.RoomModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ScreenModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ScreenModelModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.SoftwareModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.StudioMapModelData;
import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
import fr.hd3d.common.ui.client.modeldata.resource.AbsenceModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ContractModelData;
import fr.hd3d.common.ui.client.modeldata.resource.EventModelData;
import fr.hd3d.common.ui.client.modeldata.resource.OrganizationModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.resource.QualificationModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.common.ui.client.modeldata.resource.SkillLevelModelData;
import fr.hd3d.common.ui.client.modeldata.resource.SkillModelData;
import fr.hd3d.common.ui.client.modeldata.task.ExtraLineModelData;
import fr.hd3d.common.ui.client.modeldata.task.PlanningModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskGroupModelData;
import fr.hd3d.common.ui.client.modeldata.technical.AuditModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ProjectSecurityTemplateModelData;
import fr.hd3d.common.ui.client.modeldata.technical.RoleModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;


/**
 * Class that gives services path depending on ModelDataType.
 * 
 * @author HD3D
 */
public class PermissionPath
{
    private final static HashMap<String, String> map = new HashMap<String, String>();

    public static void initPath()
    {
        map.put(AuditModelData.SIMPLE_CLASS_NAME, "audits");
        map.put(SheetModelData.SIMPLE_CLASS_NAME, "sheets");

        map.put(ComputerModelData.SIMPLE_CLASS_NAME, "computers");
        map.put(SoftwareModelData.SIMPLE_CLASS_NAME, "softwares");
        map.put(LicenseModelData.SIMPLE_CLASS_NAME, "licenses");
        map.put(DeviceModelData.SIMPLE_CLASS_NAME, "devices");
        map.put(ScreenModelData.SIMPLE_CLASS_NAME, "screens");
        map.put(StudioMapModelData.SIMPLE_CLASS_NAME, "studiomaps");
        map.put(PoolModelData.SIMPLE_CLASS_NAME, "pools");
        map.put(ComputerModelModelData.SIMPLE_CLASS_NAME, "computermodels");
        map.put(DeviceModelModelData.SIMPLE_CLASS_NAME, "devicemodels");
        map.put(ScreenModelModelData.SIMPLE_CLASS_NAME, "screenmodels");
        map.put(ManufacturerModelData.SIMPLE_CLASS_NAME, "manufacturers");
        map.put(ProcessorModelData.SIMPLE_CLASS_NAME, "processors");
        map.put(ComputerTypeModelData.SIMPLE_CLASS_NAME, "computertypes");
        map.put(DeviceTypeModelData.SIMPLE_CLASS_NAME, "devicetypes");
        map.put(ComputerModelModelData.SIMPLE_CLASS_NAME, "computermodels");
        map.put(DeviceModelModelData.SIMPLE_CLASS_NAME, "devicemodels");
        map.put(ScreenModelModelData.SIMPLE_CLASS_NAME, "screenmodels");
        map.put(RoomModelData.SIMPLE_CLASS_NAME, "rooms");

        map.put(ResourceGroupModelData.SIMPLE_CLASS_NAME, "resourcegroups");
        map.put(SkillModelData.SIMPLE_CLASS_NAME, "skills");
        map.put(SkillLevelModelData.SIMPLE_CLASS_NAME, "skilllevels");
        map.put(QualificationModelData.SIMPLE_CLASS_NAME, "qualifications");
        map.put(OrganizationModelData.SIMPLE_CLASS_NAME, "organizations");
        map.put(ContractModelData.SIMPLE_CLASS_NAME, "contracts");
        map.put(SequenceModelData.SIMPLE_CLASS_NAME, "sequences");
        map.put(CategoryModelData.SIMPLE_CLASS_NAME, "categories");
        map.put(PersonModelData.SIMPLE_CLASS_NAME, "persons");
        map.put(RoleModelData.SIMPLE_CLASS_NAME, "roles");
        map.put(ProjectSecurityTemplateModelData.SIMPLE_CLASS_NAME, "security_templates:templates");
        map.put("Role Templates", "security_templates:roles");
        map.put("Apply Template", "security_templates:apply_template");

        map.put(ProjectModelData.SIMPLE_CLASS_NAME, "projects");

        map.put(PlanningModelData.SIMPLE_CLASS_NAME, "plannings");
        map.put(TaskGroupModelData.SIMPLE_CLASS_NAME, "taskGroups");
        map.put(ExtraLineModelData.SIMPLE_CLASS_NAME, "extraLines");
        map.put(AbsenceModelData.SIMPLE_CLASS_NAME, "absences");
        map.put(EventModelData.SIMPLE_CLASS_NAME, "events");

    }

    public static String getPath(String boundClassName)
    {
        return map.get(boundClassName);
    }
}
