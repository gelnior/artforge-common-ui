package fr.hd3d.common.ui.client.service.parameter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;


/**
 * Constraints is useful to write a multiple
 * 
 * @author HD3D
 */
public class Constraints extends ArrayList<Constraint> implements IUrlParameter
{
    /** Automatically generated serial version UID. */
    private static final long serialVersionUID = 8464584873846635254L;

    public Constraints()
    {
        super();
    }

    public Constraints(Collection<? extends Constraint> constraints)
    {
        super(constraints);
    }

    public Constraints(Constraint... constraints)
    {
        this.addAll(Arrays.asList(constraints));
    }

    /**
     * Write constraint list as json list.
     */
    public String toJson()
    {
        StringBuffer json = new StringBuffer();

        json.append("[");
        for (Constraint constraint : this)
        {
            json.append(constraint.toJson());
            json.append(",");
        }
        if (this.size() > 0)
        {
            json.deleteCharAt(json.length() - 1);
        }
        json.append("]");

        return json.toString();
    }

    /**
     * @return This constraint in a JSON String format with the parameter name in front of it. <br>
     *         Ex : constraint=[{"type":"lt","column":"prix","value":10}, {"type":"eq","column":"size","value":10}]
     */
    @Override
    public String toString()
    {
        return fr.hd3d.common.client.Constraint.CONSTRAINT + "=" + this.toJson();
    }

    public void addEqConstraint(String column, Object value)
    {
        this.add(new EqConstraint(column, value));
    }

    public void addInConstraint(String column, List<Long> values)
    {
        this.add(new InConstraint(column, values));
    }

}
