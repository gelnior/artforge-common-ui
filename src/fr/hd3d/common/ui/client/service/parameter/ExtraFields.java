package fr.hd3d.common.ui.client.service.parameter;

import java.util.ArrayList;
import java.util.List;

import fr.hd3d.common.client.Const;
import fr.hd3d.common.ui.client.modeldata.writer.JSONWriter;


/**
 * ExtraFields generates URL extrafields parameter.
 * 
 * @author HD3D
 */
public class ExtraFields implements IUrlParameter
{
    /** Columns to use for order by. */
    protected List<String> extraFields = new ArrayList<String>();

    /**
     * Default constructor, add one field to the field list.
     * 
     * @param field
     *            The field to add to classic fields.
     */
    public ExtraFields(String field)
    {
        super();

        this.extraFields.add(field);
    }

    /**
     * Second constructor : sets a list of fields to add to classic fields.
     * 
     * @param fieldList
     */
    public ExtraFields(List<String> fieldList)
    {
        super();

        this.extraFields = fieldList;
    }

    /**
     * Add a field at the end of the column list.
     * 
     * @param field
     *            The field to add.
     */
    public void addColumn(String field)
    {
        this.extraFields.add(field);
    }

    /**
     * Remove a column from the list.
     * 
     * @param field
     *            The column to remove.
     */
    public void removeColumn(String field)
    {
        this.extraFields.remove(field);
    }

    /**
     * Clear the field list.
     */
    public void clearFields()
    {
        this.extraFields.clear();
    }

    /**
     * @return The order by parameter at a list format.
     */
    public List<String> toList()
    {
        return this.extraFields;
    }

    /**
     * Return the order by parameter at JSON format.
     */
    public String toJson()
    {
        JSONWriter writer = new JSONWriter();
        return writer.write(this.toList());
    }

    /**
     * Return the order by parameter at JSON format with its variable name.
     */
    @Override
    public String toString()
    {
        return Const.EXTRAFIELDS + "=" + toJson();
    }
}
