package fr.hd3d.common.ui.client.service.parameter;

/**
 * IURLParameter is an URL Parameter that can be represented in json format and rendered in a string at format
 * "key=json-value".
 * 
 * @author HD3D
 */
public interface IUrlParameter
{
    /**
     * @return The parameter at JSON format.
     */
    public String toJson();

    /**
     * @return The parameter at "key=json-value". format.
     */
    public String toString();
}
