package fr.hd3d.common.ui.client.service.parameter;

import java.util.ArrayList;
import java.util.List;

import fr.hd3d.common.client.Constraint;
import fr.hd3d.common.ui.client.modeldata.writer.JSONWriter;


/**
 * ItemOrderBy generates URL item order by parameter.
 * 
 * @author HD3D
 */
public class ItemOrderBy implements IUrlParameter
{
    /** Columns to use for order by. */
    protected List<Long> columList = new ArrayList<Long>();

    /**
     * Default constructor, add one column to the column list.
     * 
     * @param column
     *            The column on which the ordering will be done.
     */
    public ItemOrderBy(Long column)
    {
        this.columList.add(column);
    }

    /**
     * Second constructor : sets a list of columns for the order by.
     * 
     * @param columnList
     */
    public ItemOrderBy(List<Long> columnList)
    {
        this.columList = columnList;
    }

    /**
     * Add a column at the end of the column list.
     * 
     * @param column
     *            The column to add.
     */
    public void addColumn(Long column)
    {
        this.columList.add(column);
    }

    /**
     * Remove a column from the list.
     * 
     * @param column
     *            The column to remove.
     */
    public void removeColumn(Long column)
    {
        this.columList.remove(column);
    }

    /**
     * Clear the columm list.
     */
    public void clearColumns()
    {
        this.columList.clear();
    }

    /**
     * @return true if column list is empty.
     */
    public boolean isEmpty()
    {
        return columList.isEmpty();
    }

    /**
     * @return The order by parameter at a list format.
     */
    public List<Long> toList()
    {
        return this.columList;
    }

    /**
     * Return the order by parameter at JSON format.
     */
    public String toJson()
    {
        JSONWriter writer = new JSONWriter();
        return writer.write(this.toList());
    }

    /**
     * Return the order by parameter at JSON format with its variable name.
     */
    @Override
    public String toString()
    {
        return Constraint.ITEM_SORT + "=" + toJson();
    }

}
