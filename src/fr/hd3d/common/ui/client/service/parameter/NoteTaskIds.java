package fr.hd3d.common.ui.client.service.parameter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;


/**
 * 
 * @author HD3D
 */
public class NoteTaskIds extends ArrayList<Long> implements IUrlParameter
{
    /** Automatically generated serial version UID. */
    private static final long serialVersionUID = 8464584873846635254L;

    public NoteTaskIds()
    {
        super();
    }

    public NoteTaskIds(Collection<Long> ids)
    {
        super(ids);
    }

    public NoteTaskIds(Long... ids)
    {
        this.addAll(Arrays.asList(ids));
    }

    /**
     * Write constraint list as json list.
     */
    public String toJson()
    {
        StringBuffer json = new StringBuffer();

        json.append("[");
        for (Long id : this)
        {
            json.append(id);
            json.append(",");
        }
        if (this.size() > 0)
        {
            json.deleteCharAt(json.length() - 1);
        }
        json.append("]");

        return json.toString();
    }

    /**
     * @return This parameter at in a JSON String format.
     */
    @Override
    public String toString()
    {
        return fr.hd3d.common.client.ServicesURI.TASK_IDS + "=" + this.toJson();
    }

}
