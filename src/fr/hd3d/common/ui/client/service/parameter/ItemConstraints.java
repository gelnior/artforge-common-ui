package fr.hd3d.common.ui.client.service.parameter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import fr.hd3d.common.ui.client.modeldata.writer.IJsonWriter;
import fr.hd3d.common.ui.client.modeldata.writer.JsonWriterSingleton;


/**
 * Constraints is useful to write a multiple
 * 
 * @author HD3D
 */
public class ItemConstraints extends ArrayList<ItemConstraint> implements IUrlParameter
{
    /** Automatically generated serial version UID. */
    private static final long serialVersionUID = 8464584873846635254L;

    public ItemConstraints()
    {
        super();
    }

    public ItemConstraints(Collection<? extends ItemConstraint> constraints)
    {
        super(constraints);
    }

    /**
     * Write constraint list as json list.
     */
    public String toJson()
    {

        if (this.isEmpty())
            return "[]";

        if (this.size() == 1)
        {
            StringBuffer json = new StringBuffer();

            json.append("[");
            json.append(this.get(0).toJson());
            json.append("]");
            return json.toString();
        }

        Stack<Object> tmp = new Stack<Object>();

        Stack<ItemConstraint> stack = new Stack<ItemConstraint>();
        stack.addAll(this);

        tmp.push(stack.pop());
        while (!stack.empty())
        {
            tmp.push(makeMap(tmp.pop(), stack.pop().toMap()));
        }

        IJsonWriter writer = JsonWriterSingleton.getInstance();
        List<Object> ret = new ArrayList<Object>();
        ret.add(tmp.pop());
        return writer.write(ret);
    }

    public Map<String, Object> makeMap(Object first, Object second)
    {
        Map<String, Object> map = new java.util.HashMap<String, Object>();
        map.put("logic", "AND");
        List<Object> list = new ArrayList<Object>();
        list.add(first);
        list.add(second);
        map.put("filter", list);
        return map;
    }

    /**
     * @return This constraint in a JSON String format with the parameter name in front of it. <br>
     *         Ex : constraint=[{"type":"lt","column":"prix","value":10}, {"type":"eq","column":"size","value":10}]
     */
    @Override
    public String toString()
    {
        return fr.hd3d.common.client.ItemConstraint.ITEMCONSTRAINT + "=" + this.toJson();
    }

}
