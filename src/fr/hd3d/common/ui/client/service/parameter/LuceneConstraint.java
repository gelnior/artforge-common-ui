package fr.hd3d.common.ui.client.service.parameter;

import fr.hd3d.common.client.enums.EConstraintOperator;


/**
 * LuceneConstraint generates URL Lucene constraint parameter from its members (column, operator and values).
 * 
 * @author HD3D
 */
public class LuceneConstraint extends Constraint implements IUrlConstraint
{
    public LuceneConstraint(EConstraintOperator type, String column, Object memberLeft, Object memberRight)
    {
        super(type, column, memberLeft, memberRight);
    }

    public LuceneConstraint(EConstraintOperator operator, String column)
    {
        super(operator, column);
    }

    public LuceneConstraint(EConstraintOperator operator, String column, Object value)
    {
        super(operator, column, value);
    }

    /**
     * @return This constraint in a JSON String format with the parameter name in front of it. <br>
     *         Ex : luceneConstraint=[{"type":"lt","column":"prix","value":10}]
     */
    @Override
    public String toString()
    {
        return fr.hd3d.common.client.Constraint.LUCENE_CONSTRAINT + "=[" + this.toJson() + "]";
    }
}
