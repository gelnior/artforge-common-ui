package fr.hd3d.common.ui.client.service.parameter;

import fr.hd3d.common.client.enums.EConstraintOperator;


/**
 * Class for easy building in constraint parameter.
 * 
 * @author HD3D
 */
public class InConstraint extends Constraint
{
    /**
     * Constructor : set in as operator and <i>column</i> as column on which constraint apply.
     * 
     * @param column
     *            The column on which constraint apply.
     */
    public InConstraint(String column)
    {
        super(EConstraintOperator.in, column);
    }

    /**
     * Constructor : set in as operator, <i>column</i> as column on which constraint apply and value as the <i>value</i>
     * to use for filtering.
     * 
     * @param column
     *            The column on which constraint apply.
     * @param value
     *            The filtering value.
     */
    public InConstraint(String column, Object value)
    {
        super(EConstraintOperator.in, column, value);
    }
}
