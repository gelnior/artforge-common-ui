package fr.hd3d.common.ui.client.service.parameter;

import java.util.Date;

import fr.hd3d.common.client.Const;
import fr.hd3d.common.ui.client.modeldata.writer.IJsonWriter;
import fr.hd3d.common.ui.client.modeldata.writer.JsonWriterSingleton;


/**
 * Build an end date URL parameter like "endDate=<i>date</i>".
 * 
 * @author HD3D
 */
public class EndDateParameter implements IUrlParameter
{
    /** Date to set as parameter value. */
    private final Date endDate;

    /**
     * Constructor.
     * 
     * @param endDate
     *            Date to set as parameter value.
     */
    public EndDateParameter(final Date endDate)
    {
        this.endDate = new Date(endDate.getTime());
    }

    /**
     * Return parameter value as string.
     */
    public String toJson()
    {
        IJsonWriter writer = JsonWriterSingleton.getInstance();
        String date = writer.write(endDate);
        return date.substring(1, date.length() - 1);
    }

    /**
     * @return The string to set as parameter : EndDate=<i>EndDate</i>.
     */
    @Override
    public String toString()
    {
        return Const.ENDDATE + "=" + toJson();
    }
}
