package fr.hd3d.common.ui.client.service.parameter;

import com.extjs.gxt.ui.client.util.Params;

import fr.hd3d.common.client.Constraint;
import fr.hd3d.common.ui.client.modeldata.writer.JSONWriter;


/**
 * ScriptParams allows to add params to sevice script request. <br>
 * It is based on GXT Params object for its similitude.
 * 
 * @author HD3D
 */
public class ScriptParams extends Params implements IUrlParameter
{
    /**
     * Return the order by parameter at JSON format.
     */
    public String toJson()
    {
        JSONWriter writer = new JSONWriter();
        return writer.write(this.getMap());
    }

    /**
     * Return the order by parameter at JSON format with its variable name.
     */
    @Override
    public String toString()
    {
        return Constraint.PARAMS + "=" + toJson();
    }
}
