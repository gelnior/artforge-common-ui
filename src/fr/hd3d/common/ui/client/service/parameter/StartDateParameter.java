package fr.hd3d.common.ui.client.service.parameter;

import java.util.Date;

import fr.hd3d.common.client.Const;
import fr.hd3d.common.ui.client.modeldata.writer.IJsonWriter;
import fr.hd3d.common.ui.client.modeldata.writer.JsonWriterSingleton;


/**
 * Build a start date URL parameter like "startDate=<i>date</i>".
 * 
 * @author HD3D
 */
public class StartDateParameter implements IUrlParameter
{
    /** Date to set as parameter value. */
    private final Date startDate;

    /**
     * Constructor.
     * 
     * @param startDate
     *            Date to set as parameter value.
     */
    public StartDateParameter(final Date startDate)
    {
        this.startDate = new Date(startDate.getTime());
    }

    /**
     * Return parameter value as string.
     */
    public String toJson()
    {
        IJsonWriter writer = JsonWriterSingleton.getInstance();
        String date = writer.write(startDate);
        return date.substring(1, date.length() - 1);
    }

    /**
     * @return The string to set as parameter : startDate=<i>startDate</i>.
     */
    @Override
    public String toString()
    {
        return Const.STARTDATE + "=" + toJson() + "";
    }
}
