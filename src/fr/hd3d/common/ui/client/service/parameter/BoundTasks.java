package fr.hd3d.common.ui.client.service.parameter;

import fr.hd3d.common.client.Const;


public class BoundTasks implements IUrlParameter
{

    private final boolean enable;

    public BoundTasks(boolean enable)
    {
        this.enable = enable;
    }

    public String toJson()
    {
        return "";
    }

    @Override
    public String toString()
    {
        return Const.BOUNDTASKS + "=" + enable;
    }

}
