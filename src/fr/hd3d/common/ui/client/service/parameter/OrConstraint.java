package fr.hd3d.common.ui.client.service.parameter;

import fr.hd3d.common.client.enums.EConstraintLogicalOperator;


/**
 * Class for easy building OR logical constraint parameter.
 * 
 * @author HD3D
 */
public class OrConstraint extends LogicConstraint
{
    /**
     * Constructor : set OR as logical parameter.
     */
    public OrConstraint()
    {
        super(EConstraintLogicalOperator.OR);
    }

    public OrConstraint(IUrlConstraint leftMember, IUrlConstraint rightMember)
    {
        super(EConstraintLogicalOperator.OR, leftMember, rightMember);
    }
}
