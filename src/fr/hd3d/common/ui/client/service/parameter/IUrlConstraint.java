package fr.hd3d.common.ui.client.service.parameter;

import com.extjs.gxt.ui.client.core.FastMap;


/**
 * A constraint is a parameter that filters data and that can be render as a map.
 * 
 * @author HD3D
 */
public interface IUrlConstraint extends IUrlParameter
{
    public FastMap<Object> toMap();
}
