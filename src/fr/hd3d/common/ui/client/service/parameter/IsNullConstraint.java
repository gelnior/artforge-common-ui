package fr.hd3d.common.ui.client.service.parameter;

import fr.hd3d.common.client.enums.EConstraintOperator;


public class IsNullConstraint extends Constraint
{
    public IsNullConstraint(String column)
    {
        super(EConstraintOperator.isnull, column);
    }
}
