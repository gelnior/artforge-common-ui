package fr.hd3d.common.ui.client.service.parameter;

import com.extjs.gxt.ui.client.core.FastMap;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.modeldata.writer.IJsonWriter;
import fr.hd3d.common.ui.client.modeldata.writer.JsonWriterSingleton;


/**
 * Constraint generates URL constraint parameter from its members.
 * 
 * @author HD3D
 */
public class Constraint implements IUrlConstraint
{
    /** Constraint operator type. */
    private EConstraintOperator type;
    /** Column on which apply the operator. */
    private String column;
    /** Left member of the operator, main member when there is only one operator. */
    private Object leftMember;
    /** Right member of the operator. */
    private Object rightMember;

    /**
     * Constructor with no right member.
     * 
     * @param type
     *            Constraint operator type.
     * @param column
     *            Column on which apply the operator
     * @param memberLeft
     *            Left member of the operator, main member when there is only one operator.
     */
    public Constraint(EConstraintOperator type, String column, Object memberLeft)
    {
        super();
        this.type = type;
        this.column = column;
        this.leftMember = memberLeft;
        this.rightMember = null;
    }

    /**
     * Default constructor.
     * 
     * @param type
     *            Constraint operator type.
     * @param column
     *            Column on which apply the operator
     * @param memberLeft
     *            Left member of the operator, main member when there is only one operator.
     * @param memberRight
     *            Right member of the operator.
     */
    public Constraint(EConstraintOperator type, String column, Object memberLeft, Object memberRight)
    {
        super();
        this.type = type;
        this.column = column;
        this.leftMember = memberLeft;
        this.rightMember = memberRight;
    }

    /**
     * Constructor with no value comparison (for is null and is not null operators).
     * 
     * @param type
     *            Constraint operator type.
     * @param column
     *            Column on which apply the operator
     */
    public Constraint(EConstraintOperator type, String column)
    {
        this(type, column, null);
    }

    public Constraint()
    {}

    /** @return Constraint operator type. */
    public EConstraintOperator getType()
    {
        return type;
    }

    /**
     * Set constraint operator type.
     * 
     * @param type
     *            Constraint operator type.
     */
    public void setOperator(EConstraintOperator type)
    {
        this.type = type;
    }

    /** @return Column on which apply the operator. */
    public String getColumn()
    {
        return column;
    }

    /**
     * Set column on which apply the operator.
     * 
     * @param column
     *            Column on which apply the operator.
     */
    public void setColumn(String column)
    {
        this.column = column;
    }

    /**
     * @return Left member of the operator, main member when there is only one operator.
     */
    public Object getLeftMember()
    {
        return leftMember;
    }

    /**
     * Set left member of the operator, main member when there is only one operator.
     * 
     * @param leftMember
     *            Left member of the operator, main member when there is only one operator.
     */
    public void setLeftMember(Object leftMember)
    {
        this.leftMember = leftMember;
    }

    /**
     * @return Right member of the operator.
     */
    public Object getRightMember()
    {
        return rightMember;
    }

    /**
     * Right member of the operator.
     * 
     * @param rightMember
     *            Right member of the operator.
     */
    public void setRightMember(Object rightMember)
    {
        this.rightMember = rightMember;
    }

    /**
     * @return This constraint object in a map form.
     */
    public FastMap<Object> toMap()
    {
        FastMap<Object> map = new FastMap<Object>();

        map.put(fr.hd3d.common.client.Constraint.TYPE_MAP_FIELD, this.type.toString());
        map.put(fr.hd3d.common.client.Constraint.COLUMN_MAP_FIELD, this.column);

        if (this.type == EConstraintOperator.btw)
        {
            FastMap<Object> valueMap = new FastMap<Object>();
            valueMap.put(fr.hd3d.common.client.Constraint.START_MAP_FIELD, this.leftMember);
            valueMap.put(fr.hd3d.common.client.Constraint.END_MAP_FIELD, this.rightMember);

            map.put(fr.hd3d.common.client.Constraint.VALUE_MAP_FIELD, valueMap);
        }
        else if (this.type != EConstraintOperator.isnull && this.type != EConstraintOperator.isnotnull)
        {
            map.put(fr.hd3d.common.client.Constraint.VALUE_MAP_FIELD, this.leftMember);
        }

        return map;
    }

    /**
     * @return This constraint in a JSON String format.<br>
     *         Ex : {"type":"lt","column":"prix","value":10}
     */
    public String toJson()
    {
        IJsonWriter writer = JsonWriterSingleton.getInstance();
        return writer.write(this.toMap());
    }

    /**
     * @return This constraint in a JSON String format with the parameter name in front of it. <br>
     *         Ex : constraint=[{"type":"lt","column":"prix","value":10}]
     */
    @Override
    public String toString()
    {
        return fr.hd3d.common.client.Constraint.CONSTRAINT + "=[" + this.toJson() + "]";
    }

}
