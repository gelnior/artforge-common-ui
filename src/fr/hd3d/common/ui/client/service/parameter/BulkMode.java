package fr.hd3d.common.ui.client.service.parameter;

import fr.hd3d.common.client.Const;


/**
 * Build a bulk mode URL parameter like "mode=bulk".
 * 
 * @author HD3D
 */
public class BulkMode implements IUrlParameter
{

    /**
     * @return The bulk value ("mode").
     * 
     * @see fr.hd3d.common.ui.client.service.parameter.IUrlParameter#toJson()
     */
    public String toJson()
    {
        return Const.BULK;
    }

    /**
     * @return The URL parameter ("bulk=mode").
     */
    @Override
    public String toString()
    {
        return Const.MODE + "=" + toJson();
    }
}
