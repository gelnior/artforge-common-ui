package fr.hd3d.common.ui.client.service.parameter;

import java.util.ArrayList;
import java.util.List;

import fr.hd3d.common.client.Constraint;
import fr.hd3d.common.ui.client.modeldata.writer.JSONWriter;


/**
 * OrderBy generates URL order by parameter.
 * 
 * @author HD3D
 */
public class OrderBy implements IUrlParameter
{
    /** Columns to use for order by. */
    protected List<String> columnList = new ArrayList<String>();

    /**
     * Default constructor, add one column to the column list.
     * 
     * @param column
     *            The column on which the ordering will be done.
     */
    public OrderBy(String column)
    {
        super();

        this.columnList.add(column);
    }

    /**
     * Second constructor : sets a list of columns for the order by.
     * 
     * @param columnList
     */
    public OrderBy(List<String> columnList)
    {
        super();

        this.columnList = columnList;
    }

    /**
     * Add a column at the end of the column list.
     * 
     * @param column
     *            The column to add.
     */
    public void addColumn(String column)
    {
        this.columnList.add(column);
    }

    /**
     * Add a list of columns at the end of the column list.
     * 
     * @param columns
     *            The columns to add.
     */
    public void addColumns(List<String> columns)
    {
        this.columnList.addAll(columns);
    }

    /**
     * Remove a column from the list.
     * 
     * @param column
     *            The column to remove.
     */
    public void removeColumn(String column)
    {
        this.columnList.remove(column);
    }

    /**
     * Clear the columm list.
     */
    public void clearColumns()
    {
        this.columnList.clear();
    }

    /**
     * @return The order by parameter at a list format.
     */
    public List<String> toList()
    {
        return this.columnList;
    }

    /**
     * Return the order by parameter at JSON format.
     */
    public String toJson()
    {
        JSONWriter writer = new JSONWriter();
        return writer.write(this.toList());
    }

    /**
     * Return the order by parameter at JSON format with its variable name.
     */
    @Override
    public String toString()
    {
        return Constraint.ORDERBY + "=" + toJson();
    }

}
