package fr.hd3d.common.ui.client.service.parameter;

import java.util.List;

import com.extjs.gxt.ui.client.util.Util;


/**
 * Build a string of URL parameters from IUrlParameter objects.
 * 
 * @author HD3D
 */
public class ParameterBuilder
{

    /**
     * @param parameters
     *            Parameters that should appear in the URL parameter string.
     * @return A string of URL parameters from <i>parameters</i>.
     */
    public static String parametersToString(List<IUrlParameter> parameters)
    {
        StringBuilder parametersString = new StringBuilder();
        for (IUrlParameter parameter : parameters)
        {
            if (parametersString.length() == 0)
            {
                parametersString.append("?" + parameter.toString());
            }
            else
            {
                parametersString.append("&" + parameter.toString());
            }
        }

        return parametersString.toString();
    }

    /**
     * @param parameter
     *            Parameters that should appear in the URL parameter string.
     * @return A string of URL parameters from <i>parameters</i> preceded by a '?'. <br>
     *         Warning : Constraint list should be set in a Constraints parameter to be efficient. If a simple
     *         constraint array list is given in parameter a constraint parameter will be built for each constraint and
     *         only the first one will be used by services.
     */
    public static String parameterToString(IUrlParameter parameter)
    {
        if (parameter != null)
        {
            String param = parameter.toString();
            if (!Util.isEmptyString(param))
            {
                return "?" + param;
            }
        }
        return "";
    }
}
