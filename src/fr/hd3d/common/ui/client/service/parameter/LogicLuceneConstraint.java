package fr.hd3d.common.ui.client.service.parameter;

import fr.hd3d.common.client.enums.EConstraintLogicalOperator;


public class LogicLuceneConstraint extends LogicConstraint
{

    public LogicLuceneConstraint(EConstraintLogicalOperator operator)
    {
        super(operator);
    }

    @Override
    public String toString()
    {
        return fr.hd3d.common.client.LuceneConstraint.LUCENECONSTRAINT + "=[" + this.toJson() + "]";
    }

}
