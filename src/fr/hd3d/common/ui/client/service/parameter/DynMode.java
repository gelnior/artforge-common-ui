package fr.hd3d.common.ui.client.service.parameter;

import fr.hd3d.common.client.Const;


/**
 * Build a dynamic attribute mode URL parameter like <code>dyn=true</code>.
 * 
 * @author HD3D
 */
public class DynMode implements IUrlParameter
{

    /**
     * @return The dynamic attribute mode on value ("true").
     * 
     * @see fr.hd3d.common.ui.client.service.parameter.IUrlParameter#toJson()
     */
    public String toJson()
    {
        return "true";
    }

    /**
     * @return The URL parameter <code>dyn=true</code>.
     */
    @Override
    public String toString()
    {
        return Const.DYN + "=" + toJson();
    }
}
