package fr.hd3d.common.ui.client.service.parameter;

import fr.hd3d.common.client.enums.EConstraintOperator;


/**
 * Class for easy building of equal constraint parameter.
 * 
 * @author HD3D
 */
public class EqConstraint extends Constraint
{
    /**
     * Constructor : set equal as operator and <i>column</i> as column on which constraint apply.
     * 
     * @param column
     *            The column on which constraint apply.
     */
    public EqConstraint(String column)
    {
        super(EConstraintOperator.eq, column);
    }

    /**
     * Constructor : set equal as operator, <i>column</i> as column on which constraint apply and value as the
     * <i>value</i> to use for filtering.
     * 
     * @param column
     *            The column on which constraint apply.
     * @param value
     *            The filtering value.
     */
    public EqConstraint(String column, Object value)
    {
        super(EConstraintOperator.eq, column, value);
    }
}
