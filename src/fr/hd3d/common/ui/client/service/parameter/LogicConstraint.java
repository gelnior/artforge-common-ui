package fr.hd3d.common.ui.client.service.parameter;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.core.FastMap;

import fr.hd3d.common.client.enums.EConstraintLogicalOperator;
import fr.hd3d.common.ui.client.modeldata.writer.IJsonWriter;
import fr.hd3d.common.ui.client.modeldata.writer.JsonWriterSingleton;


public class LogicConstraint implements IUrlConstraint
{
    protected EConstraintLogicalOperator operator;

    protected IUrlConstraint leftMember;
    protected IUrlConstraint rightMember;

    public LogicConstraint()
    {}

    public LogicConstraint(EConstraintLogicalOperator operator, IUrlConstraint leftMember, IUrlConstraint rightMember)
    {
        this.operator = operator;
        this.leftMember = leftMember;
        this.rightMember = rightMember;
    }

    public LogicConstraint(EConstraintLogicalOperator operator)
    {
        this.operator = operator;
    }

    public EConstraintLogicalOperator getOperator()
    {
        return operator;
    }

    public void setOperator(EConstraintLogicalOperator operator)
    {
        this.operator = operator;
    }

    public IUrlConstraint getLeftMember()
    {
        return leftMember;
    }

    public void setLeftMember(IUrlConstraint leftMember)
    {
        this.leftMember = leftMember;
    }

    public IUrlConstraint getRightMember()
    {
        return rightMember;
    }

    public void setRightMember(IUrlConstraint rightMember)
    {
        this.rightMember = rightMember;
    }

    public FastMap<Object> toMap()
    {
        FastMap<Object> logicConstraintMap = new FastMap<Object>();
        List<FastMap<Object>> filters = new ArrayList<FastMap<Object>>();

        filters.add(leftMember.toMap());

        if (operator != EConstraintLogicalOperator.NOT && rightMember != null)
        {
            filters.add(rightMember.toMap());
        }

        logicConstraintMap.put(fr.hd3d.common.client.LogicConstraint.LOGIC, operator.toString());
        logicConstraintMap.put(fr.hd3d.common.client.LogicConstraint.FILTER, filters);

        return logicConstraintMap;
    }

    public String toJson()
    {
        if (leftMember != null)
        {
            IJsonWriter writer = JsonWriterSingleton.getInstance();
            return writer.write(this.toMap());
        }
        else
        {
            return "";
        }
    }

    @Override
    public String toString()
    {
        return fr.hd3d.common.client.Constraint.CONSTRAINT + "=[" + this.toJson() + "]";
    }
}
