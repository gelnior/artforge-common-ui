package fr.hd3d.common.ui.client.service.parameter;

import fr.hd3d.common.client.enums.EConstraintLogicalOperator;


/**
 * Class for easy building AND logical constraint parameter.
 * 
 * @author HD3D
 */
public class AndConstraint extends LogicConstraint
{
    /**
     * Constructor : set AND as logical operator.
     */
    public AndConstraint()
    {
        super(EConstraintLogicalOperator.AND);
    }

    /**
     * Constructor : set AND as logical operator and left member and right member as member..
     */
    public AndConstraint(IUrlConstraint leftMember, IUrlConstraint rightMember)
    {
        super(EConstraintLogicalOperator.AND, leftMember, rightMember);
    }
}
