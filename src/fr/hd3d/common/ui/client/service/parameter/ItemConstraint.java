package fr.hd3d.common.ui.client.service.parameter;

import com.extjs.gxt.ui.client.core.FastMap;

import fr.hd3d.common.client.enums.EItemFilterOperator;
import fr.hd3d.common.ui.client.modeldata.writer.JSONWriter;


/**
 * Constraint generates URL constraint parameter from its members.
 * 
 * @author HD3D
 */
public class ItemConstraint implements IUrlConstraint
{
    /** Constraint operator type. */
    private EItemFilterOperator type;
    /** Column on which apply the operator. */
    private Long itemId;
    /** Left member of the operator, main member when there is only one operator. */
    private Object leftMember;
    /** Right member of the operator. */
    private Object rightMember;

    private String field;

    // /** Constraint operator type key needed for the map form. */
    // private final static String TYPE_MAP_FIELD = "type";
    // /** Column key needed for the map form. */
    // private final static String ITEMID_MAP_FIELD = "itemId";
    // /** Value field key (URL constraint parameter look for operator members in the value field). */
    // private final static String VALUE_MAP_FIELD = "value";
    // /** Left member key needed for the map form (main member key when there is only one operator). */
    // private final static String START_MAP_FIELD = "start";
    // /** Right member key needed for the map form. */
    // private final static String END_MAP_FIELD = "end";
    // private final static String FIELD_FIELD = "field";

    /**
     * Constructor with no right member.
     * 
     * @param type
     *            Constraint operator type.
     * @param column
     *            Column on which apply the operator
     * @param memberLeft
     *            Left member of the operator, main member when there is only one operator.
     */
    public ItemConstraint(EItemFilterOperator type, Long column, Object memberLeft)
    {
        super();
        this.type = type;
        this.itemId = column;
        this.leftMember = memberLeft;
        this.rightMember = null;
    }

    /**
     * Default constructor.
     * 
     * @param type
     *            Constraint operator type.
     * @param column
     *            Column on which apply the operator
     * @param memberLeft
     *            Left member of the operator, main member when there is only one operator.
     * @param memberRight
     *            Right member of the operator.
     */
    public ItemConstraint(EItemFilterOperator type, Long column, Object memberLeft, Object memberRight)
    {
        super();
        this.type = type;
        this.itemId = column;
        this.leftMember = memberLeft;
        this.rightMember = memberRight;
    }

    /**
     * Constructor with no value comparison (for is null and is not null operators).
     * 
     * @param type
     *            Constraint operator type.
     * @param column
     *            Column on which apply the operator
     */
    public ItemConstraint(EItemFilterOperator type, Long column)
    {
        this(type, column, null);
    }

    public ItemConstraint()
    {}

    /** @return Constraint operator type. */
    public EItemFilterOperator getType()
    {
        return type;
    }

    /**
     * Set constraint operator type.
     * 
     * @param type
     *            Constraint operator type.
     */
    public void setOperator(EItemFilterOperator type)
    {
        this.type = type;
    }

    /** @return Column on which apply the operator. */
    public Long getItemId()
    {
        return itemId;
    }

    /**
     * Set column on which apply the operator.
     * 
     * @param column
     *            Column on which apply the operator.
     */
    public void setItemId(Long column)
    {
        this.itemId = column;
    }

    /**
     * @return Left member of the operator, main member when there is only one operator.
     */
    public Object getLeftMember()
    {
        return leftMember;
    }

    /**
     * Set left member of the operator, main member when there is only one operator.
     * 
     * @param leftMember
     *            Left member of the operator, main member when there is only one operator.
     */
    public void setLeftMember(Object leftMember)
    {
        this.leftMember = leftMember;
    }

    /**
     * @return Right member of the operator.
     */
    public Object getRightMember()
    {
        return rightMember;
    }

    /**
     * Right member of the operator.
     * 
     * @param rightMember
     *            Right member of the operator.
     */
    public void setRightMember(Object rightMember)
    {
        this.rightMember = rightMember;
    }

    public String getField()
    {
        return field;
    }

    public void setField(String field)
    {
        this.field = field;
    }

    /**
     * @return This constraint object in a map form.
     */
    public FastMap<Object> toMap()
    {
        FastMap<Object> map = new FastMap<Object>();

        map.put(fr.hd3d.common.client.ItemConstraint.TYPE_MAP_FIELD, this.type.toString());
        map.put(fr.hd3d.common.client.ItemConstraint.ITEMID_MAP_FIELD, this.itemId);
        map.put(fr.hd3d.common.client.ItemConstraint.FIELD_FIELD, this.field);

        if (this.type == EItemFilterOperator.btw)
        {
            FastMap<Object> valueMap = new FastMap<Object>();
            valueMap.put(fr.hd3d.common.client.ItemConstraint.START_MAP_FIELD, this.leftMember);
            valueMap.put(fr.hd3d.common.client.ItemConstraint.END_MAP_FIELD, this.rightMember);

            map.put(fr.hd3d.common.client.ItemConstraint.VALUE_MAP_FIELD, valueMap);
        }
        else if (this.type != EItemFilterOperator.isnull && this.type != EItemFilterOperator.isnotnull)
        {
            map.put(fr.hd3d.common.client.ItemConstraint.VALUE_MAP_FIELD, this.leftMember);
        }

        return map;
    }

    /**
     * @return This constraint in a JSON String format.<br>
     *         Ex : {"type":"lt","column":"prix","value":10}
     */
    public String toJson()
    {
        JSONWriter writer = new JSONWriter();
        return writer.write(this.toMap());
    }

    /**
     * @return This constraint in a JSON String format with the parameter name in front of it. <br>
     *         Ex : constraint=[{"type":"lt","column":"prix","value":10}]
     */
    @Override
    public String toString()
    {
        return fr.hd3d.common.client.ItemConstraint.ITEMCONSTRAINT + "=[" + this.toJson() + "]";
    }

}
