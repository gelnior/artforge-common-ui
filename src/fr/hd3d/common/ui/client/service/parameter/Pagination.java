package fr.hd3d.common.ui.client.service.parameter;

import com.extjs.gxt.ui.client.core.FastMap;

import fr.hd3d.common.client.Constraint;
import fr.hd3d.common.ui.client.modeldata.writer.JSONWriter;


/**
 * Pagination generates URL pagination parameter from its members.
 * 
 * @author HD3D
 */
public class Pagination implements IUrlParameter
{
    /** Index of first value. */
    private Long first;
    /** Quantity of data to return. */
    private Long quantity;

    /**
     * Default constructor.
     * 
     * @param first
     *            Index of first value.
     * @param quantity
     *            Quantity of data to return.
     */
    public Pagination(Long first, Long quantity)
    {
        super();
        this.first = first;
        this.quantity = quantity;
    }

    /** @return Index of first value. */
    public Long getFirst()
    {
        return first;
    }

    /**
     * set index of first value.
     * 
     * @param first
     *            Index of first value.
     */
    public void setFirst(Long first)
    {
        this.first = first;
    }

    /** @return Quantity of data to return. */
    public Long getQuantity()
    {
        return quantity;
    }

    /**
     * @param quantity
     *            Quantity of data to return.
     */
    public void setQuantity(Long quantity)
    {
        this.quantity = quantity;
    }

    /**
     * @return The map shape of the pagingation parameter.
     */
    public FastMap<Object> toMap()
    {
        FastMap<Object> map = new FastMap<Object>();

        map.put(Constraint.FIRST_MAP_FIELD, this.first);
        map.put(Constraint.QUANTITY_MAP_FIELD, this.quantity);

        return map;
    }

    /**
     * @return This constraint in a JSON String format.<br>
     *         Ex : {"first":10,"quantity":30}
     */
    public String toJson()
    {
        // JSONObject obj = JsonEncoder.encode(this.toMap());
        // return obj.toString().replaceAll(" ", "");

        JSONWriter writer = new JSONWriter();
        return writer.write(this.toMap());

    }

    /**
     * @return This constraint in a JSON String format with the parameter name in front of it. <br>
     *         Ex : pagination={"first":10,"quantity":30}
     */
    @Override
    public String toString()
    {
        return Constraint.PAGINATION + "=" + toJson();
    }
}
