package fr.hd3d.common.ui.client.service.parameter;

import fr.hd3d.common.client.enums.EConstraintOperator;


public class IsNotNullConstraint extends Constraint
{
    public IsNotNullConstraint(String column)
    {
        super(EConstraintOperator.isnotnull, column);
    }
}
