package fr.hd3d.common.ui.client.service.parameter;

import fr.hd3d.common.client.Const;


/**
 * TagsMode generates URL tags parameter.
 * 
 * @author HD3D
 */
public class TagsMode implements IUrlParameter
{
    /**
     * @return The tag mode on value ("true").
     * 
     * @see fr.hd3d.common.ui.client.service.parameter.IUrlParameter#toJson()
     */
    public String toJson()
    {
        return "true";
    }

    /**
     * @return The URL parameter ("tags=true").
     */
    @Override
    public String toString()
    {
        return Const.TAGS + "=" + toJson();
    }
}
