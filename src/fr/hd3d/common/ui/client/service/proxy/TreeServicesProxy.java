package fr.hd3d.common.ui.client.service.proxy;

import java.util.List;

import org.restlet.client.data.Method;

import com.extjs.gxt.ui.client.data.DataProxy;
import com.extjs.gxt.ui.client.data.DataReader;
import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.parameter.ParameterBuilder;


/**
 * TreeServicesProxy can be used by tree loader to access data.
 * 
 * @author HD3D
 * 
 * @param <C>
 *            ModelData handled by the tree using the proxy.
 */
public class TreeServicesProxy<C extends Hd3dModelData> extends BaseProxy<C> implements DataProxy<List<C>>
{

    /** Reader needed to parse result. */
    protected IReader<C> reader;

    /**
     * Default constructor.
     */
    public TreeServicesProxy(IReader<C> reader)
    {
        super();
        this.reader = reader;
    }

    /**
     * @return reader used to parse retrieved data.
     */
    public IReader<C> getReader()
    {
        return reader;
    }

    /**
     * Set reader used to parse retrieved data.
     * 
     * @param reader
     *            The reader to set.
     */
    public void setReader(IReader<C> reader)
    {
        this.reader = reader;
    }

    /**
     * Retrieved data from parameterized path with adding pagination parameter. Pagination parameter is build with
     * configuration info given by <i>loadConfig</i>
     */
    public void load(DataReader<List<C>> reader, Object loadConfig, AsyncCallback<List<C>> callback)
    {
        if (this.path == null)
        {
            this.path = this.reader.newModelInstance().getPath();
        }
        String parameterPath = this.path + ParameterBuilder.parametersToString(parameters);

        IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
        requestHandler.handleRequest(Method.GET, parameterPath, null, new TreeServicesProxyCallback<C>(this.reader,
                callback));

    }

}
