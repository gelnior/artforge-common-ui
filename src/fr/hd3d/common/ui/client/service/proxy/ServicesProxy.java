package fr.hd3d.common.ui.client.service.proxy;

import org.restlet.client.data.Method;

import com.extjs.gxt.ui.client.data.DataProxy;
import com.extjs.gxt.ui.client.data.DataReader;
import com.extjs.gxt.ui.client.data.ListLoadConfig;
import com.extjs.gxt.ui.client.data.ListLoadResult;
import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.parameter.ParameterBuilder;


/**
 * Acts as an interface between web services and GXT paging widgets. It retrieves data from server applying parameters
 * set by user.
 * 
 * @author HD3D
 * 
 * @param <C>
 *            Model data class to cache.
 */
public class ServicesProxy<C extends Hd3dModelData> extends BaseProxy<C> implements DataProxy<ListLoadResult<C>>
{
    /** Reader needed to parse result. */
    protected IReader<C> reader;

    /**
     * Default constructor.
     */
    public ServicesProxy(IReader<C> reader)
    {
        super(null);
        this.reader = reader;
    }

    /**
     * @return reader used to parse retrieved data.
     */
    public IReader<C> getReader()
    {
        return reader;
    }

    /**
     * Set reader used to parse retrieved data.
     * 
     * @param reader
     *            The reader to set.
     */
    public void setReader(IReader<C> reader)
    {
        this.reader = reader;
    }

    /**
     * Retrieved data from parameterized path with adding pagination parameter. Pagination parameter is build with
     * configuration info given by <i>loadConfig</i>.
     */
    public void load(DataReader<ListLoadResult<C>> reader, Object loadConfig, AsyncCallback<ListLoadResult<C>> callback)
    {
        ListLoadConfig config = (ListLoadConfig) loadConfig;

        this.applySort(config);
        if (this.path == null && this.reader != null)
        {
            this.path = this.reader.newModelInstance().getPath();
        }
        String parameterPath = this.path + ParameterBuilder.parametersToString(parameters);

        IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
        requestHandler.handleRequest(Method.GET, parameterPath, null, new ServicesProxyCallback<C>(this.reader,
                callback));

        this.removeSort();
    }
}
