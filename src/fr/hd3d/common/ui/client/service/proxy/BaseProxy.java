package fr.hd3d.common.ui.client.service.proxy;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.data.ListLoadConfig;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.service.parameter.IUrlParameter;
import fr.hd3d.common.ui.client.service.parameter.OrderBy;


/**
 * Base class to define service proxy. it provides default utils to handle URL used to connect to services.
 * 
 * @author HD3D
 * 
 * @param <C>
 *            Class type of retrieved data.
 */
public abstract class BaseProxy<C extends Hd3dModelData>
{
    /** Path from where data are retrieved. */
    protected String path;
    /** The parameters to set on the path from where data are retrieved. */
    protected List<IUrlParameter> parameters = new ArrayList<IUrlParameter>();
    /** Order by parameter to apply. */
    protected OrderBy orderBy = new OrderBy("");

    /**
     * Default constructor (set path to null).
     */
    public BaseProxy()
    {
        this.path = null;
    }

    /**
     * Constructor with path.
     * 
     * @param path
     *            The services path to get data.
     */
    public BaseProxy(String path)
    {
        this.path = path;
    }

    /** @return Path from where data are retrieved. */
    public String getPath()
    {
        return path;
    }

    /**
     * Set path from where data are retrieved.
     * 
     * @param path
     *            Path from where data are retrieved.
     */
    public void setPath(String path)
    {
        this.path = path;
    }

    /**
     * @return Parameters set on actual URL.
     */
    public List<IUrlParameter> getParameters()
    {
        return this.parameters;
    }

    /**
     * Clear URL parameters set on this proxy.
     */
    public void clearParameters()
    {
        this.parameters.clear();
    }

    /**
     * Add an URL parameter to the URL parameter list set for this proxy.
     * 
     * @param parameter
     *            The URL parameter to add.
     */
    public void addParameter(IUrlParameter parameter)
    {
        this.parameters.add(parameter);
    }

    /**
     * @param parameters
     *            A list of parameter to apply on proxy URL.
     */
    public void addParameters(List<IUrlParameter> parameters)
    {
        this.parameters.addAll(parameters);
    }

    /** Return order by parameter used to remotely sort data. */
    public OrderBy getOrderBy()
    {
        return this.orderBy;
    }

    /**
     * Remove <i>parameter</i> from parameters applied for data retrieving.
     * 
     * @param parameter
     *            The parameter to remove.
     */
    public void removeParameter(IUrlParameter parameter)
    {
        this.parameters.remove(parameter);
    }

    /**
     * @param parameter
     *            The parameter to test.
     * 
     * @return True if the parameter is set on the proxy.
     */
    public boolean hasParameter(IUrlParameter parameter)
    {
        return this.parameters.contains(parameter);
    }

    /**
     * Build order by parameter depending on config data to remotely sort retrieved data. If order by parameter has been
     * set, nothing is done.
     * 
     * @param config
     */
    protected void applySort(ListLoadConfig config)
    {
        boolean isOrderBy = false;
        for (IUrlParameter parameter : this.getParameters())
        {
            if (parameter instanceof OrderBy)
            {
                isOrderBy = true;
                continue;
            }
        }

        if (!isOrderBy && config.getSortField() != null && config.getSortField().length() > 0)
        {
            String columnName = "";
            if (config.getSortDir() == SortDir.DESC)
            {
                columnName += "-";
            }
            columnName += config.getSortField();

            orderBy.clearColumns();
            orderBy.addColumn(columnName);

            if (!this.parameters.contains(orderBy))
            {
                this.parameters.add(orderBy);
            }
        }
    }

    /**
     * Remove order by data to the parameter list. By this way sorting does not interfere in external parameter
     * handling.
     */
    protected void removeSort()
    {
        if (this.parameters.contains(orderBy))
        {
            this.parameters.remove(orderBy);
        }
    }
}
