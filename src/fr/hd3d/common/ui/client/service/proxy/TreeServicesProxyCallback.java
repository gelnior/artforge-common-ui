package fr.hd3d.common.ui.client.service.proxy;

import java.util.List;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.data.ListLoadResult;
import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;


/**
 * Callback needed by tree services proxy to handle request response it sends to server (it parses retrieved data).
 * 
 * @author HD3D
 * 
 * @param <C>
 *            Retrieved object class.
 */
public class TreeServicesProxyCallback<C extends Hd3dModelData> extends BaseCallback
{
    /** Reader needed to parse data retrieved. */
    protected IReader<C> reader;
    /** The GXT callback. */
    protected AsyncCallback<List<C>> callback;
    /** Pagination offset of retrieved value. */
    protected int offset;

    /**
     * Default constructor.
     * 
     * @param reader
     *            Reader needed to parse data retrieved.
     * @param callback
     *            The GXT callback.
     */
    public TreeServicesProxyCallback(IReader<C> reader, AsyncCallback<List<C>> callback)
    {
        super();
        this.reader = reader;
        this.callback = callback;
    }

    @Override
    public void onSuccess(Request request, Response response)
    {
        if (response.getStatus().isSuccess())
        {
            try
            {
                String text = response.getEntity().getText();
                if (reader != null)
                {
                    ListLoadResult<C> result = reader.read(text);
                    callback.onSuccess(result.getData());
                }
                else
                {
                    callback.onFailure(new Exception());
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
                callback.onFailure(e);
            }
        }
        else
        {
            callback.onFailure(new Exception());
        }
    }
}
