package fr.hd3d.common.ui.client.service.proxy;

import org.restlet.client.data.Method;

import com.extjs.gxt.ui.client.data.DataProxy;
import com.extjs.gxt.ui.client.data.DataReader;
import com.extjs.gxt.ui.client.data.PagingLoadConfig;
import com.extjs.gxt.ui.client.data.PagingLoadResult;
import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.parameter.Pagination;
import fr.hd3d.common.ui.client.service.parameter.ParameterBuilder;


/**
 * Acts as an interface between web services and GXT paging widgets. It retrieves data from server applying pagination
 * parameter sets by user via the GXT widget.
 * 
 * @author HD3D
 * 
 * @param <C>
 *            Model data class to cache.
 */
public class ServicesPagingProxy<C extends Hd3dModelData> extends BaseProxy<C> implements
        DataProxy<PagingLoadResult<C>>
{
    /**
     * Retrieved data from parameterized path with adding pagination parameter. Pagination parameter is build with
     * configuration info given by <i>loadConfig</i>
     */
    public void load(DataReader<PagingLoadResult<C>> reader, Object loadConfig,
            AsyncCallback<PagingLoadResult<C>> callback)
    {
        PagingLoadConfig config = (PagingLoadConfig) loadConfig;
        Pagination pagination = new Pagination(Long.valueOf(config.getOffset()), Long.valueOf(config.getLimit()));

        this.applySort(config);
        this.parameters.add(pagination);

        String paginatedPath = path + ParameterBuilder.parametersToString(parameters);

        this.sendRequest(paginatedPath, reader, callback, config);

        this.parameters.remove(pagination);

        this.removeSort();
    }

    protected void sendRequest(String paginatedPath, DataReader<PagingLoadResult<C>> reader,
            AsyncCallback<PagingLoadResult<C>> callback, PagingLoadConfig config)
    {
        IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
        requestHandler.handleRequest(Method.GET, paginatedPath, null, new ServicesPagingProxyCallback<C>(reader,
                callback, config.getOffset()));
    }

}
