package fr.hd3d.common.ui.client.service.proxy;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.data.DataReader;
import com.extjs.gxt.ui.client.data.PagingLoadResult;
import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;


/**
 * Callback needed by Services paging proxy to handle request response it sends to server. It parses retrieved data and
 * sets paging informations for GXT paging proxy.
 * 
 * @author HD3D
 * 
 * @param <C>
 *            Retrieved object class.
 */
public class ServicesPagingProxyCallback<C extends Hd3dModelData> extends BaseCallback
{
    /** Reader needed to parse data retrieved. */
    protected DataReader<PagingLoadResult<C>> reader;
    /** The GXT paging callback. */
    protected AsyncCallback<PagingLoadResult<C>> callback;
    /** Pagination offset of retrieved value. */
    protected int offset;

    /**
     * Default constructor.
     * 
     * @param reader
     *            Reader needed to parse data retrieved.
     * @param callback
     *            The GXT paging callback.
     * @param offset
     *            Pagination offset of retrieved value.
     */
    public ServicesPagingProxyCallback(DataReader<PagingLoadResult<C>> reader,
            AsyncCallback<PagingLoadResult<C>> callback, int offset)
    {
        super();
        this.reader = reader;
        this.callback = callback;
        this.offset = offset;
    }

    @Override
    public void onSuccess(Request request, Response response)
    {
        try
        {
            if (response.getEntity() != null)
            {
                String text = response.getEntity().getText();
                if (reader != null)
                {
                    PagingLoadResult<C> paging = reader.read(null, text);
                    paging.setOffset(offset);

                    callback.onSuccess(paging);
                }
                else
                {
                    callback.onFailure(new Exception());
                }
            }
            else
            {
                Logger.log("Entity is empty.", new Exception());
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            callback.onFailure(e);
        }
    }

    /**
     * When the request has been refused because the user does not have the permission, it forwards the NO_PERMISSION
     * error.
     */
    @Override
    protected void onPermissionError()
    {
        super.onPermissionError();
        callback.onFailure(new Exception());
    }

    /**
     * When the request has been refused because the user is not authenticated, it forwards the NOT_AUTHENTIFIED error.
     */
    @Override
    protected void onAuthenticationError()
    {
        super.onAuthenticationError();
        callback.onFailure(new Exception());
    }

    /**
     * When the request has been refused because the server encountered a internal error, it forwards the SERVER_ERROR
     * error.
     */
    @Override
    protected void onServerError(Request request, Response response)
    {
        super.onServerError(request, response);
        callback.onFailure(new Exception());
    }

    /**
     * When the request has been refuses because the service has not been found, it forwards the NO_SERVICE_CONNECTION
     * error.
     */
    @Override
    protected void onNoServiceError()
    {
        super.onNoServiceError();
        callback.onFailure(new Exception());
    }
}
