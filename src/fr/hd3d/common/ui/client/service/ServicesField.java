package fr.hd3d.common.ui.client.service;

import com.extjs.gxt.ui.client.core.FastMap;

import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;


/**
 * This class contains map that gives translation for each field of the HD3D model data.
 * 
 * @author HD3D
 */
public class ServicesField
{
    /** The map containing translations. */
    private static FieldNameMap map;
    /** The map containing translations for many to many keys used for filtering. */
    private static FastMap<String> manyToManyMap;

    /**
     * Initialize map with translations. Translation are pure view process. The initialization is not automatically done
     * to not disturb testing.
     */
    public static void initFields()
    {
        map = new FieldNameMap();
        manyToManyMap = new FastMap<String>();

        manyToManyMap.put(PersonModelData.PROJECT_IDS_FIELD, "resourceGroups.projects");
    }

    /**
     * @param fieldName
     *            The field name to translate in a human readable way.
     * @return translated and readable name for field given in parameter.
     */
    public static String getHumanName(String fieldName)
    {
        if (map != null && map.containsKey(fieldName))
        {
            return map.get(fieldName);
        }
        else
        {
            return null;
        }
    }

    public static String getManyToManyQueryName(String name)
    {
        String queryName = manyToManyMap.get(name);
        if (queryName == null)
        {
            return name.substring(0, name.length() - 3) + "s";
        }
        else
        {
            return name;
        }
    }
}
