package fr.hd3d.common.ui.client.service;

import java.util.HashMap;

import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.modeldata.reader.ApprovalNoteReader;
import fr.hd3d.common.ui.client.modeldata.reader.Hd3dListJsonReader;

public class Hd3dClassReadersFactory
{
    private final static HashMap<String, Hd3dListJsonReader<?>> readers = new HashMap<String, Hd3dListJsonReader<?>>();
    
    public static void initClass()
    {
        readers.put(ApprovalNoteModelData.CLASS_NAME, new ApprovalNoteReader());
    }
    
    public static Hd3dListJsonReader<?> getReader(String className)
    {
        if( readers.containsKey(className) )
        {
            return readers.get(className);
        }
        return null;
    }
    
    public static Boolean contains(String className)
    {
        return readers.containsKey(className);
    }
}
