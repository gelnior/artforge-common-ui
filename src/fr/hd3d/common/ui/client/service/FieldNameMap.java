package fr.hd3d.common.ui.client.service;

import java.util.HashMap;

import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.modeldata.asset.AssetRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.DeviceModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.InventoryItemModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.LicenseModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.PoolModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ScreenModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.SoftwareModelData;
import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ContractModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.common.ui.client.modeldata.task.ActivityModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.modeldata.technical.AuditModelData;


/**
 * This map contains the translation of all fields returned by the web services.
 * 
 * @author HD3D
 */
public class FieldNameMap extends HashMap<String, String>
{
    /** Automatically generated. */
    private static final long serialVersionUID = 1784411536965111447L;

    /** Constant strings to display : dialog messages, button label... */
    public static final CommonConstants CONSTANTS = GWT.create(CommonConstants.class);
    /** Parameterizable messages to display (like the counter label) */
    public static final CommonConstants MESSAGES = GWT.create(CommonConstants.class);

    /** Constructor : initialize all values. */
    public FieldNameMap()
    {
        this.initValues();
    }

    /**
     * Link fields name to their display value.
     */
    private void initValues()
    {
        this.put(InventoryItemModelData.NAME_FIELD, CONSTANTS.Name());
        this.put(InventoryItemModelData.SERIAL_FIELD, CONSTANTS.Serial());
        this.put(InventoryItemModelData.BILLING_REFERENCE_FIELD, CONSTANTS.Reference());
        this.put(InventoryItemModelData.PURCHASE_DATE_FIELD, CONSTANTS.PurchaseDate());
        this.put(InventoryItemModelData.WARRANTY_END_FIELD, CONSTANTS.WarrantyEnd());

        this.put(ComputerModelData.INVENTORY_ID_FIELD, CONSTANTS.InventoryId());
        this.put(ComputerModelData.DNS_NAME_FIELD, CONSTANTS.DnsName());
        this.put(ComputerModelData.MAC_ADDRESS_FIELD, CONSTANTS.MacAddress());
        this.put(ComputerModelData.NUMBER_OF_PROCS_FIELD, CONSTANTS.Processors());
        this.put(ComputerModelData.NUMBER_OF_CORES_FIELD, CONSTANTS.Cores());
        this.put(ComputerModelData.PROC_FREQUENCY_FIELD, CONSTANTS.Frequency());
        this.put(ComputerModelData.RAM_QUANTITY_FIELD, CONSTANTS.Ram());
        this.put(ComputerModelData.WORKER_STATUS_FIELD, CONSTANTS.WorkerStatus());
        this.put(ComputerModelData.IP_ADDRESS_FIELD, CONSTANTS.IpAdress());
        this.put(ComputerModelData.ROOM_NAME_FIELD, CONSTANTS.Room());
        this.put(ComputerModelData.PERSON_ID_FIELD, CONSTANTS.Worker());
        this.put(ComputerModelData.PERSON_NAME_FIELD, CONSTANTS.Worker());

        this.put(ComputerModelData.COMPUTER_TYPE_NAME_FIELD, CONSTANTS.Type());
        this.put(ComputerModelData.COMPUTER_MODEL_NAME_FIELD, CONSTANTS.Model());

        this.put(DeviceModelData.SIZE_FIELD, CONSTANTS.Size());
        this.put(DeviceModelData.DEVICE_TYPE_NAME_FIELD, CONSTANTS.Type());
        this.put(DeviceModelData.DEVICE_MODEL_NAME_FIELD, CONSTANTS.Model());

        this.put(ScreenModelData.QUALIFICATION_FIELD, CONSTANTS.Qualification());
        this.put(ScreenModelData.SCREEN_MODEL_NAME_FIELD, CONSTANTS.Model());

        this.put(LicenseModelData.NUMBER_FIELD, CONSTANTS.Number());
        this.put(LicenseModelData.TYPE_FIELD, CONSTANTS.Type());
        this.put(LicenseModelData.SOFTWARE_NAME_FIELD, CONSTANTS.Software());

        this.put(PoolModelData.IS_DISPATCHER_ALLOWED_FIELD, CONSTANTS.DispatcherAllowed());

        this.put(SoftwareModelData.COMPUTER_IDS_FIELD, CONSTANTS.Computers());
        this.put(SoftwareModelData.COMPUTER_NAMES_FIELD, CONSTANTS.Computers());
        this.put(ComputerModelData.POOL_IDS_FIELD, CONSTANTS.Pools());
        this.put(ComputerModelData.POOL_NAMES_FIELD, CONSTANTS.Pools());
        this.put(ComputerModelData.DEVICE_IDS_FIELD, CONSTANTS.Devices());
        this.put(ComputerModelData.DEVICE_NAMES_FIELD, CONSTANTS.Devices());
        this.put(ComputerModelData.SCREEN_IDS_FIELD, CONSTANTS.Screens());
        this.put(ComputerModelData.SCREEN_NAMES_FIELD, CONSTANTS.Screens());
        this.put(ComputerModelData.SOFTWARE_IDS_FIELD, CONSTANTS.Softwares());
        this.put(ComputerModelData.SOFTWARE_NAMES_FIELD, CONSTANTS.Softwares());
        this.put(ComputerModelData.LICENSE_IDS_FIELD, CONSTANTS.Licenses());
        this.put(ComputerModelData.LICENSE_NAMES_FIELD, CONSTANTS.Licenses());
        this.put(ComputerModelData.STUDIOMAP_NAME_FIELD, "Map");
        this.put(InventoryItemModelData.INVENTORY_STATUS_FIELD, CONSTANTS.InventoryStatus());
        this.put(ResourceGroupModelData.ROLE_NAMES_FIELD, CONSTANTS.Roles());
        this.put(ResourceGroupModelData.PROJECT_IDS_FIELD, CONSTANTS.Projects());
        this.put(ResourceGroupModelData.PROJECT_NAMES_FIELD, CONSTANTS.Projects());
        this.put(PersonModelData.RESOURCEGROUP_IDS_FIELD, CONSTANTS.ResourceGroups());
        this.put(PersonModelData.RESOURCEGROUP_NAMES_FIELD, CONSTANTS.ResourceGroups());
        this.put(ResourceGroupModelData.RESOURCE_NAMES_FIELD, CONSTANTS.Resources());

        this.put(AssetRevisionModelData.CREATOR_FIELD, CONSTANTS.Creator());
        this.put(AssetRevisionModelData.CREATOR_NAME_FIELD, CONSTANTS.Creator());
        this.put(AssetRevisionModelData.REVISION_FIELD, CONSTANTS.Revision());
        this.put(AssetRevisionModelData.VARIATION_FIELD, CONSTANTS.Variation());
        this.put(AssetRevisionModelData.STATUS_FIELD, CONSTANTS.Status());
        this.put(AssetRevisionModelData.SINCE_FIELD, CONSTANTS.Since());
        this.put(AssetRevisionModelData.LAST_USER_FIELD, CONSTANTS.LastUser());
        this.put(AssetRevisionModelData.LAST_USER_NAME_FIELD, CONSTANTS.LastUser());
        this.put(AssetRevisionModelData.COMMENT_FIELD, CONSTANTS.Comment());
        this.put(AssetRevisionModelData.VALIDITY_FIELD, CONSTANTS.Validity());

        this.put(AuditModelData.ENTITY_ID_FIELD, CONSTANTS.EntityId());
        this.put(AuditModelData.ENTITY_NAME_FIELD, CONSTANTS.EntityName());
        this.put(AuditModelData.OPERATION_FIELD, CONSTANTS.Operation());
        this.put(AuditModelData.DATE_FIELD, CONSTANTS.Date());
        this.put(AuditModelData.MODIFICATION_FIELD, CONSTANTS.Modification());
        this.put(AuditModelData.FIRST_NAME_FIELD, CONSTANTS.FirstName());
        this.put(AuditModelData.LAST_NAME_FIELD, CONSTANTS.LastName());
        this.put(AuditModelData.LOGIN_FIELD, CONSTANTS.Login());

        this.put("Computers", CONSTANTS.Computers());
        this.put("Devices", CONSTANTS.Devices());
        this.put("Screens", CONSTANTS.Screens());
        this.put("Licenses", CONSTANTS.Licenses());
        this.put("Softwares", CONSTANTS.Softwares());
        this.put("software", CONSTANTS.Software());
        this.put("Pools", CONSTANTS.Pools());
        this.put("screenModel", CONSTANTS.Model());
        this.put("deviceModel", CONSTANTS.Model());
        this.put("computerModel", CONSTANTS.Model());
        this.put("computerType", CONSTANTS.Type());
        this.put("deviceType", CONSTANTS.Type());
        this.put("room", CONSTANTS.Room());
        this.put("Projects", CONSTANTS.Projects());
        this.put("ResourceGroups", CONSTANTS.ResourceGroups());
        this.put("Roles", CONSTANTS.Roles());
        this.put("Resources", CONSTANTS.Resources());

        this.put("Computer", CONSTANTS.Computers());
        this.put("Device", CONSTANTS.Devices());
        this.put("Screen", CONSTANTS.Screens());
        this.put("License", CONSTANTS.Licenses());
        this.put("Software", CONSTANTS.Softwares());
        this.put("Pool", CONSTANTS.Pools());
        this.put("fr.hd3d.services.resources.collectionquery.TaskBoundEntityApprovalNotesCollectionQuery",
                "Status notes");

        this.put(ConstituentModelData.SIMPLE_CLASS_NAME, "Constituent");
        this.put(ShotModelData.SIMPLE_CLASS_NAME, "Shot");
        this.put(TaskModelData.SIMPLE_CLASS_NAME, "Task");
        this.put(AssetRevisionModelData.TASK_TYPE_ID_FIELD, "Task Type");
        this.put(AssetRevisionModelData.KEY_FIELD, CONSTANTS.Key());
        this.put(AssetRevisionModelData.WIP_PATH_FIELD, "WIP path");
        this.put(AssetRevisionModelData.PUBLISH_PATH_FIELD, "Publish path");
        this.put(AssetRevisionModelData.OLD_PATH_FIELD, "Old path");
        this.put(AssetRevisionModelData.MOUNT_POINT_WIP_PATH_FIELD, "Mount point wip path");
        this.put(AssetRevisionModelData.MOUNT_POINT_PUBLISH_PATH_FIELD, "Mount point publish path");
        this.put(AssetRevisionModelData.MOUNT_POINT_OLD_PATH_FIELD, "Mount point old path");

        this.put(PersonModelData.SIMPLE_CLASS_NAME, "Person");
        this.put(ContractModelData.SIMPLE_CLASS_NAME, "Contract");
        this.put(ActivityModelData.SIMPLE_CLASS_NAME, "Activity");

        this.put(SequenceModelData.SIMPLE_CLASS_NAME, "Sequence");
        this.put(CategoryModelData.SIMPLE_CLASS_NAME, "Category");
    }
}
