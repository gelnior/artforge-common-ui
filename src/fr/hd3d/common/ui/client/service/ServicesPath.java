package fr.hd3d.common.ui.client.service;

import com.extjs.gxt.ui.client.core.FastMap;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.common.ui.client.modeldata.ScriptModelData;
import fr.hd3d.common.ui.client.modeldata.annotation.GraphicalAnnotationModelData;
import fr.hd3d.common.ui.client.modeldata.asset.AssetRevisionGroupModelData;
import fr.hd3d.common.ui.client.modeldata.asset.AssetRevisionLinkModelData;
import fr.hd3d.common.ui.client.modeldata.asset.AssetRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.asset.DynamicPathModelData;
import fr.hd3d.common.ui.client.modeldata.asset.FileRevisionLinkModelData;
import fr.hd3d.common.ui.client.modeldata.asset.FileRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.asset.MountPointModelData;
import fr.hd3d.common.ui.client.modeldata.asset.ProxyModelData;
import fr.hd3d.common.ui.client.modeldata.asset.ProxyTypeModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerTypeModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.DeviceModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.DeviceModelModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.DeviceTypeModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.LicenseModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ManufacturerModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.PoolModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ProcessorModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.RoomModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ScreenModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ScreenModelModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.SoftwareModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.StudioMapModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListEditModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListRevisionGroupModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteTypeModelData;
import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;
import fr.hd3d.common.ui.client.modeldata.production.CompositionModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectTypeModelData;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.modeldata.production.StepModelData;
import fr.hd3d.common.ui.client.modeldata.resource.AbsenceModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ContractModelData;
import fr.hd3d.common.ui.client.modeldata.resource.EventModelData;
import fr.hd3d.common.ui.client.modeldata.resource.MilestoneModelData;
import fr.hd3d.common.ui.client.modeldata.resource.OrganizationModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.resource.QualificationModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.common.ui.client.modeldata.resource.SkillLevelModelData;
import fr.hd3d.common.ui.client.modeldata.resource.SkillModelData;
import fr.hd3d.common.ui.client.modeldata.task.ActivityModelData;
import fr.hd3d.common.ui.client.modeldata.task.EntityTaskLinkModelData;
import fr.hd3d.common.ui.client.modeldata.task.ExtraLineModelData;
import fr.hd3d.common.ui.client.modeldata.task.PersonDayModelData;
import fr.hd3d.common.ui.client.modeldata.task.PlanningModelData;
import fr.hd3d.common.ui.client.modeldata.task.SimpleActivityModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskActivityModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskChangesModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskGroupModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.modeldata.technical.AuditModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ClassDynModelData;
import fr.hd3d.common.ui.client.modeldata.technical.DynTypeModelData;
import fr.hd3d.common.ui.client.modeldata.technical.DynValueModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ItemGroupModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ListValuesModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ProjectSecurityTemplateModelData;
import fr.hd3d.common.ui.client.modeldata.technical.RoleModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SettingModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetFilterModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.modeldata.technical.TagModelData;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;


/**
 * Class that gives services path depending on ModelDataType.
 * 
 * @author HD3D
 */
public class ServicesPath
{
    private final static FastMap<String> map = new FastMap<String>();

    public static final String NEXT_REVISION = ServicesURI.NEXTREVISION + '/';
    public static final String SELF = ServicesURI.SELF + '/';
    public static final String CHILDREN = '/' + ServicesURI.CHILDREN;
    public static final String DATA = '/' + ServicesURI.DATA;
    public static final String CSV = '/' + ServicesURI.CSV;
    public static final String ALL = ServicesURI.ALL + '/';
    public static final String DOWN_STREAM = '/' + ServicesURI.DOWNSTREAM + '/';
    public static final String UP_STREAM = '/' + ServicesURI.UPSTREAM + '/';
    public static final String THUMBNAILS = ServicesURI.IMAGES_THUMBNAILS + '/';
    public static final String SCRIPTS = ServicesURI.SCRIPTS + '/';
    public static final String PREVIEWS = ServicesURI.IMAGES_PREVIEWS + '/';
    public static final String DURATION = ServicesURI.DURATION + '/';
    public static final String DUPLICATE_PLANNING_MASTER = ServicesURI.DUPLICATE_PLANNINGS + '/';
    public static final String PLANNING_EXPORT_ODS = ServicesURI.PLANNING_EXPORT_ODS + '/';
    public static final String FILTERED_TASKGROUP_TASKS = ServicesURI.FILTERED_TASKGROUP_TASKS + '/';
    public static final String PLANNING_PLANNED_TASKS = ServicesURI.PLANNING_PLANNED_TASKS + '/';
    public static final String WORKOBJECT_CATEGORY_RANGE = ServicesURI.WORK_OBJECTS_RANGE_CATEGORY + '/';
    public static final String WORKOBJECT_SEQUENCE_RANGE = ServicesURI.WORK_OBJECTS_RANGE_TEMPORAL + '/';
    public static final String UPSTREAM = ServicesURI.UPSTREAM + '/';
    public static final String DOWNSTREAM = ServicesURI.DOWNSTREAM + '/';
    public static final String DOWNLOAD = ServicesURI.DOWNLOAD + '/';
    public static final String ROOTS = ServicesURI.ROOTS + '/';
    public static final String UPLOAD = ServicesURI.UPLOAD + '/';
    public static final String INPUT = ServicesURI.INPUT + '/';
    public static final String OUTPUT = ServicesURI.OUTPUT + '/';
    public static final String MY = ServicesURI.MY + "/";

    public static final String CONSTITUENTS = ServicesURI.CONSTITUENTS + '/';
    public static final String SHOTS = ServicesURI.SHOTS + '/';
    public static final String PLAYLISTREVISIONS = ServicesURI.PLAYLISTREVISIONS + '/';
    public static final String AUDITS = ServicesURI.AUDITS + '/';
    public static final String SHEETS = ServicesURI.SHEETS + '/';
    public static final String ITEM_GROUPS = ServicesURI.ITEMGROUPS + '/';
    public static final String ITEMS = ServicesURI.ITEMS + '/';
    public static final String TASKS = ServicesURI.TASKS + '/';
    public static final String STEPS = ServicesURI.STEPS + '/';
    public static final String TASKTYPES = ServicesURI.TASK_TYPES + '/';
    public static final String PERSONS = ServicesURI.PERSONS + '/';
    public static final String RESOURCE_GROUPS = ServicesURI.RESOURCEGROUPS + '/';
    public static final String PERSON_DAYS = ServicesURI.DAYS_AND_ACTIVITIES + '/';
    public static final String ACTIVITIES = ServicesURI.ACTIVITIES + '/';
    public static final String PLANNINGS = ServicesURI.PLANNINGS + '/';
    public static final String MILESTONES = ServicesURI.MILESTONES + '/';

    public static final String ASSETS = '/' + ServicesURI.ASSETS;
    public static final String ASSET_REVISION_LINKS = ServicesURI.ASSETREVISIONLINKS + '/';
    public static final String ASSET_REVISIONS = ServicesURI.ASSETREVISIONS + '/';
    public static final String ASSET_REVISION_GROUPS = ServicesURI.ASSETREVISIONGROUPS + '/';
    public static final String FILE_REVISION_LINKS = ServicesURI.FILEREVISIONLINKS + '/';
    public static final String FILE_REVISIONS = ServicesURI.FILEREVISIONS + '/';

    public static final String DYN_METADATA_TYPES = ServicesURI.DYNMETADATATYPES + '/';
    public static final String CLASS_DYNMETADATA_TYPES = ServicesURI.CLASSDYNMETADATATYPES + '/';
    public static final String DYN_METADATA_VALUES = ServicesURI.DYNMETADATAVALUES + '/';
    public static final String LIST_VALUES = ServicesURI.LISTS_VALUES + '/';
    public static final String WHOIS = ServicesURI.WHOIS + '/';

    public static final String TASK_ACTIVITIES = ServicesURI.TASK_ACTIVITIES + '/';
    public static final String ENTITY_TASK_LINK = ServicesURI.ENTITYTASKLINKS + '/';
    public static final String SIMPLE_ACTIVITIES = ServicesURI.SIMPLE_ACTIVITIES + '/';

    public static final String PROJECTS = ServicesURI.PROJECTS + '/';
    public static final String PROJECT_TYPES = ServicesURI.PROJECT_TYPES + '/';

    public static final String ENTITIES = ServicesURI.ENTITIES + '/';

    public static final String CATEGORIES = ServicesURI.CATEGORIES + '/';
    public static final String SEQUENCES = ServicesURI.SEQUENCES + '/';
    public static final String PLAYLISTREVISIONGROUPS = ServicesURI.PLAYLISTREVISIONGROUPS + '/';

    public static final String COMPOSITIONS = ServicesURI.COMPOSITIONS + '/';

    public static final String TASKGROUPS = ServicesURI.TASK_GROUPS + '/';
    public static final String EXTRA_LINES = ServicesURI.EXTRA_LINES + '/';

    public static final String APPROVAL_NOTES = ServicesURI.APPROVALNOTES + '/';
    public static final String APPROVAL_NOTE_TYPES = ServicesURI.APPROVALNOTETYPES + '/';

    public static final String DYNAMIC_PATHS = ServicesURI.DYNAMIC_PATHS + '/';

    public static final String GRAPHICAL_ANNOTATIONS = ServicesURI.GRAPHICAL_ANNOTATIONS + '/';

    public static final String PROXIES = ServicesURI.PROXIES + '/';
    public static final String PROXYTYPES = ServicesURI.PROXYTYPES + '/';
    public static final String MOUNTPOINTS = ServicesURI.MOUNTPOINTS + '/';

    /**
     * Initializes available path map.
     */
    public static void initPath()
    {
        map.put("Permission".toLowerCase(), ServicesURI.WHOIS_PERMISSIONS + '/');
        map.put("Ban".toLowerCase(), ServicesURI.WHOIS_BANS + '/');
        map.put(AuditModelData.SIMPLE_CLASS_NAME.toLowerCase(), AUDITS);
        map.put(SheetModelData.SIMPLE_CLASS_NAME.toLowerCase(), SHEETS);
        map.put(ItemGroupModelData.SIMPLE_CLASS_NAME.toLowerCase(), ITEM_GROUPS);
        map.put(ItemModelData.SIMPLE_CLASS_NAME.toLowerCase(), ITEMS);
        map.put(SettingModelData.SIMPLE_CLASS_NAME.toLowerCase(), ServicesURI.SETTINGS + '/');
        map.put(SheetFilterModelData.SIMPLE_CLASS_NAME.toLowerCase(), ServicesURI.SHEET_FILTERS + '/');
        map.put(ScriptModelData.SIMPLE_CLASS_NAME.toLowerCase(), SCRIPTS);

        map.put(ComputerModelData.SIMPLE_CLASS_NAME.toLowerCase(), ServicesURI.COMPUTERS + '/');
        map.put(SoftwareModelData.SIMPLE_CLASS_NAME.toLowerCase(), ServicesURI.SOFTWARES + '/');
        map.put(LicenseModelData.SIMPLE_CLASS_NAME.toLowerCase(), ServicesURI.LICENSES + '/');
        map.put(DeviceModelData.SIMPLE_CLASS_NAME.toLowerCase(), ServicesURI.DEVICES + '/');
        map.put(ScreenModelData.SIMPLE_CLASS_NAME.toLowerCase(), ServicesURI.SCREENS + '/');
        map.put(StudioMapModelData.SIMPLE_CLASS_NAME.toLowerCase(), ServicesURI.STUDIOMAPS + '/');
        map.put(PoolModelData.SIMPLE_CLASS_NAME.toLowerCase(), ServicesURI.POOLS + '/');
        map.put(ComputerModelModelData.SIMPLE_CLASS_NAME.toLowerCase(), ServicesURI.COMPUTER_MODELS + '/');
        map.put(DeviceModelModelData.SIMPLE_CLASS_NAME.toLowerCase(), ServicesURI.DEVICE_MODELS + '/');
        map.put(ScreenModelModelData.SIMPLE_CLASS_NAME.toLowerCase(), ServicesURI.SCREEN_MODELS + '/');
        map.put(ManufacturerModelData.SIMPLE_CLASS_NAME.toLowerCase(), ServicesURI.MANUFACTURERS + '/');
        map.put(ProcessorModelData.SIMPLE_CLASS_NAME.toLowerCase(), ServicesURI.PROCESSORS + '/');
        map.put(ComputerTypeModelData.SIMPLE_CLASS_NAME.toLowerCase(), ServicesURI.COMPUTER_TYPES + '/');
        map.put(DeviceTypeModelData.SIMPLE_CLASS_NAME.toLowerCase(), ServicesURI.DEVICE_TYPES + '/');
        map.put(RoomModelData.SIMPLE_CLASS_NAME.toLowerCase(), ServicesURI.ROOMS + '/');

        map.put(ResourceGroupModelData.SIMPLE_CLASS_NAME.toLowerCase(), RESOURCE_GROUPS);
        map.put(SkillModelData.SIMPLE_CLASS_NAME.toLowerCase(), ServicesURI.SKILLS + '/');
        map.put(SkillLevelModelData.SIMPLE_CLASS_NAME.toLowerCase(), ServicesURI.SKILLLEVELS + '/');
        map.put(QualificationModelData.SIMPLE_CLASS_NAME.toLowerCase(), ServicesURI.QUALIFICATIONS + '/');
        map.put(OrganizationModelData.SIMPLE_CLASS_NAME.toLowerCase(), ServicesURI.ORGANIZATIONS + '/');
        map.put(ContractModelData.SIMPLE_CLASS_NAME.toLowerCase(), ServicesURI.CONTRACTS + '/');
        map.put(PersonModelData.SIMPLE_CLASS_NAME.toLowerCase(), PERSONS);
        map.put(EventModelData.SIMPLE_CLASS_NAME.toLowerCase(), ServicesURI.EVENTS + '/');
        map.put(MilestoneModelData.SIMPLE_CLASS_NAME.toLowerCase(), ServicesURI.MILESTONES + '/');
        map.put(AbsenceModelData.SIMPLE_CLASS_NAME.toLowerCase(), ServicesURI.ABSENCES + '/');
        map.put(RoleModelData.SIMPLE_CLASS_NAME.toLowerCase(), ServicesURI.ROLES + '/');
        map.put(ProjectSecurityTemplateModelData.SIMPLE_CLASS_NAME.toLowerCase(),
                ServicesURI.SECURITY_TEMPLATES_TEMPLATES + '/');
        map.put("Role Templates".toLowerCase(), ServicesURI.ROLE_TEMPLATES + '/');
        map.put("Apply Template".toLowerCase(), ServicesURI.SECURITY_TEMPLATES + '/' + ServicesURI.APPLY_TEMPLATE + '/');
        map.put("Resource".toLowerCase(), ServicesURI.PERSONS + '/');

        map.put(ConstituentModelData.SIMPLE_CLASS_NAME.toLowerCase(), CONSTITUENTS);
        map.put(CategoryModelData.SIMPLE_CLASS_NAME.toLowerCase(), CATEGORIES);
        map.put(ShotModelData.SIMPLE_CLASS_NAME.toLowerCase(), SHOTS);
        map.put(SequenceModelData.SIMPLE_CLASS_NAME.toLowerCase(), SEQUENCES);
        map.put(PlayListRevisionGroupModelData.SIMPLE_CLASS_NAME.toLowerCase(), PLAYLISTREVISIONGROUPS);
        map.put(CompositionModelData.SIMPLE_CLASS_NAME.toLowerCase(), COMPOSITIONS);

        map.put(ProjectModelData.SIMPLE_CLASS_NAME.toLowerCase(), PROJECTS);
        map.put(ProjectTypeModelData.SIMPLE_CLASS_NAME.toLowerCase(), PROJECT_TYPES);
        map.put(TagModelData.SIMPLE_CLASS_NAME.toLowerCase(), ServicesURI.TAGS + '/');
        map.put(PlanningModelData.SIMPLE_CLASS_NAME.toLowerCase(), ServicesURI.PLANNINGS + '/');
        map.put(TaskGroupModelData.SIMPLE_CLASS_NAME.toLowerCase(), ServicesURI.TASK_GROUPS + '/');
        map.put(ApprovalNoteModelData.SIMPLE_CLASS_NAME.toLowerCase(), ServicesURI.APPROVALNOTES + '/');
        map.put(ApprovalNoteTypeModelData.SIMPLE_CLASS_NAME.toLowerCase(), ServicesURI.APPROVALNOTETYPES + '/');
        map.put(TaskTypeModelData.SIMPLE_CLASS_NAME.toLowerCase(), ServicesURI.TASK_TYPES + '/');
        map.put(ExtraLineModelData.SIMPLE_CLASS_NAME.toLowerCase(), ServicesURI.EXTRA_LINES + '/');
        map.put(StepModelData.SIMPLE_CLASS_NAME.toLowerCase(), STEPS);
        map.put(TaskModelData.SIMPLE_CLASS_NAME.toLowerCase(), TASKS);
        map.put(TaskChangesModelData.SIMPLE_CLASS_NAME.toLowerCase(), ServicesURI.TASK_CHANGES + '/');
        map.put(ActivityModelData.SIMPLE_CLASS_NAME.toLowerCase(), ACTIVITIES);
        map.put(SimpleActivityModelData.SIMPLE_CLASS_NAME.toLowerCase(), SIMPLE_ACTIVITIES);
        map.put(TaskActivityModelData.SIMPLE_CLASS_NAME.toLowerCase(), TASK_ACTIVITIES);
        map.put(PersonDayModelData.SIMPLE_CLASS_NAME.toLowerCase(), PERSON_DAYS);
        map.put(EntityTaskLinkModelData.SIMPLE_CLASS_NAME.toLowerCase(), ENTITY_TASK_LINK);

        map.put(AssetRevisionModelData.SIMPLE_CLASS_NAME.toLowerCase(), ASSET_REVISIONS);
        map.put(FileRevisionModelData.SIMPLE_CLASS_NAME.toLowerCase(), FILE_REVISIONS);
        map.put(FileRevisionLinkModelData.SIMPLE_CLASS_NAME.toLowerCase(), FILE_REVISION_LINKS);
        map.put(AssetRevisionLinkModelData.SIMPLE_CLASS_NAME.toLowerCase(), ASSET_REVISION_LINKS);
        map.put(AssetRevisionGroupModelData.SIMPLE_CLASS_NAME.toLowerCase(), ASSET_REVISION_GROUPS);
        // smap.put(AssetRevisionFileRevisionModelData.SIMPLE_CLASS_NAME.toLowerCase(), ASSET_REVISION_FILE_REVISION);

        map.put(ClassDynModelData.SIMPLE_CLASS_NAME.toLowerCase(), CLASS_DYNMETADATA_TYPES);
        map.put(DynTypeModelData.SIMPLE_CLASS_NAME.toLowerCase(), DYN_METADATA_TYPES);
        map.put(DynValueModelData.SIMPLE_CLASS_NAME.toLowerCase(), DYN_METADATA_VALUES);
        map.put(ListValuesModelData.SIMPLE_CLASS_NAME.toLowerCase(), LIST_VALUES);
        map.put(DynamicPathModelData.SIMPLE_CLASS_NAME.toLowerCase(), DYNAMIC_PATHS);
        map.put(ProxyModelData.SIMPLE_CLASS_NAME.toLowerCase(), PROXIES);
        map.put(ProxyTypeModelData.SIMPLE_CLASS_NAME.toLowerCase(), PROXYTYPES);
        map.put(MountPointModelData.SIMPLE_CLASS_NAME.toLowerCase(), MOUNTPOINTS);

        // playlists
        map.put(PlayListRevisionModelData.SIMPLE_CLASS_NAME.toLowerCase(), ServicesURI.PLAYLISTREVISIONS + '/');
        map.put(PlayListEditModelData.SIMPLE_CLASS_NAME.toLowerCase(), ServicesURI.PLAYLISTEDITS + '/');

        // SVG Annotation
        map.put(GraphicalAnnotationModelData.SIMPLE_CLASS_NAME.toLowerCase(), GRAPHICAL_ANNOTATIONS);

    }

    /**
     * @param simpleClassName
     *            The class name corresponding to the desired path.
     * @return Path corresponding to given class name.
     */
    public static String getPath(String simpleClassName)
    {
        if (simpleClassName != null)
        {
            return map.get(simpleClassName.toLowerCase());
        }
        else
        {
            return null;
        }
    }

    /**
     * @param id
     *            ID of the object for which path is requested.
     * @return Path to the task corresponding at given ID. If a project is set to main model the path is prefixed with
     *         the project path. If id is null, no ID is set at the end of the path.
     */
    public static String getTasksPath(Long id)
    {
        return getOneSegmentPath(TASKS, id);
    }

    /**
     * @param id
     *            ID of the object for which path is requested.
     * @return Path to the approval note type corresponding at given ID. If a project is set to main model the path is
     *         prefixed with the project path. If id is null, no ID is set at the end of the path.
     */
    public static String getApprovalNoteTypePath(Long id)
    {
        return getOneSegmentPath(APPROVAL_NOTE_TYPES, id);
    }

    /**
     * @param id
     *            ID of the object for which path is requested.
     * @return Path to the approval note corresponding at given ID. If a project is set to main model the path is
     *         prefixed with the project path. If id is null, no ID is set at the end of the path.
     */
    public static String getApprovalNotePath(Long id)
    {
        return getOneSegmentPath(APPROVAL_NOTES, id);
    }

    /**
     * @param id
     *            ID of the object for which path is requested.
     * @return From given segment and id, build a path. If a project is set to main model the segment is prefixed with
     *         the project path. If id is null, no ID is set at the end of the path.
     */
    public static String getOneSegmentPath(String segment, Long id)
    {
        String path = segment;
        if (MainModel.getCurrentProject() != null)
            path = MainModel.getCurrentProject().getDefaultPath() + "/" + path;
        if (id != null)
            path += id;
        return path;
    }

}
