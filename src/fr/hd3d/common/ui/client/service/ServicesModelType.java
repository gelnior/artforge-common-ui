package fr.hd3d.common.ui.client.service;

import java.util.HashMap;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.annotation.GraphicalAnnotationModelData;
import fr.hd3d.common.ui.client.modeldata.asset.AssetRevisionGroupModelData;
import fr.hd3d.common.ui.client.modeldata.asset.AssetRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.asset.FileRevisionLinkModelData;
import fr.hd3d.common.ui.client.modeldata.asset.FileRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.asset.ProxyModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerTypeModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.DeviceModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.DeviceModelModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.DeviceTypeModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.LicenseModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ManufacturerModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ModelModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.PoolModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ProcessorModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.RoomModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ScreenModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ScreenModelModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.SoftwareModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.StudioMapModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListEditModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteTypeModelData;
import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;
import fr.hd3d.common.ui.client.modeldata.production.CompositionModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.modeldata.production.StepModelData;
import fr.hd3d.common.ui.client.modeldata.resource.AbsenceModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ContractModelData;
import fr.hd3d.common.ui.client.modeldata.resource.EventModelData;
import fr.hd3d.common.ui.client.modeldata.resource.OrganizationModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.resource.QualificationModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.common.ui.client.modeldata.resource.SkillLevelModelData;
import fr.hd3d.common.ui.client.modeldata.resource.SkillModelData;
import fr.hd3d.common.ui.client.modeldata.task.ActivityModelData;
import fr.hd3d.common.ui.client.modeldata.task.EntityTaskLinkModelData;
import fr.hd3d.common.ui.client.modeldata.task.PersonDayModelData;
import fr.hd3d.common.ui.client.modeldata.task.PlanningModelData;
import fr.hd3d.common.ui.client.modeldata.task.SimpleActivityModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskActivityModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;
import fr.hd3d.common.ui.client.modeldata.technical.ProjectSecurityTemplateModelData;
import fr.hd3d.common.ui.client.modeldata.technical.RoleModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SettingModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetFilterModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;


/**
 * Class that gives services path depending on ModelDataType.
 * 
 * @author HD3D
 */
public class ServicesModelType
{
    private final static HashMap<String, ModelType> map = new HashMap<String, ModelType>();

    public static void initModelTypes()
    {
        map.put(CategoryModelData.SIMPLE_CLASS_NAME, CategoryModelData.getModelType());

        map.put("Computer", ComputerModelData.getModelType());
        map.put("Software", SoftwareModelData.getModelType());
        map.put("License", LicenseModelData.getModelType());
        map.put("Device", DeviceModelData.getModelType());
        map.put("Screen", ScreenModelData.getModelType());
        map.put("ScreenModel", ModelModelData.getModelType());
        map.put("Pool", PoolModelData.getModelType());
        map.put("StudioMap", StudioMapModelData.getModelType());
        map.put("Manufacturer", ManufacturerModelData.getModelType());
        map.put("DeviceModel", DeviceModelModelData.getModelType());
        map.put("ComputerModel", ComputerModelModelData.getModelType());
        map.put("ScreenModel", ScreenModelModelData.getModelType());
        map.put("DeviceType", DeviceTypeModelData.getModelType());
        map.put("ComputerType", ComputerTypeModelData.getModelType());
        map.put(ProcessorModelData.SIMPLE_CLASS_NAME, ProcessorModelData.getModelType());
        map.put(RoomModelData.SIMPLE_CLASS_NAME, RoomModelData.getModelType());

        map.put(ResourceGroupModelData.SIMPLE_CLASS_NAME, ResourceGroupModelData.getModelType());
        map.put(SkillModelData.SIMPLE_CLASS_NAME, SkillModelData.getModelType());
        map.put(SkillLevelModelData.SIMPLE_CLASS_NAME, SkillLevelModelData.getModelType());
        map.put(QualificationModelData.SIMPLE_CLASS_NAME, QualificationModelData.getModelType());
        map.put(OrganizationModelData.SIMPLE_CLASS_NAME, OrganizationModelData.getModelType());
        map.put(ContractModelData.SIMPLE_CLASS_NAME, ContractModelData.getModelType());
        map.put(PersonModelData.SIMPLE_CLASS_NAME, PersonModelData.getModelType());
        map.put(EventModelData.SIMPLE_CLASS_NAME, EventModelData.getModelType());
        map.put(AbsenceModelData.SIMPLE_CLASS_NAME, AbsenceModelData.getModelType());
        map.put("Resource", PersonModelData.getModelType());
        map.put(RoleModelData.SIMPLE_CLASS_NAME, RoleModelData.getModelType());
        map.put(ProjectSecurityTemplateModelData.SIMPLE_CLASS_NAME, ProjectSecurityTemplateModelData.getModelType());

        map.put(SheetModelData.SIMPLE_CLASS_NAME, SheetModelData.getModelType());
        map.put(ProjectModelData.SIMPLE_CLASS_NAME, ProjectModelData.getModelType());
        map.put(ConstituentModelData.SIMPLE_CLASS_NAME, ConstituentModelData.getModelType());
        map.put(ShotModelData.SIMPLE_CLASS_NAME, ShotModelData.getModelType());
        map.put(CompositionModelData.SIMPLE_CLASS_NAME, CompositionModelData.getModelType());

        map.put(TaskModelData.SIMPLE_CLASS_NAME, TaskModelData.getModelType());
        map.put(StepModelData.SIMPLE_CLASS_NAME, StepModelData.getModelType());
        map.put(TaskTypeModelData.SIMPLE_CLASS_NAME, TaskTypeModelData.getModelType());
        map.put(PlanningModelData.SIMPLE_CLASS_NAME, PlanningModelData.getModelType());
        map.put(PersonDayModelData.SIMPLE_CLASS_NAME, PersonDayModelData.getModelType());
        map.put(AssetRevisionModelData.SIMPLE_CLASS_NAME, AssetRevisionModelData.getModelType());
        map.put(AssetRevisionGroupModelData.SIMPLE_CLASS_NAME, AssetRevisionGroupModelData.getModelType());
        map.put(FileRevisionLinkModelData.SIMPLE_CLASS_NAME, FileRevisionLinkModelData.getModelType());
        map.put(TaskActivityModelData.SIMPLE_CLASS_NAME, TaskActivityModelData.getModelType());
        map.put(SimpleActivityModelData.SIMPLE_CLASS_NAME, SimpleActivityModelData.getModelType());
        map.put(ApprovalNoteTypeModelData.SIMPLE_CLASS_NAME, ApprovalNoteTypeModelData.getModelType());
        map.put(ApprovalNoteModelData.SIMPLE_CLASS_NAME, ApprovalNoteModelData.getModelType());
        map.put(ActivityModelData.SIMPLE_CLASS_NAME, ActivityModelData.getModelType());
        // map
        // .put(AssetRevisionFileRevisionModelData.SIMPLE_CLASS_NAME, AssetRevisionFileRevisionModelData
        // .getModelType());
        map.put(EntityTaskLinkModelData.SIMPLE_CLASS_NAME, EntityTaskLinkModelData.getModelType());
        map.put(AssetRevisionGroupModelData.SIMPLE_CLASS_NAME, AssetRevisionGroupModelData.getModelType());

        map.put(SettingModelData.SIMPLE_CLASS_NAME, SettingModelData.getModelType());
        map.put(SheetFilterModelData.SIMPLE_CLASS_NAME, SheetFilterModelData.getModelType());

        // playlists
        map.put(PlayListRevisionModelData.SIMPLE_CLASS_NAME, PlayListRevisionModelData.getModelType());
        map.put(PlayListEditModelData.SIMPLE_CLASS_NAME, PlayListEditModelData.getModelType());

        map.put(SequenceModelData.SIMPLE_CLASS_NAME, SequenceModelData.getModelType());
        map.put(GraphicalAnnotationModelData.SIMPLE_CLASS_NAME, GraphicalAnnotationModelData.getModelType());
        map.put(FileRevisionModelData.SIMPLE_CLASS_NAME, FileRevisionModelData.getModelType());
        map.put(ItemModelData.SIMPLE_CLASS_NAME, ItemModelData.getModelType());
        map.put(ProxyModelData.SIMPLE_CLASS_NAME, ProxyModelData.getModelType());
    }

    public static ModelType getModelType(String boundClassName)
    {
        return map.get(boundClassName);
    }
}
