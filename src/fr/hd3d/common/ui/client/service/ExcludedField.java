package fr.hd3d.common.ui.client.service;

import java.util.ArrayList;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.asset.AssetRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.DeviceModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.LicenseModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ScreenModelData;


/**
 * Field List that should not be displayed in identity panel. The list is accessible in a static way.
 * 
 * @author HD3D
 */
public class ExcludedField
{
    /** Field List that should not be displayed in identity panel. */
    private final static ArrayList<String> excludedFields = new ArrayList<String>();

    /** Initialize the list of fields to exclude. */
    public static void initExcludedFields()
    {
        addExcludedField("computers");
        addExcludedField("devices");
        addExcludedField("softwares");
        addExcludedField("pools");
        addExcludedField("licenses");
        addExcludedField(Hd3dModelData.ID_FIELD);
        addExcludedField(Hd3dModelData.DEFAULT_PATH_FIELD);
        addExcludedField(Hd3dModelData.USER_CAN_UPDATE_FIELD);
        addExcludedField(Hd3dModelData.USER_CAN_DELETE_FIELD);
        addExcludedField(Hd3dModelData.CLASS_NAME_FIELD);
        addExcludedField(ComputerModelData.X_POSITION_FIELD);
        addExcludedField(ComputerModelData.Y_POSITION_FIELD);
        addExcludedField(ComputerModelData.STUDIOMAP_ID_FIELD);
        addExcludedField(ComputerModelData.ROOM_ID_FIELD);
        addExcludedField(ComputerModelData.PERSON_ID_FIELD);
        addExcludedField(ComputerModelData.WORKER_STATUS_FIELD);
        addExcludedField(ScreenModelData.SCREEN_MODEL_ID_FIELD);
        addExcludedField(DeviceModelData.DEVICE_MODEL_ID_FIELD);
        addExcludedField(DeviceModelData.DEVICE_TYPE_ID_FIELD);
        addExcludedField(ComputerModelData.COMPUTER_TYPE_ID_FIELD);
        addExcludedField(ComputerModelData.COMPUTER_MODEL_ID_FIELD);
        addExcludedField(LicenseModelData.SOFTWARE_ID_FIELD);

        addExcludedField(AssetRevisionModelData.KEY_FIELD);
        addExcludedField(Hd3dModelData.DYN_VALUES_FIELD);
        addExcludedField(ComputerModelData.PROCESSOR_ID_FIELD);
        addExcludedField(ComputerModelData.PROCESSOR_NAME_FIELD);
        addExcludedField(RecordModelData.BOUND_CLASS_NAME_FIELD);

        addExcludedField(AssetRevisionModelData.WORKOBJECT_ID_FIELD);
        addExcludedField(AssetRevisionModelData.WORKOBJECT_NAME_FIELD);
        addExcludedField(AssetRevisionModelData.WORKOBJECT_PATH_FIELD);
    }

    /**
     * Add a field to the excluded field list.
     * 
     * @param fieldName
     *            The name of the field to add.
     */
    public static void addExcludedField(String fieldName)
    {
        excludedFields.add(fieldName.toLowerCase());
    }

    /**
     * @param fieldName
     *            The name of the field to test.
     * @return true if the field is in the excluded field list.
     */
    public static boolean isExcluded(String fieldName)
    {
        return excludedFields.contains(fieldName.toLowerCase());
    }
}
