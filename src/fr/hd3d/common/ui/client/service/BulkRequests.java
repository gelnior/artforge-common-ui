package fr.hd3d.common.ui.client.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import org.restlet.client.data.Method;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.callback.BaseBulkCallback;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.service.callback.EventCallback;
import fr.hd3d.common.ui.client.service.parameter.BulkMode;
import fr.hd3d.common.ui.client.service.parameter.ParameterBuilder;
import fr.hd3d.common.ui.client.util.CollectionUtils;


/**
 * Static class that performs bulk requests based on record lists given in parameter.
 * 
 * @author HD3D
 */
public class BulkRequests
{
    /**
     * Build a json list (string) of the record list given in parameter. Then it performs a bulk request of all the
     * record list.
     * 
     * @param records
     *            The list of records to send to server.
     * @param eventType
     *            The eventType to forward when request succeeds.
     */
    public static void bulkPut(List<? extends Hd3dModelData> records, EventType eventType)
    {
        bulkRequest(Method.PUT, records, eventType, null);
    }

    public static void bulkPut(List<? extends Hd3dModelData> records, BaseCallback callback)
    {
        bulkRequest(Method.PUT, records, new AppEvent(CommonEvents.MODEL_DATA_UPDATE_SUCCESS), callback);
    }

    public static void bulkDelete(List<? extends Hd3dModelData> records, AppEvent event)
    {
        bulkRequest(Method.DELETE, records, event, null);
    }

    /**
     * Build a json list (string) of the record list given in parameter. It divides given list in sub list of length
     * <i>range</i>. Then it performs sequentially a bulk request for each sub list. When all requests are finished, it
     * dispatch an event of type <i>eventType</i> to controllers.
     * 
     * @param records
     *            The list of records to send to server.
     * @param eventType
     *            The eventType to forward when request succeeds.
     * @param range
     *            Length of sublists.
     */
    public static void bulkRangePut(List<? extends Hd3dModelData> records, EventType eventType, int range)
    {
        bulkRangePut(records, new AppEvent(eventType), range);
    }

    /**
     * Build a json list (string) of the record list given in parameter. It divides given list in sub list of length
     * <i>range</i>. Then it performs sequentially a bulk request for each sub list. When all requests are finished, it
     * dispatch <i>event</i> to controllers.
     * 
     * @param records
     *            The list of records to send to server.
     * @param event
     *            The AppEvent to forward when request succeeds.
     * @param range
     *            Length of sublists.
     */
    public static void bulkRangePut(List<? extends Hd3dModelData> records, AppEvent event, int range)
    {
        bulkRangeRequest(Method.PUT, records, event, range);
    }

    public static void bulkPost(List<? extends Hd3dModelData> records, AppEvent event)
    {
        bulkRequest(Method.POST, records, event, null);
    }

    /**
     * Build a json list (string) of the record list given in parameter. Then it performs a bulk request a post of all
     * the record list.
     * 
     * @param records
     *            The list of records to send to server.
     * @param eventType
     *            The eventType to forward when request succeeds.
     */
    public static void bulkPost(List<? extends Hd3dModelData> records, EventType eventType)
    {
        bulkRequest(Method.POST, records, eventType, null);
    }

    public static void bulkPost(List<? extends Hd3dModelData> records, BaseCallback callback)
    {
        bulkRequest(Method.POST, records, new AppEvent(CommonEvents.MODEL_DATA_CREATION_SUCCESS), callback);
    }

    public static void bulkPost(List<? extends Hd3dModelData> records, BaseBulkCallback callback)
    {
        bulkRequest(Method.POST, records, new AppEvent(CommonEvents.MODEL_DATA_CREATION_SUCCESS), callback);
    }

    /**
     * Build a json list (string) of the record list given in parameter. It divides given list in sub list of length
     * <i>range</i>. Then it performs sequentially a bulk request for each sub list. When all requests are finished, it
     * dispatch an event of type <i>eventType</i> to controllers.
     * 
     * @param records
     *            The list of records to send to server.
     * @param eventType
     *            The eventType to forward when request succeeds.
     * @param range
     *            Length of sublists.
     */
    public static void bulkRangePost(List<Hd3dModelData> records, EventType eventType, int range)
    {
        bulkRangePost(records, new AppEvent(eventType), range);
    }

    /**
     * Build a json list (string) of the record list given in parameter. It divides given list in sub list of length
     * <i>range</i>. Then it performs sequentially a bulk request for each sub list. When all requests are finished, it
     * dispatch <i>event</i> to controllers.
     * 
     * @param records
     *            The list of records to send to server.
     * @param event
     *            The AppEvent to forward when request succeeds.
     * @param range
     *            Length of sublists.
     */
    public static void bulkRangePost(List<? extends Hd3dModelData> records, AppEvent event, int range)
    {
        bulkRangeRequest(Method.POST, records, event, range);
    }

    /**
     * Build a json list (string) of a sub list, with range size, of the record list given in parameter depending on the
     * chosen method. Then it performs a bulk request to post or put all the record lists.
     * 
     * @param method
     *            The request method : PUT or POST.
     * @param records
     *            The list of records to send to server.
     * @param event
     *            The eventType to forward when request succeeds.
     */
    private static void bulkRangeRequest(Method method, List<? extends Hd3dModelData> records, AppEvent event, int range)
    {
        int start = 0;
        Stack<List<? extends Hd3dModelData>> lists = new Stack<List<? extends Hd3dModelData>>();

        while (start < records.size())
        {
            List<Hd3dModelData> subList;
            int limit = Math.min(records.size(), start + range);
            subList = new ArrayList<Hd3dModelData>(range);
            for (int i = start; i < limit; i++)
            {
                subList.add(records.get(i));
            }
            lists.push(subList);
            start += range;
        }
        saveLists(lists, event);
    }

    /**
     * Build a json list (string) of the record list given in parameter depending on the chosen method. Then it performs
     * a bulk request to post or put all the record list.
     * 
     * @param method
     *            The request method : PUT or POST.
     * @param records
     *            The list of records to send to server.
     * @param eventType
     *            The eventType to forward when request succeeds.
     */
    private static void bulkRequest(Method method, List<? extends Hd3dModelData> records, EventType eventType,
            BaseCallback callback)
    {
        bulkRequest(method, records, new AppEvent(eventType), callback);
    }

    public static void bulkRequest(List<Hd3dModelData> records, Method method, BaseBulkCallback callback)
    {
        bulkRequest(method, records, new AppEvent(CommonEvents.MODEL_DATA_UPDATE_SUCCESS), callback);
    }

    /**
     * Perform a bulk request for given data and method. Then it executes given callback and dispatch event to
     * controllers when it succeeds. If request is of type POST, IDs are updated.
     * 
     * @param method
     *            The method : POST for mass creation, PUT for mass modification.
     * @param records
     *            The records to save.
     * @param event
     *            The event dispatched to controllers on success.
     * @param callback
     *            The callback that handles the server response.
     */
    private static void bulkRequest(Method method, List<? extends Hd3dModelData> records, AppEvent event,
            BaseCallback callback)
    {
        if (CollectionUtils.isNotEmpty(records))
        {
            StringBuffer json = new StringBuffer();
            json.append("[");
            for (Hd3dModelData record : records)
            {
                if (json.length() > 1)
                {
                    json.append(",");
                }
                if (method == Method.POST)
                {
                    json.append(record.toPOSTJson());
                }
                else if (method == Method.PUT)
                {
                    json.append(record.toPUTJson());
                }
                else if (method == Method.DELETE)
                {
                    json.append(record.getId());
                }
            }
            json.append("]");

            String path = records.get(0).getPath();

            if (method == Method.PUT || method == Method.DELETE)
            {
                String id = path.substring(path.lastIndexOf('/') + 1);
                path = path.substring(0, path.length() - id.length() - 1);

                if (callback == null)
                {
                    callback = new EventCallback(event);
                }
            }
            else
            {
                if (callback == null)
                {
                    callback = new BaseBulkCallback(records, event);
                }
            }

            path += ParameterBuilder.parameterToString(new BulkMode());
            if (Method.DELETE == method)
            {
                path += "&ids=" + json.toString();
            }

            IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
            requestHandler.handleRequest(method, path, json.toString(), callback);
        }
        else
        {
            GWT.log("Bulk call with nothing to post. Check that your data list is not empty", new Exception());
        }
    }

    /**
     * Save sequentially each list of <i>lists</i> by sending a bulk requests. When all list are saved event is
     * dispatched to controllers.
     * 
     * @param lists
     *            The lists of data to save.
     * @param event
     *            The event to forward when all requests succeeds.
     */
    public static void saveLists(final Stack<List<? extends Hd3dModelData>> lists, final AppEvent event)
    {
        if (lists.size() > 0)
        {
            List<? extends Hd3dModelData> list = lists.pop();

            if (list.size() > 0)
            {
                if (list.get(0).getId() != null)
                {
                    BulkRequests.bulkPut(list, new BaseBulkCallback(list) {
                        @Override
                        protected void onIdsUpdated()
                        {
                            saveLists(lists, event);
                        }
                    });
                }
                else
                {
                    BulkRequests.bulkPost(list, new BaseBulkCallback(list) {
                        @Override
                        protected void onIdsUpdated()
                        {
                            saveLists(lists, event);
                        }
                    });
                }
            }
            else
            {
                saveLists(lists, event);
            }
        }
        else
        {
            EventDispatcher.forwardEvent(event);
        }
    }

}
