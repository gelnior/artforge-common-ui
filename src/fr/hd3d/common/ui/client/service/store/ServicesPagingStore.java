package fr.hd3d.common.ui.client.service.store;

import java.util.List;

import com.extjs.gxt.ui.client.data.BasePagingLoader;
import com.extjs.gxt.ui.client.data.ListLoader;
import com.extjs.gxt.ui.client.data.PagingLoadResult;
import com.extjs.gxt.ui.client.data.PagingLoader;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.reader.IPagingReader;
import fr.hd3d.common.ui.client.service.parameter.IUrlParameter;
import fr.hd3d.common.ui.client.service.proxy.ServicesPagingProxy;


/**
 * Store connected to HD3D web services. This store handles pagination.
 * 
 * @author HD3D
 * 
 * @param <M>
 *            Class type of data retrieved.
 */
public class ServicesPagingStore<M extends Hd3dModelData> extends BaseStore<M>
{

    private static final int DEFAULT_LIMIT = 50;

    protected int limit = DEFAULT_LIMIT;
    protected PagingLoader<PagingLoadResult<M>> pagingLoader;

    /**
     * Default constructor : set proxy and loader to access services. Also add a load listener accessible through
     * protected methods.
     * 
     * @param reader
     *            The reader needed to parse data.
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public ServicesPagingStore(IPagingReader<M> reader)
    {
        super();
        this.proxy = new ServicesPagingProxy<M>();
        this.pagingLoader = new BasePagingLoader<PagingLoadResult<M>>((ServicesPagingProxy<M>) proxy, reader);
        this.loader = (ListLoader) this.pagingLoader;
        this.setLoadListener();
    }

    public ServicesPagingStore()
    {}

    @Override
    public void reload()
    {
        this.reload(0, this.limit);
    }

    public void reload(int offset)
    {
        this.pagingLoader.load(offset, this.limit);
    }

    public void reload(int offset, int limit)
    {
        this.pagingLoader.load(offset, limit);
    }

    /**
     * @return Proxy used by store to access services.
     */
    public ServicesPagingProxy<M> getProxy()
    {
        return (ServicesPagingProxy<M>) this.proxy;
    }

    public int getLimit()
    {
        return this.limit;
    }

    public void setLimit(int limit)
    {
        this.limit = limit;
    }

    public void setLoaderOffset(int loaderOffset)
    {
        this.pagingLoader.setOffset(loaderOffset);
    }

    public PagingLoader<?> getPagingLoader()
    {
        return this.pagingLoader;
    }

    public List<IUrlParameter> getParameters()
    {
        return proxy.getParameters();
    }

}
