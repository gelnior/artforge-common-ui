package fr.hd3d.common.ui.client.service.store;

import java.util.List;

import com.extjs.gxt.ui.client.data.BaseListLoader;
import com.extjs.gxt.ui.client.data.ListLoadConfig;
import com.extjs.gxt.ui.client.data.ListLoadResult;
import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.LoadListener;
import com.extjs.gxt.ui.client.store.GroupingStore;

import fr.hd3d.common.ui.client.listener.EventLoadListener;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.service.parameter.EqConstraint;
import fr.hd3d.common.ui.client.service.parameter.IUrlParameter;
import fr.hd3d.common.ui.client.service.parameter.InConstraint;
import fr.hd3d.common.ui.client.service.parameter.Pagination;
import fr.hd3d.common.ui.client.service.proxy.BaseProxy;


/**
 * BaseStore provides base functionality to connect GXT list stores to HD3D web services.
 * 
 * @author HD3D
 * 
 * @param <M>
 */
public abstract class BaseStore<M extends Hd3dModelData> extends GroupingStore<M>
{
    /** Proxy needed by store to access HD3D services. */
    protected BaseProxy<M> proxy;

    /**
     * @return path on which store is connected.
     */
    public String getPath()
    {
        return this.proxy.getPath();
    }

    /**
     * Set path to be used to access services.
     * 
     * @param path
     *            The path to set.
     */
    public void setPath(String path)
    {
        this.proxy.setPath(path);
    }

    /**
     * Refresh store data with data from services.
     */
    public abstract void reload();

    /**
     * Refresh store data with data from services using configuration given in parameter.
     * 
     * @param config
     *            Configuration to use for relaoding.
     */
    public void reload(ListLoadConfig config)
    {
        this.loader.load(config);
    }

    /**
     * Add a load listener to the store loader.
     * 
     * @param listener
     *            The loader to add.
     */
    public void addLoadListener(LoadListener listener)
    {
        this.loader.addLoadListener(listener);
    }

    /**
     * Add a load listener that forwards <i>event</i> when loading is finished.
     * 
     * @param event
     *            The event to forward.
     */
    public void addEventLoadListener(EventType event)
    {
        this.loader.addLoadListener(new EventLoadListener(event, this));
    }

    /**
     * Remove load listener given in parameter from loader listeners.
     * 
     * @param listener
     *            The listener to remove;
     */
    public void removeLoadListener(LoadListener listener)
    {
        this.loader.removeLoadListener(listener);
    }

    public void setLoader(BaseListLoader<ListLoadResult<M>> loader)
    {
        this.loader = loader;
    }

    /**
     * Add a parameter to the URL used to access to services.
     * 
     * @param parameter
     *            The parameter to add.
     */
    public void addParameter(IUrlParameter parameter)
    {
        this.proxy.addParameter(parameter);
    }

    /**
     * Add an equal constraint parameter to the URL used to access to services.
     * 
     * @param column
     *            Column on which equality occurs.
     * @param value
     *            The value to match.
     */
    public void addEqConstraint(String column, Object value)
    {
        this.addParameter(new EqConstraint(column, value));
    }

    /**
     * Add an in constraint parameter to the URL used to access to services.
     * 
     * @param column
     *            Column on which in occurs.
     * @param value
     *            The value to match.
     */
    public void addInConstraint(String column, Object value)
    {
        this.addParameter(new InConstraint(column, value));
    }

    /**
     * Remove a parameter to the URL used to access to services.
     * 
     * @param parameter
     *            The parameter to remove.
     */
    public void removeParameter(IUrlParameter parameter)
    {
        this.proxy.removeParameter(parameter);
    }

    /**
     * Add a pagination parameter thats begins at <i>start</i> index. Quantity is the number of records to retrieve.
     * 
     * @param start
     *            Start index.
     * @param quantity
     *            Number of records to retrieve.
     */
    public void addPagination(int start, int quantity)
    {
        this.proxy.addParameter(new Pagination(Long.valueOf(start), Long.valueOf(quantity)));
    }

    /**
     * Clear all parameters set on the URL used to access services.
     */
    public void clearParameters()
    {
        this.proxy.clearParameters();
    }

    /**
     * Remove from store all models contained in <i>models</i>.
     * 
     * @param models
     *            The models to remove.
     */
    public void remove(List<M> models)
    {
        for (M model : models)
        {
            this.remove(model);
        }
    }

    /**
     * @param parameter
     *            The parameter to test.
     * @return True if parameter is set in store, false either.
     */
    public boolean containsParameter(IUrlParameter parameter)
    {
        return this.proxy.getParameters().contains(parameter);
    }

    /** Set default load listener. */
    protected void setLoadListener()
    {
        this.loader.addLoadListener(new LoadListener() {
            @Override
            public void loaderBeforeLoad(LoadEvent le)
            {
                onBeforeLoad(le);
            }

            @Override
            public void loaderLoad(LoadEvent le)
            {
                onLoad(le);
            }

            @Override
            public void loaderLoadException(LoadEvent le)
            {
                onLoadException(le);
            }
        });
    }

}
