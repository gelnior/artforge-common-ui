package fr.hd3d.common.ui.client.service.store;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.LoadListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.callback.MaxRecordCallback;
import fr.hd3d.common.ui.client.service.parameter.Pagination;


/**
 * Listener to load all data to the store despite the fact services return only 300 rows by request. It makes the needed
 * amount of request to services to get the whole list of records. <br>
 * Ex: If a request should return 500 records and server limit is 300 records, this listener will perform a second
 * request after first one finishes to retrieve 300 first rows + 200 next rows.
 * 
 * @author HD3D
 * 
 * @param <T>
 */
public class AllLoadListener<T extends Hd3dModelData> extends LoadListener
{
    /** List containing all data. */
    private final ArrayList<T> dataList = new ArrayList<T>();
    /** Store which loads the data. */
    private final ServiceStore<T> store;
    /** Event forwarded after all data load. */
    private final AppEvent loadEvent;
    /** Pagination defines the number of data loaded for each reload. */
    private Pagination pagination;

    /**
     * Default constructor.
     * 
     * @param _store
     *            the store which load data.
     * @param loadEvent
     *            the event to forward when all data are loaded.
     */
    public AllLoadListener(ServiceStore<T> _store, AppEvent loadEvent)
    {
        this.store = _store;
        this.loadEvent = loadEvent;

        String path = ServicesURI.MAXRECORD;
        RestRequestHandlerSingleton.getInstance().getRequest(path, new MaxRecordCallback(new AppEvent(null)) {
            @Override
            protected void onSuccess(Request request, Response response)
            {
                long maxRecord;
                try
                {
                    maxRecord = getMaxRecord(response);
                    pagination = new Pagination(0L, maxRecord);
                    store.addParameter(pagination);
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        });
    }

    public AllLoadListener(ServiceStore<T> store, EventType loadEvent)
    {
        this(store, new AppEvent(loadEvent));
    }

    @Override
    public void loaderLoad(LoadEvent le)
    {
        List<T> tmpList = store.getModels();
        dataList.addAll(tmpList);
        if (!(tmpList.size() < pagination.getQuantity()))
        {
            store.removeParameter(pagination);
            pagination.setFirst(pagination.getFirst() + pagination.getQuantity());
            store.addParameter(pagination);
            store.reload();
        }
        else
        {
            store.removeAll();
            pagination.setFirst(0L);
            store.add(dataList);
            dataList.clear();
            EventDispatcher.forwardEvent(loadEvent);
        }
    }
}
