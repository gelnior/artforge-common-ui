package fr.hd3d.common.ui.client.service.store;

import com.extjs.gxt.ui.client.data.BaseListLoader;
import com.extjs.gxt.ui.client.data.ListLoadResult;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.service.proxy.ServicesProxy;


/**
 * Store connected to a specific HD3D web service.
 * 
 * @author HD3D
 * 
 * @param <M>
 */
public class ServiceStore<M extends Hd3dModelData> extends BaseStore<M>
{

    /**
     * Default constructor : set proxy and loader to access services. Also add a load listener accessible through
     * protected methods.
     * 
     * @param reader
     *            The reader needed to parse data.
     */
    public ServiceStore(IReader<M> reader)
    {
        this.proxy = new ServicesProxy<M>(reader);
        this.loader = new BaseListLoader<ListLoadResult<M>>((ServicesProxy<M>) proxy);

        this.setLoadListener();
    }

    @Override
    public void reload()
    {
        this.loader.load();
    }

    /**
     * @return Proxy used by store to access services.
     */
    public ServicesProxy<M> getProxy()
    {
        return (ServicesProxy<M>) this.proxy;
    }

    /** @return Reader used to parse data. */
    public IReader<M> getReader()
    {
        return getProxy().getReader();
    }

}
