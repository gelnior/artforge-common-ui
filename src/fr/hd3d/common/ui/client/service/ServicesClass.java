package fr.hd3d.common.ui.client.service;

import java.util.HashMap;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.asset.AssetRevisionGroupModelData;
import fr.hd3d.common.ui.client.modeldata.asset.FileRevisionLinkModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerTypeModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.DeviceModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.DeviceModelModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.DeviceTypeModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.LicenseModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ManufacturerModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.PoolModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ProcessorModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.RoomModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ScreenModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ScreenModelModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.SoftwareModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;
import fr.hd3d.common.ui.client.modeldata.production.CompositionModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectTypeModelData;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.modeldata.production.StepModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.common.ui.client.modeldata.task.EntityTaskLinkModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.modeldata.technical.RoleModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetFilterModelData;
import fr.hd3d.common.ui.client.modeldata.technical.TagModelData;


/**
 * Class that gives lightweight class for a given simple class name.<br>
 * Ex : "Computer" => "fr.hd3d.model.lightweight.impl.LComputer"
 * 
 * @author HD3D
 */
public class ServicesClass
{
    private final static HashMap<String, String> map = new HashMap<String, String>();

    public static void initClass()
    {
        map.put(ComputerModelData.SIMPLE_CLASS_NAME.toLowerCase(), ComputerModelData.CLASS_NAME);
        map.put(SoftwareModelData.SIMPLE_CLASS_NAME.toLowerCase(), SoftwareModelData.CLASS_NAME);
        map.put(LicenseModelData.SIMPLE_CLASS_NAME.toLowerCase(), LicenseModelData.CLASS_NAME);
        map.put(DeviceModelData.SIMPLE_CLASS_NAME.toLowerCase(), DeviceModelData.CLASS_NAME);
        map.put(ScreenModelData.SIMPLE_CLASS_NAME.toLowerCase(), ScreenModelData.CLASS_NAME);
        map.put(PoolModelData.SIMPLE_CLASS_NAME.toLowerCase(), PoolModelData.CLASS_NAME);
        map.put(ComputerModelModelData.SIMPLE_CLASS_NAME.toLowerCase(), ComputerModelModelData.CLASS_NAME);
        map.put(DeviceModelModelData.SIMPLE_CLASS_NAME.toLowerCase(), DeviceModelModelData.CLASS_NAME);
        map.put(ScreenModelModelData.SIMPLE_CLASS_NAME.toLowerCase(), ScreenModelModelData.CLASS_NAME);
        map.put(ManufacturerModelData.SIMPLE_CLASS_NAME.toLowerCase(), ManufacturerModelData.CLASS_NAME);
        map.put(ProcessorModelData.SIMPLE_CLASS_NAME.toLowerCase(), ProcessorModelData.CLASS_NAME);
        map.put(DeviceTypeModelData.SIMPLE_CLASS_NAME.toLowerCase(), DeviceTypeModelData.CLASS_NAME);
        map.put(ComputerTypeModelData.SIMPLE_CLASS_NAME.toLowerCase(), ComputerTypeModelData.CLASS_NAME);
        map.put(RoomModelData.SIMPLE_CLASS_NAME.toLowerCase(), RoomModelData.CLASS_NAME);

        map.put(ShotModelData.SIMPLE_CLASS_NAME.toLowerCase(), ShotModelData.CLASS_NAME);
        map.put(ConstituentModelData.SIMPLE_CLASS_NAME.toLowerCase(), ConstituentModelData.CLASS_NAME);
        map.put(CompositionModelData.SIMPLE_CLASS_NAME.toLowerCase(), CompositionModelData.CLASS_NAME);
        map.put(CategoryModelData.SIMPLE_CLASS_NAME.toLowerCase(), CategoryModelData.CLASS_NAME);
        map.put(SequenceModelData.SIMPLE_CLASS_NAME.toLowerCase(), SequenceModelData.CLASS_NAME);
        map.put(RoleModelData.SIMPLE_CLASS_NAME.toLowerCase(), RoleModelData.CLASS_NAME);
        map.put(ProjectModelData.SIMPLE_CLASS_NAME.toLowerCase(), ProjectModelData.CLASS_NAME);
        map.put(ProjectTypeModelData.SIMPLE_CLASS_NAME.toLowerCase(), ProjectTypeModelData.CLASS_NAME);
        map.put(ResourceGroupModelData.SIMPLE_CLASS_NAME.toLowerCase(), ResourceGroupModelData.CLASS_NAME);
        map.put("group", ResourceGroupModelData.CLASS_NAME);
        map.put(PersonModelData.SIMPLE_CLASS_NAME.toLowerCase(), PersonModelData.CLASS_NAME);
        map.put(TaskModelData.SIMPLE_CLASS_NAME.toLowerCase(), TaskModelData.CLASS_NAME);
        map.put(TaskTypeModelData.SIMPLE_CLASS_NAME.toLowerCase(), TaskTypeModelData.CLASS_NAME);
        map.put(TagModelData.SIMPLE_CLASS_NAME.toLowerCase(), TagModelData.CLASS_NAME);
        map.put(EntityTaskLinkModelData.SIMPLE_CLASS_NAME.toLowerCase(), EntityTaskLinkModelData.CLASS_NAME);

        map.put(FileRevisionLinkModelData.SIMPLE_CLASS_NAME.toLowerCase(), FileRevisionLinkModelData.CLASS_NAME);
        map.put(AssetRevisionGroupModelData.SIMPLE_CLASS_NAME.toLowerCase(), AssetRevisionGroupModelData.CLASS_NAME);
        // map.put(AssetRevisionFileRevisionModelData.SIMPLE_CLASS_NAME.toLowerCase(),
        // AssetRevisionFileRevisionModelData.CLASS_NAME);
        map.put(AssetRevisionGroupModelData.SIMPLE_CLASS_NAME.toLowerCase(), AssetRevisionGroupModelData.CLASS_NAME);

        map.put(EntityTaskLinkModelData.SIMPLE_CLASS_NAME.toLowerCase(), EntityTaskLinkModelData.CLASS_NAME);
        map.put(ApprovalNoteModelData.SIMPLE_CLASS_NAME.toLowerCase(), ApprovalNoteModelData.CLASS_NAME);
        map.put(SheetFilterModelData.SIMPLE_CLASS_NAME.toLowerCase(), SheetFilterModelData.CLASS_NAME);
        map.put(StepModelData.SIMPLE_CLASS_NAME.toLowerCase(), StepModelData.CLASS_NAME);
    }

    public static String getClass(String simpleClassName)
    {
        if (simpleClassName != null)
        {
            return map.get(simpleClassName.toLowerCase());
        }
        else
        {
            return null;
        }
    }

    public static boolean contains(String simpleClassName)
    {
        if (simpleClassName != null)
        {
            return map.keySet().contains(simpleClassName.toLowerCase());
        }
        else
        {
            return false;
        }
    }

    /**
     * @param model
     *            The model to test.
     * @return True if model class name correspond to Category class name.
     */
    public static boolean isCategory(Hd3dModelData model)
    {
        return CategoryModelData.CLASS_NAME.equals(model.getClassName());
    }

    /**
     * @param model
     *            The model to test.
     * @return True if model class name correspond to Constituent class name.
     */
    public static boolean isConstituent(Hd3dModelData model)
    {
        return ConstituentModelData.CLASS_NAME.equals(model.getClassName());
    }

    /**
     * @param model
     *            The model to test.
     * @return True if model class name correspond to Shot class name.
     */
    public static boolean isShot(Hd3dModelData model)
    {
        return ShotModelData.CLASS_NAME.equals(model.getClassName());
    }

    /**
     * @param model
     *            The model to test.
     * @return True if model class name correspond to Sequence class name.
     */
    public static boolean isSequence(Hd3dModelData model)
    {
        return SequenceModelData.CLASS_NAME.equals(model.getClassName());
    }
}
