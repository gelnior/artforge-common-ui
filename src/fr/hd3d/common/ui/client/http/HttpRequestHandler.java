package fr.hd3d.common.ui.client.http;

import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;


/**
 * This singleton gives simple method to send HTTP requests for file retrieval.
 * 
 * @author HD3D
 */
public final class HttpRequestHandler implements IHttpRequestHandler
{
    /**
     * Send a GET request to the specified URL.
     * 
     * @param url
     *            The request URL.
     * @param callback
     *            The callback that handles the response.
     * @throws Exception
     *             Raise if an error occur while sending the request.
     */
    public void getRequest(final String url, final RequestCallback callback) throws Exception
    {
        RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, url);
        builder.sendRequest(null, callback);
    }
}
