package fr.hd3d.common.ui.client.http;

import com.google.gwt.http.client.RequestCallback;


/**
 * Interface for HTTP request handler (needed for mocking HTTP request handler).
 * 
 * @author HD3D
 */
public interface IHttpRequestHandler
{
    public void getRequest(final String url, final RequestCallback callback) throws Exception;
}
