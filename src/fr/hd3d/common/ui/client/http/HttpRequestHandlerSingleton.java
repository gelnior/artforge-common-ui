package fr.hd3d.common.ui.client.http;



/**
 * Singleton needed to switch between real HTTP request handler and mock request handler.
 * 
 * @author HD3D
 */
public class HttpRequestHandlerSingleton
{
    /** The request handler instance to return. */
    private static IHttpRequestHandler requestHandler;

    /**
     * @return The set request handler instance. If it is null, it returns a new real HTTP request handler.
     */
    public final static IHttpRequestHandler getInstance()
    {
        if (requestHandler == null)
        {
            requestHandler = new HttpRequestHandler();
        }
        return requestHandler;
    }

    /**
     * Set the instance
     * 
     * @param mock
     *            The request handler to set.
     */
    public final static void setInstanceAsMock(IHttpRequestHandler mock)
    {
        requestHandler = mock;
    }
}
