package fr.hd3d.common.ui.client.modeldata;

import com.extjs.gxt.ui.client.data.BaseModelData;


public class InfoFieldModelData extends BaseModelData
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    /** Name of the field. */
    public static final String NAME_FIELD = "name";
    /** Value of the field. */
    public static final String HUMAN_NAME_FIELD = "human-name";

    /**
     * Default constructor.
     * 
     * @param name
     *            Field name.
     * @param value
     *            Field value.
     */
    public InfoFieldModelData(String name, Object value)
    {
        this.setName(name);
        this.setValue(value);
    }

    public String getName()
    {
        return get(NAME_FIELD);
    }

    public void setName(String name)
    {
        set(NAME_FIELD, name);
    }

    public Object getValue()
    {
        return get(HUMAN_NAME_FIELD);
    }

    public void setValue(Object value)
    {
        set(HUMAN_NAME_FIELD, value);
    }
}
