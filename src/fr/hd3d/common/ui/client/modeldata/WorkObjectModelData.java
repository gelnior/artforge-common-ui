package fr.hd3d.common.ui.client.modeldata;

/**
 * Base class for work objects.
 * 
 * @author HD3D
 */
public class WorkObjectModelData extends Hd3dModelData
{

    private static final long serialVersionUID = -3695748690696449982L;

    public static final String LABEL_FIELD = "label";
    public static final String DESCRIPTION_FIELD = "description";
    public static final String THUMBNAIL_FIELD = "thumbnail";

}
