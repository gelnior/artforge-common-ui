package fr.hd3d.common.ui.client.modeldata.production;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;


public class ApprovalNoteTypeModelData extends RecordModelData
{

    /** Automatically generated serial version number. */
    private static final long serialVersionUID = 1227503772523921884L;

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LApprovalNoteType";
    public static final String SIMPLE_CLASS_NAME = "approvalNoteType";

    public static final String PROJECT_FIELD = "project";
    public static final String TASK_TYPE_FIELD = "taskType";
    public static final String TASK_TYPE_NAME_FIELD = "taskTypeName";
    public static final String TASK_TYPE_COLOR_FIELD = "taskTypeColor";

    public static final String ITEM_QUERY = "fr.hd3d.services.resources.collectionquery.ApprovalNotesCollectionQuery";
    public static final String ITEM_TYPE = "collectionQuery";
    public static final String ITEM_META_TYPE = "ApprovalNote";

    /**
     * Default Constructor
     */
    public ApprovalNoteTypeModelData()
    {
        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public ApprovalNoteTypeModelData(String name, Long projectId, Long taskTypeId)
    {
        this();

        this.setName(name);
        this.setProject(projectId);
        this.setTaskType(taskTypeId);
    }

    public void setProject(Long project)
    {
        this.set(PROJECT_FIELD, project);
    }

    public void setTaskType(Long taskType)
    {
        this.set(TASK_TYPE_FIELD, taskType);
    }

    public Long getProject()
    {
        return this.get(PROJECT_FIELD);
    }

    public Long getTaskType()
    {
        return this.get(TASK_TYPE_FIELD);
    }

    public String getTaskTypeName()
    {
        return this.get(TASK_TYPE_NAME_FIELD);
    }

    public void setTaskTypeName(String taskTypeName)
    {
        this.set(TASK_TYPE_NAME_FIELD, taskTypeName);
    }

    public String getTaskTypeColor()
    {
        return this.get(TASK_TYPE_COLOR_FIELD);
    }

    public static ModelType getModelType()
    {
        ModelType modelType = Hd3dModelData.getModelType();

        modelType.addField(NAME_FIELD);

        DataField projectField = new DataField(PROJECT_FIELD);
        projectField.setType(Long.class);
        modelType.addField(projectField);

        DataField taskTypeField = new DataField(TASK_TYPE_FIELD);
        taskTypeField.setType(Long.class);
        modelType.addField(taskTypeField);

        modelType.addField(TASK_TYPE_NAME_FIELD);
        modelType.addField(TASK_TYPE_COLOR_FIELD);

        return modelType;
    }
}
