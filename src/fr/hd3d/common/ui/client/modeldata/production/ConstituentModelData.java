package fr.hd3d.common.ui.client.modeldata.production;

import java.util.List;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;


public class ConstituentModelData extends RecordModelData
{
    private static final long serialVersionUID = -8907904483900945052L;

    public static final String CONSTITUENT_HOOK = fr.hd3d.common.client.LConstituent.CONSTITUENT_HOOK;
    public static final String CONSTITUENT_CATEGORY = fr.hd3d.common.client.LConstituent.CONSTITUENT_CATEGORY;
    public static final String CONSTITUENT_LABEL = fr.hd3d.common.client.LConstituent.CONSTITUENT_LABEL;
    public static final String CONSTITUENT_DESCRIPTION = fr.hd3d.common.client.LConstituent.CONSTITUENT_DESCRIPTION;
    public static final String CONSTITUENT_DIFFICULTY = fr.hd3d.common.client.LConstituent.CONSTITUENT_DIFFICULTY;
    public static final String CONSTITUENT_TRUST = fr.hd3d.common.client.LConstituent.CONSTITUENT_TRUST;
    public static final String CONSTITUENT_DESIGN = fr.hd3d.common.client.LConstituent.CONSTITUENT_DESIGN;
    public static final String CONSTITUENT_MODELING = fr.hd3d.common.client.LConstituent.CONSTITUENT_MODELING;
    public static final String CONSTITUENT_SETUP = fr.hd3d.common.client.LConstituent.CONSTITUENT_SETUP;
    public static final String CONSTITUENT_TEXTURING = fr.hd3d.common.client.LConstituent.CONSTITUENT_TEXTURING;
    public static final String CONSTITUENT_SHADING = fr.hd3d.common.client.LConstituent.CONSTITUENT_SHADING;
    public static final String CONSTITUENT_HAIRS = fr.hd3d.common.client.LConstituent.CONSTITUENT_HAIRS;
    public static final String CONSTITUENT_PARENTLABEL = fr.hd3d.common.client.LConstituent.CONSTITUENT_PARENTLABEL;
    public static final String CONSTITUENT_TAGS = fr.hd3d.common.client.LConstituent.CONSTITUENT_TAGS;
    public static final String CONSTITUENT_CATEGORIESNAMES = fr.hd3d.common.client.LConstituent.CONSTITUENT_CATEGORIESNAMES;

    public static final String SIMPLE_CLASS_NAME = "Constituent";
    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LConstituent";

    public ConstituentModelData()
    {
        super();

        // set("class", "fr.hd3d.model.lightweight.impl.LConstituent");
        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public ConstituentModelData(String name, CategoryModelData category)
    {
        this();
        this.setLabel(name);
        this.setCategory(category.getId());
        this.setDefaultPath(category.getProject(), category.getId());
    }

    public void setTags(List<Long> tags)
    {
        set(CONSTITUENT_TAGS, tags);
    }

    public List<Long> getTags()
    {
        List<Long> allTags = get(CONSTITUENT_TAGS);
        return allTags;
    }

    public void setHook(String hook)
    {
        set(CONSTITUENT_HOOK, hook);
    }

    public void setCategory(Long category)
    {
        set(CONSTITUENT_CATEGORY, category);
    }

    public void setLabel(String label)
    {
        set(CONSTITUENT_LABEL, label);
    }

    public void setDescription(String description)
    {
        set(CONSTITUENT_DESCRIPTION, description);
    }

    public String getHook()
    {
        return get(CONSTITUENT_HOOK);
    }

    public Long getCategory()
    {
        return (Long) get(CONSTITUENT_CATEGORY);
    }

    public String getLabel()
    {
        return get(CONSTITUENT_LABEL);
    }

    public String getDescription()
    {
        return get(CONSTITUENT_DESCRIPTION);
    }

    public int getDifficulty()
    {
        return (Integer) get(CONSTITUENT_DIFFICULTY);
    }

    public void setDifficulty(int difficulty)
    {
        set(CONSTITUENT_DIFFICULTY, difficulty);
    }

    public long getTrust()
    {
        return (Long) get(CONSTITUENT_TRUST);
    }

    public void setTrust(long trust)
    {
        set(CONSTITUENT_TRUST, trust);
    }

    public String getParentLabel()
    {
        return get(CONSTITUENT_PARENTLABEL);
    }

    public void setParentLabel(String parentLabel)
    {
        set(CONSTITUENT_PARENTLABEL, parentLabel);
    }

    public void setDesign(long design)
    {
        set(CONSTITUENT_DESIGN, design);
    }

    public void setModeling(Boolean modeling)
    {
        set(CONSTITUENT_MODELING, modeling);
    }

    public void setSetup(Boolean setup)
    {
        set(CONSTITUENT_SETUP, setup);
    }

    public void setTexturing(Boolean texturing)
    {
        set(CONSTITUENT_TEXTURING, texturing);
    }

    public void setShading(Boolean shading)
    {
        set(CONSTITUENT_SHADING, shading);
    }

    public void setHairs(Boolean hairs)
    {
        set(CONSTITUENT_HAIRS, hairs);
    }

    public Boolean getDesign()
    {
        return (Boolean) get(CONSTITUENT_DESIGN);
    }

    public Boolean getModeling()
    {
        return (Boolean) get(CONSTITUENT_MODELING);
    }

    public Boolean getSetup()
    {
        return (Boolean) get(CONSTITUENT_SETUP);
    }

    public Boolean getTexturing()
    {
        return (Boolean) get(CONSTITUENT_TEXTURING);
    }

    public Boolean getShading()
    {
        return (Boolean) get(CONSTITUENT_SHADING);
    }

    public Boolean getHairs()
    {
        return (Boolean) get(CONSTITUENT_HAIRS);
    }

    public String getCategoriesNames()
    {
        return get(CONSTITUENT_CATEGORIESNAMES);
    }

    public void setDefaultPath(Long projectId, Long categoryId)
    {
        this.setDefaultPath(ServicesPath.PROJECTS + projectId + "/" + ServicesPath.CATEGORIES + categoryId + "/"
                + ServicesPath.CONSTITUENTS);
    }

    public static ModelType getModelType()
    {
        ModelType modelType = RecordModelData.getModelType();

        modelType.addField(CONSTITUENT_HOOK);

        DataField category = new DataField(CONSTITUENT_CATEGORY);
        DataField diff = new DataField(CONSTITUENT_DIFFICULTY);
        DataField trust = new DataField(CONSTITUENT_TRUST);
        DataField design = new DataField(CONSTITUENT_DESIGN);
        DataField modeling = new DataField(CONSTITUENT_MODELING);
        DataField setup = new DataField(CONSTITUENT_SETUP);
        DataField texturing = new DataField(CONSTITUENT_TEXTURING);
        DataField shading = new DataField(CONSTITUENT_SHADING);
        DataField hairs = new DataField(CONSTITUENT_HAIRS);
        DataField parentlabel = new DataField(CONSTITUENT_PARENTLABEL);
        DataField tags = new DataField(CONSTITUENT_TAGS);

        category.setType(Long.class);
        diff.setType(Integer.class);
        trust.setType(Long.class);

        design.setType(Boolean.class);
        modeling.setType(Boolean.class);
        setup.setType(Boolean.class);
        texturing.setType(Boolean.class);
        shading.setType(Boolean.class);
        hairs.setType(Boolean.class);

        parentlabel.setType(Long.class);
        tags.setType(List.class);

        modelType.addField(category);
        modelType.addField(CONSTITUENT_LABEL);
        modelType.addField(CONSTITUENT_DESCRIPTION);
        modelType.addField(diff);
        modelType.addField(trust);
        modelType.addField(design);
        modelType.addField(modeling);
        modelType.addField(setup);
        modelType.addField(texturing);
        modelType.addField(shading);
        modelType.addField(hairs);
        modelType.addField(parentlabel);
        modelType.addField(tags);
        modelType.addField(CONSTITUENT_CATEGORIESNAMES);

        return modelType;
    }

}
