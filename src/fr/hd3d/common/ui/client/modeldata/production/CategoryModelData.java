package fr.hd3d.common.ui.client.modeldata.production;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.data.ModelType;
import com.extjs.gxt.ui.client.event.LoadListener;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.reader.CategoryReader;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.store.ServiceStore;


public class CategoryModelData extends RecordModelData
{
    private static final long serialVersionUID = 11485281406632134L;
    public static final String CATEGORY_PROJECT = "project";
    public static final String CATEGORY_PARENT = "parent";

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LCategory";
    public static final String SIMPLE_CLASS_NAME = "Category";

    protected final List<Long> childrenIds = new ArrayList<Long>();
    private int categoriesLoading = 0;

    public CategoryModelData()
    {
        super();

        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public CategoryModelData(String name)
    {
        this();

        this.setName(name);
    }

    public CategoryModelData(Long id, String name)
    {
        this();
        this.setId(id);
        this.setName(name);
    }

    public CategoryModelData(String name, Long projectId, Long parentId)
    {
        this();
        this.setName(name);
        this.setProject(projectId);
        this.setParent(parentId);
    }

    public Long getParent()
    {
        return get(CATEGORY_PARENT);
    }

    public void setParent(Long parent)
    {
        set(CATEGORY_PARENT, parent);
    }

    public Long getProject()
    {
        return get(CATEGORY_PROJECT);
    }

    public void setProject(Long project)
    {
        set(CATEGORY_PROJECT, project);
    }

    @Override
    public String getPath()
    {
        if (getDefaultPath() != null)
        {
            return getDefaultPath();
        }
        else
        {
            String path = ServicesPath.PROJECTS + this.getProject() + "/" + ServicesPath.CATEGORIES;
            if (this.getId() != null)
            {
                path += this.getId();
            }
            return path;
        }
    }

    @Override
    public String getPermissionPath()
    {
        return getPath().replace("/", ":");
    }

    public static ModelType getModelType()
    {
        ModelType modelType = RecordModelData.getModelType();

        DataField project = new DataField(CATEGORY_PROJECT);
        project.setType(Long.class);
        modelType.addField(project);

        DataField parent = new DataField(CATEGORY_PARENT);
        parent.setType(Long.class);
        modelType.addField(parent);

        return modelType;
    }

    /** @return children category id list field. List contains parent id too. */
    public List<Long> getChildrenIds()
    {
        return childrenIds;
    }

    /** Load children ids from services inside children id list field. List contains parent id too. */
    public void loadChildrenIds()
    {
        if (childrenIds.size() > 0)
        {
            EventDispatcher.forwardEvent(CommonEvents.CATEGORY_CHILDREN_LOADED, childrenIds);
        }
        else
        {
            categoriesLoading = 0;
            this.retrieveCategoryIds(this);
        }
    }

    /**
     * Recursive method : it retrieves children for category. Then children ids are stored inside the ids list of top
     * parent category. Then the method is called again for each children. When categories children are retrieved, the
     * top parent category is notified. When all children are loaded the CATEGORY_CHILDREN_LOADED event is raised.
     * 
     * @param category
     *            parent category.
     */
    public void retrieveCategoryIds(CategoryModelData category)
    {
        this.childrenIds.add(category.getId());
        categoriesLoading++;
        Constraint parentConstraint = new Constraint(EConstraintOperator.eq, CategoryModelData.CATEGORY_PARENT,
                category.getId());
        final ServiceStore<CategoryModelData> store = new ServiceStore<CategoryModelData>(new CategoryReader());
        store.setPath(ServicesPath.PROJECTS + category.getProject() + "/" + ServicesPath.CATEGORIES + ServicesPath.ALL);
        store.addParameter(parentConstraint);

        store.addLoadListener(new LoadListener() {
            @Override
            public void loaderLoad(LoadEvent le)
            {
                for (CategoryModelData category : store.getModels())
                {
                    retrieveCategoryIds(category);
                }
                categoriesLoading--;

                if (store.getCount() == 0 && categoriesLoading == 0)
                {
                    EventDispatcher.forwardEvent(CommonEvents.CATEGORY_CHILDREN_LOADED, childrenIds);
                }
            }
        });
        store.reload();
    }

}
