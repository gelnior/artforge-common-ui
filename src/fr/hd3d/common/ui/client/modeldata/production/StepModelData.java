package fr.hd3d.common.ui.client.modeldata.production;

import com.extjs.gxt.ui.client.data.ModelType;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.util.field.BooleanDataField;
import fr.hd3d.common.ui.client.util.field.LongDataField;
import fr.hd3d.common.ui.client.util.field.StringDataField;


public class StepModelData extends Hd3dModelData
{
    /** Automatically generated serial version number. */
    private static final long serialVersionUID = 5800576508146243604L;

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LStep";
    public static final String SIMPLE_CLASS_NAME = "Step";

    public static final String BOUND_ENTITY_ID_FIELD = "boundEntity";
    public static final String BOUND_ENTITY_NAME_FIELD = "boundEntityName";
    public static final String TASK_TYPE_ID_FIELD = "taskType";
    public static final String TASK_TYPE_NAME_FIELD = "taskTypeName";
    public static final String CREATE_TASK_N_ASSET_FIELD = "createTasknAsset";
    public static final String ESTIMATED_DURATION_FIELD = "estimatedDuration";

    /**
     * Default constructor : sets an empty computer with no name.
     */
    public StepModelData()
    {
        super();
        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public StepModelData(Hd3dModelData model, String taskTypeIdString, Long estimatedDuration)
    {
        this();
        Long taskTypeId = Long.parseLong(taskTypeIdString);
        Long workObjectId = model.getId();
        String workObjectClass = model.getSimpleClassName();

        this.setTaskTypeId(taskTypeId);
        this.setBoundEntityId(workObjectId);
        this.setBoundEntityName(workObjectClass);

        this.setCreateTaskNAsset(Boolean.TRUE);
        this.setEstimatedDuration(estimatedDuration);

    }

    /**
     * @return Model type used by reader to handle the computer model data structure.
     */
    public static ModelType getModelType()
    {
        ModelType type = Hd3dModelData.getModelType();

        type.addField(new LongDataField(BOUND_ENTITY_ID_FIELD));
        type.addField(new StringDataField(BOUND_ENTITY_NAME_FIELD));
        type.addField(new LongDataField(TASK_TYPE_ID_FIELD));
        type.addField(new StringDataField(TASK_TYPE_NAME_FIELD));
        type.addField(new BooleanDataField(CREATE_TASK_N_ASSET_FIELD));
        type.addField(new LongDataField(ESTIMATED_DURATION_FIELD));

        return type;
    }

    public Long getBoundEntityId()
    {
        return this.get(BOUND_ENTITY_ID_FIELD);
    }

    public void setBoundEntityId(Long boundEntityId)
    {
        this.set(BOUND_ENTITY_ID_FIELD, boundEntityId);
    }

    public String getBoundEntityName()
    {
        return this.get(BOUND_ENTITY_NAME_FIELD);
    }

    public void setBoundEntityName(String boundEntityName)
    {
        this.set(BOUND_ENTITY_NAME_FIELD, boundEntityName);
    }

    public Long getTaskTypeId()
    {
        return this.get(TASK_TYPE_ID_FIELD);
    }

    public void setTaskTypeId(Long taskTypeId)
    {
        this.set(TASK_TYPE_ID_FIELD, taskTypeId);
    }

    public String getTaskTypeName()
    {
        return this.get(TASK_TYPE_NAME_FIELD);
    }

    public void setTaskTypeName(String taskTypeName)
    {
        this.set(TASK_TYPE_NAME_FIELD, taskTypeName);
    }

    public Boolean getCreateTaskNAsset()
    {
        return this.get(CREATE_TASK_N_ASSET_FIELD);
    }

    public void setCreateTaskNAsset(Boolean createTaskNAsset)
    {
        this.set(CREATE_TASK_N_ASSET_FIELD, createTaskNAsset);
    }

    public Long getEstimatedDuration()
    {
        return this.get(ESTIMATED_DURATION_FIELD);
    }

    public void setEstimatedDuration(Long estimatedDuration)
    {
        this.set(ESTIMATED_DURATION_FIELD, estimatedDuration);
    }

    @Override
    public String toString()
    {
        if (GWT.isScript())
        {
            if (getEstimatedDuration() != null)
                return getEstimatedDuration().toString();
            else
                return "0";
        }
        else
        {
            return "Step : " + getTaskTypeId() + " - " + getBoundEntityId();
        }
    }
}
