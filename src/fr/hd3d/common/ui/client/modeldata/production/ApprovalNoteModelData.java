package fr.hd3d.common.ui.client.modeldata.production;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.client.enums.EApprovalNoteStatus;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.util.field.DateDataField;
import fr.hd3d.common.ui.client.util.field.ListDataField;
import fr.hd3d.common.ui.client.util.field.LongDataField;


public class ApprovalNoteModelData extends Hd3dModelData
{
    private static final long serialVersionUID = 4501889292844819531L;
    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LApprovalNote";
    public static final String SIMPLE_CLASS_NAME = "ApprovalNote";

    public static final String APPROVAL_TYPE = "type";
    public static final String APPROVER_ID_FIELD = "approver";
    public static final String APPROVER_NAME_FIELD = "approverName";
    public static final String COMMENT_FIELD = "comment";
    public static final String DATE_FIELD = "approvalDate";
    public static final String STATUS_FIELD = "status";
    public static final String BOUND_ENTITY_ID = "boundEntity";
    public static final String BOUND_ENTITY_NAME = "boundEntityName";
    public static final String APPROVAL_TYPE_ID = "approvalNoteType";
    public static final String APPROVAL_TYPE_NAME = "approvalNoteTypeName";
    public static final String TASK_TYPE_ID = "taskTypeId";
    public static final String FILE_REVISIONS_ID = "filerevisions";

    public static final String STATUS_OK = "OK";
    public static final String STATUS_NOK = "NOK";
    public static final String STATUS_WFA = "Waiting for approval";
    public static final String STATUS_DONE = "Done";

    public static final String DISPLAY_STATUS_FIELD = "displayStatus";
    public static final String WORK_OBJECT_PATH = "workObjectPath";

    public static final String GRAPHICAL_ANNOTATION_FIELD = "graphicalAnnotationIDs";
    public static final String PROXIES_FIELD = "proxies";

    public static ModelType getModelType()
    {
        ModelType modelType = Hd3dModelData.getModelType();

        modelType.addField(new LongDataField(APPROVER_ID_FIELD));
        modelType.addField(new LongDataField(APPROVAL_TYPE_ID));
        modelType.addField(APPROVAL_TYPE_NAME);
        modelType.addField(APPROVER_NAME_FIELD);
        modelType.addField(new DateDataField(DATE_FIELD));
        modelType.addField(STATUS_FIELD);
        modelType.addField(COMMENT_FIELD);
        modelType.addField(new LongDataField(BOUND_ENTITY_ID));
        modelType.addField(BOUND_ENTITY_NAME);
        modelType.addField(new LongDataField(TASK_TYPE_ID));
        modelType.addField(new ListDataField(FILE_REVISIONS_ID));
        modelType.addField(new ListDataField(PROXIES_FIELD));
        modelType.addField(new ListDataField(GRAPHICAL_ANNOTATION_FIELD));

        return modelType;
    }

    public ApprovalNoteModelData()
    {
        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
        this.setProxies(new ArrayList<Long>());
    }

    public ApprovalNoteModelData(Date date, Long constituentId, Long approvalTypeId)
    {
        this();
        this.setDate(date);
        this.setApprovalNoteType(approvalTypeId);
        this.setBoundEntityId(constituentId);
    }

    public Long getApprovalNoteType()
    {
        return this.get(APPROVAL_TYPE_ID);
    }

    public void setApprovalNoteType(Long approvalNoteType)
    {
        this.set(APPROVAL_TYPE_ID, approvalNoteType);
    }

    public Long getApproverId()
    {
        return this.get(APPROVER_ID_FIELD);
    }

    public void setApproverId(Long approverId)
    {
        this.set(APPROVER_ID_FIELD, approverId);
    }

    public String getApproverName()
    {
        return this.get(APPROVER_NAME_FIELD);
    }

    public void setApproverName(String approverName)
    {
        this.set(APPROVER_NAME_FIELD, approverName);
    }

    public void setComment(String comment)
    {
        this.set(COMMENT_FIELD, comment);
    }

    public String getComment()
    {
        String comment = this.get(COMMENT_FIELD);
        if (comment == null)
            comment = "";
        return comment;
    }

    public void setStatus(String status)
    {
        this.set(STATUS_FIELD, status);
    }

    public String getStatus()
    {
        return this.get(STATUS_FIELD);
    }

    public void setDate(Date date)
    {
        this.set(DATE_FIELD, date);
    }

    public Date getDate()
    {
        return this.get(DATE_FIELD);
    }

    public String getBoundEntityName()
    {
        return this.get(BOUND_ENTITY_NAME);
    }

    public Long getBoundEntityId()
    {
        return this.get(BOUND_ENTITY_ID);
    }

    public void setBoundEntityId(Long entityId)
    {
        this.set(BOUND_ENTITY_ID, entityId);
    }

    public void setBoundEntityName(String entityName)
    {
        this.set(BOUND_ENTITY_NAME, entityName);
    }

    public void setColumnStatusDone()
    {
        this.set(ApprovalNoteModelData.STATUS_DONE, true);
    }

    public void setColumnStatusUndone()
    {
        this.set(ApprovalNoteModelData.STATUS_DONE, false);
    }

    public String getApprovalTypeName()
    {
        return this.get(APPROVAL_TYPE_NAME);
    }

    public void setApprovalTypeName(String typeName)
    {
        this.set(APPROVAL_TYPE_NAME, typeName);
    }

    public Long getTaskTypeId()
    {
        return this.get(TASK_TYPE_ID);
    }

    public void setTaskTypeId(Long taskTypeId)
    {
        this.set(TASK_TYPE_ID, taskTypeId);
    }

    public List<Long> getProxies()
    {
        return get(PROXIES_FIELD);
    }

    public void setProxies(List<Long> proxies)
    {
        this.set(PROXIES_FIELD, proxies);
    }

    public List<Long> getFileRevision()
    {
        return get(FILE_REVISIONS_ID);
    }

    public void setFileRevision(List<Long> fileRevisions)
    {
        this.set(FILE_REVISIONS_ID, fileRevisions);
    }

    /**
     * @return True if status is set to Done, else it returns false.
     */
    public boolean isStatusDone()
    {
        if (getStatus() == null)
            return false;
        return getStatus().equals(ApprovalNoteModelData.STATUS_DONE);
    }

    /**
     * @return True if status is set to Ok, else it returns false.
     */
    public boolean isStatusOk()
    {
        if (getStatus() == null)
            return false;
        return getStatus().equals(EApprovalNoteStatus.OK.name());
    }

    /**
     * @return True if status is set to NOK, else it returns false.
     */
    public boolean isStatusNOk()
    {
        if (getStatus() == null)
            return false;
        return getStatus().equals(ApprovalNoteModelData.STATUS_NOK);
    }

    /**
     * @return True if status is set to WAIT_APP, else it returns false.
     */
    public boolean isStatusWaitApp()
    {
        if (getStatus() == null)
            return false;
        return getStatus().equals(EApprovalNoteStatus.WAIT_APP.name());
    }

    /**
     * @return True if status is set to RETAKE, else it returns false.
     */
    public boolean isStatusRetake()
    {
        if (getStatus() == null)
            return false;
        return getStatus().equals(EApprovalNoteStatus.RETAKE.name());
    }

    /**
     * @return True if status is set to TO_DO, else it returns false.
     */
    public boolean isStatusTodo()
    {
        if (getStatus() == null)
            return false;
        return getStatus().equals(EApprovalNoteStatus.TO_DO.name());
    }

    /**
     * @return True if status is set to STAND_BY, else it returns false.
     */
    public boolean isStatusStandby()
    {
        if (getStatus() == null)
            return false;
        return getStatus().equals(EApprovalNoteStatus.STAND_BY.name());
    }

    /**
     * @return True if status is set to WIP, else it returns false.
     */
    public boolean isStatusWIP()
    {
        if (getStatus() == null)
            return false;
        return getStatus().equals(EApprovalNoteStatus.WORK_IN_PROGRESS.name());
    }

}
