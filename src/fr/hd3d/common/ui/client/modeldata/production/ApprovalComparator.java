package fr.hd3d.common.ui.client.modeldata.production;

import java.io.Serializable;
import java.util.Comparator;


/**
 * Used for approval collection comparison. Comparison is based on approval date.
 * 
 * @author HD3D
 */
public class ApprovalComparator implements Comparator<ApprovalNoteModelData>, Serializable
{
    private static final long serialVersionUID = 6467115458245486889L;

    public int compare(ApprovalNoteModelData m1, ApprovalNoteModelData m2)
    {
        Boolean val = m1.getDate().after(m2.getDate());
        return val == false ? 1 : -1;
    }
}
