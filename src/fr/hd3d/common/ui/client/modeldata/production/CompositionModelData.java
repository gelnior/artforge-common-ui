package fr.hd3d.common.ui.client.modeldata.production;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.callback.GetModelDataCallback;
import fr.hd3d.common.ui.client.service.parameter.Constraints;
import fr.hd3d.common.ui.client.service.parameter.EqConstraint;
import fr.hd3d.common.ui.client.util.field.LongDataField;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;


public class CompositionModelData extends Hd3dModelData
{
    private static final long serialVersionUID = 5486615589956173690L;

    public static final String COMPOSITION_NAME = "name";
    public static final String COMPOSITION_DESCRIPTION = "description";
    public static final String CONSTITUENT_ID_FIELD = "constituent";
    public static final String CONSTITUENT_NAME_FIELD = "constituentName";
    public static final String SHOT_ID_FIELD = "shot";
    public static final String COMPOSITION_SEQUENCE = "sequence";
    public static final String CATEGORY_ID_FIELD = "categoryId";

    public static final String COMPOSITION_OCCURENCE = "nbOccurence";

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LComposition";
    public static final String SIMPLE_CLASS_NAME = "Composition";

    public CompositionModelData()
    {
        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public CompositionModelData(final ConstituentModelData constituent, final ShotModelData shot)
    {
        this();
        if ((constituent == null || shot == null) || (constituent.getId() == null || shot.getId() == null))
        {
            GWT.log("CompositionModelData - create : Uncorrect composition : constituent and/or shot are null or have no ID",
                    null);
            return;
        }

        setName("casting_" + constituent.getId().toString() + "_" + shot.getId().toString());
        setConstituent(constituent.getId());
        setConstituentName(constituent.getName());
        setCategoryId(constituent.getCategory());
        setShot(shot.getId());
        setOcc(1);

    }

    public String getDescription()
    {
        return (String) get(COMPOSITION_DESCRIPTION);
    }

    public String getName()
    {
        return (String) get(COMPOSITION_NAME);
    }

    public void setDescription(String description)
    {
        set(COMPOSITION_DESCRIPTION, description);
    }

    public void setName(String name)
    {
        set(COMPOSITION_NAME, name);
    }

    public Long getConstituent()
    {
        return (Long) get(CONSTITUENT_ID_FIELD);
    }

    public String getConstituentName()
    {
        return get(CONSTITUENT_NAME_FIELD);
    }

    public Long getShot()
    {
        return (Long) get(SHOT_ID_FIELD);
    }

    public Long getCategoryId()
    {
        return (Long) get(CATEGORY_ID_FIELD);
    }

    public Long getSequence()
    {
        return (Long) get(COMPOSITION_SEQUENCE);
    }

    public void setConstituent(Long constituent)
    {
        set(CONSTITUENT_ID_FIELD, constituent);
    }

    public void setShot(Long shot)
    {
        set(SHOT_ID_FIELD, shot);
    }

    public void setSequence(Long sequence)
    {
        set(COMPOSITION_SEQUENCE, sequence);
    }

    public int getOcc()
    {
        return (Integer) get(COMPOSITION_OCCURENCE);
    }

    public void setOcc(Integer occ)
    {
        set(COMPOSITION_OCCURENCE, occ);
    }

    public void setCategoryId(Long categoryId)
    {
        set(CATEGORY_ID_FIELD, categoryId);
    }

    private void setConstituentName(String name)
    {
        set(CONSTITUENT_NAME_FIELD, name);
    }

    public static ModelType getModelType()
    {
        ModelType modelType = Hd3dModelData.getModelType();
        DataField id = new DataField(ID_FIELD);
        id.setType(Long.class);

        DataField sequence = new DataField(COMPOSITION_SEQUENCE);
        sequence.setType(Long.class);

        modelType.setRoot(ROOT);
        modelType.addField(id);
        modelType.addField(COMPOSITION_NAME);
        modelType.addField(COMPOSITION_DESCRIPTION);
        modelType.addField(new LongDataField(CONSTITUENT_ID_FIELD));
        modelType.addField(CONSTITUENT_NAME_FIELD);
        modelType.addField(new LongDataField(SHOT_ID_FIELD));
        modelType.addField(sequence);
        modelType.addField(new LongDataField(CATEGORY_ID_FIELD));

        DataField occ = new DataField(COMPOSITION_OCCURENCE);
        occ.setType(Integer.class);
        modelType.addField(occ);

        return modelType;
    }

    public static void getOrCreate(final ConstituentModelData constituent, final ShotModelData shot)
    {
        if ((constituent == null || shot == null) || (constituent.getId() == null || shot.getId() == null))
        {
            Logger.log("Uncorrect composition : constituent and/or shot are null or have no ID", null);
        }
        else
        {
            final CompositionModelData composition = new CompositionModelData();

            EqConstraint constituentConstraint = new EqConstraint(CompositionModelData.CONSTITUENT_ID_FIELD,
                    constituent.getId());
            EqConstraint shotConstraint = new EqConstraint(CompositionModelData.SHOT_ID_FIELD, shot.getId());
            Constraints constraints = new Constraints();
            constraints.add(constituentConstraint);
            constraints.add(shotConstraint);
            composition.setDefaultPath(constituent.getDefaultPath() + "/" + ServicesPath.COMPOSITIONS);
            composition.refreshWithParams(null, constraints, new GetModelDataCallback(composition, null) {
                @Override
                protected void onNoRecordReturned()
                {
                    CompositionModelData compo = new CompositionModelData();
                    compo.setDefaultPath(constituent.getDefaultPath() + "/" + ServicesPath.COMPOSITIONS);
                    compo.setName("casting_" + constituent.getId().toString() + "_" + shot.getId().toString());
                    compo.setConstituent(constituent.getId());
                    compo.setShot(shot.getId());
                    compo.save();
                }
            });
        }
    }

    public static CompositionModelData copyFrom(CompositionModelData composition, ShotModelData shot)
    {
        if ((shot == null) || (shot.getId() == null))
        {
            GWT.log("CompositionModelData - copyFrom : Uncorrect composition : constituent and/or shot are null or have no ID",
                    null);
            return null;
        }

        CompositionModelData compo = new CompositionModelData();
        compo.setName("casting_" + composition.getConstituent().toString() + "_" + shot.getId().toString());
        compo.setConstituent(composition.getConstituent());
        compo.setConstituentName(composition.getConstituentName());
        compo.setCategoryId(composition.getCategoryId());
        compo.setShot(shot.getId());
        compo.setOcc(composition.getOcc());
        compo.setDefaultPath(ServicesPath.PROJECTS + MainModel.getCurrentProject().getId() + "/"
                + ServicesPath.CATEGORIES + composition.getCategoryId() + "/" + ServicesPath.CONSTITUENTS
                + composition.getConstituent() + "/" + ServicesPath.COMPOSITIONS);

        return compo;
    }

    @Override
    public Hd3dModelData copy()
    {
        CompositionModelData composition = new CompositionModelData();
        Hd3dModelData.copy(this, composition);
        return composition;
    }
}
