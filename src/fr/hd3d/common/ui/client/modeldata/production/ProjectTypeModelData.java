package fr.hd3d.common.ui.client.modeldata.production;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;


public class ProjectTypeModelData extends RecordModelData
{
    private static final long serialVersionUID = 5486615589956173690L;

    public static final String PROJECT_DESCRIPTION = "description";
    public static final String PROJECT_COLOR = "color";

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LProjectType";
    public static final String SIMPLE_CLASS_NAME = "ProjectType";

    public ProjectTypeModelData()
    {
        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public ProjectTypeModelData(String name, String description)
    {
        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);

        this.setName(name);
        this.setDescription(description);
    }

    public ProjectTypeModelData(Long id, String name, String color)
    {
        this();
        this.setId(id);
        this.setName(name);
        this.setColor(color);
    }

    public void setDescription(String description)
    {
        set(PROJECT_DESCRIPTION, description);
    }

    public String getColor()
    {
        return (String) get(PROJECT_COLOR);
    }

    public void setColor(String color)
    {
        set(PROJECT_COLOR, color);
    }

    public String getDescription()
    {
        return (String) get(PROJECT_DESCRIPTION);
    }

    public String getLightClass()
    {
        return CLASS_NAME;
    }

    public static ModelType getModelType()
    {
        ModelType modelType = RecordModelData.getModelType();

        modelType.addField(PROJECT_COLOR);
        modelType.addField(PROJECT_DESCRIPTION);

        return modelType;
    }
}
