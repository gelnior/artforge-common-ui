package fr.hd3d.common.ui.client.modeldata.production;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.data.ModelType;
import com.extjs.gxt.ui.client.event.LoadListener;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.reader.SequenceReader;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.store.ServiceStore;


public class SequenceModelData extends RecordModelData
{
    private static final long serialVersionUID = 7050813939320115937L;

    public static final String SEQUENCE_HOOK = "hook";
    public static final String SEQUENCE_PROJECT = "project";
    public static final String SEQUENCE_NAME = "name";
    public static final String SEQUENCE_TITLE_FIELD = "title";
    public static final String SEQUENCE_PARENT = "parent";

    public static final String SIMPLE_CLASS_NAME = "Sequence";
    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LSequence";

    protected final List<Long> childrenIds = new ArrayList<Long>();
    private int sequencesLoading = 0;

    public SequenceModelData()
    {
        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public SequenceModelData(String name, Long projectId, Long parentId)
    {
        this();

        this.setName(name);
        this.setProject(projectId);
        this.setParent(parentId);
    }

    public void setHook(String hook)
    {
        set(SEQUENCE_HOOK, hook);
    }

    public void setProject(Long project)
    {
        set(SEQUENCE_PROJECT, project);
    }

    public void setParent(Long parent)
    {
        set(SEQUENCE_PARENT, parent);
    }

    public void setTitle(String title)
    {
        set(SEQUENCE_TITLE_FIELD, title);
    }

    public String getTitle()
    {
        return get(SEQUENCE_TITLE_FIELD);
    }

    public String getHook()
    {
        return (String) get(SEQUENCE_HOOK);
    }

    public Long getProject()
    {
        return (Long) get(SEQUENCE_PROJECT);
    }

    public Long getParent()
    {
        return (Long) get(SEQUENCE_PARENT);
    }

    @Override
    public String getDefaultPath()
    {
        String path = this.get(DEFAULT_PATH_FIELD);
        if (path == null)
        {
            path = ServicesPath.PROJECTS + this.getProject() + "/" + ServicesPath.SEQUENCES;
            if (this.getId() != null)
            {
                path += this.getId();
            }
        }
        return path;
    }

    public static ModelType getModelType()
    {
        ModelType modelType = RecordModelData.getModelType();

        modelType.addField(SEQUENCE_HOOK);
        modelType.addField(SEQUENCE_NAME);
        modelType.addField(SEQUENCE_TITLE_FIELD);

        DataField project = new DataField(SEQUENCE_PROJECT);
        project.setType(Long.class);
        modelType.addField(project);

        DataField parent = new DataField(SEQUENCE_PARENT);
        parent.setType(Long.class);
        modelType.addField(parent);

        return modelType;
    }

    public void loadChildrenIds()
    {
        if (childrenIds.size() > 0)
        {
            EventDispatcher.forwardEvent(CommonEvents.SEQUENCE_CHILDREN_LOADED, childrenIds);
        }
        else
        {
            sequencesLoading = 0;
            this.retrieveSequenceIds(this);
        }
    }

    public void retrieveSequenceIds(SequenceModelData sequence)
    {
        this.childrenIds.add(sequence.getId());
        sequencesLoading++;
        Constraint parentConstraint = new Constraint(EConstraintOperator.eq, SequenceModelData.SEQUENCE_PARENT,
                sequence.getId());
        final ServiceStore<SequenceModelData> store = new ServiceStore<SequenceModelData>(new SequenceReader());
        store.setPath(ServicesPath.PROJECTS + sequence.getProject() + "/" + ServicesPath.SEQUENCES + ServicesPath.ALL);
        store.addParameter(parentConstraint);

        store.addLoadListener(new LoadListener() {
            @Override
            public void loaderLoad(LoadEvent le)
            {
                for (SequenceModelData sequence : store.getModels())
                {
                    retrieveSequenceIds(sequence);
                }
                sequencesLoading--;

                if (store.getCount() == 0 && sequencesLoading == 0)
                {
                    EventDispatcher.forwardEvent(CommonEvents.SEQUENCE_CHILDREN_LOADED, childrenIds);
                }
            }
        });
        store.reload();
    }
}
