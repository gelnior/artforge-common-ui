package fr.hd3d.common.ui.client.modeldata.production;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.data.ModelType;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.common.ui.client.modeldata.task.PlanningModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.callback.GetModelDataCallback;
import fr.hd3d.common.ui.client.service.parameter.Constraints;
import fr.hd3d.common.ui.client.service.parameter.EqConstraint;
import fr.hd3d.common.ui.client.util.field.DateDataField;
import fr.hd3d.common.ui.client.util.field.ListDataField;
import fr.hd3d.common.ui.client.util.field.LongDataField;


public class ProjectModelData extends RecordModelData
{
    private static final long serialVersionUID = 5486615589956173690L;

    public static final String START_DATE_FIELD = "startDate";
    public static final String END_DATE_FIELD = "endDate";
    public static final String PROJECT_STATUS = "status";
    public static final String PROJECT_HOOK = "hook";
    public static final String PROJECT_COLOR = "color";

    public static final String RESOURCEGROUP_IDS_FIELD = "resourceGroupIDs";
    public static final String RESOURCEGROUP_NAMES_FIELD = "resourceGroupNames";
    public static final String TASKTYPE_IDS_FIELD = "taskTypeIDs";
    public static final String TASKTYPE_NAMES_FIELD = "taskTypeNames";
    public static final String TASKTYPE_COLORS_FIELD = "taskTypeColors";
    public static final String PROJECT_TYPE_ID_FIELD = "projectTypeId";
    public static final String PROJECT_TYPE_NAME_FIELD = "projectTypeName";
    public static final String ROOT_CATEGORY_ID_FIELD = "rootCategoryId";
    public static final String ROOT_SEQUENCE_ID_FIELD = "rootSequenceId";

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LProject";
    public static final String SIMPLE_CLASS_NAME = "Project";

    private Boolean isNew = Boolean.FALSE;

    public ProjectModelData()
    {
        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public ProjectModelData(String name, String status)
    {
        this();

        this.setName(name);
        this.setStatus(status);
    }

    public ProjectModelData(String name, String status, String hook)
    {
        this();

        this.setName(name);
        this.setStatus(status);
        this.setHook(hook);
    }

    public ProjectModelData(String name, Date startDate, Date endDate, String status, String hook)
    {
        this(name, status, hook);

        this.setStartDate(startDate);
        this.setEndDate(endDate);
    }

    public ProjectModelData(Long id, String name, String color)
    {
        this();

        this.setId(id);
        this.setName(name);
        this.setColor(color);
    }

    public Date getStartDate()
    {
        return get(START_DATE_FIELD);
    }

    public void setStartDate(Date startDate)
    {
        set(START_DATE_FIELD, startDate);
    }

    public Date getEndDate()
    {
        return get(END_DATE_FIELD);
    }

    public void setEndDate(Date endDate)
    {
        set(END_DATE_FIELD, endDate);
    }

    public void setStatus(String status)
    {
        set(PROJECT_STATUS, status);
    }

    public String getHook()
    {
        return (String) get(PROJECT_HOOK);
    }

    public void setHook(String hook)
    {
        set(PROJECT_HOOK, hook);
    }

    public String getColor()
    {
        return (String) get(PROJECT_COLOR);
    }

    public void setColor(String color)
    {
        set(PROJECT_COLOR, color);
    }

    public String getStatus()
    {
        return (String) get(PROJECT_STATUS);
    }

    public List<Long> getResourceGroupIds()
    {
        List<Long> ids = get(RESOURCEGROUP_IDS_FIELD);
        if (ids == null)
        {
            ids = new ArrayList<Long>();
            this.set(RESOURCEGROUP_IDS_FIELD, ids);
        }

        return ids;
    }

    public void setResourceGroupIDs(List<Long> resourceIDs)
    {
        this.set(RESOURCEGROUP_IDS_FIELD, resourceIDs);
    }

    public List<String> getResourceGroupNames()
    {
        return get(RESOURCEGROUP_NAMES_FIELD);
    }

    public void setResourceGroupNames(List<String> resourceGroupNames)
    {
        this.set(RESOURCEGROUP_NAMES_FIELD, resourceGroupNames);
    }

    public Long getProjectTypeId()
    {
        return (Long) get(PROJECT_TYPE_ID_FIELD);
    }

    public String getProjectTypeName()
    {
        return get(PROJECT_TYPE_NAME_FIELD);
    }

    public void setProjectTypeId(Long projectTypeId)
    {
        set(PROJECT_TYPE_ID_FIELD, projectTypeId);
    }

    public void setProjectTypeName(String projectTypeName)
    {
        set(PROJECT_TYPE_NAME_FIELD, projectTypeName);
    }

    public Long getRootCategoryId()
    {
        return get(ROOT_CATEGORY_ID_FIELD);
    }

    public Long getRootSequenceId()
    {
        return (Long) get(ROOT_SEQUENCE_ID_FIELD);
    }

    public String getLightClass()
    {
        return CLASS_NAME;
    }

    public List<Long> getTaskTypeIDs()
    {
        List<Long> ids = get(TASKTYPE_IDS_FIELD);
        if (ids == null)
        {
            ids = new ArrayList<Long>();
            this.set(TASKTYPE_IDS_FIELD, ids);
        }

        return ids;
    }

    public void setTaskTypeIDs(List<Long> taskTypeIDs)
    {
        this.set(TASKTYPE_IDS_FIELD, taskTypeIDs);
    }

    public List<String> getTaskTypeNames()
    {
        List<String> names = get(TASKTYPE_NAMES_FIELD);
        if (names == null)
        {
            names = new ArrayList<String>();
            this.set(TASKTYPE_NAMES_FIELD, names);
        }

        return names;
    }

    public void setTaskTypeNames(List<String> taskTypeNames)
    {
        this.set(TASKTYPE_NAMES_FIELD, taskTypeNames);
    }

    public List<String> getTaskTypeColors()
    {
        List<String> names = get(TASKTYPE_COLORS_FIELD);
        if (names == null)
        {
            names = new ArrayList<String>();
            this.set(TASKTYPE_COLORS_FIELD, names);
        }

        return names;
    }

    public void setTaskTypeColors(List<String> taskTypeColors)
    {
        this.set(TASKTYPE_COLORS_FIELD, taskTypeColors);
    }

    public static ModelType getModelType()
    {
        ModelType modelType = RecordModelData.getModelType();

        modelType.addField(PROJECT_HOOK);
        modelType.addField(new DateDataField(START_DATE_FIELD));
        modelType.addField(new DateDataField(END_DATE_FIELD));
        modelType.addField(PROJECT_STATUS);
        modelType.addField(PROJECT_COLOR);
        modelType.addField(new ListDataField(RESOURCEGROUP_IDS_FIELD, ResourceGroupModelData.SIMPLE_CLASS_NAME));
        modelType.addField(new ListDataField(RESOURCEGROUP_NAMES_FIELD));
        modelType.addField(new ListDataField(TASKTYPE_IDS_FIELD, TaskTypeModelData.SIMPLE_CLASS_NAME));
        modelType.addField(new ListDataField(TASKTYPE_NAMES_FIELD));
        modelType.addField(new ListDataField(TASKTYPE_COLORS_FIELD));
        modelType.addField(new LongDataField(PROJECT_TYPE_ID_FIELD));
        modelType.addField(new LongDataField(ROOT_CATEGORY_ID_FIELD));
        modelType.addField(new LongDataField(ROOT_SEQUENCE_ID_FIELD));
        modelType.addField(PROJECT_TYPE_NAME_FIELD);

        return modelType;
    }

    /**
     * Load Master planning then send it inside an event to event dispatcher
     * 
     * @param eventType
     *            Event type to forward.
     */
    public void getMasterPlanning(final EventType eventType)
    {
        PlanningModelData planning = new PlanningModelData();
        planning.setDefaultPath(this.getDefaultPath() + "/" + ServicesPath.PLANNINGS);

        Constraints parameters = new Constraints();
        parameters.add(new EqConstraint(PlanningModelData.MASTER_FIELD, Boolean.TRUE));

        planning.refreshWithParams(new AppEvent(eventType), parameters, new GetModelDataCallback(planning) {
            @Override
            protected void onRecordReturned(List<Hd3dModelData> records)
            {
                Hd3dModelData newRecord = records.get(0);
                this.record.updateFromModelData(newRecord);

                event.setType(eventType);
                event.setData(this.record);
                EventDispatcher.forwardEvent(event);
            }

            @Override
            protected void onNoRecordReturned()
            {
                EventDispatcher.forwardEvent(eventType);
            }
        });
    }

    public Boolean isNew()
    {
        return this.isNew;
    }

    public void setIsNew(Boolean isNew)
    {
        this.isNew = isNew;
    }

}
