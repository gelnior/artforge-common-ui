package fr.hd3d.common.ui.client.modeldata.production;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.util.field.IntegerDataField;
import fr.hd3d.common.ui.client.util.field.LongDataField;


public class ShotModelData extends RecordModelData
{
    public static final long serialVersionUID = -5818772585434831826L;

    public static final String SHOT_HOOK = "hook";
    public static final String SHOT_SEQUENCE = "sequence";
    public static final String SHOT_LABEL = "label";
    public static final String SHOT_DESCRIPTION = "description";
    public static final String SHOT_DIFFICULTY = "difficulty";
    public static final String SHOT_MATTEPAINTING = "mattePainting";
    public static final String SHOT_LAYOUT = "layout";
    public static final String SHOT_ANIMATION = "animation";
    public static final String SHOT_EXPORT = "export";
    public static final String SHOT_TRACKING = "tracking";
    public static final String SHOT_DRESSING = "dressing";
    public static final String SHOT_LIGHTING = "lighting";
    public static final String SHOT_RENDERING = "rendering";
    public static final String SHOT_COMPOSITING = "compositing";
    public static final String SHOT_PARENTLABEL = "parentLabel";
    public static final String SHOT_NBFRAME = "nbFrame";
    public static final String SHOT_SEQUENCESNAMES = "sequencesNames";
    public static final String SHOT_TRUST = "trust";
    public static final String SHOT_STARTFRAME = "startFrame";
    public static final String SHOT_ENDFRAME = "endFrame";

    public static final String SIMPLE_CLASS_NAME = "Shot";
    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LShot";

    public ShotModelData()
    {
        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public ShotModelData(String name, SequenceModelData parent)
    {
        this();

        setSequence(parent.getId());
        setLabel(name);
        setName(name);
        setDefaultPath(parent.getProject(), parent.getId());
    }

    public void setHook(String hook)
    {
        set(SHOT_HOOK, hook);
    }

    public void setLabel(String label)
    {
        set(SHOT_LABEL, label);
    }

    public void setDescription(String description)
    {
        set(SHOT_DESCRIPTION, description);
    }

    public void setSequence(Long sequence)
    {
        set(SHOT_SEQUENCE, sequence);
    }

    public String getHook()
    {
        return (String) get(SHOT_HOOK);
    }

    public String getLabel()
    {
        return (String) get(SHOT_LABEL);
    }

    public String getDescription()
    {
        return (String) get(SHOT_DESCRIPTION);
    }

    public Long getSequence()
    {
        return (Long) get(SHOT_SEQUENCE);
    }

    public int getDifficulty()
    {
        return (Integer) get(SHOT_DIFFICULTY);
    }

    public String getSequencesNames()
    {
        return (String) get(SHOT_SEQUENCESNAMES);
    }

    public void setDifficulty(int difficulty)
    {
        set(SHOT_DIFFICULTY, difficulty);
    }

    public Integer getNbFrame()
    {
        return (Integer) get(SHOT_NBFRAME);
    }

    public void setNbFrame(int nbFrame)
    {
        set(SHOT_NBFRAME, nbFrame);
    }

    public Integer getStartFrame()
    {
        return (Integer) get(SHOT_STARTFRAME);
    }

    public void setStartFrame(int startFrame)
    {
        set(SHOT_STARTFRAME, startFrame);
    }

    public Integer getEndFrame()
    {
        return (Integer) get(SHOT_ENDFRAME);
    }

    public void setEndFrame(int endFrame)
    {
        set(SHOT_ENDFRAME, endFrame);
    }

    public void setMattePainting(Boolean mattePainting)
    {
        set(SHOT_MATTEPAINTING, mattePainting);
    }

    public void setLayout(Boolean layout)
    {
        set(SHOT_LAYOUT, layout);
    }

    public void setAnimation(Boolean animation)
    {
        set(SHOT_ANIMATION, animation);
    }

    public void setExport(Boolean export)
    {
        set(SHOT_EXPORT, export);
    }

    public void setTracking(Boolean tracking)
    {
        set(SHOT_TRACKING, tracking);
    }

    public void setDressing(Boolean dressing)
    {
        set(SHOT_DRESSING, dressing);
    }

    public void setLighting(Boolean lighting)
    {
        set(SHOT_LIGHTING, lighting);
    }

    public void setRendering(Boolean rendering)
    {
        set(SHOT_RENDERING, rendering);
    }

    public void setCompositing(Boolean compositing)
    {

        set(SHOT_COMPOSITING, compositing);
    }

    public String getParentLabel()
    {
        return get(SHOT_PARENTLABEL);
    }

    public void setParentLabel(String parentLabel)
    {
        set(SHOT_PARENTLABEL, parentLabel);
    }

    // public long getContentUnitId()
    // {
    // return (Long) get(SHOT_CONTENTUNITID);
    // }
    //
    // public void setContentUnitId(long contentUnitId)
    // {
    // set(SHOT_CONTENTUNITID, contentUnitId);
    // }

    public Boolean getMattePainting()
    {
        return (Boolean) get(SHOT_MATTEPAINTING);
    }

    public Boolean getLayout()
    {
        return (Boolean) get(SHOT_LAYOUT);
    }

    public Boolean getAnimation()
    {
        return (Boolean) get(SHOT_ANIMATION);
    }

    public Boolean getExport()
    {
        return (Boolean) get(SHOT_EXPORT);
    }

    public Boolean getTracking()
    {
        return (Boolean) get(SHOT_TRACKING);
    }

    public Boolean getDressing()
    {
        return (Boolean) get(SHOT_DRESSING);
    }

    public Boolean getLighting()
    {
        return (Boolean) get(SHOT_LIGHTING);
    }

    public Boolean getRendering()
    {
        return (Boolean) get(SHOT_RENDERING);
    }

    public Boolean getCompositing()
    {
        return (Boolean) get(SHOT_COMPOSITING);
    }

    public static ModelType getModelType()
    {
        ModelType modelType = RecordModelData.getModelType();
        modelType.addField(SHOT_HOOK);
        modelType.addField(SHOT_LABEL);
        modelType.addField(SHOT_DESCRIPTION);

        modelType.addField(new LongDataField(SHOT_SEQUENCE));

        DataField difficulty = new DataField(SHOT_DIFFICULTY);
        DataField mattePainting = new DataField(SHOT_MATTEPAINTING);
        DataField layout = new DataField(SHOT_LAYOUT);
        DataField animation = new DataField(SHOT_ANIMATION);
        DataField export = new DataField(SHOT_EXPORT);
        DataField tracking = new DataField(SHOT_TRACKING);
        DataField dressing = new DataField(SHOT_DRESSING);
        DataField lighting = new DataField(SHOT_LIGHTING);
        DataField rendering = new DataField(SHOT_RENDERING);
        DataField compositing = new DataField(SHOT_COMPOSITING);
        DataField parentLabel = new DataField(SHOT_PARENTLABEL);
        DataField sequencesnames = new DataField(SHOT_SEQUENCESNAMES);
        DataField nbFrame = new DataField(SHOT_NBFRAME);

        nbFrame.setType(Integer.class);
        difficulty.setType(Integer.class);

        mattePainting.setType(Boolean.class);
        layout.setType(Boolean.class);
        animation.setType(Boolean.class);
        export.setType(Boolean.class);
        tracking.setType(Boolean.class);
        dressing.setType(Boolean.class);
        lighting.setType(Boolean.class);
        rendering.setType(Boolean.class);
        compositing.setType(Boolean.class);

        parentLabel.setType(Long.class);

        modelType.addField(difficulty);
        modelType.addField(mattePainting);
        modelType.addField(layout);
        modelType.addField(animation);
        modelType.addField(export);
        modelType.addField(tracking);
        modelType.addField(dressing);
        modelType.addField(lighting);
        modelType.addField(rendering);
        modelType.addField(compositing);
        modelType.addField(parentLabel);
        modelType.addField(sequencesnames);
        modelType.addField(nbFrame);
        modelType.addField(new IntegerDataField(SHOT_STARTFRAME));
        modelType.addField(new IntegerDataField(SHOT_ENDFRAME));

        return modelType;
    }

    public void setDefaultPath(Long projectId, Long sequenceId)
    {
        this.setDefaultPath(ServicesPath.PROJECTS + projectId + "/" + ServicesPath.SEQUENCES + sequenceId + "/"
                + ServicesPath.SHOTS);
    }

}
