package fr.hd3d.common.ui.client.modeldata.resource;

import java.util.Date;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;


public class ContractModelData extends RecordModelData
{
    private static final long serialVersionUID = -5484811007789570224L;

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LContract";
    public static final String SIMPLE_CLASS_NAME = "Contract";

    public static final String START_DATE_FIELD = "startDate";
    public static final String END_DATE_FIELD = "endDate";
    public static final String REAL_END_DATE_FIELD = "realEndDate";
    public static final String DOC_PATH_FIELD = "docPath";
    public static final String TYPE_FIELD = "type";
    public static final String SALARY_FIELD = "salary";

    public static final String PERSON_ID_FIELD = "personId";
    public static final String PERSON_NAME_FIELD = "personName";
    public static final String QUALIFICATION_ID_FIELD = "qualificationId";
    public static final String QUALIFICATION_NAME_FIELD = "qualificationName";
    public static final String ORGANIZATION_ID_FIELD = "organizationId";
    public static final String ORGANIZATION_NAME_FIELD = "organizationName";

    public ContractModelData()
    {
        this.setClassName(CLASS_NAME);
        this.simpleClassName = SIMPLE_CLASS_NAME;
    }

    public ContractModelData(Long id, String name)
    {
        this();

        this.setId(id);
        this.setName(name);
    }

    public ContractModelData(String name, Date startDate, Date endDate, Date realEndDate, String docPath, String type, Float salary)
    {
        this(null, name);

        this.setStartDate(startDate);
        this.setEndDate(endDate);
        this.setRealEndDate(realEndDate);
        this.setDocPath(docPath);
        this.setType(type);
        this.setSalary(salary);
    }

    public Date getStartDate()
    {
        return this.get(START_DATE_FIELD);
    }

    public void setStartDate(Date startDate)
    {
        this.set(START_DATE_FIELD, startDate);
    }

    public Date getEndDate()
    {
        return this.get(END_DATE_FIELD);
    }

    public void setEndDate(Date endDate)
    {
        this.set(END_DATE_FIELD, endDate);
    }

    public Date getRealEndDate()
    {
        return this.get(REAL_END_DATE_FIELD);
    }

    public void setRealEndDate(Date realEndDate)
    {
        this.set(REAL_END_DATE_FIELD, realEndDate);
    }

    public String getDocPath()
    {
        return this.get(DOC_PATH_FIELD);
    }

    public void setDocPath(String docPath)
    {
        this.set(DOC_PATH_FIELD, docPath);
    }

    public String getType()
    {
        return this.get(TYPE_FIELD);
    }

    public void setType(String Type)
    {
        this.set(TYPE_FIELD, Type);
    }
    
    public Float getSalary()
    {
        return this.get(SALARY_FIELD);
    }

    public void setSalary(Float salary)
    {
        this.set(SALARY_FIELD, salary);
    }

    public Long getPersonId()
    {
        return this.get(PERSON_ID_FIELD);
    }

    public void setPersonId(Long personId)
    {
        this.set(PERSON_ID_FIELD, personId);
    }

    public String getPersonName()
    {
        return this.get(PERSON_NAME_FIELD);
    }

    public void setPersonName(String personName)
    {
        this.set(PERSON_NAME_FIELD, personName);
    }

    public Long getOrganizationId()
    {
        return this.get(ORGANIZATION_ID_FIELD);
    }

    public void setOrganizationId(Long organizationId)
    {
        this.set(ORGANIZATION_ID_FIELD, organizationId);
    }

    public String getOrganizationName()
    {
        return this.get(ORGANIZATION_NAME_FIELD);
    }

    public void setOrganizationName(String organizationName)
    {
        this.set(ORGANIZATION_NAME_FIELD, organizationName);
    }

    public Long getQualificationId()
    {
        return this.get(QUALIFICATION_ID_FIELD);
    }

    public void setQualificationId(Long qualificationId)
    {
        this.set(QUALIFICATION_ID_FIELD, qualificationId);
    }

    public String getQualificationName()
    {
        return this.get(QUALIFICATION_NAME_FIELD);
    }

    public void setQualificationName(String qualificationName)
    {
        this.set(QUALIFICATION_NAME_FIELD, qualificationName);
    }

    /**
     * @return Model type used by reader to know the manufacturer model data structure.
     */
    public static ModelType getModelType()
    {
        ModelType type = RecordModelData.getModelType();

        DataField startDateDataField = new DataField(START_DATE_FIELD);
        startDateDataField.setType(Date.class);
        DataField endDateDataField = new DataField(END_DATE_FIELD);
        endDateDataField.setType(Date.class);
        DataField realEndDateDataField = new DataField(REAL_END_DATE_FIELD);
        realEndDateDataField.setType(Date.class);

        DataField personIdDataField = new DataField(PERSON_ID_FIELD);
        personIdDataField.setType(Long.class);

        DataField qualificationIdDataField = new DataField(QUALIFICATION_ID_FIELD);
        qualificationIdDataField.setType(Long.class);

        DataField organizationIdDataField = new DataField(ORGANIZATION_ID_FIELD);
        organizationIdDataField.setType(Long.class);
        
        DataField salaryDataField = new DataField(SALARY_FIELD);
        salaryDataField.setType(Float.class);

        type.addField(DOC_PATH_FIELD);
        type.addField(TYPE_FIELD);

        type.addField(startDateDataField);
        type.addField(endDateDataField);
        type.addField(realEndDateDataField);
        type.addField(personIdDataField);
        type.addField(PERSON_NAME_FIELD);
        type.addField(qualificationIdDataField);
        type.addField(QUALIFICATION_NAME_FIELD);
        type.addField(organizationIdDataField);
        type.addField(ORGANIZATION_NAME_FIELD);
        type.addField(salaryDataField);

        return type;
    }
}
