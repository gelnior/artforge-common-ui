package fr.hd3d.common.ui.client.modeldata.resource;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;


public class SkillModelData extends RecordModelData
{
    private static final long serialVersionUID = -5484811007789570224L;

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LSkill";
    public static final String SIMPLE_CLASS_NAME = "Skill";

    public static final String SOFTWARE_ID_FIELD = "softwareId";
    public static final String SOFTWARE_NAME_FIELD = "softwareName";

    public SkillModelData()
    {
        this.setClassName(CLASS_NAME);
        this.simpleClassName = SIMPLE_CLASS_NAME;
    }

    public SkillModelData(Long id, String name)
    {
        this();

        this.setId(id);
        this.setName(name);
    }

    public SkillModelData(String name)
    {
        this(null, name);
    }

    public Long getSoftwareId()
    {
        return this.get(SOFTWARE_ID_FIELD);
    }

    public void setSoftwareId(Long softwareId)
    {
        this.set(SOFTWARE_ID_FIELD, softwareId);
    }

    public String getSoftwareName()
    {
        return this.get(SOFTWARE_NAME_FIELD);
    }

    public void setSoftwareName(String softwareName)
    {
        this.set(SOFTWARE_NAME_FIELD, softwareName);
    }

    /**
     * @return Model type used by reader to know the manufacturer model data structure.
     */
    public static ModelType getModelType()
    {
        ModelType type = RecordModelData.getModelType();

        DataField softwareIdDataField = new DataField(SOFTWARE_ID_FIELD);
        softwareIdDataField.setType(Long.class);

        type.addField(softwareIdDataField);
        type.addField(SOFTWARE_NAME_FIELD);

        return type;
    }
}
