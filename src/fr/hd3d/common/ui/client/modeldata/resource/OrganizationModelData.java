package fr.hd3d.common.ui.client.modeldata.resource;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;


public class OrganizationModelData extends RecordModelData
{
    /** Automatically generated serial version number. */
    private static final long serialVersionUID = 6044953841081053128L;

    public static final String PARENT_ID_FIELD = "parent";
    public static final String RESOURCEGROUP_IDS_FIELD = "resourceGroupIds";
    public static final String RESOURCEGROUP_NAMES_FIELD = "resourceGroupNames";

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LOrganization";
    public static final String SIMPLE_CLASS_NAME = "Organization";

    public OrganizationModelData()
    {
        this.setResourceGroupIDs(new ArrayList<Long>());
        this.setResourceGroupNames(new ArrayList<String>());

        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public OrganizationModelData(String name, Long parentId)
    {
        this();

        this.setName(name);
        this.setParentId(parentId);
    }

    public Long getParentId()
    {
        return this.get(PARENT_ID_FIELD);
    }

    public void setParentId(Long id)
    {
        this.set(PARENT_ID_FIELD, id);
    }

    public List<Long> getResourceGroupIds()
    {
        return get(RESOURCEGROUP_IDS_FIELD);
    }

    public void setResourceGroupIDs(List<Long> resourceIDs)
    {
        this.set(RESOURCEGROUP_IDS_FIELD, resourceIDs);
    }

    public List<String> getResourceGroupNames()
    {
        return get(RESOURCEGROUP_NAMES_FIELD);
    }

    public void setResourceGroupNames(List<String> resourceGroupNames)
    {
        this.set(RESOURCEGROUP_NAMES_FIELD, resourceGroupNames);
    }

    /**
     * @return Model type used by reader to handle the studio map model data structure.
     */
    public static ModelType getModelType()
    {
        ModelType type = RecordModelData.getModelType();

        DataField parentIdDataField = new DataField(PARENT_ID_FIELD);
        parentIdDataField.setType(Long.class);

        DataField resourceGroupIdsDataField = new DataField(RESOURCEGROUP_IDS_FIELD);
        resourceGroupIdsDataField.setType(List.class);
        resourceGroupIdsDataField.setFormat("ResourceGroup");

        DataField resourceGroupNamesDataField = new DataField(RESOURCEGROUP_NAMES_FIELD);
        resourceGroupNamesDataField.setType(List.class);

        type.addField(parentIdDataField);
        type.addField(resourceGroupIdsDataField);
        type.addField(resourceGroupNamesDataField);

        return type;
    }
}
