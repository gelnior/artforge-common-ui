package fr.hd3d.common.ui.client.modeldata.resource;

import java.util.Date;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.util.field.DateDataField;
import fr.hd3d.common.ui.client.util.field.LongDataField;


public class MilestoneModelData extends RecordModelData
{
    private static final long serialVersionUID = 2648069275922834857L;

    public static final String IS_NEW = "isNew";
    public static final String PREVIOUS_DATE = "previousDate";

    public static final String TITLE_FIELD = "title";
    public static final String DESCRITION_FIELD = "description";
    public static final String COLOR_FIELD = "color";
    public static final String DATE_FIELD = "date";
    public static final String PLANNING_FIELD = "planningID";

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LMileStone";

    public static final String SIMPLE_CLASS_NAME = "MileStone";

    public static final String DEFAULT_COLOR = "#F66";

    public MilestoneModelData()
    {
        super();
        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
        this.setColor(DEFAULT_COLOR);
    }

    @Override
    public String getName()
    {
        return this.getTitle();
    }

    @Override
    public void setName(String name)
    {
        this.setTitle(name);
    }

    public void setTitle(String title)
    {
        set(TITLE_FIELD, title);
    }

    public void setDescription(String description)
    {
        set(DESCRITION_FIELD, description);
    }

    public void setPlanningID(Long planninID)
    {
        set(PLANNING_FIELD, planninID);
    }

    public void setDate(Date date)
    {
        set(DATE_FIELD, date);
    }

    public String getTitle()
    {
        return get(TITLE_FIELD);
    }

    public String getDescription()
    {
        return get(DESCRITION_FIELD);
    }

    public Date getDate()
    {
        return (Date) get(DATE_FIELD);
    }

    public Long getPlanningID()
    {
        return (Long) get(PLANNING_FIELD);
    }

    public String getColor()
    {
        return get(COLOR_FIELD);
    }

    public void setColor(String color)
    {
        this.set(COLOR_FIELD, color);
    }

    public static ModelType getModelType()
    {
        ModelType modelType = Hd3dModelData.getModelType();

        modelType.addField(TITLE_FIELD);
        modelType.addField(DESCRITION_FIELD);
        modelType.addField(COLOR_FIELD);
        modelType.addField(new DateDataField(DATE_FIELD));
        modelType.addField(new LongDataField(PLANNING_FIELD));

        return modelType;
    }
}
