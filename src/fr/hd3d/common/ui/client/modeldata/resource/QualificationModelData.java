package fr.hd3d.common.ui.client.modeldata.resource;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;


public class QualificationModelData extends RecordModelData
{
    private static final long serialVersionUID = -5484811007789570224L;

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LQualification";
    public static final String SIMPLE_CLASS_NAME = "Qualification";

    public static final String SKILL_IDS_FIELD = "skillIDs";
    public static final String SKILL_NAMES_FIELD = "skillNames";

    public QualificationModelData()
    {
        this.setClassName(CLASS_NAME);
        this.simpleClassName = SIMPLE_CLASS_NAME;

        this.setSkillIDs(new ArrayList<Long>());
        this.setSkillNames(new ArrayList<String>());
    }

    public QualificationModelData(Long id, String name)
    {
        this();

        this.setId(id);
        this.setName(name);
    }

    public QualificationModelData(String name)
    {
        this(null, name);
    }

    public List<Long> getSkillIDs()
    {
        return get(SKILL_IDS_FIELD);
    }

    public void setSkillIDs(List<Long> skillIDs)
    {
        this.set(SKILL_IDS_FIELD, skillIDs);
    }

    public List<String> getSkillNames()
    {
        return get(SKILL_NAMES_FIELD);
    }

    public void setSkillNames(List<String> skillNames)
    {
        this.set(SKILL_NAMES_FIELD, skillNames);
    }

    /**
     * @return Model type used by reader to know the manufacturer model data structure.
     */
    public static ModelType getModelType()
    {
        ModelType type = RecordModelData.getModelType();

        DataField skillIdsDataField = new DataField(SKILL_IDS_FIELD);
        skillIdsDataField.setType(List.class);
        skillIdsDataField.setFormat(SkillModelData.SIMPLE_CLASS_NAME);

        DataField skillNamesDataField = new DataField(SKILL_NAMES_FIELD);
        skillNamesDataField.setType(List.class);

        type.addField(skillIdsDataField);
        type.addField(skillNamesDataField);

        return type;
    }
}
