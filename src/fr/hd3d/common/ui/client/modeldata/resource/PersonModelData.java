package fr.hd3d.common.ui.client.modeldata.resource;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.client.PersonNameUtils;
import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.callback.GetModelDataCallback;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.ParameterBuilder;


/**
 * Person object entity. A person works on a project. The person object is needed for authentification.
 * 
 * @author HD3D
 */
public class PersonModelData extends RecordModelData
{
    private static final long serialVersionUID = 5747409576846072283L;

    public static final String PERSON_LOGIN_FIELD = "login";
    public static final String PERSON_FIRST_NAME_FIELD = "firstName";
    public static final String PERSON_LAST_NAME_FIELD = "lastName";
    public static final String EMAIL_FIELD = "email";
    public static final String PHONE_FIELD = "phone";
    public static final String PHOTO_PATH_FIELD = "photoPath";
    public static final String ACCOUNT_STATUS_FIELD = "accountStatus";

    public static final String ACTIVITIES_IDS_FIELD = "activitiesIDs";
    public static final String ACTIVITIES_NAMES_FIELD = "activitiesNames";
    public static final String TASKS_IDS_FIELD = "tasksIDs";
    public static final String TASKS_NAMES_FIELD = "tasksNames";
    public static final String RESOURCEGROUP_IDS_FIELD = "resourceGroupIDs";
    public static final String RESOURCEGROUP_NAMES_FIELD = "resourceGroupNames";
    public static final String PROJECT_IDS_FIELD = "projectIDs";
    public static final String PROJECT_NAMES_FIELD = "projectNames";

    public static final String SIMPLE_CLASS_NAME = "Person";
    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LPerson";

    public PersonModelData()
    {
        this.setClassName("fr.hd3d.model.lightweight.impl.LPerson");
        this.setSimpleClassName(SIMPLE_CLASS_NAME);

        this.setResourceGroupIDs(new ArrayList<Long>());
        this.setResourceGroupNames(new ArrayList<String>());
    }

    public PersonModelData(Long id, String firstName, String lastName, String login)
    {
        this();
        this.setId(id);
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setLogin(login);
    }

    public PersonModelData(String firstName, String lastName, String login, String email, String phone, String photoPath)
    {
        this();
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setLogin(login);
        this.setEmail(email);
        this.setPhone(phone);
        this.setPhotoPath(photoPath);
    }

    public String getLogin()
    {
        return (String) get(PERSON_LOGIN_FIELD);
    }

    public void setLogin(String login)
    {
        set(PERSON_LOGIN_FIELD, login);
    }

    public String getFirstName()
    {
        return (String) get(PERSON_FIRST_NAME_FIELD);
    }

    public void setFirstName(String firstName)
    {
        set(PERSON_FIRST_NAME_FIELD, firstName);
    }

    public String getLastName()
    {
        return (String) get(PERSON_LAST_NAME_FIELD);
    }

    public void setLastName(String lastName)
    {
        set(PERSON_LAST_NAME_FIELD, lastName);
    }

    public String getEmail()
    {
        return (String) get(EMAIL_FIELD);
    }

    public void setEmail(String email)
    {
        set(EMAIL_FIELD, email);
    }

    public String getPhone()
    {
        return (String) get(PHONE_FIELD);
    }

    public void setPhone(String phone)
    {
        set(PHONE_FIELD, phone);
    }

    public String getPhotoPath()
    {
        return (String) get(PHOTO_PATH_FIELD);
    }

    public void setPhotoPath(String photoPath)
    {
        set(PHOTO_PATH_FIELD, photoPath);
    }

    public Long getAccountStatus()
    {
        Object object = get(ACCOUNT_STATUS_FIELD);
        if (object instanceof Double)
        {
            Double value = (Double) object;
            return Long.valueOf(value.longValue());
        }
        return (Long) object;
    }

    public void setAccountStatus(Long accountStatus)
    {
        set(ACCOUNT_STATUS_FIELD, accountStatus);
    }

    public List<Long> getActivityIds()
    {
        return get(ACTIVITIES_IDS_FIELD);
    }

    public void setActivityIds(List<Long> activitiesIds)
    {
        set(ACTIVITIES_IDS_FIELD, activitiesIds);
    }

    public void setTaskIds(List<Long> taskIds)
    {
        set(TASKS_IDS_FIELD, taskIds);
    }

    public List<String> getActivityNames()
    {
        return get(ACTIVITIES_NAMES_FIELD);
    }

    public void setActivityNames(List<String> activitiesNames)
    {
        set(ACTIVITIES_NAMES_FIELD, activitiesNames);
    }

    public List<Long> getTaskIds()
    {
        return get(TASKS_IDS_FIELD);
    }

    public List<String> getTaskNames()
    {
        return get(TASKS_NAMES_FIELD);
    }

    public void setTaskNames(List<String> taskNames)
    {
        set(TASKS_NAMES_FIELD, taskNames);
    }

    public List<Long> getResourceGroupIds()
    {
        List<Long> ids = get(RESOURCEGROUP_IDS_FIELD);
        if (ids == null)
        {
            ids = new ArrayList<Long>();
            set(RESOURCEGROUP_IDS_FIELD, ids);
        }

        return ids;
    }

    public void setResourceGroupIDs(List<Long> resourceGroupIDs)
    {
        this.set(RESOURCEGROUP_IDS_FIELD, resourceGroupIDs);
    }

    public List<String> getResourceGroupNames()
    {
        return get(RESOURCEGROUP_NAMES_FIELD);
    }

    public void setResourceGroupNames(List<String> resourceGroupNames)
    {
        this.set(RESOURCEGROUP_NAMES_FIELD, resourceGroupNames);
    }

    public List<Long> getProjectIds()
    {
        return get(RESOURCEGROUP_IDS_FIELD);
    }

    public void setProjectIDs(List<Long> projectIDs)
    {
        this.set(PROJECT_IDS_FIELD, projectIDs);
    }

    public List<String> getProjectNames()
    {
        return get(PROJECT_NAMES_FIELD);
    }

    public void setProjectNames(List<String> projectNames)
    {
        this.set(PROJECT_NAMES_FIELD, projectNames);
    }

    /**
     * @return Model data description, it means object fields, hibernate class, root field name and total field name
     */
    public static ModelType getModelType()
    {
        ModelType modelType = RecordModelData.getModelType();

        DataField resourceGroupIdsDataField = new DataField(RESOURCEGROUP_IDS_FIELD);
        resourceGroupIdsDataField.setType(List.class);
        resourceGroupIdsDataField.setFormat("ResourceGroup");

        DataField resourceGroupNamesDataField = new DataField(RESOURCEGROUP_NAMES_FIELD);
        resourceGroupNamesDataField.setType(List.class);

        DataField projectIdsDataField = new DataField(PROJECT_IDS_FIELD);
        projectIdsDataField.setType(List.class);
        projectIdsDataField.setFormat("Project");

        DataField projectNamesDataField = new DataField(PROJECT_NAMES_FIELD);
        projectNamesDataField.setType(List.class);

        DataField taskIdsDataField = new DataField(TASKS_IDS_FIELD);
        taskIdsDataField.setType(List.class);
        taskIdsDataField.setFormat("ResourceGroup");

        DataField taskNamesDataField = new DataField(TASKS_NAMES_FIELD);
        taskNamesDataField.setType(List.class);

        DataField accountStatusField = new DataField(ACCOUNT_STATUS_FIELD);
        accountStatusField.setType(Long.class);

        modelType.addField(PERSON_LOGIN_FIELD);
        modelType.addField(PERSON_FIRST_NAME_FIELD);
        modelType.addField(PERSON_LAST_NAME_FIELD);
        modelType.addField(EMAIL_FIELD);
        modelType.addField(PHONE_FIELD);
        modelType.addField(PHOTO_PATH_FIELD);
        modelType.addField(accountStatusField);

        modelType.addField(ACTIVITIES_IDS_FIELD);
        modelType.addField(ACTIVITIES_NAMES_FIELD);
        modelType.addField(TASKS_IDS_FIELD);
        modelType.addField(TASKS_NAMES_FIELD);
        modelType.addField(resourceGroupIdsDataField);
        modelType.addField(resourceGroupNamesDataField);
        modelType.addField(projectIdsDataField);
        modelType.addField(projectNamesDataField);
        modelType.addField(taskIdsDataField);
        modelType.addField(taskNamesDataField);

        return modelType;
    }

    public String getFullName()
    {
        // return this.getLastName() + " " + this.getFirstName() + " - " + this.getLogin();
        return PersonNameUtils.getFullName(this.getLastName(), this.getFirstName(), this.getLogin());
    }

    /**
     * Act as refresh() method, it means refresh all fields with server data. But the way to select record is not by id
     * it is by login.
     * 
     * @param eventType
     *            Event type to forward when refresh is finished.
     */
    public void refreshByLogin(EventType eventType)
    {
        Constraint constraint = new Constraint(EConstraintOperator.eq, PERSON_LOGIN_FIELD, this.getLogin(), null);
        String path = this.getPath() + ParameterBuilder.parameterToString(constraint);

        AppEvent event = new AppEvent(eventType);
        IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
        GetModelDataCallback callback = new GetModelDataCallback(this, event);
        requestHandler.getRequest(path, callback);
    }
}
