package fr.hd3d.common.ui.client.modeldata.resource;

import com.extjs.gxt.ui.client.core.FastMap;

import fr.hd3d.common.client.enums.EContractType;
import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.util.FieldUtils;


/**
 * Map containing all task status where status is the key and a field model data with name and status as field value is
 * the map value.
 * 
 * @author HD3D
 */
public class ContractTypeMap extends FastMap<FieldModelData>
{
    private static final long serialVersionUID = 4585914586916480649L;

    // CDI, CDD, INTERIM, FREELANCE, INTERMITTENT, UNKNOWN

    public static final String CDI_COLOR = "#c8fbbf";
    public static final String CDD_COLOR = "#cbf0f2";
    public static final String INTERIM_COLOR = "#fffcaf";
    public static final String FREELANCE_COLOR = "#DDDDDD";
    public static final String INTERMITTENT_COLOR = "#ffde8f";
    public static final String UNKNOWN_COLOR = "#EE5555";

    /** static map for retrieving colors corresponding to task status. */
    private static FastMap<String> colors = new FastMap<String>();

    /**
     * @param status
     *            Status of which color is requested.
     * @return Color corresponding to <i>status</i>.
     */
    public static String getColorForStatus(String status)
    {
        if (colors.isEmpty())
        {
            colors.put(EContractType.CDI.toString(), CDI_COLOR);
            colors.put(EContractType.CDD.toString(), CDD_COLOR);
            colors.put(EContractType.INTERIM.toString(), INTERIM_COLOR);
            colors.put(EContractType.FREELANCE.toString(), FREELANCE_COLOR);
            colors.put(EContractType.INTERMITTENT.toString(), INTERMITTENT_COLOR);
            colors.put(EContractType.UNKNOWN.toString(), UNKNOWN_COLOR);

        }

        return colors.get(status);
    }

    public ContractTypeMap()
    {
        super();
        FieldUtils.addFieldToMap(this, 0, "CDI", EContractType.CDI.toString());
        FieldUtils.addFieldToMap(this, 1, "CDD", EContractType.CDD.toString());
        FieldUtils.addFieldToMap(this, 2, "Interim", EContractType.INTERIM.toString());
        FieldUtils.addFieldToMap(this, 3, "Freelance", EContractType.FREELANCE.toString());
        FieldUtils.addFieldToMap(this, 4, "Intermittent", EContractType.INTERMITTENT.toString());
        FieldUtils.addFieldToMap(this, 5, "Unknown", EContractType.UNKNOWN.toString());

    }
}
