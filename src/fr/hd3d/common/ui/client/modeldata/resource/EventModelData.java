package fr.hd3d.common.ui.client.modeldata.resource;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.DurationModelData;


public class EventModelData extends DurationModelData
{
    private static final long serialVersionUID = -8399455399381068236L;

    public static final String PLANNING_ID_FIELD = "planningID";
    public static final String TITLE_FIELD = "title";
    public static final String DESCRIPTION_FIELD = "description";

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LEvent";
    public static final String SIMPLE_CLASS_NAME = "Event";

    public EventModelData()
    {
        super();

        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public Long getPlanningID()
    {
        return get(PLANNING_ID_FIELD);
    }

    public void setPlanningID(Long planningId)
    {
        set(PLANNING_ID_FIELD, planningId);
    }

    public String getTitle()
    {
        return get(TITLE_FIELD);
    }

    public void setTitle(String title)
    {
        set(TITLE_FIELD, title);
    }

    public String getDescription()
    {
        return get(DESCRIPTION_FIELD);
    }

    public void setDescription(String description)
    {
        set(DESCRIPTION_FIELD, description);
    }

    public static ModelType getModelType()
    {
        ModelType type = DurationModelData.getModelType();

        DataField planningIdField = new DataField(PLANNING_ID_FIELD);
        planningIdField.setType(Long.class);
        DataField titleField = new DataField(TITLE_FIELD);
        titleField.setType(String.class);
        DataField descriptionField = new DataField(DESCRIPTION_FIELD);
        descriptionField.setType(String.class);

        type.addField(planningIdField);
        type.addField(titleField);
        type.addField(descriptionField);

        return type;
    }
}
