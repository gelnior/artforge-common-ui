package fr.hd3d.common.ui.client.modeldata.resource;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;


public class ResourceGroupModelData extends RecordModelData
{
    /** Automatically generated serial version number. */
    private static final long serialVersionUID = -5557795790888008500L;

    public static final String PARENT_ID_FIELD = "parent";

    public static final String LEADER_ID_FIELD = "leaderId";
    public static final String LEADER_NAME_FIELD = "leaderName";
    public static final String RESOURCE_IDS_FIELD = "resourceIDs";
    public static final String RESOURCE_NAMES_FIELD = "resourceNames";
    public static final String ROLE_IDS_FIELD = "roleIDs";
    public static final String ROLE_NAMES_FIELD = "roleNames";
    public static final String PROJECT_IDS_FIELD = "projectIDs";
    public static final String PROJECT_NAMES_FIELD = "projectNames";

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LResourceGroup";
    public static final String SIMPLE_CLASS_NAME = "ResourceGroup";

    public ResourceGroupModelData()
    {
        this.setResourceIDs(new ArrayList<Long>());
        this.setResourceNames(new ArrayList<String>());
        this.setRoleIds(new ArrayList<Long>());
        this.setRoleNames(new ArrayList<String>());
        this.setProjectIDs(new ArrayList<Long>());
        this.setProjectNames(new ArrayList<String>());

        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public ResourceGroupModelData(String name, Long parentId)
    {
        this();

        this.setName(name);
        this.setParentId(parentId);
    }

    public ResourceGroupModelData(String name)
    {
        this(name, null);
    }

    public ResourceGroupModelData(long id, String name)
    {
        this.setId(id);
        this.setName(name);
    }

    public Long getParentId()
    {
        return this.get(PARENT_ID_FIELD);
    }

    public void setParentId(Long id)
    {
        this.set(PARENT_ID_FIELD, id);
    }

    public Long getLeaderId()
    {
        return this.get(LEADER_ID_FIELD);
    }

    public void setLeaderId(Long id)
    {
        this.set(LEADER_ID_FIELD, id);
    }

    public Long getLeaderName()
    {
        return this.get(LEADER_NAME_FIELD);
    }

    public void setLeaderName(Long id)
    {
        this.set(LEADER_NAME_FIELD, id);
    }

    public List<Long> getResourceIds()
    {
        List<Long> ids = get(RESOURCE_IDS_FIELD);
        if (ids == null)
        {
            ids = new ArrayList<Long>();
            this.set(RESOURCE_IDS_FIELD, ids);
        }

        return ids;
    }

    public void setResourceIDs(List<Long> resourceIDs)
    {
        this.set(RESOURCE_IDS_FIELD, resourceIDs);
    }

    public List<String> getResourceNames()
    {
        return get(RESOURCE_NAMES_FIELD);
    }

    public void setResourceNames(List<String> resourceNames)
    {
        this.set(RESOURCE_NAMES_FIELD, resourceNames);
    }

    public List<Long> getRoleIds()
    {
        List<Long> ids = get(ROLE_IDS_FIELD);
        if (ids == null)
        {
            ids = new ArrayList<Long>();
            this.set(ROLE_IDS_FIELD, ids);
        }

        return ids;
    }

    public void setRoleIds(List<Long> roleIDs)
    {
        this.set(ROLE_IDS_FIELD, roleIDs);
    }

    public List<String> getRoleNames()
    {
        return get(ROLE_NAMES_FIELD);
    }

    public void setRoleNames(List<String> roleNames)
    {
        this.set(ROLE_NAMES_FIELD, roleNames);
    }

    public List<Long> getProjectIds()
    {
        List<Long> ids = get(PROJECT_IDS_FIELD);
        if (ids == null)
        {
            ids = new ArrayList<Long>();
            this.set(PROJECT_IDS_FIELD, ids);
        }

        return ids;
    }

    public void setProjectIDs(List<Long> projectIds)
    {
        this.set(PROJECT_IDS_FIELD, projectIds);
    }

    public List<String> getProjectNames()
    {
        return get(PROJECT_NAMES_FIELD);
    }

    public void setProjectNames(List<String> roleNames)
    {
        this.set(PROJECT_NAMES_FIELD, roleNames);
    }

    /**
     * @return Model type used by reader to handle the studio map model data structure.
     */
    public static ModelType getModelType()
    {
        ModelType type = RecordModelData.getModelType();

        DataField parentIdDataField = new DataField(PARENT_ID_FIELD);
        parentIdDataField.setType(Long.class);

        DataField leaderIdDataField = new DataField(LEADER_ID_FIELD);
        leaderIdDataField.setType(Long.class);

        DataField leaderNameDataField = new DataField(LEADER_NAME_FIELD);
        leaderNameDataField.setType(String.class);

        DataField resourceIdsDataField = new DataField(RESOURCE_IDS_FIELD);
        resourceIdsDataField.setType(List.class);
        resourceIdsDataField.setFormat("Resource");
        DataField resourceNamesDataField = new DataField(RESOURCE_NAMES_FIELD);
        resourceNamesDataField.setType(List.class);

        DataField roleIdsDataField = new DataField(ROLE_IDS_FIELD);
        roleIdsDataField.setType(List.class);
        roleIdsDataField.setFormat("Role");
        DataField roleNamesDataField = new DataField(ROLE_NAMES_FIELD);
        roleNamesDataField.setType(List.class);

        DataField projectIdsDataField = new DataField(PROJECT_IDS_FIELD);
        projectIdsDataField.setType(List.class);
        projectIdsDataField.setFormat("Project");
        DataField projectNamesDataField = new DataField(PROJECT_NAMES_FIELD);
        projectNamesDataField.setType(List.class);

        type.addField(CLASS_NAME_FIELD);
        type.addField(parentIdDataField);
        type.addField(leaderIdDataField);
        type.addField(leaderNameDataField);
        type.addField(resourceIdsDataField);
        type.addField(resourceNamesDataField);
        type.addField(roleIdsDataField);
        type.addField(roleNamesDataField);
        type.addField(projectIdsDataField);
        type.addField(projectNamesDataField);

        return type;
    }
}
