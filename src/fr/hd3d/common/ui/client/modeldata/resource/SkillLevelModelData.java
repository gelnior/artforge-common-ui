package fr.hd3d.common.ui.client.modeldata.resource;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;


public class SkillLevelModelData extends Hd3dModelData
{
    private static final long serialVersionUID = -5484811007789570224L;

    public static final String LEVEL_FIELD = "level";
    public static final String MOTIVATION_FIELD = "motivation";

    public static final String PERSON_ID_FIELD = "personId";
    public static final String PERSON_NAME_FIELD = "personName";
    public static final String SKILL_ID_FIELD = "skillId";
    public static final String SKILL_NAME_FIELD = "skillName";

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LSkillLevel";
    public static final String SIMPLE_CLASS_NAME = "SkillLevel";

    public SkillLevelModelData()
    {
        this.setClassName(CLASS_NAME);
        this.simpleClassName = SIMPLE_CLASS_NAME;
    }

    public SkillLevelModelData(Long id)
    {
        this();

        this.setId(id);
    }

    public SkillLevelModelData(String level, String motivation)
    {
        this();

        this.setLevel(level);
        this.setMotivation(motivation);
    }

    public final String getLevel()
    {
        return get(LEVEL_FIELD);
    }

    public final void setLevel(String level)
    {
        set(LEVEL_FIELD, level);
    }

    public final String getMotivation()
    {
        return get(MOTIVATION_FIELD);
    }

    public final void setMotivation(String motivation)
    {
        set(MOTIVATION_FIELD, motivation);
    }

    public Long getPersonId()
    {
        return this.get(PERSON_ID_FIELD);
    }

    public void setPersonId(Long personId)
    {
        this.set(PERSON_ID_FIELD, personId);
    }

    public String getPersonName()
    {
        return this.get(PERSON_ID_FIELD);
    }

    public void setPersonName(String personName)
    {
        this.set(PERSON_ID_FIELD, personName);
    }

    public Long getSkillId()
    {
        return this.get(SKILL_ID_FIELD);
    }

    public void setSkillId(Long skillId)
    {
        this.set(SKILL_ID_FIELD, skillId);
    }

    public String getSkillName()
    {
        return this.get(SKILL_NAME_FIELD);
    }

    public void setSkillName(String skillName)
    {
        this.set(SKILL_NAME_FIELD, skillName);
    }

    /**
     * @return Model type used by reader to know the manufacturer model data structure.
     */
    public static ModelType getModelType()
    {
        ModelType type = RecordModelData.getModelType();

        type.addField(LEVEL_FIELD);
        type.addField(MOTIVATION_FIELD);

        DataField personIdDataField = new DataField(PERSON_ID_FIELD);
        personIdDataField.setType(Long.class);

        type.addField(personIdDataField);
        type.addField(PERSON_NAME_FIELD);

        DataField skillIdDataField = new DataField(SKILL_ID_FIELD);
        skillIdDataField.setType(Long.class);

        type.addField(skillIdDataField);
        type.addField(SKILL_NAME_FIELD);

        return type;
    }
}
