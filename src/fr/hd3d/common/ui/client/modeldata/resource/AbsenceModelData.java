package fr.hd3d.common.ui.client.modeldata.resource;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.DurationModelData;


/**
 * Model data describing a person absence.
 * 
 * @author HD3D
 */
public class AbsenceModelData extends DurationModelData
{
    private static final long serialVersionUID = -8399455399381068236L;

    public static final String WORKER_ID_FIELD = "workerID";
    public static final String COMMENT_FIELD = "comment";

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LAbsence";
    public static final String SIMPLE_CLASS_NAME = "Absence";

    public AbsenceModelData()
    {
        super();

        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public Long getWorkerID()
    {
        return get(WORKER_ID_FIELD);
    }

    public void setWorkerID(Long workerId)
    {
        set(WORKER_ID_FIELD, workerId);
    }

    public String getComment()
    {
        return get(COMMENT_FIELD);
    }

    public void setComment(String comment)
    {
        set(COMMENT_FIELD, comment);
    }

    public static ModelType getModelType()
    {
        ModelType type = DurationModelData.getModelType();

        DataField workerIdField = new DataField(WORKER_ID_FIELD);
        workerIdField.setType(Long.class);
        DataField commentField = new DataField(COMMENT_FIELD);
        commentField.setType(String.class);

        type.addField(workerIdField);
        type.addField(commentField);

        return type;
    }
}
