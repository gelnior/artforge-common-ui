package fr.hd3d.common.ui.client.modeldata;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.calendar.DatetimeUtil;


public class BaseTaskModelData extends DurationModelData
{

    private static final long serialVersionUID = 7913458632361352949L;

    public static final String DURATION_FIELD = "duration";
    public static final String TASK_GROUP_ID_FIELD = "taskGroupID";
    public static final String EXTRA_LINE_ID_FIELD = "extraLineID";

    public void setDuration(Long duration)
    {
        set(DURATION_FIELD, duration);
    }

    public void setTaskGroupID(Long taskGroupID)
    {
        set(TASK_GROUP_ID_FIELD, taskGroupID);
    }

    public void setExtraLineID(Long extraLineID)
    {
        set(EXTRA_LINE_ID_FIELD, extraLineID);
    }

    public Long getDuration()
    {
        return (Long) get(DURATION_FIELD);
    }

    public Long getDayDuration()
    {
        if (getDuration() != null)
            return getDuration() / DatetimeUtil.DAY_SECONDS;
        else
            return 0L;
    }

    public Long getTaskGroupID()
    {
        return (Long) get(TASK_GROUP_ID_FIELD);
    }

    public Long getExtraLineID()
    {
        return (Long) get(EXTRA_LINE_ID_FIELD);
    }

    public static ModelType getBaseModelType()
    {
        ModelType modelType = DurationModelData.getModelType();
        DataField idDataField = new DataField(ID_FIELD);
        DataField durationDataField = new DataField(DURATION_FIELD);
        DataField taskGroupIdDataField = new DataField(TASK_GROUP_ID_FIELD);
        DataField extraLineIdField = new DataField(EXTRA_LINE_ID_FIELD);

        idDataField.setType(Long.class);
        durationDataField.setType(Long.class);
        taskGroupIdDataField.setType(Long.class);
        extraLineIdField.setType(Long.class);

        modelType.setRoot(ROOT);
        modelType.setTotalName(NB_RECORDS);
        modelType.addField(idDataField);
        modelType.addField(durationDataField);
        modelType.addField(taskGroupIdDataField);
        modelType.addField(extraLineIdField);

        return modelType;
    }
}
