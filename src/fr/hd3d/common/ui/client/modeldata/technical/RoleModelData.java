package fr.hd3d.common.ui.client.modeldata.technical;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;


public class RoleModelData extends RecordModelData
{
    /** Automatically generated serial version number. */
    private static final long serialVersionUID = 6044953841081053128L;

    public static final String RESOURCEGROUP_IDS_FIELD = "resourceGroupIDs";
    public static final String RESOURCEGROUP_NAMES_FIELD = "resourceGroupNames";

    public static final String PERMISSIONS_FIELD = "permissions";
    public static final String BANS_FIELD = "bans";

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LRole";
    public static final String SIMPLE_CLASS_NAME = "Role";

    public RoleModelData()
    {
        this.setResourceGroupIds(new ArrayList<Long>());
        this.setResourceGroupNames(new ArrayList<String>());

        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public RoleModelData(String name)
    {
        this();

        this.setName(name);
    }

    public List<Long> getResourceGroupIds()
    {
        return get(RESOURCEGROUP_IDS_FIELD);
    }

    public void setResourceGroupIds(List<Long> resourceIDs)
    {
        this.set(RESOURCEGROUP_IDS_FIELD, resourceIDs);
    }

    public List<String> getResourceGroupNames()
    {
        return get(RESOURCEGROUP_NAMES_FIELD);
    }

    public void setResourceGroupNames(List<String> resourceGroupNames)
    {
        this.set(RESOURCEGROUP_NAMES_FIELD, resourceGroupNames);
    }

    public List<String> getPermissions()
    {
        List<String> perms = get(PERMISSIONS_FIELD);
        if (perms == null)
        {
            perms = new ArrayList<String>();
            setPermissions(perms);
        }
        return perms;
    }

    public void setPermissions(List<String> permissions)
    {
        this.set(PERMISSIONS_FIELD, permissions);
    }

    public List<String> getBans()
    {
        List<String> bans = get(BANS_FIELD);
        if (bans == null)
        {
            bans = new ArrayList<String>();
            setBans(bans);
        }
        return bans;
    }

    public void setBans(List<String> bans)
    {
        this.set(BANS_FIELD, bans);
    }

    /**
     * @return Model type used by reader to handle the studio map model data structure.
     */
    public static ModelType getModelType()
    {
        ModelType type = RecordModelData.getModelType();

        DataField resourceGroupIdsDataField = new DataField(RESOURCEGROUP_IDS_FIELD);
        resourceGroupIdsDataField.setType(List.class);
        resourceGroupIdsDataField.setFormat(ResourceGroupModelData.SIMPLE_CLASS_NAME);

        DataField resourceGroupNamesDataField = new DataField(RESOURCEGROUP_NAMES_FIELD);
        resourceGroupNamesDataField.setType(List.class);
        resourceGroupNamesDataField.setFormat(ResourceGroupModelData.SIMPLE_CLASS_NAME);

        DataField permissionsDataField = new DataField(PERMISSIONS_FIELD);
        permissionsDataField.setType(List.class);
        permissionsDataField.setFormat("Permissions");

        DataField bansDataField = new DataField(BANS_FIELD);
        bansDataField.setType(List.class);
        bansDataField.setFormat("Bans");

        type.addField(CLASS_NAME_FIELD);
        type.addField(resourceGroupIdsDataField);
        type.addField(resourceGroupNamesDataField);
        type.addField(permissionsDataField);
        type.addField(bansDataField);

        return type;
    }
}
