package fr.hd3d.common.ui.client.modeldata.technical;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;


public class ListValuesModelData extends RecordModelData
{

    private static final long serialVersionUID = 5733863964218791098L;

    public static final String SEPARATOR_FIELD = "separator";
    public static final String VALUES_FIELD = "values";

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LListValues";
    public static final String SIMPLE_CLASS_NAME = "ListValues";

    public ListValuesModelData()
    {
        super();

        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public void setSeparator(String separator)
    {
        this.set(SEPARATOR_FIELD, separator);
    }

    public String getSeparator()
    {
        return this.get(SEPARATOR_FIELD);
    }

    public void setValues(String values)
    {
        this.set(VALUES_FIELD, values);
    }

    public String getValues()
    {
        return this.get(VALUES_FIELD);
    }

    public static ModelType getModelType()
    {
        ModelType type = RecordModelData.getModelType();

        type.addField(SEPARATOR_FIELD);
        type.addField(VALUES_FIELD);

        return type;
    }
}
