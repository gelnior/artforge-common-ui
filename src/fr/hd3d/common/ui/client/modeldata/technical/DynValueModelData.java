package fr.hd3d.common.ui.client.modeldata.technical;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;


/**
 * This object represents dynamic attribute attached to a specific entity (class).
 * 
 * @author HD3D
 */
public class DynValueModelData extends Hd3dModelData
{
    private static final long serialVersionUID = -3277734931759285204L;
    public static final String VALUE_FIELD = "value";
    public static final String PARENT_ID_FIELD = "parent";
    public static final String DYN_CLASS_ID_FIELD = "classDynMetaDataType";
    public static final String PARENT_TYPE_FIELD = "parentType";
    public static final String DYN_CLASS_NAME_FIELD = "classDynMetaDataTypeName";
    public static final String DYN_TYPE_FIELD = "classDynMetaDataType_Type";

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LDynMetaDataValue";
    public static final String SIMPLE_CLASS_NAME = "DynMetaDataValue";

    public DynValueModelData()
    {
        super();

        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public String getValue()
    {
        return this.get(VALUE_FIELD);
    }

    public void setValue(String value)
    {
        this.set(VALUE_FIELD, value);
    }

    public Long getParentId()
    {
        return this.get(PARENT_ID_FIELD);
    }

    public void setParentId(Long id)
    {
        this.set(PARENT_ID_FIELD, id);
    }

    public Long getDynClassId()
    {
        return this.get(DYN_CLASS_ID_FIELD);
    }

    public void setDynClassId(Long id)
    {
        this.set(DYN_CLASS_ID_FIELD, id);
    }

    public String getParentType()
    {
        return this.get(PARENT_TYPE_FIELD);
    }

    public void setParentType(String type)
    {
        this.set(PARENT_TYPE_FIELD, type);
    }

    public String getDynClassName()
    {
        return this.get(DYN_CLASS_NAME_FIELD);
    }

    public void setDynClassName(String className)
    {
        this.set(DYN_CLASS_NAME_FIELD, className);
    }

    public String getDynType()
    {
        return this.get(DYN_TYPE_FIELD);
    }

    public void setDynType(String type)
    {
        this.set(DYN_TYPE_FIELD, type);
    }

    /**
     * @return Model type used by reader to handle the device model data structure.
     */
    public static ModelType getModelType()
    {
        ModelType type = RecordModelData.getModelType();

        DataField parentIdField = new DataField(PARENT_ID_FIELD);
        parentIdField.setType(Long.class);

        DataField dynClassIdField = new DataField(DYN_CLASS_ID_FIELD);
        dynClassIdField.setType(Long.class);

        type.addField(VALUE_FIELD);
        type.addField(parentIdField);
        type.addField(dynClassIdField);
        type.addField(PARENT_TYPE_FIELD);
        type.addField(DYN_CLASS_NAME_FIELD);
        type.addField(DYN_TYPE_FIELD);

        return type;
    }
}
