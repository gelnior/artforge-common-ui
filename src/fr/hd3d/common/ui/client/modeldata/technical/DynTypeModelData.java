package fr.hd3d.common.ui.client.modeldata.technical;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;


/**
 * This object represent dynamic attribute type. A class attribute always references a type.
 * 
 * @author HD3D
 */
public class DynTypeModelData extends RecordModelData
{
    private static final long serialVersionUID = -3091870474363403665L;

    public static final String NATURE_FIELD = "nature";
    public static final String TYPE_FIELD = "type";
    public static final String DEFAULT_VALUE_FIELD = "defaultValue";
    public static final String STRUCT_NAME_FIELD = "structName";
    public static final String STRUCT_ID_FIELD = "structId";

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LDynMetaDataType";
    public static final String SIMPLE_CLASS_NAME = "DynMetaDataType";

    public static final String SIMPLE_NATURE = "simple";
    public static final String LIST_VALUES_STRUCT_NAME = "ListValues";

    public DynTypeModelData()
    {
        super();
        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public String getNature()
    {
        return this.get(NATURE_FIELD);
    }

    public String getType()
    {
        return this.get(TYPE_FIELD);
    }

    public String getDefaultValue()
    {
        return this.get(DEFAULT_VALUE_FIELD);
    }

    public String getStructName()
    {
        return this.get(STRUCT_NAME_FIELD);
    }

    public Long getStructId()
    {
        return this.get(STRUCT_ID_FIELD);
    }

    public void setNature(String nature)
    {
        this.set(NATURE_FIELD, nature);
    }

    public void setType(String type)
    {
        this.set(TYPE_FIELD, type);
    }

    public void setDefaultValue(String defaultValue)
    {
        this.set(DEFAULT_VALUE_FIELD, defaultValue);
    }

    public void setStructName(String structName)
    {
        this.set(STRUCT_NAME_FIELD, structName);
    }

    public void setStructId(Long structId)
    {
        this.set(STRUCT_ID_FIELD, structId);
    }

    /**
     * @return Model type used by reader to handle the class dyn meta data model data structure.
     */
    public static ModelType getModelType()
    {
        ModelType type = RecordModelData.getModelType();

        DataField structIdField = new DataField(STRUCT_ID_FIELD);
        structIdField.setType(Long.class);

        type.addField(NATURE_FIELD);
        type.addField(TYPE_FIELD);
        type.addField(DEFAULT_VALUE_FIELD);
        type.addField(STRUCT_NAME_FIELD);
        type.addField(structIdField);

        return type;
    }
}
