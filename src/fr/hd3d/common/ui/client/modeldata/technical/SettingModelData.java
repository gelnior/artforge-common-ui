package fr.hd3d.common.ui.client.modeldata.technical;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;


/**
 * SettingModelData describes a user setting. Setting is a way to store user context data inside database. It is useful
 * to reload last user context when he comes back in an HD3D application.
 * 
 * @author HD3D
 */
public class SettingModelData extends Hd3dModelData
{
    /** Automatically generated serial version number. */
    private static final long serialVersionUID = -7002413712913012251L;

    public static final String KEY_FIELD = "key";
    public static final String VALUE_FIELD = "value";

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LSetting";
    public static final String SIMPLE_CLASS_NAME = "Setting";

    public SettingModelData()
    {
        super();
        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public SettingModelData(String key, String value)
    {
        this();
        this.setKey(key);
        this.setValue(value);
    }

    public String getKey()
    {
        return this.get(KEY_FIELD);
    }

    public void setKey(String key)
    {
        this.set(KEY_FIELD, key);
    }

    public String getValue()
    {
        return this.get(VALUE_FIELD);
    }

    public void setValue(String value)
    {
        this.set(VALUE_FIELD, value);
    }

    /**
     * @return Model type used by reader to know the manufacturer model data structure.
     */
    public static ModelType getModelType()
    {
        ModelType type = Hd3dModelData.getModelType();

        DataField keyDataField = new DataField(KEY_FIELD);
        keyDataField.setType(String.class);

        DataField valueDataField = new DataField(VALUE_FIELD);
        valueDataField.setType(String.class);

        type.addField(keyDataField);
        type.addField(valueDataField);

        return type;
    }
}
