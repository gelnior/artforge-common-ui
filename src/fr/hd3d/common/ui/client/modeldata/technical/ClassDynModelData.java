package fr.hd3d.common.ui.client.modeldata.technical;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;


/**
 * This object represent dynamic attribute attached to a specific entity (class).
 * 
 * @author HD3D
 */
public class ClassDynModelData extends RecordModelData
{
    private static final long serialVersionUID = -3277734931759285204L;

    public static final String DEFAULT_PREDICATE = "*";

    public static final String ENTITY_FIELD = "className";
    public static final String PREDICATE_FIELD = "predicate";
    public static final String DYNMETADATATYPE_FIELD = "dynMetaDataType";
    public static final String DYNMETADATATYPE_NAME_FIELD = "dynMetaDataTypeName";
    public static final String NATURE_FIELD = "dynMetaDataTypeNature";
    public static final String DEFAULT_VALUE_FIELD = "defaultValue";

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LClassDynMetaDataType";
    public static final String SIMPLE_CLASS_NAME = "ClassDynMetaDataType";

    public ClassDynModelData()
    {
        super();

        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public void setEntityName(String className)
    {
        this.set(ENTITY_FIELD, className);
    }

    public String getEntityName()
    {
        return this.get(ENTITY_FIELD);
    }

    public void setPredicate(String predicate)
    {
        this.set(PREDICATE_FIELD, predicate);
    }

    public String getPredicate()
    {
        return this.get(PREDICATE_FIELD);
    }

    public String getNature()
    {
        return this.get(NATURE_FIELD);
    }

    public void setNature(String nature)
    {
        this.set(NATURE_FIELD, nature);
    }

    public String getDefaultValue()
    {
        return this.get(DEFAULT_VALUE_FIELD);
    }

    public void setDefaultValue(String defaultValue)
    {
        this.set(DEFAULT_VALUE_FIELD, defaultValue);
    }

    public Long getDynMetaDataType()
    {
        return this.get(DYNMETADATATYPE_FIELD);
    }

    public void setDynMetaDataType(Long dynMetaDataType)
    {
        this.set(DYNMETADATATYPE_FIELD, dynMetaDataType);
    }

    public String getDynMetaDataTypeName()
    {
        return this.get(DYNMETADATATYPE_NAME_FIELD);
    }

    public void setDynMetaDataTypeName(String dynMetaDataType)
    {
        this.set(DYNMETADATATYPE_NAME_FIELD, dynMetaDataType);
    }

    /**
     * @return Model type used by reader to handle the device model data structure.
     */
    public static ModelType getModelType()
    {
        ModelType type = RecordModelData.getModelType();

        DataField typeField = new DataField(DYNMETADATATYPE_FIELD);
        typeField.setType(Long.class);

        type.addField(ENTITY_FIELD);
        type.addField(PREDICATE_FIELD);
        type.addField(NATURE_FIELD);
        type.addField(DEFAULT_VALUE_FIELD);
        type.addField(typeField);
        type.addField(DYNMETADATATYPE_NAME_FIELD);

        return type;
    }
}
