package fr.hd3d.common.ui.client.modeldata.technical;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.BaseModelData;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.util.field.LongDataField;
import fr.hd3d.common.ui.client.widget.explorer.model.DataFieldArray;


public class SheetDataModelData extends Hd3dModelData
{
    private static final long serialVersionUID = 393755533621010741L;

    public static final String NAME_FIELD = "name";
    public static final String VALUE_FIELD = "value";
    public static final String ITEMS_FIELD = "items";
    public static final String GROUPS_FIELD = "groups";

    public static final ModelType getDataModelType()
    {
        ModelType itemValueModelType = new ModelType();
        itemValueModelType.addField(new LongDataField(ID_FIELD));
        itemValueModelType.addField(NAME_FIELD);
        itemValueModelType.addField(VALUE_FIELD);

        ModelType groupModelType = new ModelType();
        groupModelType.addField(new LongDataField(ID_FIELD));
        DataFieldArray itemField = new DataFieldArray(ITEMS_FIELD);
        itemField.setModelType(itemValueModelType);
        groupModelType.addField(itemField);

        ModelType objectModelType = new ModelType();
        objectModelType.setRoot(ROOT);
        objectModelType.setTotalName(NB_RECORDS);
        objectModelType.addField(new LongDataField(ID_FIELD));
        DataFieldArray groupField = new DataFieldArray(GROUPS_FIELD);
        groupField.setModelType(groupModelType);
        objectModelType.addField(groupField);

        return objectModelType;
    }

    public List<BaseModelData> getGroups()
    {
        return get(GROUPS_FIELD);
    }

    public List<BaseModelData> getValues()
    {
        List<BaseModelData> values = new ArrayList<BaseModelData>();

        List<BaseModelData> groups = get(GROUPS_FIELD);
        for (BaseModelData group : groups)
        {
            List<BaseModelData> items = group.get(ITEMS_FIELD);
            for (BaseModelData item : items)
            {
                values.add(item);
            }
        }
        return values;
    }

}
