package fr.hd3d.common.ui.client.modeldata.technical;

import java.util.List;

import com.extjs.gxt.ui.client.data.BaseTreeModel;
import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.widget.workobjecttree.Urls;


public class TagCategoryModelData extends Hd3dModelData
{
    private static final long serialVersionUID = 21485281406632134L;
    private static final String TAGCATEGORIES_NAME = "name";
    private static final String TAGCATEGORIES_PARENT = "parent";
    private static final String TAGCATEGORIES_BOUNDTAGS = "boundTags";
    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LTagCategory";
    public static final String SIMPLE_CLASS_NAME = "TagCategory";

    public TagCategoryModelData()
    {
        super();

        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public TagCategoryModelData(String string)
    {
        this();

        this.setName(string);
    }

    public void setName(String name)
    {
        set(TAGCATEGORIES_NAME, name);
    }

    public void setParent(Long parent)
    {
        set(TAGCATEGORIES_PARENT, parent);
    }

    public String getName()
    {
        return (String) get(TAGCATEGORIES_NAME);
    }

    public Long getParent()
    {
        return get(TAGCATEGORIES_PARENT);
    }

    public void setTags(List<Long> boundTags)
    {
        set(TAGCATEGORIES_BOUNDTAGS, boundTags);
    }

    @SuppressWarnings("unchecked")
    public List<Long> getTags()
    {
        return (List<Long>) get(TAGCATEGORIES_BOUNDTAGS);
    }

    @Override
    public String getPath()
    {
        return Urls.urlTagCat;
    }

    @Override
    public String getPermissionPath()
    {
        return getPath().replace("/", ":");
    }

    public static ModelType getModelType()
    {
        ModelType modelType = Hd3dModelData.getModelType();

        modelType.addField(TAGCATEGORIES_NAME);

        DataField parent = new DataField(TAGCATEGORIES_PARENT);
        parent.setType(Long.class);
        DataField boundTags = new DataField(TAGCATEGORIES_BOUNDTAGS);
        parent.setType(String.class);
        modelType.addField(parent);
        modelType.addField(boundTags);

        return modelType;
    }

    public String getTagsString()
    {
        List<Long> l = getTags();
        StringBuilder ret = new StringBuilder();
        String sep = "";
        for (Long id : l)
        {
            ret.append(ret + sep + id.toString());
            sep = ",";
        }
        return ret.toString();
    }

    public static String getTagsString(BaseTreeModel m)
    {
        List<Long> l = m.get(TAGCATEGORIES_BOUNDTAGS);
        StringBuilder ret = new StringBuilder();
        String sep = "";
        for (Long id : l)
        {
            ret.append(ret + sep + id.toString());
            sep = ",";
        }
        return ret.toString();
    }
}
