package fr.hd3d.common.ui.client.modeldata.technical;

import java.util.Date;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;


/**
 * AuditModelData describes a log line.
 * 
 * @author HD3D
 */
public class AuditModelData extends Hd3dModelData
{
    /** Automatically generated serial version number. */
    private static final long serialVersionUID = -7002413712913012251L;

    public static final String OPERATION_FIELD = "operation";
    public static final String DATE_FIELD = "date";
    public static final String DATE_MS_FIELD = "dateMs";
    public static final String ENTITY_ID_FIELD = "entityId";
    public static final String ENTITY_NAME_FIELD = "entityName";
    public static final String MODIFICATION_FIELD = "modification";
    public static final String FIRST_NAME_FIELD = "firstName";
    public static final String LAST_NAME_FIELD = "lastName";
    public static final String LOGIN_FIELD = "login";

    public static final String CLASS_NAME = "fr.hd3d.model.audit.Audit";
    public static final String SIMPLE_CLASS_NAME = "Audit";

    /**
     * Default constructor : sets an empty log line with no name.
     */
    public AuditModelData()
    {
        super();
        this.setClassName(CLASS_NAME);
    }

    /**
     * Sets up a log line.
     * 
     * @param operation
     *            Operation logged : CREATE, DELETE or UPDATE.
     * @param date
     *            Date when operation was performed.
     * @param entityId
     *            ID of the Entity involved in the operation.
     * @param entityName
     *            Name of the Entity involved in the operation.
     * @param modification
     *            Text describing the new value and the old one.
     */
    public AuditModelData(String operation, Date date, Long entityId, String entityName, String modification)
    {
        super();
        this.setOperation(operation);
        this.setDate(date);
        this.setEntityId(entityId);
        this.setEntityName(entityName);
        this.setModification(modification);
    }

    public String getOperation()
    {
        return this.get(OPERATION_FIELD);
    }

    public void setOperation(String operation)
    {
        this.set(OPERATION_FIELD, operation);
    }

    public Date getDate()
    {
        return this.get(DATE_FIELD);
    }

    public void setDate(Date date)
    {
        this.set(DATE_FIELD, date);
    }

    public Long getEntityId()
    {
        return this.get(ENTITY_ID_FIELD);
    }

    public void setEntityId(Long entityId)
    {
        this.set(ENTITY_ID_FIELD, entityId);
    }

    public String getEntityName()
    {
        return this.get(ENTITY_NAME_FIELD);
    }

    public void setEntityName(String entityName)
    {
        this.set(ENTITY_NAME_FIELD, entityName);
    }

    public String getModification()
    {
        return this.get(MODIFICATION_FIELD);
    }

    public void setModification(String modification)
    {
        this.set(MODIFICATION_FIELD, modification);
    }

    public Long getDateMs()
    {
        return this.get(DATE_MS_FIELD);
    }

    public String getFirstName()
    {
        return this.get(OPERATION_FIELD);
    }

    public void setFirstName(String firstName)
    {
        this.set(OPERATION_FIELD, firstName);
    }

    public String getLastName()
    {
        return this.get(OPERATION_FIELD);
    }

    public void setLastName(String lastName)
    {
        this.set(OPERATION_FIELD, lastName);
    }

    public String getUser()
    {
        return this.get(OPERATION_FIELD);
    }

    public void setLogin(String login)
    {
        this.set(LOGIN_FIELD, login);
    }

}
