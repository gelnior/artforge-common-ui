package fr.hd3d.common.ui.client.modeldata.technical;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.widget.explorer.controller.SheetController;
import fr.hd3d.common.ui.client.widget.explorer.model.modeldata.IColumn;
import fr.hd3d.common.ui.client.widget.explorer.model.modeldata.IColumnGroup;


public class SheetModelData extends RecordModelData
{
    private static final long serialVersionUID = 6547979615562026868L;

    public SheetController controller = new SheetController(this);
    private List<ItemGroupModelData> groups = new ArrayList<ItemGroupModelData>();

    private boolean groupsLoaded = false;

    public static final String PROJECT_ID_FIELD = "project";
    public static final String ITEM_GROUP_FIELD = "itemGroups";
    public static final String DESCRIPTION_FIELD = "description";
    public static final String TYPE_FIELD = "type";

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LSheet";
    public static final String SIMPLE_CLASS_NAME = "Sheet";

    public SheetModelData()
    {
        super();

        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public SheetModelData(Long id, String name)
    {
        this();

        setId(id);
        setName(name);
    }

    public String getDescription()
    {
        return get(DESCRIPTION_FIELD);
    }

    public void setDescription(String descrpition)
    {
        this.set(DESCRIPTION_FIELD, descrpition);
    }

    public String getType()
    {
        return get(TYPE_FIELD);
    }

    public void setType(String type)
    {
        this.set(TYPE_FIELD, type);
    }

    public Long getProjectId()
    {
        return this.get(PROJECT_ID_FIELD);
    }

    public void setProjectId(Long projectId)
    {
        this.set(PROJECT_ID_FIELD, projectId);
    }

    public void setItemGroupIds(List<Long> ids)
    {
        this.set(ITEM_GROUP_FIELD, ids);
    }

    public List<ItemGroupModelData> getColumnGroups()
    {
        return this.groups;
    }

    // public void setColumnGroups(List<IColumnGroup> groups)
    // {
    // for (IColumnGroup igroup : groups)
    // {
    // ((ItemGroupModelData) igroup).setSheet(this);
    // }
    // this.groups = groups;
    // this.setGroupLoaded(true);
    // }

    public void setItemGroups(List<ItemGroupModelData> groups)
    {
        for (IColumnGroup igroup : groups)
        {
            ((ItemGroupModelData) igroup).setSheet(this);
        }
        this.groups = groups;
        this.setGroupLoaded(true);
    }

    public boolean areGroupsLoaded()
    {
        return groupsLoaded;
    }

    public void setGroupLoaded(Boolean groupLoaded)
    {
        this.groupsLoaded = groupLoaded;
        this.controller.setGroupsLoaded(groupLoaded);
    }

    @Override
    public String getPath()
    {
        String path = "";
        if (this.getId() != null)
        {
            path = this.getDefaultPath();
        }
        else
        {
            if (this.getProjectId() != null)
            {
                path += ServicesPath.PROJECTS + this.getProjectId() + "/";
            }
            path += ServicesPath.SHEETS;
        }
        return path;
    }

    public boolean areGroupsItemsLoaded()
    {
        if (groupsLoaded == true)
        {
            List<ItemGroupModelData> groups = this.getColumnGroups();
            for (ItemGroupModelData igroup : groups)
            {
                ItemGroupModelData group = igroup;
                if (group.areColumnsLoaded() == false)
                {
                    return false;
                }
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    static public ModelType getModelType()
    {
        ModelType type = RecordModelData.getModelType();

        DataField projectId = new DataField(PROJECT_ID_FIELD);
        projectId.setType(Long.class);
        type.addField(projectId);

        type.addField(ITEM_GROUP_FIELD);
        type.addField(DESCRIPTION_FIELD);
        type.addField(BOUND_CLASS_NAME_FIELD);
        type.addField(TYPE_FIELD);

        return type;
    }

    public List<IColumn> getColumns()
    {
        List<IColumn> items = new ArrayList<IColumn>();
        if (CollectionUtils.isNotEmpty(groups))
        {
            for (IColumnGroup group : groups)
            {
                List<IColumn> columns = group.getColumns();
                if (columns != null)
                {
                    items.addAll(columns);
                }
            }

            return items;
        }
        else
        {
            return new ArrayList<IColumn>();
        }
    }

    public SheetController getController()
    {
        return controller;
    }

    @Deprecated
    public String getPostUrl()
    {
        return "sheets/";
    }

    @Deprecated
    public String getPutUrl()
    {
        return "sheets/" + this.getId();
    }

    @Deprecated
    public String getJsonRepresentation()
    {
        String id = "";
        if (this.getId() != null && this.getId() != 0)
        {
            id = " \"id\":" + this.getId() + ", ";
        }

        String description = this.getDescription();
        if (description == null || description.length() == 0)
        {
            description = "";
        }
        String project = "null";
        Long projectId = this.getProjectId();
        if (projectId != null)
        {
            project = projectId.toString();
        }

        String json = "{\"class\": \"fr.hd3d.model.lightweight.impl.LSheet\", " + id + " \"name\":\"" + this.getName()
                + "\",  \"project\":" + project + ",  \"description\":\"" + description + "\",   \"type\":\""
                + this.getType() + "\", \"boundClassName\":\""
                + this.getBoundClassName().replaceAll("fr.hd3d.model.persistence.I", "") + "\"}";
        return json;
    }

    public List<ItemGroupModelData> getGroups()
    {
        return groups;
    }

    /**
     * @param column
     *            the ID to test
     * @return True if column has same ID as given column ID.
     */
    public Boolean isColumn(String column)
    {
        Boolean isColumn = false;
        List<IColumn> columns = this.getColumns();

        String columnString = column;
        if (columnString.startsWith("-") && columnString.length() > 0)
            columnString = columnString.substring(1);
        Long columnLong = Long.valueOf(columnString);

        for (IColumn sheetColumn : columns)
        {
            if (sheetColumn.getId().longValue() == columnLong.longValue())
            {
                isColumn = true;
                continue;
            }
        }

        return isColumn;
    }

    public Long getColumnId(String defaultColumn)
    {
        List<IColumn> columns = this.getColumns();

        if (CollectionUtils.isNotEmpty(columns))
        {
            for (IColumn item : columns)
            {
                if (item.getQuery().equals(defaultColumn))
                    return item.getId();
            }
            return columns.get(0).getId();
        }
        return null;
    }
}
