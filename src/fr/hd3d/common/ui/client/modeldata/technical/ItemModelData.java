package fr.hd3d.common.ui.client.modeldata.technical;

import java.util.List;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.client.Editor;
import fr.hd3d.common.client.Renderer;
import fr.hd3d.common.client.Sheet;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectTypeModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.util.field.LongDataField;
import fr.hd3d.common.ui.client.widget.explorer.model.modeldata.IColumn;


public class ItemModelData extends RecordModelData implements IColumn
{
    private static final long serialVersionUID = 4680743609444398669L;

    public static final String QUERY = "query";
    public static final String META_TYPE = "metaType";
    public static final String ITEM_TYPE = "type";
    public static final String RENDERER = "renderer";
    public static final String EDITOR = "editor";
    public static final String ITEM_GROUP_ID = "itemGroup";
    public static final String ITEM_GROUP = "item-Group";
    public static final String TRANSFORMER_FIELD = "transformer";
    public static final String PARAMETER_FIELD = "transformerParameters";
    public static final String PARAM_FIELD = "param";
    public static final String PARAM2_FIELD = "parameter";
    public static final String LIST_VALUES_FIELD = "listValues";
    public static final String CLASS_DYN_NAME_FIELD = "classDynName";

    public static final String NAME_FIELD = "name";

    public static final String APPROVAL_NOTE_QUERY = "approvalNotes";
    public static final String SHOTS_APPROVAL_COMPLETION_QUERY = "shotsApprovalCompletion";

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LItem";
    public static final String SIMPLE_CLASS_NAME = "Item";

    public static final String EXTRA_FIELD = "extra";

    public static ModelType getModelType()
    {
        ModelType type = Hd3dModelData.getModelType();

        DataField groupId = new DataField(ITEM_GROUP_ID, ITEM_GROUP);
        groupId.setType(Long.class);
        type.addField(groupId);

        DataField listValuesList = new DataField(LIST_VALUES_FIELD);
        listValuesList.setType(List.class);

        type.addField(QUERY);
        type.addField(NAME);
        type.addField(META_TYPE);
        type.addField(VERSION_FIELD);
        type.addField(ITEM_TYPE);
        type.addField(RENDERER);
        type.addField(EDITOR);
        type.addField(PARAMETER_FIELD);
        type.addField(PARAM_FIELD);
        type.addField(PARAM2_FIELD);
        type.addField(new LongDataField(ITEM_GROUP_ID));
        type.addField(LIST_VALUES_FIELD);

        return type;
    }

    public ItemModelData()
    {
        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public void setQuery(String query)
    {
        set(QUERY, query);
    }

    public String getQuery()
    {
        Object query = get(QUERY);
        return query.toString();
    }

    public String getType()
    {
        return get(META_TYPE);
    }

    public String getMetaType()
    {
        return get(META_TYPE);
    }

    public String setType(String type)
    {
        return set(META_TYPE, type);
    }

    public void setItemType(String type)
    {
        this.set(ITEM_TYPE, type);
    }

    public String getItemType()
    {
        return get(ITEM_TYPE);
    }

    public Long getItemGroupId()
    {
        return get(ITEM_GROUP_ID);
    }

    public void setItemGroupId(Long id)
    {
        set(ITEM_GROUP_ID, id);
    }

    public void setItemGroup(ItemGroupModelData group)
    {
        set(ITEM_GROUP, group);
        if (group != null)
            set(ITEM_GROUP_ID, group.getId());
        else
            set(ITEM_GROUP_ID, null);
    }

    public String getClassDynName()
    {
        return get(CLASS_DYN_NAME_FIELD);
    }

    public void setClassDynName(String name)
    {
        set(CLASS_DYN_NAME_FIELD, name);
    }

    public ItemGroupModelData getItemGroup()
    {
        return get(ITEM_GROUP);
    }

    public void setRenderer(String renderer)
    {
        this.set(RENDERER, renderer);
    }

    public void setEditor(String editor)
    {
        this.set(EDITOR, editor);
    }

    public String getParam()
    {
        return this.get(PARAM_FIELD);
    }

    public void setParam(String param)
    {
        this.set(PARAM_FIELD, param);
    }

    public String getRenderer()
    {
        String renderer = this.get(RENDERER);
        if (renderer == null)
        {
            renderer = "";
        }
        return renderer;
    }

    public String getEditor()
    {
        String editor = this.get(EDITOR);
        if (editor == null)
        {
            editor = "";
        }
        return editor;
    }

    public void setTransformer(String transformer)
    {
        this.set(TRANSFORMER_FIELD, transformer);
    }

    public String getTransformer()
    {
        return this.get(TRANSFORMER_FIELD);
    }

    public void setParameter(String parameter)
    {
        this.set(PARAMETER_FIELD, parameter);
    }

    public String getParameter()
    {
        return this.get(PARAMETER_FIELD);
    }

    public void setListValues(List<String> values)
    {
        this.set(LIST_VALUES_FIELD, values);
    }

    public List<String> getListValues()
    {
        return this.get(LIST_VALUES_FIELD);
    }

    @Override
    public String getPath()
    {
        if (getDefaultPath() == null)
        {
            if (this.getId() == null)
            {
                return ServicesPath.SHEETS + this.getItemGroup().getSheetId() + "/" + ServicesPath.ITEM_GROUPS
                        + this.getItemGroupId() + "/" + ServicesPath.ITEMS;
            }
            else
            {
                return ServicesPath.SHEETS + this.getItemGroup().getSheetId() + "/" + ServicesPath.ITEM_GROUPS
                        + this.getItemGroupId() + "/" + ServicesPath.ITEMS + this.getId();
            }
        }
        else
        {
            return this.getDefaultPath();
        }
    }

    public String getDataIndex()
    {
        final String itemType = this.getItemType();

        if (itemType == null)
            return "id_" + this.getId().toString();

        if (itemType.equals(Sheet.ITEMTYPE_ATTRIBUTE) || itemType.equals(Sheet.ITEMTYPE_METADATA))
        {
            return this.getQuery();
        }
        else if (itemType.equals(Sheet.ITEMTYPE_METHOD))
        {
            final String parameter = this.getParameter();
            if (parameter != null && parameter.length() > 0)
            {
                return this.getQuery() + "_" + parameter;
            }
            return this.getQuery();
        }

        return "id_" + this.getId().toString();
    }

    public String getJsonRepresentation()
    {
        String transformer = "";
        String parameter = "";
        String id = "";

        final Long _id = this.getId();
        final String _transformer = this.getTransformer();
        final String _parameter = this.getParameter();

        if (_id != null && !_id.equals(0L))
        {
            id = " \"id\":" + _id + ", ";
        }
        if (_transformer != null && !_transformer.equals(""))
        {
            transformer = " \"" + ItemModelData.TRANSFORMER_FIELD + "\":\"" + _transformer + "\", ";
        }
        if (_parameter != null)
        {
            parameter = " \"" + ItemModelData.PARAMETER_FIELD + "\":\"" + _parameter + "\", ";
        }

        String itemType = "";
        itemType = this.getItemType();

        // String json = "{\"class\": \"" + this.getLightClass() + "\", " + id + transformer + parameter +
        // "  \"name\":\""
        // + this.getName() + "\",   \"itemGroup\":" + this.getItemGroupId() + ", \"metaType\":\""
        // + this.getType() + "\", \"controlContent\":\" \", \"query\":\"" + this.getQuery() + "\", \"type\":\""
        // + itemType + "\", \"renderer\":\"" + this.getRenderer() + "\", \"editor\":\"" + this.getEditor()
        // + "\"  }";

        StringBuilder json = new StringBuilder("{\"class\": \"").append(this.getClassName()).append("\", ").append(id)
                .append(transformer).append(parameter).append("  \"name\":\"").append(this.getName()).append(
                        "\",   \"itemGroup\":").append(this.getItemGroupId()).append(", \"metaType\":\"").append(
                        this.getType()).append("\", \"controlContent\":\" \", \"query\":\"").append(this.getQuery())
                .append("\", \"type\":\"").append(itemType).append("\", \"renderer\":\"").append(this.getRenderer())
                .append("\", \"editor\":\"").append(this.getEditor()).append("\"  }");

        return json.toString();
    }

    @Deprecated
    public String getPostUrl()
    {
        return this.getItemGroup().getPutUrl() + "items/";
    }

    @Deprecated
    public String getPutUrl()
    {
        return this.getItemGroup().getPutUrl() + "items/" + this.getId() + "/";
    }

    public void setDefaultRendererAndEditor()
    {

        String type = this.getType();

        if (Renderer.THUMBNAIL.equals(this.getQuery()) || PersonModelData.PHOTO_PATH_FIELD.equals(this.getQuery()))
        {
            this.setRenderer(Renderer.THUMBNAIL);
            this.setEditor("");
        }
        else if ("fr.hd3d.services.resources.collectionquery.TaskActivitiesDurationCollectionQuery".equals(this
                .getQuery()))
        {
            this.setRenderer(Renderer.DURATION);
            this.setEditor("");
        }
        else if ("fr.hd3d.services.resources.collectionquery.StepsByTaskTypeCollectionQuery".equals(this.getQuery()))
        {
            this.setRenderer("step");
            this.setEditor("step");
        }
        else if (FieldUtils.isList(type))
        {
            this.setRenderer("");
            this.setEditor(Editor.LIST);
        }
        else if (FieldUtils.isString(type))
        {
            this.setRenderer("");
            this.setEditor(Editor.SHORT_TEXT);
        }
        else if (FieldUtils.isBoolean(type))
        {
            this.setRenderer(Renderer.BOOLEAN);
            this.setEditor(Editor.BOOLEAN);
        }
        else if (FieldUtils.isDate(type))
        {
            this.setRenderer(Renderer.DATE_AND_DAY);
            this.setEditor(Editor.DATE);
        }
        else if (FieldUtils.isTimeStamp(type))
        {
            this.setRenderer(Renderer.FRENCH_DATE);
            this.setEditor("");
        }
        else if (FieldUtils.isDecimalNumber(type))
        {
            this.setRenderer(Renderer.FLOAT);
            this.setEditor(Editor.FLOAT);
        }
        else if (FieldUtils.isIntNumber(type))
        {
            this.setRenderer("");
            this.setEditor(Editor.INT);
        }
        else if (FieldUtils.isApproval(type))
        {
            this.setRenderer(Renderer.APPROVAL);
            this.setEditor(Editor.APPROVAL);
        }
        else if (FieldUtils.isPerson(type))
        {
            this.setRenderer(PersonModelData.SIMPLE_CLASS_NAME);
            this.setEditor(PersonModelData.SIMPLE_CLASS_NAME);
        }
        else if (TaskTypeModelData.SIMPLE_CLASS_NAME.equals(type))
        {
            this.setRenderer(Renderer.TASK_TYPE);
            this.setEditor("");
        }
        else if ("fr.hd3d.common.client.enums.ETaskStatus".equals(type))
        {
            this.setRenderer(Renderer.TASK_STATUS);
            this.setEditor(Editor.TASK_STATUS);
        }
        else if ("fr.hd3d.common.client.enums.EProjectStatus".equals(type))
        {
            this.setRenderer("");
            this.setEditor(Editor.PROJECT_STATUS);
        }
        else if (ProjectTypeModelData.CLASS_NAME.equals(type))
        {
            this.setRenderer(Renderer.PROJECT_TYPE);
            this.setEditor(Editor.PROJECT_TYPE);
        }
        else if ("resourceGroups".equals(this.getQuery()))
        {
            this.setRenderer(Renderer.RESOURCE_GROUP);
            this.setEditor("");
        }
        else if ("projects".equals(this.getQuery()))
        {
            this.setRenderer(Renderer.PROJECT);
            this.setEditor("");
        }
        else if ("fr.hd3d.services.resources.collectionquery.TaskBoundEntityCollectionQuery".equals(this.getQuery()))
        {
            this.setRenderer(Renderer.WORK_OBJECT);
            this.setEditor("");
        }
        else if (FieldUtils.isEntity(type))
        {
            this.setRenderer(type);
            this.setEditor("");
        }
    }

}
