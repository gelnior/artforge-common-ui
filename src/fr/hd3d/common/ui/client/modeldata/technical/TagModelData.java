package fr.hd3d.common.ui.client.modeldata.technical;

import org.restlet.client.data.Method;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.callback.TagRefreshCallback;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.ParameterBuilder;


public class TagModelData extends RecordModelData
{
    private static final long serialVersionUID = -5390146874904053321L;
    private static final String TAG_NAME = "name";
    private static final String TAG_POPULARITY = "popularity";
    private static final String TAG_PARENTS = "parents";
    public static final String SIMPLE_CLASS_NAME = "Tag";
    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LTag";

    public TagModelData()
    {
        super();
        setClassName(CLASS_NAME);
        setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public TagModelData(Long id, String name)
    {
        this();

        this.setId(id);
        this.setName(name);
    }

    public TagModelData(String name)
    {
        this();
        this.setName(name);
    }

    public void setLabel(String value)
    {
        setName(value);
    }

    public String getLabel()
    {
        return getName();
    }

    @Override
    public void setName(String name)
    {
        set(TAG_NAME, name);
    }

    public void setPopularity(Long pop)
    {
        set(TAG_POPULARITY, pop);
    }

    public void setParents(Long parents)
    {
        set(TAG_PARENTS, parents);
    }

    /*
     * public void setParents(String parents) { set(TAG_PARENTS, parents); }
     */

    @Override
    public String getName()
    {
        return (String) get(TAG_NAME);
    }

    public Long getPopularity()
    {
        return (Long) get(TAG_POPULARITY);
    }

    /*
     * public Long getParents() { return (Long) get(TAG_PARENTS); }
     */

    public String getParents()
    {
        return (String) get(TAG_PARENTS);
    }

    public String getParentsString()
    {
        return (String) get(TAG_PARENTS + "_string");
    }

    public void refreshByName()
    {
        Constraint constraint = new Constraint(EConstraintOperator.like, NAME_FIELD, this.getName(), null);
        String path = this.getPath() + ParameterBuilder.parameterToString(constraint);

        IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
        requestHandler.handleRequest(Method.GET, path, null, new TagRefreshCallback(this));
    }

    public static ModelType getModelType()
    {
        ModelType modelType = new ModelType();
        DataField id = new DataField(ID_FIELD);

        id.setType(Long.class);

        modelType.setRoot(ROOT);
        modelType.setTotalName(NB_RECORDS);
        modelType.addField(id);
        modelType.addField(TAG_NAME);

        DataField pop = new DataField(TAG_POPULARITY);
        pop.setType(Long.class);
        modelType.addField(pop);

        /*
         * DataField parents = new DataField(TAG_PARENTS); parents.type = Long.class; modelType.addField(parents);
         */

        modelType.addField(TAG_PARENTS);

        return modelType;
    }

}
