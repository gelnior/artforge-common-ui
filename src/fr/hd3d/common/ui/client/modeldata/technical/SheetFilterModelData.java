package fr.hd3d.common.ui.client.modeldata.technical;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.util.field.LongDataField;
import fr.hd3d.common.ui.client.util.field.StringDataField;


/**
 * SheetFilterModelData describes sheet filters created and stored by user.
 * 
 * @author HD3D
 */
public class SheetFilterModelData extends RecordModelData
{
    /** Automatically generated serial version number. */
    private static final long serialVersionUID = -7002413712913012251L;

    public static final String SHEET_ID_FIELD = "sheetId";
    public static final String FILTER_FIELD = "filter";

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LSheetFilter";
    public static final String SIMPLE_CLASS_NAME = "SheetFilter";

    /**
     * Constructor : configure class name and simple class name.
     */
    public SheetFilterModelData()
    {
        super();
        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    /**
     * Constructor : configure class name and simple class name and set filter data with parameters.
     * 
     * @param name
     *            Filter name.
     * @param id
     *            ID of sheet linked to filter.
     * @param filter
     *            Text describing filter.
     */
    public SheetFilterModelData(String name, Long id, String filter)
    {
        this();
        this.setName(name);
        this.setSheetId(id);
        this.setFilter(filter);
    }

    public SheetFilterModelData(String name)
    {
        this();
        this.setName(name);
    }

    public Long getSheetId()
    {
        return this.get(SHEET_ID_FIELD);
    }

    public void setSheetId(Long sheetId)
    {
        this.set(SHEET_ID_FIELD, sheetId);
    }

    public String getFilter()
    {
        return this.get(FILTER_FIELD);
    }

    public void setFilter(String filter)
    {
        this.set(FILTER_FIELD, filter);
    }

    /**
     * @return Model type used by reader to know the sheet filter model data structure.
     */
    public static ModelType getModelType()
    {
        ModelType type = RecordModelData.getModelType();
        type.addField(new LongDataField(SHEET_ID_FIELD));
        type.addField(new StringDataField(FILTER_FIELD));
        return type;
    }
}
