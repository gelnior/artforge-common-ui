package fr.hd3d.common.ui.client.modeldata.technical;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;


public class ProjectSecurityTemplateModelData extends RecordModelData
{

    private static final long serialVersionUID = 1998845890123632453L;

    public static final String ROLE_IDS_FIELD = "roleIds";
    public static final String ROLE_NAMES_FIELD = "roleNames";

    public static final String COMMENT = "comment";

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LProjectSecurityTemplate";
    public static final String SIMPLE_CLASS_NAME = "ProjectSecurityTemplate";

    public ProjectSecurityTemplateModelData()
    {
        this.setRoleIds(new ArrayList<Long>());
        this.setRoleNames(new ArrayList<String>());

        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public List<Long> getRoleIds()
    {
        return get(ROLE_IDS_FIELD);
    }

    public void setRoleIds(List<Long> roleIDs)
    {
        this.set(ROLE_IDS_FIELD, roleIDs);
    }

    public List<String> getRoleNames()
    {
        return get(ROLE_NAMES_FIELD);
    }

    public void setRoleNames(List<String> roleNames)
    {
        this.set(ROLE_NAMES_FIELD, roleNames);
    }

    public String getComment()
    {
        return (String) get(COMMENT);
    }

    public void setComment(String comment)
    {
        set(COMMENT, comment);
    }

    /**
     * @return the convenient model type for security template data structure
     */
    public static ModelType getModelType()
    {
        ModelType type = RecordModelData.getModelType();

        DataField roleIdsDataField = new DataField(ROLE_IDS_FIELD);
        roleIdsDataField.setType(List.class);
        roleIdsDataField.setFormat("Role");
        DataField roleNamesDataField = new DataField(ROLE_NAMES_FIELD);
        roleNamesDataField.setType(List.class);

        type.addField(CLASS_NAME_FIELD);
        type.addField(NAME_FIELD);
        type.addField(COMMENT);
        type.addField(roleIdsDataField);
        type.addField(roleNamesDataField);

        return type;
    }

}
