package fr.hd3d.common.ui.client.modeldata.technical;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.util.field.IntegerDataField;
import fr.hd3d.common.ui.client.util.field.ListDataField;
import fr.hd3d.common.ui.client.widget.explorer.controller.ItemGroupController;
import fr.hd3d.common.ui.client.widget.explorer.model.modeldata.IColumn;
import fr.hd3d.common.ui.client.widget.explorer.model.modeldata.IColumnGroup;


public class ItemGroupModelData extends RecordModelData implements IColumnGroup
{
    private static final long serialVersionUID = -6784262929550493860L;

    public static final String SHEET_ID_FIELD = "sheetId";
    public static final String SHEET_FIELD = "sheet";
    public static final String ITEMS_FIELD = "itemsObject";
    public static final String ITEM_IDS_FIELD = "items";
    public static final String INDEX_FIELD = "index";

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LItemGroup";
    public static final String SIMPLE_CLASS_NAME = "ItemGroup";

    public final ItemGroupController controller;
    public Boolean itemsLoaded = false;

    public ItemGroupModelData()
    {
        super();
        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);

        controller = new ItemGroupController(this);
    }

    public Long getSheetId()
    {
        return get(SHEET_ID_FIELD);
    }

    public void setSheet(SheetModelData sheet)
    {
        set(SHEET_FIELD, sheet);
        if (sheet != null)
            set(SHEET_ID_FIELD, sheet.getId());
        else
            set(SHEET_ID_FIELD, null);
    }

    public SheetModelData getSheet()
    {
        return get(SHEET_FIELD);
    }

    public void setIndex(int index)
    {
        set(INDEX_FIELD, index);
    }

    public void setColumns(List<IColumn> items)
    {
        this.itemsLoaded = true;
        set(ITEMS_FIELD, items);
    }

    public void setItems(List<ItemModelData> items)
    {
        this.itemsLoaded = true;
        set(ITEMS_FIELD, items);
    }

    public void setItemIds(List<Long> ids)
    {
        this.set(ITEM_IDS_FIELD, ids);
    }

    public List<IColumn> getColumns()
    {
        List<IColumn> columns = get(ITEMS_FIELD);
        if (columns == null)
        {
            return new ArrayList<IColumn>();
        }
        else
        {
            return columns;
        }
    }

    public Boolean areColumnsLoaded()
    {
        return this.itemsLoaded;
    }

    public static final ModelType getModelType()
    {
        ModelType type = Hd3dModelData.getModelType();

        DataField sheetId = new DataField(SHEET_ID_FIELD, "sheet");
        sheetId.setType(Long.class);
        type.addField(sheetId);

        type.addField(NAME_FIELD);
        type.addField(VERSION_FIELD);
        type.addField(new ListDataField(ITEM_IDS_FIELD));
        type.addField(new IntegerDataField(INDEX_FIELD));

        return type;
    }

    private List<Long> getColumnsIds()
    {
        List<Long> ids = new ArrayList<Long>();
        List<IColumn> items = this.getColumns();
        for (Iterator<IColumn> itemIterator = items.iterator(); itemIterator.hasNext();)
        {
            IColumn column = itemIterator.next();
            ids.add(column.getId());
        }
        return ids;
    }

    public int getColumnsCount()
    {
        if (this.getColumns() != null)
        {
            return this.getColumns().size();
        }
        return 0;
    }

    @Override
    public String getPath()
    {
        if (getDefaultPath() == null)
        {
            if (this.getId() == null)
            {
                return ServicesPath.SHEETS + this.getSheetId() + "/" + ServicesPath.ITEM_GROUPS;
            }
            else
            {
                return ServicesPath.SHEETS + this.getSheetId() + "/" + ServicesPath.ITEM_GROUPS + "/" + this.getId();
            }
        }
        else
        {
            return this.getDefaultPath();
        }
    }

    @Deprecated
    public String getJsonRepresentation()
    {
        StringBuilder id = new StringBuilder();
        if (this.getId() != null && this.getId() != 0)
        {
            id.append(" \"id\":" + this.getId() + ", ");
        }

        StringBuilder ids = new StringBuilder();
        List<Long> itemIds = getColumnsIds();
        for (Iterator<Long> iterator = itemIds.iterator(); iterator.hasNext();)
        {
            Long itemId = iterator.next();
            if (itemId != null)
            {
                ids.append(itemId + ",");
            }
        }
        if (ids.length() > 0)
        {
            ids.deleteCharAt(ids.length() - 1);
        }

        String json = "{\"class\": \"fr.hd3d.model.lightweight.impl.LItemGroup\", " + id + "  \"name\":\""
                + this.getName() + "\",   \"sheet\":\"" + this.getSheetId() + "\", \"hook\":\"" + this.getName()
                + "\", \"items\":[" + ids + "]}";
        return json;
    }

    @Deprecated
    public String getPostUrl()
    {
        return ServicesPath.SHEETS + this.getSheetId() + "/" + ServicesPath.ITEM_GROUPS;
    }

    @Deprecated
    public String getPutUrl()
    {
        return getPostUrl() + this.getId() + "/";
    }

}
