package fr.hd3d.common.ui.client.modeldata.annotation;

import java.util.Date;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.Node;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;

public class GraphicalAnnotationModelData extends Hd3dModelData
{

    /**
     * 
     */
    private static final long serialVersionUID = -7926942012034613912L;

    private static final String ANNOTATION_SVG_FIELD = "annotationSvg";
    private static final String MARK_IN_FIELD = "markIn";
    private static final String MARK_OUT_FIELD = "markOut";
    private static final String COMMENT_FIELD = "comment";
    private static final String AUTHOR_FIELD = "author";
    private static final String PROXY_FIELD = "proxy";
    private static final String APPROVAL_NOTE_FIELD = "approvalNote";
    private static final String DATE_FIELD = "date";

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LGraphicalAnnotation";
    public static final String SIMPLE_CLASS_NAME = "GraphicalAnnotation";

    public GraphicalAnnotationModelData()
    {
        super();

        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public Long getApprovalNote() {
        return this.get(APPROVAL_NOTE_FIELD);
    }
    
    public void setApprovalNote(Long approvalNoteId) {
        this.set(APPROVAL_NOTE_FIELD, approvalNoteId);
    }
    
    public String getAnnotationSvg()
    {
        return get(ANNOTATION_SVG_FIELD);
    }

    public void setAnnotationSvg(String annotationSvg)
    {
        set(ANNOTATION_SVG_FIELD, annotationSvg);
    }

    public Long getMarkIn()
    {
        Long markIn = get(MARK_IN_FIELD);
        if (markIn == null)
        {
            return 0L;
        }
        return markIn;
    }

    public void setMarkIn(Long markIn)
    {
        set(MARK_IN_FIELD, markIn);
    }

    public Long getMarkOut()
    {
        Long markOut = get(MARK_OUT_FIELD);
        if (markOut == null)
        {
            return 0L;
        }
        return markOut;
    }

    public void setMarkOut(Long markOut)
    {
        set(MARK_OUT_FIELD, markOut);
    }

    public String getComment()
    {
        return get(COMMENT_FIELD);
    }

    public void setComment(String comment)
    {
        set(COMMENT_FIELD, comment);
    }

    public Long getAuthor()
    {
        return get(AUTHOR_FIELD);
    }

    public void setAuthor(Long author)
    {
        set(AUTHOR_FIELD, author);
    }

    public Long getProxy()
    {
        return get(PROXY_FIELD);
    }

    public void setProxy(Long proxy)
    {
        set(PROXY_FIELD, proxy);
    }

    public Date getDate()
    {
        return get(DATE_FIELD);
    }

    public void setDate(Date date)
    {
        set(DATE_FIELD, date);
    }

    public static native JsArray<Node> getAttributes(Element elem) /*-{
        return elem.attributes;
    }-*/;

    public static ModelType getModelType()
    {
        ModelType modelType = Hd3dModelData.getModelType();

        DataField annotationSvgField = new DataField(ANNOTATION_SVG_FIELD);
        DataField markInField = new DataField(MARK_IN_FIELD);
        DataField markOutField = new DataField(MARK_OUT_FIELD);
        DataField commentField = new DataField(COMMENT_FIELD);
        DataField authorField = new DataField(AUTHOR_FIELD);
        DataField fileRevisionField = new DataField(PROXY_FIELD);
        DataField dateField = new DataField(DATE_FIELD);

        annotationSvgField.setType(String.class);
        markInField.setType(Long.class);
        markOutField.setType(Long.class);
        commentField.setType(String.class);
        authorField.setType(Long.class);
        fileRevisionField.setType(Long.class);
        dateField.setType(Date.class);

        modelType.addField(annotationSvgField);
        modelType.addField(markInField);
        modelType.addField(markOutField);
        modelType.addField(commentField);
        modelType.addField(authorField);
        modelType.addField(fileRevisionField);
        modelType.addField(dateField);
        
        return modelType;
    }

}
