package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.production.CompositionModelData;


public class CompositionReader extends Hd3dListJsonReader<CompositionModelData>
{

    public CompositionReader()
    {
        super(CompositionModelData.getModelType());
    }

    @Override
    public CompositionModelData newModelInstance()
    {
        return new CompositionModelData();
    }

}
