package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.task.EntityTaskLinkModelData;


/**
 * Reader that translates EntityTaskLinkModelData object from json format to model data object.
 * 
 * @author HD3D
 */
public class EntityTaskLinkReader extends Hd3dListJsonReader<EntityTaskLinkModelData>
{

    /**
     * Default constructor.
     */
    public EntityTaskLinkReader()
    {
        super(EntityTaskLinkModelData.getModelType());
    }

    /**
     * @return A new instance of EntityTaskLinkModelData.
     */
    @Override
    public EntityTaskLinkModelData newModelInstance()
    {
        return new EntityTaskLinkModelData();
    }

}
