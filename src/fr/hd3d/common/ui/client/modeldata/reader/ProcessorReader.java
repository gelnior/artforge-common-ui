package fr.hd3d.common.ui.client.modeldata.reader;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.inventory.ProcessorModelData;


public class ProcessorReader extends Hd3dListJsonReader<ProcessorModelData>
{
    public ProcessorReader()
    {
        super(ProcessorModelData.getModelType());
    }

    public ProcessorReader(ModelType modelType)
    {
        super(modelType);
    }

    @Override
    public ProcessorModelData newModelInstance()
    {
        return new ProcessorModelData();
    }

}
