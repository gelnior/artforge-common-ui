package fr.hd3d.common.ui.client.modeldata.reader;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.extjs.gxt.ui.client.data.BaseListLoadResult;
import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.DataReader;
import com.extjs.gxt.ui.client.data.ListLoadResult;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.data.ModelType;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONNumber;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.json.client.JSONValue;

import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.modeldata.production.StepModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.modeldata.technical.DynValueModelData;
import fr.hd3d.common.ui.client.service.Hd3dClassReadersFactory;
import fr.hd3d.common.ui.client.widget.explorer.model.DataFieldArray;


/**
 * GXT JSON reader adapted for HD3D purposes.
 * 
 * @param <C>
 */
public abstract class Hd3dJsonReader<C extends ModelData, D> implements IReader<C>, DataReader<D>
{
    protected ModelType modelType;
    protected int totalCount = 0;
    private HashMap<String, JSONObject> objects;

    /**
     * Default constructor.
     */
    public Hd3dJsonReader()
    {
        this.modelType = new ModelType();
    }

    public Hd3dJsonReader(ModelType modelType)
    {
        this.modelType = modelType;
    }

    public void setModelType(ModelType modelType)
    {
        this.modelType = modelType;
    }

    public int getTotalCount()
    {
        return totalCount;
    }

    public void setTotalCount(int totalCount)
    {
        this.totalCount = totalCount;
    }

    public ListLoadResult<C> read(String data)
    {
        return new BaseListLoadResult<C>(this.readData(null, data));
    }

    public D read(Object loadConfig, Object data)
    {
        return createReturnData(loadConfig, this.readData(null, data), totalCount);
    }

    public ArrayList<C> readList(List<JSONObject> root, ModelType modelType)
    {
        int size = root.size();
        ArrayList<C> models = new ArrayList<C>();
        for (int i = 0; i < size; i++)
        {
            JSONObject obj = root.get(i);
            C model = this.readObject(obj, modelType);
            models.add(model);
        }
        return models;
    }

    public List<C> readData(Object loadConfig, Object data)
    {
        JSONObject jsonRoot = null;

        objects = new HashMap<String, JSONObject>();
        if (data instanceof JavaScriptObject)
        {
            jsonRoot = new JSONObject((JavaScriptObject) data);
        }
        else if (data instanceof JSONObject)
        {
            jsonRoot = (JSONObject) data;
        }
        else if (data != null)
        {
            jsonRoot = (JSONObject) JSONParser.parse((String) data);
        }

        if (jsonRoot != null)
        {
            JSONArray root = (JSONArray) jsonRoot.get(modelType.getRoot());

            ArrayList<C> models = readRecords(root, modelType);

            totalCount = models.size();
            if (modelType.getTotalName() != null)
            {
                totalCount = getTotalCount(jsonRoot);
            }

            return models;
        }
        else
        {
            Logger.log("JSON string unreadable : nothing is returned. The format is probably wrong.", new Exception());

            totalCount = 0;
            return new ArrayList<C>();
        }
    }

    public ArrayList<C> readRecords(JSONArray root, ModelType modelType)
    {
        int size = 0;
        if (root != null)
            size = root.size();
        ArrayList<C> models = new ArrayList<C>();
        for (int i = 0; i < size; i++)
        {
            JSONObject obj = (JSONObject) root.get(i);
            C model = this.readObject(obj, modelType);
            // C model = this.newModelInstance();
            models.add(model);
            List<JSONObject> boundTaskList = model.get(Hd3dModelData.BOUND_TASKS_FIELD);
            if (boundTaskList != null && boundTaskList.size() > 0)
            {
                List<TaskModelData> tmpList = new ArrayList<TaskModelData>(boundTaskList.size());
                for (JSONObject taskObject : boundTaskList)
                {
                    TaskReader taskReader = new TaskReader();
                    TaskModelData task = taskReader.readObject(taskObject, TaskModelData.getModelType());
                    tmpList.add(task);
                }
                model.set(Hd3dModelData.BOUND_TASKS_FIELD, tmpList);
            }
            getDynValues(model);

        }

        return models;
    }

    private void getDynValues(C model)
    {
        List<JSONObject> dynValuesJsonObjectList = model.get(Hd3dModelData.DYN_VALUES_FIELD);
        if (dynValuesJsonObjectList != null && dynValuesJsonObjectList.size() > 0)
        {
            Map<Long, DynValueModelData> dynValuesModelData = new HashMap<Long, DynValueModelData>(
                    dynValuesJsonObjectList.size());
            for (JSONObject dynValueJsonObject : dynValuesJsonObjectList)
            {
                DynValueReader dynValueReader = new DynValueReader();
                DynValueModelData dynValue = dynValueReader.readObject(dynValueJsonObject,
                        DynValueModelData.getModelType());
                dynValuesModelData.put(dynValue.getDynClassId(), dynValue);
            }
            model.set(Hd3dModelData.DYN_VALUES_FIELD, dynValuesModelData);
        }
    }

    public C readObject(JSONObject obj, ModelType modelType)
    {
        C model = newModelInstance();

        for (int j = 0; j < modelType.getFieldCount(); j++)
        {
            DataField field = modelType.getField(j);
            String name = field.getName();
            Class<?> type = field.getType();
            String format = field.getFormat();

            String map = field.getMap() != null ? field.getMap() : name;
            JSONValue value = obj.get(map);

            if (value == null)
            {
                continue;
            }
            if (value.isArray() != null)
            {
                this.readArray(model, field, name, type, format, value.isArray());
            }
            else if (value.isBoolean() != null)
            {
                this.readBoolean(model, name, type, format, value.isBoolean().booleanValue());
            }
            else if (value.isNumber() != null)
            {
                this.readNumber(model, name, type, format, value.isNumber().doubleValue());
            }
            else if (value.isObject() != null)
            {
                this.readObject(model, name, type, format, value.isObject());
            }
            else if (value.isString() != null)
            {
                this.readString(model, name, type, format, value.isString().stringValue());
            }
            else if (value.isNull() != null)
            {
                model.set(name, null);
            }
        }
        return model;
    }

    private void readArray(C model, DataField field, String name, Class<?> type, String format, JSONArray array)
    {
        if (field instanceof DataFieldArray)
        {
            DataFieldArray arrayField = (DataFieldArray) field;
            ModelType subModelType = arrayField.getModelType();
            if (subModelType != null)
            {
                ArrayList<C> subModels = new ArrayList<C>();
                subModels = readRecords(array, subModelType);
                model.set(name, subModels);
            }
        }
        else
        {
            List<Object> tmpList = new ArrayList<Object>();
            for (int k = 0; k < array.size(); k++)
            {
            	if(array.get(k) instanceof JSONValue) {
	                JSONValue arrayValue = array.get(k);
	                if (arrayValue.isNumber() != null)
	                {
	                    JSONNumber number = arrayValue.isNumber();
	                    tmpList.add(new Long(number.toString()));
	                }
	                else if (arrayValue.isString() != null)
	                {
	                    JSONString string = arrayValue.isString();
	                    String s = string.stringValue();
	                    if (s.startsWith("~unique-id~") == true)
	                    {
	                        s = s.replaceAll("~unique-id~", "");
	                        JSONObject object = objects.get(s);
	                        if (object != null)
	                        {
	                            // model.set(field.name, object);
	                            tmpList.add(object);
	                        }
	                    }
	                    else
	                    {
	                        tmpList.add(string.stringValue());
	                    }
	                }
	                // Case of approval data for production application.
	                else if (arrayValue.isBoolean() != null && array.size() == 2)
	                {
	                    Boolean bool = arrayValue.isBoolean().booleanValue();
	                    if (bool)
	                    {
	                        arrayValue = array.get(1);
	                        if (arrayValue.isArray() != null && arrayValue.isArray().get(0) != null)
	                        {
	                            JSONArray internalArray = arrayValue.isArray();
	                            ApprovalNoteReader reader = new ApprovalNoteReader();
	                            List<ApprovalNoteModelData> approvalList = new ArrayList<ApprovalNoteModelData>();
	                            for (int l = 0; l < internalArray.size(); l++)
	                            {
	                                JSONValue val = internalArray.get(l);
	                                JSONObject jsonValue = val.isObject();
	                                approvalList.add(reader.readObject(jsonValue, reader.modelType));
	                            }
	                            tmpList.addAll(approvalList);
	                        }
	                    }
	                    else
	                    {
	                        model.set(name, bool);
	                        tmpList = null;
	                    }
	                    continue;
	                }
	                // Case of approval data for task manager application.
	                else if (arrayValue.isObject() != null)
	                {
	                    JSONObject jsonValue = arrayValue.isObject();
	                    JSONValue uniqueId = jsonValue.get("~unique-id~");
	                    if (uniqueId.isString() != null)
	                    {
	                        String id = uniqueId.isString().stringValue();
	                        objects.put(id, jsonValue);
	                    }
	                    Object localValue = jsonValue;
	                    JSONString classValue = (JSONString) jsonValue.get("class");
	                    if (Hd3dClassReadersFactory.contains(classValue.stringValue()))
	                    {
	                        Hd3dListJsonReader<?> reader = Hd3dClassReadersFactory.getReader(classValue.stringValue());
	                        localValue = reader.readObject(jsonValue, reader.modelType);
	                    }
	
	                    tmpList.add(localValue);
	                }
            	}
            }
            if (tmpList != null)
            {
                model.set(name, tmpList);
            }
        }
    }

    /**
     * @param field
     *            The field that should be read.
     * @param value
     *            The corresponding value to analyze.
     * @return Read number and return it to with correct type.
     */
    private void readNumber(C model, String name, Class<?> type, String format, Double d)
    {
        if (type != null)
        {
            if (type.equals(Integer.class))
            {
                model.set(name, d.intValue());
            }
            else if (type.equals(Long.class))
            {
                model.set(name, d.longValue());
            }
            else if (type.equals(Byte.class))
            {
                model.set(name, d.byteValue());
            }
            else if (type.equals(Float.class))
            {
                model.set(name, d.floatValue());
            }
            else
            {
                model.set(name, d);
            }
        }
        else
        {
            model.set(name, d);
        }

    }

    private void readBoolean(C model, String name, Class<?> type, String format, boolean b)
    {
        model.set(name, b);
    }

    private void readObject(C model, String name, Class<?> type, String format, JSONObject object)
    {
        JSONValue uniqueId = object.get("~unique-id~");
        if (uniqueId.isString() != null)
        {
            String id = uniqueId.isString().stringValue();

            JSONString classValue = (JSONString) object.get("class");
            if (classValue.stringValue().equals(StepModelData.CLASS_NAME))
            {
                StepReader reader = new StepReader();
                StepModelData step = reader.readObject(object, reader.modelType);
                model.set(name, step);
            }
            else
            {
                objects.put(id, object);
                model.set(name, object);
            }
        }

    }

    private void readString(C model, String name, Class<?> type, String format, String s)
    {
        if (s.startsWith("~unique-id~") == true)
        {
            s = s.replaceAll("~unique-id~", "");
            JSONObject object = objects.get(s);
            if (object != null)
            {
                model.set(name, object);
            }
        }
        else if (type != null)
        {
            if (type.equals(Date.class))
            {
                if (format.equals("timestamp"))
                {
                    Date d = new Date(Long.parseLong(s) * 1000);
                    model.set(name, d);
                }
                else
                {
                    DateTimeFormat dateFormat = DateTimeFormat.getFormat(format);
                    Date d = dateFormat.parse(s);
                    model.set(name, d);
                }
            }
            else if (type.equals(String.class) || type.equals(Enum.class))
            {
                model.set(name, s);
            }
        }
        else
        {
            model.set(name, s);
        }
    }

    /**
     * Responsible for the object being returned by the reader.
     * 
     * @param loadConfig
     *            the load config
     * @param records
     *            the list of models
     * @param totalCount
     *            the total count
     * @return the data to be returned by the reader
     */
    @SuppressWarnings("unchecked")
    protected D createReturnData(Object loadConfig, List<C> records, int totalCount)
    {
        return (D) records;
    }

    protected int getTotalCount(JSONObject root)
    {
        JSONValue v = root.get(modelType.getTotalName());
        if (v != null)
        {
            if (v.isNumber() != null)
            {
                return (int) v.isNumber().doubleValue();
            }
            else if (v.isString() != null)
            {
                return Integer.parseInt(v.isString().stringValue());
            }
        }
        return -1;
    }

    /**
     * Returns the new model instances. Subclasses may override to provide an model data subclass.
     * 
     * @return the new model data instance
     */
    public abstract C newModelInstance();
}
