package fr.hd3d.common.ui.client.modeldata.reader;

import com.extjs.gxt.ui.client.data.DataReader;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.data.PagingLoadResult;


public interface IPagingReader<C extends ModelData> extends DataReader<PagingLoadResult<C>>
{

    public PagingLoadResult<C> read(Object loadConfig, Object data);

}
