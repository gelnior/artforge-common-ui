package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.inventory.RoomModelData;


/**
 * GXT Reader for device data.
 * 
 * @author HD3D
 */
public class RoomReader extends Hd3dListJsonReader<RoomModelData>
{

    public RoomReader()
    {
        super(RoomModelData.getModelType());
    }

    @Override
    public RoomModelData newModelInstance()
    {
        return new RoomModelData();
    }
}
