package fr.hd3d.common.ui.client.modeldata.reader;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;


public class ConstituentReader extends Hd3dListJsonReader<ConstituentModelData>
{
    public ConstituentReader()
    {
        super(ConstituentModelData.getModelType());
    }

    public ConstituentReader(ModelType modelType)
    {
        super(modelType);
    }

    @Override
    public ConstituentModelData newModelInstance()
    {
        return new ConstituentModelData();
    }

}
