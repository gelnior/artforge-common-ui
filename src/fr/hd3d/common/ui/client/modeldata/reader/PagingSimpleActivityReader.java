package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.task.SimpleActivityModelData;



/**
 * Paging reader for Task data.
 * 
 * @author HD3D
 */
public class PagingSimpleActivityReader extends Hd3dPagingJsonReader<SimpleActivityModelData>
{
    /**
     * Default constructor.
     */
    public PagingSimpleActivityReader()
    {
        super(SimpleActivityModelData.getModelType());
    }

    @Override
    public SimpleActivityModelData newModelInstance()
    {
        return new SimpleActivityModelData();
    }
}
