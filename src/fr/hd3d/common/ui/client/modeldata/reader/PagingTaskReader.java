package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;


/**
 * Paging reader for Task data.
 * 
 * @author HD3D
 */
public class PagingTaskReader extends Hd3dPagingJsonReader<TaskModelData>
{
    /**
     * Default constructor.
     */
    public PagingTaskReader()
    {
        super(TaskModelData.getModelType());
    }

    @Override
    public TaskModelData newModelInstance()
    {
        return new TaskModelData();
    }
}
