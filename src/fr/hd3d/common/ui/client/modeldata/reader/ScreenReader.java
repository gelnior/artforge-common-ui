package fr.hd3d.common.ui.client.modeldata.reader;

import com.extjs.gxt.ui.client.data.ListLoadResult;

import fr.hd3d.common.ui.client.modeldata.inventory.ScreenModelData;


/**
 * GXT Reader for screen data.
 * 
 * @author HD3D
 */
public class ScreenReader extends Hd3dJsonReader<ScreenModelData, ListLoadResult<ScreenModelData>>
{

    public ScreenReader()
    {
        super(ScreenModelData.getModelType());
    }

    @Override
    public ScreenModelData newModelInstance()
    {
        return new ScreenModelData();
    }
}
