package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.inventory.ScreenModelModelData;


/**
 * GXT Reader for device data.
 * 
 * @author HD3D
 */
public class ScreenModelReader extends Hd3dListJsonReader<ScreenModelModelData>
{

    public ScreenModelReader()
    {
        super(ScreenModelModelData.getModelType());
    }

    @Override
    public ScreenModelModelData newModelInstance()
    {
        return new ScreenModelModelData();
    }
}
