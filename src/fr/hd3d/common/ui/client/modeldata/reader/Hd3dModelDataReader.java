package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;


public class Hd3dModelDataReader extends Hd3dListJsonReader<Hd3dModelData>
{

    public Hd3dModelDataReader()
    {
        super(Hd3dModelData.getModelType());
    }

    @Override
    public Hd3dModelData newModelInstance()
    {
        return new Hd3dModelData();
    }

}
