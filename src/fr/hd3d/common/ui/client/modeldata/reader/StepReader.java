package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.production.StepModelData;


public class StepReader extends Hd3dListJsonReader<StepModelData>
{
    public StepReader()
    {
        super(StepModelData.getModelType());
    }

    @Override
    public StepModelData newModelInstance()
    {
        return new StepModelData();
    }
}
