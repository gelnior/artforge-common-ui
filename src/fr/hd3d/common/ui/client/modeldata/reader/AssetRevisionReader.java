package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.asset.AssetRevisionModelData;


public class AssetRevisionReader extends Hd3dListJsonReader<AssetRevisionModelData>
{

    public AssetRevisionReader()
    {
        super(AssetRevisionModelData.getModelType());
    }

    @Override
    public AssetRevisionModelData newModelInstance()
    {
        return new AssetRevisionModelData();
    }

}
