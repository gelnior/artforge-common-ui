package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;


/**
 * Reader that translates Person object from json format to model data object.
 * 
 * @author HD3D
 */
public class PersonReader extends Hd3dListJsonReader<PersonModelData>
{
    /**
     * Default constructor.
     */
    public PersonReader()
    {
        super(PersonModelData.getModelType());
    }

    /**
     * @return A new instance of PersonModelData.
     */
    @Override
    public PersonModelData newModelInstance()
    {
        return new PersonModelData();
    }
}
