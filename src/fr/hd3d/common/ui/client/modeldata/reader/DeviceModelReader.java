package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.inventory.DeviceModelModelData;


/**
 * GXT Reader for device data.
 * 
 * @author HD3D
 */
public class DeviceModelReader extends Hd3dListJsonReader<DeviceModelModelData>
{

    public DeviceModelReader()
    {
        super(DeviceModelModelData.getModelType());
    }

    @Override
    public DeviceModelModelData newModelInstance()
    {
        return new DeviceModelModelData();
    }
}
