package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteTypeModelData;


/**
 * GXT Reader for approvalNoteType data.
 * 
 * @author HD3D
 */
public class ApprovalNoteTypeReader extends Hd3dListJsonReader<ApprovalNoteTypeModelData>
{

    public ApprovalNoteTypeReader()
    {
        super(ApprovalNoteTypeModelData.getModelType());
    }

    @Override
    public ApprovalNoteTypeModelData newModelInstance()
    {
        return new ApprovalNoteTypeModelData();
    }
}
