package fr.hd3d.common.ui.client.modeldata.reader.playlist;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.playlist.PlayListEditModelData;
import fr.hd3d.common.ui.client.modeldata.reader.Hd3dListJsonReader;


public class PlayListEditReader extends Hd3dListJsonReader<PlayListEditModelData>
{

    public PlayListEditReader()
    {
        super(PlayListEditModelData.getModelType());
    }

    public PlayListEditReader(ModelType modelType)
    {
        super(modelType);
    }

    @Override
    public PlayListEditModelData newModelInstance()
    {
        return new PlayListEditModelData();
    }

}
