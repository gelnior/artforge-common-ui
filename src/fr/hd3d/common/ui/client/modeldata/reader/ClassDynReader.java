package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.technical.ClassDynModelData;


public class ClassDynReader extends Hd3dListJsonReader<ClassDynModelData>
{

    public ClassDynReader()
    {
        super(ClassDynModelData.getModelType());
    }

    @Override
    public ClassDynModelData newModelInstance()
    {
        return new ClassDynModelData();
    }
}
