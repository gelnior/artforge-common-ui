package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.service.ServicesModelType;


/**
 * Give record reader instance needed to read JSON data from server.
 * 
 * @author HD3D
 */
public class Hd3dModelDataReaderSingleton
{

    /** The record reader instance to return. */
    private static IReader<Hd3dModelData> reader;

    /**
     * @return The record reader instance. If it is null, it returns a new real record reader.
     */
    public final static IReader<Hd3dModelData> getInstance()
    {
        if (reader == null)
        {
            reader = new Hd3dListJsonReader<Hd3dModelData>(Hd3dModelData.getModelType()) {
                @Override
                public Hd3dModelData newModelInstance()
                {
                    return new Hd3dModelData();
                }
            };
        }

        return reader;
    }

    /**
     * Sets the record instance. This method is useful for unit tests.
     * 
     * @param mock
     *            The request handler to set.
     */
    public final static void setInstanceAsMock(IReader<Hd3dModelData> mock)
    {
        reader = mock;
    }

    public final static void setModelType(String simpleClassName)
    {
        getInstance().setModelType(ServicesModelType.getModelType(simpleClassName));
    }
}
