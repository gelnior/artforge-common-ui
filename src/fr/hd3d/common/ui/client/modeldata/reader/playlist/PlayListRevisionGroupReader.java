package fr.hd3d.common.ui.client.modeldata.reader.playlist;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.playlist.PlayListRevisionGroupModelData;
import fr.hd3d.common.ui.client.modeldata.reader.Hd3dListJsonReader;


public class PlayListRevisionGroupReader extends Hd3dListJsonReader<PlayListRevisionGroupModelData>
{

    public PlayListRevisionGroupReader()
    {
        super(PlayListRevisionGroupModelData.getModelType());
    }

    public PlayListRevisionGroupReader(ModelType modelType)
    {
        super(modelType);
    }

    @Override
    public PlayListRevisionGroupModelData newModelInstance()
    {
        return new PlayListRevisionGroupModelData();
    }

}
