package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;


public class TaskTypeReader extends Hd3dListJsonReader<TaskTypeModelData>
{

    public TaskTypeReader()
    {
        super(TaskTypeModelData.getModelType());
    }

    @Override
    public TaskTypeModelData newModelInstance()
    {
        return new TaskTypeModelData();
    }

}
