package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;


/**
 * Give record reader instance needed to read JSON data from server.
 * 
 * @author HD3D
 */
public class RecordReaderSingleton
{
    /** The record reader instance to return. */
    private static IReader<RecordModelData> reader;

    /**
     * @return The record reader instance. If it is null, it returns a new real record reader.
     */
    public final static IReader<RecordModelData> getInstance()
    {
        if (reader == null)
        {
            reader = new RecordReader();
        }
        return reader;
    }

    /**
     * Sets the record instance. This method is useful for unit tests.
     * 
     * @param mock
     *            The request handler to set.
     */
    public final static void setInstanceAsMock(IReader<RecordModelData> mock)
    {
        reader = mock;
    }

}
