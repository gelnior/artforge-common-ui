package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.inventory.SoftwareModelData;


/**
 * GXT Reader for computer data.
 * 
 * @author HD3D
 */
public class SoftwareReader extends Hd3dListJsonReader<SoftwareModelData>
{

    public SoftwareReader()
    {
        super(SoftwareModelData.getModelType());
    }

    @Override
    public SoftwareModelData newModelInstance()
    {
        return new SoftwareModelData();
    }
}
