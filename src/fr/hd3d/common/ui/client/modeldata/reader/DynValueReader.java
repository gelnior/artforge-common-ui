package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.technical.DynValueModelData;


public class DynValueReader extends Hd3dListJsonReader<DynValueModelData>
{

    public DynValueReader()
    {
        super(DynValueModelData.getModelType());
    }

    @Override
    public DynValueModelData newModelInstance()
    {
        return new DynValueModelData();
    }
}
