package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;


public class ResourceGroupReader extends Hd3dListJsonReader<ResourceGroupModelData>
{

    public ResourceGroupReader()
    {
        super(ResourceGroupModelData.getModelType());
    }

    @Override
    public ResourceGroupModelData newModelInstance()
    {
        return new ResourceGroupModelData();
    }
}
