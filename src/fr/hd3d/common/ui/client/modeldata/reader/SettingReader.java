package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.technical.SettingModelData;


/**
 * GXT Reader for computer data.
 * 
 * @author HD3D
 */
public class SettingReader extends Hd3dListJsonReader<SettingModelData>
{

    public SettingReader()
    {
        super(SettingModelData.getModelType());
    }

    @Override
    public SettingModelData newModelInstance()
    {
        return new SettingModelData();
    }
}
