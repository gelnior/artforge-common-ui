package fr.hd3d.common.ui.client.modeldata.reader;

/**
 * This interface defines a mechanism for iterating over a range of characters. For a given range of text, a beginning
 * and ending index, as well as a current index are defined. These values can be queried by the methods in this
 * interface. Additionally, various methods allow the index to be set.
 * 
 * @author Aaron M. Renn (arenn@urbanophile.com)
 */
public interface CharacterIterator extends Cloneable
{
    /**
     * This is a special constant value that is returned when the beginning or end of the character range has been
     * reached.
     */
    char DONE = '\uFFFF';

    /**
     * This method returns the character at the current index position
     * 
     * @return The character at the current index position.
     */
    char current();

    /**
     * This method increments the current index and then returns the character at the new index value. If the index is
     * already at <code>getEndIndex() - 1</code>, it will not be incremented.
     * 
     * @return The character at the position of the incremented index value, or <code>DONE</code> if the index has
     *         reached getEndIndex() - 1
     */
    char next();

    /**
     * This method decrements the current index and then returns the character at the new index value. If the index
     * value is already at the beginning index, it will not be decremented.
     * 
     * @return The character at the position of the decremented index value, or {@link #DONE} if index was already equal
     *         to the beginning index value.
     */
    char previous();

    /**
     * This method sets the index value to the beginning of the range and returns the character there.
     * 
     * @return The character at the beginning of the range, or {@link #DONE} if the range is empty.
     */
    char first();

    /**
     * This method sets the index value to <code>getEndIndex() - 1</code> and returns the character there. If the range
     * is empty, then the index value will be set equal to the beginning index.
     * 
     * @return The character at the end of the range, or {@link #DONE} if the range is empty.
     */
    char last();

    /**
     * This method returns the current value of the index.
     * 
     * @return The current index value
     */
    int getIndex();

    /**
     * This method sets the value of the index to the specified value, then returns the character at that position.
     * 
     * @param index
     *            The new index value.
     * 
     * @return The character at the new index value or {@link #DONE} if the index value is equal to
     *         {@link #getEndIndex()}.
     */
    char setIndex(int index) throws IllegalArgumentException;

    /**
     * This method returns the character position of the first character in the range.
     * 
     * @return The index of the first character in the range.
     */
    int getBeginIndex();

    /**
     * This method returns the character position of the end of the text range. This will actually be the index of the
     * first character following the end of the range. In the event the text range is empty, this will be equal to the
     * first character in the range.
     * 
     * @return The index of the end of the range.
     */
    int getEndIndex();
}
