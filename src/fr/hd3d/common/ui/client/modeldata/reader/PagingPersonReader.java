package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;


/**
 * Paging reader for person data.
 * 
 * @author HD3D
 */
public class PagingPersonReader extends Hd3dPagingJsonReader<PersonModelData>
{
    /**
     * Default constructor.
     */
    public PagingPersonReader()
    {
        super(PersonModelData.getModelType());
    }

    @Override
    public PersonModelData newModelInstance()
    {
        return new PersonModelData();
    }
}
