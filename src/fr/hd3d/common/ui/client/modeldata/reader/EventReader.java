package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.resource.EventModelData;


/**
 * GXT Reader for event data.
 * 
 * @author HD3D
 */
public class EventReader extends Hd3dListJsonReader<EventModelData>
{

    public EventReader()
    {
        super(EventModelData.getModelType());
    }

    @Override
    public EventModelData newModelInstance()
    {
        return new EventModelData();
    }
}
