package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.technical.ProjectSecurityTemplateModelData;


public class ProjectSecurityTemplateReader extends Hd3dListJsonReader<ProjectSecurityTemplateModelData>
{

    public ProjectSecurityTemplateReader()
    {
        super(ProjectSecurityTemplateModelData.getModelType());
    }

    public ProjectSecurityTemplateModelData newModelInstance()
    {
        return new ProjectSecurityTemplateModelData();
    }

}
