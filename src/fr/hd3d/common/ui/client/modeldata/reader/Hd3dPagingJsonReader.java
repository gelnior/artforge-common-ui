package fr.hd3d.common.ui.client.modeldata.reader;

import java.util.List;

import com.extjs.gxt.ui.client.data.BasePagingLoadResult;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.data.ModelType;
import com.extjs.gxt.ui.client.data.PagingLoadResult;


/**
 * Reader needed by GXT paging widgets.
 * 
 * @author HD3D
 * @param <C>
 *            Model Data class that reader can read.
 */
public abstract class Hd3dPagingJsonReader<C extends ModelData> extends Hd3dJsonReader<C, PagingLoadResult<C>>
        implements IPagingReader<C>
{
    /**
     * Creates a new reader.
     * 
     * @param modelType
     *            the model type definition
     */
    public Hd3dPagingJsonReader(ModelType modelType)
    {
        super(modelType);
    }

    /**
     * Responsible for the object being returned by the reader.
     * 
     * @param loadConfig
     *            the load configuration
     * @param records
     *            the list of models
     * @param totalCount
     *            the total count
     * @return the data to be returned by the reader
     */
    @Override
    protected BasePagingLoadResult<C> createReturnData(Object loadConfig, List<C> records, int totalCount)
    {
        BasePagingLoadResult<C> result = new BasePagingLoadResult<C>(records);
        result.setTotalLength(totalCount);

        return result;
    }
}
