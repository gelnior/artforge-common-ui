package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.task.PlanningModelData;


/**
 * Reader that translates Planning object from json format to model data object.
 * 
 * @author HD3D
 */
public class PlanningReader extends Hd3dListJsonReader<PlanningModelData>
{

    /**
     * Default constructor.
     */
    public PlanningReader()
    {
        super(PlanningModelData.getModelType());
    }

    /**
     * @return A new instance of PlanningModelData.
     */
    @Override
    public PlanningModelData newModelInstance()
    {
        return new PlanningModelData();
    }

}
