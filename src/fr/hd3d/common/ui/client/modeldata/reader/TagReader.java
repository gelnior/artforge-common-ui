package fr.hd3d.common.ui.client.modeldata.reader;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.technical.TagModelData;


public class TagReader extends Hd3dListJsonReader<TagModelData>
{
    public TagReader()
    {
        super(TagModelData.getModelType());
    }

    public TagReader(ModelType modelType)
    {
        super(modelType);
    }

    @Override
    public TagModelData newModelInstance()
    {
        return new TagModelData();
    }

}
