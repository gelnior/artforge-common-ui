package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.task.ActivityModelData;


/**
 * Reader that translates Activity object from json format to model data object.
 * 
 * @author HD3D
 */
public class ActivityReader extends Hd3dListJsonReader<ActivityModelData>
{

    /**
     * Default constructor.
     */
    public ActivityReader()
    {
        super(ActivityModelData.getModelType());
    }

    /**
     * @return A new instance of TaskModelData.
     */
    @Override
    public ActivityModelData newModelInstance()
    {
        return new ActivityModelData();
    }

}
