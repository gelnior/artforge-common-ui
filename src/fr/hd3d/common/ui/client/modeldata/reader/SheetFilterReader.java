package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.technical.SheetFilterModelData;


/**
 * GXT Reader for computer data.
 * 
 * @author HD3D
 */
public class SheetFilterReader extends Hd3dListJsonReader<SheetFilterModelData>
{
    public SheetFilterReader()
    {
        super(SheetFilterModelData.getModelType());
    }

    @Override
    public SheetFilterModelData newModelInstance()
    {
        return new SheetFilterModelData();
    }
}
