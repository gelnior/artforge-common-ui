package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelData;


/**
 * GXT Reader for computer data.
 * 
 * @author HD3D
 */
public class ComputerReader extends Hd3dListJsonReader<ComputerModelData>
{

    public ComputerReader()
    {
        super(ComputerModelData.getModelType());
    }

    @Override
    public ComputerModelData newModelInstance()
    {
        return new ComputerModelData();
    }
}
