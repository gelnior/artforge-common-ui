package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.asset.MountPointModelData;


/**
 * GXT Reader for approvalNote data.
 * 
 * @author HD3D
 */
public class MountPointReader extends Hd3dListJsonReader<MountPointModelData>
{

    public MountPointReader()
    {
        super(MountPointModelData.getModelType());
    }

    @Override
    public MountPointModelData newModelInstance()
    {
        return new MountPointModelData();
    }
}
