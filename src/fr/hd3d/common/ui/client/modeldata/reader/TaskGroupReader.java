package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.task.TaskGroupModelData;


/**
 * Reader that translates PlanningTaskGroupModelData object from json format to model data object.
 * 
 * @author HD3D
 */
public class TaskGroupReader extends Hd3dListJsonReader<TaskGroupModelData>
{

    /**
     * Default constructor.
     */
    public TaskGroupReader()
    {
        super(TaskGroupModelData.getModelType());
    }

    /**
     * @return A new instance of PlanningTaskGroupModelData.
     */
    @Override
    public TaskGroupModelData newModelInstance()
    {
        return new TaskGroupModelData();
    }

}
