package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.technical.TagModelData;


public class TagReaderSingleton
{
    private static IReader<TagModelData> reader;

    public final static IReader<TagModelData> getInstance()
    {
        if (reader == null)
        {
            reader = new TagReader();
        }
        return reader;
    }

    public final static void setInstanceAsMock(IReader<TagModelData> mock)
    {
        reader = mock;
    }
}
