package fr.hd3d.common.ui.client.modeldata.reader;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.production.ProjectTypeModelData;


/**
 * Reader that translates ProjectTypeReader object from json format to model data object.
 * 
 * @author HD3D
 */
public class ProjectTypeReader extends Hd3dListJsonReader<ProjectTypeModelData>
{

    /**
     * Default constructor.
     */
    public ProjectTypeReader()
    {
        super(ProjectTypeModelData.getModelType());
    }

    public ProjectTypeReader(ModelType modelType)
    {
        super(modelType);
    }

    /**
     * @return A new instance of ProjectTypeReader.
     */
    @Override
    public ProjectTypeModelData newModelInstance()
    {
        return new ProjectTypeModelData();
    }

}
