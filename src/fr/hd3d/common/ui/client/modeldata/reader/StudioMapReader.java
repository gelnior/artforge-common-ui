package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.inventory.StudioMapModelData;


/**
 * GXT Reader for screen data.
 * 
 * @author HD3D
 */
public class StudioMapReader extends Hd3dListJsonReader<StudioMapModelData>
{

    public StudioMapReader()
    {
        super(StudioMapModelData.getModelType());
    }

    @Override
    public StudioMapModelData newModelInstance()
    {
        return new StudioMapModelData();
    }
}
