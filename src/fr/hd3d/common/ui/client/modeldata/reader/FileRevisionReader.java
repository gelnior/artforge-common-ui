package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.asset.FileRevisionModelData;


public class FileRevisionReader extends Hd3dListJsonReader<FileRevisionModelData>
{

    public FileRevisionReader()
    {
        super(FileRevisionModelData.getModelType());
    }

    @Override
    public FileRevisionModelData newModelInstance()
    {
        return new FileRevisionModelData();
    }

}
