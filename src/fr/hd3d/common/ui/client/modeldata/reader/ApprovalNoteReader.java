package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;


/**
 * GXT Reader for approvalNote data.
 * 
 * @author HD3D
 */
public class ApprovalNoteReader extends Hd3dListJsonReader<ApprovalNoteModelData>
{

    public ApprovalNoteReader()
    {
        super(ApprovalNoteModelData.getModelType());
    }

    @Override
    public ApprovalNoteModelData newModelInstance()
    {
        return new ApprovalNoteModelData();
    }
}
