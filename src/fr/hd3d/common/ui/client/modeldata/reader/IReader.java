package fr.hd3d.common.ui.client.modeldata.reader;

import com.extjs.gxt.ui.client.data.ListLoadResult;
import com.extjs.gxt.ui.client.data.ModelType;


public interface IReader<C>
{
    public ListLoadResult<C> read(String data);

    public void setModelType(ModelType type);

    public int getTotalCount();

    public C newModelInstance();
}
