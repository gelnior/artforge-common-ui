package fr.hd3d.common.ui.client.modeldata.reader;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;


public class CategoryReader extends Hd3dListJsonReader<CategoryModelData>
{
    public CategoryReader()
    {
        super(CategoryModelData.getModelType());
    }

    public CategoryReader(ModelType modelType)
    {
        super(modelType);
    }

    @Override
    public CategoryModelData newModelInstance()
    {
        return new CategoryModelData();
    }

}
