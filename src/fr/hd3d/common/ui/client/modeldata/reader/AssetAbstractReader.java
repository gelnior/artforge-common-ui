package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.asset.AssetAbstractModelData;


public class AssetAbstractReader extends Hd3dListJsonReader<AssetAbstractModelData>
{

    public AssetAbstractReader()
    {
        super(AssetAbstractModelData.getModelType());
    }

    @Override
    public AssetAbstractModelData newModelInstance()
    {
        return new AssetAbstractModelData();
    }

}
