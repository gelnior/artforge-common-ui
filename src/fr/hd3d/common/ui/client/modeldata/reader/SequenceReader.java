package fr.hd3d.common.ui.client.modeldata.reader;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;


public class SequenceReader extends Hd3dListJsonReader<SequenceModelData>
{
    public SequenceReader()
    {
        super(SequenceModelData.getModelType());
    }

    public SequenceReader(ModelType modelType)
    {
        super(modelType);
    }

    @Override
    public SequenceModelData newModelInstance()
    {
        return new SequenceModelData();
    }

}
