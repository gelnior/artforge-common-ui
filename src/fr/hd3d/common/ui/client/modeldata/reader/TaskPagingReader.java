package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;


public class TaskPagingReader extends Hd3dPagingJsonReader<TaskModelData>
{

    public TaskPagingReader()
    {
        super(TaskModelData.getModelType());
    }

    @Override
    public TaskModelData newModelInstance()
    {
        return new TaskModelData();
    }

}
