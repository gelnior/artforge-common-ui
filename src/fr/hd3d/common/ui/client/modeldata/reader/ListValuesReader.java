package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.technical.ListValuesModelData;


public class ListValuesReader extends Hd3dListJsonReader<ListValuesModelData>
{

    public ListValuesReader()
    {
        super(ListValuesModelData.getModelType());
    }

    @Override
    public ListValuesModelData newModelInstance()
    {
        return new ListValuesModelData();
    }
}
