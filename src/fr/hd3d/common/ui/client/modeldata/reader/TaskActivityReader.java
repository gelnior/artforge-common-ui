package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.task.TaskActivityModelData;


/**
 * Reader that translates Task Activity object from json format to model data object.
 * 
 * @author HD3D
 */
public class TaskActivityReader extends Hd3dListJsonReader<TaskActivityModelData>
{

    /**
     * Default constructor.
     */
    public TaskActivityReader()
    {
        super(TaskActivityModelData.getModelType());
    }

    /**
     * @return A new instance of TaskModelData.
     */
    @Override
    public TaskActivityModelData newModelInstance()
    {
        return new TaskActivityModelData();
    }

}
