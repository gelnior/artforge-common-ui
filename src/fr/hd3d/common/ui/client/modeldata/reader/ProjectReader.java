package fr.hd3d.common.ui.client.modeldata.reader;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;


/**
 * Reader that translates ProjectReader object from json format to model data object.
 * 
 * @author HD3D
 */
public class ProjectReader extends Hd3dListJsonReader<ProjectModelData>
{

    /**
     * Default constructor.
     */
    public ProjectReader()
    {
        super(ProjectModelData.getModelType());
    }

    public ProjectReader(ModelType modelType)
    {
        super(modelType);
    }

    /**
     * @return A new instance of ProjectReader.
     */
    @Override
    public ProjectModelData newModelInstance()
    {
        return new ProjectModelData();
    }

}
