package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.asset.ProxyTypeModelData;


public class ProxyTypeReader extends Hd3dListJsonReader<ProxyTypeModelData>
{

    public ProxyTypeReader()
    {
        super(ProxyTypeModelData.getModelType());
    }

    @Override
    public ProxyTypeModelData newModelInstance()
    {
        return new ProxyTypeModelData();
    }

}
