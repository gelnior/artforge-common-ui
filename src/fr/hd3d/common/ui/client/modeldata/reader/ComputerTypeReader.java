package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.inventory.ComputerTypeModelData;


/**
 * GXT Reader for computer data.
 * 
 * @author HD3D
 */
public class ComputerTypeReader extends Hd3dListJsonReader<ComputerTypeModelData>
{

    public ComputerTypeReader()
    {
        super(ComputerTypeModelData.getModelType());
    }

    @Override
    public ComputerTypeModelData newModelInstance()
    {
        return new ComputerTypeModelData();
    }
}
