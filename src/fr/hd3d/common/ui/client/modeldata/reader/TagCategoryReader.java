package fr.hd3d.common.ui.client.modeldata.reader;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.technical.TagCategoryModelData;


public class TagCategoryReader extends Hd3dListJsonReader<TagCategoryModelData>
{

    public TagCategoryReader(ModelType modelType)
    {
        super(modelType);
    }

    @Override
    public TagCategoryModelData newModelInstance()
    {
        return new TagCategoryModelData();
    }

}
