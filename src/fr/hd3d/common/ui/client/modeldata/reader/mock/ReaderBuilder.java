package fr.hd3d.common.ui.client.modeldata.reader.mock;

import com.extjs.gxt.ui.client.data.ModelType;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteTypeModelData;
import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;
import fr.hd3d.common.ui.client.modeldata.production.CompositionModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.modeldata.reader.Hd3dListJsonReader;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.common.ui.client.modeldata.task.SimpleActivityModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskActivityModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskGroupModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.modeldata.technical.RoleModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;


/**
 * Provides JSON javascript parser corresponding to class given in parameter.
 * 
 * @author HD3D
 */
public class ReaderBuilder implements IReaderBuilder
{
    public <T extends Hd3dModelData> IReader<T> makeBuilder(final Class<T> classLiteral, ModelType modelType)
    {
        return new Hd3dListJsonReader<T>(modelType) {
            @Override
            public T newModelInstance()
            {
                if (classLiteral == ApprovalNoteTypeModelData.class)
                    return GWT.<T> create(ApprovalNoteTypeModelData.class);
                else if (classLiteral == ApprovalNoteModelData.class)
                    return GWT.<T> create(ApprovalNoteModelData.class);
                else if (classLiteral == TaskActivityModelData.class)
                    return GWT.<T> create(TaskActivityModelData.class);
                else if (classLiteral == SimpleActivityModelData.class)
                    return GWT.<T> create(SimpleActivityModelData.class);
                else if (classLiteral == TaskModelData.class)
                    return GWT.<T> create(TaskModelData.class);
                else if (classLiteral == ShotModelData.class)
                    return GWT.<T> create(ShotModelData.class);
                else if (classLiteral == ConstituentModelData.class)
                    return GWT.<T> create(ConstituentModelData.class);
                else if (classLiteral == CategoryModelData.class)
                    return GWT.<T> create(CategoryModelData.class);
                else if (classLiteral == SequenceModelData.class)
                    return GWT.<T> create(SequenceModelData.class);
                else if (classLiteral == CompositionModelData.class)
                    return GWT.<T> create(CompositionModelData.class);
                else if (classLiteral == ResourceGroupModelData.class)
                    return GWT.<T> create(ResourceGroupModelData.class);
                else if (classLiteral == PersonModelData.class)
                    return GWT.<T> create(PersonModelData.class);
                else if (classLiteral == ApprovalNoteTypeModelData.class)
                    return GWT.<T> create(ApprovalNoteTypeModelData.class);
                else if (classLiteral == TaskGroupModelData.class)
                    return GWT.<T> create(TaskGroupModelData.class);
                else if (classLiteral == TaskTypeModelData.class)
                    return GWT.<T> create(TaskTypeModelData.class);
                else if (classLiteral == RoleModelData.class)
                    return GWT.<T> create(RoleModelData.class);
                else if (classLiteral == SheetModelData.class)
                    return GWT.<T> create(SheetModelData.class);
                else
                    return null;
            }
        };
    }
}
