package fr.hd3d.common.ui.client.modeldata.reader;

import java.util.List;

import com.extjs.gxt.ui.client.data.BaseListLoadResult;
import com.extjs.gxt.ui.client.data.ListLoadResult;
import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.data.ModelType;


public abstract class Hd3dListJsonReader<C extends ModelData> extends Hd3dJsonReader<C, ListLoadResult<C>>
{
    /**
     * Creates a new reader.
     * 
     * @param modelType
     *            the model type definition
     */
    public Hd3dListJsonReader(ModelType modelType)
    {
        super(modelType);
    }

    /**
     * Responsible for the object being returned by the reader.
     * 
     * @param loadConfig
     *            the load config
     * @param records
     *            the list of models
     * @param totalCount
     *            the total count
     * @return the data to be returned by the reader
     */
    @Override
    protected ListLoadResult<C> createReturnData(Object loadConfig, List<C> records, int totalCount)
    {
        BaseListLoadResult<C> result = new BaseListLoadResult<C>(records);

        return result;
    }
}
