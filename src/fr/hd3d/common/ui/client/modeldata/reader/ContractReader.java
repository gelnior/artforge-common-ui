package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.resource.ContractModelData;


public class ContractReader extends Hd3dListJsonReader<ContractModelData>
{

    public ContractReader()
    {
        super(ContractModelData.getModelType());
    }

    @Override
    public ContractModelData newModelInstance()
    {
        return new ContractModelData();
    }

}
