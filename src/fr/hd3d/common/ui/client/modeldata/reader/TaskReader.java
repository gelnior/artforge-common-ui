package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;


/**
 * Reader that translates TaskModelData object from json format to model data object.
 * 
 * @author HD3D
 */
public class TaskReader extends Hd3dListJsonReader<TaskModelData>
{

    /**
     * Default constructor.
     */
    public TaskReader()
    {
        super(TaskModelData.getModelType());
    }

    /**
     * @return A new instance of TaskModelData.
     */
    @Override
    public TaskModelData newModelInstance()
    {
        return new TaskModelData();
    }

}
