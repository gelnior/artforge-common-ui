package fr.hd3d.common.ui.client.modeldata.reader;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata.EntityPropertyModelData;


public class EntityPropertyReader extends Hd3dListJsonReader<EntityPropertyModelData>
{

    public EntityPropertyReader(ModelType modelType)
    {
        super(modelType);
    }

    @Override
    public EntityPropertyModelData newModelInstance()
    {
        return new EntityPropertyModelData();
    }
}
