package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.resource.MilestoneModelData;


public class MilestoneReader extends Hd3dListJsonReader<MilestoneModelData>
{

    public MilestoneReader()
    {
        super(MilestoneModelData.getModelType());
    }

    @Override
    public MilestoneModelData newModelInstance()
    {
        return new MilestoneModelData();
    }

}
