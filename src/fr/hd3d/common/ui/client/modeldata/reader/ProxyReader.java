package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.asset.ProxyModelData;


public class ProxyReader extends Hd3dListJsonReader<ProxyModelData>
{

    public ProxyReader()
    {
        super(ProxyModelData.getModelType());
    }

    @Override
    public ProxyModelData newModelInstance()
    {
        return new ProxyModelData();
    }

}
