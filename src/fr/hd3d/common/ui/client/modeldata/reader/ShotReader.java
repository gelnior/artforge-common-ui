package fr.hd3d.common.ui.client.modeldata.reader;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;


public class ShotReader extends Hd3dListJsonReader<ShotModelData>
{

    public ShotReader()
    {
        super(ShotModelData.getModelType());
    }

    public ShotReader(ModelType modelType)
    {
        super(modelType);
    }

    @Override
    public ShotModelData newModelInstance()
    {
        return new ShotModelData();
    }

}
