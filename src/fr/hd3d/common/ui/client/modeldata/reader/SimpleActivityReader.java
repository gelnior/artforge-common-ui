package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.task.SimpleActivityModelData;


/**
 * Reader that translates Simple Activity object from json format to model data object.
 * 
 * @author HD3D
 */
public class SimpleActivityReader extends Hd3dListJsonReader<SimpleActivityModelData>
{

    /**
     * Default constructor.
     */
    public SimpleActivityReader()
    {
        super(SimpleActivityModelData.getModelType());
    }

    /**
     * @return A new instance of TaskModelData.
     */
    @Override
    public SimpleActivityModelData newModelInstance()
    {
        return new SimpleActivityModelData();
    }

}
