package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.task.TaskActivityModelData;


/**
 * Paging reader for Task data.
 * 
 * @author HD3D
 */
public class PagingTaskActivityReader extends Hd3dPagingJsonReader<TaskActivityModelData>
{
    /**
     * Default constructor.
     */
    public PagingTaskActivityReader()
    {
        super(TaskActivityModelData.getModelType());
    }

    @Override
    public TaskActivityModelData newModelInstance()
    {
        return new TaskActivityModelData();
    }
}
