package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.inventory.DeviceTypeModelData;


/**
 * GXT Reader for device data.
 * 
 * @author HD3D
 */
public class DeviceTypeReader extends Hd3dListJsonReader<DeviceTypeModelData>
{

    public DeviceTypeReader()
    {
        super(DeviceTypeModelData.getModelType());
    }

    @Override
    public DeviceTypeModelData newModelInstance()
    {
        return new DeviceTypeModelData();
    }
}
