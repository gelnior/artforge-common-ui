package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.task.PersonDayModelData;


/**
 * Reader that translates Personday object from json format to model data object.
 * 
 * @author HD3D
 */
public class PersonDayReader extends Hd3dListJsonReader<PersonDayModelData>
{
    /**
     * Default constructor.
     */
    public PersonDayReader()
    {
        super(PersonDayModelData.getModelType());
    }

    /**
     * @return A new instance of PersonDayModelData.
     */
    @Override
    public PersonDayModelData newModelInstance()
    {
        return new PersonDayModelData();
    }
}
