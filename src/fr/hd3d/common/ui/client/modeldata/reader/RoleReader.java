package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.technical.RoleModelData;


public class RoleReader extends Hd3dListJsonReader<RoleModelData>
{

    public RoleReader()
    {
        super(RoleModelData.getModelType());
    }

    @Override
    public RoleModelData newModelInstance()
    {
        return new RoleModelData();
    }
}
