package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.inventory.LicenseModelData;


/**
 * GXT Reader for device data.
 * 
 * @author HD3D
 */
public class LicenseReader extends Hd3dListJsonReader<LicenseModelData>
{

    public LicenseReader()
    {
        super(LicenseModelData.getModelType());
    }

    @Override
    public LicenseModelData newModelInstance()
    {
        return new LicenseModelData();
    }
}
