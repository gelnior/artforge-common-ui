package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.inventory.PoolModelData;


/**
 * GXT Reader for device data.
 * 
 * @author HD3D
 */
public class PoolReader extends Hd3dListJsonReader<PoolModelData>
{

    public PoolReader()
    {
        super(PoolModelData.getModelType());
    }

    @Override
    public PoolModelData newModelInstance()
    {
        return new PoolModelData();
    }
}
