package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.inventory.DeviceModelData;


/**
 * GXT Reader for device data.
 * 
 * @author HD3D
 */
public class DeviceReader extends Hd3dListJsonReader<DeviceModelData>
{

    public DeviceReader()
    {
        super(DeviceModelData.getModelType());
    }

    @Override
    public DeviceModelData newModelInstance()
    {
        return new DeviceModelData();
    }
}
