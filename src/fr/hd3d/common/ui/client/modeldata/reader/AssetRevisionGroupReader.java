package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.asset.AssetRevisionGroupModelData;


public class AssetRevisionGroupReader extends Hd3dListJsonReader<AssetRevisionGroupModelData>
{

    public AssetRevisionGroupReader()
    {
        super(AssetRevisionGroupModelData.getModelType());
    }

    @Override
    public AssetRevisionGroupModelData newModelInstance()
    {
        return new AssetRevisionGroupModelData();
    }

}
