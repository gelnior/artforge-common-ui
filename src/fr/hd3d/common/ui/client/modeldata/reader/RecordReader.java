package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;


/**
 * GXT Reader for explorer record data.
 * 
 * @author HD3D
 */
public class RecordReader extends Hd3dListJsonReader<RecordModelData>
{
    /**
     * Default constructor. Sets the Record Model Type into the reader.
     */
    public RecordReader()
    {
        super(RecordModelData.getModelType());
    }

    @Override
    public RecordModelData newModelInstance()
    {
        return new RecordModelData();
    }
}
