package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;


public class ProjectReaderSingleton
{
    private static IReader<ProjectModelData> instance;

    public static IReader<ProjectModelData> getInstance()
    {
        if (instance == null)
            instance = new ProjectReader(ProjectModelData.getModelType());
        return instance;
    }

    public static void setInstanceAsMock(IReader<ProjectModelData> mock)
    {
        instance = mock;
    }
}
