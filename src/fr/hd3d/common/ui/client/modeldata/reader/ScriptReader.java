package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.ScriptModelData;


/**
 * GXT Reader for script data.
 * 
 * @author HD3D
 */
public class ScriptReader extends Hd3dListJsonReader<ScriptModelData>
{
    /**
     * Default contructor.
     */
    public ScriptReader()
    {
        super(ScriptModelData.getModelType());
    }

    @Override
    public ScriptModelData newModelInstance()
    {
        return new ScriptModelData();
    }
}
