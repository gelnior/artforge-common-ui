package fr.hd3d.common.ui.client.modeldata.reader;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.asset.FileModelData;


public class FilesReader extends Hd3dListJsonReader<FileModelData>
{

    public FilesReader(ModelType modelType)
    {
        super(modelType);
    }

    @Override
    public FileModelData newModelInstance()
    {
        return new FileModelData();
    }
}
