package fr.hd3d.common.ui.client.modeldata.reader.playlist;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.playlist.PlayListRevisionModelData;
import fr.hd3d.common.ui.client.modeldata.reader.Hd3dListJsonReader;


public class PlayListRevisionReader extends Hd3dListJsonReader<PlayListRevisionModelData>
{

    public PlayListRevisionReader()
    {
        super(PlayListRevisionModelData.getModelType());
    }

    public PlayListRevisionReader(ModelType modelType)
    {
        super(modelType);
    }

    @Override
    public PlayListRevisionModelData newModelInstance()
    {
        return new PlayListRevisionModelData();
    }

}
