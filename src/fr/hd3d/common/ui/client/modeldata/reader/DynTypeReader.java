package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.technical.DynTypeModelData;


/**
 * GXT Reader for dynamic type data.
 * 
 * @author HD3D
 */
public class DynTypeReader extends Hd3dListJsonReader<DynTypeModelData>
{
    public DynTypeReader()
    {
        super(DynTypeModelData.getModelType());
    }

    @Override
    public DynTypeModelData newModelInstance()
    {
        return new DynTypeModelData();
    }
}
