package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.task.TaskChangesModelData;


public class TaskChangesReader extends Hd3dListJsonReader<TaskChangesModelData>
{

    public TaskChangesReader()
    {
        super(TaskChangesModelData.getModelType());
    }

    @Override
    public TaskChangesModelData newModelInstance()
    {
        return new TaskChangesModelData();
    }

}
