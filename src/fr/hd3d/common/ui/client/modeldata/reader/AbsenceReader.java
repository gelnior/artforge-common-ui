package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.resource.AbsenceModelData;


/**
 * GXT Reader for event data.
 * 
 * @author HD3D
 */
public class AbsenceReader extends Hd3dListJsonReader<AbsenceModelData>
{

    public AbsenceReader()
    {
        super(AbsenceModelData.getModelType());
    }

    @Override
    public AbsenceModelData newModelInstance()
    {
        return new AbsenceModelData();
    }
}
