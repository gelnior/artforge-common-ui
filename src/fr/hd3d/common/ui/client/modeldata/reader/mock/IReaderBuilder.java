package fr.hd3d.common.ui.client.modeldata.reader.mock;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;


public interface IReaderBuilder
{
    public abstract <T extends Hd3dModelData> IReader<T> makeBuilder(final Class<T> classLiteral, ModelType modelType);
}
