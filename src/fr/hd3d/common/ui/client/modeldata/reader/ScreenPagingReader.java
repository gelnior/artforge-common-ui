package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.inventory.ScreenModelData;


/**
 * Specific JSON reader for audit data.
 * 
 * @author HD3D
 */
public class ScreenPagingReader extends Hd3dPagingJsonReader<ScreenModelData>
{
    public ScreenPagingReader()
    {
        super(ScreenModelData.getModelType());
    }

    @Override
    public ScreenModelData newModelInstance()
    {
        return new ScreenModelData();
    }
}
