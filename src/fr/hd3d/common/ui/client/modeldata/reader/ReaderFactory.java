package fr.hd3d.common.ui.client.modeldata.reader;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteTypeModelData;
import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;
import fr.hd3d.common.ui.client.modeldata.production.CompositionModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.modeldata.reader.mock.IReaderBuilder;
import fr.hd3d.common.ui.client.modeldata.reader.mock.ReaderBuilder;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.common.ui.client.modeldata.task.SimpleActivityModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskActivityModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskGroupModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.modeldata.technical.RoleModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SettingModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;


/**
 * Factory providing right reader for JSON parsing. It is needed by test environment to switch easily to pure Java
 * parser.
 * 
 * @author HD3D
 */
public class ReaderFactory
{

    /** Reader builder user by the factory to generate the right reader. */
    static IReaderBuilder readerBuilder = null;

    /**
     * @return Reader builder user by the factory to generate the right reader. If no builder is the reader builder is
     *         automatically set a Javascript reader builder.
     */
    public static IReaderBuilder getReaderBuilder()
    {
        if (readerBuilder == null)
        {
            readerBuilder = new ReaderBuilder();
        }
        return readerBuilder;
    }

    /**
     * Set reader build : needed by test environment to set a pure Java reader builder.
     * 
     * @param readerBuilderMock
     *            The reader builder to set.
     */
    public static void setReaderBuilder(IReaderBuilder readerBuilderMock)
    {
        readerBuilder = readerBuilderMock;
    }

    /**
     * @param <T>
     *            Type of data to parse.
     * @param classLiteral
     *            The class of data to pars (T.class)
     * @param modelType
     *            The descriptor of objects to parse.
     * @return Reader corresponding to <i>classLiteral</i>.
     */
    public static <T extends Hd3dModelData> IReader<T> getReader(final Class<T> classLiteral, ModelType modelType)
    {
        return getReaderBuilder().makeBuilder(classLiteral, modelType);
    }

    /**
     * @return Correct approval type reader depending on context (if code is executed inside a navigator or in a java
     *         environment for test cases).
     */
    public static IReader<ApprovalNoteTypeModelData> getApprovalTypeReader()
    {
        return getReader(ApprovalNoteTypeModelData.class, ApprovalNoteTypeModelData.getModelType());
    }

    /**
     * @return Correct approval note reader depending on context (if code is executed inside a navigator or in a java
     *         environment for test cases).
     */
    public static IReader<ApprovalNoteModelData> getApprovalNoteReader()
    {
        return getReader(ApprovalNoteModelData.class, ApprovalNoteModelData.getModelType());
    }

    /**
     * @return Correct task reader depending on context (if code is executed inside a navigator or in a java environment
     *         for test cases).
     */
    public static IReader<TaskModelData> getTaskReader()
    {
        return getReader(TaskModelData.class, TaskModelData.getModelType());
    }

    /**
     * @return Correct task activity reader depending on context (if code is executed inside a navigator or in a java
     *         environment for test cases).
     */
    public static IReader<TaskActivityModelData> getTaskActivityReader()
    {
        return getReader(TaskActivityModelData.class, TaskActivityModelData.getModelType());
    }

    /**
     * @return Correct simple activity reader depending on context (if code is executed inside a navigator or in a java
     *         environment for test cases).
     */
    public static IReader<SimpleActivityModelData> getSimpleActivityReader()
    {
        return getReader(SimpleActivityModelData.class, SimpleActivityModelData.getModelType());
    }

    public static IReader<SettingModelData> getSettingsReader()
    {
        return getReader(SettingModelData.class, SettingModelData.getModelType());
    }

    /**
     * @return Correct resource group reader depending on context (if code is executed inside a navigator or in a java
     *         environment for test cases).
     */
    public static IReader<ResourceGroupModelData> getResourceGroupReader()
    {
        return getReader(ResourceGroupModelData.class, ResourceGroupModelData.getModelType());
    }

    /**
     * @return Correct resource group reader depending on context (if code is executed inside a navigator or in a java
     *         environment for test cases).
     */
    public static IReader<PersonModelData> getPersonReader()
    {
        return getReader(PersonModelData.class, PersonModelData.getModelType());
    }

    /**
     * @return Correct task group reader depending on context (if code is executed inside a navigator or in a java
     *         environment for test cases).
     */
    public static IReader<TaskGroupModelData> getTaskGroupReader()
    {
        return getReader(TaskGroupModelData.class, TaskGroupModelData.getModelType());
    }

    /**
     * @return Correct task group reader depending on context (if code is executed inside a navigator or in a java
     *         environment for test cases).
     */
    public static IReader<ApprovalNoteTypeModelData> getApprovalNoteTypeReader()
    {
        return getReader(ApprovalNoteTypeModelData.class, ApprovalNoteTypeModelData.getModelType());
    }

    /**
     * @return Correct shot reader depending on context (if code is executed inside a navigator or in a java environment
     *         for test cases).
     */
    public static IReader<ShotModelData> getShotReader()
    {
        return getReader(ShotModelData.class, ShotModelData.getModelType());
    }

    /**
     * @return Correct composition reader depending on context (if code is executed inside a navigator or in a java
     *         environment for test cases).
     */
    public static IReader<CompositionModelData> getCompositionReader()
    {
        return getReader(CompositionModelData.class, CompositionModelData.getModelType());
    }

    /**
     * @return Correct category reader depending on context (if code is executed inside a navigator or in a java
     *         environment for test cases).
     */
    public static IReader<CategoryModelData> getCategoryReader()
    {
        return getReader(CategoryModelData.class, CategoryModelData.getModelType());
    }

    /**
     * @return Correct task type reader depending on context (if code is executed inside a navigator or in a java
     *         environment for test cases).
     */
    public static IReader<TaskTypeModelData> getTaskTypeReader()
    {
        return getReader(TaskTypeModelData.class, TaskTypeModelData.getModelType());
    }

    /**
     * @return Correct role reader depending on context (if code is executed inside a navigator or in a java environment
     *         for test cases).
     */
    public static IReader<RoleModelData> getRoleReader()
    {
        return getReader(RoleModelData.class, RoleModelData.getModelType());
    }

    /**
     * @return Correct constituent reader depending on context (if code is executed inside a navigator or in a java
     *         environment for test cases).
     */
    public static IReader<ConstituentModelData> getConstituentReader()
    {
        return getReader(ConstituentModelData.class, ConstituentModelData.getModelType());
    }

    /**
     * @return Correct sheet reader depending on context (if code is executed inside a navigator or in a java
     *         environment for test cases).
     */
    public static IReader<SheetModelData> getSheetReader()
    {
        return getReader(SheetModelData.class, SheetModelData.getModelType());
    }
}
