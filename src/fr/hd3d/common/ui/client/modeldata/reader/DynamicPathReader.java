package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.asset.DynamicPathModelData;


/**
 * GXT Reader for approvalNote data.
 * 
 * @author HD3D
 */
public class DynamicPathReader extends Hd3dListJsonReader<DynamicPathModelData>
{

    public DynamicPathReader()
    {
        super(DynamicPathModelData.getModelType());
    }

    @Override
    public DynamicPathModelData newModelInstance()
    {
        return new DynamicPathModelData();
    }
}
