package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelModelData;


/**
 * GXT Reader for device data.
 * 
 * @author HD3D
 */
public class ComputerModelReader extends Hd3dListJsonReader<ComputerModelModelData>
{

    public ComputerModelReader()
    {
        super(ComputerModelModelData.getModelType());
    }

    @Override
    public ComputerModelModelData newModelInstance()
    {
        return new ComputerModelModelData();
    }
}
