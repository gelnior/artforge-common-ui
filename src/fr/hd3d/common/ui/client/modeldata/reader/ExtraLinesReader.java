package fr.hd3d.common.ui.client.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.task.ExtraLineModelData;


public class ExtraLinesReader extends Hd3dListJsonReader<ExtraLineModelData>
{

    public ExtraLinesReader()
    {
        super(ExtraLineModelData.getModelType());
    }

    @Override
    public ExtraLineModelData newModelInstance()
    {
        return new ExtraLineModelData();
    }

}
