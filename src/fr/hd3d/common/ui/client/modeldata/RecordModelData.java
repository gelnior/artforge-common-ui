package fr.hd3d.common.ui.client.modeldata;

import org.restlet.client.data.Method;

import com.extjs.gxt.ui.client.data.ModelType;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.callback.GetModelDataCallback;
import fr.hd3d.common.ui.client.service.callback.IsExistCallBack;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.EqConstraint;
import fr.hd3d.common.ui.client.service.parameter.ParameterBuilder;


/**
 * Model data for describing, base record : i.e. an Hd3dModelData with a name.
 * 
 * @author HD3D
 */
public class RecordModelData extends Hd3dModelData
{
    /** Automatically generated serial UID. */
    private static final long serialVersionUID = -7073360879632096712L;
    /** The record name. */
    public static final String NAME_FIELD = "name";
    /** The record name. */
    public static final String BOUND_CLASS_NAME_FIELD = "boundClassName";

    /**
     * @return Model type used by reader to know the record model data structure.
     */
    public static ModelType getModelType()
    {
        ModelType type = Hd3dModelData.getModelType();

        type.addField(ModelDataUtils.createDataField(NAME_FIELD, String.class));
        type.addField(ModelDataUtils.createDataField(BOUND_CLASS_NAME_FIELD, String.class));

        return type;
    }

    public RecordModelData(Long id, String name)
    {
        this.setId(id);
        this.setName(name);
    }

    public RecordModelData(String name)
    {
        this(null, name);
    }

    public RecordModelData()
    {}

    public String getName()
    {
        return this.get(NAME_FIELD);
    }

    public void setName(String name)
    {
        this.set(NAME_FIELD, name);
    }

    public String getBoundClassName()
    {
        return this.get(BOUND_CLASS_NAME_FIELD);
    }

    public void setBoundClassName(String boundClassName)
    {
        this.set(BOUND_CLASS_NAME_FIELD, boundClassName);
    }

    /**
     * Act as refresh() method, it means refresh all fields with server data. But the way to select record is not by id
     * it is by name.
     * 
     * @param eventType
     *            Event type to forward when refresh is finished.
     */
    public void refreshByName(EventType eventType)
    {
        Constraint constraint = new Constraint(EConstraintOperator.eq, NAME_FIELD, this.getName(), null);
        String path = this.getPath() + ParameterBuilder.parameterToString(constraint);

        IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
        requestHandler.handleRequest(Method.GET, path, null, new GetModelDataCallback(this, new AppEvent(eventType)));
    }

    /**
     * Act as refresh() method, it means refresh all fields with server data. But the way to select record is not by id
     * it is by name.
     * 
     * @param callback
     *            Callback called when request response is returned.
     */
    public void refreshByName(GetModelDataCallback callback)
    {
        Constraint constraint = new Constraint(EConstraintOperator.eq, NAME_FIELD, this.getName(), null);
        String path = this.getPath() + ParameterBuilder.parameterToString(constraint);

        IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
        requestHandler.handleRequest(Method.GET, path, null, callback);
    }

    /**
     * Check if the record is already exist in server data. Forward the given event type with the boolean corresponding
     * to if exist.
     * 
     * @param eventType
     *            Event type to forward to know if exist or not.
     */
    public void checkIsExist(EventType eventType, String... properties)
    {
        String path = ServicesPath.getPath(this.simpleClassName)
                + ParameterBuilder.parameterToString(new EqConstraint(NAME_FIELD, this.getName()));

        IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
        requestHandler.handleRequest(Method.GET, path, null, new IsExistCallBack(this, new AppEvent(eventType),
                properties));
    }

    public void checkIsExist(String path, EventType eventType, String... properties)
    {
        path += ParameterBuilder.parameterToString(new EqConstraint(NAME_FIELD, this.getName()));

        IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
        requestHandler.handleRequest(Method.GET, path, null, new IsExistCallBack(this, new AppEvent(eventType),
                properties));
    }
}
