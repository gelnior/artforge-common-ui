package fr.hd3d.common.ui.client.modeldata.asset;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;


/**
 * Model representing the class LDynamicPath.
 * 
 * @author HD3D
 * 
 */
public class ProxyTypeModelData extends RecordModelData
{
    private static final long serialVersionUID = 118437662888877910L;

    public static final String SIMPLE_CLASS_NAME = "ProxyType";
    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LProxyType";

    public static final String OPENTYPE_FIELD = "openType";
    public static final String DESCRIPTION_FIELD = "description";

    /**
     * Default constructor.
     */
    public ProxyTypeModelData()
    {
        super();

        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public ProxyTypeModelData(String name, String openType, String description)
    {
        this();
        this.setName(name);
        this.setOpenType(openType);
        this.setDescription(description);
    }

    public void setOpenType(String openType)
    {
        set(OPENTYPE_FIELD, openType);
    }

    public String getOpenType()
    {
        return get(OPENTYPE_FIELD);
    }

    public String getDescription()
    {
        return get(DESCRIPTION_FIELD);
    }

    public void setDescription(String description)
    {
        set(DESCRIPTION_FIELD, description);
    }

    /**
     * @return Model type used by reader to know the record model data structure.
     */
    public static ModelType getModelType()
    {
        ModelType modelType = RecordModelData.getModelType();

        modelType.addField(OPENTYPE_FIELD);
        modelType.addField(DESCRIPTION_FIELD);

        return modelType;
    }

}
