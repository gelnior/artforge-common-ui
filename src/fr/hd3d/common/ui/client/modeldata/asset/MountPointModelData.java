package fr.hd3d.common.ui.client.modeldata.asset;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;


/**
 * Model representing the class LDynamicPath.
 * 
 * @author HD3D
 * 
 */
public class MountPointModelData extends RecordModelData
{
    private static final long serialVersionUID = 118437662888877910L;

    public static final String SIMPLE_CLASS_NAME = "MountPoint";
    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LMountPoint";

    public static final String STORAGE_NAME_FIELD = "storageName";
    public static final String OPERATING_SYSTEM_FIELD = "operatingSystem";
    public static final String MOUNT_POINT_PATH_FIELD = "mountPointPath";

    /**
     * Default constructor.
     */
    public MountPointModelData()
    {
        super();

        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public MountPointModelData(String storageName, String operatingSystem, String mountPointPath)
    {
        this();

        this.setStorageName(storageName);
        this.setOperatingSystem(operatingSystem);
        this.setMountPointPath(mountPointPath);
    }

    public void setMountPointPath(String mountPointPath)
    {
        set(MOUNT_POINT_PATH_FIELD, mountPointPath);
    }

    public void setOperatingSystem(String operatingSystem)
    {
        set(OPERATING_SYSTEM_FIELD, operatingSystem);
    }

    public void setStorageName(String storageName)
    {
        set(STORAGE_NAME_FIELD, storageName);
    }

    public String getStorageName()
    {
        return get(STORAGE_NAME_FIELD);
    }

    public String getOperatingSystem()
    {
        return get(OPERATING_SYSTEM_FIELD);
    }

    public String getMountPointPath()
    {
        return get(MOUNT_POINT_PATH_FIELD);
    }

    /**
     * @return Model type used by reader to know the record model data structure.
     */
    public static ModelType getModelType()
    {
        ModelType modelType = RecordModelData.getModelType();

        modelType.addField(STORAGE_NAME_FIELD);
        modelType.addField(OPERATING_SYSTEM_FIELD);
        modelType.addField(MOUNT_POINT_PATH_FIELD);

        return modelType;
    }

}
