package fr.hd3d.common.ui.client.modeldata.asset;

import java.util.ArrayList;
import java.util.List;

import org.restlet.client.data.Method;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.data.ModelType;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.LoadListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.reader.AssetRevisionReader;
import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.Constraints;
import fr.hd3d.common.ui.client.service.parameter.InConstraint;
import fr.hd3d.common.ui.client.service.parameter.ParameterBuilder;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.util.CollectionUtils;


public class AssetRevisionGroupModelData extends RecordModelData
{

    private static final long serialVersionUID = -2187325828364069504L;
    public static final String CRITERIA_FIELD = "criteria";
    public static final String VALUE_FIELD = "value";
    public static final String PROJECT_ID_FIELD = "project";

    public static final String ASSET_REVISION_IDS_FIELD = "assetRevisions";

    public static final String SIMPLE_CLASS_NAME = "AssetRevisionGroup";
    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LAssetRevisionGroup";

    public static ModelType getModelType()
    {
        ModelType modelType = RecordModelData.getModelType();

        DataField projectIdField = new DataField(PROJECT_ID_FIELD);
        projectIdField.setType(Long.class);

        DataField assetIdsDataField = new DataField(ASSET_REVISION_IDS_FIELD);
        assetIdsDataField.setType(List.class);
        assetIdsDataField.setFormat("AssetRevision");

        modelType.addField(CRITERIA_FIELD);
        modelType.addField(VALUE_FIELD);
        modelType.addField(projectIdField);
        modelType.addField(assetIdsDataField);

        return modelType;
    }

    public AssetRevisionGroupModelData()
    {
        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public String getValue()
    {
        return get(VALUE_FIELD);
    }

    public void setValue(String value)
    {
        set(VALUE_FIELD, value);
    }

    public String getCriteria()
    {
        return get(CRITERIA_FIELD);
    }

    public void setCriteria(String criteria)
    {
        set(CRITERIA_FIELD, criteria);
    }

    public Long getProjectId()
    {
        return get(PROJECT_ID_FIELD);
    }

    public void setProjectId(Long projectId)
    {
        set(PROJECT_ID_FIELD, projectId);
    }

    public List<Long> getAssetRevisionIDs()
    {
        return get(ASSET_REVISION_IDS_FIELD);
    }

    public void setAssetRevisionIDs(List<Long> assetRevisionIDs)
    {
        this.set(ASSET_REVISION_IDS_FIELD, assetRevisionIDs);
    }

    public void refreshByWorkObject(Long workObjectId, String workObjectName, BaseCallback callback)
    {
        Constraints constraints = new Constraints();
        String criteria = "fr.hd3d.model.lightweight.impl.L" + workObjectName + ".id";
        constraints.add(new Constraint(EConstraintOperator.eq, VALUE_FIELD, workObjectId.toString()));
        constraints.add(new Constraint(EConstraintOperator.eq, CRITERIA_FIELD, criteria));
        String path = this.getPath() + ParameterBuilder.parameterToString(constraints);

        IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
        requestHandler.handleRequest(Method.GET, path, null, callback);
    }

    public void retrieveAssets(Long taskTypeId, final EventType eventType)
    {
        if (CollectionUtils.isNotEmpty(this.getAssetRevisionIDs()))
        {
            final ServiceStore<AssetRevisionModelData> assets = new ServiceStore<AssetRevisionModelData>(
                    new AssetRevisionReader());

            Constraints constraints = new Constraints();
            constraints.add(new InConstraint(AssetRevisionModelData.ID_FIELD, this.getAssetRevisionIDs()));
            if (taskTypeId != null)
                constraints.add(new Constraint(EConstraintOperator.eq, AssetRevisionModelData.TASK_TYPE_ID_FIELD,
                        taskTypeId));
            assets.addParameter(constraints);
            assets.setPath(ServicesPath.ASSET_REVISIONS);

            assets.addLoadListener(new LoadListener() {
                @Override
                public void loaderLoad(LoadEvent le)
                {
                    AppEvent appEvent = new AppEvent(eventType, assets.getModels());
                    EventDispatcher.forwardEvent(appEvent);
                }
            });
            assets.reload();
        }
        else
        {
            AppEvent appEvent = new AppEvent(eventType, new ArrayList<AssetRevisionModelData>());
            EventDispatcher.forwardEvent(appEvent);
        }
    }
}
