package fr.hd3d.common.ui.client.modeldata.asset;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;

public class AssetAbstractModelData extends Hd3dModelData
{
    private static final long serialVersionUID = 4878719229370904938L;
    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LAssetRevision";
    public static final String KEY_FIELD = "key";
    public static final String REVISION_FIELD = "revision";
    public static final String VARIATION_FIELD = "variation";
    public static final String NAME_FIELD = "name";
    public static final String LAST_REVISION_FIELD = "lastRevision";
    public static final String TYPE_FIELD = "type";
    public static final String CREATOR_ID_FIELD = "creatorId";
    public static final String CREATION_DATE_FIELD = "creationDate";
    
    
    /**
     * TODO cette class est incomplete !!!
     */
    public AssetAbstractModelData()
    {

    }
    
    public void setKey( String key )
    {
        this.set(KEY_FIELD, key);
    }
    
    public String getKey(  )
    {
        return this.get(KEY_FIELD);
    }
    
    public void setRevision(Long revision)
    {
        this.set(REVISION_FIELD, revision);
    }
    
    public Long getRevision()
    {
        return this.get(REVISION_FIELD);
    }
    
    public static ModelType getModelType()
    {
        ModelType type = Hd3dModelData.getModelType();
        
        type.addField( KEY_FIELD );
        type.addField( NAME_FIELD );
        type.addField( TYPE_FIELD ); 
        
        return type;
    }
}
