// package fr.hd3d.common.ui.client.modeldata.asset;
//
// import org.restlet.client.data.Method;
//
// import com.extjs.gxt.ui.client.data.DataField;
// import com.extjs.gxt.ui.client.data.ModelType;
//
// import fr.hd3d.common.client.enums.EConstraintOperator;
// import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
// import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
// import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
// import fr.hd3d.common.ui.client.service.callback.BaseCallback;
// import fr.hd3d.common.ui.client.service.parameter.Constraint;
// import fr.hd3d.common.ui.client.service.parameter.Constraints;
// import fr.hd3d.common.ui.client.service.parameter.ParameterBuilder;
//
//
// /**
// * TODO cette class est incomplete !!!
// */
// public class AssetRevisionFileRevisionModelData extends Hd3dModelData
// {
//
// private static final long serialVersionUID = 878739496502831151L;
//
// public static final String ASSET_KEY_FIELD = "assetKey";
// public static final String FILE_KEY_FIELD = "fileKey";
//
// public static final String ASSET_VARIATION_FIELD = "assetVariation";
// public static final String FILE_VARIATION_FIELD = "fileVariation";
//
// public static final String ASSET_REVISION_FIELD = "assetRevision";
// public static final String FILE_REVISION_FIELD = "fileRevision";
//
// public static final String SIMPLE_CLASS_NAME = "AssetRevisionFileRevision";
// public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LAssetRevisionFileRevision";
//
// /**
// * TODO cette class est incomplete !!!
// */
// public AssetRevisionFileRevisionModelData()
// {
// this.setSimpleClassName(SIMPLE_CLASS_NAME);
// this.setClassName(CLASS_NAME);
// }
//
// public void setAssetKey(String key)
// {
// this.set(ASSET_KEY_FIELD, key);
// }
//
// public String getAssetKey()
// {
// return this.get(ASSET_KEY_FIELD);
// }
//
// public void setAssetRevision(Integer revision)
// {
// this.set(ASSET_REVISION_FIELD, revision);
// }
//
// public Integer getAssetRevision()
// {
// return this.get(ASSET_REVISION_FIELD);
// }
//
// public void setAssetVariation(String variation)
// {
// this.set(ASSET_VARIATION_FIELD, variation);
// }
//
// public String getAssetVariation()
// {
// return this.get(ASSET_VARIATION_FIELD);
// }
//
// public void setFileKey(String key)
// {
// this.set(FILE_KEY_FIELD, key);
// }
//
// public String getFileKey()
// {
// return this.get(FILE_KEY_FIELD);
// }
//
// public void setFileRevision(Integer revision)
// {
// this.set(FILE_REVISION_FIELD, revision);
// }
//
// public Integer getFileRevision()
// {
// return this.get(FILE_REVISION_FIELD);
// }
//
// public void setFileVariation(String variation)
// {
// this.set(FILE_VARIATION_FIELD, variation);
// }
//
// public String getFileVariation()
// {
// return this.get(FILE_VARIATION_FIELD);
// }
//
// /**
// * Initialize asset fields (key, revision, variation) with information from asset given in parameter.
// *
// * @param asset
// * The asset needed to update asset fields.
// */
// public void setAsset(AssetRevisionModelData asset)
// {
// this.setAssetKey(asset.getName());
// this.setAssetRevision(asset.getRevision());
// this.setAssetVariation(asset.getVariation());
// }
//
// /**
// * Initialize file fields (key, revision, variation) with information from file given in parameter.
// *
// * @param file
// * The file needed to update file fields.
// */
// public void setFile(FileRevisionModelData file)
// {
// this.setFileKey(file.getKey());
// this.setFileRevision(file.getRevision());
// this.setFileVariation(file.getVariation());
// }
//
// public static ModelType getModelType()
// {
// ModelType md = Hd3dModelData.getModelType();
//
// md.addField(ASSET_KEY_FIELD);
// DataField assetRevisionDataField = new DataField(ASSET_REVISION_FIELD);
// assetRevisionDataField.setType(Integer.class);
// md.addField(assetRevisionDataField);
// md.addField(ASSET_VARIATION_FIELD);
//
// md.addField(FILE_KEY_FIELD);
// DataField fileRevisionDataField = new DataField(FILE_REVISION_FIELD);
// fileRevisionDataField.setType(Integer.class);
// md.addField(fileRevisionDataField);
// md.addField(FILE_VARIATION_FIELD);
//
// return md;
// }
//
// public void refreshByInputOutput(AssetRevisionModelData asset, FileRevisionModelData file, BaseCallback callback)
// {
// Constraints constraints = new Constraints();
// constraints.add(new Constraint(EConstraintOperator.eq, ASSET_KEY_FIELD, asset.getName(), null));
// constraints.add(new Constraint(EConstraintOperator.eq, ASSET_REVISION_FIELD, asset.getRevision(), null));
// constraints.add(new Constraint(EConstraintOperator.eq, ASSET_VARIATION_FIELD, asset.getVariation(), null));
// constraints.add(new Constraint(EConstraintOperator.eq, FILE_KEY_FIELD, file.getKey(), null));
// constraints.add(new Constraint(EConstraintOperator.eq, FILE_REVISION_FIELD, file.getRevision(), null));
// constraints.add(new Constraint(EConstraintOperator.eq, FILE_VARIATION_FIELD, file.getVariation(), null));
// String path = this.getPath() + ParameterBuilder.parameterToString(constraints);
//
// IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
// requestHandler.handleRequest(Method.GET, path, null, callback);
// }
// }
