package fr.hd3d.common.ui.client.modeldata.asset;

import com.extjs.gxt.ui.client.data.DataField;


public class EnumField extends DataField
{
    public EnumField(String field)
    {
        super(field);
        this.setType(Enum.class);
    }
}
