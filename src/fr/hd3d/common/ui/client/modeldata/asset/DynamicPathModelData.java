package fr.hd3d.common.ui.client.modeldata.asset;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.util.field.LongDataField;


/**
 * Model representing the class LDynamicPath.
 * 
 * @author HD3D
 * 
 */
public class DynamicPathModelData extends RecordModelData
{
    private static final long serialVersionUID = 118437662888877910L;

    public static final String SIMPLE_CLASS_NAME = "DynamicPath";
    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LDynamicPath";

    public static final String TASKTYPE_ID_FIELD = "taskTypeId";
    public static final String TASKTYPE_NAME_FIELD = "taskTypeName";
    public static final String TASKTYPE_COLOR_FIELD = "taskTypeColor";

    public static final String PROJECT_ID_FIELD = "projectId";
    public static final String BOUND_ENTITY_NAME_FIELD = "boundEntityName";
    public static final String WORKOBJECT_ID_FIELD = "workObjectId";
    public static final String WORKOBJECT_NAME_FIELD = "workObjectName";
    public static final String WIP_DYNAMIC_PATH_FIELD = "wipDynamicPath";
    public static final String OLD_DYNAMIC_PATH_FIELD = "oldDynamicPath";
    public static final String PUBLISH_DYNAMIC_PATH_FIELD = "publishDynamicPath";
    public static final String PARENT_FIELD = "parent";
    public static final String TYPE_FIELD = "type";

    public static final String FILE_TYPE = "file";
    public static final String THUMBNAIL_TYPE = "thumbnail";
    public static final String PROXY_TYPE = "proxy";

    /**
     * Default constructor.
     */
    public DynamicPathModelData()
    {
        super();

        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public DynamicPathModelData(String name, String type, Long projectId, Long workObjectId, String workObjectClass,
            Long tasktypeId, String wipPath, String publishPath, String oldPath, Long parent)
    {
        this();
        this.setName(name);
        this.setProjectId(projectId);
        this.setWorkObjectId(workObjectId);
        this.setBoundEntityName(workObjectClass);
        this.setTaskTypeId(tasktypeId);
        this.setWipDynamicPath(wipPath);
        this.setOldDynamicPath(oldPath);
        this.setPublishDynamicPath(publishPath);
        this.setType(type);
        this.setParent(parent);
    }

    /**
     * @return Model type used by reader to know the record model data structure.
     */
    public static ModelType getModelType()
    {
        ModelType modelType = RecordModelData.getModelType();

        modelType.addField(new LongDataField(PROJECT_ID_FIELD));
        modelType.addField(new LongDataField(WORKOBJECT_ID_FIELD));
        modelType.addField(WORKOBJECT_NAME_FIELD);
        modelType.addField(BOUND_ENTITY_NAME_FIELD);
        modelType.addField(new LongDataField(TASKTYPE_ID_FIELD));
        modelType.addField(TASKTYPE_NAME_FIELD);
        modelType.addField(TASKTYPE_COLOR_FIELD);
        modelType.addField(WIP_DYNAMIC_PATH_FIELD);
        modelType.addField(OLD_DYNAMIC_PATH_FIELD);
        modelType.addField(PUBLISH_DYNAMIC_PATH_FIELD);
        modelType.addField(TYPE_FIELD);
        modelType.addField(new LongDataField(PARENT_FIELD));

        return modelType;
    }

    public Long getWorkObjectId()
    {
        return this.get(WORKOBJECT_ID_FIELD);
    }

    public void setWorkObjectId(Long workObjectId)
    {
        set(WORKOBJECT_ID_FIELD, workObjectId);
    }

    public String getWorkObjectName()
    {
        return this.get(WORKOBJECT_NAME_FIELD);
    }

    public void setWorkObjectName(String workObjectName)
    {
        set(WORKOBJECT_NAME_FIELD, workObjectName);
    }

    public String getBoundEntityName()
    {
        return get(BOUND_ENTITY_NAME_FIELD);
    }

    public void setBoundEntityName(String boundEntityName)
    {
        set(BOUND_ENTITY_NAME_FIELD, boundEntityName);
    }

    public Long getTaskTypeId()
    {
        return get(TASKTYPE_ID_FIELD);
    }

    public void setTaskTypeId(Long tasktypeId)
    {
        set(TASKTYPE_ID_FIELD, tasktypeId);
    }

    public String getTaskTypeName()
    {
        return get(TASKTYPE_NAME_FIELD);
    }

    public void setTaskTypeName(Long tasktypeName)
    {
        set(TASKTYPE_NAME_FIELD, tasktypeName);
    }

    public String getWipDynamicPath()
    {
        return get(WIP_DYNAMIC_PATH_FIELD);
    }

    public void setWipDynamicPath(String wipDynamicPath)
    {
        set(WIP_DYNAMIC_PATH_FIELD, wipDynamicPath);
    }

    public String getOldDynamicPath()
    {
        return get(OLD_DYNAMIC_PATH_FIELD);
    }

    public void setOldDynamicPath(String oldDynamicPath)
    {
        set(OLD_DYNAMIC_PATH_FIELD, oldDynamicPath);
    }

    public String getPublishDynamicPath()
    {
        return get(PUBLISH_DYNAMIC_PATH_FIELD);
    }

    public void setPublishDynamicPath(String publishDynamicPath)
    {
        set(PUBLISH_DYNAMIC_PATH_FIELD, publishDynamicPath);
    }

    public Long getProjectId()
    {
        return get(PROJECT_ID_FIELD);
    }

    public Long getParent()
    {
        return (Long) get(PARENT_FIELD);
    }

    public String getType()
    {
        return get(TYPE_FIELD);
    }

    public void setType(String type)
    {
        set(TYPE_FIELD, type);
    }

    public void setParent(Long parent)
    {
        set(PARENT_FIELD, parent);
    }

    public void setProjectId(Long projectId)
    {
        set(PROJECT_ID_FIELD, projectId);
    }

    public String getTaskTypeColor()
    {
        return get(TASKTYPE_COLOR_FIELD);
    }

}
