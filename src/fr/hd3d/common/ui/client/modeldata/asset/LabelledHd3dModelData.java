package fr.hd3d.common.ui.client.modeldata.asset;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;


@Deprecated
public abstract class LabelledHd3dModelData extends Hd3dModelData
{
    private static final long serialVersionUID = 8073139578036142714L;

    public abstract String getLabel();

    public abstract void setLabel(String value);
}
