package fr.hd3d.common.ui.client.modeldata.asset;

import java.util.List;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;


public class FileModelData extends RecordModelData
{

    /**
     * 
     */
    private static final long serialVersionUID = -8907904483900945052L;

    private static final String FILE_NAME = "key";
    private static final String FILE_LOCATION = "location";
    private static final String FILE_PATH = "relativePath";
    private static final String FILE_FORMAT = "format";
    private static final String FILE_SIZE = "size";
    private static final String FILE_TAGS = "tags";

    private static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LFileRevision";
    private static final String SIMPLE_CLASS_NAME = "FileRevision";

    public enum PREVIEW_TYPE
    {
        IMAGE, VIDEO, NONE, FLASH
    };

    public FileModelData()
    {
        super();

        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public String getPreviewPath()
    {
        return addSuffix(getFullPath(), "_preview");
    }

    private String getFullPath()
    {
        return getLocation() + "/" + getId() + "_" + getFilename();
    }

    public String getPreviewMoviePath()
    {
        return addSuffix(getFullPath(), "_low");
    }

    private static String addSuffix(String path, String suffix)
    {
        String ext = "";
        // TODO get last index of
        int extPos = path.indexOf(".");
        if (extPos != -1)
        {
            ext = path.substring(extPos);
            path = path.replaceFirst(ext, suffix + ext);
        }
        else
        {
            path = path + suffix;
        }
        return path;
    }

    @Override
    public String getName()
    {
        return get(FILE_NAME);
    }

    @Override
    public void setName(String name)
    {
        set(FILE_NAME, name);
    }

    public String getFilename()
    {
        return get(FILE_NAME);
    }

    public void setFilename(String filename)
    {
        set(FILE_NAME, filename);
    };

    public void setTags(List<Long> tags)
    {
        set(FILE_TAGS, tags);
        /*
         * remove( FILE_TAGS + "_string" ); set( FILE_TAGS, tags );
         * 
         * set( FILE_TAGS, Integer.toString( tags.size() ) ); String arrayStr = "["; String sep = ""; for ( Long tag :
         * tags) { arrayStr += sep + Long.toString( tag ); sep = ","; } arrayStr += "]"; //remove( FILE_TAGS + "_string"
         * ); set( FILE_TAGS + "_string", tags );
         */
    }

    public List<Long> getTags()
    {
        return get(FILE_TAGS);
        /*
         * String allTags = get( FILE_TAGS + "_string" ); allTags = allTags.replaceAll( "\\[", "" ); allTags =
         * allTags.replaceAll( "\\]", "" );
         * 
         * List<Long> ret = new ArrayList<Long>(); if ( allTags.length() > 0 ) { String allTagsStr[] = allTags.split(
         * "," ); for ( String s : allTagsStr ) { ret.add( Long.valueOf( s ) ); } } return ret;
         */
    }

    @Override
    public String getPath()
    {
        return get(FILE_PATH);
    }

    public void setPath(String path)
    {
        set(FILE_PATH, path);
    };

    public String getFormat()
    {
        return get(FILE_FORMAT);
    }

    public void setFileFormat(String format)
    {
        set(FILE_FORMAT, format);
    };

    public String getSize()
    {
        return get(FILE_SIZE);
    }

    public void setSize(String size)
    {
        set(FILE_SIZE, size);
    };

    public String setLocation(String location)
    {
        return set(FILE_LOCATION, location);
    }

    public String getLocation()
    {
        return get(FILE_LOCATION);
    }

    public static ModelType getModelType()
    {
        ModelType modelType = RecordModelData.getModelType();

        modelType.addField(FILE_LOCATION);
        modelType.addField(FILE_NAME);
        modelType.addField(FILE_PATH);
        modelType.addField(FILE_FORMAT);

        DataField size = new DataField(FILE_SIZE);
        size.setType(Long.class);
        modelType.addField(size);

        DataField tags = new DataField(FILE_TAGS);
        tags.setType(List.class);
        modelType.addField(tags);

        return modelType;
    }

    public boolean canShowPreview()
    {
        return getPreviewType() != PREVIEW_TYPE.NONE;
    }

    public PREVIEW_TYPE getPreviewType()
    {
        String filename = getFilename().toLowerCase();

        // TODO to be based on model data
        if (filename.contains(".jpg") || filename.contains(".jpeg") || filename.contains(".png")
                || filename.contains(".gif") || filename.contains(".bmp") || filename.contains(".exf"))
        {
            return PREVIEW_TYPE.IMAGE;
        }
        else if (filename.contains(".mov") || filename.contains(".avi") || filename.contains(".mpg")
                || filename.contains(".anim") || filename.contains(".mpeg") || filename.contains(".flv")
                || filename.contains(".m2v") || filename.contains(".mp4") || filename.contains(".mkv"))
        {
            return PREVIEW_TYPE.VIDEO;
        }
        else if (filename.contains(".swf") || filename.contains(".fla"))
        {
            return PREVIEW_TYPE.FLASH;
        }
        return PREVIEW_TYPE.NONE;
    }
}
