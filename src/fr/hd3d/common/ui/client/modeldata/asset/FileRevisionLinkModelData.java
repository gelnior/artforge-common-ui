package fr.hd3d.common.ui.client.modeldata.asset;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;


public class FileRevisionLinkModelData extends RecordModelData
{
    private static final long serialVersionUID = 4878719229370904938L;

    public static final String INPUT_FILE_FIELD = "inputFile";
    public static final String OUTPUT_FILE_FIELD = "outputFile";

    public static final String SIMPLE_CLASS_NAME = "FileRevisionLink";
    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LFileRevisionLink";

    public static ModelType getModelType()
    {
        ModelType modelType = Hd3dModelData.getModelType();

        DataField inputFileField = new DataField(INPUT_FILE_FIELD);
        inputFileField.setType(Long.class);

        DataField outputFileField = new DataField(OUTPUT_FILE_FIELD);
        outputFileField.setType(Long.class);

        modelType.addField(inputFileField);
        modelType.addField(outputFileField);

        return modelType;
    }

    public FileRevisionLinkModelData()
    {
        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public Long getInputFile()
    {
        return this.get(INPUT_FILE_FIELD);
    }

    public void setInputFile(Long fileId)
    {
        this.set(INPUT_FILE_FIELD, fileId);
    }

    public Long getOutputFile()
    {
        return this.get(OUTPUT_FILE_FIELD);
    }

    public void setOutputFile(Long fileId)
    {
        this.set(OUTPUT_FILE_FIELD, fileId);
    }

    public void setInputFile(FileRevisionModelData file)
    {
        this.setInputFile(file.getId());
    }

    public void setOutputFile(FileRevisionModelData file)
    {
        this.setOutputFile(file.getId());
    }
}
