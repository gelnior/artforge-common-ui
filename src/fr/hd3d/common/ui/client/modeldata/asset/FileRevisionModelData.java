package fr.hd3d.common.ui.client.modeldata.asset;

import java.sql.Date;
import java.util.List;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.client.enums.EFileState;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.util.field.ListDataField;
import fr.hd3d.common.ui.client.util.field.LongDataField;


/**
 * GXT object representation of File Revision.
 * 
 * @author HD3D
 */
public class FileRevisionModelData extends RecordModelData
{
    private static final long serialVersionUID = 4155371902508531457L;

    public static final String SIMPLE_CLASS_NAME = "FileRevision";
    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LFileRevision";

    public static final String LOCKED_STATUS = "LOCKED";
    public static final String UNLOCKED_STATUS = "UNLOCKED";

    public static final String CREATION_DATE_FIELD = "creationDate";
    public static final String CREATOR_NAME_FIELD = "creatorName";
    public static final String KEY_FIELD = "key";
    public static final String VARIATION_FIELD = "variation";
    public static final String REVISION_FIELD = "revision";
    public static final String CREATOR_FIELD = "creator";
    public static final String FORMAT_FIELD = "format";
    public static final String SIZE_FIELD = "size";
    public static final String CHECKSUM_FIELD = "checksum";
    public static final String LOCATION_FIELD = "location";
    public static final String RELATIVE_PATH_FIELD = "relativePath";
    public static final String STATUS_FIELD = "status";
    public static final String LAST_OPERATION_DATE_FIELD = "lastOperationDate";
    public static final String LAST_USER_FIELD = "lastUser";
    public static final String LAST_USER_NAME_FIELD = "lastUserName";
    public static final String COMMENT_FIELD = "comment";
    public static final String VALIDITY_FIELD = "validity";
    public static final String SEQUENCE_FIELD = "sequence";
    public static final String STATE_FIELD = "state";
    public static final String TASKTYPE_NAME_FIELD = "taskTypeName";
    public static final String MOUNT_POINT_FIELD = "mountPoint";
    public static final String PROXIES_FIELD = "proxies";

    public static final String ASSET_REVISION_FIELD = "assetRevision";

    public FileRevisionModelData()
    {
        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);

        this.setState(EFileState.getDefault().name());
    }

    @Override
    public String getName()
    {
        return this.get(KEY_FIELD);// FIXME strange ...
    }

    @Override
    public void setName(String name)
    {
        this.set(NAME_FIELD, name);
    }

    public Date getCreationDate()
    {
        return this.get(CREATION_DATE_FIELD);
    }

    public void setCreationDate(Date creationDate)
    {
        this.set(CREATION_DATE_FIELD, creationDate);
    }

    public String getKey()
    {
        return this.get(KEY_FIELD);
    }

    public void setKey(String key)
    {
        this.set(KEY_FIELD, key);
    }

    public String getVariation()
    {
        return this.get(VARIATION_FIELD);
    }

    public void setVariation(String variation)
    {
        this.set(VARIATION_FIELD, variation);
    }

    public Integer getRevision()
    {
        return this.get(REVISION_FIELD);
    }

    public void setRevision(Integer revision)
    {
        this.set(REVISION_FIELD, revision);
    }

    public Long getCreator()
    {
        return this.get(CREATOR_FIELD);
    }

    public void setCreator(Long creator)
    {
        this.set(CREATOR_FIELD, creator);
    }

    public String getCreatorName()
    {
        return (String) get(CREATOR_NAME_FIELD);
    }

    public void setCreatorName(String name)
    {
        set(CREATOR_NAME_FIELD, name);
    }

    public String getFormat()
    {
        return this.get(FORMAT_FIELD);
    }

    public void setFormat(String format)
    {
        this.set(FORMAT_FIELD, format);
    }

    public Long getSize()
    {
        return this.get(SIZE_FIELD);
    }

    public void setSize(Long size)
    {
        this.set(SIZE_FIELD, size);
    }

    public String getChecksum()
    {
        return this.get(CHECKSUM_FIELD);
    }

    public void setChecksum(String checksum)
    {
        this.set(CHECKSUM_FIELD, checksum);
    }

    public String getLocation()
    {
        return this.get(LOCATION_FIELD);
    }

    public void setLocation(String location)
    {
        this.set(LOCATION_FIELD, location);
    }

    public String getRelativePath()
    {
        return this.get(RELATIVE_PATH_FIELD);
    }

    public void setRelativePath(String relativePath)
    {
        this.set(RELATIVE_PATH_FIELD, relativePath);
    }

    public String getStatus()
    {
        return this.get(STATUS_FIELD);
    }

    public void setStatus(String status)
    {
        this.set(STATUS_FIELD, status);
    }

    public Date getLastOperationDate()
    {
        return this.get(LAST_OPERATION_DATE_FIELD);
    }

    public void setLastOperationDate(Date lastOperationDate)
    {
        this.set(LAST_OPERATION_DATE_FIELD, lastOperationDate);
    }

    public Long getLastUser()
    {
        return this.get(LAST_USER_FIELD);
    }

    public void setLastUser(Long lastUser)
    {
        this.set(LAST_USER_FIELD, lastUser);
    }

    public String getLastUserName()
    {
        return (String) get(LAST_USER_NAME_FIELD);
    }

    public String getComment()
    {
        return this.get(COMMENT_FIELD);
    }

    public void setComment(String comment)
    {
        this.set(VALIDITY_FIELD, comment);
    }

    public String getValidity()
    {
        return this.get(VALIDITY_FIELD);
    }

    public void setValidity(String validity)
    {
        this.set(VALIDITY_FIELD, validity);
    }

    public String getSequence()
    {
        return this.get(SEQUENCE_FIELD);
    }

    public void setSequence(String sequence)
    {
        this.set(SEQUENCE_FIELD, sequence);
    }

    public Long getAssetRevision()
    {
        return (Long) get(ASSET_REVISION_FIELD);
    }

    public void setAssetRevision(Long assetRevision)
    {
        set(ASSET_REVISION_FIELD, assetRevision);
    }

    public String getState()
    {
        return get(STATE_FIELD);
    }

    public void setState(String state)
    {
        set(STATE_FIELD, state);
    }

    public String getTaskTypeName()
    {
        return get(TASKTYPE_NAME_FIELD);
    }

    public List<Long> getProxies()
    {
        return get(PROXIES_FIELD);
    }

    public void setProxies(List<Long> proxiesIds)
    {
        set(PROXIES_FIELD, proxiesIds);
    }

    public static ModelType getModelType()
    {
        ModelType md = Hd3dModelData.getModelType();

        DataField revisionField = new DataField(REVISION_FIELD);
        revisionField.setType(Integer.class);

        DataField creationDateField = new DataField(CREATION_DATE_FIELD);
        creationDateField.setType(Date.class);

        DataField creatorField = new DataField(CREATOR_FIELD);
        creatorField.setType(Long.class);

        DataField sizeField = new DataField(SIZE_FIELD);
        sizeField.setType(Long.class);

        DataField lastOperationDateField = new DataField(LAST_OPERATION_DATE_FIELD);
        lastOperationDateField.setType(Date.class);

        DataField lastUserField = new DataField(LAST_USER_FIELD);
        lastUserField.setType(Long.class);

        md.addField(creationDateField);
        md.addField(KEY_FIELD);
        md.addField(VARIATION_FIELD);
        md.addField(revisionField);
        md.addField(creatorField);
        md.addField(CREATOR_NAME_FIELD);
        md.addField(FORMAT_FIELD);
        md.addField(sizeField);
        md.addField(CHECKSUM_FIELD);
        md.addField(LOCATION_FIELD);
        md.addField(RELATIVE_PATH_FIELD);
        md.addField(STATUS_FIELD);
        md.addField(lastOperationDateField);
        md.addField(lastUserField);
        md.addField(LAST_USER_NAME_FIELD);
        md.addField(COMMENT_FIELD);
        md.addField(VALIDITY_FIELD);
        md.addField(SEQUENCE_FIELD);
        md.addField(new LongDataField(ASSET_REVISION_FIELD));
        md.addField(STATE_FIELD);
        md.addField(TASKTYPE_NAME_FIELD);
        md.addField(MOUNT_POINT_FIELD);
        md.addField(new ListDataField(PROXIES_FIELD));

        return md;
    }

    public Boolean isLocked()
    {
        return FileRevisionModelData.LOCKED_STATUS.equals(this.getStatus());
    }

    // public void deleteLink(RecordModelData asset)
    // {
    // String url = ServicesPath.ASSET_REVISION_FILE_REVISION + asset.getId() + "-" + this.getId();
    //
    // IRestRequestHandler handler = RestRequestHandlerSingleton.getInstance();
    // handler.deleteRequest(url, new BaseCallback() {
    // @Override
    // protected void onSuccess(Request request, Response response)
    // {
    //
    // }
    // });
    // }

    public void deleteUpLink(FileRevisionModelData file, AppEvent event)
    {
        deleteLink(file, this, event);
    }

    public void deleteDownLink(FileRevisionModelData file, AppEvent event)
    {
        deleteLink(this, file, event);
    }

    /**
     * Delete link between <i>fileFrom</i> and <i>fileTo</i>.
     * 
     * @param fileFrom
     *            Input File
     * @param fileTo
     *            Output file
     * @param event
     *            The event to dispatch when delete is complete.
     */
    public static void deleteLink(final FileRevisionModelData fileFrom, final FileRevisionModelData fileTo,
            final AppEvent event)
    {
        String url = ServicesPath.FILE_REVISION_LINKS + ServicesPath.INPUT + fileFrom.getId() + "/"
                + ServicesPath.OUTPUT + fileTo.getId();

        IRestRequestHandler handler = RestRequestHandlerSingleton.getInstance();
        handler.deleteRequest(url, new BaseCallback() {
            @Override
            protected void onSuccess(Request request, Response response)
            {
                EventDispatcher.forwardEvent(event);
            }
        });
    }

    public void deleteAssetLink()
    {
        setAssetRevision(null);
        save();
    }

}
