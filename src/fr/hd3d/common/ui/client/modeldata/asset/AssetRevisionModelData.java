package fr.hd3d.common.ui.client.modeldata.asset;

import java.util.ArrayList;
import java.util.List;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.data.ModelType;
import com.extjs.gxt.ui.client.event.LoadListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.reader.AssetRevisionGroupReader;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.service.callback.GetModelDataCallback;
import fr.hd3d.common.ui.client.service.callback.PostModelDataCallback;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.Constraints;
import fr.hd3d.common.ui.client.service.parameter.EqConstraint;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.util.field.LongDataField;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;


public class AssetRevisionModelData extends RecordModelData
{
    private static final long serialVersionUID = 4878719229370904938L;

    public static final String KEY_FIELD = "key";
    public static final String REVISION_FIELD = "revision";
    public static final String CREATOR_FIELD = "creator";
    public static final String CREATOR_NAME_FIELD = "creatorName";
    public static final String VARIATION_FIELD = "variation";
    public static final String TASK_TYPE_ID_FIELD = "taskType";
    public static final String TASK_TYPE_NAME_FIELD = "taskTypeName";
    public static final String TASK_TYPE_COLOR_FIELD = "taskTypeColor";
    public static final String STATUS_FIELD = "status";
    public static final String SINCE_FIELD = "since";
    public static final String LAST_USER_FIELD = "lastUser";
    public static final String LAST_USER_NAME_FIELD = "lastUserName";
    public static final String COMMENT_FIELD = "comment";
    public static final String VALIDITY_FIELD = "validity";
    public static final String GROUP_IDS_FIELD = "assetRevisionGroups";
    public static final String WIP_PATH_FIELD = "wipPath";
    public static final String PUBLISH_PATH_FIELD = "publishPath";
    public static final String OLD_PATH_FIELD = "oldPath";

    public static final String MOUNT_POINT_WIP_PATH_FIELD = "mountPointWipPath";
    public static final String MOUNT_POINT_PUBLISH_PATH_FIELD = "mountPointPublishPath";
    public static final String MOUNT_POINT_OLD_PATH_FIELD = "mountPointOldPath";

    public static final String WORKOBJECT_PATH_FIELD = "workObjectPath";
    public static final String WORKOBJECT_NAME_FIELD = "workObjectName";
    public static final String WORKOBJECT_ID_FIELD = "workObjectId";

    public static final String FILE_REVISIONS_FIELD = "fileRevisions";

    public static final String SIMPLE_CLASS_NAME = "AssetRevision";
    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LAssetRevision";

    public static final String WORK_OBJECT_CLASS_NAME_FIELD = "workObjectClassName";

    public static ModelType getModelType()
    {
        ModelType modelType = RecordModelData.getModelType();

        DataField revisionField = new DataField(REVISION_FIELD);
        revisionField.setType(Integer.class);

        DataField creatorField = new DataField(CREATOR_FIELD);
        creatorField.setType(Long.class);

        DataField lastUserField = new DataField(LAST_USER_FIELD);
        lastUserField.setType(Long.class);

        DataField groupIdsDataField = new DataField(GROUP_IDS_FIELD);
        groupIdsDataField.setType(List.class);
        groupIdsDataField.setFormat(AssetRevisionGroupModelData.SIMPLE_CLASS_NAME);

        modelType.addField(KEY_FIELD);
        modelType.addField(VARIATION_FIELD);
        modelType.addField(new EnumField(STATUS_FIELD));
        modelType.addField(revisionField);
        modelType.addField(COMMENT_FIELD);
        modelType.addField(creatorField);
        modelType.addField(CREATOR_NAME_FIELD);
        modelType.addField(new LongDataField(TASK_TYPE_ID_FIELD));
        modelType.addField(TASK_TYPE_COLOR_FIELD);
        modelType.addField(TASK_TYPE_NAME_FIELD);
        modelType.addField(SINCE_FIELD);
        modelType.addField(lastUserField);
        modelType.addField(LAST_USER_NAME_FIELD);
        modelType.addField(VALIDITY_FIELD);
        modelType.addField(groupIdsDataField);
        modelType.addField(WIP_PATH_FIELD);
        modelType.addField(PUBLISH_PATH_FIELD);
        modelType.addField(OLD_PATH_FIELD);
        modelType.addField(MOUNT_POINT_WIP_PATH_FIELD);
        modelType.addField(MOUNT_POINT_PUBLISH_PATH_FIELD);
        modelType.addField(MOUNT_POINT_OLD_PATH_FIELD);
        modelType.addField(WORKOBJECT_PATH_FIELD);
        modelType.addField(WORKOBJECT_NAME_FIELD);
        modelType.addField(WORKOBJECT_ID_FIELD);

        DataField fileRevisionsDataField = new DataField(FILE_REVISIONS_FIELD);
        fileRevisionsDataField.setType(List.class);
        fileRevisionsDataField.setFormat(FileRevisionModelData.SIMPLE_CLASS_NAME);
        modelType.addField(fileRevisionsDataField);

        return modelType;
    }

    protected List<AssetRevisionModelData> predecessors;
    protected List<AssetRevisionModelData> successors;

    protected int x = 0;
    protected int y = 0;

    public AssetRevisionModelData()
    {
        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    @Override
    public String getName()
    {
        if (get(NAME_FIELD).equals(""))
        {
            return this.getWorkObjectName() + " => " + this.getTaskTypeName() + " (" + this.getVariation() + ")";
        }
        return get(NAME_FIELD);
    }

    public String getKey()
    {
        return this.get(KEY_FIELD);
    }

    public void setKey(String key)
    {
        this.set(KEY_FIELD, key);
    }

    public Integer getRevision()
    {
        return this.get(REVISION_FIELD);
    }

    public void setRevision(Integer revision)
    {
        this.set(REVISION_FIELD, revision);
    }

    public Long getCreator()
    {
        return (Long) get(CREATOR_FIELD);
    }

    public void setCreator(Long id)
    {
        set(CREATOR_FIELD, id);
    }

    public String getCreatorName()
    {
        return (String) get(CREATOR_NAME_FIELD);
    }

    public void setCreatorName(String name)
    {
        set(CREATOR_NAME_FIELD, name);
    }

    public String getVariation()
    {
        return (String) get(VARIATION_FIELD);
    }

    public void setVariation(String value)
    {
        set(VARIATION_FIELD, value);
    }

    public Long getTaskTypeId()
    {
        return get(TASK_TYPE_ID_FIELD);
    }

    public void setTaskTypeId(Long id)
    {
        set(TASK_TYPE_ID_FIELD, id);
    }

    public String getTaskTypeName()
    {
        return get(TASK_TYPE_NAME_FIELD);
    }

    public void setTaskTypeName(String name)
    {
        set(TASK_TYPE_NAME_FIELD, name);
    }

    public String getTaskTypeColor()
    {
        return get(TASK_TYPE_COLOR_FIELD);
    }

    public void setTaskTypeColor(String color)
    {
        set(TASK_TYPE_COLOR_FIELD, color);
    }

    public String getStatus()
    {
        return (String) get(STATUS_FIELD);
    }

    public void setStatus(String value)
    {
        set(STATUS_FIELD, value);
    }

    public String getSince()
    {
        return (String) get(SINCE_FIELD);
    }

    public void setSince(String value)
    {
        set(SINCE_FIELD, value);
    }

    public Long getLastUser()
    {
        return get(LAST_USER_FIELD);
    }

    public void setLastUser(Long id)
    {
        set(LAST_USER_FIELD, id);
    }

    public String getLastUserName()
    {
        return (String) get(LAST_USER_NAME_FIELD);
    }

    public void setLastUserName(String name)
    {
        set(LAST_USER_NAME_FIELD, name);
    }

    public String getComment()
    {
        return (String) get(COMMENT_FIELD);
    }

    public void setComment(String value)
    {
        set(COMMENT_FIELD, value);
    }

    public String getValidity()
    {
        return (String) get(VALIDITY_FIELD);
    }

    public void setValidity(String value)
    {
        set(VALIDITY_FIELD, value);
    }

    public List<Long> getAssetGroupIDs()
    {
        return get(GROUP_IDS_FIELD);
    }

    public void setAssetGroupIDs(List<Long> groupIds)
    {
        this.set(GROUP_IDS_FIELD, groupIds);
    }

    public String getWipPath()
    {
        return get(WIP_PATH_FIELD);
    }

    public void setWipPath(String wipPath)
    {
        set(WIP_PATH_FIELD, wipPath);
    }

    public String getPublishPath()
    {
        return get(PUBLISH_PATH_FIELD);
    }

    public void setPublishPath(String publishPath)
    {
        set(PUBLISH_PATH_FIELD, publishPath);
    }

    public String getOldPath()
    {
        return get(OLD_PATH_FIELD);
    }

    public void setOldPath(String oldPath)
    {
        set(OLD_PATH_FIELD, oldPath);
    }

    public String getWorkObjectPath()
    {
        return get(WORKOBJECT_PATH_FIELD);
    }

    public String getWorkObjectName()
    {
        return get(WORKOBJECT_NAME_FIELD);
    }

    public Long getWorkObjectId()
    {
        return (Long) get(WORKOBJECT_ID_FIELD);
    }

    public List<Long> getFileRevisions()
    {
        return get(FILE_REVISIONS_FIELD);
    }

    public List<AssetRevisionModelData> getPredecessors()
    {
        if (predecessors == null)
        {
            predecessors = new ArrayList<AssetRevisionModelData>();
        }
        return predecessors;
    }

    public List<AssetRevisionModelData> getSuccessors()
    {
        if (successors == null)
        {
            successors = new ArrayList<AssetRevisionModelData>();
        }
        return successors;
    }

    public int getX()
    {
        return x;
    }

    public int getY()
    {
        return y;
    }

    public void setX(int x)
    {
        this.x = x;
    }

    public void setY(int y)
    {
        this.y = y;
    }

    /**
     * Delete upstream link between <i>this</i> and <i>asset</i>.
     * 
     * @param asset
     *            The linked asset.
     * @param event
     *            The event to dispatch when delete is complete.
     */
    public void deleteUpLink(final AssetRevisionModelData asset, final AppEvent event)
    {
        deleteLink(asset, this, event);
    }

    /**
     * Delete downstream link between <i>this</i> and <i>asset</i>.
     * 
     * @param asset
     *            The linked asset.
     * @param event
     *            The event to dispatch when delete is complete.
     */
    public void deleteDownLink(final AssetRevisionModelData asset, final AppEvent event)
    {
        deleteLink(this, asset, event);
    }

    /**
     * Delete link between <i>assetFrom</i> and <i>assetTo</i>.
     * 
     * @param assetFrom
     *            Input asset
     * @param assetTo
     *            Output asset
     * @param event
     *            The event to dispatch when delete is complete.
     */
    public static void deleteLink(final AssetRevisionModelData assetFrom, final AssetRevisionModelData assetTo,
            final AppEvent event)
    {
        String url = ServicesPath.ASSET_REVISION_LINKS + assetFrom.getId() + "-" + assetTo.getId();

        IRestRequestHandler handler = RestRequestHandlerSingleton.getInstance();
        handler.deleteRequest(url, new BaseCallback() {
            @Override
            protected void onSuccess(Request request, Response response)
            {
                EventDispatcher.forwardEvent(event);
            }
        });
    }

    public void removeFromWorkObject(RecordModelData workObject, AppEvent event)
    {
        final AssetRevisionGroupModelData group = new AssetRevisionGroupModelData();

        Constraints constraints = new Constraints();
        constraints.add(new Constraint(EConstraintOperator.in, Hd3dModelData.ID_FIELD, this.getAssetGroupIDs(), null));
        constraints.add(new Constraint(EConstraintOperator.eq, AssetRevisionGroupModelData.CRITERIA_FIELD, workObject
                .getSimpleClassName()));
        constraints.add(new Constraint(EConstraintOperator.eq, AssetRevisionGroupModelData.VALUE_FIELD, workObject
                .getId().toString()));

        group.refreshWithParams(null, constraints, new GetModelDataCallback(group, event) {
            @Override
            protected void onRecordReturned(List<Hd3dModelData> records)
            {
                Hd3dModelData newRecord = records.get(0);
                this.record.updateFromModelData(newRecord);

                getAssetGroupIDs().remove(this.record.getId());
                save(event);
            }
        });
    }

    public void addToWorkObject(final RecordModelData workObject, final AppEvent eventToForward)
    {
        final ServiceStore<AssetRevisionGroupModelData> groupStore = new ServiceStore<AssetRevisionGroupModelData>(
                new AssetRevisionGroupReader());

        this.set(WORK_OBJECT_CLASS_NAME_FIELD, workObject.getSimpleClassName());
        Constraints constraints = new Constraints();
        constraints.add(new EqConstraint(AssetRevisionGroupModelData.CRITERIA_FIELD, workObject.getSimpleClassName()));
        constraints.add(new EqConstraint(AssetRevisionGroupModelData.VALUE_FIELD, workObject.getId().toString()));
        groupStore.addParameter(constraints);
        groupStore.addLoadListener(new LoadListener() {
            @Override
            public void loaderLoad(LoadEvent le)
            {
                if (CollectionUtils.isNotEmpty(groupStore.getModels()))
                {
                    AssetRevisionGroupModelData group = groupStore.getAt(0);
                    addGroup(group);
                    save(eventToForward);
                }
                else
                {
                    final AssetRevisionGroupModelData group = new AssetRevisionGroupModelData();
                    group.setName(workObject.getName().toLowerCase() + "_grp");
                    group.setCriteria(workObject.getSimpleClassName());
                    group.setValue(workObject.getId().toString());
                    group.setProjectId(MainModel.currentProject.getId());
                    group.save(new PostModelDataCallback(group, null) {
                        @Override
                        protected void onSuccess(Request request, Response response)
                        {
                            this.setNewId(response);
                            addGroup(group);
                            save(eventToForward);
                        }
                    });
                }
            }
        });
        groupStore.reload();
    }

    protected void addGroup(AssetRevisionGroupModelData group)
    {
        if (getAssetGroupIDs() == null)
        {
            this.setAssetGroupIDs(new ArrayList<Long>());
        }

        getAssetGroupIDs().add(group.getId());
    }

    public static void createAndSaveAsset(RecordModelData workObject, TaskTypeModelData taskType, AppEvent event,
            String variation)
    {
        if (variation == null || variation.equals(""))
        {
            variation = "standard";
        }
        AssetRevisionModelData asset = new AssetRevisionModelData();
        asset.setName(taskType.getName() + " (" + variation + ")");
        asset.setKey(workObject.getName().toLowerCase() + "_" + taskType.getName().toLowerCase() + "_" + variation);
        asset.setRevision(1);
        asset.setVariation(variation);
        asset.setTaskTypeId(taskType.getId());
        asset.setCreator(MainModel.currentUser.getId());
        asset.setLastUser(MainModel.currentUser.getId());
        asset.setStatus(fr.hd3d.common.client.enums.EAssetStatus.UNVALIDATED.toString());

        asset.addToWorkObject(workObject, event);
    }

    public String getGraphName()
    {
        String name = this.getTaskTypeName() + ": " + this.getVariation();
        return name;
    }
}
