package fr.hd3d.common.ui.client.modeldata.asset;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;


public class AssetRevisionLinkModelData extends RecordModelData
{
    private static final long serialVersionUID = 4878719229370904938L;

    public static final String INPUT_ASSET_FIELD = "inputAsset";
    public static final String OUTPUT_ASSET_FIELD = "outputAsset";

    public static final String SIMPLE_CLASS_NAME = "AssetRevisionLink";
    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LAssetRevisionLink";

    public static ModelType getModelType()
    {
        ModelType modelType = Hd3dModelData.getModelType();

        DataField inputAssetField = new DataField(INPUT_ASSET_FIELD);
        inputAssetField.setType(Long.class);

        DataField outputAssetField = new DataField(OUTPUT_ASSET_FIELD);
        outputAssetField.setType(Long.class);

        modelType.addField(inputAssetField);
        modelType.addField(outputAssetField);

        return modelType;
    }

    public AssetRevisionLinkModelData()
    {
        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public Long getInputAsset()
    {
        return this.get(INPUT_ASSET_FIELD);
    }

    public void setInputAsset(Long assetId)
    {
        this.set(INPUT_ASSET_FIELD, assetId);
    }

    public Long getOutputAsset()
    {
        return this.get(OUTPUT_ASSET_FIELD);
    }

    public void setOutputAsset(Long assetId)
    {
        this.set(OUTPUT_ASSET_FIELD, assetId);
    }

    public void setInputAsset(AssetRevisionModelData asset)
    {
        this.setInputAsset(asset.getId());
    }

    public void setOutputAsset(AssetRevisionModelData asset)
    {
        this.setOutputAsset(asset.getId());
    }
}
