package fr.hd3d.common.ui.client.modeldata.asset;

import java.util.Date;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.client.ServicesURI;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.util.field.LongDataField;


/**
 * Model representing the class LDynamicPath.
 * 
 * @author HD3D
 * 
 */
public class ProxyModelData extends RecordModelData
{
    private static final long serialVersionUID = 118437662888877910L;

    public static final String SIMPLE_CLASS_NAME = "Proxy";
    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LProxy";

    public static final String FILEREVISION_ID_FIELD = "fileRevisionId";
    public static final String PROXYTYPE_ID_FIELD = "proxyTypeId";
    public static final String PROXYTYPE_NAME_FIELD = "proxyTypeName";
    public static final String LOCATION_FIELD = "location";
    public static final String FILENAME_FIELD = "filename";
    public static final String MOUNT_POINT_FIELD = "mountPoint";
    public static final String DESCRIPTION_FIELD = "description";
    public static final String PROXY_TYPE_OPEN_TYPE_FIELD = "proxyTypeOpenType";
    public static final String PROXY_FRAMERATE_FIELD = "framerate";

    /**
     * Default constructor.
     */
    public ProxyModelData()
    {
        super();

        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
        this.setFramerate(25);
    }

    public ProxyModelData(Long fileRevisionId, Long proxyTypeId, String location, String filename, String description,
            Integer framerate)
    {
        this();
        this.setFileRevisionId(fileRevisionId);
        this.setProxyTypeId(proxyTypeId);
        this.setProxyLocation(location);
        this.setFilename(filename);
        this.setDescription(description);
        this.setFramerate(framerate);
    }

    public void setFramerate(Integer framerate)
    {
        set(PROXY_FRAMERATE_FIELD, framerate);
    }

    public Integer getFramerate()
    {
        return get(PROXY_FRAMERATE_FIELD);
    }

    public String getDescription()
    {
        return get(DESCRIPTION_FIELD);
    }

    public void setDescription(String description)
    {
        set(DEFAULT_PATH_FIELD, description);
    }

    public String getProxyLocation()
    {
        return get(LOCATION_FIELD);
    }

    public void setProxyLocation(String location)
    {
        set(LOCATION_FIELD, location);
    }

    public Long getProxyTypeId()
    {
        return (Long) get(PROXYTYPE_ID_FIELD);
    }

    public void setProxyTypeId(Long proxyTypeId)
    {
        set(PROXYTYPE_ID_FIELD, proxyTypeId);
    }

    public Long getFileRevisionId()
    {
        return (Long) get(FILEREVISION_ID_FIELD);
    }

    public void setFileRevisionId(Long fileRevisionId)
    {
        set(FILEREVISION_ID_FIELD, fileRevisionId);
    }

    public String getMountPoint()
    {
        return get(MOUNT_POINT_FIELD);
    }

    public void setMountPoint(String mountPoint)
    {
        set(MOUNT_POINT_FIELD, mountPoint);
    }

    public String getFilename()
    {
        return get(FILENAME_FIELD);
    }

    public void setFilename(String filename)
    {
        set(FILENAME_FIELD, filename);
    }

    public String getProxyTypeOpenType()
    {
        return get(PROXY_TYPE_OPEN_TYPE_FIELD);
    }

    public void setProxyTypeOpenType(String openType)
    {
        set(PROXY_TYPE_OPEN_TYPE_FIELD, openType);
    }

    /**
     * @return Model type used by reader to know the record model data structure.
     */
    public static ModelType getModelType()
    {
        ModelType modelType = RecordModelData.getModelType();

        modelType.addField(new LongDataField(FILEREVISION_ID_FIELD));
        modelType.addField(new LongDataField(PROXYTYPE_ID_FIELD));
        modelType.addField(PROXYTYPE_NAME_FIELD);
        modelType.addField(LOCATION_FIELD);
        modelType.addField(DESCRIPTION_FIELD);
        modelType.addField(MOUNT_POINT_FIELD);
        modelType.addField(FILENAME_FIELD);
        modelType.addField(PROXY_TYPE_OPEN_TYPE_FIELD);
        modelType.addField(PROXY_FRAMERATE_FIELD);

        return modelType;
    }

    /**
     * @return the full path of the proxy <b>mountPoint/location/filename</b>
     */
    public String getProxyPath()
    {
        String path = "";
        if (getMountPoint() != null && !"".equals(getMountPoint()))
        {
            path += getMountPoint();
            path = addPathSeparator(path);
        }
        if (getProxyLocation() != null && !"".equals(getProxyLocation()))
        {
            path += getProxyLocation();
            path = addPathSeparator(path);
        }
        if (getFilename() != null && !"".equals(getFilename()))
        {
            path += getFilename();
        }
        return path.replaceAll("\\\\", "/");
    }

    private String addPathSeparator(String path)
    {
        if (!path.endsWith("/"))
        {
            path += "/";
        }
        return path;
    }

    public String getDownloadPath()
    {
        return RestRequestHandlerSingleton.getInstance().getServicesUrl() + ServicesPath.getPath(SIMPLE_CLASS_NAME)
                + getId() + '/' + ServicesURI.DOWNLOAD + '?' + new Date().getTime();
    }
}
