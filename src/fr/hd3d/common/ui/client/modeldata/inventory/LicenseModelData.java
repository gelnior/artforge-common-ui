package fr.hd3d.common.ui.client.modeldata.inventory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;


public class LicenseModelData extends InventoryItemModelData
{
    /** Automatically generated serial version number. */
    private static final long serialVersionUID = 2633391861797367712L;

    public static final String NAME_FIELD = "name";
    public static final String NUMBER_FIELD = "number";
    public static final String TYPE_FIELD = "type";
    public static final String SOFTWARE_ID_FIELD = "softwareId";
    public static final String SOFTWARE_NAME_FIELD = "softwareName";
    public static final String COMPUTER_IDS_FIELD = "computerIDs";
    public static final String COMPUTER_NAMES_FIELD = "computerNames";

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LLicense";
    public static final String SIMPLE_CLASS_NAME = "License";

    /**
     * Default constructor : sets an empty device.
     */
    public LicenseModelData()
    {
        this.set(CLASS_NAME_FIELD, CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public LicenseModelData(String name, String serial, String billingReference, Date purchaseDate, Date warrantyEnd,
            String status, Long number, String type)
    {
        super(name, serial, billingReference, purchaseDate, warrantyEnd, status);

        this.setName(name);
        this.setNumber(number);
        this.setType(type);
        this.setComputerIDs(new ArrayList<Long>());
        this.setComputerNames(new ArrayList<String>());

        this.set(CLASS_NAME_FIELD, CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    @Override
    public String getName()
    {
        return this.get(NAME_FIELD);
    }

    @Override
    public void setName(String name)
    {
        this.set(NAME_FIELD, name);
    }

    public Long getNumber()
    {
        return this.get(NUMBER_FIELD);
    }

    public void setNumber(Long number)
    {
        this.set(NUMBER_FIELD, number);
    }

    public String getType()
    {
        return this.get(TYPE_FIELD);
    }

    public void setType(String type)
    {
        this.set(TYPE_FIELD, type);
    }

    public Long getSoftwareId()
    {
        return this.get(SOFTWARE_ID_FIELD);
    }

    public void setSoftwareId(Long id)
    {
        this.set(SOFTWARE_ID_FIELD, id);
    }

    public String getSoftwareName()
    {
        return this.get(SOFTWARE_NAME_FIELD);
    }

    public void setSoftwareName(String name)
    {
        this.set(SOFTWARE_NAME_FIELD, name);
    }

    public List<Long> getComputerIDs()
    {
        return get(COMPUTER_IDS_FIELD);
    }

    public void setComputerIDs(List<Long> computerIDs)
    {
        this.set(COMPUTER_IDS_FIELD, computerIDs);
    }

    public List<String> getComputerNames()
    {
        return get(COMPUTER_NAMES_FIELD);
    }

    public void setComputerNames(List<String> computerNames)
    {
        this.set(COMPUTER_NAMES_FIELD, computerNames);
    }

    /**
     * @return Model type used by reader to handle the device model data structure.
     */
    public static ModelType getModelType()
    {
        ModelType type = InventoryItemModelData.getModelType();

        DataField sizeDataField = new DataField(NUMBER_FIELD);
        sizeDataField.setType(Long.class);

        DataField softwareIdDataField = new DataField(SOFTWARE_ID_FIELD);
        softwareIdDataField.setType(Long.class);

        DataField computerIdsDataField = new DataField(COMPUTER_IDS_FIELD);
        computerIdsDataField.setType(List.class);
        computerIdsDataField.setFormat("Computer");

        DataField computerNamesDataField = new DataField(COMPUTER_NAMES_FIELD);
        computerNamesDataField.setType(List.class);

        type.addField(sizeDataField);
        type.addField(TYPE_FIELD);
        type.addField(softwareIdDataField);
        type.addField(SOFTWARE_NAME_FIELD);
        type.addField(computerIdsDataField);
        type.addField(computerNamesDataField);

        return type;
    }
}
