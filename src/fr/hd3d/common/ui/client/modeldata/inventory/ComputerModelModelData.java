package fr.hd3d.common.ui.client.modeldata.inventory;

import com.extjs.gxt.ui.client.data.ModelType;


public class ComputerModelModelData extends ModelModelData
{
    private static final long serialVersionUID = -1963958588078868011L;

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LComputerModel";
    public static final String SIMPLE_CLASS_NAME = "ComputerModel";

    public ComputerModelModelData()
    {
        super();

        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public ComputerModelModelData(Long id, String name)
    {
        super(id, name);

        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public ComputerModelModelData(String name)
    {
        super(null, name);

        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public static ModelType getModelType()
    {
        return ModelModelData.getModelType();
    }
}
