package fr.hd3d.common.ui.client.modeldata.inventory;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;


public class ModelModelData extends RecordModelData
{
    private static final long serialVersionUID = -1221124450649187506L;

    public static final String MANUFACTURER_ID_FIELD = "manufacturerId";
    public static final String MANUFACTURER_NAME_FIELD = "manufacturerName";

    public ModelModelData()
    {

    }

    public ModelModelData(Long id, String name)
    {
        this.setId(id);
        this.setName(name);
    }

    public Long getManufacturerId()
    {
        return this.get(MANUFACTURER_ID_FIELD);
    }

    public void setManufacturerId(Long manufacturerId)
    {
        this.set(MANUFACTURER_ID_FIELD, manufacturerId);
    }

    public String getManufacturerName()
    {
        return this.get(MANUFACTURER_NAME_FIELD);
    }

    public void setProcesorName(String manufacturerName)
    {
        this.set(MANUFACTURER_NAME_FIELD, manufacturerName);
    }

    /**
     * @return Model type used by reader to know the manufacturer model data structure.
     */
    public static ModelType getModelType()
    {
        ModelType type = RecordModelData.getModelType();

        DataField nameDataField = new DataField(NAME_FIELD);
        nameDataField.setType(String.class);
        DataField manufacturerIdDataField = new DataField(MANUFACTURER_ID_FIELD);
        nameDataField.setType(String.class);
        DataField manufacturerNameDataField = new DataField(MANUFACTURER_NAME_FIELD);
        nameDataField.setType(String.class);

        type.addField(nameDataField);
        type.addField(manufacturerIdDataField);
        type.addField(manufacturerNameDataField);

        return type;
    }
}
