package fr.hd3d.common.ui.client.modeldata.inventory;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;


public class ManufacturerModelData extends Hd3dModelData
{
    private static final long serialVersionUID = -1221124450649187506L;

    public static final String NAME_FIELD = "name";

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LManufacturer";
    public static final String SIMPLE_CLASS_NAME = "Manufacturer";

    public ManufacturerModelData()
    {
        this.setClassName(CLASS_NAME);
        this.simpleClassName = SIMPLE_CLASS_NAME;
    }

    public ManufacturerModelData(Long id, String name)
    {
        this.setId(id);
        this.setName(name);

        this.setClassName(CLASS_NAME);
        this.simpleClassName = SIMPLE_CLASS_NAME;
    }

    public ManufacturerModelData(String name)
    {
        this(null, name);
    }

    public String getName()
    {
        return this.get(NAME_FIELD);
    }

    public void setName(String name)
    {
        this.set(NAME_FIELD, name);
    }

    /**
     * @return Model type used by reader to know the manufacturer model data structure.
     */
    public static ModelType getModelType()
    {
        ModelType type = Hd3dModelData.getModelType();

        DataField nameDataField = new DataField(NAME_FIELD);
        nameDataField.setType(String.class);

        type.addField(nameDataField);

        return type;
    }
}
