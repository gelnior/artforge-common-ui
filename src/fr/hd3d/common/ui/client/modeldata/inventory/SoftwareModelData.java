package fr.hd3d.common.ui.client.modeldata.inventory;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;


public class SoftwareModelData extends RecordModelData
{
    /** Automatically generated serial version number. */
    private static final long serialVersionUID = -3079812751504934603L;

    public static final String LICENSE_IDS_FIELD = "licenseIDs";
    public static final String LICENSE_NAMES_FIELD = "licenseNames";
    public static final String COMPUTER_IDS_FIELD = "computerIDs";
    public static final String COMPUTER_NAMES_FIELD = "computerNames";

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LSoftware";
    public static final String SIMPLE_CLASS_NAME = "Software";

    /**
     * Default constructor : sets an empty software.
     */
    public SoftwareModelData()
    {
        this.set(CLASS_NAME_FIELD, CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public SoftwareModelData(String name)
    {
        super(null, name);

        this.setLicenseIDs(new ArrayList<Long>());
        this.setLicenseNames(new ArrayList<String>());
        this.setComputerIDs(new ArrayList<Long>());
        this.setComputerNames(new ArrayList<String>());

        this.set(CLASS_NAME_FIELD, CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public List<Long> getLicenseIDs()
    {
        return get(LICENSE_IDS_FIELD);
    }

    public void setLicenseIDs(List<Long> licenseIDs)
    {
        this.set(LICENSE_IDS_FIELD, licenseIDs);
    }

    public List<String> getLicenseNames()
    {
        return get(LICENSE_NAMES_FIELD);
    }

    public void setLicenseNames(List<String> licenseNames)
    {
        this.set(LICENSE_NAMES_FIELD, licenseNames);
    }

    public List<Long> getComputerIDs()
    {
        return get(COMPUTER_IDS_FIELD);
    }

    public void setComputerIDs(List<Long> computerIDs)
    {
        this.set(COMPUTER_IDS_FIELD, computerIDs);
    }

    public List<String> getComputerNames()
    {
        return get(COMPUTER_NAMES_FIELD);
    }

    public void setComputerNames(List<String> computerNames)
    {
        this.set(COMPUTER_NAMES_FIELD, computerNames);
    }

    /**
     * @return Model type used by reader to handle the software model data structure.
     */
    public static ModelType getModelType()
    {
        ModelType type = RecordModelData.getModelType();

        DataField licenseIdsDataField = new DataField(LICENSE_IDS_FIELD);
        licenseIdsDataField.setType(List.class);
        licenseIdsDataField.setFormat("License");

        DataField licenseNamesDataField = new DataField(LICENSE_NAMES_FIELD);
        licenseNamesDataField.setType(List.class);

        DataField computerIdsDataField = new DataField(COMPUTER_IDS_FIELD);
        computerIdsDataField.setType(List.class);
        computerIdsDataField.setFormat("Computer");

        DataField computerNamesDataField = new DataField(COMPUTER_NAMES_FIELD);
        computerNamesDataField.setType(List.class);

        type.addField(licenseIdsDataField);
        type.addField(licenseNamesDataField);
        type.addField(computerIdsDataField);
        type.addField(computerNamesDataField);

        return type;
    }
}
