package fr.hd3d.common.ui.client.modeldata.inventory;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;


public class DeviceModelData extends InventoryItemModelData
{
    /** Automatically generated serial version number. */
    private static final long serialVersionUID = 2633391861797367712L;

    public static final String SIZE_FIELD = "size";
    public static final String DEVICE_TYPE_ID_FIELD = "deviceTypeId";
    public static final String DEVICE_TYPE_NAME_FIELD = "deviceTypeName";
    public static final String DEVICE_MODEL_ID_FIELD = "deviceModelId";
    public static final String DEVICE_MODEL_NAME_FIELD = "deviceModelName";
    public static final String COMPUTER_IDS_FIELD = "computerIDs";
    public static final String COMPUTER_NAMES_FIELD = "computerNames";

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LDevice";
    public static final String SIMPLE_CLASS_NAME = "Device";

    /**
     * Default constructor : sets an empty device.
     */
    public DeviceModelData()
    {
        this.set(CLASS_NAME_FIELD, CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public DeviceModelData(String name, String serial, String billingReference, Date purchaseDate, Date warrantyEnd,
            String status, Double size)
    {
        super(name, serial, billingReference, purchaseDate, warrantyEnd, status);
        this.setSize(size);
        this.setComputerIDs(new ArrayList<Long>());
        this.setComputerNames(new ArrayList<String>());

        this.set(CLASS_NAME_FIELD, CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public Double getSize()
    {
        return this.get(DeviceModelData.SIZE_FIELD);
    }

    public void setSize(Double size)
    {
        this.set(DeviceModelData.SIZE_FIELD, size);
    }

    public List<Long> getComputerIDs()
    {
        return get(COMPUTER_IDS_FIELD);
    }

    public void setComputerIDs(List<Long> computerIDs)
    {
        this.set(COMPUTER_IDS_FIELD, computerIDs);
    }

    public List<String> getComputerNames()
    {
        return get(COMPUTER_NAMES_FIELD);
    }

    public void setComputerNames(List<String> computerNames)
    {
        this.set(COMPUTER_NAMES_FIELD, computerNames);
    }

    public Long getDeviceModelId()
    {
        return this.get(DEVICE_MODEL_ID_FIELD);
    }

    public void setDeviceModelId(Long deviceModelId)
    {
        this.set(DEVICE_MODEL_ID_FIELD, deviceModelId);
    }

    public String getDeviceModelName()
    {
        return this.get(DEVICE_MODEL_NAME_FIELD);
    }

    public void setDeviceModelName(String deviceModelName)
    {
        this.set(DEVICE_MODEL_NAME_FIELD, deviceModelName);
    }

    public Long getDeviceTypeId()
    {
        return this.get(DEVICE_TYPE_ID_FIELD);
    }

    public void setDeviceTypeId(Long deviceTypeId)
    {
        this.set(DEVICE_TYPE_ID_FIELD, deviceTypeId);
    }

    public String getDeviceTypeName()
    {
        return this.get(DEVICE_TYPE_NAME_FIELD);
    }

    public void setDeviceTypeName(String deviceTypeName)
    {
        this.set(DEVICE_TYPE_NAME_FIELD, deviceTypeName);
    }

    public void addComputer(ComputerModelData computer)
    {
        Set<Long> computerIDs = new HashSet<Long>(getComputerIDs());
        Set<String> computerNames = new HashSet<String>(getComputerNames());

        computerIDs.add(computer.getId());
        computerNames.add(computer.getName());

        setComputerIDs(new ArrayList<Long>(computerIDs));
        setComputerNames(new ArrayList<String>(computerNames));
    }

    public void removeComputer(ComputerModelData computer)
    {
        Set<Long> computerIDs = new HashSet<Long>(getComputerIDs());
        Set<String> computerNames = new HashSet<String>(getComputerNames());

        computerIDs.remove(computer.getId());
        computerNames.remove(computer.getName());

        setComputerIDs(new ArrayList<Long>(computerIDs));
        setComputerNames(new ArrayList<String>(computerNames));
    }

    /**
     * @return Model type used by reader to handle the device model data structure.
     */
    public static ModelType getModelType()
    {
        ModelType type = InventoryItemModelData.getModelType();

        DataField sizeDataField = new DataField(DeviceModelData.SIZE_FIELD);
        sizeDataField.setType(Double.class);

        DataField deviceModelIdDataField = new DataField(DEVICE_MODEL_ID_FIELD);
        deviceModelIdDataField.setType(Long.class);

        DataField deviceTypeIdDataField = new DataField(DEVICE_TYPE_ID_FIELD);
        deviceTypeIdDataField.setType(Long.class);

        DataField computerIdsDataField = new DataField(COMPUTER_IDS_FIELD);
        computerIdsDataField.setType(List.class);
        computerIdsDataField.setFormat("Computer");

        DataField computerNamesDataField = new DataField(COMPUTER_NAMES_FIELD);
        computerNamesDataField.setType(List.class);

        type.addField(sizeDataField);
        type.addField(deviceModelIdDataField);
        type.addField(DEVICE_MODEL_NAME_FIELD);
        type.addField(deviceTypeIdDataField);
        type.addField(DEVICE_TYPE_NAME_FIELD);
        type.addField(computerIdsDataField);
        type.addField(computerNamesDataField);

        return type;
    }
}
