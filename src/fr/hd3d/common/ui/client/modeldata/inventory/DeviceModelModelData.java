package fr.hd3d.common.ui.client.modeldata.inventory;


public class DeviceModelModelData extends ModelModelData
{
    private static final long serialVersionUID = -7751052959114195430L;
    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LDeviceModel";
    public static final String SIMPLE_CLASS_NAME = "DeviceModel";

    public DeviceModelModelData()
    {
        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public DeviceModelModelData(Long id, String name)
    {
        super(id, name);

        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public DeviceModelModelData(String name)
    {
        this(null, name);
    }
}
