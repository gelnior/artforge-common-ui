package fr.hd3d.common.ui.client.modeldata.inventory;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;


public class DeviceTypeModelData extends RecordModelData
{
    private static final long serialVersionUID = 9125368567678996812L;

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LDeviceType";
    public static final String SIMPLE_CLASS_NAME = "DeviceType";

    public DeviceTypeModelData()
    {
        this.setClassName(CLASS_NAME);
        this.simpleClassName = SIMPLE_CLASS_NAME;
    }

    public DeviceTypeModelData(Long id, String name)
    {
        this();

        this.setId(id);
        this.setName(name);
    }

    public DeviceTypeModelData(String name)
    {
        this(null, name);
    }

    /**
     * @return Model type used by reader to know the manufacturer model data structure.
     */
    public static ModelType getModelType()
    {
        ModelType type = RecordModelData.getModelType();

        DataField nameDataField = new DataField(NAME_FIELD);
        nameDataField.setType(String.class);

        type.addField(nameDataField);

        return type;
    }
}
