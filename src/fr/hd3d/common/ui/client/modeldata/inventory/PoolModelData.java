package fr.hd3d.common.ui.client.modeldata.inventory;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;


public class PoolModelData extends Hd3dModelData
{
    /** Automatically generated serial version number. */
    private static final long serialVersionUID = -5557795790888008500L;

    public static final String NAME_FIELD = "name";
    public static final String IS_DISPATCHER_ALLOWED_FIELD = "isDispatcherAllowed";
    public static final String COMPUTER_IDS_FIELD = "computerIDs";
    public static final String COMPUTER_NAMES_FIELD = "computerNames";

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LPool";
    public static final String SIMPLE_CLASS_NAME = "Pool";

    /**
     * Default constructor : sets an empty studio map.
     */
    public PoolModelData()
    {
        this.setComputerIDs(new ArrayList<Long>());
        this.setComputerNames(new ArrayList<String>());

        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public PoolModelData(String name, Boolean isDispatcherAllowed)
    {
        this();

        this.setName(name);
        this.setIsDispatcherAllowed(isDispatcherAllowed);
    }

    public String getName()
    {
        return this.get(NAME_FIELD);
    }

    public void setName(String name)
    {
        this.set(NAME_FIELD, name);
    }

    public Boolean getIsDispatcherAllowed()
    {
        return this.get(IS_DISPATCHER_ALLOWED_FIELD);
    }

    public void setIsDispatcherAllowed(Boolean isDispatcherAllowed)
    {
        this.set(IS_DISPATCHER_ALLOWED_FIELD, isDispatcherAllowed);
    }

    public List<Long> getComputerIDs()
    {
        return get(COMPUTER_IDS_FIELD);
    }

    public void setComputerIDs(List<Long> computerIDs)
    {
        this.set(COMPUTER_IDS_FIELD, computerIDs);
    }

    public List<String> getComputerNames()
    {
        return get(COMPUTER_NAMES_FIELD);
    }

    public void setComputerNames(List<String> computerNames)
    {
        this.set(COMPUTER_NAMES_FIELD, computerNames);
    }

    /**
     * @return Model type used by reader to handle the studio map model data structure.
     */
    public static ModelType getModelType()
    {
        ModelType type = Hd3dModelData.getModelType();

        DataField isDispatcherAllowedDataField = new DataField(IS_DISPATCHER_ALLOWED_FIELD);
        isDispatcherAllowedDataField.setType(Boolean.class);

        DataField computerIdsDataField = new DataField(COMPUTER_IDS_FIELD);
        computerIdsDataField.setType(List.class);
        computerIdsDataField.setFormat("Computer");

        DataField computerNamesDataField = new DataField(COMPUTER_NAMES_FIELD);
        computerNamesDataField.setType(List.class);

        type.addField(CLASS_NAME_FIELD);
        type.addField(NAME_FIELD);
        type.addField(isDispatcherAllowedDataField);
        type.addField(computerIdsDataField);
        type.addField(computerNamesDataField);

        return type;
    }
}
