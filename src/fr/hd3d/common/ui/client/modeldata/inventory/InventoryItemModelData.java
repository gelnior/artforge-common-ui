package fr.hd3d.common.ui.client.modeldata.inventory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;


public abstract class InventoryItemModelData extends RecordModelData
{
    private static final long serialVersionUID = -6052562688522816830L;

    public static final String SERIAL_FIELD = "serial";
    public static final String BILLING_REFERENCE_FIELD = "billingReference";
    public static final String PURCHASE_DATE_FIELD = "purchaseDate";
    public static final String WARRANTY_END_FIELD = "warrantyEnd";
    public static final String INVENTORY_STATUS_FIELD = "inventoryStatus";
    public static final String RESOURCEGROUP_IDS_FIELD = "resourceGroupIDs";
    public static final String RESOURCEGROUP_NAMES_FIELD = "resourceGroupNames";

    public InventoryItemModelData()
    {}

    public InventoryItemModelData(String name, String serial, String billingReference, Date purchaseDate,
            Date warrantyEnd, String status)
    {
        this.setName(name);
        this.setSerial(serial);
        this.setBillingReference(billingReference);
        this.setPurchaseDate(purchaseDate);
        this.setWarrantyEnd(warrantyEnd);
        this.setInventoryStatus(status);
        this.setResourceGroupIDs(new ArrayList<Long>());
        this.setResourceGroupNames(new ArrayList<String>());
    }

    public String getSerial()
    {
        return this.get(SERIAL_FIELD);
    }

    public void setSerial(String serial)
    {
        this.set(SERIAL_FIELD, serial);
    }

    public String getBillingReference()
    {
        return this.get(BILLING_REFERENCE_FIELD);
    }

    public void setBillingReference(String billingReference)
    {
        this.set(BILLING_REFERENCE_FIELD, billingReference);
    }

    public Date getPurchaseDate()
    {
        return this.get(PURCHASE_DATE_FIELD);
    }

    public void setPurchaseDate(Date purchaseDate)
    {
        this.set(PURCHASE_DATE_FIELD, purchaseDate);
    }

    public Date getWarrantyEnd()
    {
        return this.get(WARRANTY_END_FIELD);
    }

    public void setWarrantyEnd(Date warrantyEnd)
    {
        this.set(WARRANTY_END_FIELD, warrantyEnd);
    }

    public String getInventoryStatus()
    {
        return this.get(INVENTORY_STATUS_FIELD);
    }

    public void setInventoryStatus(String inventoryStatus)
    {
        this.set(INVENTORY_STATUS_FIELD, inventoryStatus);
    }

    public List<Long> getResourceGroupIDs()
    {
        return get(RESOURCEGROUP_IDS_FIELD);
    }

    public void setResourceGroupIDs(List<Long> resourceGroupIDs)
    {
        this.set(RESOURCEGROUP_IDS_FIELD, resourceGroupIDs);
    }

    public List<String> getResourceGroupNames()
    {
        return get(RESOURCEGROUP_NAMES_FIELD);
    }

    public void setResourceGroupNames(List<String> resourceGroupNames)
    {
        this.set(RESOURCEGROUP_NAMES_FIELD, resourceGroupNames);
    }

    public static ModelType getModelType()
    {
        ModelType type = RecordModelData.getModelType();

        DataField purchaseDateDataField = new DataField(ComputerModelData.PURCHASE_DATE_FIELD);
        purchaseDateDataField.setType(Date.class);
        purchaseDateDataField.setFormat(TIMESTAMP_FORMAT_PATTERN);

        DataField warrantyDateDataField = new DataField(ComputerModelData.WARRANTY_END_FIELD);
        warrantyDateDataField.setType(Date.class);
        warrantyDateDataField.setFormat(TIMESTAMP_FORMAT_PATTERN);

        DataField resourceGroupIdsDataField = new DataField(RESOURCEGROUP_IDS_FIELD);
        resourceGroupIdsDataField.setType(List.class);
        resourceGroupIdsDataField.setFormat("ResourceGroup");

        DataField resourceGroupNamesDataField = new DataField(RESOURCEGROUP_NAMES_FIELD);
        resourceGroupNamesDataField.setType(List.class);

        type.addField(NAME_FIELD);
        type.addField(SERIAL_FIELD);
        type.addField(BILLING_REFERENCE_FIELD);
        type.addField(purchaseDateDataField);
        type.addField(warrantyDateDataField);
        type.addField(INVENTORY_STATUS_FIELD);
        type.addField(resourceGroupIdsDataField);
        type.addField(resourceGroupNamesDataField);

        return type;
    }

    public boolean equals(InventoryItemModelData item)
    {
        return (item != null && this.getId() != null && item.getId() != null && this.getId().longValue() == item
                .getId().longValue());
    }
}
