package fr.hd3d.common.ui.client.modeldata.inventory;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;


public class RoomModelData extends RecordModelData
{
    private static final long serialVersionUID = -5484811007789570224L;

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LRoom";
    public static final String SIMPLE_CLASS_NAME = "Room";

    public RoomModelData()
    {
        this.setClassName(CLASS_NAME);
        this.simpleClassName = SIMPLE_CLASS_NAME;
    }

    public RoomModelData(Long id, String name)
    {
        this.setId(id);
        this.setName(name);

        this.setClassName(CLASS_NAME);
        this.simpleClassName = SIMPLE_CLASS_NAME;
    }

    public RoomModelData(String name)
    {
        this(null, name);
    }

    /**
     * @return Model type used by reader to know the manufacturer model data structure.
     */
    public static ModelType getModelType()
    {
        ModelType type = RecordModelData.getModelType();

        DataField nameDataField = new DataField(NAME_FIELD);
        nameDataField.setType(String.class);

        type.addField(nameDataField);

        return type;
    }
}
