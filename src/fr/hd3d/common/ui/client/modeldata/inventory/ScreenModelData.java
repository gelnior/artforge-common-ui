package fr.hd3d.common.ui.client.modeldata.inventory;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;


public class ScreenModelData extends InventoryItemModelData
{
    /** Automatically generated serial version number. */
    private static final long serialVersionUID = 7987464702840370795L;

    public static final String SIZE_FIELD = "size";
    public static final String TYPE_FIELD = "type";
    public static final String QUALIFICATION_FIELD = "qualification";
    public static final String SCREEN_MODEL_ID_FIELD = "screenModelId";
    public static final String SCREEN_MODEL_NAME_FIELD = "screenModelName";
    public static final String COMPUTER_IDS_FIELD = "computerIDs";
    public static final String COMPUTER_NAMES_FIELD = "computerNames";

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LScreen";
    public static final String SIMPLE_CLASS_NAME = "Screen";

    /**
     * Default constructor : sets an empty screen.
     */
    public ScreenModelData()
    {
        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public ScreenModelData(String name, String serial, String billingReference, Date purchaseDate, Date warrantyEnd,
            String status, Double size, String type, String qualification)
    {
        super(name, serial, billingReference, purchaseDate, warrantyEnd, status);
        this.setSize(size);
        this.setType(type);
        this.setQualification(qualification);
        this.setComputerIDs(new ArrayList<Long>());
        this.setComputerNames(new ArrayList<String>());

        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public Double getSize()
    {
        return this.get(DeviceModelData.SIZE_FIELD);
    }

    public void setSize(Double size)
    {
        this.set(DeviceModelData.SIZE_FIELD, size);
    }

    public String getType()
    {
        return this.get(TYPE_FIELD);
    }

    public void setType(String type)
    {
        this.set(TYPE_FIELD, type);
    }

    public List<Long> getComputerIDs()
    {
        return get(COMPUTER_IDS_FIELD);
    }

    public void setComputerIDs(List<Long> computerIDs)
    {
        this.set(COMPUTER_IDS_FIELD, computerIDs);
    }

    public List<String> getComputerNames()
    {
        return get(COMPUTER_NAMES_FIELD);
    }

    public void setComputerNames(List<String> computerNames)
    {
        this.set(COMPUTER_NAMES_FIELD, computerNames);
    }

    public void addComputer(ComputerModelData computer)
    {
        Set<Long> computerIDs = new HashSet<Long>(getComputerIDs());
        Set<String> computerNames = new HashSet<String>(getComputerNames());

        computerIDs.add(computer.getId());
        computerNames.add(computer.getName());

        setComputerIDs(new ArrayList<Long>(computerIDs));
        setComputerNames(new ArrayList<String>(computerNames));
    }

    public void removeComputer(ComputerModelData computer)
    {
        Set<Long> computerIDs = new HashSet<Long>(getComputerIDs());
        Set<String> computerNames = new HashSet<String>(getComputerNames());

        computerIDs.remove(computer.getId());
        computerNames.remove(computer.getName());

        setComputerIDs(new ArrayList<Long>(computerIDs));
        setComputerNames(new ArrayList<String>(computerNames));
    }

    public String getQualification()
    {
        return this.get(ScreenModelData.QUALIFICATION_FIELD);
    }

    public void setQualification(String qualification)
    {
        this.set(ScreenModelData.QUALIFICATION_FIELD, qualification);
    }

    public Long getScreenModelId()
    {
        return this.get(SCREEN_MODEL_ID_FIELD);
    }

    public void setScreenModelId(Long screenModelId)
    {
        this.set(SCREEN_MODEL_ID_FIELD, screenModelId);
    }

    public String getScreenModelName()
    {
        return this.get(SCREEN_MODEL_NAME_FIELD);
    }

    public void setScreenModelName(String screenModelName)
    {
        this.set(SCREEN_MODEL_NAME_FIELD, screenModelName);
    }

    /**
     * @return Model type used by reader to handle the screen model data structure.
     */
    public static ModelType getModelType()
    {
        ModelType type = InventoryItemModelData.getModelType();

        DataField sizeDataField = new DataField(DeviceModelData.SIZE_FIELD);
        sizeDataField.setType(Double.class);

        DataField screenModelIdDataField = new DataField(SCREEN_MODEL_ID_FIELD);
        screenModelIdDataField.setType(Long.class);

        DataField computerIdsDataField = new DataField(DeviceModelData.COMPUTER_IDS_FIELD);
        computerIdsDataField.setType(List.class);
        computerIdsDataField.setFormat("Computer");

        DataField computerNamesDataField = new DataField(DeviceModelData.COMPUTER_NAMES_FIELD);
        computerNamesDataField.setType(List.class);

        type.addField(sizeDataField);
        type.addField(TYPE_FIELD);
        type.addField(QUALIFICATION_FIELD);
        type.addField(screenModelIdDataField);
        type.addField(SCREEN_MODEL_NAME_FIELD);
        type.addField(computerIdsDataField);
        type.addField(computerNamesDataField);

        return type;
    }
}
