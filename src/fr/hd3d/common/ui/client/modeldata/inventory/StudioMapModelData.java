package fr.hd3d.common.ui.client.modeldata.inventory;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;


public class StudioMapModelData extends RecordModelData
{
    /** Automatically generated serial version number. */
    private static final long serialVersionUID = -5557795790888008500L;

    public static final String IMAGE_PATH_FIELD = "imagePath";
    public static final String SIZE_X_FIELD = "sizeX";
    public static final String SIZE_Y_FIELD = "sizeY";
    public static final String COMPUTER_IDS_FIELD = "computerIDs";
    public static final String COMPUTER_NAMES_FIELD = "computerNames";

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LStudioMap";
    public static final String SIMPLE_CLASS_NAME = "StudioMap";

    /**
     * Default constructor : sets an empty studio map.
     */
    public StudioMapModelData()
    {
        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public StudioMapModelData(String name, String imagePath, long sizeX, long sizeY)
    {
        this.setName(name);
        this.setImagePath(imagePath);
        this.setSizeX(sizeX);
        this.setSizeY(sizeY);
        this.setComputerIDs(new ArrayList<Long>());
        this.setComputerNames(new ArrayList<String>());

        this.setClassName(CLASS_NAME);
        this.simpleClassName = SIMPLE_CLASS_NAME;
    }

    public String getImagePath()
    {
        return this.get(IMAGE_PATH_FIELD);
    }

    public void setImagePath(String imagePath)
    {
        this.set(IMAGE_PATH_FIELD, imagePath);
    }

    public Long getSizeX()
    {
        return this.get(SIZE_X_FIELD);
    }

    public void setSizeX(Long sizeX)
    {
        this.set(SIZE_X_FIELD, sizeX);
    }

    public Long getSizeY()
    {
        return this.get(SIZE_Y_FIELD);
    }

    public void setSizeY(Long sizeY)
    {
        this.set(SIZE_Y_FIELD, sizeY);
    }

    public List<Long> getComputerIDs()
    {
        return get(COMPUTER_IDS_FIELD);
    }

    public void setComputerIDs(List<Long> computerIDs)
    {
        this.set(COMPUTER_IDS_FIELD, computerIDs);
    }

    public List<String> getComputerNames()
    {
        return get(COMPUTER_NAMES_FIELD);
    }

    public void setComputerNames(List<String> computerNames)
    {
        this.set(COMPUTER_NAMES_FIELD, computerNames);
    }

    /**
     * @return Model type used by reader to handle the studio map model data structure.
     */
    public static ModelType getModelType()
    {
        ModelType type = Hd3dModelData.getModelType();

        type.addField(CLASS_NAME_FIELD);
        type.addField(NAME_FIELD);
        type.addField(IMAGE_PATH_FIELD);
        type.addField(SIZE_X_FIELD);
        type.addField(SIZE_Y_FIELD);
        type.addField(COMPUTER_IDS_FIELD);
        type.addField(COMPUTER_NAMES_FIELD);

        return type;
    }
}
