package fr.hd3d.common.ui.client.modeldata.inventory;


public class ScreenModelModelData extends ModelModelData
{
    private static final long serialVersionUID = -4537193047766506261L;

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LScreenModel";
    public static final String SIMPLE_CLASS_NAME = "ScreenModel";

    public ScreenModelModelData(Long id, String name)
    {
        super(id, name);

        this.setClassName(CLASS_NAME);
        this.simpleClassName = SIMPLE_CLASS_NAME;
    }

    public ScreenModelModelData(String name)
    {
        this(null, name);
    }

    public ScreenModelModelData()
    {
        this(null, null);
    }

}
