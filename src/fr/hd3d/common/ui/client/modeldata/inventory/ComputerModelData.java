package fr.hd3d.common.ui.client.modeldata.inventory;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;


public class ComputerModelData extends InventoryItemModelData
{
    /** Automatically generated serial version number. */
    private static final long serialVersionUID = -3079812751504934603L;

    public static final String INVENTORY_ID_FIELD = "inventoryId";
    public static final String DNS_NAME_FIELD = "dnsName";
    public static final String IP_ADDRESS_FIELD = "ipAdress";
    public static final String MAC_ADDRESS_FIELD = "macAdress";
    public static final String PROC_FREQUENCY_FIELD = "procFrequency";
    public static final String NUMBER_OF_PROCS_FIELD = "numberOfProcs";
    public static final String NUMBER_OF_CORES_FIELD = "numberOfCores";
    public static final String RAM_QUANTITY_FIELD = "ramQuantity";
    public static final String WORKER_STATUS_FIELD = "workerStatus";
    public static final String X_POSITION_FIELD = "xPosition";
    public static final String Y_POSITION_FIELD = "yPosition";

    public static final String STUDIOMAP_ID_FIELD = "studioMapId";
    public static final String STUDIOMAP_NAME_FIELD = "studioMapName";
    public static final String ROOM_ID_FIELD = "roomId";
    public static final String ROOM_NAME_FIELD = "roomName";
    public static final String PERSON_ID_FIELD = "personId";
    public static final String PERSON_NAME_FIELD = "personName";
    public static final String PROCESSOR_ID_FIELD = "processorId";
    public static final String PROCESSOR_NAME_FIELD = "processorName";
    public static final String COMPUTER_TYPE_ID_FIELD = "computerTypeId";
    public static final String COMPUTER_TYPE_NAME_FIELD = "computerTypeName";
    public static final String COMPUTER_MODEL_ID_FIELD = "computerModelId";
    public static final String COMPUTER_MODEL_NAME_FIELD = "computerModelName";

    public static final String POOL_IDS_FIELD = "poolIDs";
    public static final String POOL_NAMES_FIELD = "poolNames";
    public static final String SOFTWARE_IDS_FIELD = "softwareIDs";
    public static final String SOFTWARE_NAMES_FIELD = "softwareNames";
    public static final String LICENSE_IDS_FIELD = "licenseIDs";
    public static final String LICENSE_NAMES_FIELD = "licenseNames";
    public static final String DEVICE_IDS_FIELD = "deviceIDs";
    public static final String DEVICE_NAMES_FIELD = "deviceNames";
    public static final String SCREEN_IDS_FIELD = "screenIDs";
    public static final String SCREEN_NAMES_FIELD = "screenNames";

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LComputer";
    public static final String SIMPLE_CLASS_NAME = "Computer";

    /**
     * Default constructor : sets an empty computer with no name.
     */
    public ComputerModelData()
    {
        super();
        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public ComputerModelData(String name)
    {
        this();

        this.setName(name);
    }

    public ComputerModelData(String name, String serial, String billingReference, Date purchaseDate, Date warrantyEnd,
            String status, Long inventoryId, String dnsName, String ipAdress, String macAdress, Long procFrequency,
            Long numberOfProcs, Long numberOfCores, Long ramQuantity, String workerStatus, Long xPosition,
            Long yPosition)
    {
        super(name, serial, billingReference, purchaseDate, warrantyEnd, status);

        this.setInventoryId(inventoryId);
        this.setDnsName(dnsName);
        this.setIpAdress(ipAdress);
        this.setMacAdress(macAdress);
        this.setProcFrequency(procFrequency);
        this.setNumberOfProcs(numberOfProcs);
        this.setNumberOfCores(numberOfCores);
        this.setRamQuantity(ramQuantity);
        this.setWorkerStatus(workerStatus);
        this.setXPosition(xPosition);
        this.setYPosition(yPosition);
        this.setPoolIDs(new ArrayList<Long>());
        this.setPoolNames(new ArrayList<String>());
        this.setSoftwareIDs(new ArrayList<Long>());
        this.setSoftwareNames(new ArrayList<String>());
        this.setLicenseIDs(new ArrayList<Long>());
        this.setLicenseNames(new ArrayList<String>());
        this.setDeviceIDs(new ArrayList<Long>());
        this.setDeviceNames(new ArrayList<String>());
        this.setScreenIDs(new ArrayList<Long>());
        this.setScreenNames(new ArrayList<String>());

        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public Long getInventoryId()
    {
        return this.get(INVENTORY_ID_FIELD);
    }

    public void setInventoryId(Long inventoryId)
    {
        this.set(INVENTORY_ID_FIELD, inventoryId);
    }

    public String getDnsName()
    {
        return this.get(DNS_NAME_FIELD);
    }

    public void setDnsName(String dnsName)
    {
        this.set(DNS_NAME_FIELD, dnsName);
    }

    public String getIpAdress()
    {
        return this.get(IP_ADDRESS_FIELD);
    }

    public void setIpAdress(String ipAdress)
    {
        this.set(IP_ADDRESS_FIELD, ipAdress);
    }

    public String getMacAdress()
    {
        return this.get(MAC_ADDRESS_FIELD);
    }

    public void setMacAdress(String macAdress)
    {
        this.set(MAC_ADDRESS_FIELD, macAdress);
    }

    public Long getProcFrequency()
    {
        return this.get(PROC_FREQUENCY_FIELD);
    }

    public void setProcFrequency(Long procFrequency)
    {
        this.set(PROC_FREQUENCY_FIELD, procFrequency);
    }

    public Long getNumberOfProcs()
    {
        return this.get(NUMBER_OF_PROCS_FIELD);
    }

    public void setNumberOfProcs(Long numberOfProcs)
    {
        this.set(NUMBER_OF_PROCS_FIELD, numberOfProcs);
    }

    public Long getNumberOfCores()
    {
        return this.get(NUMBER_OF_CORES_FIELD);
    }

    public void setNumberOfCores(Long numberOfCores)
    {
        this.set(NUMBER_OF_CORES_FIELD, numberOfCores);
    }

    public Long getRamQuantity()
    {
        return this.get(RAM_QUANTITY_FIELD);
    }

    public void setRamQuantity(Long ramQuantity)
    {
        this.set(RAM_QUANTITY_FIELD, ramQuantity);
    }

    public String getWorkerStatus()
    {
        return this.get(WORKER_STATUS_FIELD);
    }

    public void setWorkerStatus(String workerStatus)
    {
        this.set(WORKER_STATUS_FIELD, workerStatus);
    }

    public Long getXPosition()
    {
        return this.get(X_POSITION_FIELD);
    }

    public void setXPosition(Long xPosition)
    {
        this.set(X_POSITION_FIELD, xPosition);
    }

    public Long getYPosition()
    {
        return this.get(Y_POSITION_FIELD);
    }

    public void setYPosition(Long yPostion)
    {
        this.set(Y_POSITION_FIELD, yPostion);
    }

    public Long getStudioMapId()
    {
        return this.get(STUDIOMAP_ID_FIELD);
    }

    public void setStudioMapID(Long studioMapId)
    {
        this.set(STUDIOMAP_ID_FIELD, studioMapId);
    }

    public String getStudioMapName()
    {
        return this.get(STUDIOMAP_NAME_FIELD);
    }

    public void setStudioMapName(String studioMapName)
    {
        this.set(STUDIOMAP_NAME_FIELD, studioMapName);
    }

    public void setStudioMap(StudioMapModelData map)
    {
        if (map == null)
            return;
        this.set(STUDIOMAP_ID_FIELD, map.getId());
        this.set(STUDIOMAP_NAME_FIELD, map.getName());
    }

    public Long getRoomId()
    {
        return this.get(ROOM_ID_FIELD);
    }

    public void setRoomId(Long roomId)
    {
        this.set(ROOM_ID_FIELD, roomId);
    }

    public String getRoomName()
    {
        return this.get(ROOM_NAME_FIELD);
    }

    public void setRoomName(String roomName)
    {
        this.set(STUDIOMAP_NAME_FIELD, roomName);
    }

    public Long getPersonId()
    {
        return this.get(PERSON_ID_FIELD);
    }

    public void setPersonId(Long personId)
    {
        this.set(PERSON_ID_FIELD, personId);
    }

    public String getPersonName()
    {
        return this.get(PERSON_NAME_FIELD);
    }

    public void setPersonName(String personName)
    {
        this.set(PERSON_NAME_FIELD, personName);
    }

    public Long getProcessorId()
    {
        return this.get(PROCESSOR_ID_FIELD);
    }

    public void setProcessorID(Long processorId)
    {
        this.set(PROCESSOR_ID_FIELD, processorId);
    }

    public String getProcessorName()
    {
        return this.get(PROCESSOR_NAME_FIELD);
    }

    public void setProcesorName(String processorName)
    {
        this.set(PROCESSOR_NAME_FIELD, processorName);
    }

    public Long getComputerTypeId()
    {
        return this.get(COMPUTER_TYPE_ID_FIELD);
    }

    public void setComputerTypeId(Long computerTypeId)
    {
        this.set(COMPUTER_TYPE_ID_FIELD, computerTypeId);
    }

    public String getComputerTypeName()
    {
        return this.get(COMPUTER_TYPE_NAME_FIELD);
    }

    public void setComputerTypeName(String computerTypeName)
    {
        this.set(COMPUTER_TYPE_NAME_FIELD, computerTypeName);
    }

    public Long getComputerModelId()
    {
        return this.get(COMPUTER_MODEL_ID_FIELD);
    }

    public void setComputerModelId(Long computerModelId)
    {
        this.set(COMPUTER_MODEL_ID_FIELD, computerModelId);
    }

    public String getComputerModelName()
    {
        return this.get(COMPUTER_MODEL_NAME_FIELD);
    }

    public void setComputerModelName(String computerModelName)
    {
        this.set(COMPUTER_MODEL_NAME_FIELD, computerModelName);
    }

    public List<Long> getPoolIDs()
    {
        return get(POOL_IDS_FIELD);
    }

    public void setPoolIDs(List<Long> poolIDs)
    {
        this.set(POOL_IDS_FIELD, poolIDs);
    }

    public List<String> getPoolNames()
    {
        return get(POOL_NAMES_FIELD);
    }

    public void setPoolNames(List<String> poolNames)
    {
        this.set(POOL_NAMES_FIELD, poolNames);
    }

    public List<Long> getSoftwareIDs()
    {
        return get(SOFTWARE_IDS_FIELD);
    }

    public void setSoftwareIDs(List<Long> softwareIDs)
    {
        this.set(SOFTWARE_IDS_FIELD, softwareIDs);
    }

    public List<String> getSoftwareNames()
    {
        return get(SOFTWARE_NAMES_FIELD);
    }

    public void setSoftwareNames(List<String> softwareNames)
    {
        this.set(SOFTWARE_NAMES_FIELD, softwareNames);
    }

    public List<Long> getLicenseIDs()
    {
        return get(LICENSE_IDS_FIELD);
    }

    public void setLicenseIDs(List<Long> licenseIDs)
    {
        this.set(LICENSE_IDS_FIELD, licenseIDs);
    }

    public List<String> getLicenseNames()
    {
        return get(LICENSE_NAMES_FIELD);
    }

    public void setLicenseNames(List<String> licenseNames)
    {
        this.set(LICENSE_NAMES_FIELD, licenseNames);
    }

    public List<Long> getDeviceIDs()
    {
        return get(DEVICE_IDS_FIELD);
    }

    public void setDeviceIDs(List<Long> deviceIDs)
    {
        this.set(DEVICE_IDS_FIELD, deviceIDs);
    }

    public List<String> getDeviceNames()
    {
        return get(DEVICE_NAMES_FIELD);
    }

    public void setDeviceNames(List<String> deviceNames)
    {
        this.set(DEVICE_NAMES_FIELD, deviceNames);
    }

    public List<Long> getScreenIDs()
    {
        return get(SCREEN_IDS_FIELD);
    }

    public void setScreenIDs(List<Long> screenIDs)
    {
        this.set(SCREEN_IDS_FIELD, screenIDs);
    }

    public List<String> getScreenNames()
    {
        return get(SCREEN_NAMES_FIELD);
    }

    public void setScreenNames(List<String> screenNames)
    {
        this.set(SCREEN_NAMES_FIELD, screenNames);
    }

    public void addScreen(ScreenModelData screen)
    {
        Set<Long> screenIDs = new HashSet<Long>(getScreenIDs());
        Set<String> screenNames = new HashSet<String>(getScreenNames());

        screenIDs.add(screen.getId());
        screenNames.add(screen.getName());

        setScreenIDs(new ArrayList<Long>(screenIDs));
        setScreenNames(new ArrayList<String>(screenNames));
    }

    public void removeScreen(ScreenModelData screen)
    {
        Set<Long> screenIDs = new HashSet<Long>(getScreenIDs());
        Set<String> screenNames = new HashSet<String>(getScreenNames());

        screenIDs.remove(screen.getId());
        screenNames.remove(screen.getName());

        setScreenIDs(new ArrayList<Long>(screenIDs));
        setScreenNames(new ArrayList<String>(screenNames));
    }

    public void addDevice(DeviceModelData device)
    {
        Set<Long> deviceIDs = new HashSet<Long>(getDeviceIDs());
        Set<String> deviceNames = new HashSet<String>(getDeviceNames());

        deviceIDs.add(device.getId());
        deviceNames.add(device.getName());

        setDeviceIDs(new ArrayList<Long>(deviceIDs));
        setDeviceNames(new ArrayList<String>(deviceNames));
    }

    public void removeDevice(DeviceModelData device)
    {
        Set<Long> deviceIDs = new HashSet<Long>(getDeviceIDs());
        Set<String> deviceNames = new HashSet<String>(getDeviceNames());

        deviceIDs.remove(device.getId());
        deviceNames.remove(device.getName());

        setDeviceIDs(new ArrayList<Long>(deviceIDs));
        setDeviceNames(new ArrayList<String>(deviceNames));
    }

    /**
     * @return Model type used by reader to handle the computer model data structure.
     */
    public static ModelType getModelType()
    {
        ModelType type = InventoryItemModelData.getModelType();

        DataField inventoryIdField = new DataField(INVENTORY_ID_FIELD);
        inventoryIdField.setType(Long.class);
        DataField procFreqDataField = new DataField(PROC_FREQUENCY_FIELD);
        procFreqDataField.setType(Long.class);
        DataField numProcDataField = new DataField(NUMBER_OF_PROCS_FIELD);
        numProcDataField.setType(Long.class);
        DataField numCoreDataField = new DataField(NUMBER_OF_CORES_FIELD);
        numCoreDataField.setType(Long.class);
        DataField ramQuantField = new DataField(RAM_QUANTITY_FIELD);
        ramQuantField.setType(Long.class);
        DataField xPosDataField = new DataField(X_POSITION_FIELD);
        xPosDataField.setType(Long.class);
        DataField yPosDataField = new DataField(Y_POSITION_FIELD);
        yPosDataField.setType(Long.class);

        DataField processorIdDataField = new DataField(PROCESSOR_ID_FIELD);
        processorIdDataField.setType(Long.class);

        DataField computerTypeIdDataField = new DataField(COMPUTER_TYPE_ID_FIELD);
        computerTypeIdDataField.setType(Long.class);

        DataField computerModelIdDataField = new DataField(COMPUTER_MODEL_ID_FIELD);
        computerModelIdDataField.setType(Long.class);

        DataField studioMapIdDataField = new DataField(STUDIOMAP_ID_FIELD);
        studioMapIdDataField.setType(Long.class);

        DataField roomIdDataField = new DataField(ROOM_ID_FIELD);
        roomIdDataField.setType(Long.class);

        DataField personIdDataField = new DataField(PERSON_ID_FIELD);
        personIdDataField.setType(Long.class);

        DataField poolIdsDataField = new DataField(POOL_IDS_FIELD);
        poolIdsDataField.setType(List.class);
        poolIdsDataField.setFormat("Pool");

        DataField poolNamesDataField = new DataField(POOL_NAMES_FIELD);
        poolNamesDataField.setType(List.class);

        DataField softwareIdsDataField = new DataField(SOFTWARE_IDS_FIELD);
        softwareIdsDataField.setType(List.class);
        softwareIdsDataField.setFormat(SoftwareModelData.SIMPLE_CLASS_NAME);

        DataField softwareNamesDataField = new DataField(SOFTWARE_NAMES_FIELD);
        softwareNamesDataField.setType(List.class);

        DataField licenseIdsDataField = new DataField(LICENSE_IDS_FIELD);
        licenseIdsDataField.setType(List.class);
        licenseIdsDataField.setFormat("License");

        DataField licenseNamesDataField = new DataField(LICENSE_NAMES_FIELD);
        licenseNamesDataField.setType(List.class);

        DataField deviceIdsDataField = new DataField(DEVICE_IDS_FIELD);
        deviceIdsDataField.setType(List.class);
        deviceIdsDataField.setFormat("Device");

        DataField deviceNamesDataField = new DataField(DEVICE_NAMES_FIELD);
        deviceNamesDataField.setType(List.class);

        DataField screenIdsDataField = new DataField(SCREEN_IDS_FIELD);
        screenIdsDataField.setType(List.class);
        screenIdsDataField.setFormat("Screen");

        DataField screenNamesDataField = new DataField(SCREEN_NAMES_FIELD);
        screenNamesDataField.setType(List.class);

        type.addField(inventoryIdField);
        type.addField(DNS_NAME_FIELD);
        type.addField(IP_ADDRESS_FIELD);
        type.addField(MAC_ADDRESS_FIELD);
        type.addField(procFreqDataField);
        type.addField(numProcDataField);
        type.addField(numCoreDataField);
        type.addField(ramQuantField);
        type.addField(WORKER_STATUS_FIELD);
        type.addField(xPosDataField);
        type.addField(yPosDataField);
        type.addField(processorIdDataField);
        type.addField(PROCESSOR_NAME_FIELD);
        type.addField(computerTypeIdDataField);
        type.addField(COMPUTER_TYPE_NAME_FIELD);
        type.addField(computerModelIdDataField);
        type.addField(COMPUTER_MODEL_NAME_FIELD);
        type.addField(roomIdDataField);
        type.addField(ROOM_NAME_FIELD);
        type.addField(personIdDataField);
        type.addField(PERSON_NAME_FIELD);
        type.addField(studioMapIdDataField);
        type.addField(STUDIOMAP_NAME_FIELD);
        type.addField(poolIdsDataField);
        type.addField(poolNamesDataField);
        type.addField(licenseIdsDataField);
        type.addField(licenseNamesDataField);
        type.addField(softwareIdsDataField);
        type.addField(softwareNamesDataField);
        type.addField(deviceIdsDataField);
        type.addField(deviceNamesDataField);
        type.addField(screenIdsDataField);
        type.addField(screenNamesDataField);

        return type;
    }

}
