package fr.hd3d.common.ui.client.modeldata.inventory;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;


/**
 * Model data for describing, base record : i.e. an Hd3dModelData with a name.
 * 
 * @author HD3D
 */
public class ProcessorModelData extends RecordModelData
{
    /** Automatically generated serial UID. */
    private static final long serialVersionUID = 1618681908057828075L;

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LProcessor";

    public static final String SIMPLE_CLASS_NAME = "Processor";

    public ProcessorModelData()
    {
        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public ProcessorModelData(Long id, String name)
    {
        this();

        this.setId(id);
        this.setName(name);
    }

    public ProcessorModelData(String name)
    {
        this(null, name);
    }

    @Override
    public void setName(String name)
    {
        this.set(NAME_FIELD, name);
    }

    /**
     * @return Model type used by reader to know the record model data structure.
     */
    public static ModelType getModelType()
    {
        ModelType type = RecordModelData.getModelType();

        type.addField(CLASS_NAME_FIELD);

        return type;
    }
}
