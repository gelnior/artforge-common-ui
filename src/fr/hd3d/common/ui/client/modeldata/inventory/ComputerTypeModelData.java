package fr.hd3d.common.ui.client.modeldata.inventory;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;


public class ComputerTypeModelData extends RecordModelData
{
    private static final long serialVersionUID = 3846420760127108231L;

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LComputerType";
    public static final String SIMPLE_CLASS_NAME = "ComputerType";

    public ComputerTypeModelData()
    {
        this.setClassName(CLASS_NAME);
        this.simpleClassName = SIMPLE_CLASS_NAME;
    }

    public ComputerTypeModelData(Long id, String name)
    {
        this();
        this.setId(id);
        this.setName(name);
    }

    public ComputerTypeModelData(String name)
    {
        this(null, name);
    }

    /**
     * @return Model type used by reader to know the manufacturer model data structure.
     */
    public static ModelType getModelType()
    {
        ModelType type = RecordModelData.getModelType();

        return type;
    }
}
