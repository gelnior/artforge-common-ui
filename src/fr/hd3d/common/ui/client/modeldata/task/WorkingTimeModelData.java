package fr.hd3d.common.ui.client.modeldata.task;

import java.util.Date;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;


public class WorkingTimeModelData extends Hd3dModelData
{
    private static final long serialVersionUID = 7989821201199058692L;

    public static final String PERSONDAY_ID_FIELD = "personDayId";
    public static final String STARTDATE_FIELD = "startDate";
    public static final String ENDDATE_FIELD = "endDate";

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LWorkingTime";
    public static final String SIMPLE_CLASS_NAME = "WorkingTime";

    public WorkingTimeModelData()
    {
        super();

        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public Date getStartDate()
    {
        return get(STARTDATE_FIELD);
    }

    public void setStartDate(Date date)
    {
        set(STARTDATE_FIELD, date);
    }

    public Date getEndDate()
    {
        return get(ENDDATE_FIELD);
    }

    public void setEndDate(Date date)
    {
        set(ENDDATE_FIELD, date);
    }

    public Long getPersonDayId()
    {
        return get(PERSONDAY_ID_FIELD);
    }

    public void setPersonDayId(Long id)
    {
        set(PERSONDAY_ID_FIELD, id);
    }

    public static ModelType getModelType()
    {
        ModelType modelType = Hd3dModelData.getModelType();

        DataField startDateField = new DataField(STARTDATE_FIELD);
        startDateField.setType(Date.class);
        startDateField.setFormat(TIMESTAMP_FORMAT_PATTERN);

        DataField endDateField = new DataField(ENDDATE_FIELD);
        endDateField.setType(Date.class);
        endDateField.setFormat(TIMESTAMP_FORMAT_PATTERN);

        DataField personIdField = new DataField(PERSONDAY_ID_FIELD);
        personIdField.setType(Long.class);

        modelType.addField(startDateField);
        modelType.addField(endDateField);
        modelType.addField(personIdField);

        return modelType;
    }
}
