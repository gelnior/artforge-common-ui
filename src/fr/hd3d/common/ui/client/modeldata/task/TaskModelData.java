package fr.hd3d.common.ui.client.modeldata.task;

import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.util.DateWrapper;

import fr.hd3d.common.client.Const;
import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.common.ui.client.calendar.DatetimeUtil;
import fr.hd3d.common.ui.client.modeldata.BaseTaskModelData;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.asset.AssetRevisionGroupModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.callback.GetModelDataCallback;
import fr.hd3d.common.ui.client.service.callback.PostModelDataCallback;
import fr.hd3d.common.ui.client.util.field.LongDataField;


/**
 * Task describes a piece of work to do. A task is assigned to a person, linked to a work object and described by task
 * type (fabrication step).
 * 
 * @author HD3D
 */
public class TaskModelData extends BaseTaskModelData
{
    private static final long serialVersionUID = 7590357579205893277L;

    public static final String NAME_FIELD = "name";
    public static final String STATUS_FIELD = "status";
    public static final String COMPLETION_FIELD = "completion";
    public static final String COMPLETION_DATE_FIELD = "completionDate";
    public static final String ACTUAL_END_DATE_FIELD = "actualEndDate";
    public static final String ACTUAL_START_DATE_FIELD = "actualStartDate";
    public static final String CONFIRMED_FIELD = "confirmed";
    public static final String DEAD_LINE_FIELD = "deadLine";
    public static final String STARTABLE_FIELD = "startable";
    public static final String PROJECT_ID_FIELD = "projectID";
    public static final String PROJECT_COLOR_FIELD = "projectColor";
    public static final String PROJECT_TYPE_NAME_FIELD = "projectTypeName";
    public static final String WORKER_ID_FIELD = "workerID";
    public static final String CREATOR_ID_FIELD = "creatorID";
    public static final String PROJECT_NAME_FIELD = "projectName";
    public static final String CREATOR_NAME_FIELD = "creatorName";
    public static final String WORKER_NAME_FIELD = "workerName";
    public static final String COLOR_FIELD = "color";
    public static final String TASK_TYPE_ID_FIELD = "taskTypeID";
    public static final String TASK_TYPE_NAME_FIELD = "taskTypeName";
    public static final String BOUND_ENTITY_NAME_FIELD = "boundEntityName";
    public static final String WORK_OBJECT_ID_FIELD = "workObjectId";
    public static final String WORK_OBJECT_NAME_FIELD = "workObjectName";
    public static final String WORK_OBJECT_PARENTS_NAME_FIELD = "workObjectParentsNames";
    public static final String COMMENT_FIELD = "commentForApprovalNote";
    public static final String TOTAL_ACTIVITY_DURATION_FIELD = Const.TOTAL_ACTIVITIES_DURATION;

    public static final String FILE_REVISIONS_FIELD = "fileRevisionsForApprovalNote";

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LTask";

    public static final String SIMPLE_CLASS_NAME = "Task";

    public static final String WORKER_ID = "worker.id";

    /**
     * Creates a new task of taskType <i>taskType</i> linked to <i>workObject</i>, set by <i>user</i> for project
     * <i>project</i>. When creation succeeds it forwards <i>event</i>.
     * 
     * @param taskType
     *            Task type of the new task.
     * @param workObject
     *            Work object with which task will be linked.
     * @param user
     *            User that sets the task.
     * @param project
     *            The project of the task.
     * @param eventType
     *            The event to dispatch when creation succceeds.
     * @return The created task.
     */
    public static TaskModelData createForWorkObject(TaskTypeModelData taskType, final RecordModelData workObject,
            PersonModelData user, ProjectModelData project, final EventType eventType)
    {
        TaskModelData task = new TaskModelData();
        task.setCompletion((byte) 0);
        task.setCreatorID(user.getId());
        task.setDuration(Long.valueOf(DatetimeUtil.DAY_SECONDS));
        task.setStatus(ETaskStatus.defaultStatus().name());
        task.setProjectID(project.getId());
        task.setTaskTypeId(taskType.getId());
        task.setBoundEntityName(workObject.getSimpleClassName());
        task.setWorkObjectId(workObject.getId());
        task.setName(workObject.get(ConstituentModelData.CONSTITUENT_LABEL) + "_" + taskType.getName());

        task.setDefaultPath(project.getDefaultPath() + "/" + ServicesPath.TASKS);
        task.save(new PostModelDataCallback(task, new AppEvent(eventType)));
        return task;
    }

    public TaskModelData()
    {
        super();
        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    @Override
    public void setName(String name)
    {
        set(NAME_FIELD, name);
    }

    public void setStatus(String status)
    {
        set(STATUS_FIELD, status);
    }

    public void setCompletion(Byte completion)
    {
        set(COMPLETION_FIELD, completion);
    }

    public void setCompletionDate(Date completionDate)
    {
        set(COMPLETION_DATE_FIELD, completionDate);
    }

    public void setActualEndDate(Date actualEndDate)
    {
        set(ACTUAL_END_DATE_FIELD, actualEndDate);
    }

    public void setActualStartDate(Date actualStartDate)
    {
        set(ACTUAL_START_DATE_FIELD, actualStartDate);
    }

    public void setConfirmed(Boolean confirmed)
    {
        set(CONFIRMED_FIELD, confirmed);
    }

    public void setDeadLine(Date deadLine)
    {
        set(DEAD_LINE_FIELD, deadLine);
    }

    public void setStartable(Boolean startable)
    {
        set(STARTABLE_FIELD, startable);
    }

    public void setProjectID(Long projectID)
    {
        set(PROJECT_ID_FIELD, projectID);
    }

    public void setWorkerID(Long workerID)
    {
        set(WORKER_ID_FIELD, workerID);
    }

    public void setCreatorID(Long creatorID)
    {
        set(CREATOR_ID_FIELD, creatorID);
    }

    public void setProjectName(String projectName)
    {
        set(PROJECT_NAME_FIELD, projectName);
    }

    public void setCreatorName(String creatorName)
    {
        set(CREATOR_NAME_FIELD, creatorName);
    }

    public void setWorkerName(String workerName)
    {
        set(WORKER_NAME_FIELD, workerName);
    }

    public void setColor(String color)
    {
        set(COLOR_FIELD, color);
    }

    @Override
    public String getName()
    {
        return get(NAME_FIELD);
    }

    public String getStatus()
    {
        return get(STATUS_FIELD);
    }

    public Byte getCompletion()
    {
        return (Byte) get(COMPLETION_FIELD);
    }

    public Date getCompletionDate()
    {
        return (Date) get(COMPLETION_DATE_FIELD);
    }

    public Date getActualEndDate()
    {
        return (Date) get(ACTUAL_END_DATE_FIELD);
    }

    public Date getActualStartDate()
    {
        return (Date) get(ACTUAL_START_DATE_FIELD);
    }

    public Boolean getConfirmed()
    {
        return (Boolean) get(CONFIRMED_FIELD);
    }

    public Date getDeadLine()
    {
        return (Date) get(DEAD_LINE_FIELD);
    }

    public Boolean getStartable()
    {
        return (Boolean) get(STARTABLE_FIELD);
    }

    public Long getProjectID()
    {
        return (Long) get(PROJECT_ID_FIELD);
    }

    public Long getWorkerID()
    {
        return (Long) get(WORKER_ID_FIELD);
    }

    public Long getCreatorID()
    {
        return (Long) get(CREATOR_ID_FIELD);
    }

    public String getProjectName()
    {
        return (String) get(PROJECT_NAME_FIELD);
    }

    public String getCreatorName()
    {
        return (String) get(CREATOR_NAME_FIELD);
    }

    public String getWorkerName()
    {
        return (String) get(WORKER_NAME_FIELD);
    }

    public String getColor()
    {
        return (String) get(COLOR_FIELD);
    }

    public String getProjectColor()
    {
        return (String) get(PROJECT_COLOR_FIELD);
    }

    public String getTaskTypeName()
    {
        return (String) get(TASK_TYPE_NAME_FIELD);
    }

    public void setTaskTypeName(String taskTypeName)
    {
        set(TASK_TYPE_NAME_FIELD, taskTypeName);
    }

    public Long getTaskTypeId()
    {
        return get(TASK_TYPE_ID_FIELD);
    }

    public void setTaskTypeId(Long id)
    {
        this.set(TASK_TYPE_ID_FIELD, id);
    }

    public String getBoundEntityName()
    {
        return (String) get(BOUND_ENTITY_NAME_FIELD);
    }

    public void setBoundEntityName(String boundEntityName)
    {
        this.set(BOUND_ENTITY_NAME_FIELD, boundEntityName);
    }

    public String getWorkObjectName()
    {
        return (String) get(WORK_OBJECT_NAME_FIELD);
    }

    public void setWorkObjectName(String workObjectName)
    {
        this.set(WORK_OBJECT_NAME_FIELD, workObjectName);
    }

    public Long getWorkObjectId()
    {
        return get(WORK_OBJECT_ID_FIELD);
    }

    public void setWorkObjectId(Long workObjectId)
    {
        this.set(WORK_OBJECT_ID_FIELD, workObjectId);
    }

    public String getProjectTypeName()
    {
        return get(PROJECT_TYPE_NAME_FIELD);
    }

    public void setProjectTypeName(String projectTypeName)
    {
        set(PROJECT_TYPE_NAME_FIELD, projectTypeName);
    }

    public String getWorkObjectParentsName()
    {
        return get(WORK_OBJECT_PARENTS_NAME_FIELD);
    }

    public void setWorkObjectParentsName(String name)
    {
        set(WORK_OBJECT_PARENTS_NAME_FIELD, name);
    }

    public String getComment()
    {
        return get(COMMENT_FIELD);
    }

    public void setComment(String comment)
    {
        set(COMMENT_FIELD, comment);
    }

    public Long getTotalActivityDuration()
    {
        Long duration = get(TOTAL_ACTIVITY_DURATION_FIELD);
        if (duration == null)
            return 0L;
        else
            return duration;
    }

    public void setTotalActivityDuration(Long totalActivityDuration)
    {
        set(TOTAL_ACTIVITY_DURATION_FIELD, totalActivityDuration);
    }

    public static ModelType getModelType()
    {
        ModelType modelType = BaseTaskModelData.getBaseModelType();
        DataField creatorIdDataField = new DataField(CREATOR_ID_FIELD);
        DataField workerIdDataField = new DataField(WORKER_ID_FIELD);
        DataField projectIdDataField = new DataField(PROJECT_ID_FIELD);
        DataField actualStartDateField = new DataField(ACTUAL_START_DATE_FIELD);
        DataField actualEndDateField = new DataField(ACTUAL_END_DATE_FIELD);
        DataField startableDataField = new DataField(STARTABLE_FIELD);
        DataField confirmedDataField = new DataField(CONFIRMED_FIELD);
        DataField completionField = new DataField(COMPLETION_FIELD);
        DataField completionDateField = new DataField(COMPLETION_DATE_FIELD);
        DataField deadLineDataField = new DataField(DEAD_LINE_FIELD);
        DataField taskTypeIdDataField = new DataField(TASK_TYPE_ID_FIELD);

        creatorIdDataField.setType(Long.class);
        workerIdDataField.setType(Long.class);
        projectIdDataField.setType(Long.class);
        actualStartDateField.setType(Date.class);
        actualStartDateField.setFormat(TIMESTAMP_FORMAT_PATTERN);
        actualEndDateField.setType(Date.class);
        actualEndDateField.setFormat(TIMESTAMP_FORMAT_PATTERN);
        completionField.setType(Byte.class);
        completionDateField.setType(Date.class);
        completionDateField.setFormat(TIMESTAMP_FORMAT_PATTERN);
        startableDataField.setType(Boolean.class);
        confirmedDataField.setType(Boolean.class);
        deadLineDataField.setType(Date.class);
        deadLineDataField.setFormat(TIMESTAMP_FORMAT_PATTERN);
        taskTypeIdDataField.setType(Long.class);

        modelType.addField(STATUS_FIELD);
        modelType.addField(completionField);
        modelType.addField(completionDateField);
        modelType.addField(NAME_FIELD);
        modelType.addField(creatorIdDataField);
        modelType.addField(workerIdDataField);
        modelType.addField(projectIdDataField);
        modelType.addField(PROJECT_COLOR_FIELD);
        modelType.addField(PROJECT_TYPE_NAME_FIELD);
        modelType.addField(actualEndDateField);
        modelType.addField(actualStartDateField);
        modelType.addField(startableDataField);
        modelType.addField(confirmedDataField);
        modelType.addField(deadLineDataField);
        modelType.addField(PROJECT_NAME_FIELD);
        modelType.addField(CREATOR_NAME_FIELD);
        modelType.addField(WORKER_NAME_FIELD);
        modelType.addField(taskTypeIdDataField);
        modelType.addField(TASK_TYPE_NAME_FIELD);
        modelType.addField(COLOR_FIELD);
        modelType.addField(BOUND_ENTITY_NAME_FIELD);
        modelType.addField(new LongDataField(WORK_OBJECT_ID_FIELD));
        modelType.addField(WORK_OBJECT_NAME_FIELD);
        modelType.addField(WORK_OBJECT_PARENTS_NAME_FIELD);
        modelType.addField(COMMENT_FIELD);
        modelType.addField(new LongDataField(TOTAL_ACTIVITY_DURATION_FIELD));
        return modelType;
    }

    /**
     * Retrieve assets linked to work object. First it
     * 
     * @param eventType
     *            The event to forward to controllers when asset retrieving succeeds.
     */
    public void getWorkObjectAssets(final EventType eventType)
    {
        final EntityTaskLinkModelData link = new EntityTaskLinkModelData();
        link.refreshByTask(this, new GetModelDataCallback(link) {
            @Override
            protected void onRecordReturned(List<Hd3dModelData> records)
            {
                Hd3dModelData newRecord = records.get(0);
                this.record.updateFromModelData(newRecord);

                onEntityLinkRetrieved(link, eventType);
            }
        });
    }

    /**
     * When entity link is retrieved, it loads work object linked to the task.
     * 
     * @param link
     *            The retrieved link.
     * @param eventType
     *            The event to forward to controllers when asset retrieving succeeds.
     */
    protected void onEntityLinkRetrieved(EntityTaskLinkModelData link, final EventType eventType)
    {
        final AssetRevisionGroupModelData group = new AssetRevisionGroupModelData();
        group.refreshByWorkObject(link.getBoundEntity(), link.getBoundEntityName(), new GetModelDataCallback(group) {
            @Override
            protected void onRecordReturned(List<Hd3dModelData> records)
            {
                Hd3dModelData newRecord = records.get(0);
                this.record.updateFromModelData(newRecord);

                onAssetGroupRetrieved(group, eventType);
            }
        });
    }

    /**
     * When asset group is retrieved, it retrieves assets.
     * 
     * @param group
     *            The retrieved group.
     * @param eventType
     *            The event to send when assets are retrieved.
     */
    private void onAssetGroupRetrieved(AssetRevisionGroupModelData group, EventType eventType)
    {
        group.retrieveAssets(this.getTaskTypeId(), eventType);
    }

    /**
     * @return Calculates end date by adding duration to start date and skipping week-ends.
     */
    public DateWrapper calculateEndDate()
    {
        if (this.getStartDate() != null && this.getDuration() != null)
        {
            int nbDays = (int) Math.ceil((float) getDuration() / (float) DatetimeUtil.DAY_SECONDS);

            DateWrapper startDate = new DateWrapper(getStartDate());
            DateWrapper endDate = new DateWrapper(startDate.asDate());
            if (nbDays > 0)
                endDate = endDate.addDays(nbDays - 1);

            int nbWeekendDays = DatetimeUtil.getNbWeekEndDayBetween(startDate, endDate.addDays(1));
            endDate = endDate.addDays(nbWeekendDays);
            endDate = DatetimeUtil.skipWeekend(endDate);
            return endDate;
        }
        return null;
    }

    /**
     * @return True if status is OK, CANCELLED or CLOSED.
     */
    public boolean isFinished()
    {
        String status = this.getStatus();
        return ETaskStatus.OK.toString().equals(status) || ETaskStatus.CANCELLED.toString().equals(status)
                || ETaskStatus.CLOSE.toString().equals(status);
    }

}
