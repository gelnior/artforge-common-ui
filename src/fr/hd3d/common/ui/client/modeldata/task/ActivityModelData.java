package fr.hd3d.common.ui.client.modeldata.task;

import java.util.Date;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;


/**
 * Activity Model Data represents activity instance on client side.
 * 
 * @author HD3D
 */
public class ActivityModelData extends Hd3dModelData
{
    private static final long serialVersionUID = 8769678596594930296L;

    public static final String DAY_ID_FIELD = "dayID";
    public static final String DAY_DATE_FIELD = "dayDate";
    public static final String DURATION_FIELD = "duration";
    public static final String COMMENT_FIELD = "comment";
    public static final String WORKER_ID_FIELD = "workerID";
    public static final String WORKER_NAME_FIELD = "workerName";
    public static final String WORKER_LOGIN_FIELD = "workerLogin";
    public static final String PROJECT_ID_FIELD = "projectID";
    public static final String PROJECT_NAME_FIELD = "projectName";
    public static final String PROJECT_COLOR_FIELD = "projectColor";
    public static final String PROJECT_TYPE_NAME_FIELD = "projectTypeName";
    public static final String FILLED_DATE_FIELD = "filledDate";
    public static final String FILLED_BY_ID_FIELD = "filledByID";
    public static final String FILLED_BY_NAME_FIELD = "filledByName";
    public static final String FILLED_BY_LOGIN_FIELD = "filledByLogin";

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LActivity";
    public static final String SIMPLE_CLASS_NAME = "Activity";

    public ActivityModelData()
    {
        super();

        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public Long getDayId()
    {
        return get(DAY_ID_FIELD);
    }

    public void setDayId(Long dayId)
    {
        set(DAY_ID_FIELD, dayId);
    }

    public Date getDayDate()
    {
        return (Date) get(DAY_DATE_FIELD);
    }

    public void setDayDate(Date dayDate)
    {
        set(DAY_DATE_FIELD, dayDate);
    }

    public Long getDuration()
    {
        Long duration = get(DURATION_FIELD);
        if (duration == null)
        {
            return 0L;
        }
        return duration;
    }

    public void setDuration(Long duration)
    {
        set(DURATION_FIELD, duration);
    }

    public String getComment()
    {
        return get(COMMENT_FIELD);
    }

    public void setComment(String comment)
    {
        set(COMMENT_FIELD, comment);
    }

    public Long getWorkerId()
    {
        return get(WORKER_ID_FIELD);
    }

    public void setWorkerId(Long workerId)
    {
        set(WORKER_ID_FIELD, workerId);
    }

    public String getWorkerName()
    {
        return get(WORKER_NAME_FIELD);
    }

    public void setWorkerName(String workerName)
    {
        set(WORKER_NAME_FIELD, workerName);
    }

    public String getWorkerLogin()
    {
        return get(WORKER_LOGIN_FIELD);
    }

    public void setWorkerLogin(String workerLogin)
    {
        set(WORKER_LOGIN_FIELD, workerLogin);
    }

    public Long getProjectId()
    {
        return get(PROJECT_ID_FIELD);
    }

    public void setProjectId(Long projectId)
    {
        set(PROJECT_ID_FIELD, projectId);
    }

    public String getProjectName()
    {
        return get(PROJECT_NAME_FIELD);
    }

    public void setProjectName(String projectName)
    {
        set(PROJECT_NAME_FIELD, projectName);
    }

    public String getProjectColor()
    {
        return get(PROJECT_COLOR_FIELD);
    }

    public void setProjectColor(String projectColor)
    {
        set(PROJECT_COLOR_FIELD, projectColor);
    }

    public Date getFilledDate()
    {
        return get(FILLED_DATE_FIELD);
    }

    public void setFilledDate(Date filledDate)
    {
        set(FILLED_DATE_FIELD, filledDate);
    }

    public Long getFilledById()
    {
        return get(FILLED_BY_ID_FIELD);
    }

    public void setFilledById(Long filledById)
    {
        set(FILLED_BY_ID_FIELD, filledById);
    }

    public String getFilledByName()
    {
        return get(FILLED_BY_NAME_FIELD);
    }

    public void setFilledByName(String filledByName)
    {
        set(FILLED_BY_NAME_FIELD, filledByName);
    }

    public String getFilledByLogin()
    {
        return get(FILLED_BY_LOGIN_FIELD);
    }

    public void setFilledByLogin(String filledByLogin)
    {
        set(FILLED_BY_LOGIN_FIELD, filledByLogin);
    }

    public String getProjectTypeName()
    {
        return get(PROJECT_TYPE_NAME_FIELD);
    }

    public void setProjectTypeName(String projectTypeName)
    {
        set(PROJECT_TYPE_NAME_FIELD, projectTypeName);
    }

    public void setProject(ProjectModelData project)
    {
        this.setProjectColor(project.getColor());
        this.setProjectId(project.getId());
        this.setProjectName(project.getName());
        this.setProjectTypeName(project.getProjectTypeName());
    }

    public static ModelType getModelType()
    {
        ModelType modelType = Hd3dModelData.getModelType();

        DataField dayIdField = new DataField(DAY_ID_FIELD);
        DataField dayDateField = new DataField(DAY_DATE_FIELD);
        DataField durationField = new DataField(DURATION_FIELD);
        DataField commentField = new DataField(COMMENT_FIELD);
        DataField workerIdField = new DataField(WORKER_ID_FIELD);
        DataField workerNameField = new DataField(WORKER_NAME_FIELD);
        DataField workerLoginField = new DataField(WORKER_LOGIN_FIELD);
        DataField projectIdField = new DataField(PROJECT_ID_FIELD);
        DataField projectNameField = new DataField(PROJECT_NAME_FIELD);
        DataField projectColorField = new DataField(PROJECT_COLOR_FIELD);
        DataField projectTypeNameField = new DataField(PROJECT_TYPE_NAME_FIELD);
        DataField filledField = new DataField(FILLED_DATE_FIELD);
        DataField filledByIdField = new DataField(FILLED_BY_ID_FIELD);
        DataField filledByNameField = new DataField(FILLED_BY_NAME_FIELD);
        DataField filledByLoginField = new DataField(FILLED_BY_LOGIN_FIELD);

        dayIdField.setType(Long.class);
        dayDateField.setType(Date.class);
        dayDateField.setFormat(TIMESTAMP_FORMAT_PATTERN);
        durationField.setType(Long.class);
        commentField.setType(String.class);
        workerIdField.setType(Long.class);
        workerNameField.setType(String.class);
        workerLoginField.setType(Long.class);
        projectIdField.setType(Long.class);
        projectNameField.setType(String.class);
        projectColorField.setType(String.class);
        projectTypeNameField.setType(String.class);
        filledField.setType(Date.class);
        filledField.setFormat(TIMESTAMP_FORMAT_PATTERN);
        filledByIdField.setType(Long.class);
        filledByNameField.setType(String.class);
        filledByLoginField.setType(String.class);

        modelType.addField(dayIdField);
        modelType.addField(dayDateField);
        modelType.addField(durationField);
        modelType.addField(commentField);
        modelType.addField(workerIdField);
        modelType.addField(workerNameField);
        modelType.addField(workerLoginField);
        modelType.addField(projectIdField);
        modelType.addField(projectNameField);
        modelType.addField(projectColorField);
        modelType.addField(projectTypeNameField);
        modelType.addField(filledField);
        modelType.addField(filledByIdField);
        modelType.addField(filledByNameField);
        modelType.addField(filledByLoginField);

        return modelType;
    }
}
