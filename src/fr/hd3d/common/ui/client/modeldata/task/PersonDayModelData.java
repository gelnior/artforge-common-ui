package fr.hd3d.common.ui.client.modeldata.task;

import java.util.Date;
import java.util.List;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.util.DateWrapper;
import com.google.gwt.json.client.JSONObject;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.callback.GetOrCreateModelDataCallback;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.Constraints;
import fr.hd3d.common.ui.client.service.parameter.ParameterBuilder;


public class PersonDayModelData extends Hd3dModelData
{
    private static final long serialVersionUID = 7989821201199058692L;

    public static final String DATE_FIELD = "date";
    public static final String PERSON_ID_FIELD = "personID";
    public static final String PERSON_NAME_FIELD = "personName";
    public static final String PERSON_WORKING_TIME_START1_FIELD = "startDate1";
    public static final String PERSON_WORKING_TIME_END1_FIELD = "endDate1";
    public static final String PERSON_WORKING_TIME_START2_FIELD = "startDate2";
    public static final String PERSON_WORKING_TIME_END2_FIELD = "endDate2";
    public static final String PERSON_ACTIVITIES = "activities";

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LPersonDay";
    public static final String SIMPLE_CLASS_NAME = "PersonDay";

    public PersonDayModelData()
    {
        super();

        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    @Override
    public String getPath()
    {
        if (this.getDefaultPath() != null)
        {
            return this.getDefaultPath();
        }

        if (this.getPersonId() == null)
        {
            return ServicesPath.PERSON_DAYS;
        }

        if (this.getId() == null)
            return ServicesPath.PERSONS + this.getPersonId() + "/" + ServicesPath.PERSON_DAYS;
        else
            return ServicesPath.PERSONS + this.getPersonId() + "/" + ServicesPath.PERSON_DAYS + this.getId();
    }

    public Date getDate()
    {
        return get(DATE_FIELD);
    }

    public void setDate(Date date)
    {
        set(DATE_FIELD, date);
    }

    public Long getPersonId()
    {
        return get(PERSON_ID_FIELD);
    }

    public void setPersonId(Long id)
    {
        set(PERSON_ID_FIELD, id);
    }

    public String getPersonName()
    {
        return get(PERSON_NAME_FIELD);
    }

    public void setPersonName(String name)
    {
        set(PERSON_NAME_FIELD, name);
    }

    public void setStartDate1(Date date)
    {
        set(PERSON_WORKING_TIME_START1_FIELD, date);
    }

    public Date getStartDate1()
    {
        return (Date) get(PERSON_WORKING_TIME_START1_FIELD);
    }

    public void setEndDate1(Date date)
    {
        set(PERSON_WORKING_TIME_END1_FIELD, date);
    }

    public Date getEndDate1()
    {
        return (Date) get(PERSON_WORKING_TIME_END1_FIELD);
    }

    public void setStartDate2(Date date)
    {
        set(PERSON_WORKING_TIME_START2_FIELD, date);
    }

    public Date getStartDate2()
    {
        return (Date) get(PERSON_WORKING_TIME_START2_FIELD);
    }

    public void setEndDate2(Date date)
    {
        set(PERSON_WORKING_TIME_END2_FIELD, date);
    }

    public Date getEndDate2()
    {
        return (Date) get(PERSON_WORKING_TIME_END2_FIELD);
    }

    public List<JSONObject> getActivities()
    {
        return get(PERSON_ACTIVITIES);
    }

    public void setActivities(List<ActivityModelData> activities)
    {
        set(PERSON_ACTIVITIES, activities);
    }

    public void refreshOrCreate(EventType event)
    {
        if (this.getDate() != null && this.getPersonId() != null)
        {
            Constraints constraints = new Constraints();
            Constraint dateConstraint = new Constraint(EConstraintOperator.eq, DATE_FIELD, getDate());
            Constraint personConstraint = new Constraint(EConstraintOperator.eq, "person.id", getPersonId());
            constraints.add(dateConstraint);
            constraints.add(personConstraint);

            String path = this.getPath() + ParameterBuilder.parameterToString(constraints);

            IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
            requestHandler.getRequest(path, new GetOrCreateModelDataCallback(this, new AppEvent(event)));
        }
    }

    public static ModelType getModelType()
    {
        ModelType modelType = Hd3dModelData.getModelType();

        DataField dateField = new DataField(DATE_FIELD);
        dateField.setType(Date.class);
        dateField.setFormat(TIMESTAMP_FORMAT_PATTERN);

        DataField activitiesField = new DataField(PERSON_ACTIVITIES);
        activitiesField.setType(List.class);

        DataField startDate1TimeField = new DataField(PERSON_WORKING_TIME_START1_FIELD);
        startDate1TimeField.setType(Date.class);
        startDate1TimeField.setFormat(TIMESTAMP_FORMAT_PATTERN);

        DataField endDate1TimeField = new DataField(PERSON_WORKING_TIME_END1_FIELD);
        endDate1TimeField.setType(Date.class);
        endDate1TimeField.setFormat(TIMESTAMP_FORMAT_PATTERN);

        DataField startDate2TimeField = new DataField(PERSON_WORKING_TIME_START2_FIELD);
        startDate2TimeField.setType(Date.class);
        startDate2TimeField.setFormat(TIMESTAMP_FORMAT_PATTERN);

        DataField endDate2TimeField = new DataField(PERSON_WORKING_TIME_END2_FIELD);
        endDate2TimeField.setType(Date.class);
        endDate2TimeField.setFormat(TIMESTAMP_FORMAT_PATTERN);

        DataField personIdField = new DataField(PERSON_ID_FIELD);
        personIdField.setType(Long.class);

        modelType.addField(dateField);
        modelType.addField(personIdField);
        modelType.addField(PERSON_NAME_FIELD);
        modelType.addField(startDate1TimeField);
        modelType.addField(endDate1TimeField);
        modelType.addField(startDate2TimeField);
        modelType.addField(endDate2TimeField);
        modelType.addField(activitiesField);

        return modelType;
    }

    /**
     * @return String at "HH:mm" format giving amount of time from arrival to leaving time with break duration removed
     *         from result, i.e : result = leaving - arrival - break duration.
     */
    public String getPresenceTime()
    {
        Long total = calculPresenceTime();

        if (total > 0)
        {
            String totalString = "";

            int hours = (int) Math.floor(total / (double) 3600);
            if (hours < 10)
                totalString += "0";
            totalString += Integer.toString(hours);

            totalString += "h";

            int minutes = (int) ((total % 3600) / (double) 60);
            if (minutes != 0)
            {
                if (minutes < 10)
                    totalString += "0";
                totalString += Integer.toString(minutes);
            }

            return totalString;
        }

        return null;
    }

    /**
     * @return String at "HH:mm" format giving amount of time from arrival to leaving time with break duration removed
     *         from result, i.e : result = leaving - arrival - break duration.
     */
    public Long calculPresenceTime()
    {
        Long total = 0L;

        if (getStartDate1() != null)
        {
            if (getEndDate2() != null)
            {
                if (getStartDate2() == null || getEndDate1() == null)
                {
                    if (getStartDate1().before(getEndDate2()))
                    {
                        DateWrapper startDate = new DateWrapper(this.getStartDate1());
                        DateWrapper endDate = new DateWrapper(this.getEndDate2());
                        total += Long.valueOf(endDate.getHours() * 3600);
                        total += Long.valueOf(endDate.getMinutes() * 60);
                        total -= Long.valueOf(startDate.getHours() * 3600);
                        total -= Long.valueOf(startDate.getMinutes() * 60);
                    }
                }
                else
                {
                    DateWrapper startDate = new DateWrapper(this.getStartDate1());
                    DateWrapper endDate = new DateWrapper(this.getEndDate1());
                    total += Long.valueOf(endDate.getHours() * 3600);
                    total += Long.valueOf(endDate.getMinutes() * 60);
                    total -= Long.valueOf(startDate.getHours() * 3600);
                    total -= Long.valueOf(startDate.getMinutes() * 60);

                    startDate = new DateWrapper(this.getStartDate2());
                    endDate = new DateWrapper(this.getEndDate2());
                    total += Long.valueOf(endDate.getHours() * 3600);
                    total += Long.valueOf(endDate.getMinutes() * 60);
                    total -= Long.valueOf(startDate.getHours() * 3600);
                    total -= Long.valueOf(startDate.getMinutes() * 60);
                }
            }
            else
            {
                if (getEndDate1() != null)
                {
                    if (getStartDate1().before(getEndDate1()))
                    {
                        DateWrapper startDate = new DateWrapper(this.getStartDate1());
                        DateWrapper endDate = new DateWrapper(this.getEndDate1());
                        total += Long.valueOf(endDate.getHours() * 3600);
                        total += Long.valueOf(endDate.getMinutes() * 60);
                        total -= Long.valueOf(startDate.getHours() * 3600);
                        total -= Long.valueOf(startDate.getMinutes() * 60);
                    }
                }
            }
        }
        else
        {
            if (getStartDate2() != null && getEndDate2() != null)
            {
                if (getStartDate2().before(getEndDate2()))
                {
                    DateWrapper startDate = new DateWrapper(this.getStartDate2());
                    DateWrapper endDate = new DateWrapper(this.getEndDate2());
                    total += Long.valueOf(endDate.getHours() * 3600);
                    total += Long.valueOf(endDate.getMinutes() * 60);
                    total -= Long.valueOf(startDate.getHours() * 3600);
                    total -= Long.valueOf(startDate.getMinutes() * 60);
                }
            }
        }

        return total;
    }
}
