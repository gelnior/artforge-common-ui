package fr.hd3d.common.ui.client.modeldata.task;

import java.util.Date;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.client.Const;
import fr.hd3d.common.ui.client.util.field.LongDataField;


/**
 * Activity Model Data represents activity instance on client side.
 * 
 * @author HD3D
 */
public class TaskActivityModelData extends ActivityModelData
{
    private static final long serialVersionUID = 648619267708897087L;

    public static final String TASK_ID_FIELD = "taskID";
    public static final String TASK_NAME_FIELD = "taskName";
    public static final String TASK_DURATION_FIELD = "taskDuration";

    public static final String TASK_TYPE_ID_FIELD = "taskTypeId";
    public static final String TASK_TYPE_NAME_FIELD = "taskTypeName";
    public static final String TASK_TYPE_COLOR_FIELD = "taskTypeColor";
    public static final String WORK_OBJECT_NAME_FIELD = "workObjectName";
    public static final String WORK_OBJECT_ID_FIELD = "workObjectId";
    public static final String WORK_OBJECT_PARENTS_NAME_FIELD = "workObjectParentsNames";
    public static final String TASK_STATUS_FIELD = "taskStatus";
    public static final String TASK_START_DATE_FIELD = "startDate";
    public static final String TASK_END_DATE_FIELD = "endDate";
    public static final String WO_NAME_FIELD = "workObjectName";
    public static final String WORK_OBJECT_ENTITY_FIELD = "workObjectEntity";
    public static final String TASK_TOTAL_ACTIVITY_DURATION_FIELD = Const.TOTAL_ACTIVITIES_DURATION;

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LTaskActivity";
    public static final String SIMPLE_CLASS_NAME = "TaskActivity";

    public TaskActivityModelData()
    {
        super();

        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public Long getTaskId()
    {
        return get(TASK_ID_FIELD);
    }

    public void setTaskId(Long id)
    {
        set(TASK_ID_FIELD, id);
    }

    public String getTaskName()
    {
        return get(TASK_NAME_FIELD);
    }

    public void setTaskName(String name)
    {
        set(TASK_NAME_FIELD, name);
    }

    public Long getTaskTypeId()
    {
        return get(TASK_TYPE_ID_FIELD);
    }

    /**
     * @return Task duration. 0 if no duration is set.
     */
    public Long getTaskDuration()
    {
        Long duration = get(TASK_DURATION_FIELD);
        if (duration == null)
        {
            duration = 0L;
        }
        return duration;
    }

    public void setTaskDuration(Long duration)
    {
        set(TASK_DURATION_FIELD, duration);
    }

    @Override
    public void setDuration(Long duration)
    {
        Long previousDuration = this.getDuration();
        Long diff = duration - previousDuration;

        this.setTotalActivityDuration(this.getTotalActivityDuration() + diff);
        this.set(DURATION_FIELD, duration);
    }

    public Long getTotalActivityDuration()
    {
        Long duration = get(TASK_TOTAL_ACTIVITY_DURATION_FIELD);
        if (duration == null)
        {
            duration = 0L;
        }
        return duration;
    }

    public void setTotalActivityDuration(Long totalActivityDuration)
    {
        set(TASK_TOTAL_ACTIVITY_DURATION_FIELD, totalActivityDuration);
    }

    public void setTaskTypeId(Long id)
    {
        set(TASK_TYPE_ID_FIELD, id);
    }

    public String getTaskTypeName()
    {
        return get(TASK_TYPE_NAME_FIELD);
    }

    public void setTaskTypeName(String name)
    {
        set(TASK_TYPE_NAME_FIELD, name);
    }

    public String getTaskTypeColor()
    {
        return get(TASK_TYPE_COLOR_FIELD);
    }

    public void setTaskTypeColor(String color)
    {
        set(TASK_TYPE_COLOR_FIELD, color);
    }

    public String getWorkObjectName()
    {
        return get(WORK_OBJECT_NAME_FIELD);
    }

    public void setWorkObjectName(String name)
    {
        set(WORK_OBJECT_NAME_FIELD, name);
    }

    public String getWorkObjectParentsName()
    {
        return get(WORK_OBJECT_PARENTS_NAME_FIELD);
    }

    public void setWorkObjectParentsName(String name)
    {
        set(WORK_OBJECT_PARENTS_NAME_FIELD, name);
    }

    public String getTaskStatus()
    {
        return get(TASK_STATUS_FIELD);
    }

    public String setTaskStatus(String status)
    {
        return set(TASK_STATUS_FIELD, status);
    }

    public void setWorkObjectId(Long workObjectId)
    {
        set(WORK_OBJECT_ID_FIELD, workObjectId);
    }

    public Long getWorkObjectId()
    {
        return get(WORK_OBJECT_ID_FIELD);
    }

    public Date getTaskStartDate()
    {
        return get(TASK_START_DATE_FIELD);
    }

    public void setTaskStartDate(Date startDate)
    {
        set(TASK_START_DATE_FIELD, startDate);
    }

    public Date getTaskEndDate()
    {
        return get(TASK_END_DATE_FIELD);
    }

    public void setTaskEndDate(Date endDate)
    {
        set(TASK_END_DATE_FIELD, endDate);
    }

    public String getWorkObjectEntity()
    {
        return get(WORK_OBJECT_ENTITY_FIELD);
    }

    public void setWorkObjectEntity(String entity)
    {
        set(WORK_OBJECT_ENTITY_FIELD, entity);
    }

    /**
     * Filled all task related field with data from <i>task</i>.
     * 
     * @param task
     *            The task from which data are extracted to fill task fields.
     */
    public void setTaskFields(TaskModelData task)
    {
        this.setTaskId(task.getId());
        this.setProjectId(task.getProjectID());
        this.setProjectName(task.getProjectName());
        this.setProjectColor(task.getProjectColor());
        this.setProjectTypeName(task.getProjectTypeName());
        this.setTaskTypeName(task.getTaskTypeName());
        this.setTaskName(task.getName());
        this.setTaskStatus(task.getStatus());
        this.setTaskDuration(task.getDuration());
        this.setTaskTypeColor(task.getColor());
        this.setWorkObjectName(task.getWorkObjectName());
        this.setWorkObjectParentsName(task.getWorkObjectParentsName());
        this.setTotalActivityDuration(task.getTotalActivityDuration());
        this.setTaskStartDate(task.getStartDate());
        this.setTaskEndDate(task.getEndDate());
    }

    public static ModelType getModelType()
    {
        ModelType modelType = ActivityModelData.getModelType();

        DataField taskDurationField = new DataField(TASK_DURATION_FIELD);
        taskDurationField.setType(Long.class);

        modelType.addField(new LongDataField(TASK_ID_FIELD));
        modelType.addField(TASK_NAME_FIELD);
        modelType.addField(taskDurationField);
        modelType.addField(TASK_TYPE_NAME_FIELD);
        modelType.addField(TASK_TYPE_COLOR_FIELD);
        modelType.addField(WORK_OBJECT_NAME_FIELD);
        modelType.addField(WORK_OBJECT_PARENTS_NAME_FIELD);
        modelType.addField(TASK_STATUS_FIELD);
        modelType.addField(TASK_NAME_FIELD);
        modelType.addField(TASK_TYPE_NAME_FIELD);
        modelType.addField(new LongDataField(TASK_TYPE_ID_FIELD));
        modelType.addField(WO_NAME_FIELD);
        modelType.addField(WORK_OBJECT_ENTITY_FIELD);
        modelType.addField(new LongDataField(TASK_TOTAL_ACTIVITY_DURATION_FIELD));

        modelType.addField(new LongDataField(WORK_OBJECT_ID_FIELD));

        return modelType;
    }

}
