package fr.hd3d.common.ui.client.modeldata.task;

import java.util.Iterator;
import java.util.List;

import com.extjs.gxt.ui.client.data.BaseTreeModel;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.BaseTaskModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.util.field.LongDataField;
import fr.hd3d.common.ui.client.widget.planning.EPlanningDisplayMode;


public class TaskGroupModelData extends BaseTaskModelData
{
    private static final long serialVersionUID = -5742940384484585226L;

    public static final String NAME_FIELD = "name";
    public static final String COLOR_FIELD = "color";
    public static final String FILTER_FIELD = "filter";
    public static final String PLANNING_ID_FIELD = "planning";
    public static final String TASK_TYPE_ID_FIELD = "taskTypeId";
    public static final String EXTRA_LINE_IDS_FIELD = "extraLineIDs";

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LTaskGroup";
    public static final String SIMPLE_CLASS_NAME = "TaskGroup";

    public static final String CONSTITUENT_COLOR = "#AAFFAA";
    public static final String SHOT_COLOR = "#AAAAFF";

    public static ModelType getModelType()
    {
        ModelType modelType = BaseTaskModelData.getBaseModelType();

        modelType.addField(NAME_FIELD);
        modelType.addField(COLOR_FIELD);
        modelType.addField(FILTER_FIELD);
        modelType.addField(new LongDataField(PLANNING_ID_FIELD));
        modelType.addField(new LongDataField(TASK_TYPE_ID_FIELD));
        modelType.addField(EXTRA_LINE_IDS_FIELD);

        return modelType;
    }

    private EPlanningDisplayMode type = EPlanningDisplayMode.TASK_TYPE;

    private Long objectId = 0L;

    public TaskGroupModelData()
    {
        super();
        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public void setExtraLineIDs(List<Long> extraLineIDs)
    {
        set(EXTRA_LINE_IDS_FIELD, extraLineIDs);
    }

    public String getColor()
    {
        return (String) get(COLOR_FIELD);
    }

    public String getFilter()
    {
        return (String) get(FILTER_FIELD);
    }

    public Long getTaskTypeID()
    {
        return (Long) get(TASK_TYPE_ID_FIELD);
    }

    public List<Long> getExtraLineIDs()
    {
        return get(EXTRA_LINE_IDS_FIELD);
    }

    public void setColor(String color)
    {
        set(COLOR_FIELD, color);
    }

    public void setFilter(String filter)
    {
        set(FILTER_FIELD, filter);
    }

    public void setPlanningID(Long planningID)
    {
        set(PLANNING_ID_FIELD, planningID);
    }

    public void setTaskTypeID(Long taskTypeId)
    {
        set(TASK_TYPE_ID_FIELD, taskTypeId);
    }

    public void setPlanning(PlanningModelData planning)
    {
        this.setPlanningID(planning.getId());
        String path = planning.getDefaultPath() + "/" + ServicesPath.TASKGROUPS;
        if (this.getId() != null)
            path += this.getId();
        this.setDefaultPath(path);
    }

    public static String createFilter(BaseTreeModel treeModel, List<TaskTypeModelData> list)
    {
        StringBuffer filter = new StringBuffer();
        if (treeModel != null)
        {
            String type = treeModel.get("type");
            Long id = treeModel.get("id");
            filter.append("type=" + type + ";");
            filter.append("id=" + id + ";");
        }
        if (list.size() > 0)
        {
            filter.append("taskTypeIds=[");
            for (Iterator<TaskTypeModelData> iterator = list.iterator(); iterator.hasNext();)
            {
                TaskTypeModelData taskTypeModelData = iterator.next();
                filter.append(taskTypeModelData.getId() + ",");
            }
        }
        filter = filter.deleteCharAt(filter.length() - 1);
        if (list.size() > 0)
        {
            filter.append("]");
        }
        return filter.toString();
    }

    public static String translateFilter(String filterOptions, boolean isMaster)
    {
        StringBuffer hd3dConstraint = new StringBuffer();
        String[] params = filterOptions.split(";");
        for (int i = 0; i < params.length; i++)
        {
            String[] paramAndValue = params[i].split("=");
            hd3dConstraint.append("\"" + paramAndValue[0] + "\":");
            if (!paramAndValue[1].startsWith("["))
            {
                hd3dConstraint.append("\"" + paramAndValue[1] + "\",");
            }
            else
            {
                hd3dConstraint.append(paramAndValue[1] + ",");
            }
        }
        hd3dConstraint.append("?custom=[{" + hd3dConstraint);
        hd3dConstraint.append("\"master\":" + isMaster);
        hd3dConstraint.append("}]");
        return hd3dConstraint.toString();
    }

    public EPlanningDisplayMode getType()
    {
        return this.type;
    }

    public void setType(EPlanningDisplayMode type)
    {
        this.type = type;
    }

    public void setObjectId(Long id)
    {
        this.objectId = id;
    }

    public Long getObjectId()
    {
        return this.objectId;
    }

}
