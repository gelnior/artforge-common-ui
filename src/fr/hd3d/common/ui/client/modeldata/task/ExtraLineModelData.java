package fr.hd3d.common.ui.client.modeldata.task;

import java.util.List;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;


public class ExtraLineModelData extends Hd3dModelData
{
    private static final long serialVersionUID = -8892932656957948102L;

    public static final String NAME_FIELD = "name";
    public static final String PERSON_ID_FIELD = "hiddenPersonID";
    public static final String TASK_GROUP_ID_FIELD = "taskGroupID";
    public static final String TASK_BASE_IDS_FIELD = "taskBaseIDs";
    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LExtraLine";
    public static final String SIMPLE_CLASS_NAME = "extraLine";

    public static ModelType getModelType()
    {
        ModelType type = Hd3dModelData.getModelType();

        DataField taskGroupIDataField = new DataField(TASK_GROUP_ID_FIELD);
        DataField personIDataField = new DataField(PERSON_ID_FIELD);

        taskGroupIDataField.setType(Long.class);
        personIDataField.setType(Long.class);

        type.addField(NAME_FIELD);
        type.addField(taskGroupIDataField);
        type.addField(TASK_BASE_IDS_FIELD);
        type.addField(personIDataField);

        return type;
    }

    public ExtraLineModelData()
    {
        super();
        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public void setName(String name)
    {
        set(NAME_FIELD, name);
    }

    public void setTaskGroupID(Long taskGroupID)
    {
        set(TASK_GROUP_ID_FIELD, taskGroupID);
    }

    public void setTaskBaseIDs(List<Long> taskBaseIDs)
    {
        set(TASK_BASE_IDS_FIELD, taskBaseIDs);
    }

    public void setPersonID(Long personID)
    {
        set(PERSON_ID_FIELD, personID);
    }

    public String getName()
    {
        return get(NAME_FIELD);
    }

    public Long getTaskGroupID()
    {
        return get(TASK_GROUP_ID_FIELD);
    }

    public List<Long> getTaskBaseIDs()
    {
        return get(TASK_BASE_IDS_FIELD);
    }

    public Long getPersonID()
    {
        return get(PERSON_ID_FIELD);
    }

    public void setTaskGroup(TaskGroupModelData taskGroup)
    {
        this.setTaskGroupID(taskGroup.getId());
        String path = taskGroup.getDefaultPath() + "/" + ServicesPath.EXTRA_LINES;
        if (this.getId() != null)
        {
            path += this.getId();
        }
        this.setDefaultPath(path);
    }
}
