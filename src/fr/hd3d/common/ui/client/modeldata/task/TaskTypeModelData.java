package fr.hd3d.common.ui.client.modeldata.task;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;


public class TaskTypeModelData extends RecordModelData
{
    private static final long serialVersionUID = 7590357579205893277L;

    public static final String DESCRIPTION_FIELD = "description";
    public static final String ENTITY_NAME_FIELD = "entityName";
    public static final String COLOR_FIELD = "color";
    public static final String PROJECT_ID_FIELD = "projectID";

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LTaskType";
    public static final String SIMPLE_CLASS_NAME = "TaskType";

    public TaskTypeModelData()
    {
        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public TaskTypeModelData(String name)
    {
        this();
        this.setName(name);
    }

    public String getDescription()
    {
        return get(DESCRIPTION_FIELD);
    }

    public void setDescription(String description)
    {
        set(DESCRIPTION_FIELD, description);
    }

    public String getColor()
    {
        return get(COLOR_FIELD);
    }

    public void setColor(String color)
    {
        set(COLOR_FIELD, color);
    }

    public String getEntityName()
    {
        return get(ENTITY_NAME_FIELD);
    }

    public void setEntityName(String entityName)
    {
        set(ENTITY_NAME_FIELD, entityName);
    }

    public Long getProjectID()
    {
        return (Long) get(PROJECT_ID_FIELD);
    }

    public void setProjectID(Long projectID)
    {
        set(PROJECT_ID_FIELD, projectID);
    }

    public static ModelType getModelType()
    {
        ModelType modelType = Hd3dModelData.getModelType();
        modelType.addField(NAME_FIELD);
        modelType.addField(DESCRIPTION_FIELD);
        modelType.addField(COLOR_FIELD);
        modelType.addField(ENTITY_NAME_FIELD);

        DataField projectIdDataField = new DataField(PROJECT_ID_FIELD);
        projectIdDataField.setType(Long.class);
        modelType.addField(projectIdDataField);

        return modelType;
    }
}
