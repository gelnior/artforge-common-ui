package fr.hd3d.common.ui.client.modeldata.task;

import org.restlet.client.data.Method;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.modeldata.BaseTaskModelData;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.Constraints;
import fr.hd3d.common.ui.client.service.parameter.ParameterBuilder;


public class EntityTaskLinkModelData extends Hd3dModelData
{
    private static final long serialVersionUID = 7590357579205893277L;

    public static final String BOUND_ENTITY_ID_FIELD = "boundEntity";
    public static final String BOUND_ENTITY_NAME_FIELD = "boundEntityName";
    public static final String TASK_ID_FIELD = "task";

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LEntityTaskLink";
    public static final String SIMPLE_CLASS_NAME = "EntityTaskLink";

    public EntityTaskLinkModelData()
    {
        super();
        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public Long getBoundEntity()
    {
        return get(BOUND_ENTITY_ID_FIELD);
    }

    public String getBoundEntityName()
    {
        return get(BOUND_ENTITY_NAME_FIELD);
    }

    public Long getTaskId()
    {
        return get(TASK_ID_FIELD);
    }

    public void setBoundEntity(Long boundEntity)
    {
        set(BOUND_ENTITY_ID_FIELD, boundEntity);
    }

    public void setBoundEntityName(String boundEntityName)
    {
        set(BOUND_ENTITY_NAME_FIELD, boundEntityName);
    }

    public void setTaskId(Long taskId)
    {
        set(TASK_ID_FIELD, taskId);
    }

    public static ModelType getModelType()
    {
        ModelType modelType = BaseTaskModelData.getBaseModelType();
        DataField boundEntityIdDataField = new DataField(BOUND_ENTITY_ID_FIELD);
        DataField taskIdDataField = new DataField(TASK_ID_FIELD);

        boundEntityIdDataField.setType(Long.class);
        taskIdDataField.setType(Long.class);

        modelType.addField(boundEntityIdDataField);
        modelType.addField(BOUND_ENTITY_NAME_FIELD);
        modelType.addField(taskIdDataField);
        return modelType;
    }

    public void refreshByTask(TaskModelData task, BaseCallback callback)
    {
        Constraints constraints = new Constraints();
        constraints.add(new Constraint(EConstraintOperator.eq, TASK_ID_FIELD, task.getId(), null));
        constraints.add(new Constraint(EConstraintOperator.eq, BOUND_ENTITY_ID_FIELD, task.getWorkObjectId(), null));
        // constraints.add(new Constraint(EConstraintOperator.eq, BOUND_ENTITY_NAME_FIELD, ,
        // null));
        String path = this.getPath() + ParameterBuilder.parameterToString(constraints);

        IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
        requestHandler.handleRequest(Method.GET, path, null, callback);
    }
}
