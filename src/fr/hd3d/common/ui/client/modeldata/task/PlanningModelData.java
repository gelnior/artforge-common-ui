package fr.hd3d.common.ui.client.modeldata.task;

import java.util.Date;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;


public class PlanningModelData extends RecordModelData
{

    public static final long serialVersionUID = 1902181238971849595L;

    public static final String NAME_FIELD = "name";
    public static final String PROJECT_FIELD = "project";
    public static final String MASTER_FIELD = "master";
    public static final String START_DATE_FIELD = "startDate";
    public static final String END_DATE_FIELD = "endDate";

    public static final String SIMPLE_CLASS_NAME = "Planning";
    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LPlanning";

    public PlanningModelData()
    {
        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public void setProjectID(Long projectID)
    {
        set(PROJECT_FIELD, projectID);
    }

    public void setMaster(boolean master)
    {
        set(MASTER_FIELD, master);
    }

    public void setStartDate(Date date)
    {
        set(START_DATE_FIELD, date);
    }

    public void setEndDate(Date date)
    {
        set(END_DATE_FIELD, date);
    }

    public Long getProjectID()
    {
        return (Long) get(PROJECT_FIELD);
    }

    public Boolean isMaster()
    {
        return get(MASTER_FIELD);
    }

    public Date getStartDate()
    {
        return (Date) get(START_DATE_FIELD);
    }

    public Date getEndDate()
    {
        return (Date) get(END_DATE_FIELD);
    }

    public static ModelType getModelType()
    {
        ModelType modelType = Hd3dModelData.getModelType();

        DataField projectIdDataField = new DataField(PROJECT_FIELD);
        DataField masterDataField = new DataField(MASTER_FIELD);
        DataField startDateField = new DataField(START_DATE_FIELD);
        DataField endDateField = new DataField(END_DATE_FIELD);

        projectIdDataField.setType(Long.class);
        masterDataField.setType(Boolean.class);
        startDateField.setType(Date.class);
        startDateField.setFormat(TIMESTAMP_FORMAT_PATTERN);
        endDateField.setType(Date.class);
        endDateField.setFormat(TIMESTAMP_FORMAT_PATTERN);

        modelType.addField(projectIdDataField);
        modelType.addField(NAME_FIELD);
        modelType.addField(masterDataField);
        modelType.addField(startDateField);
        modelType.addField(endDateField);

        return modelType;
    }

    public void setProject(ProjectModelData project)
    {
        String path = project.getDefaultPath() + "/" + ServicesPath.PLANNINGS;
        if (getId() != null)
        {
            path += path + getId();
        }
        this.setDefaultPath(path);

        this.setProjectID(project.getId());
    }
}
