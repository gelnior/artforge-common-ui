package fr.hd3d.common.ui.client.modeldata.task;

import com.extjs.gxt.ui.client.core.FastMap;

import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.util.FieldUtils;


/**
 * Map containing all task status where status is the key and a field model data with name and status as field value is
 * the map value.
 * 
 * @author HD3D
 */
public class TaskStatusMap extends FastMap<FieldModelData>
{
    private static final long serialVersionUID = 4585914586916480649L;

    public static final String OK_COLOR = "#9BD46D";
    public static final String RETAKE_COLOR = "#EE5555";
    public static final String WAIT_APP_COLOR = "#FFAA00";
    public static final String CANCELLED_COLOR = "#999";
    public static final String STAND_BY_COLOR = "#DDDDDD";
    public static final String WIP_COLOR = "#FE6";
    public static final String TO_DO_COLOR = "#88AAFF";

    public static final String NEW_COLOR = "#FFF";
    public static final String NOK_COLOR = "red";
    public static final String CLOSED_COLOR = "#8C99C9";

    /** static map for retrieving colors corresponding to task status. */
    private static FastMap<String> colors = new FastMap<String>();

    /**
     * @param status
     *            Status of which color is requested.
     * @return Color corresponding to <i>status</i>.
     */
    public static String getColorForStatus(String status)
    {
        if (colors.isEmpty())
        {
            colors.put(ETaskStatus.OK.toString(), OK_COLOR);
            colors.put(ETaskStatus.WAIT_APP.toString(), WAIT_APP_COLOR);
            colors.put(ETaskStatus.CANCELLED.toString(), CANCELLED_COLOR);
            colors.put(ETaskStatus.STAND_BY.toString(), STAND_BY_COLOR);
            colors.put(ETaskStatus.WORK_IN_PROGRESS.toString(), WIP_COLOR);
            colors.put(ETaskStatus.CLOSE.toString(), CLOSED_COLOR);
            colors.put(ETaskStatus.TO_DO.toString(), TO_DO_COLOR);
            colors.put(ETaskStatus.RETAKE.toString(), RETAKE_COLOR);
            colors.put(ETaskStatus.NEW.toString(), NEW_COLOR);
        }

        return colors.get(status);
    }

    public TaskStatusMap()
    {
        super();
        FieldUtils.addFieldToMap(this, 0, "Waiting for approval", ETaskStatus.WAIT_APP.toString());
        FieldUtils.addFieldToMap(this, 1, "Stand By", ETaskStatus.STAND_BY.toString());
        FieldUtils.addFieldToMap(this, 2, "Work in Progress", ETaskStatus.WORK_IN_PROGRESS.toString());
        FieldUtils.addFieldToMap(this, 3, "To do", ETaskStatus.TO_DO.toString());
        FieldUtils.addFieldToMap(this, 4, "OK", ETaskStatus.OK.toString());
        FieldUtils.addFieldToMap(this, 5, "Cancelled", ETaskStatus.CANCELLED.toString());
        FieldUtils.addFieldToMap(this, 6, "Closed", ETaskStatus.CLOSE.toString());
        FieldUtils.addFieldToMap(this, 7, "Retake", ETaskStatus.RETAKE.toString());
    }
}
