package fr.hd3d.common.ui.client.modeldata.task;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;


/**
 * Activity Model Data represents activity instance on client side.
 * 
 * @author HD3D
 */
public class SimpleActivityModelData extends ActivityModelData
{
    private static final long serialVersionUID = 8769678596594930296L;

    public static final String TYPE_FIELD = "type";

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LSimpleActivity";
    public static final String SIMPLE_CLASS_NAME = "SimpleActivity";

    public SimpleActivityModelData()
    {
        super();

        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public String getType()
    {
        return get(TYPE_FIELD);
    }

    public void setType(String type)
    {
        set(TYPE_FIELD, type);
    }

    public static ModelType getModelType()
    {
        ModelType modelType = ActivityModelData.getModelType();

        DataField typeField = new DataField(TYPE_FIELD);
        typeField.setType(String.class);

        modelType.addField(typeField);

        return modelType;
    }

}
