package fr.hd3d.common.ui.client.modeldata.task;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.BaseTaskModelData;


public class TaskChangesModelData extends BaseTaskModelData
{

    /**
     * 
     */
    private static final long serialVersionUID = -8226543744427804671L;

    public static final String WORKER_ID_FIELD = "workerID";
    public static final String PLANNING_ID_FIELD = "planningID";
    public static final String TASK_ID_FIELD = "taskID";
    public static final String TASK_NAME_FIELD = "taskName";
    public static final String PERSON_NAME_FIELD = "personName";
    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LTaskChanges";
    public static final String WORK_OBJECT_NAME_FIELD = "workObjectName";
    public static final String COLOR_FIELD = "color";

    public static final String SIMPLE_CLASS_NAME = "taskChanges";

    public TaskChangesModelData()
    {
        super();
        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    public void setWorkerID(Long personID)
    {
        set(WORKER_ID_FIELD, personID);
    }

    public void setPlanningID(Long planningID)
    {
        set(PLANNING_ID_FIELD, planningID);
    }

    public void setTaskID(Long taskID)
    {
        set(TASK_ID_FIELD, taskID);
    }

    public void setTaskName(String taskName)
    {
        set(TASK_NAME_FIELD, taskName);
    }

    public void setPersonName(String personName)
    {
        set(PERSON_NAME_FIELD, personName);
    }

    public void setColor(String color)
    {
        set(COLOR_FIELD, color);
    }

    public void setWorkObjectName(String workObjectName)
    {
        this.set(WORK_OBJECT_NAME_FIELD, workObjectName);
    }

    public Long getWorkerID()
    {
        return (Long) get(WORKER_ID_FIELD);
    }

    public Long getPlanningID()
    {
        return (Long) get(PLANNING_ID_FIELD);
    }

    public Long getTaskID()
    {
        return (Long) get(TASK_ID_FIELD);
    }

    public String getTaskName()
    {
        return get(TASK_NAME_FIELD);
    }

    public String getPersonName()
    {
        return get(PERSON_NAME_FIELD);
    }

    public String getColor()
    {
        return get(COLOR_FIELD);
    }

    public String getWorkObjectName()
    {
        return (String) get(WORK_OBJECT_NAME_FIELD);
    }

    public static ModelType getModelType()
    {
        ModelType modelType = BaseTaskModelData.getBaseModelType();
        DataField personIdDataField = new DataField(WORKER_ID_FIELD);
        DataField planningIdDataField = new DataField(PLANNING_ID_FIELD);
        DataField taskIdDataField = new DataField(TASK_ID_FIELD);

        personIdDataField.setType(Long.class);
        planningIdDataField.setType(Long.class);
        taskIdDataField.setType(Long.class);

        modelType.addField(personIdDataField);
        modelType.addField(taskIdDataField);
        modelType.addField(planningIdDataField);
        modelType.addField(TASK_NAME_FIELD);
        modelType.addField(PERSON_NAME_FIELD);
        modelType.addField(COLOR_FIELD);
        modelType.addField(WORK_OBJECT_NAME_FIELD);
        return modelType;
    }
}
