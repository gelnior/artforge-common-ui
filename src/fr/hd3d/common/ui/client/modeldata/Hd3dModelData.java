package fr.hd3d.common.ui.client.modeldata;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.extjs.gxt.ui.client.data.BaseModelData;
import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.clipboard.Copyable;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.modeldata.technical.DynValueModelData;
import fr.hd3d.common.ui.client.modeldata.writer.IModelDataJsonWriter;
import fr.hd3d.common.ui.client.modeldata.writer.ModelDataJsonWriterSingleton;
import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.PermissionPath;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.service.callback.DeleteModelDataCallback;
import fr.hd3d.common.ui.client.service.callback.GetModelDataCallback;
import fr.hd3d.common.ui.client.service.callback.GetOrCreateModelDataCallback;
import fr.hd3d.common.ui.client.service.callback.PostModelDataCallback;
import fr.hd3d.common.ui.client.service.callback.PutModelDataCallback;
import fr.hd3d.common.ui.client.service.parameter.IUrlParameter;
import fr.hd3d.common.ui.client.service.parameter.ParameterBuilder;


/**
 * This model data contains mutual elements for all json objects coming from HD3D web services. It also provides method
 * to facilitate linking with data base (create, retrieve, delete, update for a single object).
 * 
 * @author HD3D
 */
public class Hd3dModelData extends BaseModelData implements Copyable
{
    /** Auto generated serial number. */
    private static final long serialVersionUID = -8350198516136879838L;

    /** Object ID */
    public static final String ID_FIELD = "id";
    /** Object version (timestamp) */
    public static final String VERSION_FIELD = "version";
    /** Object class name used by hibernate to handle persistence. */
    public static final String CLASS_NAME_FIELD = "class";

    /** Whether the user can update the object **/
    public static final String USER_CAN_UPDATE_FIELD = "userCanUpdate";
    /** Whether the user can delete the object **/
    public static final String USER_CAN_DELETE_FIELD = "userCanDelete";
    /** The default path to the object (might be null) **/
    public static final String DEFAULT_PATH_FIELD = "defaultPath";

    /** Name of the field containing attached tags IDs. */
    public static final String TAG_IDS_FIELD = "tags";
    /** Name of the field containing attached tags names. */
    public static final String TAG_NAMES_FIELD = "tagNames";
    /** Name of the field containing attached tasks IDs. */
    public static final String BOUND_TASKS_FIELD = "boundTasks";
    /** Name of the field containing dyn values */
    public static final String DYN_VALUES_FIELD = "dynMetaDataValues";

    /** Root object name when data are retrieved from HD3D web services at json format. */
    public static final String ROOT = "records";
    /**
     * Field name that gives total number of records the request should return when there is no pagination constraint.
     * Used when data are retrieved from HD3D web services at json format.
     */
    public static final String NB_RECORDS = "nbRecords";

    /** Date format used by database timestamps. */
    public static final String TIMESTAMP_FORMAT_PATTERN = "yyyy-MM-dd HH:mm:ss.S";

    /**
     * Copy data from an object to another one.
     * 
     * @param fromObject
     *            The object that will be copied.
     * @param toObject
     *            The copy of the from object.
     */
    public static void copy(Hd3dModelData fromObject, Hd3dModelData toObject)
    {
        for (Map.Entry<String, Object> entry : fromObject.map.entrySet())
        {
            String key = entry.getKey();
            Object value = entry.getValue();
            toObject.set(key, value);
        }
    }

    /** Server side entity name. */
    protected String simpleClassName;

    /**
     * @return Base type at GXT format for a simple HD3D model data.
     */
    public static ModelType getModelType()
    {
        ModelType type = new ModelType();
        type.setRoot(ROOT);
        type.setTotalName(NB_RECORDS);

        DataField idDataField = new DataField(Hd3dModelData.ID_FIELD);
        idDataField.setType(Long.class);

        DataField tagIdsDataField = new DataField(TAG_IDS_FIELD);
        tagIdsDataField.setType(List.class);
        tagIdsDataField.setFormat("Tag");

        DataField boundTasksField = new DataField(BOUND_TASKS_FIELD);
        boundTasksField.setType(List.class);
        boundTasksField.setFormat("Task");

        DataField dynValuesField = new DataField(DYN_VALUES_FIELD);
        boundTasksField.setType(List.class);

        DataField tagNamesDataField = new DataField(TAG_NAMES_FIELD);
        tagNamesDataField.setType(List.class);

        type.addField(idDataField);
        type.addField(CLASS_NAME_FIELD);
        type.addField(USER_CAN_UPDATE_FIELD);
        type.addField(USER_CAN_DELETE_FIELD);
        type.addField(DEFAULT_PATH_FIELD);
        type.addField(tagIdsDataField);
        type.addField(tagNamesDataField);
        type.addField(boundTasksField);
        type.addField(dynValuesField);

        return type;
    }

    /** Default constructor. */
    public Hd3dModelData()
    {

    }

    /**
     * @return The simplified class name (with no package prefix) that correspond to the entity class name on server.
     */
    public String getSimpleClassName()
    {
        return this.simpleClassName;
    }

    /**
     * Set the simplified class name (with no package prefix) that correspond to the entity class name on server.
     * 
     * @param className
     *            The class name to set.
     */
    public void setSimpleClassName(String className)
    {
        this.simpleClassName = className;
    }

    /**
     * Replace object data with the data from the object given in parameter.
     * 
     * @param model
     *            The object with data to set.
     */
    public void updateFromModelData(Hd3dModelData model)
    {
        this.map.clear();

        for (String s : model.getProperties().keySet())
        {
            this.set(s, model.getProperties().get(s));
        }
    }

    /**
     * @return ID field.
     */
    public final Long getId()
    {
        Object object = get(ID_FIELD);
        if (object instanceof Double)
        {
            Double value = (Double) object;
            return Long.valueOf(value.longValue());
        }
        return (Long) object;
    }

    /**
     * Sets ID field.
     * 
     * @param id
     *            the value to set.
     */
    public final void setId(Long id)
    {
        set(ID_FIELD, id);
    }

    /**
     * @return Version field.
     */
    public final String getVersion()
    {
        return get(VERSION_FIELD);
    }

    /**
     * Sets version field.
     * 
     * @param date
     */
    public final void setVersion(String date)
    {
        set(VERSION_FIELD, date);
    }

    /**
     * @return Server class name for hibernate persistence.
     */
    public final String getClassName()
    {
        return get(CLASS_NAME_FIELD);
    }

    /**
     * Set server class name for hibernate persistence.
     * 
     * @param className
     *            The class name
     */
    public final void setClassName(String className)
    {
        set(CLASS_NAME_FIELD, className);
    }

    /**
     * @return true if the user can update this object
     */
    public final boolean getUserCanUpdate()
    {
        Boolean can = get(USER_CAN_UPDATE_FIELD);
        if (can == null)
        {
            return true; // if we don't know, let's assume we can
        }
        return can.booleanValue();
    }

    /**
     * 
     * @param canUpdate
     *            whether the user can update the object
     */
    public final void setUserCanUpdate(Boolean canUpdate)
    {
        set(USER_CAN_UPDATE_FIELD, canUpdate);
    }

    /**
     * @return true if the user can delete this object
     */
    public final boolean getUserCanDelete()
    {
        Boolean can = get(USER_CAN_DELETE_FIELD);
        if (can == null)
        {
            return true; // if we don't know, let's assume we can
        }
        return can.booleanValue();
    }

    /**
     * 
     * @param canDelete
     *            whether the user can delete the object
     */
    public final void setUserCanDelete(Boolean canDelete)
    {
        set(USER_CAN_DELETE_FIELD, canDelete);
    }

    /**
     * 
     * @return the default path to the object's resource
     */
    public String getDefaultPath()
    {
        return this.get(DEFAULT_PATH_FIELD);
    }

    /**
     * Set the object service default path. Set it to force save, refresh and delete methods to use this field to
     * determine where to send requests.
     * 
     * @param path
     *            The new path to the object's resource
     */
    public final void setDefaultPath(String path)
    {
        this.set(DEFAULT_PATH_FIELD, path);
    }

    /**
     * @return IDs of tags linked to the object.
     */
    public List<Long> getTagIDs()
    {
        return this.get(TAG_IDS_FIELD);
    }

    /**
     * Sets IDs of tags linked to the object.
     * 
     * @param tagIDs
     *            The tags to set.
     */
    public void setTagIDs(List<Long> tagIDs)
    {
        this.set(TAG_IDS_FIELD, tagIDs);
    }

    /**
     * @return IDs of tags linked to the object.
     */
    public List<String> getTagNames()
    {
        return get(TAG_NAMES_FIELD);
    }

    /**
     * Sets IDs of tags linked to the object.
     * 
     * @param tagNames
     *            The tags to set.
     */
    public void setTagNames(List<String> tagNames)
    {
        this.set(TAG_NAMES_FIELD, tagNames);
    }

    /**
     * @return List of tasks bound to current object.
     */
    public List<TaskModelData> getBoundTasks()
    {
        return this.get(BOUND_TASKS_FIELD);
    }

    /**
     * @return get dyn value from specific class dyn id.
     */
    public DynValueModelData getDynValue(Long classDynTypeId)
    {
        Map<Long, DynValueModelData> valuesMap = this.get(DYN_VALUES_FIELD);
        DynValueModelData dynValueModelData = valuesMap.get(classDynTypeId);
        return dynValueModelData;
    }

    /**
     * @return JSON representation needed by web services for POST requests.
     */
    public String toPOSTJson()
    {
        IModelDataJsonWriter jsonWriter = ModelDataJsonWriterSingleton.getInstance();
        jsonWriter.clear();
        return jsonWriter.write(this, IModelDataJsonWriter.Method.POST);
    }

    /**
     * @return JSON representation needed by web services for PUT requests.
     */
    public String toPUTJson()
    {
        IModelDataJsonWriter jsonWriter = ModelDataJsonWriterSingleton.getInstance();
        jsonWriter.clear();
        return jsonWriter.write(this, IModelDataJsonWriter.Method.PUT);
    }

    /**
     * Saves all data to server and dispatches default event (MODEL_DATA_CREATION_SUCCESS or MODEL_DATA_UPDATE_SUCCESS)
     * to controllers when save succeeds. Neither it forwards an error.
     */
    public void save()
    {
        AppEvent event = new AppEvent(null);
        this.save(event, null);
    }

    /**
     * Saves all data to server and dispatches <i>eventType</i> to controllers when save succeeds. Neither it forwards
     * an error.
     * 
     * @param eventType
     *            The type of event to dispatch.
     */
    public void save(EventType eventType)
    {
        this.save(new AppEvent(eventType), null);
    }

    /**
     * Saves all data to server and dispatches <i>event</i> to controllers when save succeeds. Neither it forwards an
     * error.
     * 
     * @param event
     *            The event to dispatch.
     */
    public void save(AppEvent event)
    {
        this.save(event, null);
    }

    /**
     * Saves all data to server and dispatches default event (MODEL_DATA_CREATION_SUCCESS or MODEL_DATA_UPDATE_SUCCESS)
     * to controllers when save succeeds. Neither it forwards an error.<br>
     * Callback is activated when request response is returned.
     * 
     * @param callback
     *            The callback to activate when response is returned.
     */
    public void save(BaseCallback callback)
    {
        this.save(null, callback);
    }

    /**
     * Saves all data to server and dispatches <i>event</i> to controllers when save succeeds. Neither it forwards an
     * error.<br>
     * Callback is activated when request response is returned.
     * 
     * @param event
     *            The event to dispatch.
     * @param callback
     *            The callback to activate when response is returned.
     */
    public void save(AppEvent event, BaseCallback callback)
    {
        IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
        if (this.getId() == null)
        {
            String path = getPath();
            if (callback == null)
            {
                callback = new PostModelDataCallback(this, event);
            }

            requestHandler.postRequest(path, callback, this.toPOSTJson());
        }
        else
        {
            String path = getPath();
            if (callback == null)
            {
                callback = new PutModelDataCallback(this, event);
            }

            requestHandler.putRequest(path, callback, this.toPUTJson());
        }
    }

    /**
     * If an identifier is set, it refreshes object data from server . If no identifier is set, it has no effect. <br>
     * When refresh is complete, it raises a MODEL_DATA_REFRESH_SUCCESS event.
     */
    public void refresh()
    {
        this.refresh(new AppEvent(null), null);
    }

    public void refresh(AppEvent event)
    {
        this.refresh(event, null);
    }

    public void refresh(EventType eventType)
    {
        this.refresh(new AppEvent(eventType), null);
    }

    public void refresh(BaseCallback callback)
    {
        this.refresh(null, callback);
    }

    /**
     * If an identifier is set, it refreshes object data from server . If no identifier is set, it has no effect. <br>
     * When refresh is complete, it raises an <i>eventType</i> event.
     * 
     * @param event
     *            Event to raise when data are retrieved.
     * @param callback
     *            The callback function to execute when server response is returned.
     */
    public void refresh(AppEvent event, BaseCallback callback)
    {
        if (this.getId() != null)
        {
            String path = getPath();

            if (callback == null)
            {
                callback = new GetModelDataCallback(this, event);
            }

            IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
            requestHandler.getRequest(path, callback);
        }
    }

    /**
     * It refreshes object data from server with first returned record return by query built upon <i>parameters</i>. If
     * object does not exist it is created. <br>
     * When refresh or create is complete, it raises an <i>eventType</i> event.
     * 
     * @param event
     *            Event to raise when data are retrieved.
     * @param parameters
     *            The parameters to apply on service request.
     */
    public void refreshOrCreateWithParams(AppEvent event, List<IUrlParameter> parameters)
    {
        String path = getPath() + ParameterBuilder.parametersToString(parameters);
        BaseCallback callback = new GetOrCreateModelDataCallback(this, event);

        IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
        requestHandler.getRequest(path, callback);
    }

    public void refreshOrCreateWithParams(AppEvent event, IUrlParameter parameter)
    {
        List<IUrlParameter> parameters = new ArrayList<IUrlParameter>();
        parameters.add(parameter);

        this.refreshOrCreateWithParams(event, parameters);
    }

    /**
     * It refreshes object data from server with first returned record return by query built upon <i>parameters</i>.<br>
     * When refresh is complete, it raises an <i>eventType</i> event.
     * 
     * @param event
     *            Event to raise when data are retrieved.
     * @param parameters
     *            The parameters to apply on service request.
     */
    public void refreshWithParams(AppEvent event, List<IUrlParameter> parameters)
    {
        String path = getPath() + ParameterBuilder.parametersToString(parameters);
        BaseCallback callback = new GetModelDataCallback(this, event);

        IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
        requestHandler.getRequest(path, callback);
    }

    /**
     * It refreshes object data from server with first returned record return by query built upon <i>parameters</i>.<br>
     * When refresh is complete, it raises an <i>eventType</i> event.
     * 
     * @param event
     *            Event to raise when data are retrieved.
     * @param parameter
     *            The parameters to apply on service request.
     */
    public void refreshWithParams(AppEvent event, IUrlParameter parameter)
    {
        List<IUrlParameter> parameters = new ArrayList<IUrlParameter>();
        parameters.add(parameter);

        this.refreshWithParams(event, parameters);
    }

    public void refreshWithParams(AppEvent event, List<IUrlParameter> parameters, BaseCallback callback)
    {
        String path = getPath() + ParameterBuilder.parametersToString(parameters);
        if (callback == null)
        {
            callback = new GetModelDataCallback(this, event);
        }

        IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
        requestHandler.getRequest(path, callback);
    }

    public void refreshWithParams(AppEvent event, IUrlParameter parameter, BaseCallback callback)
    {
        List<IUrlParameter> parameters = new ArrayList<IUrlParameter>();
        parameters.add(parameter);

        this.refreshWithParams(event, parameters, callback);
    }

    /**
     * If an identifier is set, it deletes object from server . If no identifier is set, it has no effect. <br>
     * When delete is complete, it raises a DELETE_MODEL_DATA_SUCCESS event.
     */
    public void delete()
    {
        this.delete(CommonEvents.MODEL_DATA_DELETE_SUCCESS);
    }

    /**
     * If an identifier is set, it deletes object data from server . If no identifier is set, it has no effect. <br>
     * When delete is complete, it raises an <i>eventType</i> event.
     * 
     * @param eventType
     *            Event type to raise when data are retrieved.
     */
    public void delete(EventType eventType)
    {
        delete(new AppEvent(eventType));
    }

    /**
     * If an identifier is set, it deletes object data from server . If no identifier is set, it has no effect. <br>
     * When delete is complete, it raises an <i>eventType</i> event.
     * 
     * @param appEvent
     *            Event type to raise when data are retrieved.
     */
    public void delete(AppEvent appEvent)
    {
        BaseCallback callback = new DeleteModelDataCallback(this, appEvent);
        delete(callback);
    }

    public void delete(BaseCallback callback)
    {
        if (this.getId() != null)
        {
            String path = getPath();

            IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
            requestHandler.deleteRequest(path, callback);
        }
    }

    /**
     * @return Service path where model data can be saved.
     */
    public String getPath()
    {
        if (getDefaultPath() != null)
        {
            return getDefaultPath();
        }
        else
        {
            String path = ServicesPath.getPath(this.simpleClassName);
            if (this.getId() != null)
            {
                path += this.getId();
            }
            return path;
        }
    }

    /**
     * @return Permission path based on simple class name value.
     */
    public String getPermissionPath()
    {
        return PermissionPath.getPath(this.simpleClassName);
    }

    /**
     * 
     * @return the appropriate permission radical (without version nor operation) associated to the object
     */
    public String getPermissionRadical()
    {
        String path = getDefaultPath();
        if (path != null)
        {
            path = path.replace("/", ":");
        }
        return path;
    }

    public Hd3dModelData copy()
    {
        Hd3dModelData data = new Hd3dModelData();
        Hd3dModelData.copy(this, data);
        return data;
    }

    /**
     * @return A string representation of current object by listing all its members and their values.
     */
    @Override
    public String toString()
    {
        StringBuilder stringRepresentation = new StringBuilder();
        for (String key : this.map.keySet())
        {
            stringRepresentation.append(key).append(": ");
            Object val = this.map.get(key);
            if (val != null)
            {
                if (val instanceof Hd3dModelData)
                    stringRepresentation.append(((Hd3dModelData) val).getSimpleClassName() + " object :"
                            + ((Hd3dModelData) val).getId());
                else
                    stringRepresentation.append(val.toString());
            }

            stringRepresentation.append("\n");
        }

        return stringRepresentation.toString();
    }

}
