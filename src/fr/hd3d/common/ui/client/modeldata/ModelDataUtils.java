package fr.hd3d.common.ui.client.modeldata;

import com.extjs.gxt.ui.client.data.DataField;


@Deprecated
public class ModelDataUtils
{

    /**
     * creates a data field
     * 
     * @param name
     * @param clazz
     * @param format
     * @return Data field for field <i>name</i> and that corresponds to <i>clazz</i> with <i>format</i> as format.
     */
    @Deprecated
    public static DataField createDataField(String name, Class<?> clazz, String format)
    {
        if (name == null)
        {
            throw new NullPointerException();
        }
        if (clazz == null)
        {
            throw new NullPointerException();
        }
        final DataField dataField = new DataField(name);
        dataField.setFormat(format);
        dataField.setType(clazz);
        return dataField;
    }

    /**
     * creates a data field (convenience method)
     * 
     * @param name
     * @param clazz
     * @return Data field for field <i>name</i> and that corresponds to <i>clazz</i>.
     */
    @Deprecated
    public static DataField createDataField(String name, Class<?> clazz)
    {
        return createDataField(name, clazz, null);
    }

}
