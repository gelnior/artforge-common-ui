package fr.hd3d.common.ui.client.modeldata.writer;

public interface IJsonWriter
{

    String write(Object obj);

}
