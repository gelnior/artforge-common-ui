package fr.hd3d.common.ui.client.modeldata.writer;

import java.util.Date;

import com.google.gwt.i18n.client.DateTimeFormat;


public class ClientDateWriter implements IDateWriter
{
    public String write(String dateTimeString, Date date)
    {
        return DateTimeFormat.getFormat(dateTimeString).format(date);
    }
}
