package fr.hd3d.common.ui.client.modeldata.writer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.data.ModelData;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONBoolean;
import com.google.gwt.json.client.JSONNull;
import com.google.gwt.json.client.JSONNumber;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.json.client.JSONValue;


/**
 * This class converts GXT model-data to GWT JSON objects.
 * 
 * @author HD3D
 */
public class JsonEncoder
{

    /**
     * Encodes a map into a JSONObject.
     * 
     * @param map
     *            the map
     * @return the JSONObject
     */
    public static JSONObject encode(Map<String, Object> map)
    {
        return encodeMap(map);
    }

    /**
     * Encodes a model data instance into a JSONObject.
     * 
     * @param model
     *            the model data object
     * @return the JSONObject
     */
    public static JSONObject encode(ModelData model)
    {
        return encodeMap((model).getProperties());
    }

    /**
     * Encodes a value into a String.
     * 
     * @param value
     *            the object to convert.
     * @return the value at string format.
     */
    protected static String encodeValue(Object value)
    {
        if (value instanceof Date)
        {
            return "" + ((Date) value).getTime();
        }
        else if (value instanceof Integer)
        {
            return "" + value;
        }
        else if (value instanceof Long)
        {
            return "" + value;
        }
        else if (value instanceof Float)
        {
            return "" + value;
        }
        return "" + value.toString();
    }

    /**
     * Encodes an map instance into a JSONObject.
     * 
     * @param data
     *            the map.
     * @return the JSONObject
     */
    @SuppressWarnings("unchecked")
    protected static JSONObject encodeMap(Map<String, Object> data)
    {
        JSONObject jsobj = new JSONObject();
        for (Map.Entry<String, Object> entry : data.entrySet())
        {
            String key = entry.getKey();
            Object val = entry.getValue();
            if (val instanceof String)
            {
                jsobj.put(key, new JSONString(encodeValue(val)));
            }
            else if (val instanceof Date)
            {
                jsobj.put(key, new JSONString(encodeValue(val)));
            }
            else if (val instanceof Long)
            {
                jsobj.put(key, new JSONNumber((Long) val));
            }
            else if (val instanceof Integer)
            {
                jsobj.put(key, new JSONNumber((Integer) val));
            }
            else if (val instanceof Float)
            {
                jsobj.put(key, new JSONNumber((Float) val));
            }
            else if (val instanceof Double)
            {
                jsobj.put(key, new JSONNumber((Double) val));
            }
            else if (val instanceof Boolean)
            {
                jsobj.put(key, JSONBoolean.getInstance((Boolean) val));
            }
            else if (val == null)
            {
                jsobj.put(key, JSONNull.getInstance());
            }
            else if (val instanceof Map)
            {
                jsobj.put(key, encodeMap((Map<String, Object>) val));
            }
            else if (val instanceof List)
            {
                jsobj.put(key, encodeList((List<Object>) val));
            }
            else if (val instanceof ModelData)
            {
                jsobj.put(key, encodeMap(((ModelData) val).getProperties()));
            }
        }

        return jsobj;
    }

    /**
     * Encodes an list instance into a JSONArray.
     * 
     * @param data
     *            the list.
     * @return the JSONArray
     */
    @SuppressWarnings("unchecked")
    protected static JSONArray encodeList(List<Object> data)
    {
        JSONArray jsona = new JSONArray();
        for (int i = 0; i < data.size(); i++)
        {
            Object val = data.get(i);
            if (val instanceof ModelData)
            {
                jsona.set(i, encodeMap(((ModelData) val).getProperties()));
            }
            else if (val instanceof Map)
            {
                jsona.set(i, encodeMap((Map<String, Object>) val));
            }
            else if (val instanceof List)
            {
                jsona.set(i, encodeList((List<Object>) val));
            }
            else if (val instanceof String)
            {
                jsona.set(i, new JSONString(encodeValue(val)));
            }
            else if (val instanceof Long)
            {
                jsona.set(i, new JSONNumber((Long) val));
            }
            else if (val instanceof Integer)
            {
                jsona.set(i, new JSONNumber((Integer) val));
            }
            else if (val instanceof Float)
            {
                jsona.set(i, new JSONNumber((Float) val));
            }
            else if (val instanceof Boolean)
            {
                jsona.set(i, JSONBoolean.getInstance((Boolean) val));
            }
            else if (val == null)
            {
                jsona.set(i, JSONNull.getInstance());
            }
            else if (val instanceof Date)
            {
                jsona.set(i, new JSONString(encodeValue(val)));
            }
        }
        return jsona;
    }

    public static Map<String, Object> decode(JSONObject jso)
    {
        Map<String, Object> map = new FastMap<Object>();
        for (String key : jso.keySet())
        {
            JSONValue j = jso.get(key);
            if (j.isObject() != null)
            {
                map.put(key, decode(j.isObject()));
            }
            else if (j.isArray() != null)
            {
                map.put(key, decodeToList(j.isArray()));
            }
            else if (j.isBoolean() != null)
            {
                map.put(key, j.isBoolean().booleanValue());
            }
            else if (j.isNumber() != null)
            {
                map.put(key, j.isNumber().doubleValue());
            }
            else if (j.isString() != null)
            {
                map.put(key, j.isString().stringValue());
            }
        }
        return map;
    }

    public static List<Object> decodeToList(JSONArray array)
    {
        List<Object> list = new ArrayList<Object>();
        for (int i = 0; i < array.size(); i++)
        {
            JSONValue v = array.get(i);
            if (v.isObject() != null)
            {
                list.add(decode(v.isObject()));
            }
            else if (v.isArray() != null)
            {
                list.add(decodeToList(v.isArray()));
            }
            else if (v.isNull() != null)
            {
                list.add(null);
            }
            else if (v.isNumber() != null)
            {
                list.add(v.isNumber().doubleValue());
            }
            else if (v.isBoolean() != null)
            {
                list.add(v.isBoolean().booleanValue());
            }
            else if (v.isString() != null)
            {
                list.add(v.isString().stringValue());
            }
        }

        return list;
    }

    /**
     * @param json
     *            List of long at JSON format. If format is wrong an expection is raised.
     * @return Java list of long.
     */
    public static List<Long> parseJSONLongArray(String json)
    {
        List<Long> ids = new ArrayList<Long>();
        JSONArray array = JSONParser.parse(json).isArray();

        for (int i = 0; i < array.size(); i++)
        {
            JSONNumber idNumber = array.get(i).isNumber();
            ids.add(new Double(idNumber.doubleValue()).longValue());
        }
        return ids;
    }
}
