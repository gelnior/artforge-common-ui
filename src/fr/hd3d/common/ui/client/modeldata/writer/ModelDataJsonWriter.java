package fr.hd3d.common.ui.client.modeldata.writer;

import java.util.Map;
import java.util.Map.Entry;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;


/**
 * ModelDataJsonWriter translates a ModelData object in the JSON format expected by web services for PUT and POST
 * operations.
 * 
 * @author HD3D
 */
public class ModelDataJsonWriter extends JSONWriter implements IModelDataJsonWriter
{
    /** String to put at the beginning of a JSON Object. */
    private static final String BEGIN_OBJECT = "{";
    /** String to put at the end of a JSON Object. */
    private static final String END_OBJECT = "}";

    /**
     * Default constructor.
     */
    public ModelDataJsonWriter()
    {
        // super(false);
    }

    /**
     * @return <i>object</i> in a JSON String format.
     * 
     * @param object
     *            The object to transform in JSON string.
     * @param httpMethod
     *            If it is a put it includes version and id field. If it is a post method it does not includ these
     *            fields.
     */
    public final String write(Hd3dModelData object, Method httpMethod)
    {
        this.add(BEGIN_OBJECT);
        for (Map.Entry<String, ?> e : object.getProperties().entrySet())
        {
            this.addEntry(e, httpMethod);
        }
        this.add(END_OBJECT);

        return buf.toString();
    }

    /**
     * Add entry to the string buffer.
     * 
     * @param entry
     *            The entry (field name and value) to add to final JSON object.
     * @param httpMethod
     *            The method chosen to build the final JSON object.
     */
    private void addEntry(Entry<String, ?> entry, Method httpMethod)
    {
        String key = entry.getKey();

        if (this.isAddableField(key, httpMethod))
        {
            if (buf.length() > 1)
            {
                this.add(",");
            }

            this.add(key, entry.getValue());
        }
    }

    /**
     * Tests if the field can be added to the JSON. Version and id fields are not expected in a POST request. Version
     * field is not expected in a PUT request.
     * 
     * @param key
     *            The name field to test.
     * @param httpMethod
     *            The chosen method for json writing.
     * @return True if the field can be added, false either.
     */
    public boolean isAddableField(String key, Method httpMethod)
    {
        boolean addField = true;

        addField = httpMethod == Method.POST;
        addField = addField && 0 == key.compareTo(Hd3dModelData.ID_FIELD);
        addField = addField || 0 == key.compareTo(Hd3dModelData.VERSION_FIELD);
        addField = !addField;

        return addField;
    }

    /**
     * Resets the String buffer.
     */
    public void clear()
    {
        this.buf.setLength(0);
    }
}
