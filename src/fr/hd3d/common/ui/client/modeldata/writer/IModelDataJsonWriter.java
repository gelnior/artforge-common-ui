package fr.hd3d.common.ui.client.modeldata.writer;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;


/**
 * Writer interface needed for writer mocking.
 * 
 * @author HD3D
 */
public interface IModelDataJsonWriter
{
    /**
     * HTTP methods for the web request : PUT or POST. This information is needed by the writer to build the correct
     * JSON object.
     * 
     * @author HD3D
     */
    public enum Method
    {
        POST, PUT
    };

    /**
     * @param object
     *            The object that need to be transcript at JSON format.
     * @param httpMethod
     *            the method type for which the JSON format is expected.
     * 
     * @return Model data object at JSON Format.
     */
    public abstract String write(Hd3dModelData object, Method httpMethod);

    /**
     * Clear writer buffer.
     */
    public abstract void clear();

}
