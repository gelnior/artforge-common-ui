package fr.hd3d.common.ui.client.modeldata.writer;

/**
 * This singleton serves the JSON writer set by developer for its application or for his tests. If nothing is set, the
 * writer will be a GXT Writer.
 * 
 * @author HD3D
 */
public class JsonWriterSingleton
{
    /** The writer instance to return. */
    private static IJsonWriter writer;

    /**
     * @return The set JSON writer instance. If it is null, it returns a GWT JSON Writer.
     */
    public final static IJsonWriter getInstance()
    {
        if (writer == null)
        {
            writer = new JSONWriter();
        }
        return writer;
    }

    /**
     * Sets the JSON writer instance. This method is useful for unit tests.
     * 
     * @param mock
     *            The JSON writer to set.
     */
    public final static void setInstanceAsMock(IJsonWriter mock)
    {
        writer = mock;
    }
}
