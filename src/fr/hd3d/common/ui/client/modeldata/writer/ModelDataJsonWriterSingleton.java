package fr.hd3d.common.ui.client.modeldata.writer;

/**
 * This singleton serves the JSON model data writer set by developer for its application or for his tests. If nothing is
 * set, the writer will be a GXT Writer.
 * 
 * @author HD3D
 */
public class ModelDataJsonWriterSingleton
{
    /** The writer instance to return. */
    private static IModelDataJsonWriter writer;

    /**
     * @return The set JSON writer instance. If it is null, it returns a GWT JSON Writer.
     */
    public final static IModelDataJsonWriter getInstance()
    {
        if (writer == null)
        {
            writer = new ModelDataJsonWriter();
        }
        return writer;
    }

    /**
     * Sets the JSON writer instance. This method is useful for unit tests.
     * 
     * @param mock
     *            The JSON writer to set.
     */
    public final static void setInstanceAsMock(IModelDataJsonWriter mock)
    {
        writer = mock;
    }
}
