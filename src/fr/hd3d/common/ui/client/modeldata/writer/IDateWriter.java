package fr.hd3d.common.ui.client.modeldata.writer;

import java.util.Date;


public interface IDateWriter
{
    public String write(String dateTimeString, Date date);
}
