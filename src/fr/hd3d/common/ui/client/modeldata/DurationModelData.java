package fr.hd3d.common.ui.client.modeldata;

import java.util.Date;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;


/**
 * Class representing a hd3d element which has a name, a begin date and an end date.
 * 
 * @author HD3D
 */
public class DurationModelData extends RecordModelData
{
    private static final long serialVersionUID = 2430695121624256526L;

    public static final String START_DATE_FIELD = "startDate";
    public static final String END_DATE_FIELD = "endDate";

    public Date getStartDate()
    {
        return get(START_DATE_FIELD);
    }

    public void setStartDate(Date startDate)
    {
        set(START_DATE_FIELD, startDate);
    }

    public Date getEndDate()
    {
        return get(END_DATE_FIELD);
    }

    public void setEndDate(Date endDate)
    {
        set(END_DATE_FIELD, endDate);
    }

    public static ModelType getModelType()
    {
        ModelType type = RecordModelData.getModelType();

        DataField startDateField = new DataField(START_DATE_FIELD);
        startDateField.setType(Date.class);
        startDateField.setFormat(TIMESTAMP_FORMAT_PATTERN);
        DataField endDateField = new DataField(END_DATE_FIELD);
        endDateField.setType(Date.class);
        endDateField.setFormat(TIMESTAMP_FORMAT_PATTERN);

        type.addField(startDateField);
        type.addField(endDateField);

        return type;
    }
}
