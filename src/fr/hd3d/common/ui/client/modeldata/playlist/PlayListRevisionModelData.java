package fr.hd3d.common.ui.client.modeldata.playlist;

import java.util.List;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.ModelDataUtils;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;


public class PlayListRevisionModelData extends RecordModelData
{

    private static final long serialVersionUID = -4844880345971570706L;

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LPlayListRevision";
    public static final String SIMPLE_CLASS_NAME = "PlayListRevision";

    public static final String REVISION_FIELD = "revision";
    public static final String HOOK_FIELD = "hook";
    public static final String COMMENT_FIELD = "comment";
    public static final String PLAYLIST_EDITS_FIELD = "playListEdits";
    public static final String PROJECT_FIELD = "project";
    public static final String FRAMERATE_FIELD = "frameRate";
    public static final String PLAYLIST_REVISION_GROUP = "playListRevisionGroup";

    public static final String ENHANCED_NAME = "enhancedName";

    public static ModelType getModelType()
    {
        final ModelType modelType = RecordModelData.getModelType();
        modelType.addField(ModelDataUtils.createDataField(REVISION_FIELD, Integer.class));
        modelType.addField(ModelDataUtils.createDataField(HOOK_FIELD, String.class));
        modelType.addField(ModelDataUtils.createDataField(FRAMERATE_FIELD, String.class));
        modelType.addField(ModelDataUtils.createDataField(PROJECT_FIELD, Long.class));
        modelType.addField(ModelDataUtils.createDataField(COMMENT_FIELD, String.class));
        modelType.addField(ModelDataUtils.createDataField(PLAYLIST_EDITS_FIELD, List.class,
                PlayListEditModelData.SIMPLE_CLASS_NAME));
        modelType.addField(ModelDataUtils.createDataField(PLAYLIST_REVISION_GROUP, Long.class));
        return modelType;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <X> X get(String property)
    {
        if (PlayListRevisionModelData.ENHANCED_NAME.equals(property))
            return (X) this.getEnhancedName();

        return (X) super.get(property);
    }

    public PlayListRevisionModelData()
    {
        this(null);
    }

    public PlayListRevisionModelData(String name)
    {
        super();
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
        this.setClassName(CLASS_NAME);
        this.setName(name);
    }

    public PlayListRevisionModelData(String name, PlayListRevisionGroupModelData parent)
    {
        super();
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
        this.setClassName(CLASS_NAME);
        this.setName(name);
        setDefaultPath(parent.getProjectId(), parent.getId());
    }

    public String getEnhancedName()
    {
        return this.get(NAME_FIELD) + " (rev." + this.get(REVISION_FIELD) + ")";
    }

    public void setEnhancedName(String newName)
    {
        setName(newName);
    }

    public void setRevision(Integer revision)
    {
        this.set(REVISION_FIELD, revision);
    }

    public Integer getRevision()
    {
        return this.get(REVISION_FIELD);
    }

    public void setHook(String hook)
    {
        this.set(HOOK_FIELD, hook);

    }

    public String getHook()
    {
        return this.get(HOOK_FIELD);
    }

    public void setComment(String comment)
    {
        this.set(COMMENT_FIELD, comment);

    }

    public String getComment()
    {
        return this.get(COMMENT_FIELD);
    }

    public Long getProjectId()
    {
        return this.get(PROJECT_FIELD);
    }

    public void setProjectId(Long projectId)
    {
        this.set(PROJECT_FIELD, projectId);
    }

    public Long getPlayListRevisionGroupId()
    {
        return this.get(PLAYLIST_REVISION_GROUP);
    }

    public void setPlayListRevisionGroupId(Long playListRevisionGroupId)
    {
        this.set(PLAYLIST_REVISION_GROUP, playListRevisionGroupId);
    }

    public void setFrameRate(String frameRate)
    {
        this.set(FRAMERATE_FIELD, frameRate);
    }

    public String getFrameRate()
    {
        return this.get(FRAMERATE_FIELD);
    }

    public void setPlaylistEdits(List<Long> playListEdits)
    {
        this.set(PLAYLIST_EDITS_FIELD, playListEdits);
    }

    public List<Long> getPlayListEdits()
    {
        return this.get(PLAYLIST_EDITS_FIELD);
    }

    public String getPlaylistString()
    {
        return getName() + " rev " + getRevision();
    }

    public void setDefaultPath(Long projectId, Long groupId)
    {
        this.setDefaultPath(ServicesPath.PROJECTS + projectId + "/" + ServicesPath.PLAYLISTREVISIONGROUPS + groupId
                + "/" + ServicesPath.PLAYLISTREVISIONS);
    }
}
