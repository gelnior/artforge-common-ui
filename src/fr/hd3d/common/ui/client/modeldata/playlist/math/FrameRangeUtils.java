package fr.hd3d.common.ui.client.modeldata.playlist.math;

import java.util.ArrayList;
import java.util.List;


public class FrameRangeUtils
{
    public static boolean isRangeSorted(List<FrameRange> ranges)
    {
        FrameRange previous = null;
        for (FrameRange current : ranges)
        {
            if (previous != null && previous.compareTo(current) > 0)
            {
                return false;
            }
            previous = current;
        }
        return true;
    }

    /**
     * checks if a {@link FrameRange} is in a list. Compares references instead of invoking .equals method !
     * 
     * @param ranges
     * @param aRange
     */
    public static boolean isRangeInList(List<FrameRange> ranges, FrameRange aRange)
    {
        for (FrameRange range : ranges)
        {
            if (aRange == range)
            {
                return true;
            }
        }
        return false;
    }

    public static boolean isContiguous(final FrameRange first, final FrameRange second)
    {
        return FrameUtils.isContiguous(first.frameIn, first.frameOut, second.frameIn, second.frameOut);
    }

    public static boolean isOverlapping(final FrameRange first, final FrameRange second)
    {
        return FrameUtils.isOverlapping(first.frameIn, first.frameOut, second.frameIn, second.frameOut);
    }

    /**
     * @param included
     * @param inRange
     * @return whether 'included' range is included in 'in' range
     */
    public static boolean isIncluded(final FrameRange included, final FrameRange inRange)
    {
        return FrameUtils.isIncluded(included.frameIn, included.frameOut, inRange.frameIn, inRange.frameOut);
    }

    public static int getGap(final FrameRange first, final FrameRange second)
    {
        return FrameUtils.getGap(first.frameIn, first.frameOut, second.frameIn, second.frameOut);
    }

    // FIXME test this
    public static int guessOffset(FrameRange rangeOne, FrameRange rangeTwo)
    {
        if (isOverlapping(rangeOne, rangeTwo))
        {
            return 0;
        }
        if (rangeOne.getLength() == rangeTwo.getLength())
        {
            return rangeOne.frameIn - rangeTwo.frameIn;
        }
        return 0;
    }

    public static List<FrameRange> rangesBetween(int in, int out, List<FrameRange> sortedRanges)
    {
        final List<FrameRange> rangesBetween = new ArrayList<FrameRange>(sortedRanges.size());
        final FrameRange otherRange = new FrameRange(in, out);
        for (FrameRange range : sortedRanges)
        {
            if (range.isOverlapping(otherRange))
            {
                rangesBetween.add(range);
            }
        }
        return rangesBetween;
    }
}
