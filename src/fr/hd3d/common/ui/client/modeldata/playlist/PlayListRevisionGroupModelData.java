package fr.hd3d.common.ui.client.modeldata.playlist;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.LoadEvent;
import com.extjs.gxt.ui.client.data.ModelType;
import com.extjs.gxt.ui.client.event.LoadListener;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.reader.playlist.PlayListRevisionGroupReader;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.util.field.LongDataField;


/**
 * This RecodModelData manages PlayListRevisionGroupModelData class.
 * 
 * @see RecordModelData
 * 
 * @author guillaume-maucomble
 * @version 1.0
 * @since 1.0
 */
public class PlayListRevisionGroupModelData extends RecordModelData
{
    private static final long serialVersionUID = 1097292667832059893L;

    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LPlayListRevisionGroup";
    public static final String SIMPLE_CLASS_NAME = "PlayListRevisionGroup";

    public static final String PARENT_FIELD = "parent";
    public static final String PROJECT_FIELD = "project";

    public static final String ENHANCED_NAME = "enhancedName";

    protected final List<Long> childrenIds = new ArrayList<Long>();
    private int plrGroupLoading = 0;

    public PlayListRevisionGroupModelData()
    {
        this(null);
    }

    public PlayListRevisionGroupModelData(String name)
    {
        super();
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
        this.setClassName(CLASS_NAME);
        this.setName(name);
    }

    public Long getProjectId()
    {
        return this.get(PROJECT_FIELD);
    }

    public void setProjectId(Long projectId)
    {
        this.set(PROJECT_FIELD, projectId);
    }

    public Long getParentId()
    {
        return this.get(PARENT_FIELD);
    }

    public void setParentId(Long parentId)
    {
        this.set(PARENT_FIELD, parentId);
    }

    @Override
    public String getPath()
    {
        return getDefaultPath();
    }

    @SuppressWarnings("unchecked")
    @Override
    public <X> X get(String property)
    {
        if (PlayListRevisionGroupModelData.ENHANCED_NAME.equals(property))
            return (X) this.get(NAME_FIELD);

        return (X) super.get(property);
    }

    @Override
    public String getDefaultPath()
    {
        String path = this.get(DEFAULT_PATH_FIELD);
        if (path == null)
        {
            path = ServicesPath.PROJECTS + this.getProjectId() + "/" + ServicesPath.PLAYLISTREVISIONGROUPS;
            if (this.getId() != null)
            {
                path += this.getId();
            }
        }
        return path;
    }

    public static ModelType getModelType()
    {
        ModelType modelType = RecordModelData.getModelType();

        DataField project = new LongDataField(PROJECT_FIELD);
        modelType.addField(project);

        DataField parent = new LongDataField(PARENT_FIELD);
        modelType.addField(parent);

        return modelType;
    }

    public void loadChildrenIds()
    {
        if (childrenIds.size() > 0)
        {
            EventDispatcher.forwardEvent(CommonEvents.PLAYLISTREVISIONGROUP_CHILDREN_LOADED, childrenIds);
        }
        else
        {
            plrGroupLoading = 0;
            this.retrieveSequenceIds(this);
        }
    }

    public void retrieveSequenceIds(PlayListRevisionGroupModelData group)
    {
        this.childrenIds.add(group.getId());
        plrGroupLoading++;
        Constraint parentConstraint = new Constraint(EConstraintOperator.eq,
                PlayListRevisionGroupModelData.PARENT_FIELD, group.getId());
        final ServiceStore<PlayListRevisionGroupModelData> store = new ServiceStore<PlayListRevisionGroupModelData>(
                new PlayListRevisionGroupReader());
        store.setPath(ServicesPath.PROJECTS + group.getProjectId() + "/" + ServicesPath.PLAYLISTREVISIONGROUPS
                + ServicesPath.ALL);
        store.addParameter(parentConstraint);

        store.addLoadListener(new LoadListener() {
            @Override
            public void loaderLoad(LoadEvent le)
            {
                for (PlayListRevisionGroupModelData sequence : store.getModels())
                {
                    retrieveSequenceIds(sequence);
                }
                plrGroupLoading--;

                if (store.getCount() == 0 && plrGroupLoading == 0)
                {
                    EventDispatcher.forwardEvent(CommonEvents.PLAYLISTREVISIONGROUP_CHILDREN_LOADED, childrenIds);
                }
            }
        });
        store.reload();
    }

}
