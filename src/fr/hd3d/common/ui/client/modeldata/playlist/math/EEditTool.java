package fr.hd3d.common.ui.client.modeldata.playlist.math;

public enum EEditTool
{
    SLIP("Slip"), TRIM("Trim");

    public final String name;

    EEditTool(String name)
    {
        this.name = name;

    }
}
