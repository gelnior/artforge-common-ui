package fr.hd3d.common.ui.client.modeldata.playlist.math;

public class FrameUtils {
    public static int compareRange(int firstIn, int firstOut, int secondIn, int secondOut) {
        if (secondIn == firstIn)
            return firstOut - secondOut;
        return firstIn - secondIn;
    }

    public static boolean isContiguous(final int firstIn, final int firstOut, final int secondIn, final int secondOut) {
        return firstOut == secondIn || firstIn == secondOut;
    }

    public static boolean isIncluded(final int includedIn, final int includedOut, final int inIn, final int inOut) {
        return includedIn >= inIn && inOut >= includedOut;
    }

    public static boolean isOverlapping(final int firstIn, final int firstOut, final int secondIn, final int secondOut) {
        if (firstIn == firstOut || secondIn == secondOut)
            return false;
        if (firstIn <= secondIn)
            return firstOut > secondIn;
        return secondOut > firstIn;
    }

    public static int getDuration(int in, int out) {
        return out - in;
    }

    public static int getGap(final int firstIn, final int firstOut, final int secondIn, final int secondOut) {
        if (isOverlapping(firstIn, firstOut, secondIn, secondOut))
            return 0;
        if (compareRange(firstIn, firstOut, secondIn, secondOut) < 0)
            return secondIn - firstOut;
        return secondOut - firstIn;
    }
}
