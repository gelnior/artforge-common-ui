package fr.hd3d.common.ui.client.modeldata.playlist.math;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class EditingMathUtils
{
    public static boolean canSlip(List<FrameRange> ranges, int offset)
    {
        // we check if all the range can be slipped
        for (final FrameRange range : ranges)
        {
            if (range.frameIn + offset < 0)
            {
                return false;
            }
        }
        return true;
    }

    /*
     * SLIP TOOLS
     */
    /**
     * Slips a range with a specified offset
     * 
     * @param range
     * @param offset
     * @return true if the operation succeed, false if the resulting frameIn range is < 0
     */
    public static boolean slip(FrameRange range, int offset)
    {
        if (range.frameIn + offset < 0)
        {
            return false;
        }
        range.frameIn += offset;
        range.frameOut += offset;
        return true;
    }

    /**
     * Slips a range, to a specified frameIn
     * 
     * @param range
     * @param frameIn
     * 
     */
    public static boolean slipToFrameIn(FrameRange range, int frameIn)
    {
        final int offset = frameIn - range.frameIn;
        return slip(range, offset);
    }

    /**
     * slips a range to a specified frameOut
     * 
     * @param range
     * @param frameOut
     */
    public static boolean slipToFrameOut(FrameRange range, int frameOut)
    {
        final int offset = frameOut - range.frameOut;
        return slip(range, offset);
    }

    /**
     * slips the given ranges to the specified offset
     * 
     * @param ranges
     * @param offset
     *            return true if the operation succeed, of false, if a range cannot be slipped
     */
    public static boolean slip(List<FrameRange> ranges, int offset)
    {
        if (ranges == null)
        {
            return false;
        }
        if (canSlip(ranges, offset) == false)
        {
            return false;
        }
        for (final FrameRange range : ranges)
        {
            EditingMathUtils.slip(range, offset);
        }
        return true;
    }

    /*
     * TRIM TOOLS
     */
    /**
     * trims in a range from one offset.
     * 
     * @param range
     *            , a FrameRange
     * @param offset
     *            , the offset we want to apply to frameIn
     * @return true, if the operation succeed or false if the range.frameIn + offset < 0 o if the offset is > to range
     *         length
     */
    public static boolean trimIn(FrameRange range, int offset)
    {
        if (offset >= range.getLength())
        {
            return false;
        }
        if (range.frameIn + offset < 0)
        {
            return false;
        }
        range.frameIn += offset;
        return true;
    }

    /**
     * trims in the range to a specified frameIn
     * 
     * @param range
     *            , a FrameRange
     * @param frameIn
     * 
     * @return true, if the operation succeed or false if the frameIn is < 0 or >= range.frameOut
     */
    public static boolean trimInToFrame(FrameRange range, int frameIn)
    {
        return trimIn(range, frameIn - range.frameIn);
    }

    /**
     * trim-in a range in a list of range
     * 
     * @param sortedRanges
     *            , the list of and sorted ranges (ascendant)
     * @param rangeToTrim
     *            , the range in the list, we want to trim
     * @param offset
     *            , the offset of the trimming
     * @return true if the operation succeeds (or if the offset is zero), or false if the rangeToTrim is no contained in
     *         the list , if the list is not sorted, or a bad modification mode has been entered
     */
    public static boolean trimIn(List<FrameRange> sortedRanges, FrameRange rangeToTrim, int offset,
            EModificationMode mode)
    {
        if (offset == 0)
        {
            return true;
        }
        if (FrameRangeUtils.isRangeInList(sortedRanges, rangeToTrim) == false)
        {
            return false;
        }
        final int rangeIndex = sortedRanges.indexOf(rangeToTrim);
        if (sortedRanges.size() == 1 || rangeIndex == 0)
        {
            return EditingMathUtils.trimIn(rangeToTrim, offset);
        }
        // we check if the list is sorted
        if (!FrameRangeUtils.isRangeSorted(sortedRanges))
        {
            return false;
        }
        // we get the preceding range
        final FrameRange rangeBefore = sortedRanges.get(rangeIndex - 1);
        // we check if there is a gap between
        final int gap = rangeToTrim.getGap(rangeBefore);
        final int absoluteGap = Math.abs(gap);
        final int absoluteOffset = Math.abs(offset);
        // and apply the offset
        switch (mode)
        {
            case INSERT:
                if (offset < 0)
                {
                    // if there not enough images to trim in without cutting the range before we return false
                    if (absoluteGap >= absoluteOffset)
                    {
                        // no need to move the ranges before the rangeToTrim
                        break;
                    }
                    // else we move the ranges before
                    final List<FrameRange> listOfPrecedingRanges = sortedRanges.subList(0, rangeIndex);
                    final int slipOffset = offset - gap;
                    final boolean canSlip = slip(listOfPrecedingRanges, slipOffset);
                    if (canSlip == false)
                    {
                        return false;
                    }
                }
                break;
            case OVERWRITE:
                // check if we can trim in the range, before applying the modification to
                if (rangeToTrim.frameIn + offset >= rangeToTrim.frameOut)
                {
                    return false;
                }
                int slipOffset = offset;
                if (offset < 0)
                {
                    slipOffset = offset - gap;
                }
                else if (offset > 0 && gap != 0)
                {
                    slipOffset = 0;
                }
                final boolean trimOutSucceed = EditingMathUtils.trimOut(rangeBefore, slipOffset);
                if (trimOutSucceed == false)
                {
                    return false;
                }
                break;
            default:
                return false;
        }
        return trimIn(rangeToTrim, offset);
    }

    /**
     * trims out a range
     * 
     * @param range
     *            , the range to trim
     * @param offset
     *            , the offset of the frameOut
     * @return true if the operation succeed or false if the resulting frame out is <= frameIn
     */
    public static boolean trimOut(FrameRange range, int offset)
    {
        if (range.frameOut + offset <= range.frameIn)
        {
            return false;
        }
        range.frameOut += offset;
        return true;
    }

    /**
     * trims out a range
     * 
     * @param range
     *            , the range to trim
     * @param frameOut
     *            , the offset of the frameOut
     * @return true if the operation succeed or false if the resulting frame out is <= frameIn
     */
    public static boolean trimOutToFrame(FrameRange range, int frameOut)
    {
        return trimOut(range, frameOut - range.frameOut);
    }

    /**
     * trim-out a range in a list of range
     * 
     * @param sortedRanges
     *            , the list of and sorted ranges (ascendant)
     * @param rangeToTrim
     *            , the range in the list, we want to trim
     * @param offset
     *            , the offset of the trimming
     * @return true if the operation succeeds (or if the offset is zero), or false if the rangeToTrim is no contained in
     *         the list , if the list is not sorted, or a bad modification mode has been entered
     */
    public static boolean trimOut(List<FrameRange> sortedRanges, FrameRange rangeToTrim, int offset,
            EModificationMode mode)
    {
        if (offset == 0)
        {
            return true;
        }
        if (FrameRangeUtils.isRangeInList(sortedRanges, rangeToTrim) == false)
        {
            return false;
        }
        final int rangeIndex = sortedRanges.indexOf(rangeToTrim);
        if (sortedRanges.size() == 1 || rangeIndex == 0)
        { // FIXME should remove rangeIndex==0, make a test to prevent this error
            return EditingMathUtils.trimOut(rangeToTrim, offset);
        }
        // we check if the list is sorted
        if (!FrameRangeUtils.isRangeSorted(sortedRanges))
        {
            return false;
        }
        // we get the range after this range
        final FrameRange rangeAfter = sortedRanges.get(rangeIndex + 1);
        // we check if there is a gap betweend
        final int gap = FrameRangeUtils.getGap(rangeToTrim, rangeAfter);
        switch (mode)
        {
            case INSERT:
                if (offset > 0)
                {
                    if (offset - gap > rangeAfter.getLength())
                    {
                        return false;
                    }
                    int slipOffset = 0;
                    if (gap < offset)
                    {
                        slipOffset = offset - gap;
                    }
                    // then we slip all the edits starting at the rangeAfter position
                    boolean slip = slip(sortedRanges.subList(rangeIndex + 1, sortedRanges.size()), slipOffset);
                    if (slip == false)
                    {
                        return false;
                    }
                }
                break;
            case OVERWRITE:
                if (offset > 0)
                {
                    if (offset - gap > rangeAfter.getLength())
                    {
                        return false;
                    }
                    int trimOffset = 0;
                    if (gap < offset)
                    {
                        trimOffset = offset - gap;
                    }
                    // then we slip all the edits starting at the rangeAfter position
                    // FIXME please add a relevant comment
                    boolean trim = trimIn(rangeAfter, trimOffset);
                    if (trim == false)
                    {
                        return false;
                    }
                }
                else
                {
                    if (gap == 0)
                    {
                        boolean trimOutTheRange = trimOut(rangeToTrim, offset);
                        if (trimOutTheRange == false)
                        {
                            return false;
                        }
                        boolean trimInAfter = trimIn(rangeAfter, offset);
                        if (trimInAfter == false)
                        {
                            return false;
                        }
                        return true;
                    }
                }
                break;
            default:
                return false;
        }
        return trimOut(rangeToTrim, offset);
    }

    /*
     * CUT
     */
    /**
     * cuts a range
     * 
     * @param range
     *            , the range to cut
     * @param cuttedRange
     *            , the range where you want to put the "cutted part"
     * @param cutPosition
     *            , the position in the range, where you want to cut
     * @return true if the operation succeeds of false, if cutPosition is <= range.frameIn and >= range.frameOut
     * 
     *         example : cutting the range [0,10] at the position 5, will set the range to [0,5], and the cuttedRange to
     *         [5,10]
     */
    public static boolean cut(FrameRange range, FrameRange cuttedRange, int cutPosition)
    {
        if (cutPosition <= range.frameIn || cutPosition >= range.frameOut)
        {
            return false;
        }
        if (cuttedRange == null)
        {
            return true;
        }
        cuttedRange.frameIn = cutPosition;
        cuttedRange.frameOut = range.frameOut;
        range.frameOut = cutPosition;
        return true;
    }

    public static boolean ripple(List<FrameRange> sortedRanges)
    {
        // we check if the list is sorted
        if (!FrameRangeUtils.isRangeSorted(sortedRanges))
        {
            return false;
        }
        for (int i = 0; i < sortedRanges.size() - 1; i++)
        {
            final FrameRange range = sortedRanges.get(i);
            final FrameRange rangeAfter = sortedRanges.get(i + 1);
            final int gap = FrameRangeUtils.getGap(range, rangeAfter);
            if (FrameRangeUtils.isOverlapping(range, rangeAfter))
            {
                final boolean slipToFrameInSucceed = slipToFrameIn(rangeAfter, range.frameOut);
                if (slipToFrameInSucceed == false)
                {
                    return false;
                }
                continue;
            }
            if (gap == 0)
            {
                continue;
            }
            final boolean slipSucceed = slip(rangeAfter, -gap);
            if (slipSucceed == false)
            {
                return false;
            }
        }
        return true;
    }

    public static boolean move(List<FrameRange> sortedRanges, FrameRange rangeToMove, int moveLocation,
            EModificationMode mode)
    {
        if (FrameRangeUtils.isRangeInList(sortedRanges, rangeToMove) == false)
        {
            return false;
        }
        if (moveLocation < 0)
        {
            return false;
        }
        final List<FrameRange> initialRanges = new ArrayList<FrameRange>(sortedRanges);
        boolean slipSuccess = slipToFrameIn(rangeToMove, moveLocation);
        if (slipSuccess == false)
        {
            return false;
        }
        sortedRanges.remove(rangeToMove);
        boolean insert = insert(sortedRanges, rangeToMove, mode);
        if (insert == false)
        {
            sortedRanges.clear();
            sortedRanges.addAll(initialRanges);
            return false;
        }
        return true;
    }

    public static void shiftToRight(FrameRange rangeToShift, int shiftedFrameIn, List<FrameRange> sortedRanges)
    {
        if (shiftedFrameIn < 0)
        {
            return;
        }
        if (shiftedFrameIn <= rangeToShift.frameIn)
        {
            return;
        }
        // we slip te range
        slipToFrameIn(rangeToShift, shiftedFrameIn);
        if (FrameRangeUtils.isRangeInList(sortedRanges, rangeToShift) == false)
        {
            return;
        }
        int rangeIndex = sortedRanges.indexOf(rangeToShift);
        // if we are at the lastest element, we stop
        if (rangeIndex == sortedRanges.size() - 1)
        {
            Collections.sort(sortedRanges);
            return;
        }
        // then whe shift the next range if necessary
        FrameRange nextRange = sortedRanges.get(rangeIndex + 1);
        // if the "splipped" range to shift has moved after the next range, we must shift the nextRange
        if (rangeToShift.compareTo(nextRange) >= 0)
        {
            shiftToRight(nextRange, rangeToShift.frameOut, sortedRanges);
        }
        // else if the next range is not overlapping the shifted range, there is no need to shift the next range.
        if (FrameRangeUtils.isOverlapping(rangeToShift, nextRange) == false)
        {
            Collections.sort(sortedRanges);
            return;
        }
        shiftToRight(nextRange, rangeToShift.frameOut, sortedRanges);
    }

    public static boolean insert(List<FrameRange> sortedRanges, FrameRange rangeToInsert, EModificationMode mode)
    {
        if (sortedRanges.isEmpty())
        {
            sortedRanges.add(rangeToInsert);
            return true;
        }
        if (rangeToInsert.frameIn < 0)
        {
            return false;
        }
        // we get the ranges that are possibly "covering" the range-to-insert
        List<FrameRange> rangesUnder = FrameRangeUtils.rangesBetween(rangeToInsert.frameIn, rangeToInsert.frameOut,
                sortedRanges);
        switch (mode)
        {
            case INSERT:
            {
                if (rangesUnder.isEmpty())
                {
                    break;
                }
                FrameRange rangeToShift = rangesUnder.get(0);
                shiftToRight(rangeToShift, rangeToInsert.frameOut, sortedRanges);
                break;
            }
            case OVERWRITE:
            {
                if (rangesUnder.isEmpty())
                {
                    break;
                }
                List<FrameRange> rangesToCut = new ArrayList<FrameRange>();
                for (FrameRange range : rangesUnder)
                {
                    if (rangeToInsert.frameIn <= range.frameIn && rangeToInsert.frameOut >= range.frameOut)
                    {
                        continue;
                    }
                    else if (rangeToInsert.frameIn < range.frameOut || range.frameOut > rangeToInsert.frameOut)
                    {
                        rangesToCut.add(range);
                    }
                }
                if (rangesToCut.isEmpty() == false)
                {
                    rangesUnder.removeAll(rangesToCut);
                    for (FrameRange range : rangesToCut)
                    {
                        if (rangeToInsert.frameIn > range.frameIn && rangeToInsert.frameOut < range.frameOut)
                        {
                            // in this case the range to inserts is included in the range under.
                            // so we need to cut the range at rangeToInsert.frameIn, and trim out the cutted part to
                            // rangeToInsert.frameOut
                            FrameRange cuttedPart = new FrameRange();
                            boolean cut = cut(range, cuttedPart, rangeToInsert.frameIn);
                            if (cut == false)
                            {
                                return false;
                            }
                            boolean trim = trimInToFrame(cuttedPart, rangeToInsert.frameOut);
                            if (trim == false)
                            {
                                return false;
                            }
                            sortedRanges.add(cuttedPart);
                        }
                        else if (range.frameOut > rangeToInsert.frameOut)
                        {
                            boolean canTrim = trimInToFrame(range, rangeToInsert.frameOut);
                            if (canTrim == false)
                            {
                                return false;
                            }
                        }
                        else if (rangeToInsert.frameIn < range.frameOut)
                        {
                            boolean canTrim = trimOutToFrame(range, rangeToInsert.frameIn);
                            if (canTrim == false)
                            {
                                return false;
                            }
                        }
                    }
                }
                // we remove all the ranges under
                sortedRanges.removeAll(rangesUnder);
                break;
            }
            default:
                return false;
        }
        sortedRanges.add(rangeToInsert);
        Collections.sort(sortedRanges);
        return true;
    }

    public static boolean merge(List<FrameRange> sortedRanges, FrameRange rangeStart, FrameRange rangeEnd)
    {
        if (rangeStart.compareTo(rangeEnd) > 0)
        {
            return false;
        }
        if (FrameRangeUtils.isRangeInList(sortedRanges, rangeStart) == false)
        {
            return false;
        }
        // guaranteed not to return -1 because rangeStart is in sortedRange
        final int firstIndex = sortedRanges.indexOf(rangeStart);
        if (FrameRangeUtils.isRangeInList(sortedRanges, rangeEnd) == false)
        {
            return false;
        }
        // guaranteed not to return -1 because rangeEnd is in sortedRange
        final int lastIndex = sortedRanges.indexOf(rangeEnd);
        final FrameRange newRange = new FrameRange(rangeStart.frameIn, rangeEnd.frameOut);
        sortedRanges.set(firstIndex, newRange);
        // checking the bounds are respecting the contract of subList
        // exception will throw if (fromIndex < 0 || toIndex > size || fromIndex > toIndex)
        //
        // fromIndex > 0, because firstIndex is guaranteed to be >=0 see above.
        final int fromIndex = firstIndex + 1;
        // lastIndex might be equal to size-1 if its the last element
        // so toIndex might be equal to size but no more
        final int toIndex = lastIndex + 1;
        // fromIndex is below toIndex because of the compareTo test at the beginning of this method
        // The following function is guaranteed not to throw
        sortedRanges.subList(fromIndex, toIndex).clear();
        return true;
    }

    public static boolean delete(List<FrameRange> sortedRanges, FrameRange rangeToDelete, EModificationMode mode)
    {
        if (sortedRanges.contains(rangeToDelete) == false)
        {// FIXME test if isRangeInList instead of contains
            return false;
        }
        int indexToDelete = sortedRanges.indexOf(rangeToDelete);
        sortedRanges.remove(rangeToDelete);
        switch (mode)
        {
            case INSERT:
                return true;
            case OVERWRITE:
                return slip(sortedRanges.subList(indexToDelete, sortedRanges.size()), -rangeToDelete.getLength());
            default:
                return false;
        }
    }
}
