package fr.hd3d.common.ui.client.modeldata.playlist.math;

public enum EModificationMode {
    INSERT("INSERT"), //
    OVERWRITE("OVERWRITE");
    private final String modeName;

    private EModificationMode(String modeName) {
        this.modeName = modeName;
    }

    public String getModeName() {
        return modeName;
    }
}
