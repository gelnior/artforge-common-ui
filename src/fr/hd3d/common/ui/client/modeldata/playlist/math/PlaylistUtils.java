package fr.hd3d.common.ui.client.modeldata.playlist.math;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import fr.hd3d.common.ui.client.modeldata.playlist.PlayListEditModelData;


/**
 * 
 * Utilitary class to handle PlayList commands.
 * 
 * @author jules-pajot
 * @author guillaume-maucomble
 * @version 1.1
 * @since 1.0
 * 
 */
public class PlaylistUtils
{
    public final static int MOVE_UP = -1;
    public final static int MOVE_DOWN = 1;

    public static boolean slipSrc(PlayListEditModelData edit, int offset)
    {
        boolean slip = EditingMathUtils.slip(edit.getSrcRange(), offset);
        edit.commitRangeChanges();
        return slip;
    }

    public static boolean slipRec(PlayListEditModelData edit, int offset)
    {
        final boolean slip = EditingMathUtils.slip(edit.getRecRange(), offset);
        if (slip)
            edit.commitRangeChanges();
        return slip;
    }

    public static boolean slipRec(List<PlayListEditModelData> edits, int offset)
    {
        // check if we can slip all edits
        if (EditingMathUtils.canSlip(getRecRanges(edits), offset))
        {
            for (PlayListEditModelData edit : edits)
            {
                if (!slipRec(edit, offset))
                    return false;
            }
            return true;
        }
        return false;
    }

    /**
     * Comparator of PlayListEditModelData based on RecIn/RecOut values.
     */
    public static Comparator<PlayListEditModelData> EditComparator = new Comparator<PlayListEditModelData>() {
        public int compare(PlayListEditModelData o1, PlayListEditModelData o2)
        {
            if (o1.getRecIn() != null && o1.getRecIn().equals(o2.getRecIn()))
            {
                return o1.getRecOut() != null && o1.getRecOut().equals(o2.getRecOut()) ? 0 : (o1.getRecOut() > o2
                        .getRecOut()) ? 1 : -1;
            }
            else
            {
                return (o1.getRecOut() > o2.getRecOut()) ? 1 : -1;
            }
        }
    };

    /**
     * Erases the gaps between PlayListEditModelData objects of a collection. This uses
     * {@link EditingMathUtils#ripple(List)} to replace PlayListEditModelData objects at correct position.
     * 
     * @param collection
     *            Edit collection to which the ripple command applies
     * @return Success of computation
     */
    public static boolean ripple(List<PlayListEditModelData> collection)
    {
        if (collection.isEmpty() || collection.size() == 1)
            return false;

        // Ensure the collection is ordered
        Collections.sort(collection, EditComparator);
        List<FrameRange> frl = getRecRanges(collection);
        if (EditingMathUtils.ripple(frl))
        {
            Iterator<PlayListEditModelData> iterator = collection.iterator();
            while (iterator.hasNext())
            {
                PlayListEditModelData edit = iterator.next();
                edit.commitRangeChanges();
            }
            return true;
        }
        return false;
    }

    /**
     * Moves collection from playlist with specified offset. The method looks for the affected edit on playlist, and
     * computes the offset to be applied to Rec values of collection to this affected edit, and to the collection. Then
     * it uses {@link #slipRec(List, int)} to do the actual move.
     * 
     * @param collection
     *            List of PlayListEditModelData objects to move
     * @param playlist
     *            Original list of PlayListEditModelData to which collection belongs
     * @param offset
     *            integer value of offset
     * @return Success of computation
     */
    private static boolean contiguousMove(List<PlayListEditModelData> collection, List<PlayListEditModelData> playlist,
            int offset)
    {
        // start is the index on the playlist of the first PlayListEditModelData object
        final int start = playlist.indexOf(collection.get(0));

        // PlayListEditModelData that is affected by the move command.
        int size = (offset > 0) ? offset * collection.size() : offset;
        PlayListEditModelData affectedEdit = playlist.get(start + size);

        final int newRecInForSelectedEdits = offset > 0 ? collection.get(0).getRecIn()
                + (affectedEdit.getRecOut() - affectedEdit.getRecIn()) : affectedEdit.getRecIn();

        final int selectedOffset = offset * Math.abs(newRecInForSelectedEdits - collection.get(0).getRecIn());
        final int selectedDuration = Math.abs(collection.get(collection.size() - 1).getRecOut()
                - collection.get(0).getRecIn());

        //
        int affectedOffset = affectedEdit.getRecIn();
        affectedOffset -= (offset > 0) ? collection.get(0).getRecIn() : affectedEdit.getRecIn() + selectedDuration;

        // if (offset > 0)
        affectedOffset *= -1;

        // affectedOffset = -1 * offset
        // * Math.abs((collection.get(collection.size() - 1).getRecOut() - collection.get(0).getRecIn()));

        PlaylistUtils.slipRec(collection, selectedOffset);
        PlaylistUtils.slipRec(affectedEdit, affectedOffset);

        // Keep the playlist sorted
        Collections.sort(playlist, EditComparator);

        return true;
    }

    /**
     * Moves (if possible) selected PlayListEditModelData on playlist. The computation is divided in two parts :
     * <ol>
     * <li>Find contiguous PlayListEditModelData of selectedEdits list (selectedEdits are thus ordered first)</li>
     * <li>Perform the actual move command using {@link #contiguousMove(List, List, int)}</li>
     * </ol>
     * 
     * @param selectedEdits
     *            PlayListEditModelData to be moved
     * @param playlist
     *            Original List
     * @param offset
     *            number of indexes to swap
     * @return success of computation
     */
    public static boolean move(List<PlayListEditModelData> selectedEdits, List<PlayListEditModelData> playlist,
            int offset)
    {
        // Out of bounds indexes fool keeper
        if (offset == 0)
            return false;
        if (playlist.isEmpty())
            return false;
        if (selectedEdits.isEmpty())
            return false;

        final int start = playlist.indexOf(selectedEdits.get(0));
        if ((start == 0 && offset < 0) || (start == playlist.size() - 1 && offset > 0))
            return false;
        int end = playlist.indexOf(selectedEdits.get(selectedEdits.size() - 1));
        if (start != end && (end == 0 && offset < 0) || (end == playlist.size() - 1 && offset > 0))
            return false;

        // Quick way if only one element
        if (selectedEdits.size() == 1)
            return PlaylistUtils.contiguousMove(selectedEdits, playlist, offset);

        // Makes sure the selected PlayListEditModelData are ordered. This matters for the following loop.
        Collections.sort(selectedEdits, EditComparator);
        // Keep track ok contiguous PlayListEditModelData objects
        List<PlayListEditModelData> followers = new ArrayList<PlayListEditModelData>();
        for (PlayListEditModelData edit : selectedEdits)
        {
            final int mIndex = playlist.indexOf(edit);
            if (followers.isEmpty() || followers.get(followers.size() - 1) == playlist.get(mIndex - 1))
            {
                followers.add(edit);
            }
            else
            {
                // Perform contiguousMove for this sublist
                if (!PlaylistUtils.contiguousMove(followers, playlist, offset))
                    return false;
                // When done, clear the list, and add current PlayListEditModelData object
                followers.clear();
                followers.add(edit);
            }
        }
        // Perform contiguousMove on the last list of PlayListEditModelData followers
        if (!PlaylistUtils.contiguousMove(followers, playlist, offset))
            return false;
        return true;
    }

    /**
     * Modifies both record and source ranges for the trimed PlayListEditModelData. It does not handle any modification
     * on neither previous nor following PlayListEditModelData objects.
     * 
     * @param edit
     *            PlayListEditModelData to which the trim computation is applied.
     * @param offset
     *            trim offset
     * @return Success of the computation
     */
    public static boolean trimIn(PlayListEditModelData edit, int offset)
    {
        boolean trimIn = EditingMathUtils.trimIn(edit.getRecRange(), offset);
        if (trimIn)
        {
            EditingMathUtils.trimIn(edit.getSrcRange(), offset);
            edit.commitRangeChanges();
            return true;
        }
        return false;
    }

    /**
     * Modifies both record and source In ranges for the trimed PlayListEditModelData and the following
     * PlayListEditModelData objects.
     * 
     * @param edit
     *            PlayListEditModelData to which the trim computation is applied.
     * @param playlist
     *            collection of {@link PlayListEditModelData} in which edit is included
     * @param offset
     *            trim offset
     * @return Success of the computation
     */
    public static boolean trimIn(PlayListEditModelData edit, List<PlayListEditModelData> playlist, int offset)
    {

        if (offset == 0)
        {
            return true;
        }
        if (playlist.contains(edit) == false)
        {
            return false;
        }
        final int rangeIndex = playlist.indexOf(edit);
        if (playlist.size() == 1 || rangeIndex == 0)
        {
            return trimIn(edit, offset);
        }
        // we get the preceding range
        final FrameRange rangeToTrim = edit.getRecRange();
        PlayListEditModelData editBefore = playlist.get(rangeIndex - 1);
        final FrameRange rangeBefore = editBefore.getRecRange();
        // we check if there is a gap between
        final int gap = rangeToTrim.getGap(rangeBefore);
        // check if we can trim in the range, before applying the modification to
        if (rangeToTrim.frameIn + offset >= rangeToTrim.frameOut)
        {
            return false;
        }
        int slipOffset = offset;
        if (offset < 0)
        {
            slipOffset = offset - gap;
        }
        else if (offset > 0 && gap != 0)
        {
            slipOffset = 0;
        }
        final boolean trimOutSucceed = trimOut(editBefore, slipOffset);
        if (trimOutSucceed == false)
        {
            return false;
        }
        return trimIn(edit, offset);
    }

    /**
     * Returns a list of FrameRange objects that are included in PlayListEditModelData objects. Its size is the size of
     * the input collection.
     * 
     * @param collection
     *            List of PlayListEditModelData objects
     * @return FrameRange list
     */
    public static List<FrameRange> getRecRanges(List<PlayListEditModelData> collection)
    {
        final ArrayList<FrameRange> ranges = new ArrayList<FrameRange>(collection.size());
        for (PlayListEditModelData edit : collection)
        {
            ranges.add(edit.getRecRange());
        }
        return ranges;
    }

    /**
     * Modifies both record and source Out ranges for the trimed PlayListEditModelData. It does not handle any
     * modification on neither previous nor following PlayListEditModelData objects.
     * 
     * @param edit
     *            PlayListEditModelData to which the trim computation is applied.
     * @param offset
     *            trim offset
     * @return Success of the computation
     */
    public static boolean trimOut(PlayListEditModelData edit, int offset)
    {
        boolean trimIn = EditingMathUtils.trimOut(edit.getRecRange(), offset);
        if (trimIn)
        {
            EditingMathUtils.trimOut(edit.getSrcRange(), offset);
            edit.commitRangeChanges();
            return true;
        }
        return false;
    }

    /**
     * Modifies both record and source Out ranges for the trimed PlayListEditModelData and the following
     * PlayListEditModelData objects.
     * 
     * @param edit
     *            PlayListEditModelData to which the trim computation is applied.
     * @param playlist
     *            collection of {@link PlayListEditModelData} in which edit is included
     * @param offset
     *            trim offset
     * @return Success of the computation
     */
    public static boolean trimOut(PlayListEditModelData edit, List<PlayListEditModelData> playlist, int offset)
    {
        if (offset == 0)
        {
            return true;
        }
        if (playlist.contains(edit) == false)
        {
            return false;
        }
        List<FrameRange> recRanges = getRecRanges(playlist);
        final int rangeIndex = recRanges.indexOf(edit.getRecRange());
        if (recRanges.size() == 1 || rangeIndex == 0)
        {
            return trimOut(edit, offset);
        }
        // we check if the list is sorted
        if (!FrameRangeUtils.isRangeSorted(recRanges))
        {
            return false;
        }
        // we get the range after this range
        if (recRanges.size() > rangeIndex)
        {
            final FrameRange rangeAfter = recRanges.get(rangeIndex + 1);
            // we check if there is a gap betweend
            final int gap = FrameRangeUtils.getGap(edit.getRecRange(), rangeAfter);
            if (offset > 0)
            {
                if (offset - gap > rangeAfter.getLength())
                {
                    return false;
                }
                int slipOffset = offset;
                if (gap < offset)
                {
                    slipOffset = offset - gap;
                }
                // then we slip all the edits starting at the rangeAfter position
                boolean slip = slipRec(playlist.subList(rangeIndex + 1, recRanges.size()), slipOffset);
                if (slip == false)
                {
                    return false;
                }
            }
        }
        return trimOut(edit, offset);
    }

}
