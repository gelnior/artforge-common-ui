package fr.hd3d.common.ui.client.modeldata.playlist.math;

/**
 * A FrameRange is basically the duplet (frameIn, frameOut) where (frameIn > frameOut).
 * 
 * @version 1.0
 * @since 1.0
 */
public class FrameRange implements Comparable<FrameRange>
{
    public int frameIn = 0;
    public int frameOut = 0;

    /**
     * Creates a FrameRange with default values (0,0)
     */
    public FrameRange()
    {}

    /**
     * Creates a FrameRange object from another one.
     * 
     * @param other
     *            FrameRange
     */
    public FrameRange(FrameRange other)
    {
        this(other.frameIn, other.frameOut);
    }

    public FrameRange(int[] inAndOut)
    {
        this(inAndOut[0], inAndOut[1]);
    }

    public FrameRange(int frameIn, int frameOut)
    {
        if (frameIn > frameOut)
        {
            throw new IllegalArgumentException("frameIn > frameOut");
        }
        this.frameIn = frameIn;
        this.frameOut = frameOut;
    }

    public int getLength()
    {
        return frameOut - frameIn;
    }

    // @Override
    // protected FrameRange clone()
    // {
    // return new FrameRange(frameIn, frameOut);
    // }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + frameIn;
        result = prime * result + frameOut;
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        FrameRange other = (FrameRange) obj;
        if (frameIn != other.frameIn)
            return false;
        if (frameOut != other.frameOut)
            return false;
        return true;
    }

    @Override
    public String toString()
    {
        return "FrameRange [frameIn=" + frameIn + ", frameOut=" + frameOut + "]";
    }

    /**
     * compares two ranges. if the two ranges have the same frame in, the biggest is "superior" to the other
     */
    public int compareTo(FrameRange o)
    {
        return FrameUtils.compareRange(frameIn, frameOut, o.frameIn, o.frameOut);
    }

    /**
     * returns true if the other range is contiguous to this range example : [0,10] and [10,20] or [20,30] and [10,20]
     * are contiguous
     * 
     * @param otherRange
     */
    public boolean isContiguous(FrameRange otherRange)
    {
        return FrameRangeUtils.isContiguous(this, otherRange);
    }

    /**
     * returns true if other range overlaps another
     * 
     * @param otherRange
     */
    public boolean isOverlapping(FrameRange otherRange)
    {
        return FrameRangeUtils.isOverlapping(this, otherRange);
    }

    /**
     * computes the gap between two ranges
     * 
     * @param otherRange
     * @return a positive or negative integer : the gap in frames, between the two ranges
     */
    public int getGap(FrameRange otherRange)
    {
        return FrameRangeUtils.getGap(this, otherRange);
    }
}
