package fr.hd3d.common.ui.client.modeldata.playlist;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.asset.ProxyModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.math.FrameRange;
import fr.hd3d.common.ui.client.util.field.IntegerDataField;
import fr.hd3d.common.ui.client.util.field.LongDataField;


/**
 * Model Data for describing a Playlist Edit base record.
 * 
 * @version 1.0
 * @since 1.0
 */
public class PlayListEditModelData extends RecordModelData
{

    private static final long serialVersionUID = 8494830780580435615L;

    public static final String SIMPLE_CLASS_NAME = "LPlayListEdit";
    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LPlayListEdit";
    public static final String SRCIN_FIELD = "srcIn";
    public static final String SRCOUT_FIELD = "srcOut";
    public static final String RECIN_FIELD = "recIn";
    public static final String RECOUT_FIELD = "recOut";
    // public static final String FILE_REVISION_FIELD = "fileRevision";
    public static final String PROXY_FIELD = "proxy";
    public static final String PLAYLIST_REVISION_FIELD = "playListRevision";

    public static ModelType getModelType()
    {
        final ModelType type = RecordModelData.getModelType();
        type.addField(new IntegerDataField(SRCIN_FIELD));
        type.addField(new IntegerDataField(SRCOUT_FIELD));
        type.addField(new IntegerDataField(RECIN_FIELD));
        type.addField(new IntegerDataField(RECOUT_FIELD));
        // type.addField(ModelDataUtils.createDataField(FILE_REVISION_FIELD, Long.class,
        // FileRevisionModelData.SIMPLE_CLASS_NAME));
        type.addField(new LongDataField(PROXY_FIELD, ProxyModelData.SIMPLE_CLASS_NAME));
        type.addField(new LongDataField(PLAYLIST_REVISION_FIELD, PlayListRevisionModelData.SIMPLE_CLASS_NAME));
        return type;
    }

    private final FrameRange srcRange = new FrameRange();
    private final FrameRange recRange = new FrameRange();

    public PlayListEditModelData()
    {
        this(null);
    }

    public PlayListEditModelData(String name)
    {
        super();
        setSimpleClassName(SIMPLE_CLASS_NAME);
        setClassName(CLASS_NAME);
        setName(name);
    }

    public void setSrcIn(Integer srcIn)
    {
        this.set(SRCIN_FIELD, srcIn);
    }

    /**
     * @return the srcIn or 0 if no src was set
     */
    public Integer getSrcIn()
    {
        return this.get(SRCIN_FIELD, 0);
    }

    public void setSrcOut(Integer srcOut)
    {
        this.set(SRCOUT_FIELD, srcOut);
    }

    /**
     * @return the srcOut or 0 if no src was set
     */
    public Integer getSrcOut()
    {
        return this.get(SRCOUT_FIELD, 0);
    }

    public void setRecIn(Integer recIn)
    {
        this.set(RECIN_FIELD, recIn);
    }

    /**
     * @return the recIn or 0 if no rec was set
     */
    public Integer getRecIn()
    {
        return this.get(RECIN_FIELD, 0);
    }

    public void setRecOut(Integer recOut)
    {
        this.set(RECOUT_FIELD, recOut);
    }

    /**
     * @return gets the srcOut or 0 if no rec was set
     */
    public Integer getRecOut()
    {
        return this.get(RECOUT_FIELD, 0);
    }

    // public void setFileRevision(Long fileRevisionId)
    // {
    // this.set(FILE_REVISION_FIELD, fileRevisionId);
    // }
    //
    // public Long getFileRevision()
    // {
    // return this.get(FILE_REVISION_FIELD);
    // }

    public void setProxy(Long proxyId)
    {
        this.set(PROXY_FIELD, proxyId);
    }

    public Long getProxy()
    {
        return this.get(PROXY_FIELD);
    }

    public void setPlayListRevision(Long playListRevisionId)
    {
        this.set(PLAYLIST_REVISION_FIELD, playListRevisionId);
    }

    public Long getPlayListRevision()
    {
        return this.get(PLAYLIST_REVISION_FIELD);
    }

    public FrameRange getSrcRange()
    {
        return srcRange;
    }

    public void setSrcRange(FrameRange range)
    {
        if (range == null)
        {
            throw new NullPointerException("range must not be null");
        }
        setSrcIn(range.frameIn);
        setSrcOut(range.frameOut);
    }

    public FrameRange getRecRange()
    {
        return recRange;
    }

    public void setRecRange(FrameRange range)
    {
        if (range == null)
        {
            throw new NullPointerException("range must not be null");
        }
        setRecIn(range.frameIn);
        setRecOut(range.frameOut);
    }

    @Override
    public <X extends Object> X set(String property, X value)
    {
        if (value != null)
        {
            if (SRCIN_FIELD.equals(property))
            {
                srcRange.frameIn = (Integer) value;
            }
            else if (SRCOUT_FIELD.equals(property))
            {
                srcRange.frameOut = (Integer) value;
            }
            else if (RECIN_FIELD.equals(property))
            {
                recRange.frameIn = (Integer) value;
            }
            else if (RECOUT_FIELD.equals(property))
            {
                recRange.frameOut = (Integer) value;
            }
        }
        return super.set(property, value);
    };

    /**
     * copies src FrameRange and rec FrameRanges to src and rec In &nd Out fields Use this method, when you change
     * frameRanges in and out. To ensure data consistency between ranges and properties.
     */
    public void commitRangeChanges()
    {
        if (srcRange.frameIn != getSrcIn())
        {
            setSrcIn(srcRange.frameIn);
        }
        if (srcRange.frameOut != getSrcOut())
        {
            setSrcOut(srcRange.frameOut);
        }
        if (recRange.frameIn != getRecIn())
        {
            setRecIn(recRange.frameIn);
        }
        if (recRange.frameOut != getRecOut())
        {
            setRecOut(recRange.frameOut);
        }
    }

}
