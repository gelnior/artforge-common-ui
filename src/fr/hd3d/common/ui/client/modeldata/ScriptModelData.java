package fr.hd3d.common.ui.client.modeldata;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.util.field.IntegerDataField;


/**
 * Model representing the class LScript.
 * 
 * @author HD3D
 * 
 */
public class ScriptModelData extends RecordModelData
{
    private static final long serialVersionUID = 118437662888877910L;

    public static final String SIMPLE_CLASS_NAME = "script";
    public static final String CLASS_NAME = "fr.hd3d.model.lightweight.impl.LScript";

    public static final String TRIGGER_FIELD = "trigger";
    public static final String DESCRIPTION_FIELD = "description";
    public static final String LOCATION_FIELD = "description";
    public static final String EXITCODE_FIELD = "exitCode";
    public static final String LASTRUN_FIELD = "lastRun";
    public static final String LAST_EXITCODE_FIELD = "lastExitCode";
    public static final String LASTRUN_MESSAGE_FIELD = "lastRunMessage";

    /**
     * Default constructor.
     */
    public ScriptModelData()
    {
        super();

        this.setClassName(CLASS_NAME);
        this.setSimpleClassName(SIMPLE_CLASS_NAME);
    }

    /**
     * @return Model type used by reader to know the record model data structure.
     */
    public static ModelType getModelType()
    {
        ModelType modelType = RecordModelData.getModelType();

        modelType.addField(TRIGGER_FIELD);
        modelType.addField(DESCRIPTION_FIELD);
        modelType.addField(LOCATION_FIELD);
        modelType.addField(new IntegerDataField(EXITCODE_FIELD));
        modelType.addField(LASTRUN_FIELD);
        modelType.addField(new IntegerDataField(LAST_EXITCODE_FIELD));
        modelType.addField(LASTRUN_MESSAGE_FIELD);

        return modelType;
    }
}
