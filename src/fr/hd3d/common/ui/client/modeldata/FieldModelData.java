package fr.hd3d.common.ui.client.modeldata;

import com.extjs.gxt.ui.client.data.BaseModelData;


/**
 * Field Model Data has two fields (id field is useless),
 * 
 * @author HD3D
 */
public class FieldModelData extends BaseModelData
{
    /**
     * Automatically generated serial UID
     */
    private static final long serialVersionUID = 3469550062627295337L;

    /** Name of the field. */
    public static final String ID_FIELD = "id";
    /** Name of the field. */
    public static final String NAME_FIELD = "name";
    /** Value of the field. */
    public static final String VALUE_FIELD = "value";

    /**
     * Default constructor.
     * 
     * @param name
     *            Field name.
     * @param value
     *            Field value.
     */
    public FieldModelData(int id, String name, Object value)
    {
        this.setId(id);
        this.setName(name);
        this.setValue(value);
    }

    public Integer getId()
    {
        return this.get(ID_FIELD);
    }

    public void setId(Integer id)
    {
        set(ID_FIELD, id);
    }

    public String getName()
    {
        return get(NAME_FIELD);
    }

    public void setName(String name)
    {
        set(NAME_FIELD, name);
    }

    public Object getValue()
    {
        return get(VALUE_FIELD);
    }

    public void setValue(Object value)
    {
        set(VALUE_FIELD, value);
    }
}
