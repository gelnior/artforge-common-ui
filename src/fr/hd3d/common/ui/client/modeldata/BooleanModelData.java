package fr.hd3d.common.ui.client.modeldata;



/**
 * ModelData used for boolean combobox to display true and false choice.
 * 
 * @author HD3D
 */
public class BooleanModelData extends NameModelData
{
    private static final long serialVersionUID = 7438132011287951819L;

    /** Value of the field. */
    public static final String VALUE_FIELD = "value";

    public BooleanModelData()
    {
        super(null);
    }

    public BooleanModelData(String name, Boolean value)
    {
        super(name);

        this.setValue(value);
    }

    public void setValue(Boolean value)
    {
        this.set(VALUE_FIELD, value);
    }

    public Boolean getValue()
    {
        return this.get(VALUE_FIELD);
    }

    @Override
    public String toString()
    {
        return this.getValue().toString();
    }
}
