package fr.hd3d.common.ui.client.modeldata;

import com.extjs.gxt.ui.client.data.BaseModel;


/**
 * Model data with a name field.
 * 
 * @author HD3D
 */
public class NameModelData extends BaseModel
{
    /**
     * Automatically generated serial UID
     */
    private static final long serialVersionUID = -408190064660384868L;

    /** Name of the field. */
    public static final String NAME_FIELD = "name";

    /**
     * Default constructor.
     * 
     * @param name
     *            Field name.
     */
    public NameModelData(String name)
    {
        this.setName(name);
    }

    public String getName()
    {
        return get(NAME_FIELD);
    }

    public void setName(String name)
    {
        set(NAME_FIELD, name);
    }

    @Override
    public boolean equals(Object o)
    {
        if (o instanceof NameModelData)
        {
            NameModelData nData = (NameModelData) o;
            String oname = nData.getName();
            String name = getName();
            if (name == oname || (name != null && name.equals(oname)))
            {
                return true;
            }
        }
        return false;
    }
}
