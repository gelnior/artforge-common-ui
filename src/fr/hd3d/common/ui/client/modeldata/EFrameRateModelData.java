package fr.hd3d.common.ui.client.modeldata;

import com.extjs.gxt.ui.client.data.BaseModelData;

import fr.hd3d.common.client.enums.EFrameRate;


/**
 * Standard ModelData for EFrameRate objects.
 * 
 * @author guillaume-maucomble
 * @version 1.0
 * @since 1.0
 */
public class EFrameRateModelData extends BaseModelData
{

    private static final long serialVersionUID = -3671825569803570151L;

    /**
     * Default constructor
     */
    public EFrameRateModelData()
    {

    }

    /**
     * Constructor that sets EFrameRate value.
     * 
     * @param framerate
     *            EFrameRate to be set
     */
    public EFrameRateModelData(EFrameRate framerate)
    {
        setFrameRate(framerate);
    }

    /**
     * Sets framerate value.
     * 
     * @param framerate
     *            EFrameRate value
     */
    public void setFrameRate(EFrameRate framerate)
    {
        set("framerate", framerate);
    }

    /**
     * Returns EFrameRate value.
     * 
     * @return EFrameRate value
     */
    public EFrameRate getFrameRate()
    {
        return get("framerate");
    }

}
