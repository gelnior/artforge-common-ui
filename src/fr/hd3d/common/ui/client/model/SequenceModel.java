package fr.hd3d.common.ui.client.model;

import java.util.List;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.InConstraint;
import fr.hd3d.common.ui.client.service.parameter.LuceneConstraint;

public class SequenceModel extends TreeModel
{
    /** LuceneConstraint to reload all shots from list of sequences ( included the sub sequences ). */
    private final InConstraint sequenceConstraintId = new InConstraint("parent.id");

    public void setConstraintSequence(List<SequenceModelData> sequences)
    {
        this.setConstraints(sequences, sequenceConstraintId);
    }

    public void removeSequenceConstraint()
    {
        this.store.removeParameter(sequenceConstraintId);
    }

}