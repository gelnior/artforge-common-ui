package fr.hd3d.common.ui.client.model;

import java.util.List;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.LuceneConstraint;


public class ShotModel extends TreeModel
{
    /** LuceneConstraint to reload all shots from list of sequences ( included the sub sequences ). */
    private final LuceneConstraint sequenceConstraintId = new LuceneConstraint(EConstraintOperator.in, "sequences_id");

    private final Constraint shotConstraintId = new Constraint(EConstraintOperator.in, "id");

    /**
     * Set the constraint on sequences id to reload all constituents.
     * 
     * @param sequences
     *            the list of sequences.
     */
    public void setConstraintSequence(List<SequenceModelData> sequences)
    {
        this.setConstraints(sequences, sequenceConstraintId);
    }

    public void setConstraintShot(List<ShotModelData> shots)
    {
        this.setConstraints(shots, shotConstraintId);
    }

    public void removeSequenceConstraint()
    {
        this.store.removeParameter(sequenceConstraintId);
    }

    public void removeShotConstraint()
    {
        this.getFilter().setRightMember(null);
    }
}
