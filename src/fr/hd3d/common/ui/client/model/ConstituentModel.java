package fr.hd3d.common.ui.client.model;

import java.util.ArrayList;
import java.util.List;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.LuceneConstraint;
import fr.hd3d.common.ui.client.util.CollectionUtils;


/**
 * Model handling constituent view data.
 * 
 * @author HD3D
 */
public class ConstituentModel extends TreeModel
{

    /** LuceneConstraint to reload all shots from list of categories (included the sub categories). */
    private final LuceneConstraint categoryConstraintId = new LuceneConstraint(EConstraintOperator.in, "categories_id");

    private final Constraint constituentConstraintId = new Constraint(EConstraintOperator.in, "id");

    /**
     * Set the constraint on categories id to reload all constituents.
     * 
     * @param categories
     *            the list of category.
     */
    public void setConstraintCategory(List<CategoryModelData> categories)
    {
        this.store.removeParameter(categoryConstraintId);
        if (CollectionUtils.isNotEmpty(categories))
        {
            ArrayList<String> idList = new ArrayList<String>();
            for (CategoryModelData category : categories)
            {
                idList.add(category.getId().toString());
            }
            this.categoryConstraintId.setLeftMember(idList);
            this.store.addParameter(categoryConstraintId);
        }
    }

    public void setConstraintConstituent(List<ConstituentModelData> constituents)
    {
        if (CollectionUtils.isNotEmpty(constituents))
        {
            ArrayList<Long> idList = new ArrayList<Long>();
            for (ConstituentModelData constituent : constituents)
            {
                idList.add(constituent.getId());
            }
            this.constituentConstraintId.setLeftMember(idList);
            this.getFilter().setRightMember(constituentConstraintId);
        }
    }

    public void removeCategoryConstraint()
    {
        this.store.removeParameter(categoryConstraintId);
    }

    public void removeConstituentConstraint()
    {
        this.getFilter().setRightMember(null);
    }

}
