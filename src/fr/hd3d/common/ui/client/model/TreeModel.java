package fr.hd3d.common.ui.client.model;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.store.TreeStore;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.LuceneConstraint;
import fr.hd3d.common.ui.client.widget.workobject.browser.model.WorkObjectModel;


public class TreeModel extends WorkObjectModel
{

    public static final String PATH_SEPARATOR = ">";

    /** Navigation tree data store. */
    protected TreeStore<RecordModelData> treeStore;

    /**
     * Set navigation tree data store.
     * 
     * @param store
     *            the store to set.
     */
    public void setTreeStore(TreeStore<RecordModelData> store)
    {
        this.treeStore = store;
    }

    /**
     * @return Navigation tree data store.
     */
    public TreeStore<RecordModelData> getTreeStore()
    {
        return this.treeStore;
    }

    /**
     * Remove the constraints on the model.
     */
    public void removeAllConstraints()
    {
        this.store.clearParameters();
    }

    /**
     * @param model
     *            The model of which path should be retrieved.
     * @return Path of a model in the navigation tree. Path is formatted in this way : parentId > parentId > ... >
     *         parentId.
     */
    public String getTreePath(Hd3dModelData model)
    {
        StringBuilder path = new StringBuilder();
        RecordModelData son = this.treeStore.findModel(Hd3dModelData.ID_FIELD, model.getId());
        RecordModelData parent = this.treeStore.getParent(son);
        List<Long> ids = new ArrayList<Long>();

        while (parent != null && parent.getId() > 0)
        {
            ids.add(parent.getId());
            parent = this.treeStore.getParent(parent);
        }

        path.append(model.getId());
        for (int i = ids.size() - 1; i >= 0; i--)
        {
            path.append(PATH_SEPARATOR);
            path.append(ids.get(i));
        }

        return path.toString();
    }

    /**
     * Set the constraint on model id.
     * 
     * 
     * @param models
     *            the list of models.
     * @param constraint
     *            constraint to update.
     */
    public void setConstraints(List<? extends Hd3dModelData> models, Constraint constraint)
    {
        if (models.isEmpty())
        {
            return;
        }

        ArrayList<Long> idList = new ArrayList<Long>();
        for (Hd3dModelData model : models)
        {
            idList.add(model.getId());
        }
        constraint.setLeftMember(idList);
        this.getFilter().setRightMember(constraint);
    }

    /**
     * Set the constraint on model id. Different from setConstraint(List, Constraint) because LuceneConstraint use
     * String and not Long.
     * 
     * @param models
     *            the list of models.
     * @param luceneConstraint
     *            LuceneConstraint to update.
     */
    public void setConstraints(List<? extends Hd3dModelData> models, LuceneConstraint luceneConstraint)
    {
        this.store.removeParameter(luceneConstraint);
        if (models.isEmpty())
        {
            return;
        }

        ArrayList<String> idList = new ArrayList<String>();
        for (Hd3dModelData model : models)
        {
            idList.add(model.getId().toString());
        }
        luceneConstraint.setLeftMember(idList);
        this.store.addParameter(luceneConstraint);
    }
}
