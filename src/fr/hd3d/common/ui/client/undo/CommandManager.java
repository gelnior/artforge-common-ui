package fr.hd3d.common.ui.client.undo;

import java.util.Stack;

import com.google.gwt.user.client.Command;


/**
 * Basic Command Manager that executes command while handling an undo/redo stacks. When a new command is executed if it
 * is undoable, the manager add it to the command stack. If a command is undone, its undo method is executed and the
 * command is added to the redo stack. If a new command is executed after undo, the redo stack is cleared.
 * 
 * @author HD3D
 */
public class CommandManager
{
    /** Command stack used to manage undo. */
    protected final Stack<UndoableCommand> undoCommandStack = new Stack<UndoableCommand>();
    /** Command stack used to manage redo. */
    protected final Stack<UndoableCommand> redoCommandStack = new Stack<UndoableCommand>();

    /**
     * Executes the command given in parameter. If the command is undoable, adds it to the Undo Stack and clears the
     * Redo Stack.
     * 
     * @param cmd
     *            The command to execute.
     */
    public final void executeCommand(Command cmd)
    {
        cmd.execute();

        if (cmd instanceof UndoableCommand)
        {
            undoCommandStack.push((UndoableCommand) cmd);
            redoCommandStack.clear();
        }
    }

    /**
     * Undoes last executed undoable command. Removes it from the Undo Stack. Add it to the Redo Stack.
     */
    public final void undo()
    {
        if (undoCommandStack.size() > 0)
        {
            UndoableCommand cmd = undoCommandStack.pop();
            cmd.undo();
            redoCommandStack.push(cmd);
        }
    }

    /**
     * Redoes last undid undoable command. Removes it from the Redo Stack. Add it to the Undo Stack.
     */
    public final void redo()
    {
        if (redoCommandStack.size() > 0)
        {
            UndoableCommand cmd = redoCommandStack.pop();
            cmd.execute();
            undoCommandStack.push(cmd);
        }
    }

    /**
     * Clears undo and redo stacks
     */
    public final void clearStacks()
    {
        undoCommandStack.clear();
        redoCommandStack.clear();
    }

    /**
     * @return The stack that contains all command that can be redo.
     */
    public final Stack<UndoableCommand> getRedoCommandStack()
    {
        return redoCommandStack;
    }

    /**
     * @return The stack that contains all command that can be undo.
     */
    public final Stack<UndoableCommand> getUndoCommandStack()
    {
        return undoCommandStack;
    }

    /** @return Name of the first command in the undo stack. */
    public String getFirstUndoCommandName()
    {
        return this.undoCommandStack.peek().getName();
    }

    /** @return Name of the last command in the undo stack. */
    public String getFirstRedoCommandName()
    {
        return this.redoCommandStack.peek().getName();
    }
}
