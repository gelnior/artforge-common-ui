package fr.hd3d.common.ui.client.undo;

import com.google.gwt.user.client.Command;


/**
 * UndoableCommand is used by the Command Manager to identify command which are undoable.
 * 
 * @author HD3D
 */
public abstract class UndoableCommand implements Command
{
    protected String name;

    /**
     * Default constructor.
     */
    public UndoableCommand()
    {}

    /**
     * Constructor sets the command name.
     * 
     * @param name
     *            Command name to set.
     */
    public UndoableCommand(String name)
    {
        this.name = name;
    }

    /**
     * @return The command name.
     */
    public String getName()
    {
        return name;
    }

    /**
     * Sets the command name.
     * 
     * @param name
     *            The command name.
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * Undo actions executed by this command.
     */
    public abstract void undo();
}
