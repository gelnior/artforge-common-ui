package fr.hd3d.common.ui.client.logs;

import com.extjs.gxt.ui.client.widget.Info;
import com.google.gwt.core.client.GWT;


/**
 * Methods for fast logging (work only in debug mode).
 * 
 * @author HD3D
 */
public class Logger
{
    /**
     * Display the log in the GWT console.
     * 
     * @param log
     *            The message to log.
     */
    public static void log(String log)
    {
        log(log, null);
    }

    /**
     * Display an integer into the GWT console.
     * 
     * @param count
     *            The integer to log.
     */
    public static void log(int count)
    {
        log((Integer.valueOf(count)).toString());
    }

    /**
     * Display the log in the GWT console with exception stack trace.
     * 
     * @param msg
     *            The message to log.
     * @param throwable
     *            The exception to trace.
     */
    public static void log(String msg, Throwable throwable)
    {
        if (GWT.isClient())
        {
            GWT.log(msg, throwable);
        }
        else
        {
            out(msg);
            if (throwable != null)
                throwable.printStackTrace();
        }
    }

    /**
     * Display the log in the GWT console. Message is prefixed with [WARN].
     * 
     * @param msg
     *            The message to log.
     */
    public static void warn(String msg)
    {
        Logger.log("[WARN] " + msg);
    }

    /**
     * Display the log in the GWT console. Message is prefixed with [ERROR].
     * 
     * @param msg
     *            The message to log.
     */
    public static void error(String msg)
    {
        Logger.log("[ERROR] " + msg);
    }

    /**
     * Display the log in the GWT console with exception stack trace. Message is prefixed with [ERROR].
     * 
     * @param msg
     *            The message to log.
     */
    public static void error(String msg, Exception e)
    {
        Logger.log("[ERROR] " + msg, e);
    }

    @Deprecated
    public static void exception(String log, Exception e)
    {
        GWT.log(log, e);
    }

    /**
     * Display for a few seconds a box with the log message.
     * 
     * @param log
     *            The message to log.
     */
    public static void display(String log)
    {
        Info.display("Log", log);
    }

    /**
     * Display for a few seconds a box with the log message.
     * 
     * @param logTitle
     *            The title of the message to log.
     * @param log
     *            The message to log.
     */
    public static void display(String logTitle, String log)
    {
        Info.display(logTitle, log);
    }

    /**
     * Log the message to the standard output.
     * 
     * @param log
     *            The message to log.
     */
    public static void out(String log)
    {
        System.out.println(log);
    }

}
