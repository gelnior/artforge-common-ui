package fr.hd3d.common.ui.client.util;

/**
 * Simple class to handle pair easily in javascript or GWT mode.
 * 
 * @author HD3D
 * 
 * @param <A>
 *            Type of first element of the pair.
 * @param <B>Type of second element of the pair.
 */
public class BasePair<A, B>
{
    private final A a;
    private final B b;

    /**
     * Constructor
     * 
     * @param a
     *            First element of the pair.
     * @param b
     *            Second element of the pair.
     */
    public BasePair(A a, B b)
    {
        this.a = a;
        this.b = b;
    }

    public A getA()
    {
        return a;
    }

    public B getB()
    {
        return b;
    }
}
