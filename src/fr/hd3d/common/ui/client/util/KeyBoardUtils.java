package fr.hd3d.common.ui.client.util;

/**
 * Static class providing util methods for managing keys typing.
 * 
 * @author HD3D
 */
public class KeyBoardUtils
{
    /**
     * @param code
     *            The key code
     * @return True if the typed key is not a navigation key (it means not arrows, page up or page down).
     */
    public static boolean isTextKey(int code)
    {
        // boolean isTestKey = code != KeyCodes.KEY_DOWN;
        // isTestKey &= code != KeyCodes.KEY_UP;
        // isTestKey &= code != KeyCodes.KEY_LEFT;
        // isTestKey &= code != KeyCodes.KEY_HOME;
        // isTestKey &= code != KeyCodes.KEY_RIGHT;
        // isTestKey &= code != KeyCodes.KEY_PAGEDOWN;
        // isTestKey &= code != KeyCodes.KEY_PAGEUP;
        // isTestKey &= code != KeyCodes.KEY_ENTER;
        //
        // return isTestKey;

        return code > 48 && code < 90;
    }
}
