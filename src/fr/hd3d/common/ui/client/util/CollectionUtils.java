/*
 * Apache Shiro Copyright 2008 The Apache Software Foundation
 * 
 * This product includes software developed at The Apache Software Foundation (http://www.apache.org/).
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more contributor license agreements. See the NOTICE
 * file distributed with this work for additional information regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the
 * License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 * 
 * Source modified by Florent Della Valle for the Hd3d Project
 */
package fr.hd3d.common.ui.client.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;
import java.util.Set;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListEditModelData;


/**
 * Set of methods to facilitate collections handling.
 * 
 * @author Jeremy Haile
 * @author Les Hazlewood
 * @author HD3D
 */
public class CollectionUtils
{

    /**
     * @param <E>
     *            The type associated to class.
     * @param clazz
     *            The class of collection elements.
     * @return Return an empty collection of the desired type.
     */
    @SuppressWarnings({ "unchecked" })
    public static <E> Collection<E> emptyCollection(Class<E> clazz)
    {
        return Collections.EMPTY_SET;
    }

    @SuppressWarnings({ "unchecked" })
    public static <E> Set<E> asSet(E... elements)
    {
        if (elements == null || elements.length == 0)
        {
            return Collections.EMPTY_SET;
        }
        LinkedHashSet<E> set = new LinkedHashSet<E>(elements.length * 4 / 3 + 1);
        Collections.addAll(set, elements);
        return set;
    }

    /**
     * @param <E>
     *            Element type.
     * @param elements
     *            Elements to add to the list.
     * @return From several elements build an array list.
     */
    @SuppressWarnings({ "unchecked" })
    public static <E> List<E> asList(E... elements)
    {
        if (elements == null || elements.length == 0)
        {
            return Collections.EMPTY_LIST;
        }

        // Avoid integer overflow when a large array is passed in
        int capacity = computeListCapacity(elements.length);
        ArrayList<E> list = new ArrayList<E>(capacity);
        Collections.addAll(list, elements);
        return list;
    }

    static int computeListCapacity(int arraySize)
    {
        return (int) Math.min(5L + arraySize + (arraySize / 10), Integer.MAX_VALUE);
    }

    /**
     * @param col
     *            The collection to check.
     * @return True if list is null or list size is equal to zero.
     */
    public static boolean isEmpty(Collection<?> col)
    {
        return col == null || col.isEmpty();
    }

    /**
     * @param col
     *            The collection to check.
     * @return False if list is null or list size is equal to zero.
     */
    public static boolean isNotEmpty(Collection<?> col)
    {
        return !isEmpty(col);
    }

    /**
     * @param list
     *            The list of which first element is requested.
     * @return The first element of <i>list</i>. It returns null if collection is null or empty.
     */
    public static Object getFirst(List<?> list)
    {
        if (CollectionUtils.isNotEmpty(list))
            return list.get(0);
        else
            return null;
    }

    /**
     * @param arrayString
     *            The string to parse.
     * @return Parse string of array. String format is like toString method render of array list.
     */
    public static List<String> parseStringArray(String arrayString)
    {
        List<String> values = new ArrayList<String>();
        if (arrayString.startsWith("[") && arrayString.length() > 2)
        {
            String tmpVal = arrayString.substring(1, arrayString.length() - 1);
            String[] valuesArray = tmpVal.split(", ");
            values.addAll(CollectionUtils.asList(valuesArray));
        }
        return values;
    }

    private static final int INDEXOFSUBLIST_THRESHOLD = 35;

    /**
     * @return true if the specified arguments are equals, or both null.
     */
    private static boolean eq(Object o1, Object o2)
    {
        return (o1 == null ? o2 == null : o1.equals(o2));
    }

    public static int indexOfSubList(List<?> source, List<?> target)
    {
        int sourceSize = source.size();
        int targetSize = target.size();
        int maxCandidate = sourceSize - targetSize;

        if (sourceSize < INDEXOFSUBLIST_THRESHOLD || (source instanceof RandomAccess && target instanceof RandomAccess))
        {
            nextCand: for (int candidate = 0; candidate <= maxCandidate; candidate++)
            {
                for (int i = 0, j = candidate; i < targetSize; i++, j++)
                    if (!eq(target.get(i), source.get(j)))
                        continue nextCand; // Element mismatch, try next cand
                return candidate; // All elements of candidate matched target
            }
        }
        else
        { // Iterator version of above algorithm
            ListIterator<?> si = source.listIterator();
            nextCand: for (int candidate = 0; candidate <= maxCandidate; candidate++)
            {
                ListIterator<?> ti = target.listIterator();
                for (int i = 0; i < targetSize; i++)
                {
                    if (!eq(ti.next(), si.next()))
                    {
                        // Back up source iterator to next candidate
                        for (int j = 0; j < i; j++)
                            si.previous();
                        continue nextCand;
                    }
                }
                return candidate;
            }
        }
        return -1; // No candidate matched the target
    }

    /**
     * Returns whether a collection belongs to a reference or not. This uses Collections.indexOfSubList whose
     * implementation uses the "brute force" technique of scanning over the source list, looking for a match with the
     * target at each location in turn.
     * 
     * Basically, use this method to ensure that a collection is to be found contiguously in a reference.
     * 
     * @param collection
     *            Collection to be tested
     * @param reference
     *            Reference collection to which collection parameter should belong to
     * 
     * @return True if collection belongs to reference.
     */
    public static boolean isContiguous(List<PlayListEditModelData> collection, List<PlayListEditModelData> reference)
    {
        if (collection == null || collection.isEmpty() || reference == null || reference.isEmpty())
            throw new IllegalArgumentException();

        if (!reference.containsAll(collection))
            throw new IllegalArgumentException();

        return CollectionUtils.indexOfSubList(reference, collection) != -1;
    }

    /**
     * Moves collection sublist (from-to) with specific offset.
     */
    public static void moveSubList(List<?> collection, int from, int to, int offset)
    {
        if (collection == null || collection.isEmpty())
            return;

        if (offset > 0)
        {
            for (int i = to; i > from; --i)
                Collections.swap(collection, to, offset);
        }
        else
        {
            for (int i = from; i < to; ++i)
                Collections.swap(collection, to, offset);
        }
    }

    /**
     * @param <E>
     *            The type of list elements.
     * @param models
     *            The models from which a map based on their is returned.
     * @return A map to get quickly a model from its ID.
     */
    public static <E extends Hd3dModelData> FastLongMap<E> getMapFromModelDataList(List<E> models)
    {
        FastLongMap<E> modelMap = new FastLongMap<E>();
        for (E model : models)
        {
            if (model.getId() != null)
                modelMap.put(model.getId().toString(), model);
        }

        return modelMap;
    }

    /**
     * @param <E>
     *            The type of list elements.
     * @param models
     *            The list to convert.
     * @return The list of models converted as Hd3dModelData.
     */
    public static <E extends Hd3dModelData> List<RecordModelData> convertToRecord(List<E> models)
    {
        List<RecordModelData> records = new ArrayList<RecordModelData>();
        for (E element : models)
            records.add((RecordModelData) element);

        return records;
    }

    /**
     * @param <E>
     *            The type of list elements.
     * @param models
     *            the list from which IDs must be extracted.
     * @return IDs of the given model data list.
     */
    public static <E extends Hd3dModelData> List<Long> getIds(List<E> models)
    {
        List<Long> ids = new ArrayList<Long>();
        for (E model : models)
        {
            if (model.getId() != null)
                ids.add(model.getId());
        }

        return ids;
    }

    /**
     * @param <E>
     *            The type of list elements.
     * @param models
     *            the list from which IDs must be extracted.
     * @return IDs as string of the given model data list.
     */
    public static <E extends Hd3dModelData> List<String> getStringIds(List<E> models)
    {
        List<String> ids = new ArrayList<String>();
        for (E model : models)
        {
            if (model.getId() != null)
                ids.add(model.getId().toString());
        }

        return ids;
    }
}
