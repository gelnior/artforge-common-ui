package fr.hd3d.common.ui.client.util;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.NameModelData;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.ThumbnailRenderer;


/**
 * GridUtils provides helpful method to create grids faster.
 * 
 * @author HD3D
 */
public class GridUtils
{

    /**
     * Add a new column to a column configuration list (width is set to 100).
     * 
     * @param columns
     *            The list in which column will be added.
     * @param id
     *            Id of the column to add.
     * @param name
     *            Name of the column to add (header).
     * @return The added column configuration.
     */
    public static ColumnConfig addColumnConfig(List<ColumnConfig> columns, String id, String name)
    {
        return addColumnConfig(columns, id, name, 100);
    }

    /**
     * Add a new column to a column configuration list.
     * 
     * @param columns
     *            The list in which column will be added.
     * @param id
     *            Id of the column to add.
     * @param name
     *            Name of the column to add (header).
     * @param width
     *            Column width.
     * @return The added column configuration.
     */
    public static ColumnConfig addColumnConfig(List<ColumnConfig> columns, String id, String name, int width)
    {
        ColumnConfig column = new ColumnConfig();
        column.setId(id);
        column.setHeader(name);
        column.setWidth(width);
        columns.add(column);

        return column;
    }

    /**
     * Add a new column to a column configuration list.
     * 
     * @param columns
     *            The list in which column will be added.
     * @param id
     *            Id of the column to add.
     * @param name
     *            Name of the column to add (header).
     * @param width
     *            Column width.
     * @param resizable
     *            True if column can be resize.
     * @return The added column configuration.
     */
    public static ColumnConfig addColumnConfig(List<ColumnConfig> columns, String id, String name, int width,
            boolean resizable)
    {
        ColumnConfig column = addColumnConfig(columns, id, name, width);
        column.setResizable(resizable);

        return column;
    }

    /**
     * Insert a new column in a column configuration list.
     * 
     * @param columns
     *            The list in which column will be added.
     * @param id
     *            Id of the column to add.
     * @param name
     *            Name of the column to add (header).
     * @param width
     *            Column width.
     * @return The added column configuration.
     */
    public static ColumnConfig addColumnConfig(int index, List<ColumnConfig> columns, String id, String name, int width)
    {
        ColumnConfig column = new ColumnConfig();
        column.setId(id);
        column.setHeader(name);
        column.setWidth(width);
        columns.add(index, column);

        return column;
    }

    /**
     * @return A column model with a name column already set.
     */
    public static ColumnModel getNameColumnModel()
    {
        List<ColumnConfig> columnConfigs = new ArrayList<ColumnConfig>();
        ColumnConfig nameCC = new ColumnConfig();
        nameCC.setId(NameModelData.NAME_FIELD);
        nameCC.setDataIndex(NameModelData.NAME_FIELD);
        columnConfigs.add(nameCC);

        return new ColumnModel(columnConfigs);
    }

    /**
     * Add a thumbnail column to given column list.
     * 
     * @param columns
     *            The list in which a column should be added.
     */
    public static void addThumbnailColumn(List<ColumnConfig> columns)
    {
        ColumnConfig thumbnail = GridUtils.addColumnConfig(columns, "thumbnail", "Thumbnail", 80);
        thumbnail.setRenderer(new ThumbnailRenderer<Hd3dModelData>());
    }

}
