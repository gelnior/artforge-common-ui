package fr.hd3d.common.ui.client.util;

import com.extjs.gxt.ui.client.widget.Component;


/**
 * Useful constants and methods to easily change widget CSS properties.
 * 
 * @author HD3D
 */
public class CSSUtils
{
    public static final String HIDDEN_CLASS = "hidden";

    public static final String MODIFIED_STYLE = "x-grid3-dirty-cell";
    public static final String DIRTY_MARKER = "x-grid3-dirty-cell";
    public static final String FLAT_NAME_STYLE = "flat-button";

    public static final String BACKGROUND_COLOR = "background-color";
    public static final String BORDER = "border";
    public static final String BORDER_RIGHT = "border-right";
    public static final String BORDER_TOP = "border-top";
    public static final String BORDER_BOTTOM = "border-bottom";
    public static final String CURSOR = "cursor";
    public static final String COLOR = "color";

    public static final String COLOR_WHITE = "#FFFFFF";
    public static final String COLOR_BLACK = "black";
    public static final String COLOR_GREEN = "green";
    public static final String COLOR_RED = "red";
    public static final String COLOR_ORANGE = "orange";

    public static final String FONT_WEIGHT = "font-weight";
    public static final String FONT_BOLD = "bold";

    public static final String CURSOR_POINTER = "pointer";
    public static final String POSITION_ABSOLUTE = "absolute";
    public static final String POSITION_RELATIVE = "relative";
    public static final String PADDING = "padding";
    public static final String NONE = "none";

    public static final String FONT_SIZE = "font-size";
    public static final String Z_INDEX = "z-index";
    public static final String LEFT = "left";
    public static final String TOP = "top";
    public static final String CENTER = "center";
    public static final String FLOAT = "float";
    public static final String SIZE = "size";
    public static final String OPACITY = "opacity";
    public static final String MOZ_OPACITY = "-moz-opacity";
    public static final String HIDDEN = "hidden";
    public static final String OVERFLOW = "overflow";
    public static final String TEXT_ALIGN = "text-align";

    /**
     * Change <i>component</i> background color.
     * 
     * @param component
     *            The component of which background should be changed.
     * @param bgColor
     *            The color to set.
     */
    public static void setBackground(Component component, String bgColor)
    {
        component.setStyleAttribute(CSSUtils.BACKGROUND_COLOR, bgColor);
    }

    /**
     * Sets component border to zero.
     * 
     * @param component
     *            The component of which borders are removed.
     */
    public static void setNoBorder(Component component)
    {
        component.setStyleAttribute(CSSUtils.BORDER, "none");
    }

    /**
     * Change <i>component</i> border color. If no border exists, it add it.
     * 
     * @param component
     *            The component of which border color should be changed.
     * @param bgColor
     *            The color to set.
     */
    public static void set1pxBorder(Component component, String bgColor)
    {
        component.setStyleAttribute(CSSUtils.BORDER, "1px solid " + bgColor);
    }

    /**
     * Change <i>component</i> right border color. If no border exists, it add it.
     * 
     * @param component
     *            The component of which border color should be changed.
     * @param bgColor
     *            The color to set.
     */
    public static void set1pxBorderRight(Component component, String bgColor)
    {
        component.setStyleAttribute(CSSUtils.BORDER_RIGHT, "1px solid " + bgColor);
    }

    /**
     * Change <i>component</i> top border color. If no border exists, it add it.
     * 
     * @param component
     *            The component of which border color should be changed.
     * @param bgColor
     *            The color to set.
     */
    public static void set1pxBorderTop(Component component, String bgColor)
    {
        component.setStyleAttribute(CSSUtils.BORDER_TOP, "1px solid " + bgColor);
    }

    /**
     * Change <i>component</i> bottom border color. If no border exists, it add it.
     * 
     * @param component
     *            The component of which border color should be changed.
     * @param bgColor
     *            The color to set.
     */
    public static void set1pxBorderBottom(Component component, String bgColor)
    {
        component.setStyleAttribute(CSSUtils.BORDER_BOTTOM, "1px solid " + bgColor);
    }

    /**
     * Change <i>component</i> border color. If no border exists, it add it (2px).
     * 
     * @param component
     *            The component of which border color should be changed.
     * @param bgColor
     *            The color to set.
     */
    public static void set2pxBorder(Component component, String bgColor)
    {
        component.setStyleAttribute(CSSUtils.BORDER, "2px solid " + bgColor);
    }

    /**
     * Change <i>component</i> border color. If no border exists, it adds it (3px).
     * 
     * @param component
     *            The component of which border color should be changed.
     * @param bgColor
     *            The color to set.
     */
    public static void set3pxBorder(Component component, String bgColor)
    {
        component.setStyleAttribute(CSSUtils.BORDER, "3px solid " + bgColor);
    }

    /**
     * Change <i>component</i> bottom border color. If no border exists, it add it.
     * 
     * @param component
     *            The component of which border color should be changed.
     * @param bgColor
     *            The color to set.
     */
    public static void set2pxBorderBottom(Component component, String bgColor)
    {
        component.setStyleAttribute(CSSUtils.BORDER_BOTTOM, "2px solid " + bgColor);
    }

    /**
     * Change <i>component</i> top border color. If no border exists, it add it.
     * 
     * @param component
     *            The component of which border color should be changed.
     * @param bgColor
     *            The color to set.
     */
    public static void set2pxBorderTop(Component component, String bgColor)
    {
        component.setStyleAttribute(CSSUtils.BORDER_TOP, "2px solid " + bgColor);

    }

    /**
     * Set <i>component</i> border to none.
     * 
     * @param component
     *            The component of which borders should be removed.
     */
    public static void setNoneBorder(Component component)
    {
        component.setStyleAttribute(CSSUtils.BORDER, CSSUtils.NONE);
    }

    /**
     * Change <i>component</i> cursor.
     * 
     * @param component
     *            The component of which cursor should be changed.
     * @param cursor
     *            The cursor to set (CSS value expected).
     */
    public static void setCursor(Component component, String cursor)
    {
        component.setStyleAttribute(CURSOR, cursor);
    }

    /**
     * Change <i>component</i> CSS position.
     * 
     * @param component
     *            The component of which position behavior should be changed.
     * @param weight
     *            The weight font to set (CSS value expected).
     */
    public static void setFontWeight(Component component, String weight)
    {
        component.setStyleAttribute(FONT_WEIGHT, weight);
    }

    /**
     * Change <i>component</i> font size.
     * 
     * @param component
     *            The component of which font size should be changed.
     * @param size
     *            The font size to set.
     */
    public static void setFontSize(Component component, String size)
    {
        component.setStyleAttribute(FONT_SIZE, size);
    }

    /**
     * Change <i>component</i> CSS position.
     * 
     * @param component
     *            The component of which position behavior should be changed.
     * @param position
     *            The position behavior to set (CSS value expected).
     */
    public static void setPosition(Component component, String position)
    {
        component.setStyleAttribute("position", position);
    }

    /**
     * Change <i>component</i> padding.
     * 
     * @param component
     *            The component of which padding should be changed.
     * @param padding
     *            The padding to set.
     */
    public static void setPadding(Component component, String padding)
    {
        component.setStyleAttribute(PADDING, padding);
    }

    /**
     * Change <i>component</i> float mode.
     * 
     * @param component
     *            The component of which float mode should be changed.
     * @param floatValue
     *            The float mode to set.
     */
    public static void setFloat(Component component, String floatValue)
    {
        component.setStyleAttribute(FLOAT, floatValue);
    }

    /**
     * Change <i>component</i> float mode.
     * 
     * @param component
     *            The component of which float mode should be changed.
     * @param opacity
     *            The opacity value to set.
     */
    public static void setOpacity(Component component, String opacity)
    {
        component.setStyleAttribute(OPACITY, opacity);
        component.setStyleAttribute(MOZ_OPACITY, opacity);
    }

    /**
     * Change <i>component</i> left coordinate.
     * 
     * @param component
     *            The component of which left coordinate should be changed.
     * @param left
     *            The left coordinate to set.
     */
    public static void setLeft(Component component, String left)
    {
        component.setStyleAttribute(LEFT, left);
    }

    /**
     * Change <i>component</i> top coordinate.
     * 
     * @param component
     *            The component of which top coordinate should be changed.
     * @param top
     *            The top coordinate to set.
     */
    public static void setTop(Component component, String top)
    {
        component.setStyleAttribute(TOP, top);
    }

    /**
     * Change <i>component</i> z index.
     * 
     * @param component
     *            The component of which z index will be changed.
     * @param zIndex
     *            The z index to set.
     */
    public static void setZindex(Component component, Integer zIndex)
    {
        component.setStyleAttribute(Z_INDEX, zIndex.toString());
    }

    /**
     * Change <i>component</i> overflow.
     * 
     * @param component
     *            The component of which overflow will be changed.
     * @param overflow
     *            The overflow value to set.
     */
    public static void setOverflow(Component component, String overflow)
    {
        component.setStyleAttribute(OVERFLOW, overflow);
    }

    /**
     * Change <i>component</i> text-align.
     * 
     * @param component
     *            The component of which text-align will be changed.
     * @param overflow
     *            The text-align value to set.
     */
    public static void setTextAlign(Component component, String alignment)
    {
        component.setStyleAttribute(TEXT_ALIGN, alignment);
    }

}
