package fr.hd3d.common.ui.client.util;

/**
 * Useful constants and methods for mouse behavior handling.
 * 
 * @author HD3D
 */
public class MouseUtils
{
    public static final String DEFAULT = "default";

    public static String CURSOR_ATTRIBUTE = "cursor";
    public static String MOZ_GRAB = "-moz-grab";
    public static String MOZ_GRABBING = "-moz-grabbing";
}
