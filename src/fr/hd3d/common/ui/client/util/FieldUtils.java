package fr.hd3d.common.ui.client.util;

import java.util.Date;
import java.util.Map;

import com.extjs.gxt.ui.client.Style.SortDir;
import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.util.Util;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.extjs.gxt.ui.client.widget.form.DateField;
import com.extjs.gxt.ui.client.widget.form.Field;
import com.extjs.gxt.ui.client.widget.form.NumberField;
import com.extjs.gxt.ui.client.widget.form.TextField;

import fr.hd3d.common.client.Const;
import fr.hd3d.common.client.Sheet;
import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.calendar.DateFormat;
import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.modeldata.reader.RecordReader;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.modeldata.technical.TagModelData;
import fr.hd3d.common.ui.client.service.ServicesClass;
import fr.hd3d.common.ui.client.service.ServicesField;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.widget.AutoCompleteModelCombo;
import fr.hd3d.common.ui.client.widget.AutoMultiModelCombo;
import fr.hd3d.common.ui.client.widget.AutoMultiPersonComboBox;
import fr.hd3d.common.ui.client.widget.BooleanComboBox;
import fr.hd3d.common.ui.client.widget.FieldComboBox;
import fr.hd3d.common.ui.client.widget.ModelDataComboBox;
import fr.hd3d.common.ui.client.widget.MultiFieldComboBox;
import fr.hd3d.common.ui.client.widget.PersonComboBox;
import fr.hd3d.common.ui.client.widget.explorer.model.modeldata.IColumn;


/**
 * FieldUtils provides a set of methods to facilitate field manipulation, notably for modular widget like constraint
 * panel or identity sheet.
 * 
 * @author HD3D
 */
public class FieldUtils
{
    /** ID suffix is needed to make filter on object field like worker field in task object. */
    public static final String ID_SUFFIX = ".id";
    /** Java prefix used to recognize java base class. */
    public static final String JAVA_PREFIX = "java";
    /** HTML corresponds to the blank HTML representation. */
    public static final String HTML_BLANK = "&nbsp;";
    /** The null toString value. */
    public static final String NULL_STRING = "null";

    /**
     * @param type
     *            The java type of the object to convert.
     * @param value
     *            The value to convert.
     * @return From a Long, Integer, Float, Double object returns a Number object.
     */
    public static Object getNumber(String type, Object value)
    {
        if (Const.JAVA_LANG_DOUBLE.equals(type) || Const.JAVA_LANG_FLOAT.equals(type))
        {
            return ((Number) value).doubleValue();
        }
        else
        {
            return ((Number) value).longValue();
        }
    }

    /**
     * Set the value of a field (of any type) with a value given as a String. Type of the value is given too.
     * 
     * @param valueField
     *            The field to update.
     * @param type
     *            The type of value to set in the field.
     * @param stringValue
     *            The value as a String.
     */
    @SuppressWarnings("unchecked")
    public static void updateValue(Field<?> valueField, String type, String stringValue)
    {
        if (valueField instanceof FieldComboBox)
        {
            ((FieldComboBox) valueField).setValueByStringValue(stringValue);
        }
        else if (valueField instanceof AutoMultiModelCombo<?>)
        {
            ((AutoMultiModelCombo<?>) valueField).setValueByStringValue(stringValue);
        }
        else if (valueField instanceof MultiFieldComboBox)
        {
            ((MultiFieldComboBox) valueField).setValuesByStringValue(stringValue);
        }
        else if (valueField instanceof ModelDataComboBox<?>)
        {
            try
            {
                ((ModelDataComboBox<?>) valueField).setValueById(Long.parseLong(stringValue));
            }
            catch (Exception e)
            {
                Logger.warn("Wrong value for a model data constraint. Long expected to select model data on its ID.");
            }
        }
        else if (isString(type))
            ((TextField<String>) valueField).setValue(stringValue);
        else if (isDouble(type))
            ((TextField<Double>) valueField).setValue(Double.parseDouble(stringValue));
        else if (isFloat(type))
            ((TextField<Float>) valueField).setValue(Float.parseFloat(stringValue));
        else if (isInteger(type))
            ((TextField<Integer>) valueField).setValue(Integer.parseInt(stringValue));
        else if (isLong(type))
        {
            if (stringValue.contains("."))
            {
                String[] tmp = stringValue.split("\\.");
                stringValue = tmp[0];
            }
            ((TextField<Long>) valueField).setValue(Long.parseLong(stringValue));
        }
        else if (isDate(type))
            ((DateField) valueField).setValue(DateFormat.DATE_TIME.parse(stringValue));
        else if (isBoolean(type))
        {
            Boolean bool = Boolean.parseBoolean(stringValue);

            if (bool)
            {
                ((BooleanComboBox) valueField).setValue(BooleanComboBox.TRUE);
            }
            else
            {
                ((BooleanComboBox) valueField).setValue(BooleanComboBox.FALSE);
            }
        }
    }

    /**
     * Set the value of a field (of any type) with a value given as an Object. Type of the value is given too.
     * 
     * @param field
     *            The field to update.
     * @param type
     *            The type of value to set in the field.
     * @param val
     *            The value as a Object.
     */
    @SuppressWarnings("unchecked")
    public static void updateFieldValue(Field<?> field, String type, Object val)
    {
        if (isString(type))
            ((TextField<String>) field).setValue((String) val);
        else if (isDouble(type))
            ((TextField<Double>) field).setValue((Double) val);
        else if (isFloat(type))
            ((TextField<Float>) field).setValue((Float) val);
        else if (isInteger(type))
            ((TextField<Integer>) field).setValue((Integer) val);
        else if (isLong(type))
        {
            if (field instanceof ModelDataComboBox<?>)
                ((ModelDataComboBox<?>) field).setValueById((Long) val);
            else
                ((TextField<Long>) field).setValue((Long) val);
        }
        else if (isDate(type))
            ((DateField) field).setValue((Date) val);
        else if (field instanceof ModelDataComboBox<?>)
            ((ModelDataComboBox<?>) field).setValueById((Long) val);
        else if (isBoolean(type))
            ((CheckBox) field).setValue((Boolean) val);
        else if (isEnum(type))
            ((FieldComboBox) field).setValueByStringValue((String) val);
        else if (isEntity(type))
            ((ModelDataComboBox<?>) field).setValueById((Long) val);
    }

    /**
     * @param type
     *            The type of which field is requested.
     * @return From a java type or an Entity name returns the right field to edit the value of this type.
     */
    public static Field<?> getField(String type)
    {
        return getField(type, null);
    }

    /**
     * @param type
     *            The type of which field is requested.
     * @param operator
     *            The operator used to compare the value of this field.
     * @return From a java type or an Entity name returns the right field to edit the value of this type. If operator is
     *         not null and equal to "in".
     */
    public static Field<?> getField(String type, EConstraintOperator operator)
    {
        if (isString(type))
        {
            return new TextField<String>();
        }
        else if (isDouble(type))
        {
            NumberField field = new NumberField();
            field.setAllowDecimals(true);
            return field;
        }
        else if (isFloat(type))
        {
            NumberField field = new NumberField();
            field.setAllowDecimals(true);
            return field;
        }
        else if (isInteger(type))
        {
            NumberField field = new NumberField();
            field.setAllowDecimals(false);
            return field;
        }
        else if (isLong(type))
        {
            NumberField field = new NumberField();
            field.setAllowDecimals(false);
            return field;
        }
        else if (isBoolean(type))
        {
            return new BooleanComboBox();
        }
        else if (isDate(type))
        {
            return new DateField();
        }
        else if (isEntity(type))
        {
            return getEntityField(type, operator);
        }
        return null;
    }

    /**
     * @param type
     *            Entity for which field is requested.
     * @param operator
     *            The operator for which field is requested.
     * @return An autocomplete model data combo box if operator is different of "in", else it return multi valu
     *         autocomplete model data combo box. An exception is set if entity is a person, it returns same kind of
     *         combo box, but this time time with additional features to display correctly the name of selected persons.
     */
    private static Field<?> getEntityField(String type, EConstraintOperator operator)
    {

        if (PersonModelData.SIMPLE_CLASS_NAME.equalsIgnoreCase(type))
        {
            if (EConstraintOperator.in == operator)
            {
                AutoMultiPersonComboBox combo = new AutoMultiPersonComboBox();
                combo.setPath(ServicesPath.getPath(type));
                return combo;
            }
            else
            {
                PersonComboBox combo = new PersonComboBox();
                combo.setPath(ServicesPath.getPath(type));
                return combo;
            }
        }
        else
        {
            if (EConstraintOperator.in == operator)
            {
                AutoMultiModelCombo<RecordModelData> combo = new AutoMultiModelCombo<RecordModelData>(
                        new RecordReader());
                combo.setPath(ServicesPath.getPath(type));
                return combo;
            }
            else
            {
                AutoCompleteModelCombo<RecordModelData> combo = new AutoCompleteModelCombo<RecordModelData>(
                        new RecordReader());
                combo.setPath(ServicesPath.getPath(type));
                return combo;
            }
        }
    }

    /**
     * @param type
     *            the type to check.
     * @return True if type is a native type (number types, string, date or boolean).
     */
    public static boolean isNativeField(String type)
    {
        return isString(type) || isNumber(type) || isDate(type) || isBoolean(type);
    }

    /**
     * @param type
     *            the type to check.
     * @return True if type is a number type : double, float, long, integer.
     */
    public static boolean isNumber(String type)
    {
        return Const.JAVA_LANG_DOUBLE.equals(type) || Const.JAVA_LANG_FLOAT.equals(type)
                || Const.JAVA_LANG_LONG.equals(type) || Const.JAVA_LANG_INTEGER.equals(type);
    }

    /**
     * @param type
     *            the type to check.
     * @return True if type is a decimal number type : double, float.
     */
    public static boolean isDecimalNumber(String type)
    {
        return isDouble(type) || isFloat(type);
    }

    /**
     * @param type
     *            the type to check.
     * @return True if type is a number type : double, long, float integer.
     */
    public static boolean isIntNumber(String type)
    {
        return isInteger(type) || isLong(type) || isByte(type);
    }

    /**
     * @param type
     *            the type to check.
     * @return True if type is java.lang.Byte.
     */
    private static boolean isByte(String type)
    {
        return Const.JAVA_LANG_BYTE.equals(type);
    }

    /**
     * @param type
     *            the type to check.
     * @return True if type is java.lang.Double.
     */
    public static boolean isDouble(String type)
    {
        return Const.JAVA_LANG_DOUBLE.equals(type);
    }

    /**
     * @param type
     *            the type to check.
     * @return True if type is java.lang.Long.
     */
    public static boolean isLong(String type)
    {
        return Const.JAVA_LANG_LONG.equals(type);
    }

    /**
     * @param type
     *            the type to check.
     * @return True if type is java.lang.Float.
     */
    public static boolean isFloat(String type)
    {
        return Const.JAVA_LANG_FLOAT.equals(type);
    }

    /**
     * @param type
     *            the type to check.
     * @return True if type is java.lang.Integer.
     */
    public static boolean isInteger(String type)
    {
        return Const.JAVA_LANG_INTEGER.equals(type);
    }

    /**
     * @param type
     *            the type to check.
     * @return True if type is java.lang.Boolean.
     */
    public static boolean isBoolean(String type)
    {
        return Const.JAVA_LANG_BOOLEAN.equals(type);
    }

    /**
     * @param type
     *            the type to check.
     * @return True if type is java.lang.String.
     */
    public static boolean isString(String type)
    {
        return Const.JAVA_LANG_STRING.equals(type);
    }

    /**
     * @param type
     *            the type to check.
     * @return True if type is java.util.Date.
     */
    public static boolean isDate(String type)
    {
        return Const.JAVA_UTIL_DATE.equals(type);
    }

    /**
     * @param type
     *            the type to check.
     * @return True if type is java.util.List.
     */
    public static boolean isList(String type)
    {
        return Const.JAVA_UTIL_LIST.equals(type);
    }

    /**
     * @param type
     *            the type to check.
     * @return True if type is equal to of the entity defined in server model (Shot, Constituent, Person...).
     */
    public static boolean isEntity(String type)
    {
        return ServicesClass.contains(type);
    }

    /**
     * @param type
     *            the type to check.
     * @return True if type is Tag.
     */
    public static boolean isTag(String type)
    {
        return TagModelData.SIMPLE_CLASS_NAME.equals(type);
    }

    /**
     * @param type
     *            the type to check.
     * @return True if type is "attribute".
     */
    public static boolean isStaticAttribute(String type)
    {
        return Sheet.ITEMTYPE_ATTRIBUTE.equals(type);
    }

    /**
     * @param type
     *            the type to check.
     * @return True if type is "metaData".
     */
    public static boolean isDynAttribute(String type)
    {
        return Sheet.ITEMTYPE_METADATA.equals(type);
    }

    /**
     * @param type
     *            the type to check.
     * @return True if type is "java.util.List<fr.hd3d.model.persistence.IApprovalNote>" or "ApprovalNote".
     */
    public static boolean isApproval(String type)
    {
        return Const.LIST_OF_APPROVALS.equals(type) || ApprovalNoteModelData.SIMPLE_CLASS_NAME.equals(type);
    }

    /**
     * @param type
     *            the type to check.
     * @return True if type is "java.util.List"
     */
    public static boolean isLIst(String type)
    {
        return Const.JAVA_UTIL_LIST.equals(type);
    }

    /**
     * @param type
     *            the type to check.
     * @return True if type is "Person".
     */
    public static boolean isPerson(String type)
    {
        return type != null && PersonModelData.SIMPLE_CLASS_NAME.toLowerCase().equals(type.toLowerCase());
    }

    /**
     * Clean method to test that a type name is equal to "Person".
     * 
     * @param type
     *            Type to analyze
     * @return True if type corresponds to Person.
     */
    public static boolean isEntityPerson(String type)
    {
        return PersonModelData.SIMPLE_CLASS_NAME.equals(type);
    }

    /**
     * @param record
     *            Record to analyze
     * @return True if record class name is equals to Project full class name.
     */
    public static boolean isProject(Hd3dModelData record)
    {
        return ProjectModelData.CLASS_NAME.equals(record.getClassName());
    }

    /**
     * @param record
     *            Record to analyze
     * @return True if record simple class name is equals to "Person".
     */
    public static boolean isPerson(Hd3dModelData record)
    {
        return isPerson(record.getSimpleClassName());
    }

    /**
     * @param record
     *            Record to analyze
     * @return True if record class name is equals to Constituent full class name.
     */
    public static boolean isConstituent(Hd3dModelData record)
    {
        return ConstituentModelData.CLASS_NAME.equals(record.getClassName());
    }

    /**
     * @param record
     *            Record to analyze
     * @return True if record class name is equals to Shot full class name.
     */
    public static boolean isShot(Hd3dModelData record)
    {
        return ShotModelData.CLASS_NAME.equals(record.getClassName());
    }

    /**
     * @param record
     *            Record to analyze
     * @return True if record class name is equals to Sequence full class name.
     */
    public static boolean isSequence(Hd3dModelData record)
    {
        return SequenceModelData.CLASS_NAME.equals(record.getClassName());
    }

    /**
     * @param record
     *            Record to analyze
     * @return True if record class name is equals to Shot full class name or Constituent full class name.
     */
    public static boolean isWorkObject(Hd3dModelData record)
    {
        return isConstituent(record) || isShot(record);
    }

    /**
     * @param type
     *            The type to check.
     * @return True if type is equal to "Category".
     */
    public static boolean isConstituent(String type)
    {
        if (type != null)
            return ConstituentModelData.SIMPLE_CLASS_NAME.toLowerCase().equals(type.toLowerCase());
        else
            return false;
    }

    /**
     * @param type
     *            The type to check.
     * @return True if type is equal to "Category".
     */
    public static boolean isCategory(String type)
    {
        return CategoryModelData.SIMPLE_CLASS_NAME.equals(type);
    }

    /**
     * @param record
     *            The model to check.
     * @return True if model class name is equal to Category class.
     */
    public static boolean isCategory(RecordModelData record)
    {
        return CategoryModelData.CLASS_NAME.equals(record.getClassName());
    }

    /**
     * @param record
     *            The model to check.
     * @return True if model simple class name is equal to Resource group simple class name.
     */
    public static boolean isResourceGroup(Hd3dModelData record)
    {
        return isResourceGroup(record.getSimpleClassName());
    }

    /**
     * @param type
     *            The type to check.
     * @return True if type is equal to "ResourceGroup".
     */
    public static boolean isResourceGroup(String type)
    {
        if (type != null)
            return ResourceGroupModelData.SIMPLE_CLASS_NAME.toLowerCase().equals(type.toLowerCase());
        else
            return false;
    }

    /**
     * @param type
     *            The type to check.
     * @return True if type is equal to "Shot".
     */
    public static boolean isShot(String type)
    {
        if (type != null)
            return ShotModelData.SIMPLE_CLASS_NAME.toLowerCase().equals(type.toLowerCase());
        else
            return false;
    }

    /**
     * @param type
     *            The type to check.
     * @return True if type is equal to "Sequence".
     */
    public static boolean isSequence(String type)
    {
        return SequenceModelData.SIMPLE_CLASS_NAME.equals(type);
    }

    /**
     * @param query
     *            The query to test.
     * @return True if query corresponds to step collection query.
     */
    public static boolean isStep(String query)
    {
        return "fr.hd3d.services.resources.collectionquery.StepsByTaskTypeCollectionQuery".equals(query);
    }

    /**
     * @param query
     *            The query to test.
     * @return True if query corresponds to approval collection query.
     */
    public static boolean isApprovalColumn(String query)
    {
        return "fr.hd3d.services.resources.collectionquery.ApprovalNotesCollectionQuery".equals(query);
    }

    /**
     * @param type
     *            The type to test.
     * @return True if type corresponds to a java time stamp type.
     */
    public static boolean isTimeStamp(String type)
    {
        return Const.JAVA_SQL_TIMESTAMP.equals(type);
    }

    /**
     * @param type
     *            The type to test.
     * @return True if type corresponds to an enumeration (begin with an E).
     */
    public static boolean isEnum(String type)
    {
        if (type != null)
        {
            return type.startsWith("fr.hd3d.common.client.enums.E") || "ETaskStatus".equals(type)
                    || Const.JAVA_LANG_ENUM.equals(type);
        }
        else
        {
            return false;
        }
    }

    /**
     * Method for adding field model data based on given name and value to a FieldModelData map. The field model data is
     * the value and <i>value</i> is the key.
     * 
     * @param map
     *            The map in which the couple field model data <-> value is added.
     * @param id
     *            The id of the field model data (often not important).
     * @param name
     *            The displayed name of the field model data.
     * @param value
     *            The value to set inside the field model data (the key of the couple).
     */
    public static void addFieldToMap(FastMap<FieldModelData> map, int id, String name, Object value)
    {
        FieldModelData field = new FieldModelData(id, name, value);
        map.put(value.toString(), field);
    }

    /**
     * @param map
     *            The map from which the combo box will be filled.
     * @return A new field combobox where value list correspond to map values. Map contains key associated to field
     *         model data object. FieldModelData objects are composed of display value and real value. It is useful t
     */
    public static FieldComboBox getComboFromMap(Map<String, FieldModelData> map)
    {
        FieldComboBox combo = new FieldComboBox();
        for (Map.Entry<String, FieldModelData> entry : map.entrySet())
        {
            combo.getStore().add(entry.getValue());
        }
        combo.getStore().sort(FieldModelData.NAME_FIELD, SortDir.ASC);

        return combo;
    }

    /**
     * @param column
     *            The column on which the name should be translated.
     * @return Translation of <i>column</i> name.
     */
    public static String translateColumnName(IColumn column)
    {
        String columnName = column.getName();
        if (ServicesField.getHumanName(column.getDataIndex()) != null)
        {
            columnName = ServicesField.getHumanName(column.getDataIndex());
        }
        return columnName;
    }

    /**
     * @param fieldName
     *            The field name.
     * 
     * @return New Long data field created form field name for model type building.
     */
    @Deprecated
    public static DataField newLongDataField(String fieldName)
    {
        DataField dataField = new DataField(fieldName);
        dataField.setType(Long.class);
        return dataField;
    }

    /**
     * @param fieldName
     *            The field name.
     * 
     * @return New Integer data field created form field name for model type building.
     */
    @Deprecated
    public static DataField newIntegerDataField(String fieldName)
    {
        DataField dataField = new DataField(fieldName);
        dataField.setType(Integer.class);
        return dataField;
    }

    /**
     * @param fieldName
     *            The field name.
     * 
     * @return New Date data field created form field name for model type building.
     */
    @Deprecated
    public static DataField newDateDataField(String fieldName)
    {
        DataField dataField = new DataField(fieldName);
        dataField.setType(Date.class);
        dataField.setFormat(fr.hd3d.common.client.DateFormat.TIMESTAMP_FORMAT_STRING);

        return dataField;
    }

    /**
     * Add dirty marker to a field (red triangle).
     * 
     * @param field
     *            The field on which marker will be added.
     */
    public static void setDirty(Field<?> field)
    {
        if (field instanceof CheckBox)
        {
            field.addStyleName(CSSUtils.DIRTY_MARKER);
        }
        else
        {
            field.addInputStyleName(CSSUtils.DIRTY_MARKER);
        }
    }

    /**
     * Remove dirty marker from a field (red triangle).
     * 
     * @param field
     *            The field from which marker will be removed.
     */
    public static void removeDirty(Field<?> field)
    {

        if (field instanceof CheckBox)
        {
            field.removeStyleName(CSSUtils.DIRTY_MARKER);
        }
        else
        {
            field.removeInputStyleName(CSSUtils.DIRTY_MARKER);
        }
    }

    /**
     * @param value
     *            The value to convert.
     * @param digit
     *            The amount of digit expected.
     * @return Convert an integer to string with the amount of digit specified. If number is too small for the digit
     *         amount, 0 are added until the digit amount is reached.
     */
    public static String getDigitNumber(int value, int digit)
    {
        String number = "" + value;
        if (number.length() < digit)
        {
            for (int i = 0; i <= digit - number.length(); i++)
            {
                number = "0" + number;
            }
        }
        return number;
    }

    /** @return True if value is superior to zero, false either. */
    public static Boolean doubleToBoolean(Double value)
    {
        return value > 0;
    }

    /**
     * @param model
     *            The model of which name is requested.
     * @return Name of the given model (extract if from name field or label field).
     */
    public static String getModelName(Hd3dModelData model)
    {
        String name = null;
        if (model.get(ConstituentModelData.CONSTITUENT_LABEL) != null)
        {
            name = model.get(ConstituentModelData.CONSTITUENT_LABEL);
        }
        else if (model.get(TaskModelData.NAME_FIELD) != null)
        {
            name = model.get(TaskModelData.NAME_FIELD);
        }
        return name;
    }

    /**
     * @param workObject
     *            The work object of which name is requested.
     * @return The value for name key in a work object. If value is null, it returns the for label key.
     */
    public static String getName(Hd3dModelData workObject)
    {
        String name = workObject.get(RecordModelData.NAME_FIELD);
        if (!Util.isEmptyString(name))
            return name;
        name = workObject.get("label");
        if (!Util.isEmptyString(name))
            return name;

        return null;
    }

    /**
     * @param workObject
     *            The work object of which path is requested.
     * @return The path of the work object whether it is a shot or a constituent.
     */
    public static String getWorkObjectPath(Hd3dModelData workObject)
    {
        String path = workObject.get(ConstituentModelData.CONSTITUENT_CATEGORIESNAMES);
        if (!Util.isEmptyString(path))
            return path;
        path = workObject.get(ShotModelData.SHOT_SEQUENCESNAMES);
        if (!Util.isEmptyString(path))
            return path;

        return null;
    }
}
