package fr.hd3d.common.ui.client.util;

import com.google.gwt.user.client.Element;


/**
 * Utility that disables browser context menu.
 * 
 * @author HD3D
 */
public class NativeUtil
{
    public native static void disableContextMenuInternal(Element elem, boolean disable) /*-{
                                                                                        if (disable) {
                                                                                        elem.oncontextmenu = function() {  return false};
                                                                                        } else {
                                                                                        elem.oncontextmenu = null;
                                                                                        }
                                                                                        }-*/;
}
