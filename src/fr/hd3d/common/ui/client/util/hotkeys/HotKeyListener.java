package fr.hd3d.common.ui.client.util.hotkeys;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Listener;

import fr.hd3d.common.ui.client.event.EventDispatcher;


/**
 * This class listens keyboard events. If a keyboard event matches defined hot keys, the associated event is dispatched
 * to controllers.
 * 
 * @author HD3D
 * 
 */
public class HotKeyListener implements Listener<ComponentEvent>
{

    /** Map containing the CTRL hot keys. */
    private final FastMap<EventType> mapCtrlKeyCode = new FastMap<EventType>();
    /** Map containing the SHIFT hot keys. */
    private final FastMap<EventType> mapShiftKeyCode = new FastMap<EventType>();
    /** Map containing the ALT hot keys. */
    private final FastMap<EventType> mapAltKeyCode = new FastMap<EventType>();

    /** Map containing the hot keys without special key. */
    private final FastMap<EventType> mapKeyCode = new FastMap<EventType>();

    public void handleEvent(ComponentEvent be)
    {
        boolean action = handleEvent(be.getKeyCode(), be.isControlKey(), be.isShiftKey(), be.isAltKey());
        if (action)
        {
            be.stopEvent();
        }
    }

    /**
     * Handle the event and forward the event according the keycode.
     * 
     * @param keyCode
     *            the key code.
     * @param isControl
     *            if control is pressed.
     * @param isShift
     *            if shift is pressed.
     * @param isAlt
     *            if alt is pressed.
     * @return if a event had forwarded.
     */
    public boolean handleEvent(int keyCode, boolean isControl, boolean isShift, boolean isAlt)
    {
        boolean action = false;
        if (isControl)
        {
            EventType event = mapCtrlKeyCode.get(Integer.valueOf(keyCode).toString());
            if (event != null)
            {
                action = true;
                EventDispatcher.forwardEvent(event);
            }
        }
        else if (isAlt)
        {
            EventType event = mapAltKeyCode.get(Integer.valueOf(keyCode).toString());
            if (event != null)
            {
                action = true;
                EventDispatcher.forwardEvent(event);
            }
        }
        else if (isShift)
        {
            EventType event = mapShiftKeyCode.get(Integer.valueOf(keyCode).toString());
            if (event != null)
            {
                action = true;
                EventDispatcher.forwardEvent(event);
            }
        }
        else
        {
            EventType event = mapKeyCode.get(Integer.valueOf(keyCode).toString());
            if (event != null)
            {
                action = true;
                EventDispatcher.forwardEvent(event);
            }
        }
        return action;
    }

    /**
     * Set a CTRL hot key to listener.
     * 
     * @param keyCode
     *            the key code of the hot key
     * @param event
     *            the event to forward.
     */
    public void setCtrlHotKey(Integer keyCode, EventType event)
    {
        mapCtrlKeyCode.put(keyCode.toString(), event);
    }

    /**
     * Set a SHIFT hot key to listener.
     * 
     * @param keyCode
     *            the key code of the hot key
     * @param event
     *            the event to forward.
     */
    public void setShiftHotKey(Integer keyCode, EventType event)
    {
        mapShiftKeyCode.put(keyCode.toString(), event);
    }

    /**
     * Set a ALT hot key to listener.
     * 
     * @param keyCode
     *            the key code of the hot key
     * @param event
     *            the event to forward.
     */
    public void setAltHotKey(Integer keyCode, EventType event)
    {
        mapAltKeyCode.put(keyCode.toString(), event);
    }

    /**
     * Set a hot key without special key to listener.
     * 
     * @param keyCode
     *            the key code of the hot key
     * @param event
     *            the event to forward.
     */
    public void setHotKey(Integer keyCode, EventType event)
    {
        mapKeyCode.put(keyCode.toString(), event);
    }
}
