package fr.hd3d.common.ui.client.util.hotkeys;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.widget.Component;
import com.extjs.gxt.ui.client.widget.Document;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.google.gwt.event.dom.client.KeyCodes;

import fr.hd3d.common.ui.client.widget.FocusableComponent;


/**
 * Utils Class to add hot keys to a DOM application. The hot keys which could be use, are defined by static fields. It
 * uses CRTL key, SHIFT key and ALT key and more. Use the static method to add hot keys to a component. If the hot keys
 * will be pressed, it forwards a event.
 * 
 * @author HD3D
 * 
 */
public class HotKeysUtils
{
    private static class FocusPanelListener
    {
        private final Component panel;
        private final HotKeyListener listener;

        public FocusPanelListener(Component panel, HotKeyListener listener)
        {
            this.panel = panel;
            this.listener = listener;
        }

        public HotKeyListener getListener()
        {
            return listener;
        }

        public Component getPanel()
        {
            return panel;
        }
    }

    private static final int CTRL_TYPE = 1 << 24;
    private static final int SHIFT_TYPE = 1 << 25;
    private static final int ALT_TYPE = 1 << 26;

    public static final int CTRL_C = CTRL_TYPE + 67;
    public static final int CTRL_S = CTRL_TYPE + 83;
    public static final int CTRL_V = CTRL_TYPE + 86;

    public static final int ALT_C = ALT_TYPE + 67;

    public static final int SHIFT_C = SHIFT_TYPE + 67;

    public static final int SUPPR = KeyCodes.KEY_DELETE;

    /** Listener to hot keys to DOM application. */
    private static HotKeyListener hotKeyListener = null;

    /** Map containing the listener for each component with hotKeys. */
    private static FastMap<FocusPanelListener> listenerMap = new FastMap<FocusPanelListener>();

    /**
     * Add hot keys to the DOM application. If the hot keys will be pressed, it forwards the given event.
     * 
     * @param typeHotKey
     *            the type of hot key.
     * @param event
     *            the event forwards when the hot key are pressed.
     */
    public static void addHotKeys(int typeHotKey, EventType event)
    {
        if (hotKeyListener == null)
        {
            hotKeyListener = new HotKeyListener();
            Document.get().addListener(Events.OnKeyDown, hotKeyListener);
        }
        setHotKey(hotKeyListener, typeHotKey, event);
    }

    /**
     * Set hot keys to the hot keys listener.
     * 
     * @param listener
     *            the hot keys listener.
     * @param typeHotKey
     *            the type of hot key (static in the class).
     * @param event
     *            the event to send when the hot keys is typed.
     */
    private static void setHotKey(HotKeyListener listener, int typeHotKey, EventType event)
    {
        if (listener == null)
        {
            return;
        }

        if ((typeHotKey & CTRL_TYPE) != 0)
        {
            listener.setCtrlHotKey(typeHotKey & 0xFFFF, event);
        }
        else if ((typeHotKey & SHIFT_TYPE) != 0)
        {
            listener.setShiftHotKey(typeHotKey & 0xFFFF, event);
        }
        else if ((typeHotKey & ALT_TYPE) != 0)
        {
            listener.setAltHotKey(typeHotKey & 0xFFFF, event);
        }
        else
        {
            hotKeyListener.setHotKey(typeHotKey, event);
        }
    }

    /**
     * Add hot keys to the component. If the hot keys will be pressed, it forwards the given event. It creates a
     * focusable component. You must use this new component in your application.
     * 
     * @param panel
     *            the panel to set to hot keys.
     * @param typeHotKey
     *            the type of hot keys (static in the class).
     * @param event
     *            the event to send when the hot keys is typed.
     * @return the new component with hot keys.
     */
    public static Component addHotKeys(final Component panel, int typeHotKey, EventType event)
    {
        if (panel == null)
        {
            return null;
        }

        FocusPanelListener focusPanelListener = listenerMap.get(panel.getId());
        if (focusPanelListener == null)
        {
            final HotKeyListener listener = new HotKeyListener();

            FocusableComponent component = new FocusableComponent();
            component.add(panel);
            component.setLayout(new FitLayout());
            component.addListener(Events.OnKeyDown, listener);
            focusPanelListener = new FocusPanelListener(component, listener);
            listenerMap.put(panel.getId(), focusPanelListener);
        }
        setHotKey(focusPanelListener.getListener(), typeHotKey, event);
        return focusPanelListener.getPanel();
    }
}
