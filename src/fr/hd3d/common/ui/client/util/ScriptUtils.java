package fr.hd3d.common.ui.client.util;

import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.service.parameter.ParameterBuilder;
import fr.hd3d.common.ui.client.service.parameter.ScriptParams;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;


/**
 * ScriptUtils provides utility methods for calling reflex scripts. through services.
 * 
 * @author HD3D
 */
public class ScriptUtils
{
    /** Parameter name for update asset/task list script. */
    public static final String PROJECT_PARAMETER = "projectId";
    /** Parameter name for update asset/task list script. */
    public static final String ODS_PATH_PARAMETER = "odsPath";
    public static final String PERSON_PARAMETER = "personId";

    /**
     * Ask for server to launch specific script.
     * 
     * @param scriptName
     *            The name of script to launch.
     * @param params
     *            The script parameters.
     * @param callback
     *            The callback to execute when the script finishes.
     */
    public static void callScript(String scriptName, ScriptParams params, BaseCallback callback)
    {
        String path = ServicesPath.SCRIPTS + scriptName;
        path += ParameterBuilder.parameterToString(params);
        RestRequestHandlerSingleton.getInstance().getRequest(path, callback);
    }
    
    /**
     * @return default Parameters forwarded to the ran script.
     */
    public static ScriptParams createDefaultParams()
    {
        ScriptParams scriptParams = new ScriptParams();
        if (MainModel.currentProject != null) {
            scriptParams.set(ScriptUtils.PROJECT_PARAMETER, MainModel.currentProject);
        }
        if (MainModel.currentUser != null) {
            scriptParams.set(ScriptUtils.PERSON_PARAMETER, MainModel.currentUser);
        }
        return scriptParams;
    }
}
