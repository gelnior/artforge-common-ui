package fr.hd3d.common.ui.client.util;

import fr.hd3d.common.ui.client.modeldata.technical.DynTypeModelData;


/**
 * Util methods for dynamic attributes manipulation.<br>
 * Work in progress.
 * 
 * @author HD3D
 */
public class DynUtils
{
    public static void createDynType(String name, String type, String defaultValue)
    {
        DynTypeModelData dynType = new DynTypeModelData();
        dynType.setName(name);
        dynType.setType(type);
        dynType.setDefaultValue(defaultValue);
        dynType.setNature(DynTypeModelData.SIMPLE_NATURE);

        dynType.save();
    }
}
