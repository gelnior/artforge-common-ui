package fr.hd3d.common.ui.client.util;

import com.extjs.gxt.ui.client.core.FastMap;

import fr.hd3d.common.ui.client.logs.Logger;


/**
 * Simple FastMap wrapper to use long as key while enjoying fast map quickness. It is useful for map based on model data
 * id : you don't have to convert it all the time the ID to a string.
 * 
 * @author HD3D
 * 
 * @param <T>
 */
public class FastLongMap<T> extends FastMap<T>
{
    private static final long serialVersionUID = 8381485753646229956L;

    /**
     * @param key
     *            The key as a Long (an object ID most of the time).
     * @return Return value corresponding to given long.
     */
    public T get(Long key)
    {
        if (key != null)
            return get(key.toString());
        else
            return null;
    }

    /**
     * Add a new entry to the fast map, by converting given key to a string.
     * 
     * @param key
     *            The key as a Long object.
     * @param value
     *            The value to add into the map.
     */
    public void put(Long key, T value)
    {
        if (key != null)
            put(key.toString(), value);
        else
            Logger.error("FastLongMap: You tried to put a value with a key that is null.");
    }
}
