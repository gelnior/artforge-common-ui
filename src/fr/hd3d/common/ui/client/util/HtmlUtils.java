package fr.hd3d.common.ui.client.util;

import fr.hd3d.common.ui.client.logs.Logger;


/**
 * Utils to modify the html text.
 * 
 * @author HD3D
 * 
 */
public class HtmlUtils
{
    public static final String BLANK_CHAR = "&#160;";
    public static final String LINK_PATTERN = "(http://.*?|https://.*?|file:///.*?)[,;\n ]";

    /**
     * Return the given text changing the URL to HyperText.
     * 
     * @param text
     * @return text with hypertext.
     */
    public static String changeToHyperText(String text)
    {
        if (text == null)
        {
            Logger.log("HtmlUtils - changeToHyperText -> the text is null");
            return null;
        }
        text = text.replaceAll("<a.*?>", " ");
        text = text.replaceAll("</a>", " ");
        return text.replaceAll(LINK_PATTERN, "<a href='$1' rel='noreferrer' target='_blank'>$1</a>");
    }
}
