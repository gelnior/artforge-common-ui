package fr.hd3d.common.ui.client.util.field;

import com.extjs.gxt.ui.client.data.DataField;


/**
 * Datafield of type boolean (type already set).
 * 
 * @author HD3D
 */
public class BooleanDataField extends DataField
{
    public BooleanDataField(String name)
    {
        super(name);
        this.setType(Boolean.class);
    }
}
