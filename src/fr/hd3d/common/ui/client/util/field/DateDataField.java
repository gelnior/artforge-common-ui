package fr.hd3d.common.ui.client.util.field;

import java.util.Date;

import com.extjs.gxt.ui.client.data.DataField;

import fr.hd3d.common.client.DateFormat;


/**
 * Datafield of type date (type already set).
 * 
 * @author HD3D
 */
public class DateDataField extends DataField
{
    public DateDataField(String name)
    {
        super(name);
        this.setType(Date.class);
        this.setFormat(DateFormat.TIMESTAMP_FORMAT_STRING);
    }
}
