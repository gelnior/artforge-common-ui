package fr.hd3d.common.ui.client.util.field;

import com.extjs.gxt.ui.client.data.DataField;


/**
 * Datafield of type long (type already set).
 * 
 * @author HD3D
 */
public class LongDataField extends DataField
{
    public LongDataField(String name)
    {
        super(name);

        this.setType(Long.class);
    }

    public LongDataField(String name, String format)
    {
        this(name);
        this.setFormat(format);
    }
}
