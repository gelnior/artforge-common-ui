package fr.hd3d.common.ui.client.util.field;

import java.util.List;

import com.extjs.gxt.ui.client.data.DataField;


/**
 * Datafield of type list (already configured).
 * 
 * @author HD3D
 */
public class ListDataField extends DataField
{

    public ListDataField(String name)
    {
        super(name);
        this.setType(List.class);
    }

    public ListDataField(String name, String className)
    {
        this(name);
        this.setFormat(className);
    }
}
