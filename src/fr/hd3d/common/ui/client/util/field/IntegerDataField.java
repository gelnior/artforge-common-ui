package fr.hd3d.common.ui.client.util.field;

import com.extjs.gxt.ui.client.data.DataField;


/**
 * Datafield of type integer (type already set).
 * 
 * @author HD3D
 */
public class IntegerDataField extends DataField
{
    public IntegerDataField(String name)
    {
        super(name);
        this.setType(Integer.class);
    }
}
