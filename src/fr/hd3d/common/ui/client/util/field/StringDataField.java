package fr.hd3d.common.ui.client.util.field;

import com.extjs.gxt.ui.client.data.DataField;


/**
 * Datafield of type string (type already set).
 * 
 * @author HD3D
 */
public class StringDataField extends DataField
{
    public StringDataField(String name)
    {
        super(name);
        this.setType(String.class);
    }
}
