package fr.hd3d.common.ui.client.util;

/**
 * Generates unique long. It starts from -1 then new ID corresponds to last given id - 1. Negative values are used to
 * not conflict with real IDs.
 * 
 * @author HD3D
 */
public class IdGenerator
{
    private static Long lastId = 0L;

    public static Long getNewId()
    {
        return lastId--;
    }
}
