package fr.hd3d.common.ui.client.restlet;

import java.util.HashMap;
import java.util.Map;

import org.restlet.client.Client;
import org.restlet.client.Request;
import org.restlet.client.Response;
import org.restlet.client.data.Form;
import org.restlet.client.data.Method;
import org.restlet.client.data.Protocol;
import org.restlet.client.data.Status;
import org.restlet.client.ext.json.JsonRepresentation;

import com.google.gwt.core.client.JavaScriptException;
import com.google.gwt.http.client.URL;
import com.google.gwt.user.client.Window;

import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;


/**
 * This singleton gives simple method to send HTTP requests to HD3D web services.
 * 
 * @author HD3D
 */
public final class RestRequestHandler implements IRestRequestHandler
{
    /**
     * In the HD3D XML configuration file, the node name which contains services path is the same as XML_ELEMENT
     * _SERVICES_PATH.
     */
    public static final String XML_ELEMENT_SERVICES_PATH = "path";

    /** Path where services are accessible. */
    private String servicePath = "";

    /** Services server URL */
    private String serverUrl = "";

    /**
     * Default constructor. Sets the server URL automatically from the navigator informations.
     */
    public RestRequestHandler()
    {
        this.serverUrl = Window.Location.getProtocol() + "//" + Window.Location.getHost() + "/";
    }

    /**
     * @return Complete server URL.
     */
    public final String getServerUrl()
    {
        return this.serverUrl;
    }

    @Deprecated
    public final void setServerUrl(String serverUrl)
    {
        this.serverUrl = "http://" + serverUrl + "/";
    }

    /** Sets path where services are accessible. */
    public final void setServicePath(String servicePath)
    {
        this.servicePath = servicePath;
    }

    /**
     * @return The services path.
     */
    public final String getServicePath()
    {
        return this.servicePath;
    }

    /**
     * @return serverUrl + servicePath
     */
    public final String getServicesUrl()
    {
        return this.serverUrl + this.servicePath;
    }

    /**
     * Send a GET request to the specified URL.
     * 
     * @param path
     *            The request path.
     * @param callback
     *            The callback that handles the response.
     */
    public final void getRequest(final String path, final BaseCallback callback)
    {
        handleRequest(Method.GET, path, null, callback);
    }

    /**
     * Send a PUT request with a json object as parameter to the specified URL.
     * 
     * @param path
     *            The request path.
     * @param callback
     *            The callback that handles the response.
     * @param jsonObject
     *            The object to post written in json format.
     */
    public final void putRequest(final String path, final BaseCallback callback, final String jsonObject)
    {
        handleRequest(Method.PUT, path, jsonObject, callback);
    }

    /**
     * Send a POST request with a json object as parameter to the specified URL.
     * 
     * @param path
     *            The request path.
     * @param callback
     *            The callback that handles the response.
     */
    public final void postRequest(final String path, final BaseCallback callback, final String jsonObject)
    {
        handleRequest(Method.POST, path, jsonObject, callback);
    }

    /**
     * Send a DELETE request to the specified URL.
     * 
     * @param path
     *            The request path.
     * @param callback
     *            The callback that handles the response.
     */
    public final void deleteRequest(final String path, final BaseCallback callback)
    {
        handleRequest(Method.DELETE, path, null, callback);
    }

    /**
     * Perform the given request.
     * 
     * @param method
     *            The HTTP method to set on request.
     * @param path
     *            The service path.
     * @param jsonObject
     *            The Json object to send when method is PUT or POST.
     * @param callback
     *            The callback that handles the response.
     */
    public final void handleRequest(Method method, final String path, final String jsonObject,
            final BaseCallback callback)
    {
        final String url = URL.encode(this.getServicesUrl() + path);
        final Request request = new Request(method, url);
        final Client c = new Client(Protocol.HTTP);

        if (jsonObject != null)
        {
            JsonRepresentation representation = new JsonRepresentation(jsonObject);

            request.setEntity(representation);
        }

        try
        {
            c.handle(request, callback);
        }
        catch (JavaScriptException e)
        {
            Logger.log("Gwt error parsing", e);
            Response response = new Response(request);
            response.setStatus(new Status(Status.SERVER_ERROR_INTERNAL, "Parsing failed"));
            callback.handle(request, response);
        }
    }

    /**
     * Perform the given request.
     * 
     * @param method
     *            The HTTP method to set on request.
     * @param path
     *            The service path.
     * @param form
     *            The form to attach to the Request.
     * @param callback
     *            The callback that handles the response.
     */
    public final void handleAttributeRequest(Method method, final String path, final Form form,
            final BaseCallback callback)
    {
        final String url = URL.encode(this.getServicesUrl() + path);
        final Request request = new Request(method, url);
        final Client c = new Client(Protocol.HTTP);

        Map<String, Object> map = new HashMap<String, Object>();
        map.putAll(form.getValuesMap());

        request.setAttributes(map);
        request.setEntity(form.getWebRepresentation());

        c.handle(request, callback);
    }

    /**
     * Return id from an object location.
     * 
     * Ex: When location is http://server/services/computers/4028, it returns 4028.
     */
    public final Long getIdFromLocation(String location)
    {
        String contentID = location.substring(location.lastIndexOf('/') + 1, location.length());
        return Long.parseLong(contentID);
    }

    /**
     * Send a pre-built request.
     * 
     * @param request
     *            The request to send.
     * @param callback
     *            The callback that handles the request response.
     */
    public void sendRequest(Request request, BaseCallback callback)
    {
        final Client c = new Client(Protocol.HTTP);

        c.handle(request, callback);
    }
}
