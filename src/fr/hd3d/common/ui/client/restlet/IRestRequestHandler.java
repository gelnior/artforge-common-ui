package fr.hd3d.common.ui.client.restlet;

import org.restlet.client.data.Form;
import org.restlet.client.data.Method;

import fr.hd3d.common.ui.client.service.callback.BaseCallback;


/**
 * This singleton gives simple method to send HTTP requests to HD3D web services.
 * 
 * @author HD3D
 */
public interface IRestRequestHandler
{
    /** Name of the form variable which contains the json object to send to web services. */
    public final static String OBJECT = "object";

    /**
     * @return Complete server URL.
     */
    public String getServerUrl();

    /** Sets path where services are accessible. */
    public void setServicePath(String servicePath);

    /**
     * @return The services path.
     */
    public String getServicePath();

    /**
     * return serverUrl + servicePath
     */
    public String getServicesUrl();

    /**
     * Return id from an object location.
     * 
     * Ex: When location is http://server/services/computers/4028, it returns 4028.
     */
    public Long getIdFromLocation(String location);

    /**
     * Perform the given request.
     * 
     * @param method
     *            The HTTP method to set on request.
     * @param path
     *            The service path.
     * @param jsonObject
     *            The Json object to send when method is PUT or POST.
     * @param callback
     *            The callback that handles the response.
     * @throws Exception
     *             Raise if the connector can't be stopped.
     */
    public void handleRequest(Method method, final String path, final String jsonObject, final BaseCallback callback);

    /**
     * Perform the given request.
     * 
     * @param method
     *            The HTTP method to set on request.
     * @param path
     *            The service path.
     * @param form
     *            The form to attach to the Request.
     * @param callback
     *            The callback that handles the response.
     */
    public void handleAttributeRequest(Method method, final String path, final Form form, final BaseCallback callback);

    /**
     * Sets the server URL.
     * 
     * @param serverUrl
     *            The url to set as server URL.
     */
    public void setServerUrl(String serverUrl);

    /**
     * Send a GET request to the specified URL.
     * 
     * @param path
     *            The request URL.
     * @param callback
     *            The callback that handles the response.
     * @throws Exception
     *             Raise if the connector can't be stopped/
     */
    public void getRequest(final String path, final BaseCallback callback);

    /**
     * Send a PUT request with a json object as parameter to the specified URL.
     * 
     * @param path
     *            The request URL.
     * @param callback
     *            The callback that handles the response.
     * @throws Exception
     *             Raise if the connector can't be stopped/
     */
    public void putRequest(final String path, final BaseCallback callback, final String jsonObject);

    /**
     * Send a POST request with a json object as parameter to the specified URL.
     * 
     * @param path
     *            The request URL.
     * @param callback
     *            The callback that handles the response.
     * @throws Exception
     *             Raise if the connector can't be stopped/
     */
    public void postRequest(final String path, final BaseCallback callback, final String jsonObject);

    /**
     * Send a DELETE request to the specified URL.
     * 
     * @param path
     *            The request URL.
     * @param callback
     *            The callback that handles the response.
     * @throws Exception
     *             Raise if the connector can't be stopped/
     */
    public void deleteRequest(final String path, final BaseCallback callback);

}
