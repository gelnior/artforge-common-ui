package fr.hd3d.common.ui.client.restlet;



/**
 * Singleton needed to switch between real REST request handler and mock request handler.
 * 
 * @author HD3D
 */
public class RestRequestHandlerSingleton
{
    /** The request handler instance to return. */
    private static IRestRequestHandler requestHandler;

    /**
     * @return The set request handler instance. If it is null, it returns a new real REST request handler.
     */
    public final static IRestRequestHandler getInstance()
    {
        if (requestHandler == null)
        {
            requestHandler = new RestRequestHandler();
        }
        return requestHandler;
    }

    /**
     * Sets the request handler instance. This method is useful for unit tests.
     * 
     * @param mock
     *            The request handler to set.
     */
    public final static void setInstanceAsMock(IRestRequestHandler mock)
    {
        requestHandler = mock;
    }
}
