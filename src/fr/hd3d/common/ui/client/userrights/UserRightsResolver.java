package fr.hd3d.common.ui.client.userrights;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.restlet.client.Request;
import org.restlet.client.Response;
import org.restlet.client.data.Method;

import com.extjs.gxt.ui.client.data.BaseListLoadResult;
import com.extjs.gxt.ui.client.data.ListLoadResult;
import com.extjs.gxt.ui.client.data.ModelType;
import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.error.ErrorDispatcher;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.common.ui.client.userrights.shiro.WildcardPermission;
import fr.hd3d.common.ui.client.userrights.shiro.WildcardPermissionResolver;


/**
 * A <code>UserRightsResolver</code> is an utility for checking the client application's user's permissions. Each
 * application running must refer to it whenever it has to check whether the user is allowed to perform a given action.
 * User's permissions should be loaded from server on application's initialization or on any user's permission update.
 * 
 * @author florent-della-valle
 * 
 */
public class UserRightsResolver
{

    protected final static String WILDCARD = "*";
    protected final static Set<String> ALL_RIGHTS;
    static
    {
        ALL_RIGHTS = new HashSet<String>();
        ALL_RIGHTS.add(WILDCARD);
    }

    protected static final WildcardPermissionResolver resolver = new WildcardPermissionResolver();
    static Set<String> userPermissions = new HashSet<String>();
    static Set<String> userBans = new HashSet<String>();

    /**
     * Instead of considering the actual user's rights, the resolver will behave as if the user was granted all rights.
     * Careful, though: this doesn't grant the user any right on the server side but only on the client side. In order
     * to cancel <code>superUserMode</code>, use {@link #acquireUserRights()} to recover the user's actual rights.
     */
    public static void superUserMode()
    {
        setUserPermissions(ALL_RIGHTS);
        setUserBans(new HashSet<String>());
    }

    /**
     * Loads the current user's permissions and bans from the server. The user must of course be identified by the
     * server for this action to succeed. Otherwise it loads a single "<code>none</code>" string marker for both
     * permissions and bans set.
     */
    public static void acquireUserRights()
    {
        acquire(false);
    }

    /**
     * Loads a copy of the user permissions. You should ensure that {@link #acquireUserRights()} has been called at
     * least once before, otherwise all you will get is an empty set of permissions. In case the rights has been
     * retrieved but the user is not identified by the server, the set contains a single "<code>none</code>" string
     * marker.
     * 
     * @return the user's permissions in their <code>String</code> representation
     */
    public static Set<String> getUserPermissionsCopy()
    {
        return new HashSet<String>(userPermissions);
    }

    /**
     * Loads a copy of the user bans. You should ensure that {@link #acquireUserRights()} has been called at least once
     * before, otherwise all you will get is an empty set of bans. In case the rights has been retrieved but the user is
     * not identified by the server, the set contains a single "<code>none</code>" string marker.
     * 
     * @return the user's permissions in their <code>String</code> representation
     */
    public static Set<String> getUserBansCopy()
    {
        return new HashSet<String>(userBans);
    }

    /**
     * Decides whether the user has the right to perform a given action. The action is specified through a permission
     * string observing the server syntax. Most permissions will be formulated by a
     * <code>{Hd3dVersion}:{...(parents)...}:{TypeofObject}:{InstanceId}:{CRUDoperation}</code>-kind string. It should
     * be kept in mind that the permission string is almost always deduced from the URL of the server resource the
     * underlying action calls upon.
     * 
     * @param right
     *            a permission string formulated with server syntax
     * @return true if the user has the specified right
     */
    public static boolean userHasRight(String right)
    {
        return intersects(right, userPermissions) && !intersects(right, userBans);
    }

    private static void acquire(boolean bans)
    {
        IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
        String type = "Permission";
        if (bans)
        {
            type = "Ban";
        }
        String path = ServicesPath.getPath(type);
        BaseCallback callback = new PermissionCallback(bans);
        requestHandler.handleRequest(Method.GET, path, null, callback);
    }

    private static boolean intersects(String right, Set<String> permissions)
    {
        WildcardPermission w_right = resolver.resolvePermission(right);
        WildcardPermission w_perm;
        for (String perm : permissions)
        {
            w_perm = resolver.resolvePermission(perm);
            if (w_perm.implies(w_right))
            {
                return true;
            }
        }
        return false;
    }

    protected static void setUserPermissions(Collection<String> permissions)
    {
        userPermissions = new HashSet<String>(permissions);
    }

    protected static void setUserBans(Collection<String> bans)
    {
        userBans = new HashSet<String>(bans);
    }

    // intern call back
    static class PermissionCallback extends BaseCallback
    {
        protected EventType eventType = CommonEvents.PERMISSION_INITIALIZED;

        boolean bans = false;

        public PermissionCallback(boolean bans)
        {
            this.bans = bans;
        }

        @Override
        public void onSuccess(Request request, Response response)
        {
            try
            {
                String json = response.getEntity().getText();

                IReader<String> reader = new PermissionReader();

                ListLoadResult<String> records = reader.read(json);
                List<String> result = records.getData();

                if (result.size() > -1)
                {
                    if (bans)
                    {
                        setUserBans(result);
                        AppEvent event = new AppEvent(eventType);
                        EventDispatcher.forwardEvent(event);
                    }
                    else
                    {
                        setUserPermissions(result);
                        acquire(true);
                    }
                }
                else
                {
                    ErrorDispatcher.sendError(CommonErrors.RECORD_NOT_ON_SERVER);
                }
            }
            catch (Exception e)
            {
                GWT.log("Error occurs while parsing permissions.", e);
            }
        }
    }

    // intern reader
    static class PermissionReader implements IReader<String>
    {
        private int totalCount;

        public int getTotalCount()
        {
            return totalCount;
        }

        public ListLoadResult<String> read(String data)
        {
            data = data.replace("[", "");
            data = data.replace("]", "");
            String[] tab = data.split(", ");
            List<String> result;
            if (!tab[0].equals(""))
            {
                result = Arrays.asList(tab);
            }
            else
            {
                result = new LinkedList<String>();
            }
            ListLoadResult<String> list = new BaseListLoadResult<String>(result);
            return list;
        }

        public void setModelType(ModelType type)
        {}

        public String newModelInstance()
        {
            return null;
        }
    }

}
