package fr.hd3d.common.ui.client.userrights;

import java.util.List;

import com.extjs.gxt.ui.client.widget.Component;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.service.PermissionPath;


/**
 * Set of static method for better permission handling.
 * 
 * @author HD3D
 */
public class PermissionUtil
{
    /** Debug is set at true if the debug mode is on. */
    private static boolean debug = false;
    /** Services version number. */
    private static String VERSION = "v1";

    /**
     * @return True if debug mode is on.
     */
    public static boolean isDebugOn()
    {
        return debug;
    }

    /**
     * Sets debug mode to on.
     */
    public static void setDebugOn()
    {
        debug = true;
    }

    /**
     * Sets debug mode to off.
     */
    public static void setDebugOff()
    {
        debug = false;
    }

    /**
     * Hide <i>component</i> if current user has not the right described by <i>userRights</i>.
     * 
     * @param component
     *            The component to hide.
     * @param userRights
     *            The right to check.
     */
    public static void hideIfNotAllowed(Component component, String userRights)
    {
        if (!UserRightsResolver.userHasRight(userRights) && !debug)
        {
            component.hide();
        }
    }

    /**
     * Disable <i>component</i> if current user has not the right described by <i>userRights</i>.
     * 
     * @param component
     *            The component to disable.
     * @param userRights
     *            The right to check.
     */
    public static void disableIfNotAllowed(Component component, String userRights)
    {
        if (!UserRightsResolver.userHasRight(userRights) && !debug)
        {
            component.disable();
        }
        else
        {
            component.enable();
        }
    }

    /**
     * @param path
     *            The path to analyze
     * @return True if the current user has read rights on the given <i>path</i>.
     */
    public static boolean hasReadRightsOnPath(String path)
    {
        String right = VERSION + ":" + path + ":read";

        return debug || UserRightsResolver.userHasRight(right);
    }

    /**
     * @param path
     *            The path to analyze
     * @return True if the current user has update rights on the given <i>path</i>.
     */
    public static boolean hasUpdateRightsOnPath(String path)
    {
        String right = VERSION + ":" + path + ":*:update";

        return debug || UserRightsResolver.userHasRight(right);
    }

    /**
     * @param path
     *            The path to analyze
     * @return True if the current user has create rights on the given <i>path</i>.
     */
    public static boolean hasCreateRightsOnPath(String path)
    {
        String right = VERSION + ":" + path + ":create";

        return debug || UserRightsResolver.userHasRight(right);
    }

    /**
     * @param path
     *            The path to analyze
     * @return True if the current user has delete rights on the given <i>path</i>.
     */
    public static boolean hasDeleteRightsOnPath(String path)
    {
        String right = VERSION + ":" + path + ":*:delete";

        return debug || UserRightsResolver.userHasRight(right);
    }

    /**
     * @param simpleClassName
     *            The class to analyze
     * @return True if the current user has read rights on the given <i>class</i>.
     */
    public static boolean hasReadRights(String simpleClassName)
    {
        String right = VERSION + ":" + PermissionPath.getPath(simpleClassName) + ":read";

        return debug || UserRightsResolver.userHasRight(right);
    }

    /**
     * @param simpleClassName
     *            The class to analyze
     * @return True if the current user has update rights on the given <i>class</i>.
     */
    public static boolean hasUpdateRights(String simpleClassName)
    {
        String right = VERSION + ":" + PermissionPath.getPath(simpleClassName) + ":*:update";

        return debug || UserRightsResolver.userHasRight(right);
    }

    /**
     * @param simpleClassName
     *            The class to analyze
     * @return True if the current user has create rights on the given <i>class</i>.
     */
    public static boolean hasCreateRights(String simpleClassName)
    {
        String right = VERSION + ":" + PermissionPath.getPath(simpleClassName) + ":create";

        return debug || UserRightsResolver.userHasRight(right);
    }

    /**
     * @param simpleClassName
     *            The class to analyze
     * @return True if the current user has delete rights on the given <i>class</i>.
     */
    public static boolean hasDeleteRights(String simpleClassName)
    {
        String right = VERSION + ":" + PermissionPath.getPath(simpleClassName) + ":*:delete";

        return debug || UserRightsResolver.userHasRight(right);
    }

    @SuppressWarnings("unchecked")
    public static boolean canDeleteModelList(List<?> models)
    {
        Boolean canDelete = true;
        for (Hd3dModelData model : ((List<Hd3dModelData>) models))
        {
            canDelete &= model.getUserCanDelete();
        }

        return canDelete;
    }

    @SuppressWarnings("unchecked")
    public static boolean canUpdateModelList(List<?> models)
    {
        Boolean canUpdate = true;
        for (Hd3dModelData model : ((List<Hd3dModelData>) models))
        {
            canUpdate &= model.getUserCanDelete();
        }

        return canUpdate;
    }
}
