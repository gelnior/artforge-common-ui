package fr.hd3d.common.ui.client.userrights;

import com.extjs.gxt.ui.client.data.ModelData;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.grid.GridViewConfig;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;


/**
 * Grid view config to highlight row that cannot be updated by user due to a lack of permissions.
 * 
 * @author HD3D
 * */
public class PermissionGridViewConfig extends GridViewConfig
{
    @Override
    public String getRowStyle(ModelData model, int rowIndex, ListStore<ModelData> ds)
    {
        Boolean canUpdate = (Boolean) model.get(Hd3dModelData.USER_CAN_UPDATE_FIELD);
        if (canUpdate != null && !canUpdate)
            return "row-no-update";
        else
            return "";
    }
}
