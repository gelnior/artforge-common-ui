package fr.hd3d.common.ui.test.widget.sheetfiltercombobox;

import fr.hd3d.common.ui.client.modeldata.technical.SheetFilterModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.widget.constraint.widget.ISheetFilterView;
import fr.hd3d.common.ui.client.widget.constraint.widget.SheetFilterModel;


/**
 * Mocking of sheet filter combo box for test purpose.
 * 
 * @author HD3D
 */
public class SheetFilterComboMock implements ISheetFilterView
{
    public static final String CUSTOM_LABEL = "Custom filter";

    protected SheetFilterModel model;

    protected SheetFilterModelData selectedFilter = null;
    protected String displayFilter = "";
    protected Boolean isSaveFilterDialogDisplayed = false;
    protected Boolean isRenameDialogDisplayed = false;

    public SheetFilterComboMock(SheetFilterModel model)
    {
        this.model = model;
    }

    public Boolean isSaveFilterDialogDisplayed()
    {
        return isSaveFilterDialogDisplayed;
    }

    public void setIsSaveFilterDialogDisplayed(Boolean isSaveFilterDialogDisplayed)
    {
        this.isSaveFilterDialogDisplayed = isSaveFilterDialogDisplayed;
    }

    public Boolean isRenameDialogDisplayed()
    {
        return isRenameDialogDisplayed;
    }

    public void setIsRenameDialogDisplayed(Boolean isRenameDialogDisplayed)
    {
        this.isRenameDialogDisplayed = isRenameDialogDisplayed;
    }

    public void setNoFilterSelected()
    {
        this.selectedFilter = this.model.getNoFilter();
        this.displayFilter = this.selectedFilter.getName();
    }

    public SheetFilterModelData getSelectedFilter()
    {
        return this.selectedFilter;
    }

    public void setUpdateFilterRawValue(String name)
    {
        this.displayFilter = name;
    }

    public void showSaveFilterDialog(String filterText, SheetModelData sheet)
    {
        this.isSaveFilterDialogDisplayed = true;
    }

    public void refreshFilterList(SheetModelData sheet)
    {
        this.model.refreshFilterStore(sheet);
    }

    public void displayRenameFilter(SheetFilterModelData filter)
    {
        this.isRenameDialogDisplayed = true;
    }

    public void setCustomLabel()
    {
        this.displayFilter = CUSTOM_LABEL;
    }

    public void setValue(SheetFilterModelData filter)
    {
        this.selectedFilter = filter;
    }

    public String getRawValue()
    {
        return this.displayFilter;
    }

    public void setRawValue(String name)
    {
        this.displayFilter = name;
    }

    public void highlightFilterPanelButton()
    {
        // TODO Auto-generated method stub

    }

    public void removeHighlightFilterPanelButton()
    {
        // TODO Auto-generated method stub

    }

}
