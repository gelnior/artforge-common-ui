package fr.hd3d.common.ui.test.widget.sheetfiltercombobox;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.reader.SheetFilterReader;
import fr.hd3d.common.ui.client.modeldata.technical.SheetFilterModelData;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.EqConstraint;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.widget.constraint.event.ConstraintEvents;
import fr.hd3d.common.ui.client.widget.constraint.model.ConstraintPanelModel;
import fr.hd3d.common.ui.client.widget.constraint.widget.SheetFilterController;
import fr.hd3d.common.ui.client.widget.constraint.widget.SheetFilterModel;
import fr.hd3d.common.ui.client.widget.explorer.config.ExplorerConfig;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;
import fr.hd3d.common.ui.test.UITestCase;


public class SheetFilterControllerTest extends UITestCase
{
    SheetFilterModel model = new SheetFilterModel();
    SheetFilterComboMock view = new SheetFilterComboMock(model);

    @Override
    public void setUp()
    {
        super.setUp();

        model.setFilterStore(new ServiceStore<SheetFilterModelData>(new SheetFilterReader()));

        SheetFilterController controller = new SheetFilterController(model, view);
        EventDispatcher.get().getControllers().clear();
        EventDispatcher.get().addController(controller);
        EventDispatcher.get().addController(errorController);
    }

    @Test
    public void testFilterSaveClicked()
    {
        this.view.setIsSaveFilterDialogDisplayed(false);
        EventDispatcher.forwardEvent(ConstraintEvents.FILTER_SAVE_CLICKED);
        assertTrue(this.view.isSaveFilterDialogDisplayed());
    }

    @Test
    public void testFilterSavedSuccess()
    {
        SheetFilterModelData filter = new SheetFilterModelData("filter_test");

        EventDispatcher.forwardEvent(ConstraintEvents.FILTER_SAVED_SUCCESS, filter);
        assertNotNull(model.getFilterStore().findModel(filter));
        assertEquals("filter_test", this.view.getRawValue());

        filter.setName("filter_test_2");
        EventDispatcher.forwardEvent(ConstraintEvents.FILTER_SAVED_SUCCESS, filter);
        assertEquals("filter_test_2", model.getFilterStore().getAt(0).getName());
        assertEquals("filter_test_2", this.view.getRawValue());
    }

    @Test
    public void testFilterRenameClicked()
    {
        this.view.setIsRenameDialogDisplayed(false);
        EventDispatcher.forwardEvent(ConstraintEvents.FILTER_RENAME_CLICKED);
        assertTrue(!this.view.isRenameDialogDisplayed());

        this.view.selectedFilter = new SheetFilterModelData();
        EventDispatcher.forwardEvent(ConstraintEvents.FILTER_RENAME_CLICKED);
        assertTrue(this.view.isRenameDialogDisplayed());
    }

    @Test
    public void testFilterRenamed()
    {
        SheetFilterModelData filter = new SheetFilterModelData("filter_test");
        this.model.getFilterStore().add(filter);
        filter.setName("filter_test_2");

        EventDispatcher.forwardEvent(ConstraintEvents.FILTER_RENAMED, filter);
        assertEquals("filter_test_2", model.getFilterStore().getAt(0).getName());
        assertEquals("filter_test_2", this.view.getRawValue());
    }

    @Test
    public void testDeleteClicked()
    {
        SheetFilterModelData filter = new SheetFilterModelData("filter_test");
        filter.save();

        this.model.getFilterStore().add(filter);
        this.view.selectedFilter = filter;
        this.view.setRawValue(filter.getName());
        EventDispatcher.forwardEvent(ConstraintEvents.FILTER_DELETE_CLICKED, filter);
        assertEquals(this.model.getNoFilter(), this.view.selectedFilter);
        assertEquals(0, this.model.getFilterStore().getCount());
        assertEquals(this.model.getNoFilter().getName(), this.view.getRawValue());

        filter.refresh();
        assertEquals(CommonErrors.RECORD_NOT_ON_SERVER, errorController.getLastError());
    }

    @Test
    public void testNewUserFilterSet()
    {
        AppEvent event = new AppEvent(ExplorerEvents.NEW_USER_FILTER_SET);
        event.setData(ExplorerConfig.IS_SHEET_FILTER_EVENT_VAR_NAME, Boolean.TRUE);

        SheetFilterModelData filter = new SheetFilterModelData("filter_test");
        filter.setFilter("stringFilter");
        this.model.getFilterStore().add(filter);
        this.view.selectedFilter = filter;
        EventDispatcher.forwardEvent(event);
        assertEquals(filter, this.view.selectedFilter);

        event.setData(ExplorerConfig.IS_SHEET_FILTER_EVENT_VAR_NAME, Boolean.FALSE);
        EventDispatcher.forwardEvent(event);
        assertEquals(this.model.getNoFilter(), this.view.selectedFilter);

        EqConstraint constraint = new EqConstraint(RecordModelData.NAME_FIELD, "test");
        List<Constraint> constraints = new ArrayList<Constraint>();
        constraints.add(constraint);
        event.setData(ConstraintPanelModel.CONSTRAINTS, constraints);
        EventDispatcher.forwardEvent(event);
        assertEquals(SheetFilterComboMock.CUSTOM_LABEL, this.view.displayFilter);

        this.view.displayFilter = "";
        event.setData(ExplorerConfig.CONSTRAINT_STRING_EVENT_VAR_NAME, "otherStringFilter");
        EventDispatcher.forwardEvent(event);
        assertEquals(SheetFilterComboMock.CUSTOM_LABEL, this.view.displayFilter);

        event.setData(ExplorerConfig.CONSTRAINT_STRING_EVENT_VAR_NAME, "stringFilter");
        EventDispatcher.forwardEvent(event);
        assertEquals(filter.getName(), this.view.displayFilter);
    }
}
