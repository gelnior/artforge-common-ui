package fr.hd3d.common.ui.test.widget.sheeteditor.mock;

import java.util.List;

import com.extjs.gxt.ui.client.data.BaseModelData;
import com.extjs.gxt.ui.client.event.BaseObservable;
import com.extjs.gxt.ui.client.store.Record;

import fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata.EntityModelData;
import fr.hd3d.common.ui.client.widget.sheeteditor.view.ISheetEditorView;


public class SheetEditorViewMock extends BaseObservable implements ISheetEditorView
{
    public boolean isShowedWindow = false;
    public boolean isTreeExpanded = false;
    public boolean entityTypeEnabled = false;
    public String entityType;
    public String sheetName = "";
    public int errorType;

    public void expandTrees()
    {
        this.isTreeExpanded = true;
    }

    public String getSheetNameValue()
    {
        return sheetName;
    }

    public void setName(String name)
    {
        sheetName = name;
    }

    public void showSaving()
    {
        this.isShowedWindow = true;
    }

    public void hideSaving()
    {
        this.isShowedWindow = false;
    }

    public void setEntityTypeComboboxEnabled(boolean enable)
    {
        this.entityTypeEnabled = enable;

    }

    public void displayError(int error)
    {
        this.errorType = error;
    }

    public void showAttributeEditor()
    {
        // TODO Auto-generated method stub

    }

    public void onShow()
    {
        // TODO Auto-generated method stub

    }

    public void autoSelectEntityType()
    {
        // TODO Auto-generated method stub

    }

    public void renameSelectGroup()
    {
        // TODO Auto-generated method stub

    }

    public void editApprovalNoteType(Long projectId)
    {
        // TODO Auto-generated method stub

    }

    public List<BaseModelData> getSheetSelectedItems()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public EntityModelData getEntityType()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public void reset()
    {
        // TODO Auto-generated method stub

    }

    public void setEntityType(EntityModelData entityType)
    {
        // TODO Auto-generated method stub

    }

    public void setEntityPropertiesLoadingMask(Boolean visible)
    {

    }

    public void selectFirstEntity()
    {
        // TODO Auto-generated method stub

    }

    public Record getSelectedRecord()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public void hideValidationButton()
    {
        // TODO Auto-generated method stub

    }

    public void selectEntity(String boundClassName)
    {
        // TODO Auto-generated method stub

    }

    public void showExtendedAttributeEditor()
    {
        // TODO Auto-generated method stub

    }

    public List<BaseModelData> getSelectedSheetItems()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public Record getSelectedItemRecord()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public void setSheetName(String name)
    {
        // TODO Auto-generated method stub

    }

    public void maskEntityPropertiesTree(Boolean visible)
    {
        // TODO Auto-generated method stub

    }

    public void showNoteTypeEditor(Long projectId)
    {
        // TODO Auto-generated method stub

    }

    public void showDialog()
    {
        // TODO Auto-generated method stub

    }

    public void hideDialog()
    {
        // TODO Auto-generated method stub

    }

    public void hideSaveAsButton()
    {
        // TODO Auto-generated method stub

    }

    public void showSaveAsButton()
    {
        // TODO Auto-generated method stub

    }

}
