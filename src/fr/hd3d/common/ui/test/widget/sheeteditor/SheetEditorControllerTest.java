package fr.hd3d.common.ui.test.widget.sheeteditor;

import org.junit.Test;

import fr.hd3d.common.ui.test.UITestCase;


public class SheetEditorControllerTest extends UITestCase
{
    // private final SheetEditorModel model = new SheetEditorModel();
    // private final SheetEditorWindowMock mainView = new SheetEditorWindowMock();
    // private final SheetEditorViewMock view = new SheetEditorViewMock();

    // private final String newEditorName = "My New editor";
    // private final String newRendererName = "My New renderer";

    @Override
    public void setUp()
    {
        super.setUp();

        // SheetEditorController controller = new SheetEditorController(mainView, view, model);
        // EventDispatcher.get().getControllers().clear();
        // EventDispatcher.get().addController(controller);
        //
        // EntityPropertyReaderSingleton.setInstanceAsMock(new EntityPropertyReaderMock());
    }

    // @Test
    // public void testEntityTypeChange()
    // {
    // String entityType = "";
    // BaseModelData root = this.model.getEntityPropertyStore().getRootItems().get(0);
    // this.model.getEntityPropertyStore().add(root, new ItemModelData(), false);
    // EventType eventType = SheetEditorEvents.ENTITY_TYPE_CHANGE;
    //
    // AppEvent event = new AppEvent(eventType, entityType);
    // EventDispatcher.forwardEvent(event);
    // assertTrue(view.entityTypeEnabled);
    // assertTrue(view.isTreeExpanded);
    // assertEquals(1, this.model.getEntityPropertyStore().getAllItems().size());
    //
    // boolean lock = true;
    // this.model.getEntityPropertyStore().add(root, new ItemModelData(), false);
    // event = new AppEvent(eventType, entityType);
    // event.setData("lock", lock);
    // EventDispatcher.forwardEvent(event);
    // assertFalse(view.entityTypeEnabled);
    // assertTrue(view.isTreeExpanded);
    // assertEquals(1, this.model.getEntityPropertyStore().getAllItems().size());
    //
    // lock = false;
    // this.model.getEntityPropertyStore().add(root, new ItemModelData(), false);
    // event = new AppEvent(eventType, entityType);
    // event.setData("lock", lock);
    // EventDispatcher.forwardEvent(event);
    // assertTrue(view.entityTypeEnabled);
    // assertTrue(view.isTreeExpanded);
    // assertEquals(1, this.model.getEntityPropertyStore().getAllItems().size());
    //
    // lock = false;
    // entityType = "Constituent";
    // this.model.getEntityPropertyStore().add(root, new ItemModelData(), false);
    // event = new AppEvent(eventType, entityType);
    // event.setData("lock", lock);
    // EventDispatcher.forwardEvent(event);
    // assertTrue(view.entityTypeEnabled);
    // assertTrue(view.isTreeExpanded);
    // assertTrue(this.model.getEntityPropertyStore().getAllItems().size() > 17);
    // }
    //
    // @Test
    // public void testSheetNameChanged()
    // {
    // assertEquals("", view.getSheetNameValue());
    // String sheetName = "Mon Nouveau Nom";
    //
    // EventType eventType = SheetEditorEvents.SHEET_NAME_CHANGE;
    // AppEvent event = new AppEvent(eventType, sheetName);
    // EventDispatcher.forwardEvent(event);
    // assertEquals(sheetName, view.getSheetNameValue());
    // }
    //
    // @Test
    // public void testCreateGroup()
    // {
    // // recuperation du nombre de group actuel
    // List<IColumnGroup> groups = this.model.getSheetItemGroups();
    // int nbGroup = groups.size();
    //
    // String groupName = "newGroup";
    //
    // EventType eventType = SheetEditorEvents.SHEET_NAME_CHANGE;
    // AppEvent event = new AppEvent(eventType, groupName);
    // EventDispatcher.forwardEvent(event);
    //
    // groups = this.model.getSheetItemGroups();
    //
    // assertEquals(nbGroup + 1, groups.size());
    // }
    //
    // @Test
    // public void testEntityPropertyLoaded()
    // {
    // // TODO tester bien distinctement EntityTypeChange et EntityPropertyLoaded.
    // testEntityTypeChange();
    // }
    //
    // @Test
    // public void testStartSave()
    // {
    // assertFalse(this.view.isShowedWindow);
    // EventDispatcher.forwardEvent(SheetEditorEvents.START_SAVE);
    // assertTrue(this.view.isShowedWindow);
    // }
    //
    // @Test
    // public void testEndSave()
    // {
    // testStartSave();
    //
    // assertTrue(this.view.isShowedWindow);
    // EventDispatcher.forwardEvent(SheetEditorEvents.END_SAVE);
    // assertFalse(this.view.isShowedWindow);
    // }
    //
    // @Test
    // public void testItemEditorChanged()
    // {
    // testEntityTypeChange();
    //
    // int variousItemIndex = 5;
    // ItemModelData item = (ItemModelData) this.model.getEntityPropertyStore().getModels().get(variousItemIndex);
    // // on rajoute l'item dans la liste des items de la sheet
    // List<BaseModelData> currentSheetItems = this.model.getSheetPropertyStore().getAllItems();
    // this.model.getSheetPropertyStore().add(currentSheetItems.get(currentSheetItems.size() - 1), item, false);
    //
    // // on test que l'editor est bien vide
    // assertEquals(item.getEditor(), "");
    //
    // // on dispatch l'event
    // EventType eventType = SheetEditorEvents.ITEM_EDITOR_CHANGE;
    // AppEvent event = new AppEvent(eventType);
    // event.setData("item", item);
    // event.setData("value", newEditorName);
    // EventDispatcher.forwardEvent(event);
    //
    // // on test que maintenant le renderer n'est plus vide
    // assertEquals(item.getEditor(), newEditorName);
    // }
    //
    // @Test
    // public void testItemRendererChanged()
    // {
    // testEntityTypeChange();
    // int variousItemIndex = 5;
    // ItemModelData item = (ItemModelData) this.model.getEntityPropertyStore().getModels().get(variousItemIndex);
    // List<BaseModelData> currentSheetItems = this.model.getSheetPropertyStore().getAllItems();
    // this.model.getSheetPropertyStore().add(currentSheetItems.get(currentSheetItems.size() - 1), item, false);
    // assertEquals(item.getRenderer(), "");
    //
    // EventType eventType = SheetEditorEvents.ITEM_RENDER_CHANGE;
    // AppEvent event = new AppEvent(eventType);
    // event.setData("item", item);
    // event.setData("value", newRendererName);
    // EventDispatcher.forwardEvent(event);
    // assertEquals(item.getRenderer(), newRendererName);
    //
    // }
    //
    // @Test
    // public void testSaveSheet()
    // {
    // // cas 1, il manque le nom de la sheet
    // this.view.sheetName = null;
    // assertNull(this.view.getSheetNameValue());
    //
    // EventDispatcher.forwardEvent(SheetEditorEvents.SAVE_SHEET);
    // assertNull(this.view.getSheetNameValue());
    // assertEquals(SheetEditorErrors.NO_SHEET_NAME, this.view.errorType);
    //
    // // cas 2 on a toutes les infos
    // this.view.sheetName = "myNewSheetName";
    // // String oldSheetName = this.model.getSheet().getName();
    // // assertNotSame(this.view.sheetName, oldSheetName);
    // // EventDispatcher.forwardEvent(event);
    // // assertNotSame(this.view.sheetName, oldSheetName);
    // // assertEquals(this.view.sheetName, this.model.getSheet().getName());
    //
    // }
    //
    // @Test
    // public void testCancelSheet()
    // {}
    //
    // @Test
    // public void testShowWindow()
    // {}
    //
    // @Test
    // public void testRegisterSheet()
    // {}
    //
    @Test
    public void testSetAvailableType()
    {}
    //
    // @Test
    // public void testHideShowSave()
    // {
    // assertFalse(view.isShowedWindow);
    // EventDispatcher.forwardEvent(SheetEditorEvents.START_SAVE);
    // assertTrue(view.isShowedWindow);
    //
    // EventDispatcher.forwardEvent(SheetEditorEvents.END_SAVE);
    // assertFalse(view.isShowedWindow);
    // }
    //
    // @Test
    // public void testCancelEditor()
    // {
    // testItemEditorChanged();
    //
    // // on recupere l'item dont on est sance avoir modifie l'editeu
    // List<BaseModelData> items = this.model.getSheetPropertyStore().getAllItems();
    // ItemModelData editedItem = (ItemModelData) items.get(items.size() - 1);
    // assertEquals(editedItem.getEditor(), newEditorName);
    //
    // // on fait cancel
    // EventDispatcher.forwardEvent(SheetEditorEvents.CANCEL_SHEET);
    //
    // // on verifie que c'est bon
    // assertEquals(editedItem.getEditor(), "");
    // }
    //
    // @Test
    // public void testCancelRenderer()
    // {
    // testItemRendererChanged();
    //
    // // on recupere l'item dont on est sance avoir modifie l'editeur
    // List<BaseModelData> items = this.model.getSheetPropertyStore().getAllItems();
    // ItemModelData editedItem = (ItemModelData) items.get(items.size() - 1);
    // assertEquals(editedItem.getRenderer(), newRendererName);
    //
    // // on fait cancel
    // EventDispatcher.forwardEvent(SheetEditorEvents.CANCEL_SHEET);
    //
    // // on verifie que c'est bon
    // assertEquals(editedItem.getRenderer(), "");
    // }
}
