package fr.hd3d.common.ui.test.widget.sheeteditor.mock;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.test.util.modeldata.reader.Hd3dJsonReaderMock;


public class Hd3dModelDataReaderMock extends Hd3dJsonReaderMock<Hd3dModelData>
{
    @Override
    public Hd3dModelData newModelInstance()
    {
        return new Hd3dModelData();
    }
}
