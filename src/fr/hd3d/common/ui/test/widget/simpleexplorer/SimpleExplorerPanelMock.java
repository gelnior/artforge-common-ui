package fr.hd3d.common.ui.test.widget.simpleexplorer;

import java.util.List;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.widget.simpleexplorer.ISimpleExplorerPanel;
import fr.hd3d.common.ui.client.widget.simpleexplorer.SimpleExplorerModel;


public class SimpleExplorerPanelMock<C extends Hd3dModelData> implements ISimpleExplorerPanel<C>
{
    private boolean isSavingShown = false;
    private boolean isButtonsEnabled = false;
    private final SimpleExplorerModel<C> model;

    private boolean isSaveToolItemEnabled = false;
    private boolean isCreateToolItemEnabled = false;
    private boolean isDeleteToolItemEnabled = false;

    public SimpleExplorerPanelMock(SimpleExplorerModel<C> model)
    {
        this.model = model;
    }

    public boolean isButtonsEnabled()
    {
        return isButtonsEnabled;
    }

    public void enableButtons()
    {
        this.isButtonsEnabled = true;
    }

    public void disableButtons()
    {
        this.isButtonsEnabled = false;
    }

    public boolean isSavingShown()
    {
        return isSavingShown;
    }

    public void showSaving()
    {
        isSavingShown = true;
    }

    public void hideSaving()
    {
        isSavingShown = false;
    }

    public List<C> getSelection()
    {
        return this.model.getModelStore().getModels();
    }

    public void setPermissions()
    {}

    public void idle()
    {
    // TODO Auto-generated method stub

    }

    public void unIdle()
    {
    // TODO Auto-generated method stub

    }

    public boolean isSaveToolItemEnabled()
    {
        return isSaveToolItemEnabled;
    }

    public boolean isCreateToolItemEnabled()
    {
        return isCreateToolItemEnabled;
    }

    public boolean isDeleteToolItemEnabled()
    {
        return isDeleteToolItemEnabled;
    }

    public void enableCreateToolItem(boolean enabled)
    {
        this.isCreateToolItemEnabled = enabled;
    }

    public void enableDeleteToolItem(boolean enabled)
    {
        this.isDeleteToolItemEnabled = enabled;
    }

    public void enableSaveToolItem(boolean enabled)
    {
        this.isSaveToolItemEnabled = enabled;
    }

    public String getNewItemString()
    {
        return "new item";
    }

    public void displaySaveConfirmBox()
    {
    // TODO Auto-generated method stub

    }

    public void resetSelection()
    {
    // TODO Auto-generated method stub

    }
}
