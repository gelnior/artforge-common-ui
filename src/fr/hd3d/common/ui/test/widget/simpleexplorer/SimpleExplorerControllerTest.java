package fr.hd3d.common.ui.test.widget.simpleexplorer;

import org.junit.Test;

import com.extjs.gxt.ui.client.store.Record;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerTypeModelData;
import fr.hd3d.common.ui.client.widget.simpleexplorer.SimpleExplorerController;
import fr.hd3d.common.ui.client.widget.simpleexplorer.SimpleExplorerEvents;
import fr.hd3d.common.ui.client.widget.simpleexplorer.SimpleExplorerModel;
import fr.hd3d.common.ui.test.UITestCase;
import fr.hd3d.common.ui.test.util.TestUserRigthsResolver;
import fr.hd3d.common.ui.test.util.modeldata.reader.ComputerTypeReaderMock;


public class SimpleExplorerControllerTest extends UITestCase
{
    SimpleExplorerModel<ComputerTypeModelData> model;
    SimpleExplorerPanelMock<ComputerTypeModelData> view;
    SimpleExplorerController<ComputerTypeModelData> controller;

    @Override
    public void setUp()
    {
        super.setUp();

        model = new SimpleExplorerModel<ComputerTypeModelData>(new ComputerTypeReaderMock());
        view = new SimpleExplorerPanelMock<ComputerTypeModelData>(model);
        controller = new SimpleExplorerController<ComputerTypeModelData>(model, view);

        EventDispatcher dispatcher = EventDispatcher.get();
        dispatcher.getControllers().clear();
        dispatcher.addController(controller);

        this.model.getModelStore().removeAll();
    }

    @Test
    public void testMask()
    {
        this.controller.unMask();
        assertEquals(0, this.model.getModelStore().getCount());
        EventDispatcher.forwardEvent(SimpleExplorerEvents.ADD_CLICKED);
        assertEquals(1, this.model.getModelStore().getCount());

        this.controller.mask();
        EventDispatcher.forwardEvent(SimpleExplorerEvents.ADD_CLICKED);
        assertEquals(1, this.model.getModelStore().getCount());
    }

    @Test
    public void testOnAddClick()
    {
        assertEquals(0, this.model.getModelStore().getCount());
        EventDispatcher.forwardEvent(SimpleExplorerEvents.ADD_CLICKED);
        assertEquals(1, this.model.getModelStore().getCount());
        EventDispatcher.forwardEvent(SimpleExplorerEvents.ADD_CLICKED);
        assertEquals(2, this.model.getModelStore().getCount());
    }

    @Test
    public void testOnDeleteClick()
    {
        EventDispatcher.forwardEvent(SimpleExplorerEvents.ADD_CLICKED);
        EventDispatcher.forwardEvent(SimpleExplorerEvents.ADD_CLICKED);
        EventDispatcher.forwardEvent(SimpleExplorerEvents.ADD_CLICKED);
        assertEquals(3, this.model.getModelStore().getCount());

        this.model.getModelStore().getAt(0).setId(1L);
        EventDispatcher.forwardEvent(SimpleExplorerEvents.DELETE_CLICKED);

        assertEquals(0, this.model.getModelStore().getCount());
        assertEquals(1, this.model.getDeletedRecords().size());
    }

    @Test
    public void testOnRefreshClick()
    {
        ComputerTypeModelData type = new ComputerTypeModelData("type_test_get_simple_explorer");
        type.save();
        assertEquals(0, this.model.getModelStore().getCount());
        EventDispatcher.forwardEvent(SimpleExplorerEvents.REFRESH_CLICKED);
        assertTrue(this.model.getModelStore().getCount() > 0);

        type.delete();
    }

    @Test
    public void testonEndSaveAndDeleteUpdate()
    {
        this.controller.setNbUpdate(3);
        this.controller.setNbDelete(2);
        this.view.disableButtons();
        this.view.showSaving();

        EventDispatcher.forwardEvent(SimpleExplorerEvents.UPDATE_SUCCESS);
        assertFalse(this.view.isButtonsEnabled());
        assertTrue(this.view.isSavingShown());
        assertEquals(2, this.controller.getNbUpdate());
        assertEquals(2, this.controller.getNbDelete());

        EventDispatcher.forwardEvent(SimpleExplorerEvents.DELETE_SUCCESS);
        assertFalse(this.view.isButtonsEnabled());
        assertTrue(this.view.isSavingShown());
        assertEquals(2, this.controller.getNbUpdate());
        assertEquals(1, this.controller.getNbDelete());

        EventDispatcher.forwardEvent(SimpleExplorerEvents.DELETE_SUCCESS);
        assertFalse(this.view.isButtonsEnabled());
        assertTrue(this.view.isSavingShown());
        assertEquals(2, this.controller.getNbUpdate());
        assertEquals(0, this.controller.getNbDelete());

        EventDispatcher.forwardEvent(SimpleExplorerEvents.UPDATE_SUCCESS);
        assertFalse(this.view.isButtonsEnabled());
        assertTrue(this.view.isSavingShown());
        assertEquals(1, this.controller.getNbUpdate());
        assertEquals(0, this.controller.getNbDelete());

        EventDispatcher.forwardEvent(SimpleExplorerEvents.UPDATE_SUCCESS);
        assertTrue(this.view.isButtonsEnabled());
        assertFalse(this.view.isSavingShown());
        assertEquals(0, this.controller.getNbUpdate());
        assertEquals(0, this.controller.getNbDelete());
    }

    @Test
    public void testOnSaveSimple()
    {
        ComputerTypeModelData newType = new ComputerTypeModelData();
        ComputerTypeModelData type = new ComputerTypeModelData("type_save_simple_explorer");

        type.save();

        EventDispatcher.forwardEvent(SimpleExplorerEvents.REFRESH_CLICKED);
        EventDispatcher.forwardEvent(SimpleExplorerEvents.ADD_CLICKED);
        for (ComputerTypeModelData computerType : this.model.getModelStore().getModels())
        {
            if (computerType.getId() == null)
            {
                newType = computerType;
                newType.setName("type_created");
                this.model.getModelStore().getModifiedRecords().add(new Record(newType));
            }
            else if (computerType.getId().longValue() == type.getId().longValue())
            {
                type = computerType;
                type.setName("type_save_simple_explorer_udpated");
                this.model.getModelStore().update(type);
            }
        }

        this.view.hideSaving();
        this.view.enableButtons();

        EventDispatcher.forwardEvent(SimpleExplorerEvents.SAVE_CLICKED);
        assertTrue(this.view.isButtonsEnabled());
        assertFalse(this.view.isSavingShown());

        EventDispatcher.forwardEvent(SimpleExplorerEvents.REFRESH_CLICKED);
        for (ComputerTypeModelData computerType : this.model.getModelStore().getModels())
        {
            if (computerType.getId() == newType.getId())
            {
                if (newType.getName().equals(computerType.getName()))
                {
                    // isCreationSaved = true;
                }
            }
            else if (computerType.getId() == type.getId())
            {
                if (type.getName().equals(computerType.getName()))
                {
                    // isUpdateSaved = true;
                }
            }
        }
        //
        // assertTrue(isUpdateSaved);
        // assertTrue(isCreationSaved);

        type.delete();
        newType.delete();
    }

    @Test
    public void testOnError()
    {
        this.view.showSaving();
        this.view.disableButtons();
        EventDispatcher.forwardEvent(CommonEvents.ERROR);
        assertTrue(this.view.isButtonsEnabled());
        assertTrue(!this.view.isSavingShown());
    }

    /**
     * Test if permissions are well set.
     */
    @Test
    public void testOnPermissionInitialized()
    {
        EventDispatcher.forwardEvent(CommonEvents.PERMISSION_INITIALIZED);
        assertFalse(this.view.isCreateToolItemEnabled());
        assertFalse(this.view.isDeleteToolItemEnabled());
        assertFalse(this.view.isSaveToolItemEnabled());

        TestUserRigthsResolver.addUserPermission("v1:computertypes:create");
        EventDispatcher.forwardEvent(CommonEvents.PERMISSION_INITIALIZED);
        // assertTrue(this.view.isCreateToolItemEnabled());
        assertFalse(this.view.isDeleteToolItemEnabled());
        assertFalse(this.view.isSaveToolItemEnabled());

        TestUserRigthsResolver.addUserPermission("v1:computertypes:*:update");
        EventDispatcher.forwardEvent(CommonEvents.PERMISSION_INITIALIZED);
        // assertTrue(this.view.isCreateToolItemEnabled());
        assertTrue(this.view.isSaveToolItemEnabled());
        assertFalse(this.view.isDeleteToolItemEnabled());

        TestUserRigthsResolver.addUserPermission("v1:computertypes:*:delete");
        EventDispatcher.forwardEvent(CommonEvents.PERMISSION_INITIALIZED);
        // assertTrue(this.view.isCreateToolItemEnabled());
        assertTrue(this.view.isDeleteToolItemEnabled());
        assertTrue(this.view.isSaveToolItemEnabled());
    }
}
