package fr.hd3d.common.ui.test.widget.identitydialog;

import java.util.ArrayList;
import java.util.List;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.widget.identitydialog.IIdentityDialog;


public class IdentityDialogViewMock implements IIdentityDialog
{

    private Boolean isInfoWidgetsHidden = Boolean.FALSE;
    private Boolean isCastingTaskGridMaximized = Boolean.FALSE;
    private Boolean isCleared = Boolean.FALSE;
    private Boolean isDataReloaded = Boolean.FALSE;
    private Boolean isConstituentGridDisplayed = Boolean.FALSE;
    private Boolean isShotGridDisplayed = Boolean.FALSE;

    private Hd3dModelData simplyDisplayedWorkObject = null;
    private ProjectModelData project = null;
    private final List<TaskTypeModelData> selectedTaskTypes = new ArrayList<TaskTypeModelData>();

    public Boolean isCastingTaskGridMaximized()
    {
        return isCastingTaskGridMaximized;
    }

    public Boolean isInfoWidgetsHidden()
    {
        return isInfoWidgetsHidden;
    }

    public Boolean isConstituentGridDisplayed()
    {
        return isConstituentGridDisplayed;
    }

    public Boolean isShotGridDisplayed()
    {
        return isShotGridDisplayed;
    }

    public void setIsCleared(Boolean isCleared)
    {
        this.isCleared = isCleared;
    }

    public Boolean isCleared()
    {
        return isCleared;
    }

    public Boolean isDataReloaded()
    {
        return isDataReloaded;
    }

    public void setIsDataReloaded(Boolean isDataReloaded)
    {
        this.isDataReloaded = isDataReloaded;
    }

    public Hd3dModelData getLastSimplyDisplayedWorkObject()
    {
        return this.simplyDisplayedWorkObject;
    }

    public void setProject(ProjectModelData project)
    {
        this.project = project;
    }

    public ProjectModelData getProject()
    {
        return project;
    }

    public void show(Hd3dModelData workObject)
    {
        this.simplyDisplayedWorkObject = workObject;
    }

    public void unmaskTaskGrid()
    {
        // TODO Auto-generated method stub

    }

    public void reloadData()
    {
        this.setIsDataReloaded(Boolean.TRUE);
    }

    public void show(Hd3dModelData workObject, ProjectModelData project)
    {
        this.simplyDisplayedWorkObject = workObject;
        this.setProject(project);
    }

    public void displaySimpleDialog(Hd3dModelData workObject)
    {
        this.simplyDisplayedWorkObject = workObject;
    }

    public List<TaskTypeModelData> getSelectedTaskTypes()
    {
        return this.selectedTaskTypes;
    }

    public void clearTaskTypeCombo()
    {
        // TODO Auto-generated method stub

    }

    public void clearAll()
    {
        this.setIsCleared(Boolean.TRUE);
    }

    public void showInfoWidgets()
    {
        this.isInfoWidgetsHidden = Boolean.TRUE;
    }

    public void hideInfoWidgets()
    {
        this.isInfoWidgetsHidden = Boolean.FALSE;
    }

    public void maximizeCastingTaskGrid()
    {
        this.isCastingTaskGridMaximized = Boolean.TRUE;
    }

    public void minimizeCastingTaskGrid()
    {
        this.isCastingTaskGridMaximized = Boolean.FALSE;
    }

    public void displayConstituentGrid()
    {
        this.isConstituentGridDisplayed = Boolean.TRUE;

    }

    public void displayShotGrid()
    {
        this.isShotGridDisplayed = Boolean.TRUE;
    }

}
