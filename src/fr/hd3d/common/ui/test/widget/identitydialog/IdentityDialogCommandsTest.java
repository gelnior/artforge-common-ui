package fr.hd3d.common.ui.test.widget.identitydialog;

import java.text.SimpleDateFormat;

import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.util.DateWrapper;

import fr.hd3d.common.ui.client.calendar.DatetimeUtil;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.WorkObjectModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteTypeModelData;
import fr.hd3d.common.ui.client.modeldata.production.CompositionModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.widget.identitydialog.IdentityDialogController;
import fr.hd3d.common.ui.client.widget.identitydialog.IdentityDialogEvents;
import fr.hd3d.common.ui.client.widget.identitydialog.IdentityDialogModel;
import fr.hd3d.common.ui.client.widget.identitydialog.command.CastingNotesLoadedCmd;
import fr.hd3d.common.ui.client.widget.identitydialog.command.CastingTasksLoadedCmd;
import fr.hd3d.common.ui.client.widget.identitydialog.command.CompositionDoubleClickedCmd;
import fr.hd3d.common.ui.client.widget.identitydialog.command.CompositionTaskTypeChangedCmd;
import fr.hd3d.common.ui.client.widget.identitydialog.command.DialogShowedCmd;
import fr.hd3d.common.ui.client.widget.identitydialog.command.MaximizeCastingTaskGridCmd;
import fr.hd3d.common.ui.client.widget.identitydialog.command.WorkObjectRefreshedCmd;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.common.ui.test.UITestCase;
import fr.hd3d.common.ui.test.util.TestUtils;
import fr.hd3d.common.ui.test.util.controller.ErrorController;
import fr.hd3d.common.ui.test.util.modeldata.writer.DataWriterMock;


/**
 * Test for identity dialog widget commands.
 * 
 * @author HD3D
 */
public class IdentityDialogCommandsTest extends UITestCase
{
    protected IdentityDialogModel model;
    protected IdentityDialogViewMock view;
    protected IdentityDialogController controller;

    protected ErrorController errorController = new ErrorController();

    @Override
    public void setUp()
    {
        super.setUp();

        this.model = new IdentityDialogModel();
        this.view = new IdentityDialogViewMock();
        this.controller = new IdentityDialogController(model, view);

        MainModel.setCurrentProject(TestUtils.getDefaultProject());
        MainModel.setCurrentUser(TestUtils.getDefaultPerson());

        DatetimeUtil.setDateWriter(new DataWriterMock());

        EventDispatcher.get().addController(errorController);
    }

    public void testDialogShowedCmd()
    {
        this.model.setCurrentWorkObject(TestUtils.getDefaultConstituent());

        DialogShowedCmd cmd = new DialogShowedCmd(this.model, this.view);

        this.model.getNoteStore().add(new ApprovalNoteModelData());
        this.model.getTaskStore().add(new TaskModelData());
        this.model.getConstituentStore().add(new ConstituentModelData());
        this.model.getShotStore().add(new ShotModelData());
        this.model.getCastingTaskStore().add(new TaskModelData());
        this.model.getCastingNoteStore().add(new ApprovalNoteModelData());

        cmd.execute(new AppEvent(IdentityDialogEvents.DIALOG_SHOWED));

        assertTrue(this.view.isCleared());
        assertEquals(0, this.model.getNoteStore().getCount());
        assertEquals(0, this.model.getTaskStore().getCount());
        assertEquals(0, this.model.getConstituentStore().getCount());
        assertEquals(0, this.model.getShotStore().getCount());
        assertEquals(0, this.model.getCastingTaskStore().getCount());
        assertEquals(0, this.model.getCastingNoteStore().getCount());

        assertTrue(this.view.isDataReloaded());

        assertEquals(TestUtils.getDefaultConstituent().getDefaultPath() + "/" + ServicesPath.TASKS, this.model
                .getTaskStore().getPath());
        assertEquals(TestUtils.getDefaultConstituent().getDefaultPath() + "/" + ServicesPath.SHOTS, this.model
                .getShotStore().getPath());
        assertEquals(TestUtils.getDefaultConstituent().getDefaultPath() + "/" + ServicesPath.APPROVAL_NOTES, this.model
                .getNoteStore().getPath());
    }

    public void testWorkObjectRefreshedCmd()
    {
        this.model.setCurrentWorkObject(TestUtils.getDefaultConstituent());
        this.model.setProject(TestUtils.getDefaultProject());
        ProjectModelData project = TestUtils.getDefaultProject();
        TaskTypeModelData taskType = TestUtils.getTaskType("Test-Modeling-project-info", "#F66");
        TaskTypeModelData taskType2 = TestUtils.getTaskType("Test-Layout-project-info", "#F66",
                ShotModelData.SIMPLE_CLASS_NAME);
        project.getTaskTypeIDs().add(taskType.getId());
        project.getTaskTypeIDs().add(taskType2.getId());
        project.save();

        this.model.getCurrentWorkObject().setDefaultPath(
                ServicesPath.PROJECTS + project.getId() + "/" + ServicesPath.CATEGORIES
                        + TestUtils.getDefaultConstituent().getCategory() + "/" + ServicesPath.CONSTITUENTS
                        + TestUtils.getDefaultConstituent().getId());
        this.model.updateTaskStoresConfiguration();
        CompositionModelData.getOrCreate(TestUtils.getDefaultConstituent(), TestUtils.getDefaultShot());

        TestUtils.getDefaultConstituent().setDefaultPath(
                ServicesPath.getOneSegmentPath(ServicesPath.CONSTITUENTS, TestUtils.getDefaultConstituent().getId()));
        this.model.updateTaskStoresConfiguration();

        WorkObjectRefreshedCmd cmd = new WorkObjectRefreshedCmd(this.model, this.view);
        cmd.execute(new AppEvent(IdentityDialogEvents.WORK_OBJECT_REFRESHED));

        assertEquals(ServicesPath.PROJECTS + TestUtils.getDefaultProject().getId() + "/" + ServicesPath.TASKTYPES,
                this.model.getTaskTypeStore().getPath());
        assertNotSame(0, this.model.getShotStore().getCount());
        assertEquals(TestUtils.getDefaultShot().getId().longValue(), this.model.getShotStore().getAt(0).getId()
                .longValue());
        assertEquals(1, this.model.getTaskTypeStore().getCount());
        assertEquals(taskType2.getId().longValue(), this.model.getTaskTypeStore().getAt(0).getId().longValue());

        project.getTaskTypeIDs().clear();
        project.save();
    }

    public void testCastingTasksLoadedCmd()
    {
        CastingTasksLoadedCmd cmd = new CastingTasksLoadedCmd(this.model, this.view);

        this.model.setCurrentWorkObject(TestUtils.getDefaultConstituent());
        this.model.getCastingTaskStore().add(TestUtils.getDefaultTask());

        TaskTypeModelData taskType = TestUtils.getTaskType("Test-Modeling");
        ApprovalNoteTypeModelData noteType = TestUtils.getApprovalType(taskType.getName(), TestUtils
                .getDefaultProject().getId(), taskType.getId());
        DateWrapper date = new DateWrapper();

        TestUtils.deleteApprovalForConstiuent(TestUtils.getDefaultConstituent());

        ApprovalNoteModelData approval1 = TestUtils.getApprovalNote(date.asDate(), TestUtils.getDefaultConstituent(),
                noteType);
        approval1.setTaskTypeId(taskType.getId());
        approval1.setComment("comment 1");
        approval1.setBoundEntityName(ConstituentModelData.SIMPLE_CLASS_NAME);
        approval1.save();

        ApprovalNoteModelData approval2 = TestUtils.getApprovalNote(date.addDays(-1).asDate(),
                TestUtils.getDefaultConstituent(), noteType);
        approval2.setTaskTypeId(taskType.getId());
        approval2.setComment("comment 2");
        approval2.setBoundEntityName(ConstituentModelData.SIMPLE_CLASS_NAME);
        approval2.save();

        this.model.getCastingNoteStore().setPath(
                TestUtils.getDefaultConstituent().getDefaultPath() + "/" + ServicesPath.APPROVAL_NOTES);
        cmd.execute(new AppEvent(IdentityDialogEvents.CASTING_TASKS_LOADED));

        assertEquals(2, this.model.getCastingNoteStore().getCount());
    }

    public void testCastingNotesLoadedCmd()
    {
        CastingNotesLoadedCmd cmd = new CastingNotesLoadedCmd(this.model, this.view);

        TaskTypeModelData taskType = TestUtils.getTaskType("Test-Modeling");
        ApprovalNoteTypeModelData noteType = TestUtils.getApprovalType(taskType.getName(), TestUtils
                .getDefaultProject().getId(), taskType.getId());

        DateWrapper date = new DateWrapper();
        ApprovalNoteModelData approval1 = TestUtils.getApprovalNote(date.asDate(), TestUtils.getDefaultConstituent(),
                noteType);
        approval1.setTaskTypeId(taskType.getId());
        approval1.setComment("comment 1");

        ApprovalNoteModelData approval2 = TestUtils.getApprovalNote(date.addDays(-1).asDate(),
                TestUtils.getDefaultConstituent(), noteType);
        approval2.setTaskTypeId(taskType.getId());
        approval2.setComment("comment 2");

        this.model.getCastingTaskStore().add(TestUtils.getDefaultTask());
        this.model.getCastingNoteStore().add(approval1);
        this.model.getCastingNoteStore().add(approval2);

        cmd.execute(new AppEvent(IdentityDialogEvents.CASTING_NOTES_LOADED));

        TaskModelData task = this.model.getCastingTaskStore().getAt(0);

        SimpleDateFormat dateFormatter = new SimpleDateFormat(fr.hd3d.common.client.DateFormat.DATE_TIME_STRING);
        String approvalDate = dateFormatter.format(approval1.getDate());

        assertEquals(approvalDate + ": " + approval1.getComment(), task.get(WorkObjectModelData.DESCRIPTION_FIELD));
    }

    public void testCompositionDoubleClickedCmd()
    {
        this.model.setCurrentWorkObject(TestUtils.getDefaultShot());

        CompositionDoubleClickedCmd cmd = new CompositionDoubleClickedCmd(this.model, this.view);

        cmd.execute(new AppEvent(IdentityDialogEvents.COMPOSITION_DBLE_CLICKED, TestUtils.getDefaultTask()));
        assertNull(this.view.getLastSimplyDisplayedWorkObject());

        this.model.getConstituentStore().add(TestUtils.getDefaultConstituent());
        cmd.execute(new AppEvent(IdentityDialogEvents.COMPOSITION_DBLE_CLICKED, TestUtils.getDefaultTask()));
        assertEquals(TestUtils.getDefaultConstituent(), this.view.getLastSimplyDisplayedWorkObject());
    }

    public void testCompositionTaskTypeChangedCmd()
    {
        this.model.setCurrentWorkObject(TestUtils.getDefaultConstituent());

        CompositionTaskTypeChangedCmd cmd = new CompositionTaskTypeChangedCmd(this.model, this.view);

        cmd.execute(new AppEvent(IdentityDialogEvents.COMPOSITION_TASK_TYPE_CHANGED));
        // assertTrue(false);
    }

    public void testMaximizeCastingTaskGridCmd()
    {
        MaximizeCastingTaskGridCmd cmd = new MaximizeCastingTaskGridCmd(this.model, this.view);

        cmd.execute(new AppEvent(IdentityDialogEvents.MAXIMIZE_CLICKED));

        assertTrue(this.model.isCastingTaskGridMaximized());
        assertTrue(this.view.isCastingTaskGridMaximized());
        assertFalse(this.view.isInfoWidgetsHidden());

        cmd.execute(new AppEvent(IdentityDialogEvents.MAXIMIZE_CLICKED));

        assertFalse(this.model.isCastingTaskGridMaximized());
        assertFalse(this.view.isCastingTaskGridMaximized());
        assertTrue(this.view.isInfoWidgetsHidden());
    }
}
