package fr.hd3d.common.ui.test.widget.identitypanel;

import org.junit.Test;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.service.ExcludedField;
import fr.hd3d.common.ui.client.service.ServicesModelType;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.widget.identitypanel.IdentityPanelController;
import fr.hd3d.common.ui.client.widget.identitypanel.IdentityPanelModel;
import fr.hd3d.common.ui.test.UITestCase;


public class IdentityControllerTest extends UITestCase
{
    IdentityPanelModel model;
    IdentityPanelMock view;
    IdentityPanelController controller;

    @Override
    public void setUp()
    {
        super.setUp();

        model = new IdentityPanelModel();
        view = new IdentityPanelMock();
        controller = new IdentityPanelController(model, view);

        EventDispatcher dispatcher = EventDispatcher.get();
        dispatcher.getControllers().clear();
        dispatcher.addController(controller);

        ServicesModelType.initModelTypes();
        ServicesPath.initPath();
        ExcludedField.initExcludedFields();
    }

    // @Test
    // public void testOnDeviceDataRetrieved()
    // {
    // this.model.setSimpleClassName("Device");
    // Hd3dModelData record = new DeviceModelData("device_identity_01", "serial_01", "reference_01", new Date(),
    // new Date(), 1.2D);
    //
    // List<String> computers = new ArrayList<String>();
    // computers.add("computer_01");
    // computers.add("computer_02");
    // computers.add("computer_03");
    // record.set(SoftwareModelData.COMPUTER_NAMES_FIELD, computers);
    //
    // AppEvent event = new AppEvent(IdentityPanelEvents.IDENTITY_RETRIEVED);
    // event.setData(IdentityPanelConfig.IDENTITY_EVENT_VAR_NAME, record);
    // EventDispatcher.forwardEvent(event);
    //
    // assertEquals(record.get(ComputerModelData.NAME_FIELD), this.view.getTitle());
    // assertEquals(6, this.model.getIdentityFieldsStore().getCount());
    // assertEquals(1, this.view.getRelationStores().size());
    // assertEquals(1, this.model.getRelationStores().size());
    // assertEquals(3, this.model.getRelationStore("computerNames").getCount());
    // assertTrue(!this.controller.isFirstSelection());
    // }
    //
    // @Test
    // public void testOnSoftwareDataRetrieved()
    // {
    // this.model.setSimpleClassName("Software");
    // Hd3dModelData record = new SoftwareModelData("soft_identity_01");
    //
    // List<String> computers = new ArrayList<String>();
    // computers.add("computer_01");
    // computers.add("computer_02");
    // computers.add("computer_03");
    // record.set(SoftwareModelData.COMPUTER_NAMES_FIELD, computers);
    //
    // AppEvent event = new AppEvent(IdentityPanelEvents.IDENTITY_RETRIEVED);
    // event.setData(IdentityPanelConfig.IDENTITY_EVENT_VAR_NAME, record);
    // EventDispatcher.forwardEvent(event);
    //
    // assertEquals(record.get(ComputerModelData.NAME_FIELD), this.view.getTitle());
    // assertEquals(6, this.model.getIdentityFieldsStore().getCount());
    // assertEquals(1, this.view.getRelationStores().size());
    // assertEquals(1, this.model.getRelationStores().size());
    // assertEquals(3, this.model.getRelationStore("computerNames").getCount());
    // assertTrue(!this.controller.isFirstSelection());
    // }
    //
    // @Test
    // public void testOnComputerDataRetrieved()
    // {
    // this.model.setSimpleClassName("Computer");
    // Hd3dModelData record = new ComputerModelData("comp_identity_01", "serial_01", "billing_01", new Date(),
    // new Date(), 123L, "fr.hd3d.laptop02", "127.0.0.1", "mac_adress", 1200L, 2L, 2L, 4096L, "NO_WORKER",
    // 160L, 35L);
    // List<String> devices = new ArrayList<String>();
    // devices.add("device_01");
    // devices.add("device_02");
    // devices.add("device_03");
    // record.set(ComputerModelData.DEVICE_NAMES_FIELD, devices);
    //
    // List<String> softwares = new ArrayList<String>();
    // softwares.add("software_01");
    // softwares.add("software_02");
    // softwares.add("software_03");
    // softwares.add("software_04");
    // record.set(ComputerModelData.SOFTWARE_NAMES_FIELD, softwares);
    //
    // AppEvent event = new AppEvent(IdentityPanelEvents.IDENTITY_RETRIEVED);
    // event.setData(IdentityPanelConfig.IDENTITY_EVENT_VAR_NAME, record);
    // EventDispatcher.forwardEvent(event);
    //
    // assertEquals(record.get(ComputerModelData.NAME_FIELD), this.view.getTitle());
    // assertEquals(12, this.model.getIdentityFieldsStore().getCount());
    // assertEquals(2, this.view.getRelationStores().size());
    // assertEquals(2, this.model.getRelationStores().size());
    // assertEquals(3, this.model.getRelationStore(ComputerModelData.DEVICE_NAMES_FIELD).getCount());
    // assertEquals(4, this.model.getRelationStore(ComputerModelData.SOFTWARE_NAMES_FIELD).getCount());
    // assertTrue(!this.controller.isFirstSelection());
    // }
    //
    // public void testOnNullComputerDataRetrieved()
    // {
    // this.model.setSimpleClassName("Computer");
    // Hd3dModelData record = new ComputerModelData(null, null, null, null, null, null, null, null, null, null, null,
    // null, null, null, null, null);
    // record.set(ComputerModelData.DEVICE_NAMES_FIELD, null);
    // record.set(ComputerModelData.SOFTWARE_NAMES_FIELD, null);
    //
    // AppEvent event = new AppEvent(IdentityPanelEvents.IDENTITY_RETRIEVED);
    // event.setData(IdentityPanelConfig.IDENTITY_EVENT_VAR_NAME, record);
    // EventDispatcher.forwardEvent(event);
    //
    // assertEquals("", this.view.getTitle());
    // assertEquals(12, this.model.getIdentityFieldsStore().getCount());
    // assertEquals(2, this.view.getRelationStores().size());
    // assertEquals(2, this.model.getRelationStores().size());
    // assertEquals(0, this.model.getRelationStore(ComputerModelData.DEVICE_NAMES_FIELD).getCount());
    // assertEquals(0, this.model.getRelationStore(ComputerModelData.SOFTWARE_NAMES_FIELD).getCount());
    // assertTrue(!this.controller.isFirstSelection());
    // }
    //
    // @Test
    // public void testOnDelete()
    // {
    // this.testOnDeviceDataRetrieved();
    //
    // EventDispatcher.forwardEvent(ExplorateurEvents.DELETE_ROW);
    // assertEquals("", this.view.getTitle());
    // assertEquals(0, this.model.getIdentityFieldsStore().getCount());
    // assertEquals(1, this.view.getRelationStores().size());
    // assertEquals(0, this.model.getRelationStore("computerNames").getCount());
    // assertEquals(1, this.model.getRelationStores().size());
    // assertTrue(!this.controller.isFirstSelection());
    // assertTrue(this.view.isCollapsed());
    // }
    //
    // @Test
    // public void testOnChangeView()
    // {
    // this.testOnDeviceDataRetrieved();
    //
    // EventDispatcher.forwardEvent(ExplorateurEvents.CHANGE_VIEW);
    // assertTrue(this.view.isCollapsed());
    // assertEquals("", this.view.getTitle());
    // assertEquals(0, this.model.getIdentityFieldsStore().getCount());
    // assertEquals(0, this.view.getRelationStores().size());
    // assertEquals(0, this.model.getRelationStores().size());
    // assertTrue(this.controller.isFirstSelection());
    // }
    //
    // @Test
    // public void testCollapse()
    // {
    // this.testOnDelete();
    // this.testOnDeviceDataRetrieved();
    // assertFalse(this.view.isCollapsed());
    // }

    @Test
    public void testRetrievedData()
    {
    // this.model.refreshIdentityData(43757L, "Computer");
    }
}
