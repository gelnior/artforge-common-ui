package fr.hd3d.common.ui.test.widget.identitypanel;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.extjs.gxt.ui.client.core.El;
import com.extjs.gxt.ui.client.store.ListStore;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.widget.identitypanel.IIdentityPanel;


public class IdentityPanelMock implements IIdentityPanel
{
    private String title = "";
    private boolean isMasked = false;
    private boolean isCollapsed = false;

    /** Stores needed by relation lists widgets. */
    private final Map<String, ListStore<RecordModelData>> relationStores = new HashMap<String, ListStore<RecordModelData>>();

    public String getTitle()
    {
        return title;
    }

    public boolean isMasked()
    {
        return this.isMasked;
    }

    public boolean isCollapsed()
    {
        return this.isCollapsed;
    }

    public Map<String, ListStore<RecordModelData>> getRelationStores()
    {
        return this.relationStores;
    }

    public void addRelationToIdentityPanel(String fieldName, ListStore<RecordModelData> recordNames)
    {
        this.relationStores.put(fieldName, recordNames);
    }

    public void removePanels()
    {
        this.relationStores.clear();
    }

    public void setIdentityTitle(String title)
    {
        this.title = title;
    }

    public void unmask()
    {
        this.isMasked = true;
    }

    public void collapseAll()
    {
        this.isCollapsed = true;
    }

    public void setEmptyTitle()
    {
        this.title = "";
    }

    public void unCollapseAll()
    {
        this.isCollapsed = false;
    }

    public El mask()
    {
        return null;
    }

    public String dateToString(Date value)
    {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormatter.format(value);
    }

    public void refreshLayout()
    {
    // TODO Auto-generated method stub

    }
}
