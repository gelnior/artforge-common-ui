package fr.hd3d.common.ui.test.widget.tageditor;

import org.junit.Test;

import com.extjs.gxt.ui.client.store.ListStore;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.technical.TagModelData;
import fr.hd3d.common.ui.client.widget.tageditor.TagEditorController;
import fr.hd3d.common.ui.client.widget.tageditor.TagEditorModel;
import fr.hd3d.common.ui.test.UITestCase;


public class TagEditorControllerTest extends UITestCase
{
    TagEditorModel model;
    TagEditorPanelMock view;
    TagEditorController controller;

    @Override
    public void setUp()
    {
        super.setUp();

        model = new TagEditorModel(new ListStore<TagModelData>());
        view = new TagEditorPanelMock();
        controller = new TagEditorController(model, view);

        EventDispatcher dispatcher = EventDispatcher.get();
        dispatcher.getControllers().clear();
        dispatcher.addController(controller);

    }

    @Test
    public void testMask()
    {}
}
