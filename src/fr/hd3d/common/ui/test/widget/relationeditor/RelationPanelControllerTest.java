package fr.hd3d.common.ui.test.widget.relationeditor;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.SoftwareModelData;
import fr.hd3d.common.ui.client.modeldata.reader.RecordReaderSingleton;
import fr.hd3d.common.ui.client.modeldata.writer.ModelDataJsonWriterSingleton;
import fr.hd3d.common.ui.client.service.ServicesModelType;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.widget.relationeditor.RelationConfig;
import fr.hd3d.common.ui.client.widget.relationeditor.RelationEvents;
import fr.hd3d.common.ui.client.widget.relationeditor.RelationPanelController;
import fr.hd3d.common.ui.client.widget.relationeditor.RelationPanelModel;
import fr.hd3d.common.ui.test.UITestCase;
import fr.hd3d.common.ui.test.util.modeldata.reader.RecordReaderMock;
import fr.hd3d.common.ui.test.util.modeldata.writer.ModelDataJsonWriterMock;


/**
 * Unit tests for relation panel editor.
 * 
 * @author HD3D
 */
public class RelationPanelControllerTest extends UITestCase
{
    RelationPanelModel model;
    RelationPanelMock view;
    RelationPanelController controller;

    @Override
    public void setUp()
    {
        super.setUp();

        RecordReaderSingleton.setInstanceAsMock(new RecordReaderMock());
        ModelDataJsonWriterSingleton.setInstanceAsMock(new ModelDataJsonWriterMock());

        model = new RelationPanelModel(ComputerModelData.SIMPLE_CLASS_NAME);
        view = new RelationPanelMock();
        controller = new RelationPanelController(model, view);

        EventDispatcher dispatcher = EventDispatcher.get();
        dispatcher.getControllers().clear();
        dispatcher.addController(controller);

        ServicesModelType.initModelTypes();
        ServicesPath.initPath();
    }

    @Test
    public void testOnPresetRetrieved()
    {

        SoftwareModelData software01 = new SoftwareModelData("soft_01");
        SoftwareModelData software02 = new SoftwareModelData("soft_02");
        SoftwareModelData software03 = new SoftwareModelData("soft_03");

        software01.setComputerIDs(new ArrayList<Long>());
        software02.setComputerIDs(new ArrayList<Long>());
        software03.setComputerIDs(new ArrayList<Long>());
        software01.setComputerNames(new ArrayList<String>());
        software02.setComputerNames(new ArrayList<String>());
        software03.setComputerNames(new ArrayList<String>());

        software01.getComputerIDs().add(1L);
        software01.getComputerNames().add("relation_01");
        software01.getComputerIDs().add(2L);
        software01.getComputerNames().add("relation_02");
        software01.getComputerIDs().add(3L);
        software01.getComputerNames().add("relation_03");
        software02.getComputerIDs().add(1L);
        software02.getComputerNames().add("relation_01");
        software02.getComputerIDs().add(3L);
        software02.getComputerNames().add("relation_03");

        List<SoftwareModelData> softwares = new ArrayList<SoftwareModelData>();
        softwares.add(software01);
        softwares.add(software02);
        softwares.add(software03);

        AppEvent event = new AppEvent(RelationEvents.PRESET_RETRIEVED);
        event.setData(RelationConfig.RELATION_PRESET_EVENT_VAR_NAME, softwares);
        EventDispatcher.forwardEvent(event);
        assertEquals(0, this.model.getSaveListStore().getCount());

        software03.getComputerIDs().add(1L);
        software03.getComputerNames().add("relation_01");
        EventDispatcher.forwardEvent(event);
        assertEquals(1, this.model.getSaveListStore().getCount());

        software03.getComputerIDs().add(2L);
        software03.getComputerNames().add("relation_02");
        EventDispatcher.forwardEvent(event);
        assertEquals(1, this.model.getSaveListStore().getCount());

        software02.getComputerIDs().add(2L);
        software02.getComputerNames().add("relation_02");
        software03.getComputerIDs().add(3L);
        software03.getComputerNames().add("relation_03");
        EventDispatcher.forwardEvent(event);
        assertEquals(3, this.model.getSaveListStore().getCount());
    }

    @Test
    public void testOnSaveSuccess()
    {
        this.view.showSaving();
        this.view.enableButtons();
        EventDispatcher.forwardEvent(RelationEvents.REMOVE_SELECTED);
        assertTrue(this.view.isButtonsEnabled());
        assertTrue(this.view.isSavingShown());
    }

    /**
     * Test that only save list store is cleared when clear button is clicked.
     */
    @Test
    public void testOnClearClicked()
    {
        this.model.getSaveListStore().add(new RecordModelData());
        this.model.getSaveListStore().add(new RecordModelData());
        this.model.getDataListStore().add(new RecordModelData());

        assertEquals(2, this.model.getSaveListStore().getCount());
        assertTrue(1 <= this.model.getDataListStore().getCount());
        EventDispatcher.forwardEvent(RelationEvents.REMOVE_SELECTED);
        assertEquals(0, this.model.getSaveListStore().getCount());
        assertTrue(1 <= this.model.getDataListStore().getCount());
    }

    /**
     * Test that when nothing is preset, the save list is empty.
     */
    @Test
    public void testOnRefreshClicked()
    {
        EventDispatcher.forwardEvent(RelationEvents.REFRESH_CLICKED);
        assertEquals(0, this.model.getSaveListStore().getCount());
    }

    @Test
    public void testOnSaveClicked()
    {
        SoftwareModelData software01 = new SoftwareModelData("soft_relation_01");
        SoftwareModelData software02 = new SoftwareModelData("soft_relation_02");
        ComputerModelData computer01 = new ComputerModelData("comp_relation_01");

        software01.save();
        software02.save();
        computer01.save();

        this.model.getSelection().add(software01);
        this.model.getSelection().add(software02);
        this.model.getSaveListStore().add(computer01);

        EventDispatcher.forwardEvent(RelationEvents.SAVE_CLICKED);
        software01.refresh();
        software02.refresh();

        assertEquals(1, software01.getComputerIDs().size());
        assertEquals(1, software02.getComputerIDs().size());
        assertEquals(computer01.getName(), software01.getComputerNames().get(0));
        assertEquals(computer01.getName(), software02.getComputerNames().get(0));

        software01.delete();
        software02.delete();
        computer01.delete();
    }

    @Test
    public void testOnFilterChanged()
    {
        ComputerModelData computer = new ComputerModelData("EEE-PC");
        computer.save();

        this.view.setFilterValue(computer.getName());
        EventDispatcher.forwardEvent(RelationEvents.FILTER_CHANGED);
        assertEquals(computer.getName() + "%", this.model.getFilter().getLeftMember());
        assertNotNull(this.model.getDataListStore().findModel(RecordModelData.NAME_FIELD, computer.getName()));
    }

    @Test
    public void testOnError()
    {
        this.view.showSaving();
        this.view.enableButtons();
        EventDispatcher.forwardEvent(RelationEvents.REMOVE_SELECTED);
        assertTrue(this.view.isButtonsEnabled());
        assertTrue(this.view.isSavingShown());
    }
}
