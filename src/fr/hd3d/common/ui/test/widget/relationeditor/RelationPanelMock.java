package fr.hd3d.common.ui.test.widget.relationeditor;

import fr.hd3d.common.ui.client.widget.relationeditor.IRelationPanel;


public class RelationPanelMock implements IRelationPanel
{

    private boolean isButtonsEnabled = true;
    private boolean isSavingShown = false;
    private String filterValue;

    public boolean isSavingShown()
    {
        return isSavingShown;
    }

    public boolean isButtonsEnabled()
    {
        return isButtonsEnabled;
    }

    public void disableButtons()
    {
        this.isButtonsEnabled = false;
    }

    public void enableButtons()
    {
        this.isButtonsEnabled = true;
    }

    public String getFilterValue()
    {
        return this.filterValue;
    }

    public void setFilterValue(String value)
    {
        this.filterValue = value;
    }

    public void hideSaving()
    {
        this.isSavingShown = false;
    }

    public void showSaving()
    {
        this.isSavingShown = true;
    }

    public void idle()
    {}

    public void unIdle()
    {}

    public void removeSaveListSelection()
    {}

}
