package fr.hd3d.common.ui.test.widget.relationeditor;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelData;
import fr.hd3d.common.ui.client.modeldata.reader.RecordReaderSingleton;
import fr.hd3d.common.ui.client.modeldata.writer.ModelDataJsonWriterSingleton;
import fr.hd3d.common.ui.client.service.ExcludedField;
import fr.hd3d.common.ui.client.service.ServicesModelType;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.userrights.PermissionUtil;
import fr.hd3d.common.ui.client.widget.relationeditor.RelationController;
import fr.hd3d.common.ui.client.widget.relationeditor.RelationEvents;
import fr.hd3d.common.ui.client.widget.relationeditor.RelationModel;
import fr.hd3d.common.ui.test.UITestCase;
import fr.hd3d.common.ui.test.util.modeldata.reader.RecordReaderMock;
import fr.hd3d.common.ui.test.util.modeldata.writer.ModelDataJsonWriterMock;


public class RelationControllerTest extends UITestCase
{
    RelationModel model;
    RelationDialogMock view;
    RelationController controller;

    @Override
    public void setUp()
    {
        super.setUp();

        RecordReaderSingleton.setInstanceAsMock(new RecordReaderMock());
        ModelDataJsonWriterSingleton.setInstanceAsMock(new ModelDataJsonWriterMock());

        model = new RelationModel();
        view = new RelationDialogMock();
        controller = new RelationController(model, view);

        EventDispatcher dispatcher = EventDispatcher.get();
        dispatcher.getControllers().clear();
        dispatcher.addController(controller);

        ServicesModelType.initModelTypes();
        ServicesPath.initPath();
        ExcludedField.initExcludedFields();

        PermissionUtil.setDebugOn();
    }

    @Test
    public void testOnShow()
    {
        ExcludedField.addExcludedField(ComputerModelData.RESOURCEGROUP_IDS_FIELD);
        ExcludedField.addExcludedField(ComputerModelData.RESOURCEGROUP_NAMES_FIELD);

        List<Hd3dModelData> computers = new ArrayList<Hd3dModelData>();
        ComputerModelData computer = new ComputerModelData("computer_relationdialog_01");
        computer.save();
        computers.add(computer);

        this.controller.show(computers, ComputerModelData.SIMPLE_CLASS_NAME);
        assertEquals(1, this.model.getSelection().size());
        assertEquals(ComputerModelData.SIMPLE_CLASS_NAME, this.model.getBoundClassName());
        assertEquals(ComputerModelData.getModelType().getFieldCount(), this.model.getModelType().getFieldCount());
        assertEquals(5, this.view.getTabs().size());
        assertEquals(5, this.model.getRelationTypes().size());
        assertEquals(1, this.model.getPresetRecords().size());

        computer.delete();
    }

    @Test
    public void testOnDialogClosed()
    {
        this.view.show();
        EventDispatcher.forwardEvent(RelationEvents.DIALOG_CLOSED);
        assertFalse(this.view.isShown());
        assertTrue(this.controller.isMasked());
    }

    @Test
    public void testOnSaveClicked()
    {
        this.view.enableTabHeaders();
        EventDispatcher.forwardEvent(RelationEvents.SAVE_CLICKED);
        assertFalse(this.view.isTabsEnabled());
    }

    @Test
    public void testOnSaveSucess()
    {
        this.view.disableTabHeaders();
        EventDispatcher.forwardEvent(RelationEvents.SAVE_SUCCESS);
        assertTrue(this.view.isTabsEnabled());
    }

    @Test
    public void testSaveAllClicked()
    {

    }

    @Test
    public void testSaveAllSuccess()
    {
        this.view.showSaving();
        this.view.disableButtons();
        this.view.disableTabHeaders();
        EventDispatcher.forwardEvent(RelationEvents.SAVE_ALL_SUCCESS);
        assertTrue(this.view.isButtonsEnabled());
        assertFalse(this.view.isSavingShown());
        assertTrue(this.view.isTabsEnabled());
    }

    @Test
    public void testOnError()
    {
        this.view.showSaving();
        this.view.disableButtons();
        this.view.disableTabHeaders();
        EventDispatcher.forwardEvent(CommonEvents.ERROR);
        assertTrue(this.view.isButtonsEnabled());
        assertFalse(this.view.isSavingShown());
        assertTrue(this.view.isTabsEnabled());
    }
}
