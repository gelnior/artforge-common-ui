package fr.hd3d.common.ui.test.modeldata.playlist.math;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

import fr.hd3d.common.ui.client.modeldata.playlist.PlayListEditModelData;
import fr.hd3d.common.ui.client.modeldata.playlist.math.FrameRange;
import fr.hd3d.common.ui.client.modeldata.playlist.math.PlaylistUtils;


public class PlaylistUtilsTest
{
    private PlayListEditModelData createEdit(int srcIn, int srcOut, int recIn, int recOut)
    {
        final PlayListEditModelData edit = new PlayListEditModelData();
        edit.setSrcIn(srcIn);
        edit.setSrcOut(srcOut);
        edit.setRecIn(recIn);
        edit.setRecOut(recOut);
        return edit;
    }

    private List<PlayListEditModelData> createTestPlayList()
    {
        final ArrayList<PlayListEditModelData> pl = new ArrayList<PlayListEditModelData>(5);
        pl.add(createEdit(5, 10, 0, 5));
        pl.add(createEdit(5, 15, 5, 15));
        pl.add(createEdit(10, 30, 20, 40));
        pl.add(createEdit(10, 20, 40, 50));
        pl.add(createEdit(10, 20, 50, 60));
        return pl;
    }

    private void assertEditEquals(int srcIn, int srcOut, int recIn, int recOut, PlayListEditModelData edit)
    {
        Assert.assertEquals(new FrameRange(srcIn, srcOut), edit.getSrcRange());
        Assert.assertEquals(new FrameRange(recIn, recOut), edit.getRecRange());
        Assert.assertEquals(srcIn, (int) edit.getSrcIn());
        Assert.assertEquals(srcOut, (int) edit.getSrcOut());
        Assert.assertEquals(recIn, (int) edit.getRecIn());
        Assert.assertEquals(recOut, (int) edit.getRecOut());
    }

    // slip tests
    @Test
    public void slipEdit()
    {
        final PlayListEditModelData edit = createEdit(0, 10, 0, 10);
        Assert.assertTrue(PlaylistUtils.slipSrc(edit, 10));
        assertEditEquals(10, 20, 0, 10, edit);
    }

    @Test
    public void slipEditWithBadOffset()
    {
        final PlayListEditModelData edit = createEdit(0, 10, 0, 10);
        Assert.assertFalse(PlaylistUtils.slipSrc(edit, -1));
        assertEditEquals(0, 10, 0, 10, edit);
    }

    // trim single tests
    @Test
    public void trimInSingleTest()
    {
        final PlayListEditModelData edit = createEdit(0, 10, 0, 10);
        Assert.assertTrue(PlaylistUtils.trimIn(edit, 5));
        assertEditEquals(5, 10, 5, 10, edit);
    }

    @Test
    public void trimInSingleOutOfRangeTest()
    {
        final PlayListEditModelData edit = createEdit(0, 10, 0, 10);
        Assert.assertFalse(PlaylistUtils.trimIn(edit, 15));
        assertEditEquals(0, 10, 0, 10, edit);
    }

    @Test
    public void trimOutSingleTest()
    {
        final PlayListEditModelData edit = createEdit(0, 10, 0, 10);
        Assert.assertTrue(PlaylistUtils.trimOut(edit, 5));
        assertEditEquals(0, 15, 0, 15, edit);
    }

    @Test
    public void trimOutSingleOutOfRangeTest()
    {
        final PlayListEditModelData edit = createEdit(0, 10, 0, 10);
        Assert.assertFalse(PlaylistUtils.trimOut(edit, -15));
        assertEditEquals(0, 10, 0, 10, edit);
    }

    // trim in in playlist
    // pl.add(createEdit(5, 10, 0, 5));
    // pl.add(createEdit(5, 15, 5, 15));
    // pl.add(createEdit(10, 30, 20, 40));
    // pl.add(createEdit(10, 20, 40, 50));
    // pl.add(createEdit(10, 20, 50, 60));
    @Test
    public void trimIn()
    {
        final List<PlayListEditModelData> playlist = createTestPlayList();
        final PlayListEditModelData editToTrim = playlist.get(2);
        PlayListEditModelData editBefore = playlist.get(1);
        Assert.assertTrue(PlaylistUtils.trimIn(editToTrim, playlist, -6));
        assertEditEquals(4, 30, 14, 40, editToTrim);
        assertEditEquals(5, 14, 5, 14, editBefore);
    }

    @Test
    public void trimOut()
    {
        final List<PlayListEditModelData> playlist = createTestPlayList();
        final PlayListEditModelData editToTrim = playlist.get(1);
        Assert.assertTrue(PlaylistUtils.trimOut(editToTrim, playlist, 6));
        assertEditEquals(5, 10, 0, 5, playlist.get(0));
        assertEditEquals(5, 21, 5, 21, editToTrim);
        assertEditEquals(10, 30, 21, 41, playlist.get(2));
        assertEditEquals(10, 20, 41, 51, playlist.get(3));
        assertEditEquals(10, 20, 51, 61, playlist.get(4));
    }

}
