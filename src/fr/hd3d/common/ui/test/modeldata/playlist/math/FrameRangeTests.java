package fr.hd3d.common.ui.test.modeldata.playlist.math;

import junit.framework.Assert;

import org.junit.Test;

import fr.hd3d.common.ui.client.modeldata.playlist.math.FrameRange;


public class FrameRangeTests
{
    @Test
    public void testConstructor()
    {
        final FrameRange range = new FrameRange(0, 10);
        Assert.assertEquals(range.frameIn, 0);
        Assert.assertEquals(range.frameOut, 10);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testBadConstructor()
    {
        new FrameRange(10, 4);
    }

    @Test
    public void testEquals()
    {
        final FrameRange range = new FrameRange(0, 10);
        final FrameRange otherRange = new FrameRange(0, 10);
        Assert.assertEquals(range, otherRange);
    }

    @Test
    public void testLength()
    {
        final FrameRange range = new FrameRange(0, 10);
        Assert.assertEquals(range.getLength(), 10);
    }

    @Test
    public void testOneFrameLength()
    {
        final FrameRange range = new FrameRange(9, 10);
        Assert.assertEquals(range.getLength(), 1);
    }

    @Test
    public void testRangeBefore()
    {
        final FrameRange rangeBefore = new FrameRange(0, 10);
        final FrameRange rangeAfter = new FrameRange(10, 20);
        Assert.assertTrue(rangeBefore.compareTo(rangeAfter) < 0);
    }

    @Test
    public void testRangeAfter()
    {
        final FrameRange rangeBefore = new FrameRange(0, 10);
        final FrameRange rangeAfter = new FrameRange(10, 20);
        Assert.assertTrue(rangeAfter.compareTo(rangeBefore) > 0);
    }

    @Test
    public void testCompareToRangeBefore()
    {
        final FrameRange rangeBefore = new FrameRange(0, 10);
        final FrameRange rangeAfter = new FrameRange(0, 20);
        Assert.assertTrue(rangeBefore.compareTo(rangeAfter) < 0);
    }

    @Test
    public void testCompareToRangeAfter()
    {
        final FrameRange rangeBefore = new FrameRange(0, 10);
        final FrameRange rangeAfter = new FrameRange(0, 20);
        Assert.assertTrue(rangeAfter.compareTo(rangeBefore) > 0);
    }

    @Test
    public void testCompareToRangeEquals()
    {
        final FrameRange range = new FrameRange(0, 10);
        Assert.assertTrue(range.compareTo(range) == 0);
    }

    @Test
    public void testCompareToRangeOverlaps()
    {
        final FrameRange smallRange = new FrameRange(10, 20);
        final FrameRange bigRange = new FrameRange(15, 40);
        Assert.assertTrue(smallRange.compareTo(bigRange) < 0);
    }

    @Test
    public void testIsContiguous()
    {
        final FrameRange aRange = new FrameRange(0, 10);
        final FrameRange otherRange = new FrameRange(10, 20);
        Assert.assertTrue(aRange.isContiguous(otherRange));
        Assert.assertTrue(otherRange.isContiguous(aRange));
    }

    @Test
    public void testIsOverlapping1()
    {
        final FrameRange smallRange = new FrameRange(10, 20);
        final FrameRange bigRange = new FrameRange(5, 40);
        Assert.assertTrue(smallRange.isOverlapping(bigRange));
        Assert.assertTrue(bigRange.isOverlapping(smallRange));
    }

    @Test
    public void testIsOverlapping2()
    {
        final FrameRange smallRange = new FrameRange(10, 20);
        final FrameRange bigRange = new FrameRange(15, 40);
        Assert.assertTrue(smallRange.isOverlapping(bigRange));
        Assert.assertTrue(bigRange.isOverlapping(smallRange));
    }

    @Test
    public void hasGap()
    {
        final FrameRange smallRange = new FrameRange(10, 20);
        final FrameRange bigRange = new FrameRange(25, 40);
        Assert.assertEquals(5, smallRange.getGap(bigRange));
        Assert.assertEquals(-5, bigRange.getGap(smallRange));
    }

    @Test
    public void hasOverlappingGap()
    {
        final FrameRange smallRange = new FrameRange(10, 20);
        final FrameRange bigRange = new FrameRange(15, 40);
        Assert.assertEquals(0, smallRange.getGap(bigRange));
        Assert.assertEquals(0, bigRange.getGap(smallRange));
    }
}
