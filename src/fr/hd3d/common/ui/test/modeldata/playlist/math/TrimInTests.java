/**
 * 
 */
package fr.hd3d.common.ui.test.modeldata.playlist.math;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

import fr.hd3d.common.ui.client.modeldata.playlist.math.EModificationMode;
import fr.hd3d.common.ui.client.modeldata.playlist.math.EditingMathUtils;
import fr.hd3d.common.ui.client.modeldata.playlist.math.FrameRange;
import fr.hd3d.common.ui.client.modeldata.playlist.math.FrameRangeUtils;


/**
 * @author paj
 * 
 */
public class TrimInTests
{
    /**
     * creates a frame range test list with no gaps between ranges. Creates the following ordered list : 10 20 40 50 60
     * |--R1---|------R2-------|--R3---|----R4--|
     * 
     * @return a List of FrameRange
     */
    private static List<FrameRange> getTestList()
    {
        List<FrameRange> list = new ArrayList<FrameRange>();
        final FrameRange r1 = new FrameRange(10, 20);
        final FrameRange r2 = new FrameRange(20, 40);
        final FrameRange r3 = new FrameRange(40, 50);
        final FrameRange r4 = new FrameRange(50, 60);
        list.add(r1);
        list.add(r2);
        list.add(r3);
        list.add(r4);
        return list;
    }

    /**
     * creates a frame range test list with one gap between ranges 2 and 3. Creates the following ordered list : 10 20
     * 35 40 50 60 |--R1---|------R2-----| |--R3---|----R4--|
     * 
     * @return FrameRange
     */
    private static List<FrameRange> getTestListWithGap()
    {
        List<FrameRange> list = new ArrayList<FrameRange>();
        final FrameRange r1 = new FrameRange(10, 20);
        final FrameRange r2 = new FrameRange(20, 35);
        final FrameRange r3 = new FrameRange(40, 50);
        final FrameRange r4 = new FrameRange(50, 60);
        list.add(r1);
        list.add(r2);
        list.add(r3);
        list.add(r4);
        return list;
    }

    /*
     * Basic tests
     */
    @Test
    public void testIsSorted()
    {
        final List<FrameRange> sortedList = getTestList();
        Assert.assertTrue(FrameRangeUtils.isRangeSorted(sortedList));
        final List<FrameRange> unsortedList = new ArrayList<FrameRange>();
        unsortedList.add(new FrameRange(10, 20));
        unsortedList.add(new FrameRange(0, 5));
        Assert.assertTrue(FrameRangeUtils.isRangeSorted(unsortedList) == false);
    }

    public void trimIn(final List<FrameRange> list, int offset, EModificationMode mode)
    {
        trimIn(list, offset, mode, true);
    }

    public void trimIn(final List<FrameRange> list, int offset, EModificationMode mode, boolean assertTrue)
    {
        System.out.println("list before :" + list);
        if (assertTrue)
        {
            Assert.assertTrue(EditingMathUtils.trimIn(list, list.get(2), offset, mode));
        }
        else
        {
            Assert.assertTrue(EditingMathUtils.trimIn(list, list.get(2), offset, mode) == false);
        }
        System.out.println("list after :" + list);
    }

    @Test
    public void trimWithNonExistingRange()
    {
        List<FrameRange> testList = EditingMathUtilsTest.getTestList();
        Assert
                .assertTrue(EditingMathUtils.trimIn(testList, new FrameRange(20, 40), 10, EModificationMode.INSERT) == false);
        EditingMathUtilsTest.assertRangesEquals(testList, EditingMathUtilsTest.getTestList());
        // overwrite
        testList = EditingMathUtilsTest.getTestList();
        Assert
                .assertTrue(EditingMathUtils.trimIn(testList, new FrameRange(20, 40), 10, EModificationMode.OVERWRITE) == false);
        EditingMathUtilsTest.assertRangesEquals(testList, EditingMathUtilsTest.getTestList());
    }

    /*
     * frame range list L1 tests
     */
    /**
     * performs a trim in on the r3 range, with a 5 offset, and with INSERT mode
     */
    @Test
    public void trimR3WithInsertMode()
    {
        System.out.println("trimInR3WithInsertMode :");
        final List<FrameRange> list = getTestList();
        trimIn(list, 5, EModificationMode.INSERT);
        Assert.assertEquals(new FrameRange(10, 20), list.get(0));
        Assert.assertEquals(new FrameRange(20, 40), list.get(1));
        Assert.assertEquals(new FrameRange(45, 50), list.get(2));
        Assert.assertEquals(new FrameRange(50, 60), list.get(3));
    }

    /**
     * performs a trim in on the r3 range, with a 5 offset, and with OVERWRITE mode
     */
    @Test
    public void trimR3WithOverwriteMode()
    {
        System.out.println("trimInR3WithOverwriteMode :");
        final List<FrameRange> list = getTestList();
        trimIn(list, 5, EModificationMode.OVERWRITE);
        Assert.assertEquals(new FrameRange(10, 20), list.get(0));
        Assert.assertEquals(new FrameRange(20, 45), list.get(1));
        Assert.assertEquals(new FrameRange(45, 50), list.get(2));
        Assert.assertEquals(new FrameRange(50, 60), list.get(3));
    }

    /**
     * performs a trim in on the r3 range, with a -5 offset, and with INSERT mode
     */
    @Test
    public void negativeTrimR3WithInsertMode()
    {
        System.out.println("negativeTrimR3WithInsertMode :");
        final List<FrameRange> list = getTestList();
        trimIn(list, -5, EModificationMode.INSERT);
        Assert.assertEquals(new FrameRange(5, 15), list.get(0));
        Assert.assertEquals(new FrameRange(15, 35), list.get(1));
        Assert.assertEquals(new FrameRange(35, 50), list.get(2));
        Assert.assertEquals(new FrameRange(50, 60), list.get(3));
    }

    /**
     * performs a trim in on the r3 range, with a -5 offset, and with OVERWRITE mode
     */
    @Test
    public void negativeTrimR3WithOverwriteMode()
    {
        System.out.println("negativeTrimR3WithOverwriteMode :");
        final List<FrameRange> list = getTestList();
        trimIn(list, -5, EModificationMode.OVERWRITE);
        Assert.assertEquals(new FrameRange(10, 20), list.get(0));
        Assert.assertEquals(new FrameRange(20, 35), list.get(1));
        Assert.assertEquals(new FrameRange(35, 50), list.get(2));
        Assert.assertEquals(new FrameRange(50, 60), list.get(3));
    }

    /**
     * performs a trim in on the r3 range, with a +11 offset, and with INSERT mode
     */
    @Test
    public void badTrimR3WithInsertMode()
    {
        System.out.println("badTrimR3WithInsertMode :");
        final List<FrameRange> list = getTestList();
        trimIn(list, 11, EModificationMode.INSERT, false);
        Assert.assertEquals(new FrameRange(10, 20), list.get(0));
        Assert.assertEquals(new FrameRange(20, 40), list.get(1));
        Assert.assertEquals(new FrameRange(40, 50), list.get(2));
        Assert.assertEquals(new FrameRange(50, 60), list.get(3));
    }

    /**
     * performs a negative trim in on the r3 range, with a -11 offset, and with INSERT mode
     */
    @Test
    public void badNegativeTrimR3WithInsertMode()
    {
        System.out.println("badNegativeTrimR3WithInsertMode :");
        final List<FrameRange> list = getTestList();
        trimIn(list, -11, EModificationMode.INSERT, false);
        Assert.assertEquals(new FrameRange(10, 20), list.get(0));
        Assert.assertEquals(new FrameRange(20, 40), list.get(1));
        Assert.assertEquals(new FrameRange(40, 50), list.get(2));
        Assert.assertEquals(new FrameRange(50, 60), list.get(3));
    }

    /**
     * performs a trim in on the r3 range, with a +11 offset, and with OVERWRITE mode
     */
    @Test
    public void badTrimR3WithOverwriteMode()
    {
        System.out.println("badTrimR3WithInsertMode :");
        final List<FrameRange> list = getTestList();
        trimIn(list, 11, EModificationMode.OVERWRITE, false);
        Assert.assertEquals(new FrameRange(10, 20), list.get(0));
        Assert.assertEquals(new FrameRange(20, 40), list.get(1));
        Assert.assertEquals(new FrameRange(40, 50), list.get(2));
        Assert.assertEquals(new FrameRange(50, 60), list.get(3));
    }

    /**
     * performs a trim in on the r3 range, with a -25 offset, and with OVERWRITE mode
     */
    @Test
    public void badNegativeTrimR3WithOverwriteMode()
    {
        System.out.println("badTrimR3WithInsertMode :");
        final List<FrameRange> list = getTestList();
        trimIn(list, -21, EModificationMode.OVERWRITE, false);
        Assert.assertEquals(new FrameRange(10, 20), list.get(0));
        Assert.assertEquals(new FrameRange(20, 40), list.get(1));
        Assert.assertEquals(new FrameRange(40, 50), list.get(2));
        Assert.assertEquals(new FrameRange(50, 60), list.get(3));
    }

    /*
     * TRIM with the second list
     */
    /**
     * performs a trimIn on the R3 range, il the second list with a -5 offset, and with INSERT MODE
     */
    @Test
    public void testTrimInR3InList2WithInsertMode()
    {
        System.out.println("testTrimInR3InList2WithInsertMode :");
        final List<FrameRange> list = getTestListWithGap();
        trimIn(list, -5, EModificationMode.INSERT);
        Assert.assertEquals(new FrameRange(10, 20), list.get(0));
        Assert.assertEquals(new FrameRange(20, 35), list.get(1));
        Assert.assertEquals(new FrameRange(35, 50), list.get(2));
        Assert.assertEquals(new FrameRange(50, 60), list.get(3));
    }

    /**
     * performs a trimIn on the R3 range, il the second list with a -10 offset, and with INSERT MODE
     */
    @Test
    public void testTrimInR3InList2WithInsertMode_2()
    {
        System.out.println("testTrimInR3InList2WithInsertMode_2 :");
        final List<FrameRange> list = getTestListWithGap();
        trimIn(list, -10, EModificationMode.INSERT);
        Assert.assertEquals(new FrameRange(5, 15), list.get(0));
        Assert.assertEquals(new FrameRange(15, 30), list.get(1));
        Assert.assertEquals(new FrameRange(30, 50), list.get(2));
        Assert.assertEquals(new FrameRange(50, 60), list.get(3));
    }

    /**
     * performs a trimIn on the R3 range, il the second list with a -5 offset, and with OVERWRITE MODE
     */
    @Test
    public void testNegativeTrimInR3InList2WithOverwriteMode()
    {
        System.out.println("testNegativeTrimInR3InList2WithOverwriteMode :");
        final List<FrameRange> list = getTestListWithGap();
        trimIn(list, -5, EModificationMode.OVERWRITE);
        Assert.assertEquals(new FrameRange(10, 20), list.get(0));
        Assert.assertEquals(new FrameRange(20, 35), list.get(1));
        Assert.assertEquals(new FrameRange(35, 50), list.get(2));
        Assert.assertEquals(new FrameRange(50, 60), list.get(3));
    }

    /**
     * performs a trimIn on the R3 range, il the second list with a -10 offset, and with OVERWRITE MODE
     */
    @Test
    public void testNegativeTrimInR3InList2WithOverwriteMode_2()
    {
        System.out.println("testNegativeTrimInR3InList2WithOverwriteMode_2 :");
        final List<FrameRange> list = getTestListWithGap();
        trimIn(list, -10, EModificationMode.OVERWRITE);
        Assert.assertEquals(new FrameRange(10, 20), list.get(0));
        Assert.assertEquals(new FrameRange(20, 30), list.get(1));
        Assert.assertEquals(new FrameRange(30, 50), list.get(2));
        Assert.assertEquals(new FrameRange(50, 60), list.get(3));
    }

    /**
     * performs a trimIn on the R3 range, il the second list with a 5 offset, and with OVERWRITE MODE
     */
    @Test
    public void testTrimInR3InList2WithOverwriteMode()
    {
        System.out.println("testTrimInR3InList2WithOverwriteMode :");
        final List<FrameRange> list = getTestListWithGap();
        trimIn(list, 5, EModificationMode.OVERWRITE);
        Assert.assertEquals(new FrameRange(10, 20), list.get(0));
        Assert.assertEquals(new FrameRange(20, 35), list.get(1));
        Assert.assertEquals(new FrameRange(45, 50), list.get(2));
        Assert.assertEquals(new FrameRange(50, 60), list.get(3));
    }
}
