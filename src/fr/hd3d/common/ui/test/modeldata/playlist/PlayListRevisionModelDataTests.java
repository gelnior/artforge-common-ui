package fr.hd3d.common.ui.test.modeldata.playlist;

import org.junit.Test;

import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListRevisionModelData;
import fr.hd3d.common.ui.test.modeldata.ModelDataTest;
import fr.hd3d.common.ui.test.util.modeldata.reader.PlayListRevisionModelDataReaderMock;


public class PlayListRevisionModelDataTests extends
        ModelDataTest<PlayListRevisionModelData, PlayListRevisionModelDataReaderMock>
{

    @Override
    public void setUp()
    {
        super.setUp();

        this.reader = new PlayListRevisionModelDataReaderMock();
    }

    @Test
    public void testCreatePlayList()
    {
        PlayListRevisionModelData playlist = new PlayListRevisionModelData("playlist");
        PlayListRevisionModelData savedPlaylist = this.saveObject(playlist);
        assertEquals(playlist.getName(), savedPlaylist.getName());
        playlist.delete();
    }

    @Test
    public void testUpdatePlaylist()
    {
        PlayListRevisionModelData playlist = new PlayListRevisionModelData("playlist");
        playlist.save();
        playlist.setName("playlist-updated");
        playlist.save();
        PlayListRevisionModelData updtPlaylist = new PlayListRevisionModelData();
        updtPlaylist.setId(playlist.getId());
        updtPlaylist.refresh();
        assertEquals(playlist.getName(), updtPlaylist.getName());
        playlist.delete();
    }

    @Test
    public void testDeletePlaylist()
    {
        PlayListRevisionModelData playlist = new PlayListRevisionModelData("playlist");
        playlist.save();
        playlist.delete();
        playlist.refresh();
        assertEquals(CommonErrors.RECORD_NOT_ON_SERVER, controller.getLastError());

    }

}
