package fr.hd3d.common.ui.test.modeldata.playlist.math;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import fr.hd3d.common.ui.client.modeldata.playlist.math.EModificationMode;
import fr.hd3d.common.ui.client.modeldata.playlist.math.EditingMathUtils;
import fr.hd3d.common.ui.client.modeldata.playlist.math.FrameRange;
import fr.hd3d.common.ui.client.modeldata.playlist.math.FrameRangeUtils;


public class EditingMathUtilsTest
{
    /**
     * creates a frame range test list with no gaps between ranges. Creates the following ordered list : 10 20 40 50 60
     * |--R1---|------R2-------|--R3---|----R4--|
     * 
     * @return a List of FrameRange
     */
    public static List<FrameRange> getTestList()
    {
        List<FrameRange> list = new ArrayList<FrameRange>();
        list.add(new FrameRange(10, 20));
        list.add(new FrameRange(20, 40));
        list.add(new FrameRange(40, 50));
        list.add(new FrameRange(50, 60));
        return list;
    }

    /**
     * creates a frame range test list with one gap between ranges 2 and 3. Creates the following ordered list : 10 20
     * 40 50 55 65 |--R1---|------R2-------|--R3---| |----R4--|
     * 
     * @return FrameRange
     */
    public static List<FrameRange> getTestListWithGap()
    {
        List<FrameRange> list = new ArrayList<FrameRange>();
        list.add(new FrameRange(10, 20));
        list.add(new FrameRange(20, 35));
        list.add(new FrameRange(40, 50));
        list.add(new FrameRange(55, 65));
        return list;
    }

    public static void assertRangesEquals(List<FrameRange> modifiedFrameRanges, List<FrameRange> expectedRanges)
    {
        if (modifiedFrameRanges.size() != expectedRanges.size())
        {
            Assert.fail();
        }
        for (int rangeIndex = 0; rangeIndex < expectedRanges.size(); rangeIndex++)
        {
            Assert.assertEquals(expectedRanges.get(rangeIndex), modifiedFrameRanges.get(rangeIndex));
        }
    }

    public static void assertRangesEquals(List<FrameRange> modifiedFrameRanges, FrameRange... expectedRanges)
    {
        final List<FrameRange> expectedRangesList = Arrays.asList(expectedRanges);
        assertRangesEquals(modifiedFrameRanges, expectedRangesList);
    }

    public static void assertRangesBeforeEquals(List<FrameRange> modifiedFrameRanges, int modifiedRangeIndex,
            FrameRange... expectedRangesBefore)
    {
        final List<FrameRange> expectedRanges = new ArrayList<FrameRange>(modifiedFrameRanges.size());
        expectedRanges.addAll(Arrays.asList(expectedRangesBefore));
        expectedRanges.addAll(modifiedFrameRanges.subList(modifiedRangeIndex, modifiedFrameRanges.size()));
        assertRangesEquals(modifiedFrameRanges, expectedRanges);
    }

    public static void assertRangesAfterEquals(List<FrameRange> modifiedFrameRanges, int modifiedRangeIndex,
            FrameRange... expectedRangesAfter)
    {
        final List<FrameRange> expectedRanges = new ArrayList<FrameRange>(modifiedFrameRanges.subList(0,
                modifiedRangeIndex + 1));
        expectedRanges.addAll(Arrays.asList(expectedRangesAfter));
        assertRangesEquals(modifiedFrameRanges, expectedRanges);
    }

    public static void assertTrimmedItemEquals(List<FrameRange> modifiedFrameRanges, int modifiedRangeIndex,
            FrameRange expectedRange)
    {
        final FrameRange trimmedRange = modifiedFrameRanges.get(modifiedRangeIndex);
        Assert.assertEquals(expectedRange, trimmedRange);
    }

    @Test
    public void testAssertRangeEquals()
    {
        EditingMathUtilsTest.assertRangesEquals(getTestList(), getTestList());
    }

    @Test
    public void testRangeBeforeEquals()
    {
        EditingMathUtilsTest.assertRangesBeforeEquals(getTestList(), 2, getTestList().get(0), getTestList().get(1));
    }

    @Test
    public void testRangeAfterEquals()
    {
        EditingMathUtilsTest.assertRangesAfterEquals(getTestList(), 2, getTestList().get(3));
    }

    @Test
    public void testRangesBetween()
    {
        List<FrameRange> sortedRanges = getTestListWithGap();
        List<FrameRange> rangesAt = FrameRangeUtils.rangesBetween(15, 20, sortedRanges);
        assertRangesEquals(rangesAt, new FrameRange(10, 20));
        rangesAt = FrameRangeUtils.rangesBetween(15, 40, sortedRanges);
        assertRangesEquals(rangesAt, new FrameRange(10, 20), new FrameRange(20, 35));
        rangesAt = FrameRangeUtils.rangesBetween(15, 45, sortedRanges);
        assertRangesEquals(rangesAt, new FrameRange(10, 20), new FrameRange(20, 35), new FrameRange(40, 50));
        rangesAt = FrameRangeUtils.rangesBetween(65, 75, sortedRanges);
        Assert.assertEquals(0, rangesAt.size());
        rangesAt = FrameRangeUtils.rangesBetween(0, 5, sortedRanges);
        Assert.assertEquals(0, rangesAt.size());
        rangesAt = FrameRangeUtils.rangesBetween(35, 40, sortedRanges);
        Assert.assertEquals(0, rangesAt.size());
        rangesAt = FrameRangeUtils.rangesBetween(36, 39, sortedRanges);
        Assert.assertEquals(0, rangesAt.size());
    }

    /*
     * test utils
     */
    @Test
    public void testGuessOffset()
    {
        final FrameRange rangeOne = new FrameRange(0, 10);
        final FrameRange rangeTwo = new FrameRange(20, 30);
        final FrameRange rangeThree = new FrameRange(20, 25);
        Assert.assertEquals(-20, FrameRangeUtils.guessOffset(rangeOne, rangeTwo));
        Assert.assertEquals(20, FrameRangeUtils.guessOffset(rangeTwo, rangeOne));
        Assert.assertEquals(0, FrameRangeUtils.guessOffset(rangeOne, rangeThree));
    }

    /*
     * 
     * SLIP TESTS
     */
    @Test(expected = NullPointerException.class)
    public void testNullRange()
    {
        EditingMathUtils.slip((FrameRange) null, 0);
        EditingMathUtils.slipToFrameIn(null, 0);
        EditingMathUtils.slipToFrameOut(null, 0);
    }

    @Test
    public void testSlipDoesNotAffectsLength()
    {
        final FrameRange rangeToSlip = new FrameRange(10, 20);
        final int expectedLength = rangeToSlip.getLength();
        Assert.assertEquals(expectedLength, rangeToSlip.getLength());
        Assert.assertTrue(EditingMathUtils.slip(rangeToSlip, 10));
        Assert.assertEquals(expectedLength, rangeToSlip.getLength());
    }

    @Test
    public void testSlip()
    {
        final FrameRange rangeToSlip = new FrameRange(10, 20);
        final FrameRange expectedSlippedRange = new FrameRange(15, 25);
        final int offset = 5;
        Assert.assertTrue(EditingMathUtils.slip(rangeToSlip, offset));
        Assert.assertEquals(expectedSlippedRange, rangeToSlip);
    }

    @Test
    public void testSlipFail()
    {
        final FrameRange rangeToSlip = new FrameRange(10, 20);
        final FrameRange expectedSlippedRange = new FrameRange(10, 20);
        final int offset = -11;
        Assert.assertTrue(EditingMathUtils.slip(rangeToSlip, offset) == false);
        Assert.assertEquals(expectedSlippedRange, rangeToSlip);
    }

    @Test
    public void testSlipToFrameIn()
    {
        final FrameRange rangeToSlip = new FrameRange(10, 20);
        final int frameIn = 5;
        final FrameRange expectedSlippedRange = new FrameRange(5, 15);
        Assert.assertTrue(EditingMathUtils.slipToFrameIn(rangeToSlip, frameIn));
        Assert.assertEquals(expectedSlippedRange, rangeToSlip);
    }

    @Test
    public void testSlipToFrameOut()
    {
        final FrameRange rangeToSlip = new FrameRange(10, 20);
        final int frameOut = 10;
        final FrameRange expectedSlippedRange = new FrameRange(0, 10);
        Assert.assertTrue(EditingMathUtils.slipToFrameOut(rangeToSlip, frameOut));
        Assert.assertEquals(expectedSlippedRange, rangeToSlip);
    }

    /*
     * 
     * TRIM TESTS
     */
    // TRIM IN
    @Test
    public void testTrimIn()
    {
        final FrameRange rangeToTrim = new FrameRange(10, 20);
        final int trimOffset = 5;
        final FrameRange expectedTrimmedRange = new FrameRange(15, 20);
        Assert.assertTrue(EditingMathUtils.trimIn(rangeToTrim, trimOffset));
        Assert.assertEquals(expectedTrimmedRange, rangeToTrim);
    }

    @Test
    public void testTrimInToFrameIn()
    {
        final FrameRange rangeToTrim = new FrameRange(10, 20);
        final int trimToFrame = 5;
        final FrameRange expectedTrimmedRange = new FrameRange(5, 20);
        Assert.assertTrue(EditingMathUtils.trimInToFrame(rangeToTrim, trimToFrame));
        Assert.assertEquals(expectedTrimmedRange, rangeToTrim);
    }

    @Test
    public void testTrimInWithTooBigOffset()
    {
        final FrameRange rangeToTrim = new FrameRange(10, 20);
        final int trimToFrame = 20;
        final FrameRange expectedTrimmedRange = new FrameRange(10, 20);
        Assert.assertTrue(EditingMathUtils.trimInToFrame(rangeToTrim, trimToFrame) == false);
        Assert.assertEquals(expectedTrimmedRange, rangeToTrim);
    }

    @Test
    public void testTrimInWithResultingRangeBeneathZero()
    {
        final FrameRange rangeToTrim = new FrameRange(10, 20);
        final int trimOffset = -20;
        final FrameRange expectedTrimmedRange = new FrameRange(10, 20);
        Assert.assertTrue(EditingMathUtils.trimIn(rangeToTrim, trimOffset) == false);
        Assert.assertEquals(expectedTrimmedRange, rangeToTrim);
    }

    @Test
    public void testTrimInWithResultingRangeEqualsZero()
    {
        final FrameRange rangeToTrim = new FrameRange(10, 20);
        final int trimOffset = -10;
        final FrameRange expectedTrimmedRange = new FrameRange(0, 20);
        Assert.assertTrue(EditingMathUtils.trimIn(rangeToTrim, trimOffset));
        Assert.assertEquals(expectedTrimmedRange, rangeToTrim);
    }

    // TRIM OUT
    @Test
    public void testTrimOut()
    {
        final FrameRange rangeToTrim = new FrameRange(10, 20);
        final int trimOffset = 5;
        final FrameRange expectedTrimmedRange = new FrameRange(10, 25);
        Assert.assertTrue(EditingMathUtils.trimOut(rangeToTrim, trimOffset));
        Assert.assertEquals(expectedTrimmedRange, rangeToTrim);
    }

    @Test
    public void testTrimOutToFrame()
    {
        final FrameRange rangeToTrim = new FrameRange(10, 20);
        final int trimToFrame = 15;
        final FrameRange expectedTrimmedRange = new FrameRange(10, 15);
        Assert.assertTrue(EditingMathUtils.trimOutToFrame(rangeToTrim, trimToFrame));
        Assert.assertEquals(expectedTrimmedRange, rangeToTrim);
    }

    @Test
    public void testTrimOutWithWrongOffset()
    {
        final FrameRange rangeToTrim = new FrameRange(10, 20);
        final int trimToFrame = 10;
        final FrameRange expectedTrimmedRange = new FrameRange(10, 20);
        Assert.assertTrue(EditingMathUtils.trimOutToFrame(rangeToTrim, trimToFrame) == false);
        Assert.assertEquals(expectedTrimmedRange, rangeToTrim);
    }

    // CUT test
    @Test
    public void testCut()
    {
        final FrameRange rangeToCut = new FrameRange(0, 10);
        int initialRangeIn = rangeToCut.frameIn;
        int initialRangeOut = rangeToCut.frameOut;
        FrameRange cuttedRange = new FrameRange();
        int cutPosition = 5;
        Assert.assertTrue(EditingMathUtils.cut(rangeToCut, cuttedRange, cutPosition));
        Assert.assertEquals(initialRangeIn, rangeToCut.frameIn);
        Assert.assertEquals(cutPosition, rangeToCut.frameOut);
        Assert.assertEquals(cutPosition, cuttedRange.frameIn);
        Assert.assertEquals(rangeToCut.frameOut, cuttedRange.frameIn);
        Assert.assertEquals(initialRangeOut, cuttedRange.frameOut);
    }

    @Test
    public void testCutAtStartPosition()
    {
        final FrameRange rangeToCut = new FrameRange(0, 10);
        FrameRange cuttedRange = new FrameRange();
        int cutPosition = 10;
        Assert.assertTrue(EditingMathUtils.cut(rangeToCut, cuttedRange, cutPosition) == false);
        Assert.assertEquals(0, rangeToCut.frameIn);
        Assert.assertEquals(10, rangeToCut.frameOut);
    }

    @Test
    public void testCutAtEndPosition()
    {
        final FrameRange rangeToCut = new FrameRange(0, 10);
        FrameRange cuttedRange = new FrameRange();
        int cutPosition = 0;
        Assert.assertTrue(EditingMathUtils.cut(rangeToCut, cuttedRange, cutPosition) == false);
        Assert.assertEquals(0, rangeToCut.frameIn);
        Assert.assertEquals(10, rangeToCut.frameOut);
    }

    // RIPPLE tests
    @Test
    public void testRipple()
    {
        List<FrameRange> listToRipple = new ArrayList<FrameRange>();
        listToRipple.add(new FrameRange(10, 20));
        listToRipple.add(new FrameRange(25, 30));
        listToRipple.add(new FrameRange(40, 75));
        listToRipple.add(new FrameRange(75, 80));
        listToRipple.add(new FrameRange(80, 100));
        listToRipple.add(new FrameRange(101, 150));
        Assert.assertTrue(EditingMathUtils.ripple(listToRipple));
        Assert.assertEquals(new FrameRange(10, 20), listToRipple.get(0));
        Assert.assertEquals(new FrameRange(20, 25), listToRipple.get(1));
        Assert.assertEquals(new FrameRange(25, 60), listToRipple.get(2));
        Assert.assertEquals(new FrameRange(60, 65), listToRipple.get(3));
        Assert.assertEquals(new FrameRange(65, 85), listToRipple.get(4));
        Assert.assertEquals(new FrameRange(85, 134), listToRipple.get(5));
    }

    @Test
    public void testRippleOverlappingEdits()
    {
        List<FrameRange> listToRipple = new ArrayList<FrameRange>();
        final FrameRange rangeOne = new FrameRange(0, 10);
        final FrameRange rangeTwo = new FrameRange(5, 20);
        listToRipple.add(rangeOne);
        listToRipple.add(rangeTwo);
        Assert.assertTrue(EditingMathUtils.ripple(listToRipple));
        Assert.assertEquals(new FrameRange(0, 10), listToRipple.get(0));
        Assert.assertEquals(new FrameRange(10, 25), listToRipple.get(1));
    }

    // DELETE TESTS
    @Test
    public void testDelete()
    {
        final FrameRange rangeToDelete = new FrameRange(20, 30);
        final List<FrameRange> ranges = new ArrayList<FrameRange>(5);
        ranges.add(new FrameRange(0, 10));
        ranges.add(new FrameRange(10, 20));
        ranges.add(rangeToDelete);
        ranges.add(new FrameRange(30, 40));
        ranges.add(new FrameRange(40, 50));
        Assert.assertTrue(EditingMathUtils.delete(ranges, rangeToDelete, EModificationMode.INSERT));
        Assert.assertEquals(4, ranges.size());
        Assert.assertEquals(new FrameRange(0, 10), ranges.get(0));
        Assert.assertEquals(new FrameRange(10, 20), ranges.get(1));
        Assert.assertEquals(new FrameRange(30, 40), ranges.get(2));
        Assert.assertEquals(new FrameRange(40, 50), ranges.get(3));
    }

    @Test
    public void testDeleteWithOverwrite()
    {
        final FrameRange rangeToDelete = new FrameRange(20, 30);
        final List<FrameRange> ranges = new ArrayList<FrameRange>(5);
        ranges.add(new FrameRange(0, 10));
        ranges.add(new FrameRange(10, 20));
        ranges.add(rangeToDelete);
        ranges.add(new FrameRange(30, 40));
        ranges.add(new FrameRange(40, 50));
        Assert.assertTrue(EditingMathUtils.delete(ranges, rangeToDelete, EModificationMode.OVERWRITE));
        Assert.assertEquals(4, ranges.size());
        Assert.assertEquals(new FrameRange(0, 10), ranges.get(0));
        Assert.assertEquals(new FrameRange(10, 20), ranges.get(1));
        Assert.assertEquals(new FrameRange(20, 30), ranges.get(2));
        Assert.assertEquals(new FrameRange(30, 40), ranges.get(3));
    }

    @Test
    public void testDeleteInListWithGaps()
    {
        final FrameRange rangeToDelete = new FrameRange(20, 30);
        final List<FrameRange> ranges = new ArrayList<FrameRange>(5);
        ranges.add(new FrameRange(0, 5));
        ranges.add(new FrameRange(10, 15));
        ranges.add(rangeToDelete);
        ranges.add(new FrameRange(30, 40));
        ranges.add(new FrameRange(45, 50));
        Assert.assertTrue(EditingMathUtils.delete(ranges, rangeToDelete, EModificationMode.OVERWRITE));
        Assert.assertEquals(4, ranges.size());
        Assert.assertEquals(new FrameRange(0, 5), ranges.get(0));
        Assert.assertEquals(new FrameRange(10, 15), ranges.get(1));
        Assert.assertEquals(new FrameRange(20, 30), ranges.get(2));
        Assert.assertEquals(new FrameRange(35, 40), ranges.get(3));
    }

    // merge tests
    @Test
    public void testMerge()
    {
        final FrameRange rangeStart = new FrameRange(20, 30);
        final FrameRange rangeEnd = new FrameRange(60, 80);
        final List<FrameRange> ranges = new ArrayList<FrameRange>(5);
        ranges.add(new FrameRange(0, 5));
        ranges.add(rangeStart);
        ranges.add(new FrameRange(40, 50));
        ranges.add(rangeEnd);
        ranges.add(new FrameRange(85, 100));
        Assert.assertTrue(EditingMathUtils.merge(ranges, rangeStart, rangeEnd));
        Assert.assertEquals(3, ranges.size());
        Assert.assertEquals(new FrameRange(0, 5), ranges.get(0));
        Assert.assertEquals(new FrameRange(20, 80), ranges.get(1));
        Assert.assertEquals(new FrameRange(85, 100), ranges.get(2));
    }
}
