package fr.hd3d.common.ui.test.modeldata.playlist.math;

import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

import fr.hd3d.common.ui.client.modeldata.playlist.math.EModificationMode;
import fr.hd3d.common.ui.client.modeldata.playlist.math.EditingMathUtils;
import fr.hd3d.common.ui.client.modeldata.playlist.math.FrameRange;


public class InsertTests
{
    @Test
    public void testInsertAtBadPosition()
    {
        List<FrameRange> testList = EditingMathUtilsTest.getTestList();
        boolean insert = EditingMathUtils.insert(testList, new FrameRange(-10, 10), EModificationMode.OVERWRITE);
        Assert.assertEquals(false, insert);
        insert = EditingMathUtils.insert(testList, new FrameRange(-10, 10), EModificationMode.INSERT);
        Assert.assertEquals(false, insert);
        EditingMathUtilsTest.assertRangesEquals(testList, EditingMathUtilsTest.getTestList());
    }

    private List<FrameRange> insert(List<FrameRange> ranges, int insertStart, int insertEnd, EModificationMode mode,
            boolean assertTrue)
    {
        if (assertTrue)
        {
            Assert.assertTrue(EditingMathUtils.insert(ranges, new FrameRange(insertStart, insertEnd), mode));
            return ranges;
        }
        Assert.assertTrue(EditingMathUtils.insert(ranges, new FrameRange(insertStart, insertEnd), mode) == false);
        return ranges;
    }

    private List<FrameRange> insert(List<FrameRange> ranges, int insertStart, int insertEnd, EModificationMode mode)
    {
        return insert(ranges, insertStart, insertEnd, mode, true);
    }

    @Test
    public void testInsertAt35()
    {
        List<FrameRange> newRanges = insert(EditingMathUtilsTest.getTestList(), 35, 45, EModificationMode.INSERT);
        EditingMathUtilsTest.assertRangesEquals(newRanges, new FrameRange(10, 20), new FrameRange(35, 45),
                new FrameRange(45, 65), new FrameRange(65, 75), new FrameRange(75, 85));
        newRanges = insert(EditingMathUtilsTest.getTestList(), 35, 45, EModificationMode.OVERWRITE);
        EditingMathUtilsTest.assertRangesEquals(newRanges, new FrameRange(10, 20), new FrameRange(20, 35),
                new FrameRange(35, 45), new FrameRange(45, 50), new FrameRange(50, 60));
    }

    @Test
    public void testInsertAt35InListWithGap()
    {
        List<FrameRange> newRanges = insert(EditingMathUtilsTest.getTestListWithGap(), 35, 45, EModificationMode.INSERT);
        EditingMathUtilsTest.assertRangesEquals(newRanges, new FrameRange(10, 20), new FrameRange(20, 35),
                new FrameRange(35, 45), new FrameRange(45, 55), new FrameRange(55, 65));
        newRanges = insert(EditingMathUtilsTest.getTestListWithGap(), 35, 45, EModificationMode.OVERWRITE);
        EditingMathUtilsTest.assertRangesEquals(newRanges, new FrameRange(10, 20), new FrameRange(20, 35),
                new FrameRange(35, 45), new FrameRange(45, 50), new FrameRange(55, 65));
    }

    @Test
    public void testInsertAt15()
    {
        List<FrameRange> newRanges = insert(EditingMathUtilsTest.getTestList(), 15, 25, EModificationMode.INSERT);
        EditingMathUtilsTest.assertRangesEquals(newRanges, new FrameRange(15, 25), new FrameRange(25, 35),
                new FrameRange(35, 55), new FrameRange(55, 65), new FrameRange(65, 75));
        newRanges = insert(EditingMathUtilsTest.getTestList(), 15, 25, EModificationMode.OVERWRITE);
        EditingMathUtilsTest.assertRangesEquals(newRanges, new FrameRange(10, 15), new FrameRange(15, 25),
                new FrameRange(25, 40), new FrameRange(40, 50), new FrameRange(50, 60));
    }

    @Test
    public void testInsertAt15InListWithGap()
    {
        List<FrameRange> newRanges = insert(EditingMathUtilsTest.getTestListWithGap(), 15, 25, EModificationMode.INSERT);
        EditingMathUtilsTest.assertRangesEquals(newRanges, new FrameRange(15, 25), new FrameRange(25, 35),
                new FrameRange(35, 50), new FrameRange(50, 60), new FrameRange(60, 70));
        newRanges = insert(EditingMathUtilsTest.getTestListWithGap(), 15, 25, EModificationMode.OVERWRITE);
        EditingMathUtilsTest.assertRangesEquals(newRanges, new FrameRange(10, 15), new FrameRange(15, 25),
                new FrameRange(25, 35), new FrameRange(40, 50), new FrameRange(55, 65));
    }

    @Test
    public void testInsertOnBigEdit()
    {
        // test with overwrite mode
        List<FrameRange> newRanges = insert(EditingMathUtilsTest.getTestList(), 25, 35, EModificationMode.OVERWRITE);
        EditingMathUtilsTest.assertRangesBeforeEquals(newRanges, 4, new FrameRange(10, 20), new FrameRange(20, 25),
                new FrameRange(25, 35), new FrameRange(35, 40));
        // test with insert mode
        List<FrameRange> newRangesWithInsert = insert(EditingMathUtilsTest.getTestList(), 25, 35,
                EModificationMode.INSERT);
        System.out.println(newRangesWithInsert);
        EditingMathUtilsTest.assertRangesEquals(newRangesWithInsert, new FrameRange(10, 20), new FrameRange(25, 35),
                new FrameRange(35, 55), new FrameRange(55, 65), new FrameRange(65, 75));
    }
}
