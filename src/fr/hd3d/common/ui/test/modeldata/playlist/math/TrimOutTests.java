package fr.hd3d.common.ui.test.modeldata.playlist.math;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

import fr.hd3d.common.ui.client.modeldata.playlist.math.EModificationMode;
import fr.hd3d.common.ui.client.modeldata.playlist.math.EditingMathUtils;
import fr.hd3d.common.ui.client.modeldata.playlist.math.FrameRange;


public class TrimOutTests
{
    public List<FrameRange> trimOut(final List<FrameRange> list, int rangeToTrimIndex, int offset,
            EModificationMode mode)
    {
        return trimOut(list, rangeToTrimIndex, offset, mode, true);
    }

    public List<FrameRange> trimOut(final List<FrameRange> list, int rangeToTrimIndex, int offset,
            EModificationMode mode, boolean assertTrue)
    {
        System.out.println("list before :" + list);
        if (assertTrue)
        {
            Assert.assertTrue(EditingMathUtils.trimOut(list, list.get(rangeToTrimIndex), offset, mode));
        }
        else
        {
            Assert.assertTrue(EditingMathUtils.trimOut(list, list.get(rangeToTrimIndex), offset, mode) == false);
        }
        System.out.println("list after :" + list);
        return list;
    }

    @Test
    public void trimWithNonExistingRange()
    {
        List<FrameRange> testList = EditingMathUtilsTest.getTestList();
        Assert
                .assertTrue(EditingMathUtils.trimOut(testList, new FrameRange(20, 40), 10, EModificationMode.INSERT) == false);
        EditingMathUtilsTest.assertRangesEquals(testList, EditingMathUtilsTest.getTestList());
        // overwrite
        testList = EditingMathUtilsTest.getTestList();
        Assert
                .assertTrue(EditingMathUtils.trimOut(testList, new FrameRange(20, 40), 10, EModificationMode.OVERWRITE) == false);
        EditingMathUtilsTest.assertRangesEquals(testList, EditingMathUtilsTest.getTestList());
    }

    @Test
    public void trimWithZeroOffset()
    {
        System.out.println("trimWithZeroOffset");
        final int rangeToTrimIndex = 2;
        final int offset = 0;
        List<FrameRange> trimmedRanges = trimOut(EditingMathUtilsTest.getTestList(), rangeToTrimIndex, offset,
                EModificationMode.INSERT);
        EditingMathUtilsTest.assertRangesEquals(trimmedRanges, EditingMathUtilsTest.getTestList());
        trimmedRanges = trimOut(EditingMathUtilsTest.getTestList(), rangeToTrimIndex, offset,
                EModificationMode.OVERWRITE);
        EditingMathUtilsTest.assertRangesEquals(trimmedRanges, EditingMathUtilsTest.getTestList());
        System.out.println();
    }

    @Test
    public void trimWithPositiveOffset()
    {
        System.out.println("trimWithPositiveOffset");
        final int rangeToTrimIndex = 2;
        final int offset = 5;
        // trim with insert mode
        List<FrameRange> trimmedRanges = trimOut(EditingMathUtilsTest.getTestList(), rangeToTrimIndex, offset,
                EModificationMode.INSERT);
        EditingMathUtilsTest.assertTrimmedItemEquals(trimmedRanges, rangeToTrimIndex, new FrameRange(40, 55));
        EditingMathUtilsTest.assertRangesAfterEquals(trimmedRanges, rangeToTrimIndex, new FrameRange(55, 65));
        // trim with overwrite mode
        trimmedRanges = trimOut(EditingMathUtilsTest.getTestList(), rangeToTrimIndex, offset,
                EModificationMode.OVERWRITE);
        EditingMathUtilsTest.assertTrimmedItemEquals(trimmedRanges, rangeToTrimIndex, new FrameRange(40, 55));
        EditingMathUtilsTest.assertRangesAfterEquals(trimmedRanges, rangeToTrimIndex, new FrameRange(55, 60));
        System.out.println();
    }

    @Test
    public void trimWithNegativeOffset()
    {
        System.out.println("trimWithNegativeOffset");
        final int rangeToTrimIndex = 2;
        // trim with insert mode
        final int offset = -5;
        List<FrameRange> trimmedRanges = trimOut(EditingMathUtilsTest.getTestList(), rangeToTrimIndex, offset,
                EModificationMode.INSERT);
        EditingMathUtilsTest.assertTrimmedItemEquals(trimmedRanges, rangeToTrimIndex, new FrameRange(40, 45));
        EditingMathUtilsTest.assertRangesAfterEquals(trimmedRanges, rangeToTrimIndex, new FrameRange(50, 60));
        // trim with overwrite mode
        trimmedRanges = trimOut(EditingMathUtilsTest.getTestList(), rangeToTrimIndex, offset,
                EModificationMode.OVERWRITE);
        EditingMathUtilsTest.assertTrimmedItemEquals(trimmedRanges, rangeToTrimIndex, new FrameRange(40, 45));
        EditingMathUtilsTest.assertRangesAfterEquals(trimmedRanges, rangeToTrimIndex, new FrameRange(45, 60));
        System.out.println();
    }

    public void trimWithOffsetThatExceedEditLength()
    {
        System.out.println("trimWithOffsetThatExceedEditLength");
        // test with a negative offset that exceeds previous edit length
        final int rangeToTrimIndex = 2;
        List<FrameRange> trimmedRanges = trimOut(EditingMathUtilsTest.getTestList(), rangeToTrimIndex, -20,
                EModificationMode.INSERT, false);
        EditingMathUtilsTest.assertRangesEquals(trimmedRanges, EditingMathUtilsTest.getTestList());
        trimmedRanges = trimOut(EditingMathUtilsTest.getTestList(), rangeToTrimIndex, -20, EModificationMode.OVERWRITE,
                false);
        EditingMathUtilsTest.assertRangesEquals(trimmedRanges, EditingMathUtilsTest.getTestList());
        trimmedRanges = trimOut(EditingMathUtilsTest.getTestList(), rangeToTrimIndex, 20, EModificationMode.OVERWRITE,
                false);
        EditingMathUtilsTest.assertRangesEquals(trimmedRanges, EditingMathUtilsTest.getTestList());
        System.out.println();
    }

    @Test
    public void trimWithOffsetEqualsToNextEditLength()
    {
        System.out.println("trimWithOffsetEqualsToNextEditLength");
        final int rangeToTrimIndex = 2;
        final int offset = 10;
        // offset with insert mode
        List<FrameRange> trimmedRanges = trimOut(EditingMathUtilsTest.getTestList(), rangeToTrimIndex, offset,
                EModificationMode.INSERT);
        EditingMathUtilsTest.assertTrimmedItemEquals(trimmedRanges, rangeToTrimIndex, new FrameRange(40, 60));
        EditingMathUtilsTest.assertRangesAfterEquals(trimmedRanges, rangeToTrimIndex, new FrameRange(60, 70));
        // offset with overwrite
        trimmedRanges = trimOut(EditingMathUtilsTest.getTestList(), rangeToTrimIndex, offset,
                EModificationMode.OVERWRITE, false);
        EditingMathUtilsTest.assertRangesEquals(trimmedRanges, EditingMathUtilsTest.getTestList());
        // offset with overwrite and with one frame less
        trimmedRanges = trimOut(EditingMathUtilsTest.getTestList(), rangeToTrimIndex, offset - 1,
                EModificationMode.OVERWRITE);
        EditingMathUtilsTest.assertTrimmedItemEquals(trimmedRanges, rangeToTrimIndex, new FrameRange(40, 59));
        EditingMathUtilsTest.assertRangesAfterEquals(trimmedRanges, rangeToTrimIndex, new FrameRange(59, 60));
        System.out.println();
    }

    public void trimWithOffsetEqualsToEditLengthLessOne()
    {
        System.out.println("trimWithOffsetEqualsToEditLengthLessOne");
        final int rangeToTrimIndex = 2;
        final int offset = -9;
        // offset with INSERT and with one frame less
        List<FrameRange> trimmedRanges = trimOut(EditingMathUtilsTest.getTestList(), rangeToTrimIndex, offset,
                EModificationMode.INSERT);
        EditingMathUtilsTest.assertTrimmedItemEquals(trimmedRanges, rangeToTrimIndex, new FrameRange(40, 59));
        EditingMathUtilsTest.assertRangesAfterEquals(trimmedRanges, rangeToTrimIndex, new FrameRange(59, 69));
        // offset with overwrite and with one frame less
        trimmedRanges = trimOut(EditingMathUtilsTest.getTestList(), rangeToTrimIndex, offset,
                EModificationMode.OVERWRITE);
        EditingMathUtilsTest.assertTrimmedItemEquals(trimmedRanges, rangeToTrimIndex, new FrameRange(40, 59));
        EditingMathUtilsTest.assertRangesAfterEquals(trimmedRanges, rangeToTrimIndex, new FrameRange(59, 60));
        System.out.println();
    }

    @Test
    public void trimWithNegativeOffsetEqualsToPreviousEditLength()
    {
        System.out.println("trimWithNegativeOffsetEqualsToPreviousEditLength");
        final int rangeToTrimIndex = 2;
        final int offset = -10;
        // offset with insert mode
        List<FrameRange> trimmedRanges = trimOut(EditingMathUtilsTest.getTestList(), rangeToTrimIndex, offset,
                EModificationMode.INSERT, false);
        EditingMathUtilsTest.assertRangesEquals(trimmedRanges, EditingMathUtilsTest.getTestList());
        // offset with overwrite
        trimmedRanges = trimOut(EditingMathUtilsTest.getTestList(), rangeToTrimIndex, offset,
                EModificationMode.OVERWRITE, false);
        EditingMathUtilsTest.assertRangesEquals(trimmedRanges, EditingMathUtilsTest.getTestList());
        // offset with insert and with one frame less
        trimmedRanges = trimOut(EditingMathUtilsTest.getTestList(), rangeToTrimIndex, offset + 1,
                EModificationMode.INSERT);
        List<FrameRange> expectedRanges = new ArrayList<FrameRange>(trimmedRanges.size());
        expectedRanges.add(EditingMathUtilsTest.getTestList().get(0));
        expectedRanges.add(EditingMathUtilsTest.getTestList().get(1));
        expectedRanges.add(new FrameRange(40, 41));
        expectedRanges.add(EditingMathUtilsTest.getTestList().get(3));
        EditingMathUtilsTest.assertRangesEquals(trimmedRanges, expectedRanges);
        // offset with overwrite and with one frame less
        trimmedRanges = trimOut(EditingMathUtilsTest.getTestList(), rangeToTrimIndex, offset + 1,
                EModificationMode.OVERWRITE);
        expectedRanges.clear();
        expectedRanges.add(EditingMathUtilsTest.getTestList().get(0));
        expectedRanges.add(EditingMathUtilsTest.getTestList().get(1));
        expectedRanges.add(new FrameRange(40, 41));
        expectedRanges.add(new FrameRange(41, 60));
        EditingMathUtilsTest.assertRangesEquals(trimmedRanges, expectedRanges);
        System.out.println();
    }

    @Test
    public void trimWithNegativeOffsetEqualsToEditLengthLessOne()
    {
        System.out.println("trimWithNegativeOffsetEqualsToEditLengthLessOne");
        final int rangeToTrimIndex = 2;
        final int offset = -9;
        // offset with insert and with one frame less
        List<FrameRange> trimmedRanges = trimOut(EditingMathUtilsTest.getTestList(), rangeToTrimIndex, offset,
                EModificationMode.INSERT);
        List<FrameRange> expectedRanges = new ArrayList<FrameRange>(trimmedRanges.size());
        expectedRanges.add(EditingMathUtilsTest.getTestList().get(0));
        expectedRanges.add(EditingMathUtilsTest.getTestList().get(1));
        expectedRanges.add(new FrameRange(40, 41));
        expectedRanges.add(EditingMathUtilsTest.getTestList().get(3));
        EditingMathUtilsTest.assertRangesEquals(trimmedRanges, expectedRanges);
        // offset with overwrite and with one frame less
        trimmedRanges = trimOut(EditingMathUtilsTest.getTestList(), rangeToTrimIndex, offset,
                EModificationMode.OVERWRITE);
        expectedRanges.clear();
        expectedRanges.add(EditingMathUtilsTest.getTestList().get(0));
        expectedRanges.add(EditingMathUtilsTest.getTestList().get(1));
        expectedRanges.add(new FrameRange(40, 41));
        expectedRanges.add(new FrameRange(41, 60));
        EditingMathUtilsTest.assertRangesEquals(trimmedRanges, expectedRanges);
        System.out.println();
    }

    // TESTS WITH L2
    @Test
    public void trimListWithGap()
    {
        System.out.println("trimListWithGap");
        final int rangeToTrimIndex = 2;
        final int offset = 5;
        // insert
        List<FrameRange> trimmedRanges = trimOut(EditingMathUtilsTest.getTestListWithGap(), rangeToTrimIndex, offset,
                EModificationMode.INSERT);
        EditingMathUtilsTest.assertTrimmedItemEquals(trimmedRanges, rangeToTrimIndex, new FrameRange(40, 55));
        EditingMathUtilsTest.assertRangesAfterEquals(trimmedRanges, rangeToTrimIndex, new FrameRange(55, 65));
        // overwrite
        trimmedRanges = trimOut(EditingMathUtilsTest.getTestListWithGap(), rangeToTrimIndex, offset,
                EModificationMode.OVERWRITE);
        EditingMathUtilsTest.assertTrimmedItemEquals(trimmedRanges, rangeToTrimIndex, new FrameRange(40, 55));
        EditingMathUtilsTest.assertRangesAfterEquals(trimmedRanges, rangeToTrimIndex, new FrameRange(55, 65));
        System.out.println();
    }

    @Test
    public void trimListWithGapAndEditLengthOffset()
    {
        System.out.println("trimListWithGapAndEditLengthOffset");
        final int rangeToTrimIndex = 2;
        final int offset = 10;
        // insert
        List<FrameRange> trimmedRanges = trimOut(EditingMathUtilsTest.getTestListWithGap(), rangeToTrimIndex, offset,
                EModificationMode.INSERT);
        EditingMathUtilsTest.assertTrimmedItemEquals(trimmedRanges, rangeToTrimIndex, new FrameRange(40, 60));
        EditingMathUtilsTest.assertRangesAfterEquals(trimmedRanges, rangeToTrimIndex, new FrameRange(60, 70));
        // overwrite
        trimmedRanges = trimOut(EditingMathUtilsTest.getTestListWithGap(), rangeToTrimIndex, offset,
                EModificationMode.OVERWRITE);
        EditingMathUtilsTest.assertTrimmedItemEquals(trimmedRanges, rangeToTrimIndex, new FrameRange(40, 60));
        EditingMathUtilsTest.assertRangesAfterEquals(trimmedRanges, rangeToTrimIndex, new FrameRange(60, 65));
        System.out.println();
    }

    @Test
    public void trimListWithGapWithNegativeOffset()
    {
        System.out.println("trimListWithGapWithNegativeOffset");
        final int rangeToTrimIndex = 2;
        final int offset = -2;
        // insert
        List<FrameRange> trimmedRanges = trimOut(EditingMathUtilsTest.getTestListWithGap(), rangeToTrimIndex, offset,
                EModificationMode.INSERT);
        EditingMathUtilsTest.assertTrimmedItemEquals(trimmedRanges, rangeToTrimIndex, new FrameRange(40, 48));
        EditingMathUtilsTest.assertRangesAfterEquals(trimmedRanges, rangeToTrimIndex, new FrameRange(55, 65));
        // overwrite
        trimmedRanges = trimOut(EditingMathUtilsTest.getTestListWithGap(), rangeToTrimIndex, offset,
                EModificationMode.OVERWRITE);
        EditingMathUtilsTest.assertTrimmedItemEquals(trimmedRanges, rangeToTrimIndex, new FrameRange(40, 48));
        EditingMathUtilsTest.assertRangesAfterEquals(trimmedRanges, rangeToTrimIndex, new FrameRange(55, 65));
        System.out.println();
    }
}
