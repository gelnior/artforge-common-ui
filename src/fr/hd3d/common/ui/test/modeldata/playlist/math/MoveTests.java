package fr.hd3d.common.ui.test.modeldata.playlist.math;

import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

import fr.hd3d.common.ui.client.modeldata.playlist.math.EModificationMode;
import fr.hd3d.common.ui.client.modeldata.playlist.math.EditingMathUtils;
import fr.hd3d.common.ui.client.modeldata.playlist.math.FrameRange;


public class MoveTests
{
    private List<FrameRange> move(List<FrameRange> ranges, int indexToMove, int moveTo, EModificationMode mode,
            boolean assertTrue)
    {
        final FrameRange rangeToMove = ranges.get(indexToMove);
        if (assertTrue)
        {
            Assert.assertTrue(EditingMathUtils.move(ranges, rangeToMove, moveTo, mode));
        }
        else
        {
            Assert.assertTrue(EditingMathUtils.move(ranges, rangeToMove, moveTo, mode) == false);
        }
        return ranges;
    }

    private List<FrameRange> move(List<FrameRange> ranges, int indexToMove, int moveTo, EModificationMode mode)
    {
        return move(ranges, indexToMove, moveTo, mode, true);
    }

    @Test
    public void testShift()
    {
        List<FrameRange> list = EditingMathUtilsTest.getTestList();
        EditingMathUtils.shiftToRight(list.get(1), 25, list);
        EditingMathUtilsTest.assertRangesEquals(list, new FrameRange(10, 20), new FrameRange(25, 45), new FrameRange(
                45, 55), new FrameRange(55, 65));
        List<FrameRange> listWithGap = EditingMathUtilsTest.getTestListWithGap();
        EditingMathUtils.shiftToRight(listWithGap.get(1), 25, listWithGap);
        EditingMathUtilsTest.assertRangesEquals(listWithGap, new FrameRange(10, 20), new FrameRange(25, 40),
                new FrameRange(40, 50), new FrameRange(55, 65));
        List<FrameRange> listWithGapTwo = EditingMathUtilsTest.getTestListWithGap();
        EditingMathUtils.shiftToRight(listWithGapTwo.get(0), 20, listWithGapTwo);
        EditingMathUtilsTest.assertRangesEquals(listWithGapTwo, new FrameRange(20, 30), new FrameRange(30, 45),
                new FrameRange(45, 55), new FrameRange(55, 65));
    }

    @Test
    public void testShiftWithBigOffset()
    {
        List<FrameRange> list = EditingMathUtilsTest.getTestList();
        EditingMathUtils.shiftToRight(list.get(1), 65, list);
        EditingMathUtilsTest.assertRangesEquals(list, new FrameRange(10, 20), new FrameRange(65, 85), new FrameRange(
                85, 95), new FrameRange(95, 105));
    }

    @Test
    public void testMoveWithBadPosition()
    {
        move(EditingMathUtilsTest.getTestList(), 2, -1, EModificationMode.INSERT, false);
        move(EditingMathUtilsTest.getTestList(), 2, -1, EModificationMode.OVERWRITE, false);
    }

    @Test
    public void testMoveRangeWichIsNotInTheList()
    {
        List<FrameRange> movedRanges = EditingMathUtilsTest.getTestList();
        boolean move = EditingMathUtils.move(movedRanges, new FrameRange(10, 20), 15, EModificationMode.INSERT);
        Assert.assertEquals(false, move);
        EditingMathUtilsTest.assertRangesEquals(movedRanges, EditingMathUtilsTest.getTestList());
    }

    @Test
    public void moveR3afterR4()
    {
        System.out.println("move R3 after R4");
        // insert mode
        final List<FrameRange> initialList = EditingMathUtilsTest.getTestList();
        List<FrameRange> movedRanges = move(EditingMathUtilsTest.getTestList(), 2, 60, EModificationMode.INSERT);
        Assert.assertEquals(initialList.size(), movedRanges.size());
        EditingMathUtilsTest.assertRangesAfterEquals(movedRanges, 1, new FrameRange(50, 60), new FrameRange(60, 70));
        // overwrite mode
        movedRanges = move(EditingMathUtilsTest.getTestList(), 2, 60, EModificationMode.OVERWRITE);
        Assert.assertEquals(initialList.size(), movedRanges.size());
        EditingMathUtilsTest.assertRangesAfterEquals(movedRanges, 1, new FrameRange(50, 60), new FrameRange(60, 70));
        System.out.println();
    }

    @Test
    public void moveR3at55()
    {
        System.out.println("move R3 at 55");
        // insert mode
        final List<FrameRange> initialList = EditingMathUtilsTest.getTestList();
        List<FrameRange> movedRanges = move(EditingMathUtilsTest.getTestList(), 2, 55, EModificationMode.INSERT);
        Assert.assertEquals(initialList.size(), movedRanges.size());
        EditingMathUtilsTest.assertRangesAfterEquals(movedRanges, 1, new FrameRange(55, 65), new FrameRange(65, 75));
        // overwrite mode
        movedRanges = move(EditingMathUtilsTest.getTestList(), 2, 55, EModificationMode.OVERWRITE);
        Assert.assertEquals(initialList.size(), movedRanges.size());
        EditingMathUtilsTest.assertRangesAfterEquals(movedRanges, 1, new FrameRange(50, 55), new FrameRange(55, 65));
        System.out.println();
    }

    @Test
    public void moveR3at56()
    {
        System.out.println("move R3 at 56");
        // insert mode
        final List<FrameRange> initialList = EditingMathUtilsTest.getTestList();
        List<FrameRange> movedRanges = move(EditingMathUtilsTest.getTestList(), 2, 56, EModificationMode.INSERT);
        Assert.assertEquals(initialList.size(), movedRanges.size());
        EditingMathUtilsTest.assertRangesAfterEquals(movedRanges, 1, new FrameRange(56, 66), new FrameRange(66, 76));
        System.out.println();
    }

    @Test
    public void moveR3at54()
    {
        System.out.println("move R3 at 54");
        // insert mode
        final List<FrameRange> initialList = EditingMathUtilsTest.getTestList();
        List<FrameRange> movedRanges = move(EditingMathUtilsTest.getTestList(), 2, 54, EModificationMode.INSERT);
        Assert.assertEquals(initialList.size(), movedRanges.size());
        EditingMathUtilsTest.assertRangesAfterEquals(movedRanges, 1, new FrameRange(54, 64), new FrameRange(64, 74));
        System.out.println();
    }

    @Test
    public void moveR2beforeR1()
    {
        System.out.println("move R2 before R1");
        // insert mode
        List<FrameRange> movedRanges = move(EditingMathUtilsTest.getTestList(), 1, 10, EModificationMode.INSERT);
        EditingMathUtilsTest.assertRangesBeforeEquals(movedRanges, 2, new FrameRange(10, 30), new FrameRange(30, 40));
        movedRanges = move(EditingMathUtilsTest.getTestList(), 1, 10, EModificationMode.OVERWRITE);
        EditingMathUtilsTest.assertRangesEquals(movedRanges, new FrameRange(10, 30), new FrameRange(40, 50),
                new FrameRange(50, 60));
        System.out.println();
    }

    @Test
    public void moveR2at5()
    {
        System.out.println("move R2 at 5");
        // insert mode
        List<FrameRange> movedRanges = move(EditingMathUtilsTest.getTestList(), 1, 5, EModificationMode.INSERT);
        EditingMathUtilsTest.assertRangesEquals(movedRanges, new FrameRange(5, 25), new FrameRange(25, 35),
                new FrameRange(40, 50), new FrameRange(50, 60));
        // overwrite mode
        movedRanges = move(EditingMathUtilsTest.getTestList(), 1, 5, EModificationMode.OVERWRITE);
        EditingMathUtilsTest.assertRangesEquals(movedRanges, new FrameRange(5, 25), new FrameRange(40, 50),
                new FrameRange(50, 60));
        System.out.println();
    }

    @Test
    public void moveR2at10()
    {
        System.out.println("move R2 at 10");
        // insert mode
        final List<FrameRange> initialList = EditingMathUtilsTest.getTestList();
        List<FrameRange> movedRanges = move(EditingMathUtilsTest.getTestList(), 1, 10, EModificationMode.INSERT);
        Assert.assertEquals(initialList.size(), movedRanges.size());
        EditingMathUtilsTest.assertRangesEquals(movedRanges, new FrameRange(10, 30), new FrameRange(30, 40),
                new FrameRange(40, 50), new FrameRange(50, 60));
        System.out.println();
    }

    @Test
    public void moveR4at15()
    {
        System.out.println("move R4 at 15");
        // insert mode
        List<FrameRange> movedRanges = move(EditingMathUtilsTest.getTestList(), 3, 15, EModificationMode.INSERT);
        EditingMathUtilsTest.assertRangesEquals(movedRanges, new FrameRange(15, 25), new FrameRange(25, 35),
                new FrameRange(35, 55), new FrameRange(55, 65));
        // overwrite mode
        movedRanges = move(EditingMathUtilsTest.getTestList(), 3, 15, EModificationMode.OVERWRITE);
        EditingMathUtilsTest.assertRangesEquals(movedRanges, new FrameRange(10, 15), new FrameRange(15, 25),
                new FrameRange(25, 40), new FrameRange(40, 50));
        System.out.println();
    }

    @Test
    public void moveR3at35inListWithGap()
    {
        System.out.println("move R3 at 35 in list with gap");
        // insert mode
        List<FrameRange> movedRanges = move(EditingMathUtilsTest.getTestListWithGap(), 2, 35, EModificationMode.INSERT);
        EditingMathUtilsTest.assertRangesEquals(movedRanges, new FrameRange(10, 20), new FrameRange(20, 35),
                new FrameRange(35, 45), new FrameRange(55, 65));
        // overwrite mode
        movedRanges = move(EditingMathUtilsTest.getTestListWithGap(), 2, 35, EModificationMode.OVERWRITE);
        EditingMathUtilsTest.assertRangesEquals(movedRanges, new FrameRange(10, 20), new FrameRange(20, 35),
                new FrameRange(35, 45), new FrameRange(55, 65));
        System.out.println();
    }

    @Test
    public void moveR3at20inListWithGap()
    {
        System.out.println("move R3 at 20 in list with gap");
        // insert mode
        List<FrameRange> movedRanges = move(EditingMathUtilsTest.getTestListWithGap(), 2, 20, EModificationMode.INSERT);
        EditingMathUtilsTest.assertRangesEquals(movedRanges, new FrameRange(10, 20), new FrameRange(20, 30),
                new FrameRange(30, 45), new FrameRange(55, 65));
        // overwrite mode
        movedRanges = move(EditingMathUtilsTest.getTestListWithGap(), 2, 20, EModificationMode.OVERWRITE);
        EditingMathUtilsTest.assertRangesEquals(movedRanges, new FrameRange(10, 20), new FrameRange(20, 30),
                new FrameRange(30, 35), new FrameRange(55, 65));
        System.out.println();
    }

    @Test
    public void moveR3at28inListWithGap()
    {
        System.out.println("move R3 at 28 in list with gap");
        // insert mode
        List<FrameRange> movedRanges = move(EditingMathUtilsTest.getTestListWithGap(), 2, 28, EModificationMode.INSERT);
        EditingMathUtilsTest.assertRangesEquals(movedRanges, new FrameRange(10, 20), new FrameRange(28, 38),
                new FrameRange(38, 53), new FrameRange(55, 65));
        System.out.println();
    }
}
