package fr.hd3d.common.ui.test.modeldata.playlist;

import org.junit.Test;

import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.modeldata.playlist.PlayListEditModelData;
import fr.hd3d.common.ui.test.modeldata.ModelDataTest;
import fr.hd3d.common.ui.test.util.modeldata.reader.PlayListEditReaderMock;


public class PlayListEditModelDataTests extends ModelDataTest<PlayListEditModelData, PlayListEditReaderMock>
{

    @Override
    public void setUp()
    {
        super.setUp();
        this.reader = new PlayListEditReaderMock();
    }

    @Test
    public void testCreate()
    {
        PlayListEditModelData edit = new PlayListEditModelData("edit");
        PlayListEditModelData savedEdit = this.saveObject(edit);
        assertEquals(edit.getName(), savedEdit.getName());
        edit.delete();
    }

    @Test
    public void testUpdate()
    {
        PlayListEditModelData edit = new PlayListEditModelData("edit");
        edit.save();
        edit.setName("edit-piaf");
        edit.save();
        PlayListEditModelData updtEdit = new PlayListEditModelData();
        updtEdit.setId(edit.getId());
        updtEdit.refresh();
        assertEquals(edit.getName(), updtEdit.getName());
        edit.delete();
    }

    @Test
    public void testDelete()
    {
        PlayListEditModelData edit = new PlayListEditModelData("edit");
        edit.save();
        edit.delete();
        edit.refresh();
        assertEquals(CommonErrors.RECORD_NOT_ON_SERVER, controller.getLastError());
    }

    @Test
    public void testFrameRangeUpdate()
    {
        PlayListEditModelData edit = new PlayListEditModelData("rangetest");
        edit.setSrcIn(0);
        edit.setSrcOut(10);
        edit.setRecIn(0);
        edit.setRecOut(10);
        edit.save();

        assertEquals(edit.getSrcRange().frameIn, 0);
        assertEquals(edit.getSrcRange().frameOut, 10);
        assertEquals(edit.getRecRange().frameIn, 0);
        assertEquals(edit.getRecRange().frameOut, 10);

        PlayListEditModelData updtEdit = new PlayListEditModelData();
        updtEdit.setId(edit.getId());
        updtEdit.refresh();

        assertEquals(updtEdit.getSrcRange().frameIn, 0);
        assertEquals(updtEdit.getSrcRange().frameOut, 10);
        assertEquals(updtEdit.getRecRange().frameIn, 0);
        assertEquals(updtEdit.getRecRange().frameOut, 10);

        edit.delete();
    }

}
