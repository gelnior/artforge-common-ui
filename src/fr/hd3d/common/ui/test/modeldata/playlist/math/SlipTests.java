package fr.hd3d.common.ui.test.modeldata.playlist.math;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

import fr.hd3d.common.ui.client.modeldata.playlist.math.EditingMathUtils;
import fr.hd3d.common.ui.client.modeldata.playlist.math.FrameRange;


public class SlipTests
{
    /**
     * creates a frame range test list with no gaps between ranges. Creates the following ordered list : 10 20 40 50 60
     * |--R1---|------R2-------|--R3---|----R4--|
     * 
     * @return a List of FrameRange
     */
    private static List<FrameRange> getTestList()
    {
        List<FrameRange> list = new ArrayList<FrameRange>();
        final FrameRange r1 = new FrameRange(10, 20);
        final FrameRange r2 = new FrameRange(20, 40);
        final FrameRange r3 = new FrameRange(40, 50);
        final FrameRange r4 = new FrameRange(50, 60);
        list.add(r1);
        list.add(r2);
        list.add(r3);
        list.add(r4);
        return list;
    }

    @Test
    public void testSlipRanges()
    {
        final List<FrameRange> list = getTestList();
        Assert.assertTrue(EditingMathUtils.slip(list, 5));
    }

    @Test
    public void testSlipRangesWithBadOffset()
    {
        final List<FrameRange> list = getTestList();
        Assert.assertTrue(EditingMathUtils.slip(list, -11) == false);
    }
}
