package fr.hd3d.common.ui.test.modeldata;

import org.junit.Test;

import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.test.util.TestUtils;
import fr.hd3d.common.ui.test.util.controller.ErrorController;
import fr.hd3d.common.ui.test.util.modeldata.reader.TaskTypeReaderMock;


public class TaskTypeModelDataTest extends ModelDataTest<TaskTypeModelData, TaskTypeReaderMock>
{
    protected ErrorController errorController = new ErrorController();

    @Override
    public void setUp()
    {
        super.setUp();

        this.reader = new TaskTypeReaderMock();
        this.path = ServicesPath.getPath("DeviceModel");

        EventDispatcher.get().addController(errorController);
    }

    @Test
    public void testPostTaskType() throws Exception
    {
        TaskTypeModelData taskType = new TaskTypeModelData();
        taskType.setName("test-creation-task-type");
        taskType.setColor("#FFF");
        taskType.setDescription("description");
        taskType.setEntityName("Shot");
        taskType.setProjectID(TestUtils.getDefaultProject().getId());

        TaskTypeModelData postedTaskType = this.saveObject(taskType);
        assertEquals(taskType.getName(), postedTaskType.getName());
        assertEquals(taskType.getDescription(), postedTaskType.getDescription());
        assertEquals(taskType.getColor(), postedTaskType.getColor());
        assertEquals(taskType.getEntityName(), postedTaskType.getEntityName());
        assertEquals(taskType.getProjectID(), postedTaskType.getProjectID());

        postedTaskType.delete();
    }

    @Test
    public void testPutTaskType() throws Exception
    {
        TaskTypeModelData taskType = new TaskTypeModelData("test-creation-task-type");
        taskType.setName("test-creation-task-type");
        taskType.setColor("#FFF");
        taskType.setDescription("description");
        taskType.setEntityName("Shot");
        taskType.setProjectID(TestUtils.getDefaultProject().getId());
        TaskTypeModelData putTaskType = this.saveObject(taskType);

        putTaskType.setName("test-creation-task-type_updated");
        taskType.setColor("#FFA");
        taskType.setDescription("description_updated");
        taskType.setEntityName("Constituent");
        taskType.setProjectID(null);

        TaskTypeModelData putTaskType_server = this.saveObject(putTaskType);
        assertEquals(putTaskType.getName(), putTaskType_server.getName());
        assertEquals(putTaskType.getDescription(), putTaskType_server.getDescription());
        assertEquals(putTaskType.getColor(), putTaskType_server.getColor());
        assertEquals(putTaskType.getEntityName(), putTaskType_server.getEntityName());
        assertEquals(putTaskType.getProjectID(), putTaskType_server.getProjectID());

        putTaskType.delete();
    }

    @Test
    public void testDeleteTaskType() throws Exception
    {
        TaskTypeModelData taskType = new TaskTypeModelData("device_model_delete_01");

        taskType.save();
        taskType.delete();
        taskType.refresh();

        assertEquals(CommonErrors.RECORD_NOT_ON_SERVER, errorController.getLastError());
    }

}
