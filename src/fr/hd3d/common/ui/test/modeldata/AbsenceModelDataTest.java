package fr.hd3d.common.ui.test.modeldata;

import java.util.Calendar;

import org.junit.Test;

import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.modeldata.resource.AbsenceModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.test.util.modeldata.reader.AbsenceModelReaderMock;


/**
 * Test absence service validity.
 * 
 * @author HD3D
 */

public class AbsenceModelDataTest extends ModelDataTest<AbsenceModelData, AbsenceModelReaderMock>
{
    PersonModelData myPerson = new PersonModelData("testAbsenceFirstName", "testAbsenceLastName", "testAbsenceLogin",
            "testAbsenceMail", "testAbsencePhone", "testAbsencePath");

    Calendar myDateStart = Calendar.getInstance();
    Calendar myDateEnd = Calendar.getInstance();

    Calendar myDateStartUpdate = Calendar.getInstance();
    Calendar myDateEndUpdate = Calendar.getInstance();

    @Override
    public void setUp()
    {
        super.setUp();
        this.reader = new AbsenceModelReaderMock();

        myDateStart.set(110, 4, 28, 13, 45, 00);
        myDateStart.set(14, 0);
        myDateEnd.set(110, 4, 28, 20, 00, 00);
        myDateEnd.set(14, 0);

        myDateStartUpdate.set(110, 4, 29, 13, 45, 00);
        myDateStartUpdate.set(14, 0);
        myDateEndUpdate.set(110, 4, 29, 20, 00, 00);
        myDateEndUpdate.set(14, 0);

        myPerson.refreshByLogin(null);
        if (myPerson.getId() == null)
        {
            myPerson.save();
        }
    }

    @Test
    public void testPost() throws Exception
    {
        AbsenceModelData myAbsence = new AbsenceModelData();

        myAbsence.setWorkerID(myPerson.getId());
        myAbsence.setComment("myComment");
        myAbsence.setStartDate(myDateStart.getTime());
        myAbsence.setEndDate(myDateEnd.getTime());

        AbsenceModelData myAbsenceServer = this.saveObject(myAbsence);

        assertEquals(myAbsence.getWorkerID(), myAbsenceServer.getWorkerID());
        assertEquals(myAbsence.getComment(), myAbsenceServer.getComment());
        assertEquals(myAbsence.getStartDate(), myAbsenceServer.getStartDate());
        assertEquals(myAbsence.getEndDate(), myAbsenceServer.getEndDate());

        myAbsence.delete();
        myAbsenceServer.delete();
    }

    @Test
    public void testPut() throws Exception
    {
        AbsenceModelData myAbsence = new AbsenceModelData();

        myAbsence.setWorkerID(myPerson.getId());
        myAbsence.setComment("myComment");
        myAbsence.setStartDate(myDateStart.getTime());
        myAbsence.setEndDate(myDateEnd.getTime());

        AbsenceModelData myPutAbsence = this.saveObject(myAbsence);

        myPutAbsence.setComment("myCommentUpdated");
        myPutAbsence.setStartDate(myDateStartUpdate.getTime());
        myPutAbsence.setEndDate(myDateEndUpdate.getTime());

        AbsenceModelData myPutAbsenceUpdated = this.saveObject(myPutAbsence);

        assertEquals(myPutAbsence.getComment(), myPutAbsenceUpdated.getComment());
        assertEquals(myPutAbsence.getStartDate(), myPutAbsenceUpdated.getStartDate());
        assertEquals(myPutAbsence.getEndDate(), myPutAbsenceUpdated.getEndDate());

        myPutAbsence.delete();
        myAbsence.delete();
    }

    @Test
    public void testDelete() throws Exception
    {
        AbsenceModelData myAbsence = new AbsenceModelData();

        myAbsence.setWorkerID(myPerson.getId());
        myAbsence.setComment("myComment");
        myAbsence.setStartDate(myDateStart.getTime());
        myAbsence.setEndDate(myDateEnd.getTime());

        myAbsence.save();
        myAbsence.delete();
        myAbsence.refresh();

        assertEquals(CommonErrors.RECORD_NOT_ON_SERVER, controller.getLastError());
    }

    @Override
    public void tearDown()
    {
        myPerson.delete();
        myDateStart = null;
        myDateEnd = null;
    }
}
