package fr.hd3d.common.ui.test.modeldata;

import org.junit.Test;

import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.common.ui.client.modeldata.technical.RoleModelData;
import fr.hd3d.common.ui.test.util.modeldata.reader.RoleReaderMock;


/**
 * Test resource group service validity.
 * 
 * @author HD3D
 */
public class RoleModelDataTest extends ModelDataTest<RoleModelData, RoleReaderMock>
{
    @Override
    public void setUp()
    {
        super.setUp();

        this.reader = new RoleReaderMock();
    }

    @Test
    public void testPostRole() throws Exception
    {
        RoleModelData role = new RoleModelData("organization_post_01");
        RoleModelData roleServer = this.saveObject(role);

        assertEquals(role.getName(), roleServer.getName());

        roleServer.delete();
    }

    @Test
    public void testPutRole() throws Exception
    {
        RoleModelData role = new RoleModelData("organization_put_01");

        role.save();
        role.setName("organization_put_01_udpated");
        role.save();

        RoleModelData organizationServer = new RoleModelData();
        organizationServer.setId(role.getId());
        organizationServer.refresh();

        assertEquals(role.getName(), organizationServer.getName());

        role.delete();
    }

    @Test
    public void testDeleteRole() throws Exception
    {
        RoleModelData role = new RoleModelData("organization_delete_01");

        role.save();
        role.delete();
        role.refresh();

        assertEquals(CommonErrors.RECORD_NOT_ON_SERVER, controller.getLastError());
    }

    @Test
    public void testAddResourceGroupToRole() throws Exception
    {
        RoleModelData role = new RoleModelData("role_resourcegroup_01");
        ResourceGroupModelData resourceGroup = new ResourceGroupModelData("resourcegroup_role_01", null);

        resourceGroup.save();
        role.getResourceGroupIds().add(resourceGroup.getId());

        RoleModelData roleServer = this.saveObject(role);

        assertNotNull(roleServer.getResourceGroupIds());
        assertEquals(1, roleServer.getResourceGroupIds().size());
        assertEquals(resourceGroup.getId(), roleServer.getResourceGroupIds().get(0));

        resourceGroup.delete();
        role.delete();
    }
}
