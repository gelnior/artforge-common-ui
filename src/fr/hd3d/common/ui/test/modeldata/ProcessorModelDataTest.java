package fr.hd3d.common.ui.test.modeldata;

import org.junit.Test;

import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.modeldata.inventory.ProcessorModelData;
import fr.hd3d.common.ui.test.util.modeldata.reader.ProcessorReaderMock;


public class ProcessorModelDataTest extends ModelDataTest<ProcessorModelData, ProcessorReaderMock>
{
    @Override
    public void setUp()
    {
        super.setUp();

        this.reader = new ProcessorReaderMock();
    }

    @Test
    public void testPostProcessor() throws Exception
    {
        ProcessorModelData processor = new ProcessorModelData("processor_post_01");
        ProcessorModelData processorServer = this.saveObject(processor);

        assertEquals(processor.getName(), processorServer.getName());

        processor.delete();
    }

    @Test
    public void testPutProcessor() throws Exception
    {
        ProcessorModelData processor = new ProcessorModelData("processor_put_01");

        processor.save();
        processor.setName("processor_put_01_updated");
        processor.save();

        ProcessorModelData processorServer = new ProcessorModelData();
        processorServer.setId(processor.getId());
        processorServer.refresh();

        assertEquals(processor.getName(), processorServer.getName());

        processor.delete();
    }

    @Test
    public void testDeleteProcessor() throws Exception
    {
        ProcessorModelData contract = new ProcessorModelData("contract_delete_01");

        contract.save();
        contract.delete();
        contract.refresh();

        assertEquals(CommonErrors.RECORD_NOT_ON_SERVER, controller.getLastError());
    }
}
