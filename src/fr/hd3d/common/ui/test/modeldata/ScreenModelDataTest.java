package fr.hd3d.common.ui.test.modeldata;

import java.util.Date;

import org.junit.Test;

import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ScreenModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ScreenModelModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.test.util.modeldata.reader.ScreenReaderMock;


public class ScreenModelDataTest extends ModelDataTest<ScreenModelData, ScreenReaderMock>
{
    @Override
    public void setUp()
    {
        super.setUp();

        this.reader = new ScreenReaderMock();
        this.path = ServicesPath.getPath("Screen");
    }

    @Test
    public void testPostScreen() throws Exception
    {
        ScreenModelData screen = new ScreenModelData("screen_post_01", "serial_01", "reference_01", new Date(),
                new Date(), defaultInventoryStatus, 1.2D, "LCD", "COLOR");

        ScreenModelData postedScreen = this.saveObject(screen);

        assertEquals(screen.getName(), postedScreen.getName());
        assertEquals(screen.getSerial(), postedScreen.getSerial());
        assertEquals(screen.getBillingReference(), postedScreen.getBillingReference());
        assertEquals(dateFormatter.format(screen.getPurchaseDate()), dateFormatter.format(postedScreen
                .getPurchaseDate()));
        assertEquals(dateFormatter.format(screen.getWarrantyEnd()), dateFormatter.format(postedScreen.getWarrantyEnd()));
        assertEquals(screen.getSize(), postedScreen.getSize());
        assertEquals(screen.getType(), postedScreen.getType());
        assertEquals(screen.getQualification(), postedScreen.getQualification());

        postedScreen.delete();
    }

    @Test
    public void testPutscreen() throws Exception
    {
        ScreenModelData screen = new ScreenModelData("screen_put_01", "serial_01", "reference_01", new Date(),
                new Date(), defaultInventoryStatus, 1.2D, "LCD", "COLOR");

        ScreenModelData putScreen = this.saveObject(screen);

        putScreen.setName("test_put_soft_updated");
        putScreen.setSerial("test_put_serial_updated");
        putScreen.setBillingReference("test_put_reference");
        putScreen.setPurchaseDate(new Date());
        putScreen.setWarrantyEnd(new Date());
        putScreen.setSize(1.4D);
        putScreen.setType("CRT");
        putScreen.setQualification("OUT");

        ScreenModelData putscreen_server = this.saveObject(putScreen);

        assertEquals(putScreen.getName(), putscreen_server.getName());
        assertEquals(putScreen.getSerial(), putscreen_server.getSerial());
        assertEquals(putScreen.getBillingReference(), putscreen_server.getBillingReference());
        assertEquals(dateFormatter.format(putScreen.getPurchaseDate()), dateFormatter.format(putscreen_server
                .getPurchaseDate()));
        assertEquals(dateFormatter.format(putScreen.getWarrantyEnd()), dateFormatter.format(putscreen_server
                .getWarrantyEnd()));
        assertEquals(putScreen.getSize(), putscreen_server.getSize());
        assertEquals(putScreen.getType(), putscreen_server.getType());
        assertEquals(putScreen.getQualification(), putscreen_server.getQualification());

        putScreen.delete();
    }

    @Test
    public void testDeletescreen() throws Exception
    {
        ScreenModelData screen = new ScreenModelData("screen_delete_01", "serial_01", "reference_01", new Date(),
                new Date(), defaultInventoryStatus, 1.2D, "LCD", "COLOR");

        screen.save();
        screen.delete();
        screen.refresh();

        assertEquals(CommonErrors.RECORD_NOT_ON_SERVER, controller.getLastError());
    }

    @Test
    public void testComputersList() throws Exception
    {
        ScreenModelData screen = new ScreenModelData("screen_comp_01", "serial_01", "reference_01", new Date(),
                new Date(), defaultInventoryStatus, 1.2D, "LCD", "COLOR");
        ComputerModelData computer = new ComputerModelData("comp_screen_01", "serial_01", "billing_01", new Date(),
                new Date(), defaultInventoryStatus, 123L, "fr.hd3d.laptop01", "127.0.0.1", "mac_adress", 1200L, 2L, 2L,
                4096L, "NO_WORKER", 160L, 35L);

        computer.save();
        screen.getComputerIDs().add(computer.getId());
        ScreenModelData postedScreen = this.saveObject(screen);
        computer.refresh();
        assertEquals(computer.getId().longValue(), postedScreen.getComputerIDs().get(0).longValue());
        assertEquals(postedScreen.getId().longValue(), computer.getScreenIDs().get(0).longValue());

        ComputerModelData computer02 = new ComputerModelData("comp_screen_02", "serial_02", "billing_02", new Date(),
                new Date(), defaultInventoryStatus, 123L, "fr.hd3d.laptop02", "127.0.0.1", "mac_adress", 1200L, 2L, 2L,
                4096L, "NO_WORKER", 160L, 35L);

        computer02.save();
        postedScreen.getComputerIDs().add(computer02.getId());
        ScreenModelData putScreen = this.saveObject(postedScreen);
        computer.refresh();
        assertEquals(2, putScreen.getComputerIDs().size());
        assertEquals(postedScreen.getId().longValue(), computer.getScreenIDs().get(0).longValue());

        computer.delete();
        computer02.delete();
        putScreen.delete();
    }

    @Test
    public void testScreenModel() throws Exception
    {
        ScreenModelData screen = new ScreenModelData("screen_comp_01", "serial_01", "reference_01", new Date(),
                new Date(), defaultInventoryStatus, 1.2D, "LCD", "COLOR");
        ScreenModelModelData screenModel = new ScreenModelModelData("screen_model_screen_01");

        screenModel.save();
        screen.setScreenModelId(screenModel.getId());
        screen = this.saveObject(screen);

        assertEquals(screenModel.getId().longValue(), screen.getScreenModelId().longValue());
        assertEquals(screenModel.getName(), screen.getScreenModelName());

        ScreenModelModelData screenModel02 = new ScreenModelModelData("screen_model_screen_02");
        screenModel02.save();
        screen.setScreenModelId(screenModel02.getId());
        screen = this.saveObject(screen);

        assertEquals(screenModel02.getId().longValue(), screen.getScreenModelId().longValue());
    }

    @Test
    public void testResourceGroupList() throws Exception
    {
        ResourceGroupModelData resourceGroup = new ResourceGroupModelData("resourcegroup_screen_01", null);
        ScreenModelData screen = new ScreenModelData("screen_resourceGroup_01", "serial_01", "reference_01",
                new Date(), new Date(), defaultInventoryStatus, 1.2D, "LCD", "COLOR");

        resourceGroup.save();
        screen.getResourceGroupIDs().add(resourceGroup.getId());
        ScreenModelData screenServer = this.saveObject(screen);
        resourceGroup.refresh();

        assertEquals(1, screenServer.getResourceGroupIDs().size());
        assertEquals(resourceGroup.getId(), screenServer.getResourceGroupIDs().get(0));
        assertEquals(1, resourceGroup.getResourceIds().size());
        assertEquals(screenServer.getId(), resourceGroup.getResourceIds().get(0));

        ResourceGroupModelData resourceGroup02 = new ResourceGroupModelData("resourcegroup_screen_02", null);
        resourceGroup02.save();
        screen.getResourceGroupIDs().add(resourceGroup02.getId());
        screen.save();
        screen.refresh();

        assertEquals(2, screen.getResourceGroupIDs().size());
        assertEquals(1, resourceGroup.getResourceIds().size());
        assertEquals(screen.getId(), resourceGroup.getResourceIds().get(0));

        resourceGroup.delete();
        resourceGroup02.delete();
        screen.delete();
    }
}
