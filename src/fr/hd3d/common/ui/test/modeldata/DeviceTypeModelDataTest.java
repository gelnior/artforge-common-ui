package fr.hd3d.common.ui.test.modeldata;

import org.junit.Test;

import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.modeldata.inventory.DeviceTypeModelData;
import fr.hd3d.common.ui.test.util.modeldata.reader.DeviceTypeReaderMock;


public class DeviceTypeModelDataTest extends ModelDataTest<DeviceTypeModelData, DeviceTypeReaderMock>
{
    @Override
    public void setUp()
    {
        super.setUp();

        this.reader = new DeviceTypeReaderMock();
    }

    @Test
    public void testPostDeviceType() throws Exception
    {
        DeviceTypeModelData deviceType = new DeviceTypeModelData();
        deviceType.setName("device_type_post_01");
        DeviceTypeModelData deviceTypeServer = this.saveObject(deviceType);

        assertEquals(deviceType.getName(), deviceTypeServer.getName());

        deviceType.delete();
    }

    @Test
    public void testPutDeviceType() throws Exception
    {
        DeviceTypeModelData deviceType = new DeviceTypeModelData("device_type_put_01");

        deviceType.save();
        deviceType.setName("device_type_put_01_updated");
        deviceType.save();

        DeviceTypeModelData deviceTypeServer = new DeviceTypeModelData();
        deviceTypeServer.setId(deviceType.getId());
        deviceTypeServer.refresh();

        assertEquals(deviceType.getName(), deviceTypeServer.getName());

        deviceType.delete();
    }

    @Test
    public void testDeleteDeviceType() throws Exception
    {
        DeviceTypeModelData deviceType = new DeviceTypeModelData("device_type_delete_01");

        deviceType.save();
        deviceType.delete();
        deviceType.refresh();

        assertEquals(CommonErrors.RECORD_NOT_ON_SERVER, controller.getLastError());
    }
}
