package fr.hd3d.common.ui.test.modeldata;

import org.junit.Test;

import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.inventory.ManufacturerModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ScreenModelModelData;
import fr.hd3d.common.ui.client.modeldata.reader.Hd3dModelDataReaderSingleton;
import fr.hd3d.common.ui.client.service.ServicesModelType;
import fr.hd3d.common.ui.test.util.modeldata.reader.ScreenModelReaderMock;
import fr.hd3d.common.ui.test.widget.sheeteditor.mock.Hd3dModelDataReaderMock;


public class ScreenModelModelDataTest extends ModelDataTest<ScreenModelModelData, ScreenModelReaderMock>
{
    @Override
    public void setUp()
    {
        super.setUp();

        this.reader = new ScreenModelReaderMock();
        Hd3dModelDataReaderSingleton.setInstanceAsMock(new Hd3dModelDataReaderMock());
        ServicesModelType.initModelTypes();
    }

    @Test
    public void testPostScreenModel() throws Exception
    {
        ScreenModelModelData screenModel = new ScreenModelModelData("screen_model_post_01");
        ScreenModelModelData screenModelServer = this.saveObject(screenModel);

        assertEquals(screenModel.getName(), screenModelServer.getName());

        screenModel.delete();
    }

    @Test
    public void testPutScreenModel() throws Exception
    {
        ScreenModelModelData screenModel = new ScreenModelModelData("screen_model_put_01");

        screenModel.save();
        screenModel.setName("screen_model_put_01_updated");
        screenModel.save();

        ScreenModelModelData screenModelServer = new ScreenModelModelData();
        screenModelServer.setId(screenModel.getId());
        screenModelServer.refresh();

        assertEquals(screenModel.getName(), screenModelServer.getName());

        screenModel.delete();
    }

    @Test
    public void testDeleteScreenModel() throws Exception
    {
        ScreenModelModelData screenModel = new ScreenModelModelData("screen_model_delete_01");

        screenModel.save();
        screenModel.delete();
        screenModel.refresh();

        assertEquals(CommonErrors.RECORD_NOT_ON_SERVER, controller.getLastError());
    }

    @Test
    public void testManufacturer() throws Exception
    {
        ScreenModelModelData screenModel = new ScreenModelModelData("screen_model_manufacturer_01");
        ManufacturerModelData manufacturer = new ManufacturerModelData("manufacturer_screen_model_01");
        manufacturer.save();

        screenModel.setManufacturerId(manufacturer.getId());
        ScreenModelModelData screenModelServer = this.saveObject(screenModel);

        Logger.out(screenModel.toPUTJson());
        assertEquals(manufacturer.getId(), screenModelServer.getManufacturerId());

        ManufacturerModelData manufacturer02 = new ManufacturerModelData("manufacturer_screen_model_02");
        manufacturer02.save();

        screenModel.setManufacturerId(manufacturer02.getId());
        screenModel.save();

        screenModelServer.refresh();
        assertEquals(manufacturer02.getId(), screenModelServer.getManufacturerId());

        screenModel.delete();
        manufacturer.delete();
        manufacturer02.delete();
    }
}
