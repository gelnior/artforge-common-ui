package fr.hd3d.common.ui.test.modeldata;

import org.junit.Test;

import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.test.util.modeldata.reader.PersonReaderMock;


/**
 * Test person service validity.
 * 
 * @author HD3D
 */

public class PersonModelDataTest extends ModelDataTest<PersonModelData, PersonReaderMock>
{

    @Override
    public void setUp()
    {
        super.setUp();

        this.reader = new PersonReaderMock();
    }

    @Test
    public void testPostPerson() throws Exception
    {
        PersonModelData myPostPerson = new PersonModelData("person_testPost_01_firstName",
                "person_testPost_01_lastName", "person_testPost_01_login", "person_testPost_01_email",
                "person_testPost_01_phone", "person_testPost_01_path");

        myPostPerson.refreshByLogin(null);
        if (myPostPerson.getId() == null)
        {
            myPostPerson.save();
        }

        PersonModelData myPostPersonServer = this.saveObject(myPostPerson);

        myPostPersonServer.setId(myPostPerson.getId());
        myPostPersonServer.refresh();

        assertEquals(myPostPerson.getFirstName(), myPostPersonServer.getFirstName());
        assertEquals(myPostPerson.getLastName(), myPostPersonServer.getLastName());
        assertEquals(myPostPerson.getLogin(), myPostPersonServer.getLogin());
        assertEquals(myPostPerson.getEmail(), myPostPersonServer.getEmail());
        assertEquals(myPostPerson.getPhone(), myPostPersonServer.getPhone());
        assertEquals(myPostPerson.getPhotoPath(), myPostPersonServer.getPhotoPath());

        myPostPersonServer.delete();
    }

    @Test
    public void testPutPerson() throws Exception
    {
        PersonModelData myPerson = new PersonModelData("person_testPut_01_firstName", "person_testPut_01_lastName",
                "person_testPut_01_login", "person_testPut_01_email", "person_testPut_01_phone",
                "person_testPut_01_path");

        PersonModelData myPutPerson = this.saveObject(myPerson);

        myPutPerson.setFirstName("person_testPut_01_firstName_Updated");
        myPutPerson.setPhone("person_testPut_01_phone_updated");

        PersonModelData myPutPersonServer = this.saveObject(myPutPerson);

        assertEquals(myPutPerson.getFirstName(), myPutPersonServer.getFirstName());
        assertEquals(myPutPerson.getLastName(), myPutPersonServer.getLastName());
        assertEquals(myPutPerson.getLogin(), myPutPersonServer.getLogin());
        assertEquals(myPutPerson.getEmail(), myPutPersonServer.getEmail());
        assertEquals(myPutPerson.getPhone(), myPutPersonServer.getPhone());
        assertEquals(myPutPerson.getPhotoPath(), myPutPersonServer.getPhotoPath());

        myPutPersonServer.delete();
    }

    @Test
    public void testDeletePerson() throws Exception
    {
        PersonModelData myDelPerson = new PersonModelData("person_testDel_01_firstName", "person_testDel_01_lastName",
                "person_testDel_01_login", "person_testDel_01_email", "person_testDel_01_phone",
                "person_testDel_01_path");

        myDelPerson.save();
        myDelPerson.delete();
        myDelPerson.refresh();

        assertEquals(CommonErrors.RECORD_NOT_ON_SERVER, controller.getLastError());
    }

    @Test
    public void testResourceGroupList() throws Exception
    {
        ResourceGroupModelData resourceGroup = new ResourceGroupModelData("person_testGrp_01_group", null);

        PersonModelData person = new PersonModelData("person_testGrp_01_firstName", "person_testGrp_01_lastName",
                "person_testGrp_01_login", "person_testGrp_01_email", "person_testGrp_01_phone",
                "person_testGrp_01_path");

        person.refreshByLogin(null);
        if (person.getId() == null)
        {
            person.save();
        }

        resourceGroup.getResourceIds().add(person.getId());
        resourceGroup.save();

        person.setResourceGroupIDs(CollectionUtils.asList(resourceGroup.getId()));

        PersonModelData personServer = this.saveObject(person);

        assertEquals(1, personServer.getResourceGroupIds().size());
        assertEquals(resourceGroup.getId(), personServer.getResourceGroupIds().get(0));

        resourceGroup.delete();
        person.delete();
    }
}
