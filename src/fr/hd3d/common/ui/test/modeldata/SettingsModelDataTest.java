package fr.hd3d.common.ui.test.modeldata;

import org.junit.Test;

import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.modeldata.technical.SettingModelData;
import fr.hd3d.common.ui.test.util.modeldata.reader.SettingReaderMock;


public class SettingsModelDataTest extends ModelDataTest<SettingModelData, SettingReaderMock>
{

    @Override
    public void setUp()
    {
        super.setUp();

        this.reader = new SettingReaderMock();
    }

    @Test
    public void testPostSetting() throws Exception
    {
        SettingModelData setting = new SettingModelData("key", "value");
        SettingModelData settingServer = this.saveObject(setting);

        assertEquals(setting.getKey(), settingServer.getKey());
        assertEquals(setting.getValue(), settingServer.getValue());
        assertNotNull(setting.getId());

        setting.delete();
    }

    @Test
    public void testPutSetting() throws Exception
    {
        SettingModelData setting = new SettingModelData("key", "value");

        setting.save();
        setting.setValue("value_new");
        setting.save();

        SettingModelData settingServer = new SettingModelData();
        settingServer.setId(setting.getId());
        settingServer.refresh();

        assertEquals(setting.getKey(), settingServer.getKey());
        assertEquals(setting.getValue(), settingServer.getValue());

        setting.delete();
    }

    @Test
    public void testDeleteSetting() throws Exception
    {
        SettingModelData setting = new SettingModelData("key", "value");

        setting.save();
        setting.delete();
        setting.refresh();

        assertEquals(CommonErrors.RECORD_NOT_ON_SERVER, controller.getLastError());
    }

}
