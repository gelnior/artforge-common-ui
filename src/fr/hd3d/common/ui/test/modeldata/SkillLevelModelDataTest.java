package fr.hd3d.common.ui.test.modeldata;

import org.junit.Test;

import fr.hd3d.common.client.enums.ESkillLevel;
import fr.hd3d.common.client.enums.ESkillMotivation;
import fr.hd3d.common.ui.client.modeldata.resource.SkillLevelModelData;
import fr.hd3d.common.ui.test.util.modeldata.reader.SkillLevelReaderMock;


public class SkillLevelModelDataTest extends ModelDataTest<SkillLevelModelData, SkillLevelReaderMock>
{
    @Override
    public void setUp()
    {
        super.setUp();

        this.reader = new SkillLevelReaderMock();
    }

    @Test
    public void testPostSkillLevel() throws Exception
    {
        SkillLevelModelData skillLevel = new SkillLevelModelData(ESkillLevel.HIGH.toString(),
                ESkillMotivation.LOW.toString());
        SkillLevelModelData skillLevelServer = this.saveObject(skillLevel);

        assertEquals(skillLevel.getLevel(), skillLevelServer.getLevel());
        assertEquals(skillLevel.getMotivation(), skillLevelServer.getMotivation());

        skillLevel.delete();
    }
    //
    // @Test
    // public void testPutSkillLevel() throws Exception
    // {
    // SkillLevelModelData skillLevel = new SkillLevelModelData("HIGH", "LOW");
    //
    // skillLevel.save();
    // skillLevel.setLevel("VERY_LOW");
    // skillLevel.setMotivation("VERY_LOW");
    // skillLevel.save();
    //
    // SkillLevelModelData skillLevelServer = new SkillLevelModelData();
    // skillLevelServer.setId(skillLevel.getId());
    // skillLevelServer.refresh();
    //
    // assertEquals(skillLevel.getLevel(), skillLevelServer.getLevel());
    // assertEquals(skillLevel.getMotivation(), skillLevelServer.getMotivation());
    //
    // skillLevel.delete();
    // }
    //
    // @Test
    // public void testDeleteSkillLevel() throws Exception
    // {
    // SkillLevelModelData skillLevel = new SkillLevelModelData("HIGH", "LOW");
    // skillLevel.save();
    // skillLevel.delete();
    // skillLevel.refresh();
    //
    // assertEquals(CommonErrors.RECORD_NOT_ON_SERVER, controller.getLastError());
    // }
    //
    // @Test
    // public void testPerson() throws Exception
    // {
    // SkillLevelModelData skillLevel = new SkillLevelModelData("HIGH", "LOW");
    // PersonModelData person = new PersonModelData("person_skilllevel_01", "last-name", "login", "email",
    // "01010101010", "path/");
    // person.save();
    //
    // skillLevel.setPersonId(person.getId());
    // SkillLevelModelData skillLevelServer = this.saveObject(skillLevel);
    //
    // assertEquals(person.getId(), skillLevelServer.getPersonId());
    //
    // PersonModelData person02 = new PersonModelData("person_skilllevel_02", "last-name", "login", "email",
    // "01010101010", "path/");
    // person02.save();
    //
    // skillLevel.setPersonId(person02.getId());
    // skillLevel.save();
    //
    // skillLevelServer.refresh();
    // // assertEquals(person02.getId(), skillLevelServer.getPersonId());
    //
    // skillLevel.delete();
    // person.delete();
    // person02.delete();
    // }
    //
    // @Test
    // public void testSkill() throws Exception
    // {
    // SkillLevelModelData skillLevel = new SkillLevelModelData("HIGH", "LOW");
    // SkillModelData skill = new SkillModelData("skill_skilllevel_01");
    // skill.save();
    //
    // skillLevel.setSkillId(skill.getId());
    // SkillLevelModelData skillLevelServer = this.saveObject(skillLevel);
    //
    // assertEquals(skill.getId(), skillLevelServer.getSkillId());
    //
    // SkillModelData skill02 = new SkillModelData("skill_skilllevel_02");
    // skill02.save();
    //
    // skillLevel.setSkillId(skill02.getId());
    // skillLevel.save();
    //
    // skillLevelServer.refresh();
    // assertEquals(skill02.getId(), skillLevelServer.getSkillId());
    //
    // skillLevel.delete();
    // skill.delete();
    // skill02.delete();
    // }
}
