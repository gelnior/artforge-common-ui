package fr.hd3d.common.ui.test.modeldata;

import java.util.Date;

import org.junit.Test;

import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.common.ui.client.modeldata.technical.RoleModelData;
import fr.hd3d.common.ui.test.util.TestUtils;
import fr.hd3d.common.ui.test.util.modeldata.reader.ResourceGroupReaderMock;


/**
 * Test resource group service validity.
 * 
 * @author HD3D
 */
public class ResourceGroupModelDataTest extends ModelDataTest<ResourceGroupModelData, ResourceGroupReaderMock>
{
    @Override
    public void setUp()
    {
        super.setUp();

        this.reader = new ResourceGroupReaderMock();
    }

    @Test
    public void testPostResourceGroup() throws Exception
    {
        ResourceGroupModelData group = new ResourceGroupModelData("resourcegroup_post_01", null);
        ResourceGroupModelData groupServer = this.saveObject(group);

        assertEquals(group.getName(), groupServer.getName());

        groupServer.delete();
    }

    @Test
    public void testPutResourceGroup() throws Exception
    {
        ResourceGroupModelData resourceGroup = new ResourceGroupModelData("resourcegroup_put_01", null);

        resourceGroup.save();
        resourceGroup.setName("resourcegroup_put_01_udpated");
        resourceGroup.save();

        ResourceGroupModelData resourceGroupServer = new ResourceGroupModelData();
        resourceGroupServer.setId(resourceGroup.getId());
        resourceGroupServer.refresh();

        assertEquals(resourceGroup.getName(), resourceGroupServer.getName());

        resourceGroup.delete();
    }

    @Test
    public void testDeleteResourceGroup() throws Exception
    {
        ResourceGroupModelData resourceGroup = new ResourceGroupModelData("resourcegroup_delete_01", null);

        resourceGroup.save();
        resourceGroup.delete();
        resourceGroup.refresh();

        // assertEquals(CommonErrors.RECORD_NOT_ON_SERVER, controller.getLastError());
    }

    @Test
    public void testAddResourceToResourceGroup() throws Exception
    {
        ResourceGroupModelData resourceGroup = new ResourceGroupModelData("resourcegroup_computer_01", null);
        ComputerModelData computer = new ComputerModelData("comp_resourcegroup_01", "serial_01", "billing_01",
                new Date(), new Date(), defaultInventoryStatus, 123L, "fr.hd3d.laptop01", "127.0.0.1", "mac_adress",
                1200L, 2L, 2L, 4096L, "NO_WORKER", 160L, 35L);

        computer.save();
        resourceGroup.getResourceIds().add(computer.getId());

        ResourceGroupModelData resourceGroupServer = this.saveObject(resourceGroup);

        assertEquals(1, resourceGroupServer.getResourceIds().size());
        assertEquals(computer.getId(), resourceGroupServer.getResourceIds().get(0));

        computer.delete();
        resourceGroup.delete();
    }

    @Test
    public void testResourceGroupHierarchy() throws Exception
    {
        ResourceGroupModelData parent01 = new ResourceGroupModelData("resourcegroup_parent_01", null);
        ResourceGroupModelData parent02 = new ResourceGroupModelData("resourcegroup_parent_02", null);

        ResourceGroupModelData child01 = new ResourceGroupModelData("resourcegroup_child_01", null);
        ResourceGroupModelData child02 = new ResourceGroupModelData("resourcegroup_child_02", null);
        ResourceGroupModelData child03 = new ResourceGroupModelData("resourcegroup_child_03", null);

        parent01.save();
        parent02.save();

        child01.setParentId(parent01.getId());
        child02.setParentId(parent01.getId());

        ResourceGroupModelData child01Server = this.saveObject(child01);
        ResourceGroupModelData child02Server = this.saveObject(child02);
        child03.save();

        assertEquals(parent01.getId().longValue(), child01Server.getParentId().longValue());
        assertEquals(parent01.getId(), child02Server.getParentId());

        child01.setParentId(parent02.getId());
        child03.setParentId(parent02.getId());
        child01.save();
        child03.save();
        child01.refresh();
        child03.refresh();

        assertEquals(child01.getParentId(), parent02.getId());
        assertEquals(child03.getParentId(), parent02.getId());

        parent01.delete();
        child01.delete();
        child02.delete();
        child03.delete();
    }

    @Test
    public void testRoleList() throws Exception
    {
        RoleModelData role = new RoleModelData("role_resourcegroup_01");
        ResourceGroupModelData resourceGroup = new ResourceGroupModelData("resourcegroup_role_01", null);

        role.save();
        resourceGroup.getRoleIds().add(role.getId());
        ResourceGroupModelData resourceGroupServer = this.saveObject(resourceGroup);
        role.refresh();

        assertEquals(1, resourceGroupServer.getRoleIds().size());
        assertEquals(role.getId(), resourceGroupServer.getRoleIds().get(0));
        assertEquals(1, role.getResourceGroupIds().size());
        assertEquals(resourceGroupServer.getId(), role.getResourceGroupIds().get(0));

        RoleModelData role02 = new RoleModelData("resourcegroup_resourcegroup_02");
        role02.save();
        resourceGroup.getRoleIds().add(role02.getId());
        resourceGroup.save();
        resourceGroup.refresh();

        assertEquals(2, resourceGroup.getRoleIds().size());
        assertEquals(1, role.getResourceGroupIds().size());
        assertEquals(resourceGroup.getId(), role.getResourceGroupIds().get(0));

        role.delete();
        role02.delete();
        resourceGroup.delete();
    }

    @Test
    public void testProjectList() throws Exception
    {
        ProjectModelData project = TestUtils.getOpenProject("project_resourcegroup_01");
        project.getResourceGroupIds().clear();
        project.save();

        ResourceGroupModelData resourceGroup = new ResourceGroupModelData("resourcegroup_project_01", null);

        project.save();
        resourceGroup.getProjectIds().add(project.getId());
        ResourceGroupModelData resourceGroupServer = this.saveObject(resourceGroup);
        project.refresh();

        assertEquals(1, resourceGroupServer.getProjectIds().size());
        assertEquals(project.getId(), resourceGroupServer.getProjectIds().get(0));
        assertEquals(1, project.getResourceGroupIds().size());
        assertEquals(resourceGroupServer.getId(), project.getResourceGroupIds().get(0));

        ProjectModelData project02 = TestUtils.getOpenProject("project_resourcegroup_02");
        project02.save();
        resourceGroup.getProjectIds().add(project02.getId());
        resourceGroup.save();
        resourceGroup.refresh();

        assertEquals(2, resourceGroup.getProjectIds().size());
        assertEquals(1, project.getResourceGroupIds().size());
        assertEquals(resourceGroup.getId(), project.getResourceGroupIds().get(0));

        project.delete();
        project02.delete();
        resourceGroup.delete();
    }
}
