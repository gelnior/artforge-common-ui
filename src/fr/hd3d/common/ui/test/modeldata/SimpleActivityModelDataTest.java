package fr.hd3d.common.ui.test.modeldata;

import java.util.Calendar;

import org.junit.Test;

import fr.hd3d.common.client.enums.ESimpleActivityType;
import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.task.PersonDayModelData;
import fr.hd3d.common.ui.client.modeldata.task.SimpleActivityModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.test.util.TestUtils;
import fr.hd3d.common.ui.test.util.modeldata.reader.SimpleActivityReaderMock;


public class SimpleActivityModelDataTest extends ModelDataTest<SimpleActivityModelData, SimpleActivityReaderMock>
{
    Calendar myDate = Calendar.getInstance();

    PersonModelData myPerson = new PersonModelData("PersonSimpleActivityTest_firstName",
            "PersonSimpleActivityTest_lastName", "PersonSimpleActivityTest_login", "PersonSimpleActivityTest_email",
            "PersonSimpleActivityTest_phone", "PersonSimpleActivityTest_photoPath");

    ProjectModelData myProject;

    PersonDayModelData myPersonDay = new PersonDayModelData();

    @Override
    public void setUp()
    {
        super.setUp();

        this.reader = new SimpleActivityReaderMock();
        this.myProject = TestUtils.getDefaultProject();

        myDate.set(110, 4, 28, 22, 45, 00);
        myDate.set(14, 0);

        myPerson.refreshByLogin(null);
        if (myPerson.getId() == null)
        {
            myPerson.save();
        }

        myProject.refreshByName(CommonEvents.MODEL_DATA_REFRESH_SUCCESS);

        if (myProject.getId() == null)
        {
            myProject.save();
        }

        myPersonDay.refreshOrCreate(null);
        if (myPersonDay.getId() == null)
        {
            myPersonDay.setDate(myDate.getTime());
            myPersonDay.setPersonId(myPerson.getId());
            myPersonDay.save();
        }
    }

    @Test
    public void testPostSimpleActivity() throws Exception
    {
        SimpleActivityModelData myPostSimpleActivity = new SimpleActivityModelData();
        myPostSimpleActivity.setProjectId(myProject.getId());
        myPostSimpleActivity.setType(ESimpleActivityType.MEETING.toString());
        myPostSimpleActivity.setWorkerId(myPerson.getId());
        myPostSimpleActivity.setFilledDate(myDate.getTime());
        myPostSimpleActivity.setFilledById(myPerson.getId());
        myPostSimpleActivity.setDayId(myPersonDay.getId());
        myPostSimpleActivity.setDuration((long) 34);

        SimpleActivityModelData myPostSimpleActivityServer = this.saveObject(myPostSimpleActivity);

        assertEquals(myPostSimpleActivity.getProjectId(), myPostSimpleActivityServer.getProjectId());
        assertEquals(myPostSimpleActivity.getType(), myPostSimpleActivityServer.getType());

        myPostSimpleActivity.setDefaultPath(ServicesPath.SIMPLE_ACTIVITIES);
        myPostSimpleActivity.setId(null);
        myPostSimpleActivity.save();
        assertEquals(CommonErrors.SERVER_ERROR, controller.getLastError());

        myPostSimpleActivityServer.delete();
    }

    @Test
    public void testPutSimpleActivity() throws Exception
    {
        SimpleActivityModelData mySimpleActivity = new SimpleActivityModelData();
        mySimpleActivity.setProjectId(myProject.getId());
        mySimpleActivity.setType("meeting");
        mySimpleActivity.setWorkerId(myPerson.getId());
        mySimpleActivity.setFilledDate(myDate.getTime());
        mySimpleActivity.setFilledById(myPerson.getId());
        mySimpleActivity.setDayId(myPersonDay.getId());
        mySimpleActivity.setDuration((long) 34);
        SimpleActivityModelData myPutSimpleActivity = this.saveObject(mySimpleActivity);

        myPutSimpleActivity.setType(ESimpleActivityType.OTHER.toString());
        myPutSimpleActivity.setDuration((long) 436);

        SimpleActivityModelData myPutSimpleActivityServer = this.saveObject(myPutSimpleActivity);

        assertEquals(myPutSimpleActivity.getType(), myPutSimpleActivityServer.getType());
        assertEquals(myPutSimpleActivity.getDuration(), myPutSimpleActivityServer.getDuration());

        myPutSimpleActivityServer.delete();
    }

    @Test
    public void testDeleteSimpleActivity() throws Exception
    {
        SimpleActivityModelData myDelSimpleActivity = new SimpleActivityModelData();
        myDelSimpleActivity.setProjectId(myProject.getId());
        myDelSimpleActivity.setType("meeting");
        myDelSimpleActivity.setWorkerId(myPerson.getId());
        myDelSimpleActivity.setFilledDate(myDate.getTime());
        myDelSimpleActivity.setFilledById(myPerson.getId());
        myDelSimpleActivity.setDayId(myPersonDay.getId());
        myDelSimpleActivity.setDuration((long) 34);

        myDelSimpleActivity.save();
        myDelSimpleActivity.delete();
        myDelSimpleActivity.refresh();

        assertEquals(CommonErrors.RECORD_NOT_ON_SERVER, controller.getLastError());
    }

    @Test
    public void testWorker() throws Exception
    {
        SimpleActivityModelData mySimpleActivity = new SimpleActivityModelData();
        mySimpleActivity.setProjectId(myProject.getId());
        mySimpleActivity.setType("meeting");
        mySimpleActivity.setWorkerId(myPerson.getId());
        mySimpleActivity.setFilledDate(myDate.getTime());
        mySimpleActivity.setFilledById(myPerson.getId());
        mySimpleActivity.setDayId(myPersonDay.getId());
        mySimpleActivity.setDuration((long) 34);

        SimpleActivityModelData mySimpleActivityPosted = this.saveObject(mySimpleActivity);

        assertEquals(myPerson.getId(), mySimpleActivityPosted.getWorkerId());
        assertEquals(myPerson.getLastName() + " " + myPerson.getFirstName() + " - " + myPerson.getLogin(),
                mySimpleActivityPosted.getWorkerName());
        assertEquals(myPerson.getLogin(), mySimpleActivityPosted.getWorkerLogin());

        mySimpleActivityPosted.delete();

    }

    @Test
    public void testProject() throws Exception
    {
        SimpleActivityModelData mySimpleActivity = new SimpleActivityModelData();
        mySimpleActivity.setProjectId(myProject.getId());
        mySimpleActivity.setType("meeting");
        mySimpleActivity.setWorkerId(myPerson.getId());
        mySimpleActivity.setFilledDate(myDate.getTime());
        mySimpleActivity.setFilledById(myPerson.getId());
        mySimpleActivity.setDayId(myPersonDay.getId());
        mySimpleActivity.setDuration((long) 34);

        SimpleActivityModelData mySimpleActivityPosted = this.saveObject(mySimpleActivity);

        assertEquals(myProject.getId(), mySimpleActivityPosted.getProjectId());
        assertEquals(myProject.getName(), mySimpleActivityPosted.getProjectName());
        assertEquals(myProject.getColor(), mySimpleActivityPosted.getProjectColor());

        mySimpleActivityPosted.delete();
    }

    @Test
    public void testFilled() throws Exception
    {
        SimpleActivityModelData mySimpleActivity = new SimpleActivityModelData();
        mySimpleActivity.setProjectId(myProject.getId());
        mySimpleActivity.setType("meeting");
        mySimpleActivity.setWorkerId(myPerson.getId());
        mySimpleActivity.setFilledDate(myDate.getTime());
        mySimpleActivity.setFilledById(myPerson.getId());
        mySimpleActivity.setDayId(myPersonDay.getId());
        mySimpleActivity.setDuration((long) 34);

        SimpleActivityModelData mySimpleActivityPosted = this.saveObject(mySimpleActivity);

        assertEquals(myPerson.getId(), mySimpleActivityPosted.getFilledById());
        assertEquals(myPerson.getLastName() + " " + myPerson.getFirstName() + " - " + myPerson.getLogin(),
                mySimpleActivityPosted.getFilledByName());
        assertEquals(myPerson.getLogin(), mySimpleActivityPosted.getFilledByLogin());

        mySimpleActivityPosted.delete();
    }
}
