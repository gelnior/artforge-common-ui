package fr.hd3d.common.ui.test.modeldata;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.RoomModelData;
import fr.hd3d.common.ui.client.service.BulkRequests;
import fr.hd3d.common.ui.test.UITestCase;
import fr.hd3d.common.ui.test.util.controller.ErrorController;


/**
 * Tests for BulkRequest class.
 * 
 * @author HD3D
 */
public class BulkTest extends UITestCase
{
    private final ErrorController errorController = new ErrorController();

    @Override
    public void setUp()
    {
        super.setUp();

        EventDispatcher.get().addController(errorController);
        errorController.reset();
    }

    @Test
    public void testBulkPost()
    {
        RoomModelData room01 = new RoomModelData("room_bulk_post_test_01");
        RoomModelData room02 = new RoomModelData("room_bulk_post_test_02");

        List<Hd3dModelData> rooms = new ArrayList<Hd3dModelData>();
        rooms.add(room01);
        rooms.add(room02);

        BulkRequests.bulkPost(rooms, CommonEvents.MODEL_DATA_CREATION_SUCCESS);
        room01.refreshByName(CommonEvents.MODEL_DATA_REFRESH_SUCCESS);
        room02.refreshByName(CommonEvents.MODEL_DATA_REFRESH_SUCCESS);

        assertNotNull(room01.getId());
        assertNotNull(room02.getId());
        assertEquals(-1, errorController.getLastError());

        room01.delete();
        room02.delete();
    }

    @Test
    public void testBulkPut()
    {
        RoomModelData room01 = new RoomModelData("room_bulk_put_test_01");
        RoomModelData room02 = new RoomModelData("room_bulk_put_test_02");

        room01.save();
        room02.save();

        room01.setName("room_bulk_put_test_01_updated");
        room02.setName("room_bulk_put_test_02_updated");

        List<Hd3dModelData> rooms = new ArrayList<Hd3dModelData>();
        rooms.add(room01);
        rooms.add(room02);

        BulkRequests.bulkPut(rooms, CommonEvents.MODEL_DATA_UPDATE_SUCCESS);

        RoomModelData roomServer01 = new RoomModelData();
        RoomModelData roomServer02 = new RoomModelData();

        roomServer01.setId(room01.getId());
        roomServer02.setId(room02.getId());

        roomServer01.refresh();
        roomServer02.refresh();

        assertEquals(room01.getName(), roomServer01.getName());
        assertEquals(room02.getName(), roomServer02.getName());

        room01.delete();
        room02.delete();
    }
}
