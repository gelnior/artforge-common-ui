package fr.hd3d.common.ui.test.modeldata;

import java.text.SimpleDateFormat;

import org.junit.Test;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.reader.Hd3dModelDataReaderSingleton;
import fr.hd3d.common.ui.client.modeldata.writer.JsonWriterSingleton;
import fr.hd3d.common.ui.client.modeldata.writer.ModelDataJsonWriterSingleton;
import fr.hd3d.common.ui.client.service.ServicesModelType;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.test.UITestCase;
import fr.hd3d.common.ui.test.util.controller.ErrorController;
import fr.hd3d.common.ui.test.util.modeldata.reader.Hd3dJsonReaderMock;
import fr.hd3d.common.ui.test.util.modeldata.writer.JSONWriterMock;
import fr.hd3d.common.ui.test.util.modeldata.writer.ModelDataJsonWriterMock;
import fr.hd3d.common.ui.test.widget.sheeteditor.mock.Hd3dModelDataReaderMock;


/**
 * Base Test case for every model data tests.
 * 
 * @author HD3D
 */
public class ModelDataTest<C extends Hd3dModelData, D extends Hd3dJsonReaderMock<C>> extends UITestCase
{
    protected ErrorController controller = new ErrorController();

    protected D reader;
    protected String path;
    protected SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    protected static String defaultInventoryStatus = "AVAILABLE";

    @Override
    public void setUp()
    {
        super.setUp();

        ServicesPath.initPath();

        ModelDataJsonWriterSingleton.setInstanceAsMock(new ModelDataJsonWriterMock());
        JsonWriterSingleton.setInstanceAsMock(new JSONWriterMock());
        Hd3dModelDataReaderSingleton.setInstanceAsMock(new Hd3dModelDataReaderMock());
        ServicesModelType.initModelTypes();

        EventDispatcher.get().addController(controller);
    }

    /**
     * Save object to database, then return a new object from same type of which data comes from services (same id is
     * set then refresh method is called).
     * 
     * @param object
     *            The object to save.
     * @return The copy of the object with data retrieved from services.
     */
    protected C saveObject(C object)
    {
        object.save();
        assertNotNull(object.getId());

        C objectServer = this.reader.newModelInstance();
        objectServer.setId(object.getId());
        objectServer.refresh();

        return objectServer;
    }

    /**
     * Useless test to avoid failing when whole test suite is ran.
     */
    @Test
    public void testNothing()
    {
        assertTrue(true);
    }
}
