package fr.hd3d.common.ui.test.modeldata;

import org.junit.Test;

import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.modeldata.inventory.SoftwareModelData;
import fr.hd3d.common.ui.client.modeldata.resource.SkillModelData;
import fr.hd3d.common.ui.test.util.modeldata.reader.SkillReaderMock;


public class SkillModelDataTest extends ModelDataTest<SkillModelData, SkillReaderMock>
{
    @Override
    public void setUp()
    {
        super.setUp();

        this.reader = new SkillReaderMock();
    }

    @Test
    public void testPostSkill() throws Exception
    {
        SkillModelData skill = new SkillModelData("skill_post_01");
        SkillModelData skillServer = this.saveObject(skill);

        assertEquals(skill.getName(), skillServer.getName());

        skill.delete();
    }

    @Test
    public void testPutSkill() throws Exception
    {
        SkillModelData skill = new SkillModelData("skill_put_01");

        skill.save();
        skill.setName("skill_put_01_updated");
        skill.save();

        SkillModelData skillServer = new SkillModelData();
        skillServer.setId(skill.getId());
        skillServer.refresh();

        assertEquals(skill.getName(), skillServer.getName());

        skill.delete();
    }

    @Test
    public void testDeleteSkill() throws Exception
    {
        SkillModelData skill = new SkillModelData("skill_delete_01");

        skill.save();
        skill.delete();
        skill.refresh();

        assertEquals(CommonErrors.RECORD_NOT_ON_SERVER, controller.getLastError());
    }

    @Test
    public void testSoftware() throws Exception
    {
        SkillModelData skill = new SkillModelData("skill_software_01");
        SoftwareModelData software = new SoftwareModelData("software_skill_01");
        software.save();

        skill.setSoftwareId(software.getId());
        SkillModelData skillServer = this.saveObject(skill);

        assertEquals(software.getId(), skillServer.getSoftwareId());

        SoftwareModelData software02 = new SoftwareModelData("software_skill_02");
        software02.save();

        skill.setSoftwareId(software02.getId());
        skill.save();

        skillServer.refresh();
        assertEquals(software02.getId(), skillServer.getSoftwareId());

        skill.delete();
        software.delete();
        software02.delete();
    }
}
