package fr.hd3d.common.ui.test.modeldata;

import java.util.Date;

import org.junit.Test;

import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.modeldata.resource.ContractModelData;
import fr.hd3d.common.ui.client.modeldata.resource.OrganizationModelData;
import fr.hd3d.common.ui.client.modeldata.resource.QualificationModelData;
import fr.hd3d.common.ui.test.util.modeldata.reader.ContractReaderMock;


public class ContractModelDataTest extends ModelDataTest<ContractModelData, ContractReaderMock>
{
    @Override
    public void setUp()
    {
        super.setUp();

        this.reader = new ContractReaderMock();
    }

    @Test
    public void testPostContract() throws Exception
    {
        ContractModelData contract = new ContractModelData("contract_post_01", new Date(), new Date(), new Date(),
                "/path/", "CDI", new Float(200));
        ContractModelData contractServer = this.saveObject(contract);

        assertEquals(contract.getName(), contractServer.getName());
        assertEquals(contract.getStartDate().toString(), contractServer.getStartDate().toString());
        assertEquals(contract.getEndDate().toString(), contractServer.getEndDate().toString());
        assertEquals(contract.getRealEndDate().toString(), contractServer.getRealEndDate().toString());
        assertEquals(contract.getDocPath(), contractServer.getDocPath());
        assertEquals(contract.getType(), contractServer.getType());

        contract.delete();
    }

    @Test
    public void testPutContract() throws Exception
    {
        ContractModelData contract = new ContractModelData("contract_put_01", new Date(), new Date(), new Date(),
                "/path/", "CDI", new Float(200));

        contract.save();
        contract.setName("contract_put_01_updated");
        contract.setStartDate(new Date());
        contract.setEndDate(new Date());
        contract.setRealEndDate(new Date());
        contract.setDocPath("/newpath/");
        contract.setType("CDD");
        contract.save();

        ContractModelData contractServer = new ContractModelData();
        contractServer.setId(contract.getId());
        contractServer.refresh();

        assertEquals(contract.getName(), contractServer.getName());
        assertEquals(contract.getStartDate().toString(), contractServer.getStartDate().toString());
        assertEquals(contract.getEndDate().toString(), contractServer.getEndDate().toString());
        assertEquals(contract.getRealEndDate().toString(), contractServer.getRealEndDate().toString());
        assertEquals(contract.getDocPath(), contractServer.getDocPath());
        assertEquals(contract.getType(), contractServer.getType());

        contract.delete();
    }

    @Test
    public void testDeleteContract() throws Exception
    {
        ContractModelData contract = new ContractModelData("contract_delete_01", new Date(), new Date(), new Date(),
                "/path/", "CDI", new Float(200));

        contract.save();
        contract.delete();
        contract.refresh();

        assertEquals(CommonErrors.RECORD_NOT_ON_SERVER, controller.getLastError());
    }

    @Test
    public void testPerson() throws Exception
    {
    // ContractModelData contract = new ContractModelData("contract_software_01");
    // PersonModelData software = new PersonModelData("software_contract_01", "last", "login", "pass", "", "", "path");
    // software.save();
    //
    // contract.setPersonId(software.getId());
    // ContractModelData contractServer = this.saveObject(contract);
    //
    // assertEquals(software.getId(), contractServer.getPersonId());
    //
    // PersonModelData software02 = new PersonModelData("software_contract_02", "last", "login", "pass", "", "",
    // "path");
    // software02.save();
    //
    // contract.setPersonId(software02.getId());
    // contract.save();
    //
    // contractServer.refresh();
    // assertEquals(software02.getId(), contractServer.getPersonId());
    //
    // contract.delete();
    // software.delete();
    // software02.delete();
    }

    @Test
    public void testQualification() throws Exception
    {
        ContractModelData contract = new ContractModelData("contract_qualification_01", new Date(), new Date(),
                new Date(), "/path/", "CDI", new Float(200));
        QualificationModelData qualification = new QualificationModelData("qualification_contract_01");
        qualification.save();

        contract.setQualificationId(qualification.getId());
        ContractModelData contractServer = this.saveObject(contract);

        assertEquals(qualification.getId(), contractServer.getQualificationId());

        QualificationModelData qualification02 = new QualificationModelData("qualification_contract_02");
        qualification02.save();

        contract.setQualificationId(qualification02.getId());
        contract.save();

        contractServer.refresh();
        assertEquals(qualification02.getId(), contractServer.getQualificationId());

        contract.delete();
        qualification.delete();
        qualification02.delete();
    }

    @Test
    public void testOrganization() throws Exception
    {
        ContractModelData contract = new ContractModelData("contract_organization_01", new Date(), new Date(),
                new Date(), "/path/", "CDI", new Float(200));
        OrganizationModelData organization = new OrganizationModelData("organization_contract_01", null);
        organization.save();

        contract.setOrganizationId(organization.getId());
        ContractModelData contractServer = this.saveObject(contract);

        assertEquals(organization.getId(), contractServer.getOrganizationId());

        OrganizationModelData organization02 = new OrganizationModelData("organization_contract_02", null);
        organization02.save();

        contract.setOrganizationId(organization02.getId());
        contract.save();

        contractServer.refresh();
        assertEquals(organization02.getId(), contractServer.getOrganizationId());

        contract.delete();
        organization.delete();
        organization02.delete();
    }
}
