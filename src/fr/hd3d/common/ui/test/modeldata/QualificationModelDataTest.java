package fr.hd3d.common.ui.test.modeldata;

import org.junit.Test;

import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.modeldata.resource.QualificationModelData;
import fr.hd3d.common.ui.client.modeldata.resource.SkillModelData;
import fr.hd3d.common.ui.test.util.modeldata.reader.QualificationReaderMock;


public class QualificationModelDataTest extends ModelDataTest<QualificationModelData, QualificationReaderMock>
{
    @Override
    public void setUp()
    {
        super.setUp();

        this.reader = new QualificationReaderMock();
    }

    @Test
    public void testPostQualification() throws Exception
    {
        QualificationModelData qualification = new QualificationModelData("qualification_post_01");
        QualificationModelData qualificationServer = this.saveObject(qualification);

        assertEquals(qualification.getName(), qualificationServer.getName());

        qualification.delete();
    }

    @Test
    public void testPutQualification() throws Exception
    {
        QualificationModelData qualification = new QualificationModelData("qualification_put_01");

        qualification.save();
        qualification.setName("qualification_put_01_updated");
        qualification.save();

        QualificationModelData qualificationServer = new QualificationModelData();
        qualificationServer.setId(qualification.getId());
        qualificationServer.refresh();

        assertEquals(qualification.getName(), qualificationServer.getName());

        qualification.delete();
    }

    @Test
    public void testDeleteQualification() throws Exception
    {
        QualificationModelData qualification = new QualificationModelData("qualification_delete_01");

        qualification.save();
        qualification.delete();
        qualification.refresh();

        assertEquals(CommonErrors.RECORD_NOT_ON_SERVER, controller.getLastError());
    }

    @Test
    public void testSkillList() throws Exception
    {
        SkillModelData skill = new SkillModelData("skill_qualification_01");
        QualificationModelData qualification = new QualificationModelData("qualification_skill_01");

        skill.save();
        qualification.getSkillIDs().add(skill.getId());
        QualificationModelData qualificationServer = this.saveObject(qualification);
        skill.refresh();

        assertEquals(1, qualificationServer.getSkillIDs().size());
        assertEquals(skill.getId(), qualificationServer.getSkillIDs().get(0));

        SkillModelData skill02 = new SkillModelData("qualification_qualification_02");
        skill02.save();
        qualification.getSkillIDs().add(skill02.getId());
        qualification.save();
        qualification.refresh();

        assertEquals(2, qualification.getSkillIDs().size());

        skill.delete();
        skill02.delete();
        qualification.delete();
    }
}
