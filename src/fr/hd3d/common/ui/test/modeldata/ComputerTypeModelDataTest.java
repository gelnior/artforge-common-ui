package fr.hd3d.common.ui.test.modeldata;

import org.junit.Test;

import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerTypeModelData;
import fr.hd3d.common.ui.test.util.modeldata.reader.ComputerTypeReaderMock;


public class ComputerTypeModelDataTest extends ModelDataTest<ComputerTypeModelData, ComputerTypeReaderMock>
{
    @Override
    public void setUp()
    {
        super.setUp();

        this.reader = new ComputerTypeReaderMock();
    }

    @Test
    public void testPostComputerType() throws Exception
    {
        ComputerTypeModelData computerType = new ComputerTypeModelData("computer_type_post_01");
        ComputerTypeModelData computerTypeServer = this.saveObject(computerType);

        assertEquals(computerType.getName(), computerTypeServer.getName());

        computerType.delete();
    }

    @Test
    public void testPutComputerType() throws Exception
    {
        ComputerTypeModelData computerType = new ComputerTypeModelData("computer_type_put_01");

        computerType.save();
        computerType.setName("computer_type_put_01_updated");
        computerType.save();

        ComputerTypeModelData computerTypeServer = new ComputerTypeModelData();
        computerTypeServer.setId(computerType.getId());
        computerTypeServer.refresh();

        assertEquals(computerType.getName(), computerTypeServer.getName());

        computerType.delete();
    }

    @Test
    public void testDeleteComputerType() throws Exception
    {
        ComputerTypeModelData computerType = new ComputerTypeModelData("computer_type_delete_01");

        computerType.save();
        computerType.delete();
        computerType.refresh();

        assertEquals(CommonErrors.RECORD_NOT_ON_SERVER, controller.getLastError());
    }
}
