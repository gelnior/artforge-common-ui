package fr.hd3d.common.ui.test.modeldata;

import org.junit.Test;

import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.modeldata.inventory.DeviceModelModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ManufacturerModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.test.util.modeldata.reader.DeviceModelReaderMock;


public class DeviceModelModelDataTest extends ModelDataTest<DeviceModelModelData, DeviceModelReaderMock>
{
    @Override
    public void setUp()
    {
        super.setUp();

        this.reader = new DeviceModelReaderMock();
        this.path = ServicesPath.getPath("DeviceModel");
    }

    @Test
    public void testPostDeviceModel() throws Exception
    {
        DeviceModelModelData deviceModel = new DeviceModelModelData("device_model_post_01");

        DeviceModelModelData postedDeviceModel = this.saveObject(deviceModel);
        assertEquals(deviceModel.getName(), postedDeviceModel.getName());

        postedDeviceModel.delete();
    }

    @Test
    public void testPutDeviceModel() throws Exception
    {
        DeviceModelModelData deviceModel = new DeviceModelModelData("device_model_put_01");
        DeviceModelModelData putDeviceModel = this.saveObject(deviceModel);

        putDeviceModel.setName("device_model_put_01_updated");
        DeviceModelModelData putDeviceModel_server = this.saveObject(putDeviceModel);
        assertEquals(putDeviceModel.getName(), putDeviceModel_server.getName());

        putDeviceModel.delete();
    }

    @Test
    public void testDeleteDeviceModel() throws Exception
    {
        DeviceModelModelData deviceModel = new DeviceModelModelData("device_model_delete_01");

        deviceModel.save();
        deviceModel.delete();
        deviceModel.refresh();

        assertEquals(CommonErrors.RECORD_NOT_ON_SERVER, controller.getLastError());
    }

    @Test
    public void testManufacturer() throws Exception
    {
        DeviceModelModelData deviceModel = new DeviceModelModelData("comp_model_manufacturer_01");
        ManufacturerModelData manufacturer = new ManufacturerModelData("manufacturer_comp_model_01");

        manufacturer.save();
        deviceModel.setManufacturerId(manufacturer.getId());
        DeviceModelModelData postedDeviceModel = this.saveObject(deviceModel);
        assertEquals(manufacturer.getId().longValue(), postedDeviceModel.getManufacturerId().longValue());

        ManufacturerModelData manufacturer02 = new ManufacturerModelData("manufacturer_comp_model_02");
        manufacturer02.save();
        postedDeviceModel.setManufacturerId(manufacturer02.getId());
        DeviceModelModelData putDeviceModel = this.saveObject(postedDeviceModel);
        manufacturer02.refresh();

        assertEquals(manufacturer02.getId().longValue(), putDeviceModel.getManufacturerId().longValue());

        manufacturer.delete();
        manufacturer02.delete();
        putDeviceModel.delete();
    }
}
