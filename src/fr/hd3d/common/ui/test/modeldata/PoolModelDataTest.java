package fr.hd3d.common.ui.test.modeldata;

import java.util.Date;

import org.junit.Test;

import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.PoolModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.test.util.modeldata.reader.PoolReaderMock;


public class PoolModelDataTest extends ModelDataTest<PoolModelData, PoolReaderMock>
{
    @Override
    public void setUp()
    {
        super.setUp();

        this.reader = new PoolReaderMock();
        this.path = ServicesPath.getPath("Pool");
    }

    @Test
    public void testPostPool() throws Exception
    {
        PoolModelData pool = new PoolModelData("pool_post_01", true);
        PoolModelData postedPool = this.saveObject(pool);

        assertEquals(pool.getName(), postedPool.getName());
        assertEquals(pool.getIsDispatcherAllowed(), postedPool.getIsDispatcherAllowed());

        postedPool.delete();
    }

    @Test
    public void testPutPool() throws Exception
    {
        PoolModelData license = new PoolModelData("pool_put_01", true);

        PoolModelData putPool = this.saveObject(license);

        putPool.setName("pool_put_01_updated");
        putPool.setIsDispatcherAllowed(false);

        PoolModelData putPool_server = this.saveObject(putPool);

        assertEquals(putPool.getName(), putPool_server.getName());
        assertEquals(putPool.getIsDispatcherAllowed(), putPool_server.getIsDispatcherAllowed());

        putPool.delete();
    }

    @Test
    public void testDeletePool() throws Exception
    {
        PoolModelData pool = new PoolModelData("pool_delete_01", false);

        pool.save();
        pool.delete();
        pool.refresh();

        assertEquals(CommonErrors.RECORD_NOT_ON_SERVER, controller.getLastError());

    }

    @Test
    public void testComputerList() throws Exception
    {
        PoolModelData pool = new PoolModelData("pool_computer_01", true);
        ComputerModelData computer = new ComputerModelData("comp_license_01", "serial_01", "billing_01", new Date(),
                new Date(), defaultInventoryStatus, 123L, "fr.hd3d.laptop01", "127.0.0.1", "mac_adress_01", 1200L, 2L,
                2L, 4096L, "NO_WORKER", 160L, 35L);

        computer.save();
        pool.getComputerIDs().add(computer.getId());

        PoolModelData postedPool = this.saveObject(pool);
        computer.refresh();

        assertEquals(1, postedPool.getComputerIDs().size());
        assertEquals(computer.getId().longValue(), postedPool.getComputerIDs().get(0).longValue());
        assertEquals(postedPool.getId().longValue(), computer.getPoolIDs().get(0).longValue());

        ComputerModelData computer2 = new ComputerModelData("comp_license_02", "serial_03", "billing_02", new Date(),
                new Date(), defaultInventoryStatus, 123L, "fr.hd3d.laptop02", "127.0.0.1", "mac_adress_02", 1200L, 2L,
                2L, 4096L, "NO_WORKER", 160L, 35L);

        computer2.save();
        postedPool.getComputerIDs().add(computer2.getId());
        PoolModelData putPool = this.saveObject(postedPool);
        computer2.refresh();

        assertEquals(2, putPool.getComputerIDs().size());
        assertEquals(postedPool.getId().longValue(), computer.getPoolIDs().get(0).longValue());

        computer.delete();
        computer2.delete();
        putPool.delete();
    }
}
