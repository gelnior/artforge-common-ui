package fr.hd3d.common.ui.test.modeldata;

import java.util.Date;

import org.junit.Test;

import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.LicenseModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.SoftwareModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.test.util.modeldata.reader.LicenseReaderMock;


public class LicenseModelDataTest extends ModelDataTest<LicenseModelData, LicenseReaderMock>
{
    @Override
    public void setUp()
    {
        super.setUp();

        this.reader = new LicenseReaderMock();
        this.path = ServicesPath.getPath("License");
    }

    @Test
    public void testPostLicense() throws Exception
    {
        LicenseModelData license = new LicenseModelData("License_post_01", "serial_01", "reference_01", new Date(),
                new Date(), defaultInventoryStatus, 3L, "CLASSIC");
        LicenseModelData postedLicense = this.saveObject(license);
        assertEquals(license.getName(), postedLicense.getName());
        assertEquals(license.getSerial(), postedLicense.getSerial());
        assertEquals(license.getBillingReference(), postedLicense.getBillingReference());
        assertEquals(dateFormatter.format(license.getPurchaseDate()), dateFormatter.format(postedLicense
                .getPurchaseDate()));
        assertEquals(dateFormatter.format(license.getWarrantyEnd()), dateFormatter.format(postedLicense
                .getWarrantyEnd()));
        assertEquals(license.getNumber().toString(), postedLicense.getNumber().toString());
        assertEquals(license.getType(), postedLicense.getType());

        postedLicense.delete();
    }

    @Test
    public void testPutLicense() throws Exception
    {
        LicenseModelData license = new LicenseModelData("License_put_01", "serial_01", "reference_01", new Date(),
                new Date(), defaultInventoryStatus, 3L, "CLASSIC");

        LicenseModelData putLicense = this.saveObject(license);

        putLicense.setName("License_put_01_updated");
        putLicense.setSerial("test_put_serial_updated");
        putLicense.setBillingReference("test_put_reference");
        putLicense.setPurchaseDate(new Date());
        putLicense.setWarrantyEnd(new Date());
        putLicense.setNumber(4L);
        putLicense.setType("RENDER");

        LicenseModelData putLicense_server = this.saveObject(putLicense);

        assertEquals(putLicense.getName(), putLicense_server.getName());
        assertEquals(putLicense_server.getSerial(), putLicense_server.getSerial());
        assertEquals(dateFormatter.format(putLicense.getPurchaseDate()), dateFormatter.format(putLicense
                .getPurchaseDate()));
        assertEquals(putLicense.getNumber(), putLicense_server.getNumber());
        assertEquals(putLicense.getType(), putLicense_server.getType());

        putLicense.delete();
    }

    @Test
    public void testDeleteLicense() throws Exception
    {
        LicenseModelData license = new LicenseModelData("License_delete_01", "serial_01", "reference_01", new Date(),
                new Date(), defaultInventoryStatus, 3L, "CLASSIC");

        license.save();
        license.delete();
        license.refresh();

        assertEquals(CommonErrors.RECORD_NOT_ON_SERVER, controller.getLastError());
    }

    @Test
    public void testComputersList() throws Exception
    {
        LicenseModelData license = new LicenseModelData("License_computer_01", "serial_01", "reference_01", new Date(),
                new Date(), defaultInventoryStatus, 3L, "CLASSIC");
        ComputerModelData computer = new ComputerModelData("comp_license_01", "serial_01", "billing_01", new Date(),
                new Date(), defaultInventoryStatus, 123L, "fr.hd3d.laptop01", "127.0.0.1", "mac_adress_01", 1200L, 2L,
                2L, 4096L, "NO_WORKER", 160L, 35L);

        computer.save();
        license.getComputerIDs().add(computer.getId());

        LicenseModelData postedLicense = this.saveObject(license);
        computer.refresh();

        assertEquals(1, postedLicense.getComputerIDs().size());
        assertEquals(computer.getId().longValue(), postedLicense.getComputerIDs().get(0).longValue());
        assertEquals(postedLicense.getId().longValue(), computer.getLicenseIDs().get(0).longValue());

        ComputerModelData computer02 = new ComputerModelData("comp_license_02", "serial_03", "billing_02", new Date(),
                new Date(), defaultInventoryStatus, 123L, "fr.hd3d.laptop02", "127.0.0.1", "mac_adress_02", 1200L, 2L,
                2L, 4096L, "NO_WORKER", 160L, 35L);
        computer02.save();
        postedLicense.getComputerIDs().add(computer02.getId());
        LicenseModelData putLicense = this.saveObject(postedLicense);
        computer02.refresh();

        assertEquals(2, putLicense.getComputerIDs().size());
        assertEquals(postedLicense.getId().longValue(), computer02.getLicenseIDs().get(0).longValue());

        computer.delete();
        computer02.delete();
        putLicense.delete();
    }

    @Test
    public void testSoftware() throws Exception
    {
        SoftwareModelData software = new SoftwareModelData("soft_license_01");
        LicenseModelData license = new LicenseModelData("License_software_01", "serial_01", "reference_01", new Date(),
                new Date(), defaultInventoryStatus, 3L, "CLASSIC");

        software.save();
        license.setSoftwareId(software.getId());

        LicenseModelData postedLicense = this.saveObject(license);
        software.refresh();

        assertEquals(software.getId().longValue(), postedLicense.getSoftwareId().longValue());
        assertEquals(software.getLicenseIDs().get(0).longValue(), postedLicense.getId().longValue());

        SoftwareModelData software02 = new SoftwareModelData("soft_license_02");
        software02.save();
        postedLicense.setSoftwareId(software02.getId());
        LicenseModelData putLicense = this.saveObject(postedLicense);
        software02.refresh();

        assertEquals(software02.getId().longValue(), putLicense.getSoftwareId().longValue());
        assertEquals(software.getLicenseIDs().get(0).longValue(), putLicense.getId().longValue());

        software.delete();
        software02.delete();
        putLicense.delete();
    }

    @Test
    public void testResourceGroupList() throws Exception
    {
        ResourceGroupModelData resourceGroup = new ResourceGroupModelData("resourcegroup_license_01", null);
        LicenseModelData license = new LicenseModelData("license_resourceGroup_01", "serial_01", "reference_01",
                new Date(), new Date(), defaultInventoryStatus, 3L, "CLASSIC");

        resourceGroup.save();
        license.getResourceGroupIDs().add(resourceGroup.getId());
        LicenseModelData licenseServer = this.saveObject(license);
        resourceGroup.refresh();

        assertEquals(1, licenseServer.getResourceGroupIDs().size());
        assertEquals(resourceGroup.getId(), licenseServer.getResourceGroupIDs().get(0));
        assertEquals(1, resourceGroup.getResourceIds().size());
        assertEquals(licenseServer.getId(), resourceGroup.getResourceIds().get(0));

        ResourceGroupModelData resourceGroup02 = new ResourceGroupModelData("resourcegroup_license_02", null);
        resourceGroup02.save();
        license.getResourceGroupIDs().add(resourceGroup02.getId());
        license.save();
        license.refresh();

        assertEquals(2, license.getResourceGroupIDs().size());
        assertEquals(1, resourceGroup.getResourceIds().size());
        assertEquals(license.getId(), resourceGroup.getResourceIds().get(0));

        resourceGroup.delete();
        resourceGroup02.delete();
        license.delete();
    }
}
