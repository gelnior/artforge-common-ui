package fr.hd3d.common.ui.test.modeldata;

import java.util.Date;

import org.junit.Test;

import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerTypeModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.DeviceModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.LicenseModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.PoolModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.RoomModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ScreenModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.SoftwareModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.StudioMapModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.test.util.modeldata.reader.ComputerReaderMock;


public class ComputerModelDataTest extends ModelDataTest<ComputerModelData, ComputerReaderMock>
{

    @Override
    public void setUp()
    {
        super.setUp();

        this.reader = new ComputerReaderMock();
        this.path = ServicesPath.getPath("Computer");
    }

    @Test
    public void testPostComputer() throws Exception
    {
        ComputerModelData computer = new ComputerModelData("comp_post_01", "serial_01", "billing_01", new Date(),
                new Date(), defaultInventoryStatus, 123L, "fr.hd3d.laptop01", "127.0.0.1", "mac_adress", 1200L, 2L, 2L,
                4096L, "NO_WORKER", 160L, 35L);

        ComputerModelData postedComputer = this.saveObject(computer);

        assertEquals(computer.getName(), postedComputer.getName());
        assertEquals(computer.getSerial(), postedComputer.getSerial());
        assertEquals(computer.getBillingReference(), postedComputer.getBillingReference());
        assertEquals(dateFormatter.format(computer.getPurchaseDate()), dateFormatter.format(postedComputer
                .getPurchaseDate()));
        assertEquals(dateFormatter.format(computer.getWarrantyEnd()), dateFormatter.format(postedComputer
                .getWarrantyEnd()));
        assertEquals(computer.getInventoryId(), postedComputer.getInventoryId());
        assertEquals(computer.getDnsName(), postedComputer.getDnsName());
        assertEquals(computer.getMacAdress(), postedComputer.getMacAdress());
        assertEquals(computer.getProcFrequency(), postedComputer.getProcFrequency());
        assertEquals(computer.getNumberOfProcs(), postedComputer.getNumberOfProcs());
        assertEquals(computer.getNumberOfCores(), postedComputer.getNumberOfCores());
        assertEquals(computer.getRamQuantity(), postedComputer.getRamQuantity());
        assertEquals(computer.getWorkerStatus(), postedComputer.getWorkerStatus());
        assertEquals(computer.getXPosition(), postedComputer.getXPosition());
        assertEquals(computer.getYPosition(), postedComputer.getYPosition());

        postedComputer.delete();
    }

    @Test
    public void testPutComputer() throws Exception
    {
        ComputerModelData computer = new ComputerModelData("comp_put_01", "serial_01", "billing_01", new Date(),
                new Date(), defaultInventoryStatus, 123L, "fr.hd3d.laptop01", "127.0.0.1", "mac_adress", 1200L, 2L, 2L,
                4096L, "NO_WORKER", 160L, 35L);

        ComputerModelData putComputer = this.saveObject(computer);

        putComputer.setName("test_put_name");
        putComputer.setSerial("test_put_serial");
        putComputer.setBillingReference("test_put_reference");
        putComputer.setPurchaseDate(new Date());
        putComputer.setWarrantyEnd(new Date());
        putComputer.setInventoryId(124L);
        putComputer.setDnsName("fr.hd3d.put");
        putComputer.setMacAdress("test_put_mac_adress");
        putComputer.setProcFrequency(1600L);
        putComputer.setNumberOfProcs(4L);
        putComputer.setNumberOfCores(1L);
        putComputer.setRamQuantity(8192L);
        putComputer.setWorkerStatus("ACTIVE");
        putComputer.setXPosition(244L);
        putComputer.setYPosition(450L);

        ComputerModelData putComputer_server = this.saveObject(putComputer);

        assertEquals(putComputer.getName(), putComputer_server.getName());
        assertEquals(putComputer.getSerial(), putComputer_server.getSerial());
        assertEquals(putComputer.getBillingReference(), putComputer_server.getBillingReference());
        assertEquals(dateFormatter.format(putComputer.getPurchaseDate()), dateFormatter.format(putComputer_server
                .getPurchaseDate()));
        assertEquals(dateFormatter.format(putComputer.getWarrantyEnd()), dateFormatter.format(putComputer_server
                .getWarrantyEnd()));
        assertEquals(putComputer.getInventoryId(), putComputer_server.getInventoryId());
        assertEquals(putComputer.getDnsName(), putComputer_server.getDnsName());
        assertEquals(putComputer.getMacAdress(), putComputer_server.getMacAdress());
        assertEquals(putComputer.getProcFrequency(), putComputer_server.getProcFrequency());
        assertEquals(putComputer.getNumberOfProcs(), putComputer_server.getNumberOfProcs());
        assertEquals(putComputer.getNumberOfCores(), putComputer_server.getNumberOfCores());
        assertEquals(putComputer.getRamQuantity(), putComputer_server.getRamQuantity());
        assertEquals(putComputer.getWorkerStatus(), putComputer_server.getWorkerStatus());
        assertEquals(putComputer.getXPosition(), putComputer_server.getXPosition());
        assertEquals(putComputer.getYPosition(), putComputer_server.getYPosition());

        putComputer.delete();
    }

    @Test
    public void testDeleteComputer() throws Exception
    {
        ComputerModelData computer = new ComputerModelData("comp_delete_01", "serial_01", "billing_01", new Date(),
                new Date(), defaultInventoryStatus, 123L, "fr.hd3d.laptop01", "127.0.0.1", "mac_adress", 1200L, 2L, 2L,
                4096L, "NO_WORKER", 160L, 35L);

        computer.save();
        computer.delete();
        computer.refresh();

        assertEquals(CommonErrors.RECORD_NOT_ON_SERVER, controller.getLastError());
    }

    @Test
    public void testSoftwareList() throws Exception
    {
        SoftwareModelData software = new SoftwareModelData("soft_comp_01");
        ComputerModelData computer = new ComputerModelData("comp_soft_01", "serial_01", "billing_01", new Date(),
                new Date(), defaultInventoryStatus, 123L, "fr.hd3d.laptop01", "127.0.0.1", "mac_adress", 1200L, 2L, 2L,
                4096L, "NO_WORKER", 160L, 35L);

        software.save();
        computer.getSoftwareIDs().add(software.getId());
        ComputerModelData postedComputer = this.saveObject(computer);
        software.refresh();

        assertEquals(1, postedComputer.getSoftwareIDs().size());
        assertEquals(software.getId().longValue(), postedComputer.getSoftwareIDs().get(0).longValue());
        assertEquals(postedComputer.getId().longValue(), software.getComputerIDs().get(0).longValue());

        SoftwareModelData software02 = new SoftwareModelData("soft_comp_02");
        software02.save();
        postedComputer.getSoftwareIDs().add(software02.getId());
        ComputerModelData putComputer = this.saveObject(postedComputer);
        software02.refresh();
        assertEquals(2, putComputer.getSoftwareIDs().size());
        assertEquals(postedComputer.getId().longValue(), software.getComputerIDs().get(0).longValue());

        software.delete();
        software02.delete();
        putComputer.delete();
    }

    @Test
    public void testDeviceList() throws Exception
    {
        DeviceModelData device = new DeviceModelData("device_comp_01", "serial_01", "reference_01", new Date(),
                new Date(), defaultInventoryStatus, 1.2D);
        ComputerModelData computer = new ComputerModelData("comp_soft_01", "serial_01", "billing_01", new Date(),
                new Date(), defaultInventoryStatus, 123L, "fr.hd3d.laptop01", "127.0.0.1", "mac_adress", 1200L, 2L, 2L,
                4096L, "NO_WORKER", 160L, 35L);

        device.save();
        computer.getDeviceIDs().add(device.getId());
        ComputerModelData postedComputer = this.saveObject(computer);
        device.refresh();
        assertEquals(device.getId().longValue(), postedComputer.getDeviceIDs().get(0).longValue());
        assertEquals(postedComputer.getId().longValue(), device.getComputerIDs().get(0).longValue());

        DeviceModelData device02 = new DeviceModelData("device_comp_02", "serial_01", "reference_01", new Date(),
                new Date(), defaultInventoryStatus, 1.2D);
        device02.save();
        postedComputer.getDeviceIDs().add(device02.getId());
        ComputerModelData putComputer = this.saveObject(postedComputer);
        device02.refresh();

        assertEquals(2, putComputer.getDeviceIDs().size());
        assertEquals(postedComputer.getId().longValue(), device02.getComputerIDs().get(0).longValue());

        device.delete();
        device02.delete();
        putComputer.delete();
    }

    @Test
    public void testScreenList() throws Exception
    {
        ScreenModelData screen = new ScreenModelData("screen_comp_01", "serial_01", "reference_01", new Date(),
                new Date(), defaultInventoryStatus, 1.2D, "LCD", "GRAPHIC");
        ComputerModelData computer = new ComputerModelData("comp_soft_01", "serial_01", "billing_01", new Date(),
                new Date(), defaultInventoryStatus, 123L, "fr.hd3d.laptop01", "127.0.0.1", "mac_adress", 1200L, 2L, 2L,
                4096L, "NO_WORKER", 160L, 35L);

        screen.save();
        computer.getScreenIDs().add(screen.getId());
        ComputerModelData computerServer = this.saveObject(computer);
        screen.refresh();

        assertEquals(screen.getId(), computerServer.getScreenIDs().get(0));
        assertEquals(computerServer.getId().longValue(), screen.getComputerIDs().get(0).longValue());

        ScreenModelData screen02 = new ScreenModelData("screen_comp_02", "serial_01", "reference_01", new Date(),
                new Date(), defaultInventoryStatus, 1.2D, "LCD", "REFERENCE");
        screen02.save();
        computerServer.getScreenIDs().add(screen02.getId());
        ComputerModelData putComputer = this.saveObject(computerServer);
        screen02.refresh();

        assertEquals(2, putComputer.getScreenIDs().size());
        assertEquals(computerServer.getId().longValue(), screen02.getComputerIDs().get(0).longValue());

        screen.delete();
        screen02.delete();
        putComputer.delete();
    }

    @Test
    public void testStudioMap() throws Exception
    {
        ComputerModelData computer = new ComputerModelData("comp_map_01", "serial_01", "billing_01", new Date(),
                new Date(), defaultInventoryStatus, 123L, "fr.hd3d.laptop01", "127.0.0.1", "mac_adress", 1200L, 2L, 2L,
                4096L, "NO_WORKER", 160L, 35L);
        StudioMapModelData studioMap = new StudioMapModelData("test_map_for_comp_01", "test_path", 55L, 50L);

        studioMap.save();
        computer.setStudioMapID(studioMap.getId());
        ComputerModelData postedComputer = this.saveObject(computer);
        studioMap.refresh();

        assertEquals(studioMap.getId().longValue(), postedComputer.getStudioMapId().longValue());
        assertEquals(studioMap.getComputerIDs().get(0).longValue(), postedComputer.getId().longValue());

        StudioMapModelData studioMap02 = new StudioMapModelData("test_map_for_comp_02", "test_path", 55L, 50L);
        studioMap02.save();
        postedComputer.setStudioMapID(studioMap02.getId());
        ComputerModelData putComputer = this.saveObject(postedComputer);
        studioMap02.refresh();

        assertEquals(studioMap02.getId().longValue(), putComputer.getStudioMapId().longValue());
        assertEquals(studioMap.getComputerIDs().get(0).longValue(), putComputer.getId().longValue());

        studioMap.delete();
        studioMap02.delete();
        putComputer.delete();
    }

    @Test
    public void testLicenseList() throws Exception
    {
        LicenseModelData license = new LicenseModelData("License_computer_01", "serial_01", "billing_01", new Date(),
                new Date(), defaultInventoryStatus, 3L, "CLASSIC");
        ComputerModelData computer = new ComputerModelData("comp_license_01", "serial_01", "billing_01", new Date(),
                new Date(), defaultInventoryStatus, 123L, "fr.hd3d.laptop01", "127.0.0.1", "mac_adress", 1200L, 2L, 2L,
                4096L, "NO_WORKER", 160L, 35L);

        license.save();
        computer.getLicenseIDs().add(license.getId());
        ComputerModelData postedComputer = this.saveObject(computer);
        license.refresh();
        assertEquals(license.getId().longValue(), postedComputer.getLicenseIDs().get(0).longValue());
        assertEquals(postedComputer.getId().longValue(), license.getComputerIDs().get(0).longValue());

        LicenseModelData license02 = new LicenseModelData("License_computer_02", "serial_01", "billing_01", new Date(),
                new Date(), defaultInventoryStatus, 3L, "RENDER");
        license02.save();
        postedComputer.getLicenseIDs().add(license02.getId());
        ComputerModelData putComputer = this.saveObject(postedComputer);
        license02.refresh();

        assertEquals(2, putComputer.getLicenseIDs().size());
        assertEquals(postedComputer.getId().longValue(), license.getComputerIDs().get(0).longValue());

        license.delete();
        license02.delete();
        putComputer.delete();
    }

    @Test
    public void testPoolList() throws Exception
    {
        PoolModelData pool = new PoolModelData("pool_computer_01", true);
        ComputerModelData computer = new ComputerModelData("comp_license_01", "serial_01", "billing_01", new Date(),
                new Date(), defaultInventoryStatus, 123L, "fr.hd3d.laptop01", "127.0.0.1", "mac_adress", 1200L, 2L, 2L,
                4096L, "NO_WORKER", 160L, 35L);

        pool.save();
        computer.getPoolIDs().add(pool.getId());
        ComputerModelData postedComputer = this.saveObject(computer);
        pool.refresh();
        assertEquals(pool.getId().longValue(), postedComputer.getPoolIDs().get(0).longValue());
        assertEquals(postedComputer.getId().longValue(), pool.getComputerIDs().get(0).longValue());

        PoolModelData pool02 = new PoolModelData("pool_computer_02", false);
        pool02.save();
        postedComputer.getPoolIDs().add(pool02.getId());
        ComputerModelData putComputer = this.saveObject(postedComputer);
        pool02.refresh();

        assertEquals(2, putComputer.getPoolIDs().size());
        assertEquals(postedComputer.getId().longValue(), pool.getComputerIDs().get(0).longValue());

        pool.delete();
        pool02.delete();
        putComputer.delete();
    }

    @Test
    public void testComputerModel() throws Exception
    {
        ComputerModelData computer = new ComputerModelData("comp_model_01", "serial_01", "billing_01", new Date(),
                new Date(), defaultInventoryStatus, 123L, "fr.hd3d.laptop01", "127.0.0.1", "mac_adress", 1200L, 2L, 2L,
                4096L, "NO_WORKER", 160L, 35L);
        ComputerModelModelData computerModel = new ComputerModelModelData("computer_model_computer_01");

        computerModel.save();
        computer.setComputerModelId(computerModel.getId());
        computer = this.saveObject(computer);

        assertEquals(computerModel.getId().longValue(), computer.getComputerModelId().longValue());
        assertEquals(computerModel.getName(), computer.getComputerModelName());

        ComputerModelModelData computerModel02 = new ComputerModelModelData("computer_model_computer_02");
        computerModel02.save();
        computer.setComputerModelId(computerModel02.getId());
        computer = this.saveObject(computer);

        assertEquals(computerModel02.getId().longValue(), computer.getComputerModelId().longValue());

        computer.delete();
        computerModel02.delete();
        computerModel.delete();
    }

    @Test
    public void testComputerType() throws Exception
    {
        ComputerModelData computer = new ComputerModelData("comp_computer_type_01", "serial_01", "billing_01",
                new Date(), new Date(), defaultInventoryStatus, 123L, "fr.hd3d.laptop01", "127.0.0.1", "mac_adress",
                1200L, 2L, 2L, 4096L, "NO_WORKER", 160L, 35L);
        ComputerTypeModelData computerType = new ComputerTypeModelData("computer_type_computer_01");
        computerType.save();

        computer.setComputerTypeId(computerType.getId());
        ComputerModelData computerServer = this.saveObject(computer);
        assertEquals(computerType.getId(), computerServer.getComputerTypeId());

        ComputerTypeModelData computerType02 = new ComputerTypeModelData("computer_type_computer_02");
        computerType02.save();
        computer.setComputerTypeId(computerType02.getId());
        computer.save();
        computerServer.refresh();
        assertEquals(computerType02.getId(), computerServer.getComputerTypeId());

        computer.delete();
        computerType.delete();
        computerType02.delete();
    }

    @Test
    public void testRoom() throws Exception
    {
        ComputerModelData computer = new ComputerModelData("comp_computer_type_01", "serial_01", "billing_01",
                new Date(), new Date(), defaultInventoryStatus, 123L, "fr.hd3d.laptop01", "127.0.0.1", "mac_adress",
                1200L, 2L, 2L, 4096L, "NO_WORKER", 160L, 35L);
        RoomModelData room = new RoomModelData("computer_type_computer_01");
        room.save();

        computer.setRoomId(room.getId());
        ComputerModelData computerServer = this.saveObject(computer);
        assertEquals(room.getId(), computerServer.getRoomId());

        RoomModelData room02 = new RoomModelData("computer_type_computer_02");
        room02.save();
        computer.setRoomId(room02.getId());
        computer.save();
        computerServer.refresh();
        assertEquals(room02.getId(), computerServer.getRoomId());

        computer.delete();
        room.delete();
        room02.delete();
    }

    @Test
    public void testResourceGroupList() throws Exception
    {
        ResourceGroupModelData resourceGroup = new ResourceGroupModelData("resourcegroup_computer_01", null);
        ComputerModelData computer = new ComputerModelData("comp_resourcegroup_01", "serial_01", "billing_01",
                new Date(), new Date(), defaultInventoryStatus, 123L, "fr.hd3d.laptop01", "127.0.0.1", "mac_adress",
                1200L, 2L, 2L, 4096L, "NO_WORKER", 160L, 35L);

        resourceGroup.save();
        computer.getResourceGroupIDs().add(resourceGroup.getId());
        ComputerModelData computerServer = this.saveObject(computer);
        resourceGroup.refresh();

        assertEquals(1, computerServer.getResourceGroupIDs().size());
        assertEquals(resourceGroup.getId(), computerServer.getResourceGroupIDs().get(0));
        assertEquals(1, resourceGroup.getResourceIds().size());
        assertEquals(computerServer.getId(), resourceGroup.getResourceIds().get(0));

        ResourceGroupModelData resourceGroup02 = new ResourceGroupModelData("resourcegroup_computer_02", null);
        resourceGroup02.save();
        computer.getResourceGroupIDs().add(resourceGroup02.getId());
        computer.save();
        computer.refresh();

        assertEquals(2, computer.getResourceGroupIDs().size());
        assertEquals(1, resourceGroup.getResourceIds().size());
        assertEquals(computer.getId(), resourceGroup.getResourceIds().get(0));

        resourceGroup.delete();
        resourceGroup02.delete();
        computer.delete();
    }
}
