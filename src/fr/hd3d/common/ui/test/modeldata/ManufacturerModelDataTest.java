package fr.hd3d.common.ui.test.modeldata;

import org.junit.Test;

import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.modeldata.inventory.ManufacturerModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.test.util.modeldata.reader.ManufacturerReaderMock;


public class ManufacturerModelDataTest extends ModelDataTest<ManufacturerModelData, ManufacturerReaderMock>
{
    @Override
    public void setUp()
    {
        super.setUp();

        this.reader = new ManufacturerReaderMock();
        this.path = ServicesPath.getPath("Manufacturer");
    }

    @Test
    public void testPostManufacturer() throws Exception
    {
        ManufacturerModelData manufacturer = new ManufacturerModelData("manufacturer_model_post_01");

        ManufacturerModelData postedManufacturer = this.saveObject(manufacturer);
        assertEquals(manufacturer.getName(), postedManufacturer.getName());

        postedManufacturer.delete();
    }

    @Test
    public void testPutManufacturer() throws Exception
    {
        ManufacturerModelData manufacturer = new ManufacturerModelData("manufacturer_model_put_01");
        ManufacturerModelData putManufacturer = this.saveObject(manufacturer);

        putManufacturer.setName("manufacturer_model_put_01_updated");
        ManufacturerModelData putManufacturer_server = this.saveObject(putManufacturer);
        assertEquals(putManufacturer.getName(), putManufacturer_server.getName());

        putManufacturer.delete();
    }

    @Test
    public void testDeleteManufacturer() throws Exception
    {
        ManufacturerModelData manufacturer = new ManufacturerModelData("manufacturer_model_delete_01");

        manufacturer.save();
        manufacturer.delete();
        manufacturer.refresh();

        assertEquals(CommonErrors.RECORD_NOT_ON_SERVER, controller.getLastError());
    }
}
