package fr.hd3d.common.ui.test.modeldata;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.StudioMapModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.test.util.modeldata.reader.StudioMapReaderMock;


public class StudioMapModelDataTest extends ModelDataTest<StudioMapModelData, StudioMapReaderMock>
{
    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    public void setUp()
    {
        super.setUp();

        this.reader = new StudioMapReaderMock();
        this.path = ServicesPath.getPath("StudioMap");
    }

    @Test
    public void testPostStudioMap() throws Exception
    {
        StudioMapModelData studioMap = new StudioMapModelData("map_post_01", "path_01", 55, 50);
        StudioMapModelData postedStudioMap = this.saveObject(studioMap);

        assertEquals(studioMap.getName(), postedStudioMap.getName());
        assertEquals(studioMap.getImagePath(), postedStudioMap.getImagePath());
        assertEquals(studioMap.getSizeX(), postedStudioMap.getSizeX());
        assertEquals(studioMap.getSizeY(), postedStudioMap.getSizeY());

        studioMap.delete();
    }

    @Test
    public void testPutStudioMap() throws Exception
    {
        StudioMapModelData studioMap = new StudioMapModelData("map_put_01", "path_01", 55L, 50L);
        StudioMapModelData putStudioMap = this.saveObject(studioMap);

        putStudioMap.setName("test_put_map_01_updated");
        putStudioMap.setImagePath("test_put_path_01_updated");
        putStudioMap.setSizeX(60L);
        putStudioMap.setSizeY(68L);

        StudioMapModelData putStudioMap_server = this.saveObject(putStudioMap);

        assertEquals(putStudioMap.getName(), putStudioMap_server.getName());
        assertEquals(putStudioMap.getImagePath(), putStudioMap_server.getImagePath());
        assertEquals(putStudioMap.getSizeX(), putStudioMap_server.getSizeX());
        assertEquals(putStudioMap.getSizeY(), putStudioMap_server.getSizeY());

        putStudioMap.delete();
    }

    @Test
    public void testDeleteStudioMap() throws Exception
    {
        StudioMapModelData studioMap = new StudioMapModelData("map_delete_01", "path_01", 55L, 50L);

        studioMap.save();
        studioMap.delete();
        studioMap.refresh();

        assertEquals(CommonErrors.RECORD_NOT_ON_SERVER, controller.getLastError());
    }

    @Test
    public void testComputersList() throws Exception
    {
        StudioMapModelData studioMap = new StudioMapModelData("map_comp_01", "path_01", 55L, 50L);
        ComputerModelData computer = new ComputerModelData("comp_map_01", "serial_01", "billing_01", new Date(),
                new Date(), defaultInventoryStatus, 123L, "fr.hd3d.laptop01", "127.0.0.1", "mac_adress_01", 1200L, 2L,
                2L, 4096L, "NO_WORKER", 160L, 35L);

        computer.save();
        studioMap.getComputerIDs().add(computer.getId());
        StudioMapModelData postedStudioMap = this.saveObject(studioMap);

        ComputerModelData computer2 = new ComputerModelData("comp_map_02", "serial_03", "billing_02", new Date(),
                new Date(), defaultInventoryStatus, 123L, "fr.hd3d.laptop02", "127.0.0.1", "mac_adress_02", 1200L, 2L,
                2L, 4096L, "NO_WORKER", 160L, 35L);
        computer2.save();
        postedStudioMap.getComputerIDs().add(computer2.getId());
        StudioMapModelData putStudioMap = this.saveObject(postedStudioMap);
        computer2.refresh();
        assertEquals(1, putStudioMap.getComputerIDs().size());
        assertEquals(postedStudioMap.getId().longValue(), computer2.getStudioMapId().longValue());

        computer2.delete();
        computer.delete();
        putStudioMap.delete();
    }
}
