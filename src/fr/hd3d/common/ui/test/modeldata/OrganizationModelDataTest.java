package fr.hd3d.common.ui.test.modeldata;

import org.junit.Test;

import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.modeldata.resource.OrganizationModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.common.ui.test.util.modeldata.reader.OrganizationReaderMock;


/**
 * Test resource group service validity.
 * 
 * @author HD3D
 */
public class OrganizationModelDataTest extends ModelDataTest<OrganizationModelData, OrganizationReaderMock>
{
    @Override
    public void setUp()
    {
        super.setUp();

        this.reader = new OrganizationReaderMock();
    }

    @Test
    public void testPostOrganization() throws Exception
    {
        OrganizationModelData group = new OrganizationModelData("organization_post_01", null);
        OrganizationModelData groupServer = this.saveObject(group);

        assertEquals(group.getName(), groupServer.getName());

        groupServer.delete();
    }

    @Test
    public void testPutOrganization() throws Exception
    {
        OrganizationModelData organization = new OrganizationModelData("organization_put_01", null);

        organization.save();
        organization.setName("organization_put_01_udpated");
        organization.save();

        OrganizationModelData organizationServer = new OrganizationModelData();
        organizationServer.setId(organization.getId());
        organizationServer.refresh();

        assertEquals(organization.getName(), organizationServer.getName());

        organization.delete();
    }

    @Test
    public void testDeleteOrganization() throws Exception
    {
        OrganizationModelData organization = new OrganizationModelData("organization_delete_01", null);

        organization.save();
        organization.delete();
        organization.refresh();

        assertEquals(CommonErrors.RECORD_NOT_ON_SERVER, controller.getLastError());
    }

    @Test
    public void testAddResourceGroupToOrganization() throws Exception
    {
        OrganizationModelData organization = new OrganizationModelData("organization_resourcegroup_01", null);
        ResourceGroupModelData resourceGroup = new ResourceGroupModelData("resourcegroup_organization_01", null);

        resourceGroup.save();
        organization.getResourceGroupIds().add(resourceGroup.getId());

        OrganizationModelData organizationServer = this.saveObject(organization);

        assertEquals(1, organizationServer.getResourceGroupIds().size());
        assertEquals(resourceGroup.getId(), organizationServer.getResourceGroupIds().get(0));

        resourceGroup.delete();
        organization.delete();
    }

    @Test
    public void testOrganizationHierarchy() throws Exception
    {
        OrganizationModelData parent01 = new OrganizationModelData("organization_parent_01", null);
        OrganizationModelData parent02 = new OrganizationModelData("organization_parent_02", null);

        OrganizationModelData child01 = new OrganizationModelData("organization_child_01", null);
        OrganizationModelData child02 = new OrganizationModelData("organization_child_02", null);
        OrganizationModelData child03 = new OrganizationModelData("organization_child_03", null);

        parent01.save();
        parent02.save();

        child01.setParentId(parent01.getId());
        child02.setParentId(parent01.getId());

        OrganizationModelData child01Server = this.saveObject(child01);
        OrganizationModelData child02Server = this.saveObject(child02);
        child03.save();

        assertEquals(parent01.getId().longValue(), child01Server.getParentId().longValue());
        assertEquals(parent01.getId(), child02Server.getParentId());

        child01.setParentId(parent02.getId());
        child03.setParentId(parent02.getId());
        child01.save();
        child03.save();
        child01.refresh();
        child03.refresh();

        assertEquals(child01.getParentId(), parent02.getId());
        assertEquals(child03.getParentId(), parent02.getId());

        parent01.delete();
        child01.delete();
        child02.delete();
        child03.delete();
    }
}
