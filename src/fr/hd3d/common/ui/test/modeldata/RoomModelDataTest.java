package fr.hd3d.common.ui.test.modeldata;

import org.junit.Test;

import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.modeldata.inventory.RoomModelData;
import fr.hd3d.common.ui.test.util.modeldata.reader.RoomReaderMock;


public class RoomModelDataTest extends ModelDataTest<RoomModelData, RoomReaderMock>
{
    @Override
    public void setUp()
    {
        super.setUp();

        this.reader = new RoomReaderMock();
    }

    @Test
    public void testPostRoom() throws Exception
    {
        RoomModelData room = new RoomModelData("room_post_01");
        RoomModelData roomServer = this.saveObject(room);

        assertEquals(room.getName(), roomServer.getName());

        room.delete();
    }

    @Test
    public void testPutRoom() throws Exception
    {
        RoomModelData room = new RoomModelData("room_put_01");

        room.save();
        room.setName("room_put_01_updated");
        room.save();

        RoomModelData roomServer = new RoomModelData();
        roomServer.setId(room.getId());
        roomServer.refresh();

        assertEquals(room.getName(), roomServer.getName());

        room.delete();
    }

    @Test
    public void testDeleteRoom() throws Exception
    {
        RoomModelData room = new RoomModelData("room_delete_01");

        room.save();
        room.delete();
        room.refresh();

        assertEquals(CommonErrors.RECORD_NOT_ON_SERVER, controller.getLastError());
    }
}
