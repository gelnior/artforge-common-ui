package fr.hd3d.common.ui.test.modeldata;

import java.util.Calendar;

import org.junit.Test;

import com.extjs.gxt.ui.client.util.DateWrapper;

import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.task.PersonDayModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskActivityModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.test.util.TestUtils;
import fr.hd3d.common.ui.test.util.modeldata.reader.TaskActivityReaderMock;


public class TaskActivityModelDataTest extends ModelDataTest<TaskActivityModelData, TaskActivityReaderMock>
{
    protected PersonModelData myPerson = new PersonModelData("PersonActivityTaskTest_firstName",
            "PersonActivityTaskTest_lastName", "PersonActivityTaskTest_login", "PersonActivityTaskTest_email",
            "PersonActivityTaskTest_phone", "PersonActivityTaskTest_photoPath");

    protected ProjectModelData myProject;

    protected PersonDayModelData myPersonDay = new PersonDayModelData();

    protected Calendar myDate = Calendar.getInstance();

    @Override
    public void setUp()
    {
        super.setUp();

        this.myProject = TestUtils.getDefaultProject();
        this.reader = new TaskActivityReaderMock();

        myDate.set(110, 4, 28, 13, 45, 00);
        myDate.set(14, 0);

        myProject.refreshByName(CommonEvents.MODEL_DATA_REFRESH_SUCCESS);

        if (myProject.getId() == null)
            myProject.save();

        myPerson.refreshByLogin(null);
        if (myPerson.getId() == null)
            myPerson.save();

        myPersonDay.refreshOrCreate(null);
        if (myPersonDay.getId() == null)
        {
            myPersonDay.setDate(myDate.getTime());
            myPersonDay.setPersonId(myPerson.getId());
            myPersonDay.save();
        }
    }

    @Test
    public void testPostTaskActivity() throws Exception
    {
        TaskActivityModelData myPostTaskActivity = new TaskActivityModelData();

        TaskModelData myPostTask = new TaskModelData();

        myPostTask.setName("myPostTaskActivityName");
        myPostTask.setStatus(ETaskStatus.OK.toString());
        myPostTask.setCompletion((byte) 0);
        myPostTask.setWorkerID(myPerson.getId());
        myPostTask.setProjectID(myProject.getId());
        myPostTask.save();

        myPostTaskActivity.setTaskFields(myPostTask);
        myPostTaskActivity.setWorkerId(myPerson.getId());
        myPostTaskActivity.setFilledDate(myDate.getTime());
        myPostTaskActivity.setFilledById(myPerson.getId());
        myPostTaskActivity.setDayId(myPersonDay.getId());
        myPostTaskActivity.setDuration((long) 34);

        TaskActivityModelData myPostTaskActivityServer = this.saveObject(myPostTaskActivity);

        assertEquals(myPostTask.getId(), myPostTaskActivityServer.getTaskId());

        myPostTaskActivity.setDefaultPath(ServicesPath.TASK_ACTIVITIES);
        myPostTaskActivity.setId(null);
        myPostTaskActivity.save();

        assertEquals(CommonErrors.SERVER_ERROR, controller.getLastError());

        myPostTaskActivityServer.delete();
        myPostTask.delete();
    }

    @Test
    public void testPutTaskActivity() throws Exception
    {
        TaskActivityModelData myPutTaskActivity = new TaskActivityModelData();
        TaskModelData myPutTask = new TaskModelData();

        myPutTask.setName("myPutTaskActivityName");
        myPutTask.setStatus(ETaskStatus.OK.toString());
        myPutTask.setCompletion((byte) 0);
        myPutTask.setWorkerID(myPerson.getId());
        myPutTask.setProjectID(myProject.getId());

        myPutTask.save();

        myPutTaskActivity.setTaskFields(myPutTask);
        myPutTaskActivity.setWorkerId(myPerson.getId());
        myPutTaskActivity.setFilledDate(myDate.getTime());
        myPutTaskActivity.setFilledById(myPerson.getId());
        myPutTaskActivity.setDayId(myPersonDay.getId());
        myPutTaskActivity.setDuration((long) 34);

        TaskActivityModelData myPutTaskActivityServer = this.saveObject(myPutTaskActivity);

        myPutTaskActivityServer.setTaskName("myPutTaskActivityName_Updated");
        myPutTaskActivityServer.setDuration((long) 98);
        myPutTaskActivityServer.refresh();

        assertEquals(myPutTaskActivity.getTaskName(), myPutTaskActivityServer.getTaskName());
        assertEquals(myPutTaskActivity.getDuration(), myPutTaskActivityServer.getDuration());

        myPutTask.delete();
        myPutTaskActivity.delete();
        myPutTaskActivityServer.delete();
    }

    @Test
    public void testDelete() throws Exception
    {
        TaskActivityModelData myDelTaskActivity = new TaskActivityModelData();
        TaskModelData myDelTask = new TaskModelData();

        myDelTask.setName("myPutTaskActivityName");
        myDelTask.setStatus(ETaskStatus.OK.toString());
        myDelTask.setCompletion((byte) 0);
        myDelTask.setWorkerID(myPerson.getId());
        myDelTask.setProjectID(myProject.getId());

        myDelTask.save();

        myDelTaskActivity.setTaskFields(myDelTask);
        myDelTaskActivity.setWorkerId(myPerson.getId());
        myDelTaskActivity.setFilledDate(myDate.getTime());
        myDelTaskActivity.setFilledById(myPerson.getId());
        myDelTaskActivity.setDayId(myPersonDay.getId());
        myDelTaskActivity.setDuration((long) 34);

        myDelTaskActivity.save();
        myDelTaskActivity.delete();
        myDelTaskActivity.refresh();

        assertEquals(CommonErrors.RECORD_NOT_ON_SERVER, controller.getLastError());
    }

    @Test
    public void testTask() throws Exception
    {
        TaskActivityModelData myTaskActivity = new TaskActivityModelData();
        TaskModelData myTask = new TaskModelData();

        myTask.setName("myPutTaskActivityName");
        myTask.setStatus(ETaskStatus.OK.toString());
        myTask.setCompletion((byte) 0);
        myTask.setWorkerID(myPerson.getId());
        myTask.setProjectID(myProject.getId());
        myTask.setDuration((long) 56);
        myTask.save();

        myTaskActivity.setTaskFields(myTask);
        myTaskActivity.setWorkerId(myPerson.getId());
        myTaskActivity.setFilledDate(myDate.getTime());
        myTaskActivity.setFilledById(myPerson.getId());
        myTaskActivity.setDayId(myPersonDay.getId());
        myTaskActivity.setDuration((long) 34);

        TaskActivityModelData myTaskActivityPosted = this.saveObject(myTaskActivity);

        assertEquals(myTask.getId(), myTaskActivityPosted.getTaskId());
        assertEquals(myTask.getName(), myTaskActivityPosted.getTaskName());
        assertEquals(myTask.getDuration(), myTaskActivityPosted.getTaskDuration());

        myTask.delete();
        myTaskActivityPosted.delete();
    }

    public void testTaskActualStartDateUpdate()
    {
        ConstituentModelData constituent = TestUtils.getDefaultConstituent();
        PersonModelData person = TestUtils.getDefaultPerson();
        PersonDayModelData day1 = TestUtils.getDefaultDay();
        day1.setPersonId(person.getId());
        day1.save();
        PersonDayModelData day2 = TestUtils.getDefaultDay();
        DateWrapper date = new DateWrapper(day2.getDate());
        day2.setDate(date.addDays(-1).asDate());
        day2.setPersonId(person.getId());
        day2.save();

        TaskTypeModelData taskType = TestUtils.getTaskType("Test-Type-actual-start-date");

        TaskModelData task1 = TestUtils.getTask(person, taskType, constituent, ETaskStatus.STAND_BY);
        assertNull(task1.getActualStartDate());

        TaskActivityModelData activity1 = TestUtils.getTaskActivity(task1, day1);
        task1.refresh();
        assertEquals(task1.getActualStartDate(), day1.getDate());
        TaskActivityModelData activity2 = TestUtils.getTaskActivity(task1, day2);
        task1.refresh();
        assertEquals(task1.getActualStartDate(), day2.getDate());

        activity1.delete();
        activity2.delete();
        task1.delete();
        taskType.delete();
        day1.delete();
        day2.delete();
    }
}
