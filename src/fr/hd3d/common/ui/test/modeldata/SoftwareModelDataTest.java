package fr.hd3d.common.ui.test.modeldata;

import java.util.Date;

import org.junit.Test;

import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.LicenseModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.SoftwareModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.test.util.modeldata.reader.SoftwareReaderMock;


public class SoftwareModelDataTest extends ModelDataTest<SoftwareModelData, SoftwareReaderMock>
{
    @Override
    public void setUp()
    {
        super.setUp();

        this.reader = new SoftwareReaderMock();
        this.path = ServicesPath.getPath("Software");
    }

    @Test
    public void testPostSoftware() throws Exception
    {
        SoftwareModelData software = new SoftwareModelData("soft_post_01");
        SoftwareModelData postedSoftware = this.saveObject(software);

        assertEquals(software.getName(), postedSoftware.getName());

        postedSoftware.delete();
    }

    @Test
    public void testPutSoftware() throws Exception
    {
        SoftwareModelData software = new SoftwareModelData("soft_put_01");
        SoftwareModelData putSoftware = this.saveObject(software);

        putSoftware.setName("test_put_soft_updated");

        SoftwareModelData putSoftware_server = this.saveObject(putSoftware);

        assertEquals(putSoftware.getName(), putSoftware_server.getName());

        putSoftware.delete();
    }

    @Test
    public void testDeleteSoftware() throws Exception
    {
        SoftwareModelData software = new SoftwareModelData("soft_delete_01");

        software.save();
        software.delete();
        software.refresh();

        assertEquals(CommonErrors.RECORD_NOT_ON_SERVER, controller.getLastError());
    }

    @Test
    public void testComputerList() throws Exception
    {
        SoftwareModelData software = new SoftwareModelData("soft_comp_01");
        ComputerModelData computer = new ComputerModelData("comp_software_01", "serial_01", "billing_01", new Date(),
                new Date(), defaultInventoryStatus, 123L, "fr.hd3d.laptop01", "127.0.0.1", "mac_adress", 1200L, 2L, 2L,
                4096L, "NO_WORKER", 160L, 35L);

        computer.save();
        software.getComputerIDs().add(computer.getId());
        SoftwareModelData postedSoftware = this.saveObject(software);
        computer.refresh();
        assertEquals(computer.getId().longValue(), postedSoftware.getComputerIDs().get(0).longValue());
        assertEquals(1, computer.getSoftwareIDs().size());
        assertEquals(postedSoftware.getId().longValue(), computer.getSoftwareIDs().get(0).longValue());

        ComputerModelData computer02 = new ComputerModelData("comp_software_02", "serial_02", "billing_02", new Date(),
                new Date(), defaultInventoryStatus, 123L, "fr.hd3d.laptop02", "127.0.0.1", "mac_adress", 1200L, 2L, 2L,
                4096L, "NO_WORKER", 160L, 35L);
        computer02.save();
        postedSoftware.getComputerIDs().add(computer02.getId());
        SoftwareModelData putSoftware = this.saveObject(postedSoftware);
        computer02.refresh();

        assertEquals(2, putSoftware.getComputerIDs().size());
        assertEquals(1, computer02.getSoftwareIDs().size());
        assertEquals(putSoftware.getId().longValue(), computer02.getSoftwareIDs().get(0).longValue());

        computer.getId();
        computer02.getId();
        putSoftware.delete();
    }

    @Test
    public void testLicenseList() throws Exception
    {
        SoftwareModelData software = new SoftwareModelData("soft_license_01");
        LicenseModelData license = new LicenseModelData("license_software_01", "serial_01", "billing_01", new Date(),
                new Date(), defaultInventoryStatus, 3L, "CLASSIC");

        license.save();
        software.getLicenseIDs().add(license.getId());

        SoftwareModelData postedSoftware = this.saveObject(software);
        license.refresh();

        LicenseModelData license02 = new LicenseModelData("license_soft_02", "serial_02", "billing_02", new Date(),
                new Date(), defaultInventoryStatus, 3L, "CLASSIC");
        license02.save();
        postedSoftware.getLicenseIDs().add(license02.getId());
        SoftwareModelData putSoftware = this.saveObject(postedSoftware);
        license02.refresh();
        assertEquals(1, putSoftware.getLicenseIDs().size());
        assertEquals(postedSoftware.getId().longValue(), license02.getSoftwareId().longValue());

        license.delete();
        license02.delete();
        putSoftware.delete();
    }

}
