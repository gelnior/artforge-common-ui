package fr.hd3d.common.ui.test.modeldata;

import java.util.Calendar;

import org.junit.Test;

import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.test.util.modeldata.reader.TaskReaderMock;


/**
 * Test task service validity.
 * 
 * @author HD3D
 */
public class TaskModelDataTest extends ModelDataTest<TaskModelData, TaskReaderMock>
{
    ProjectModelData project = new ProjectModelData("test", "open", "test");
    PersonModelData workerPerson = new PersonModelData("workerTestTask", "workerTest", "testTaskWorkerPerson",
            "worker@hd3d.fr", "workerTestNumber", "workerTestPath");
    PersonModelData creatorPerson = new PersonModelData("creatorTestTask", "workerTest", "testTaskCreatorPerson",
            "creator@hd3d.fr", "creatorTestNumber", "creatorTestPath");

    @Override
    public void setUp()
    {
        super.setUp();

        this.reader = new TaskReaderMock();

        project.refreshByName(CommonEvents.MODEL_DATA_REFRESH_SUCCESS);

        if (project.getId() == null)
        {
            project.save();
        }

        workerPerson.refreshByLogin(null);
        if (workerPerson.getId() == null)
        {
            workerPerson.save();
        }

        creatorPerson.refreshByLogin(null);
        if (creatorPerson.getId() == null)
        {
            creatorPerson.save();
        }
    }

    @Test
    public void testPostTask() throws Exception
    {

        TaskModelData postTask = new TaskModelData();

        Calendar task_post_date_end = Calendar.getInstance();
        Calendar task_post_date_start = Calendar.getInstance();
        Calendar task_post_date_deadline = Calendar.getInstance();

        task_post_date_end.set(2010, 4, 28, 16, 00, 00);
        task_post_date_end.set(Calendar.MILLISECOND, 0);

        task_post_date_start.set(2010, 4, 28, 12, 00, 00);
        task_post_date_start.set(Calendar.MILLISECOND, 0);

        task_post_date_deadline.set(2010, 4, 28, 20, 00, 00);
        task_post_date_deadline.set(Calendar.MILLISECOND, 0);

        postTask.setName("task_post_test_name");
        postTask.setStatus(ETaskStatus.OK.toString());
        postTask.setCompletion((byte) 0);
        postTask.setActualEndDate(task_post_date_end.getTime());
        postTask.setActualStartDate(task_post_date_start.getTime());
        postTask.setConfirmed(true);
        postTask.setDeadLine(task_post_date_deadline.getTime());
        postTask.setStartable(true);
        postTask.setProjectID(project.getId());
        postTask.setWorkerID(workerPerson.getId());
        postTask.setCreatorID(creatorPerson.getId());

        TaskModelData taskServer = this.saveObject(postTask);

        assertEquals(postTask.getName(), taskServer.getName());
        assertEquals(postTask.getStatus(), taskServer.getStatus());
        assertEquals(postTask.getCompletion(), taskServer.getCompletion());
        assertEquals(postTask.getActualEndDate(), taskServer.getActualEndDate());
        assertEquals(postTask.getActualStartDate(), taskServer.getActualStartDate());
        assertEquals(postTask.getConfirmed(), taskServer.getConfirmed());
        assertEquals(postTask.getDeadLine(), taskServer.getDeadLine());
        assertEquals(postTask.getStartable(), taskServer.getStartable());
        assertEquals(postTask.getProjectID(), taskServer.getProjectID());
        assertEquals(postTask.getWorkerID(), taskServer.getWorkerID());
        assertEquals(postTask.getCreatorID(), taskServer.getCreatorID());

        postTask.delete();
        taskServer.delete();
    }

    @Test
    public void testPutTask() throws Exception
    {
        TaskModelData putTask = new TaskModelData();

        putTask.setName("putTaskTest");
        putTask.setStatus(ETaskStatus.CLOSE.toString());
        putTask.setCompletion((byte) 0);
        putTask.setProjectID(project.getId());
        putTask.setWorkerID(workerPerson.getId());
        putTask.setCreatorID(creatorPerson.getId());

        TaskModelData postedPutTask = this.saveObject(putTask);

        assertEquals(putTask.getProjectID(), postedPutTask.getProjectID());
        assertEquals(putTask.getWorkerID(), postedPutTask.getWorkerID());
        assertEquals(putTask.getCreatorID(), postedPutTask.getCreatorID());

        TaskModelData putTask02 = new TaskModelData();
        putTask02.setName("putTaskTest");
        putTask02.setStatus(ETaskStatus.CLOSE.toString());
        putTask02.setCompletion((byte) 0);
        putTask02.setProjectID(project.getId());
        putTask02.setWorkerID(workerPerson.getId());
        putTask02.setCreatorID(creatorPerson.getId());

        putTask02.save();

        TaskModelData putTaskServer = this.saveObject(putTask02);

        putTask02.setWorkerID(creatorPerson.getId());
        putTask02.setCreatorID(workerPerson.getId());
        putTask02.refresh();

        assertEquals(putTaskServer.getProjectID(), putTask02.getProjectID());
        assertEquals(putTaskServer.getWorkerID(), putTask02.getWorkerID());
        assertEquals(putTaskServer.getCreatorID(), putTask02.getCreatorID());

        putTask.delete();
    }

    @Test
    public void testDeleteTask() throws Exception
    {
        TaskModelData task = new TaskModelData();
        task.setName("putTaskTest");
        task.setStatus(ETaskStatus.CLOSE.toString());
        task.setCompletion((byte) 0);
        task.save();
        task.delete();
        task.refresh();

        assertEquals(CommonErrors.RECORD_NOT_ON_SERVER, controller.getLastError());
    }
}
