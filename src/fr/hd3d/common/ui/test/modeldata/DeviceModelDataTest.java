package fr.hd3d.common.ui.test.modeldata;

import java.util.Date;

import org.junit.Test;

import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.DeviceModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.DeviceModelModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.DeviceTypeModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.test.util.modeldata.reader.DeviceReaderMock;


public class DeviceModelDataTest extends ModelDataTest<DeviceModelData, DeviceReaderMock>
{
    @Override
    public void setUp()
    {
        super.setUp();

        this.reader = new DeviceReaderMock();
        this.path = ServicesPath.getPath("Device");
    }

    @Test
    public void testPostDevice() throws Exception
    {
        DeviceModelData device = new DeviceModelData("device_post_01", "serial_01", "reference_01", new Date(),
                new Date(), defaultInventoryStatus, 1.2D);

        DeviceModelData postedDevice = this.saveObject(device);

        assertEquals(device.getName(), postedDevice.getName());
        assertEquals(device.getSerial(), postedDevice.getSerial());
        assertEquals(device.getBillingReference(), postedDevice.getBillingReference());
        assertEquals(dateFormatter.format(device.getPurchaseDate()), dateFormatter.format(postedDevice
                .getPurchaseDate()));
        assertEquals(dateFormatter.format(device.getWarrantyEnd()), dateFormatter.format(postedDevice.getWarrantyEnd()));
        assertEquals(device.getSize(), postedDevice.getSize());

        postedDevice.delete();
    }

    @Test
    public void testPutDevice() throws Exception
    {
        DeviceModelData device = new DeviceModelData("device_put_01", "serial_01", "reference_01", new Date(),
                new Date(), defaultInventoryStatus, 1.2D);

        DeviceModelData putDevice = this.saveObject(device);

        putDevice.setName("test_put_soft_updated");
        putDevice.setSerial("test_put_serial_updated");
        putDevice.setBillingReference("test_put_reference");
        putDevice.setPurchaseDate(new Date());
        putDevice.setWarrantyEnd(new Date());
        putDevice.setSize(1.4D);

        DeviceModelData putDevice_server = this.saveObject(putDevice);

        assertEquals(putDevice.getName(), putDevice_server.getName());
        assertEquals(putDevice.getSerial(), putDevice_server.getSerial());
        assertEquals(putDevice.getBillingReference(), putDevice_server.getBillingReference());
        assertEquals(dateFormatter.format(putDevice.getPurchaseDate()), dateFormatter.format(putDevice_server
                .getPurchaseDate()));
        assertEquals(dateFormatter.format(putDevice.getWarrantyEnd()), dateFormatter.format(putDevice_server
                .getWarrantyEnd()));
        assertEquals(putDevice.getSize(), putDevice_server.getSize());

        putDevice.delete();
    }

    @Test
    public void testDeleteDevice() throws Exception
    {
        DeviceModelData device = new DeviceModelData("device_delete_01", "serial_01", "reference_01", new Date(),
                new Date(), defaultInventoryStatus, 1.2D);

        device.save();
        device.delete();
        device.refresh();

        assertEquals(CommonErrors.RECORD_NOT_ON_SERVER, controller.getLastError());

    }

    @Test
    public void testComputersList() throws Exception
    {
        DeviceModelData device = new DeviceModelData("device_comp_01", "serial_01", "reference_01", new Date(),
                new Date(), defaultInventoryStatus, 1.2D);
        ComputerModelData computer = new ComputerModelData("comp_device_01", "serial_01", "billing_01", new Date(),
                new Date(), defaultInventoryStatus, 123L, "fr.hd3d.laptop01", "127.0.0.1", "mac_adress", 1200L, 2L, 2L,
                4096L, "NO_WORKER", 160L, 35L);

        computer.save();
        device.getComputerIDs().add(computer.getId());
        DeviceModelData postedDevice = this.saveObject(device);
        computer.refresh();
        assertEquals(computer.getId().longValue(), postedDevice.getComputerIDs().get(0).longValue());
        assertEquals(postedDevice.getId().longValue(), computer.getDeviceIDs().get(0).longValue());

        ComputerModelData computer2 = new ComputerModelData("comp_device_02", "serial_02", "billing_02", new Date(),
                new Date(), defaultInventoryStatus, 123L, "fr.hd3d.laptop02", "127.0.0.1", "mac_adress", 1200L, 2L, 2L,
                4096L, "NO_WORKER", 160L, 35L);
        computer2.save();
        postedDevice.getComputerIDs().add(computer2.getId());
        DeviceModelData putDevice = this.saveObject(postedDevice);
        computer2.refresh();
        assertEquals(2, putDevice.getComputerIDs().size());
        assertEquals(postedDevice.getId().longValue(), computer.getDeviceIDs().get(0).longValue());

        computer2.delete();
        computer.delete();
        putDevice.delete();
    }

    @Test
    public void testDeviceModel() throws Exception
    {
        DeviceModelData device = new DeviceModelData("device_device_model_01", "serial_01", "reference_01", new Date(),
                new Date(), defaultInventoryStatus, 1.2D);
        DeviceModelModelData deviceModel = new DeviceModelModelData("device_model_device_01");

        deviceModel.save();
        device.setDeviceModelId(deviceModel.getId());
        device = this.saveObject(device);

        assertEquals(deviceModel.getId().longValue(), device.getDeviceModelId().longValue());
        assertEquals(deviceModel.getName(), device.getDeviceModelName());

        DeviceModelModelData deviceModel02 = new DeviceModelModelData("device_model_device_02");
        deviceModel02.save();
        device.setDeviceModelId(deviceModel02.getId());
        device = this.saveObject(device);

        assertEquals(deviceModel02.getId().longValue(), device.getDeviceModelId().longValue());
    }

    @Test
    public void testDeviceType() throws Exception
    {
        DeviceModelData device = new DeviceModelData("device_device_type_01", "serial_01", "reference_01", new Date(),
                new Date(), defaultInventoryStatus, 1.2D);
        DeviceTypeModelData deviceType = new DeviceTypeModelData("device_type_device_01");
        deviceType.save();

        device.setDeviceTypeId(deviceType.getId());
        DeviceModelData deviceServer = this.saveObject(device);
        assertEquals(deviceType.getId(), deviceServer.getDeviceTypeId());

        DeviceTypeModelData deviceType02 = new DeviceTypeModelData("device_type_device_02");
        deviceType02.save();
        device.setDeviceTypeId(deviceType02.getId());
        device.save();
        deviceServer.refresh();
        assertEquals(deviceType02.getId(), deviceServer.getDeviceTypeId());

        device.delete();
        deviceType.delete();
        deviceType02.delete();
    }

    @Test
    public void testResourceGroupList() throws Exception
    {
        ResourceGroupModelData resourceGroup = new ResourceGroupModelData("resourcegroup_device_01", null);
        DeviceModelData device = new DeviceModelData("device_resourcegroup_type_01", "serial_01", "reference_01",
                new Date(), new Date(), defaultInventoryStatus, 1.2D);

        resourceGroup.save();
        device.getResourceGroupIDs().add(resourceGroup.getId());
        DeviceModelData deviceServer = this.saveObject(device);
        resourceGroup.refresh();

        assertEquals(1, deviceServer.getResourceGroupIDs().size());
        assertEquals(resourceGroup.getId(), deviceServer.getResourceGroupIDs().get(0));
        assertEquals(1, resourceGroup.getResourceIds().size());
        assertEquals(deviceServer.getId(), resourceGroup.getResourceIds().get(0));

        ResourceGroupModelData resourceGroup02 = new ResourceGroupModelData("resourcegroup_device_02", null);
        resourceGroup02.save();
        device.getResourceGroupIDs().add(resourceGroup02.getId());
        device.save();
        device.refresh();

        assertEquals(2, device.getResourceGroupIDs().size());
        assertEquals(1, resourceGroup.getResourceIds().size());
        assertEquals(device.getId(), resourceGroup.getResourceIds().get(0));

        resourceGroup.delete();
        resourceGroup02.delete();
        device.delete();
    }
}
