package fr.hd3d.common.ui.test.modeldata;

import org.junit.Test;

import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ManufacturerModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.test.util.modeldata.reader.ComputerModelReaderMock;


public class ComputerModelModelDataTest extends ModelDataTest<ComputerModelModelData, ComputerModelReaderMock>
{
    @Override
    public void setUp()
    {
        super.setUp();

        this.reader = new ComputerModelReaderMock();
        this.path = ServicesPath.getPath("ComputerModel");
    }

    @Test
    public void testPostComputerModel() throws Exception
    {
        ComputerModelModelData computerModel = new ComputerModelModelData("computer_model_post_01");

        ComputerModelModelData postedComputerModel = this.saveObject(computerModel);
        assertEquals(computerModel.getName(), postedComputerModel.getName());

        postedComputerModel.delete();
    }

    @Test
    public void testPutComputerModel() throws Exception
    {
        ComputerModelModelData license = new ComputerModelModelData("computer_model_put_01");
        ComputerModelModelData putComputerModel = this.saveObject(license);

        putComputerModel.setName("computer_model_put_01_updated");
        ComputerModelModelData putComputerModel_server = this.saveObject(putComputerModel);
        assertEquals(putComputerModel.getName(), putComputerModel_server.getName());

        putComputerModel.delete();
    }

    @Test
    public void testDeleteComputerModel() throws Exception
    {
        ComputerModelModelData computerModel = new ComputerModelModelData("computer_model_delete_01");

        computerModel.save();
        computerModel.delete();
        computerModel.refresh();

        assertEquals(CommonErrors.RECORD_NOT_ON_SERVER, controller.getLastError());
    }

    @Test
    public void testManufacturer() throws Exception
    {
        ComputerModelModelData computerModel = new ComputerModelModelData("comp_model_manufacturer_01");

        ManufacturerModelData manufacturer = new ManufacturerModelData("manufacturer_comp_model_01");
        manufacturer.save();

        computerModel.setManufacturerId(manufacturer.getId());
        ComputerModelModelData postedComputerModel = this.saveObject(computerModel);
        assertEquals(manufacturer.getId().longValue(), postedComputerModel.getManufacturerId().longValue());

        ManufacturerModelData manufacturer2 = new ManufacturerModelData("manufacturer_comp_model_02");
        manufacturer2.save();
        postedComputerModel.setManufacturerId(manufacturer2.getId());
        ComputerModelModelData putComputerModel = this.saveObject(postedComputerModel);

        manufacturer2.refresh();
        assertEquals(manufacturer2.getId().longValue(), putComputerModel.getManufacturerId().longValue());

        manufacturer.delete();
        manufacturer2.delete();
        putComputerModel.delete();
    }
}
