package fr.hd3d.common.ui.test.modeldata;

import org.junit.Test;

import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteTypeModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.reader.ReaderFactory;
import fr.hd3d.common.ui.client.modeldata.task.PlanningModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskGroupModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.parameter.EqConstraint;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.test.util.TestUtils;
import fr.hd3d.common.ui.test.util.modeldata.reader.ProjectReaderMock;


public class ProjectModelDataTest extends ModelDataTest<ProjectModelData, ProjectReaderMock>
{
    @Override
    public void setUp()
    {
        super.setUp();

        this.reader = new ProjectReaderMock();
    }

    // @SuppressWarnings("deprecation")
    // @Test
    // public void testPostProject() throws Exception
    // {
    // ProjectModelData postProject = new ProjectModelData("testPostProject", new Date(2011, 05, 01), new Date(2011,
    // 05, 04), EProjectStatus.OPEN.toString(), "testPostProjectHook");
    //
    // ProjectModelData postProjectServer = this.saveObject(postProject);
    //
    // assertEquals(postProject.getId(), postProjectServer.getId());
    // assertEquals(postProject.getName(), postProjectServer.getName());
    // assertEquals(postProject.getStartDate(), postProjectServer.getStartDate());
    // assertEquals(postProject.getEndDate(), postProjectServer.getEndDate());
    // assertEquals(postProject.getStatus(), postProjectServer.getStatus());
    // assertEquals(postProject.getHook(), postProjectServer.getHook());
    //
    // postProject.delete();
    // postProjectServer.delete();
    // }
    //
    // @SuppressWarnings("deprecation")
    // @Test
    // public void testPutProject() throws Exception
    // {
    // ProjectModelData putProject = new ProjectModelData("testPutProject", new Date(2011, 05, 01), new Date(2011, 05,
    // 04), EProjectStatus.OPEN.toString(), "testPutProjectHook");
    //
    // ProjectModelData putProjectServer = this.saveObject(putProject);
    //
    // putProjectServer.setName("testPutProjectServer");
    // putProjectServer.setStatus(EProjectStatus.CLOSED.toString());
    // putProjectServer.setStartDate(new Date(2012, 05, 01));
    // putProjectServer.setEndDate(new Date(2012, 05, 01));
    //
    // ProjectModelData putProjectServerUpdate = this.saveObject(putProjectServer);
    //
    // assertEquals(putProjectServer.getName(), putProjectServerUpdate.getName());
    // assertEquals(putProjectServer.getStatus(), putProjectServerUpdate.getStatus());
    // assertEquals(putProjectServer.getStartDate(), putProjectServerUpdate.getStartDate());
    // assertEquals(putProjectServer.getEndDate(), putProjectServerUpdate.getEndDate());
    //
    // putProject.delete();
    // putProjectServer.delete();
    // putProjectServerUpdate.delete();
    // }
    //
    // @Test
    // public void testDeleteProject() throws Exception
    // {
    // ProjectModelData delProject = new ProjectModelData("testDelProject", EProjectStatus.OPEN.toString(),
    // "testDelProjectHook");
    //
    // delProject.save();
    // delProject.delete();
    // delProject.refresh();
    //
    // assertEquals(CommonErrors.RECORD_NOT_ON_SERVER, controller.getLastError());
    // }
    //
    // @Test
    // public void testGroupProject() throws Exception
    // {
    // ResourceGroupModelData resourceGroup = new ResourceGroupModelData("person_testGrp_01_group", null);
    //
    // PersonModelData person = new PersonModelData("person_testGrp_01_firstName", "person_testGrp_01_lastName",
    // "person_testGrp_01_login", "person_testGrp_01_email", "person_testGrp_01_phone",
    // "person_testGrp_01_path");
    //
    // ProjectModelData project = new ProjectModelData("testGroupProject", EProjectStatus.OPEN.toString(),
    // "testGroupProjectHook");
    //
    // person.refreshByLogin(null);
    // if (person.getId() == null)
    // {
    // person.save();
    // }
    //
    // resourceGroup.getResourceIds().add(person.getId());
    // resourceGroup.save();
    //
    // project.refreshByName(CommonEvents.MODEL_DATA_REFRESH_SUCCESS);
    //
    // if (project.getId() == null)
    // {
    // project.save();
    // }
    //
    // project.setResourceGroupIDs(CollectionUtils.asList(resourceGroup.getId()));
    //
    // ProjectModelData projectServer = this.saveObject(project);
    //
    // assertEquals(1, projectServer.getResourceGroupIds().size());
    // assertEquals(resourceGroup.getId(), projectServer.getResourceGroupIds().get(0));
    //
    // resourceGroup.delete();
    // project.delete();
    // }

    @Test
    public void testTaskType() throws Exception
    {
        TaskTypeModelData taskType = TestUtils.getTaskType("test_tasktype_projects_12");
        TaskTypeModelData taskType2 = TestUtils.getTaskType("test_tasktype_projects_23");
        ProjectModelData project = TestUtils.getOpenProject("test_tasktype_project");
        project.getTaskTypeIDs().clear();
        project.save();

        project.getTaskTypeIDs().add(taskType.getId());
        ProjectModelData projectServer = this.saveObject(project);
        assertEquals(taskType.getId(), projectServer.getTaskTypeIDs().get(0));

        projectServer.getTaskTypeIDs().add(taskType2.getId());
        ProjectModelData putProject = this.saveObject(projectServer);
        assertEquals(2, putProject.getTaskTypeIDs().size());

        ServiceStore<ApprovalNoteTypeModelData> noteTypes = new ServiceStore<ApprovalNoteTypeModelData>(
                ReaderFactory.getApprovalNoteTypeReader());
        noteTypes.addParameter(new EqConstraint("project.id", project.getId()));
        noteTypes.reload();

        PlanningModelData planning = new PlanningModelData();
        planning.setProject(project);
        planning.refreshWithParams(null, new EqConstraint("project.id", project.getId()));
        assertNotNull(planning.getId());
        assertEquals(2, noteTypes.getCount());

        ServiceStore<TaskGroupModelData> taskGroups = new ServiceStore<TaskGroupModelData>(
                ReaderFactory.getTaskGroupReader());
        taskGroups.setPath(planning.getDefaultPath() + "/" + ServicesPath.TASKGROUPS);
        taskGroups.addParameter(new EqConstraint("planning.id", planning.getId()));
        taskGroups.reload();

        assertEquals(2, taskGroups.getCount());
        putProject.getTaskTypeIDs().remove(0);
        putProject.save();
        noteTypes.reload();
        assertEquals(1, noteTypes.getCount());

        taskType.delete();
        taskType2.delete();
        putProject.getTaskTypeIDs().clear();
        putProject.save();
        noteTypes.reload();
        assertEquals(0, noteTypes.getCount());

        project.delete();
    }
}
