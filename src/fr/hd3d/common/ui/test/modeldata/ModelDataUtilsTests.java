package fr.hd3d.common.ui.test.modeldata;

import junit.framework.Assert;

import org.junit.Test;

import com.extjs.gxt.ui.client.data.DataField;

import fr.hd3d.common.ui.client.modeldata.ModelDataUtils;


public class ModelDataUtilsTests
{
    private static final String NAME = "Class";
    private static final Class<?> CLAZZ = Class.class;
    private static final String FORMAT = "pouet";

    @SuppressWarnings("deprecation")
    @Test(expected = NullPointerException.class)
    public void testCreateDataFiledWithoutName()
    {

        ModelDataUtils.createDataField(null, CLAZZ, FORMAT);
    }

    @SuppressWarnings("deprecation")
    @Test(expected = NullPointerException.class)
    public void testCreateDataFiledWithoutClass()
    {
        ModelDataUtils.createDataField(NAME, null, FORMAT);

    }

    @Test
    public void testCreateDataField()
    {
        @SuppressWarnings("deprecation")
        final DataField df = ModelDataUtils.createDataField(NAME, CLAZZ, FORMAT);
        Assert.assertNotNull(df);
        Assert.assertEquals(NAME, df.getName());
        Assert.assertEquals(CLAZZ, df.getType());
        Assert.assertEquals(FORMAT, df.getFormat());
    }

    @Test
    public void testCreateDataFieldWithoutFormat()
    {
        @SuppressWarnings("deprecation")
        final DataField df = ModelDataUtils.createDataField(NAME, CLAZZ);
        Assert.assertNotNull(df);
        Assert.assertEquals(NAME, df.getName());
        Assert.assertEquals(CLAZZ, df.getType());
        Assert.assertEquals(null, df.getFormat());

    }

}
