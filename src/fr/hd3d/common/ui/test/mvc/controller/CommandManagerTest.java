package fr.hd3d.common.ui.test.mvc.controller;

import junit.framework.TestCase;

import org.junit.Test;

import com.google.gwt.user.client.Command;

import fr.hd3d.common.ui.client.undo.CommandManager;
import fr.hd3d.common.ui.client.undo.UndoableCommand;


public class CommandManagerTest extends TestCase
{
    private int counter = 0;
    private final CommandManager manager = new CommandManager();
    private final Command cmd = new Command() {
        public void execute()
        {
            counter++;
        }
    };

    private final UndoableCommand undoCmd = new UndoableCommand() {

        public void execute()
        {
            counter++;
        }

        @Override
        public void undo()
        {
            counter--;
        }
    };

    @Test
    public void testExecuteCommand()
    {

        manager.executeCommand(cmd);
        manager.executeCommand(cmd);
        manager.executeCommand(cmd);
        manager.executeCommand(cmd);

        assertEquals(4, counter);
        assertEquals(0, manager.getUndoCommandStack().size());
        assertEquals(0, manager.getRedoCommandStack().size());
    }

    @Test
    public void testExecuteUndoableCommand()
    {
        counter = 0;
        manager.clearStacks();

        manager.executeCommand(undoCmd);
        manager.executeCommand(undoCmd);
        manager.executeCommand(undoCmd);
        manager.executeCommand(undoCmd);

        assertEquals(4, counter);
        assertEquals(4, manager.getUndoCommandStack().size());
        assertEquals(0, manager.getRedoCommandStack().size());
    }

    @Test
    public void testUndoExecuteCommand()
    {
        this.testExecuteUndoableCommand();

        manager.undo();
        manager.undo();
        manager.undo();

        assertEquals(1, counter);
        assertEquals(1, manager.getUndoCommandStack().size());
        assertEquals(3, manager.getRedoCommandStack().size());
    }

    @Test
    public void testRedoExecuteCommand()
    {
        this.testUndoExecuteCommand();

        manager.redo();
        manager.redo();

        assertEquals(3, counter);
        assertEquals(3, manager.getUndoCommandStack().size());
        assertEquals(1, manager.getRedoCommandStack().size());
    }

    @Test
    public void testExecuteCommandAfterUndoRedo()
    {
        this.testRedoExecuteCommand();

        manager.executeCommand(undoCmd);

        assertEquals(4, counter);
        assertEquals(4, manager.getUndoCommandStack().size());
        assertEquals(0, manager.getRedoCommandStack().size());

        manager.executeCommand(cmd);
        assertEquals(4, manager.getUndoCommandStack().size());
        assertEquals(0, manager.getRedoCommandStack().size());

    }

    @Test
    public void testClearStacks()
    {
        this.testRedoExecuteCommand();

        manager.clearStacks();

        assertEquals(3, counter);
        assertEquals(0, manager.getUndoCommandStack().size());
        assertEquals(0, manager.getRedoCommandStack().size());
    }
}
