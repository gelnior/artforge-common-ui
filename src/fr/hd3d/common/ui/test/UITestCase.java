package fr.hd3d.common.ui.test;

import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;

import junit.framework.TestCase;

import org.restlet.Context;

import fr.hd3d.common.ui.client.modeldata.reader.Hd3dModelDataReaderSingleton;
import fr.hd3d.common.ui.client.modeldata.reader.ReaderFactory;
import fr.hd3d.common.ui.client.modeldata.writer.JsonWriterSingleton;
import fr.hd3d.common.ui.client.modeldata.writer.ModelDataJsonWriterSingleton;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.PermissionPath;
import fr.hd3d.common.ui.client.service.ServicesModelType;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.test.util.RestRequestHandlerMock;
import fr.hd3d.common.ui.test.util.controller.ErrorController;
import fr.hd3d.common.ui.test.util.modeldata.reader.ReaderBuilderMock;
import fr.hd3d.common.ui.test.util.modeldata.writer.JSONWriterMock;
import fr.hd3d.common.ui.test.util.modeldata.writer.ModelDataJsonWriterMock;
import fr.hd3d.common.ui.test.widget.sheeteditor.mock.Hd3dModelDataReaderMock;


public abstract class UITestCase extends TestCase
{
    protected static final String UTF_8 = "UTF-8";
    protected static final String JSON_HEADER_VALUE = "application/json";

    protected String serviceUrl = "";
    protected ErrorController errorController = new ErrorController();

    @Override
    public void setUp()
    {
        Context.getCurrentLogger().setLevel(Level.OFF);
        RestRequestHandlerMock requestHandler = new RestRequestHandlerMock();
        RestRequestHandlerSingleton.setInstanceAsMock(requestHandler);

        Hd3dModelDataReaderMock reader = new Hd3dModelDataReaderMock();
        Hd3dModelDataReaderSingleton.setInstanceAsMock(reader);
        ReaderFactory.setReaderBuilder(new ReaderBuilderMock());

        JsonWriterSingleton.setInstanceAsMock(new JSONWriterMock());
        ModelDataJsonWriterSingleton.setInstanceAsMock(new ModelDataJsonWriterMock());

        ServicesModelType.initModelTypes();
        ServicesPath.initPath();
        PermissionPath.initPath();

        try
        {
            InputStream in = this.getClass().getClassLoader().getResourceAsStream("services.properties");
            Properties properties = new Properties();
            properties.load(in);
            requestHandler.setServerUrl(properties.getProperty("server"));
            requestHandler.setServicePath(properties.getProperty("path"));

            InputStream testIn = this.getClass().getClassLoader().getResourceAsStream("test.properties");
            Properties credentials = new Properties();
            credentials.load(testIn);
            requestHandler.setRootUsername(credentials.getProperty("root.username"));
            requestHandler.setRootPassword(credentials.getProperty("root.password"));

            this.serviceUrl = requestHandler.getServicesUrl();
        }
        catch (Exception e)
        {
            System.out.println(String.valueOf(e.getStackTrace()));
        }
    }
}
