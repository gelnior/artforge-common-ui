package fr.hd3d.common.ui.test;

import java.util.Date;

import junit.framework.TestCase;

import org.junit.Test;

import com.extjs.gxt.ui.client.util.DateWrapper;

import fr.hd3d.common.ui.client.calendar.DatetimeUtil;


public class DateTimeUtilTest extends TestCase
{
    @Test
    public void testToday()
    {
        DateWrapper nowWrapper = new DateWrapper();
        Date now = DatetimeUtil.today();
        assertEquals(nowWrapper.clearTime().asDate(), now);
    }

    @Test
    public void testDaySeconds()
    {
        Long daySeconds = DatetimeUtil.getDaySeconds(new DateWrapper().clearTime().addHours(2).addMinutes(10).asDate());
        assertEquals(daySeconds.longValue(), 7800L);
    }

    @Test
    public void testTomorrow()
    {
        DateWrapper nowWrapper = new DateWrapper();
        Date tommorow = DatetimeUtil.tomorrow();
        assertEquals(nowWrapper.clearTime().addDays(1).asDate(), tommorow);
    }

    @Test
    public void testGetNbWeekEndDayBetween()
    {
        DateWrapper date1 = new DateWrapper(2011, 02, 01);
        DateWrapper date2 = new DateWrapper(2011, 02, 04);
        assertEquals(0, DatetimeUtil.getNbWeekEndDayBetween(date1, date2).intValue());

        date2 = new DateWrapper(2011, 02, 8);
        assertEquals(2, DatetimeUtil.getNbWeekEndDayBetween(date1, date2).intValue());

        date2 = new DateWrapper(2011, 02, 15);
        assertEquals(4, DatetimeUtil.getNbWeekEndDayBetween(date1, date2).intValue());
    }

    @Test
    public void testIsWeekend()
    {
        DateWrapper date1 = new DateWrapper(2011, 02, 01);
        assertFalse(DatetimeUtil.isWeekend(date1));
        date1 = new DateWrapper(2011, 02, 02);
        assertFalse(DatetimeUtil.isWeekend(date1));
        date1 = new DateWrapper(2011, 02, 03);
        assertFalse(DatetimeUtil.isWeekend(date1));
        date1 = new DateWrapper(2011, 02, 04);
        assertFalse(DatetimeUtil.isWeekend(date1));
        date1 = new DateWrapper(2011, 02, 05);
        assertTrue(DatetimeUtil.isWeekend(date1));
        date1 = new DateWrapper(2011, 02, 06);
        assertTrue(DatetimeUtil.isWeekend(date1));
        date1 = new DateWrapper(2011, 02, 07);
        assertFalse(DatetimeUtil.isWeekend(date1));
    }

    @Test
    public void testIsDay()
    {
        DateWrapper date1 = new DateWrapper(2011, 02, 7);
        assertTrue(DatetimeUtil.isMonday(date1));

        date1 = new DateWrapper(2011, 02, 8);
        assertTrue(DatetimeUtil.isTuesday(date1));

        date1 = new DateWrapper(2011, 02, 9);
        assertTrue(DatetimeUtil.isWednesday(date1));

        date1 = new DateWrapper(2011, 02, 10);
        assertTrue(DatetimeUtil.isThursday(date1));

        date1 = new DateWrapper(2011, 02, 11);
        assertTrue(DatetimeUtil.isFriday(date1));

    }
}
