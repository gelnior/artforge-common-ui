package fr.hd3d.common.ui.test.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import org.junit.Test;
import org.restlet.Request;
import org.restlet.client.Response;
import org.restlet.client.Uniform;
import org.restlet.client.data.Form;
import org.restlet.client.data.MediaType;
import org.restlet.client.data.Method;
import org.restlet.client.data.Status;
import org.restlet.client.representation.Representation;
import org.restlet.client.representation.StringRepresentation;
import org.restlet.data.Protocol;
import org.restlet.resource.ClientResource;

import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;


/**
 * Mock request handler to replace GWT request handler when HTTP calls are sent to REST web services.
 * 
 * @author HD3D
 */
public class RestRequestHandlerMock extends TestCase implements IRestRequestHandler
{

    protected static final String TEXT_HEADER_VALUE = "text/plain";
    protected static final String ACCEPT_HEADER = "Accept";
    protected static final String JSON_HEADER_VALUE = "application/json";
    protected static final String CONTENT_TYPE = "Content type";
    protected static final String CONTENT_LOCATION = "CONTENT-LOCATION";
    protected static final String CREATION_SUCCEED = "Creation ok";
    protected static final String UTF_8 = "UTF-8";
    protected static final String DELETE = "DELETE";

    /** Client used for sending HTTP requests and handling responses. */
    protected TestClient client = new TestClient(Protocol.HTTP);

    /** Path where services are accessible. */
    private String servicePath = "";

    /** Services server URL */
    private String serverUrl = "";

    /** Default constructor : initialize the request client. */
    public RestRequestHandlerMock()
    {
        try
        {
            client.start();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /** @return URL where services are reachable. */
    public final String getServicesUrl()
    {
        return this.serverUrl + this.servicePath;
    }

    /** @return The URL of the server where requests are sent. */
    public String getServerUrl()
    {
        return this.serverUrl;
    }

    /** @return Path relative to services URL. */
    public String getServicePath()
    {
        return this.servicePath;
    }

    /** Sets path where services are accessible. */
    public final void setServerUrl(String serverUrl)
    {
        this.serverUrl = serverUrl;
    }

    /** Sets path where services are accessible. */
    public final void setServicePath(String servicePath)
    {
        this.servicePath = servicePath;
    }

    /**
     * Sets the user name to send to the server for authentication
     * 
     * @param username
     *            the username to set
     */
    public void setRootUsername(String username)
    {
        client.setRootUsername(username);
    }

    /**
     * Sets the user password to send to the server for authentication
     * 
     * @param password
     *            the password to set
     */
    public void setRootPassword(String password)
    {
        client.setRootPassword(password);
    }

    /**
     * Send a request to path and call handle method of GWT callback, to make it works like in a navigator. Method of
     * request is defined in parameter, such as data to send for POST and PUT requests.
     * 
     * @param method
     *            HTTP method of the request.
     * @param path
     *            The path where to send request (relative form services URL).
     * @param jsonObject
     *            JSON data to send for POST and PUT requests.
     * @param callback
     *            The GWT callback.
     */
    public void handleRequest(Method method, String path, String jsonObject, BaseCallback callback)
    {
        if (method == Method.GET)
        {
            this.getRequest(path, callback);
        }
        else if (method == Method.POST)
        {
            this.postRequest(path, callback, jsonObject);
        }
        else if (method == Method.PUT)
        {
            this.putRequest(path, callback, jsonObject);
        }
        else if (method == Method.DELETE)
        {
            this.deleteRequest(path, callback);
        }
    }

    /**
     * Send a GET request to path and call handle method of GWT callback, to make it works like in a navigator.
     * 
     * @param path
     *            The path where to send request (relative form services URL).
     * @param callback
     *            The GWT callback.
     */
    public void getRequest(String path, BaseCallback callback)
    {
        try
        {
            path = path.replace("%", "%25");
            String url = this.getServicesUrl() + path;

            ClientResource resource = new ClientResource(url);
            resource.setNext(client);

            org.restlet.representation.Representation response_r = resource.get();
            Response response_restlet = new Response(null);

            response_restlet.setEntity(response_r.getText(), MediaType.APPLICATION_JSON);
            response_restlet.setStatus(new Status(resource.getStatus().getCode()));

            callback.handle(null, response_restlet);
            response_r.exhaust();
        }
        catch (IOException e)
        {
            Response response_restlet = new Response(null);
            response_restlet.setStatus(Status.CONNECTOR_ERROR_COMMUNICATION);
            callback.handle(null, response_restlet);
        }
    }

    public String parseISToString(java.io.InputStream is)
    {
        StringWriter writer = new StringWriter();
        InputStreamReader streamReader = new InputStreamReader(is);

        BufferedReader buffer = new BufferedReader(streamReader);
        String line = "";
        try
        {
            while (null != (line = buffer.readLine()))
            {
                writer.write(line);
            }
            return writer.toString();
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * Send a DELETE request to path and call handle method of GWT callback, to make it works like in a navigator.
     * 
     * @param path
     *            The path where to send request (relative form services URL).
     * @param callback
     *            The GWT callback.
     */
    public void deleteRequest(String path, BaseCallback callback)
    {
        try
        {
            String url = this.serverUrl + this.servicePath + path;
            Request request = new Request(org.restlet.data.Method.DELETE, url);
            org.restlet.Response response_r = client.handle(request);

            Response response_restlet = new Response(null);
            response_restlet.setStatus(new Status(response_r.getStatus().getCode()));

            callback.handle(null, response_restlet);

        }
        catch (Exception e)
        {
            Response response_restlet = new Response(null);
            response_restlet.setStatus(Status.CONNECTOR_ERROR_COMMUNICATION);
            callback.handle(null, response_restlet);
        }

    }

    /**
     * Send a POST request to path and call handle method of GWT callback, to make it works like in a navigator.
     * 
     * @param path
     *            The path where to send request (relative form services URL).
     * @param jsonObject
     *            The data to post.
     * @param callback
     *            The GWT callback.
     */
    public void postRequest(String path, BaseCallback callback, String jsonObject)
    {
        try
        {
            String url = this.getServicesUrl() + path;
            url = url.replace("%", "%25");

            org.restlet.representation.StringRepresentation requestRepresentation = new org.restlet.representation.StringRepresentation(
                    jsonObject);
            ClientResource resource = new ClientResource(url);
            resource.setNext(client);

            org.restlet.representation.Representation rep = resource.post(requestRepresentation); //

            Representation representation = new StringRepresentation(null);
            String uri = rep.getLocationRef().getIdentifier();
            org.restlet.client.data.Reference ref = new org.restlet.client.data.Reference(uri);
            representation.setLocationRef(ref);

            Response response_restlet = new Response(null);
            response_restlet.setEntity(representation);
            response_restlet.setStatus(new Status(resource.getStatus().getCode()));

            callback.handle(null, response_restlet);
            rep.exhaust();
        }
        catch (Exception e)
        {
            Response response_restlet = new Response(null);
            response_restlet.setStatus(Status.CONNECTOR_ERROR_COMMUNICATION);
            callback.handle(null, response_restlet);
        }
    }

    /**
     * Send a PUT request to path and call handle method of GWT callback, to make it works like in a navigator.
     * 
     * @param path
     *            The path where to send request (relative form services URL).
     * @param jsonObject
     *            The data to post.
     * @param callback
     *            The GWT callback.
     */
    public void putRequest(String path, BaseCallback callback, String jsonObject)
    {
        try
        {
            String url = this.getServicesUrl() + path;
            url = url.replace("%", "%25");

            org.restlet.representation.StringRepresentation requestRepresentation = new org.restlet.representation.StringRepresentation(
                    jsonObject);
            ClientResource resource = new ClientResource(org.restlet.data.Method.PUT, url);
            resource.setNext(client);
            org.restlet.representation.Representation rep = resource.put(requestRepresentation);

            Response response_restlet = new Response(null);
            response_restlet.setStatus(new Status(resource.getStatus().getCode()));

            callback.handle(null, response_restlet);
            rep.exhaust();
        }
        catch (Exception e)
        {
            Response response_restlet = new Response(null);
            response_restlet.setStatus(Status.CONNECTOR_ERROR_COMMUNICATION);
            callback.handle(null, response_restlet);
        }
    }

    public void handleAttributeRequest(Method method, String path, Form form, BaseCallback callback)
    {
        handleAttributeRequest(path, form, callback, Method.PUT.equals(method));
    }

    public void handleAttributeRequest(String path, Form form, Uniform callback, boolean putMethod)
    {
        try
        {
            String url = this.getServerUrl();

            // Workaround to handle login request.
            if (!"../login".equals(path))
                url += this.getServerUrl() + getServicePath() + path;
            else
                url += getServicePath().substring(0, getServicePath().length() - 4) + "/login";

            org.restlet.data.Form restletForm = new org.restlet.data.Form();
            Map<String, Object> map = new HashMap<String, Object>();

            for (Map.Entry<String, String> entry : form.getValuesMap().entrySet())
            {
                restletForm.add(entry.getKey(), entry.getValue());
                map.put(entry.getKey(), entry.getValue());
            }

            ClientResource resource = new ClientResource(url);
            resource.setNext(client);
            org.restlet.representation.Representation rep;

            if (putMethod)
            {
                rep = resource.put(restletForm.getWebRepresentation());
            }
            else
            {
                org.restlet.representation.Representation formRep = restletForm.getWebRepresentation();
                rep = resource.post(formRep);
            }

            Response response_restlet = new Response(null);
            response_restlet.setEntity(rep.getText(), MediaType.APPLICATION_JSON);
            response_restlet.setStatus(new Status(resource.getStatus().getCode()));

            callback.handle(null, response_restlet);
        }
        catch (IOException e)
        {
            Response response_restlet = new Response(null);
            response_restlet.setStatus(Status.CONNECTOR_ERROR_COMMUNICATION);
            callback.handle(null, response_restlet);
        }
        catch (Exception e)
        {
            Response response_restlet = new Response(null);
            response_restlet.setEntity(new StringRepresentation("Error"));
            response_restlet.setStatus(Status.SERVER_ERROR_INTERNAL);
            callback.handle(null, response_restlet);
        }
    }

    /**
     * @param The
     *            location from which ID should be extracted.
     * @return object ID from location by extracting it from last segments of location.
     */
    public Long getIdFromLocation(String location)
    {
        String contentID = location.substring(location.lastIndexOf('/') + 1, location.length());
        return Long.parseLong(contentID);
    }

    @Test
    public void testServiceUrl()
    {
        String serverUrl = "http://test/";
        String servicesPath = "services/";

        RestRequestHandlerMock requestHandler = new RestRequestHandlerMock();
        requestHandler.setServerUrl(serverUrl);
        requestHandler.setServicePath(servicesPath);

        assertEquals(serverUrl + servicesPath, requestHandler.getServicesUrl());
    }

}
