package fr.hd3d.common.ui.test.util.controller;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.mvc.Controller;

import fr.hd3d.common.ui.client.config.CommonConfig;
import fr.hd3d.common.ui.client.event.CommonEvents;


public class ErrorController extends Controller
{
    public int error = -1;

    public ErrorController()
    {
        this.registerEvents();
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();
        if (type == CommonEvents.ERROR)
        {
            this.onError(event);
        }
        else
        {
            this.forwardToChild(event);
        }
    }

    public int getLastError()
    {
        return this.error;
    }

    public boolean compareToLastError(Integer errorToCompare)
    {
        return this.error == errorToCompare.intValue();
    }

    /**
     * When error occurs, it save the last error code.
     * 
     * @param event
     *            The error event.
     */
    protected void onError(AppEvent event)
    {
        Integer error = event.getData(CommonConfig.ERROR_EVENT_VAR_NAME);
        this.error = error.intValue();

        this.forwardToChild(event);
    }

    protected void registerEvents()
    {
        this.registerEventTypes(CommonEvents.ERROR);
    }

    public void reset()
    {
        this.error = -1;
    }
}
