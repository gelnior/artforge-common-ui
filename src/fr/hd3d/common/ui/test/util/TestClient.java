package fr.hd3d.common.ui.test.util;

import org.restlet.Client;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.data.ChallengeResponse;
import org.restlet.data.ChallengeScheme;
import org.restlet.data.Protocol;


public class TestClient extends Client
{

    private String root;
    private String rootpass;

    public TestClient(Protocol protocol)
    {
        super(protocol);
    }

    /**
     * Sets the user name to send to the server for authentication
     * 
     * @param username
     *            the username to set
     */
    public void setRootUsername(String username)
    {
        root = username;
    }

    /**
     * Sets the user password to send to the server for authentication
     * 
     * @param password
     *            the password to set
     */
    public void setRootPassword(String password)
    {
        rootpass = password;
    }

    @Override
    public void handle(Request request, Response response)
    {
        request.setChallengeResponse(new ChallengeResponse(ChallengeScheme.HTTP_BASIC, root, rootpass));
        super.handle(request, response);
    }

}
