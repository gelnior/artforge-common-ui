package fr.hd3d.common.ui.test.util;

import java.util.Collection;

import fr.hd3d.common.ui.client.userrights.UserRightsResolver;


public class TestUserRigthsResolver extends UserRightsResolver
{
    public static void addUserPermission(String right)
    {
        Collection<String> permissions = getUserPermissionsCopy();

        permissions.add(right);
        setUserPermissions(permissions);
    }

    public static void clearUserRights()
    {
        Collection<String> permissions = getUserPermissionsCopy();

        permissions.clear();
        setUserPermissions(permissions);
        
        Collection<String> bans = getUserBansCopy();

        bans.clear();
        setUserBans(bans);
    }
    
    public static void addUserBan(String ban)
    {
        Collection<String> bans = getUserBansCopy();

        bans.add(ban);
        setUserPermissions(bans);
    }
}
