package fr.hd3d.common.ui.test.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.core.FastMap;

import fr.hd3d.common.client.enums.EProjectStatus;
import fr.hd3d.common.client.enums.ESimpleActivityType;
import fr.hd3d.common.client.enums.ETaskStatus;
import fr.hd3d.common.ui.client.calendar.DatetimeUtil;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;
import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteTypeModelData;
import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;
import fr.hd3d.common.ui.client.modeldata.production.ConstituentModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;
import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;
import fr.hd3d.common.ui.client.modeldata.production.ShotModelData;
import fr.hd3d.common.ui.client.modeldata.reader.ReaderFactory;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;
import fr.hd3d.common.ui.client.modeldata.task.EntityTaskLinkModelData;
import fr.hd3d.common.ui.client.modeldata.task.PersonDayModelData;
import fr.hd3d.common.ui.client.modeldata.task.SimpleActivityModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskActivityModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.callback.GetModelDataCallback;
import fr.hd3d.common.ui.client.service.callback.PostModelDataCallback;
import fr.hd3d.common.ui.client.service.parameter.Constraints;
import fr.hd3d.common.ui.client.service.parameter.EqConstraint;
import fr.hd3d.common.ui.client.service.parameter.IsNullConstraint;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;


/**
 * Set of methods for easy test development.
 * 
 * @author HD3D
 */
public class TestUtils
{
    private static PersonModelData personTest = new PersonModelData();
    private static PersonModelData personRootTest = new PersonModelData();
    private static ProjectModelData projectTest = new ProjectModelData("UI Test Project",
            EProjectStatus.OPEN.toString());
    private static ResourceGroupModelData groupTest = new ResourceGroupModelData();

    private static FastMap<TaskTypeModelData> taskTypes = new FastMap<TaskTypeModelData>();
    private static FastMap<TaskModelData> tasks = new FastMap<TaskModelData>();

    private static FastMap<ApprovalNoteTypeModelData> approvalTypes = new FastMap<ApprovalNoteTypeModelData>();
    private static FastMap<ApprovalNoteModelData> approvalNotes = new FastMap<ApprovalNoteModelData>();;

    private static FastMap<CategoryModelData> categories = new FastMap<CategoryModelData>();;
    private static FastMap<ConstituentModelData> constituents = new FastMap<ConstituentModelData>();;

    private static FastMap<SequenceModelData> sequences = new FastMap<SequenceModelData>();;
    private static FastMap<ShotModelData> shots = new FastMap<ShotModelData>();;

    /**
     * @return A default person named "Blender Tester" whose login is "blt" (create it if it does not exist in
     *         services).
     */
    public static PersonModelData getDefaultPerson()
    {
        if (personTest.getId() == null)
        {
            personTest.setLogin("blt");
            personTest.setFirstName("Blender");
            personTest.setLastName("Tester");

            personTest.refreshByLogin(CommonEvents.MODEL_DATA_REFRESH_SUCCESS);
            if (personTest.getId() == null)
            {
                personTest.save();
            }
        }

        return personTest;
    }

    /**
     * @return A default project named "UT Test Project" (create it if it does not exist in services).
     */
    public static ProjectModelData getDefaultProject()
    {
        if (projectTest.getId() == null)
        {
            projectTest.refreshByName(CommonEvents.MODEL_DATA_REFRESH_SUCCESS);

            if (projectTest.getId() == null)
            {
                projectTest.save();
            }
        }
        return projectTest;
    }

    /**
     * @return The group linked to default project.
     */
    public static ResourceGroupModelData getDefaultGroup()
    {
        if (groupTest.getId() == null)
        {
            ProjectModelData project = getDefaultProject();
            groupTest.setId(project.getResourceGroupIds().get(0));

            groupTest.refresh();
        }
        return groupTest;
    }

    /**
     * @return A category called Persos for default project UI Test project.
     */
    public static CategoryModelData getDefaultCategory()
    {
        return TestUtils.getCategory("Persos", getDefaultProject().getId(), null);
    }

    public static SequenceModelData getDefaultSequence()
    {
        return TestUtils.getSequence("SO1", getDefaultProject().getId(), null);
    }

    /**
     * @return A constituent called Sam for default category Persos of project UI Test project.
     */
    public static ConstituentModelData getDefaultConstituent()
    {
        return TestUtils.getConstituent("Sam", getDefaultCategory());
    }

    public static ShotModelData getDefaultShot()
    {
        return TestUtils.getShot("S01_SHOT01", getDefaultSequence());
    }

    /**
     * @return A constituent called Bob for default category Persos of project UI Test project.
     */
    public static ConstituentModelData getDefaultConstituent2()
    {
        return TestUtils.getConstituent("Bob", getDefaultCategory());
    }

    public static TaskTypeModelData getTaskType(String name)
    {
        return getTaskType(name, "#FFF");
    }

    public static TaskTypeModelData getTaskType(String name, String color)
    {
        return getTaskType(name, "#FFF", ConstituentModelData.SIMPLE_CLASS_NAME);
    }

    /**
     * @param taskTypeName
     *            The name of the task type to return.
     * @param color
     *            The color of the task type to return.
     * @return Task Type corresponding to given task type name (create it if it does not exist in services).
     */
    public static TaskTypeModelData getTaskType(String taskTypeName, String color, String entityName)
    {
        TaskTypeModelData taskType = taskTypes.get(taskTypeName);
        if (taskType == null)
        {
            taskType = new TaskTypeModelData(taskTypeName);
            taskType.setColor(color);
            taskType.setEntityName(entityName);

            taskType.refreshByName(CommonEvents.MODEL_DATA_REFRESH_SUCCESS);

            if (taskType.getId() == null)
            {
                taskType.save();
            }
            taskTypes.put(taskTypeName, taskType);
        }

        return taskType;
    }

    /**
     * @param name
     *            Name of the approval type.
     * @param projectId
     *            ID of the project linked to the approval type.
     * @param taskTypeId
     *            ID of the task type linked to the approval type.
     * @return Approval type corresponding to parameters (create it if it does not exist in services).
     */
    public static ApprovalNoteTypeModelData getApprovalType(String name, Long projectId, Long taskTypeId)
    {
        ApprovalNoteTypeModelData approvalType = approvalTypes.get(name + "-" + projectId + "-" + taskTypeId);
        if (approvalType == null)
        {
            approvalType = new ApprovalNoteTypeModelData(name, projectId, taskTypeId);

            Constraints constraints = new Constraints();
            constraints.add(new EqConstraint(RecordModelData.NAME_FIELD, name));
            if (projectId != null)
                constraints.add(new EqConstraint("project.id", projectId));
            else
                constraints.add(new IsNullConstraint("project.id"));
            if (taskTypeId != null)
                constraints.add(new EqConstraint("taskType.id", taskTypeId));
            else
                constraints.add(new IsNullConstraint("taskType.id"));
            approvalType.refreshOrCreateWithParams(null, constraints);

            approvalTypes.put(name + "-" + projectId + "-" + taskTypeId, approvalType);
        }
        return approvalType;
    }

    /**
     * @param name
     *            Name of the category.
     * @param projectId
     *            ID of the project linked to the categiry.
     * @param parentID
     *            ID of the parent category.
     * @return Category corresponding to parameters (create it if it does not exist in services).
     */
    public static CategoryModelData getCategory(String name, Long projectId, Long parentId)
    {
        CategoryModelData category = categories.get(name + "-" + projectId + "-" + parentId);
        if (category == null)
        {
            category = new CategoryModelData(name, projectId, parentId);

            Constraints constraints = new Constraints();
            constraints.add(new EqConstraint(RecordModelData.NAME_FIELD, name));
            category.refreshOrCreateWithParams(null, constraints);

            categories.put(name + "-" + projectId + "-" + parentId, category);
        }
        return category;
    }

    public static SequenceModelData getSequence(String name, Long projectId, Long parentId)
    {
        SequenceModelData sequence = sequences.get(name + "-" + projectId + "-" + parentId);
        if (sequence == null)
        {
            sequence = new SequenceModelData(name, projectId, parentId);
            sequence.setDefaultPath(ServicesPath.getOneSegmentPath(ServicesPath.SEQUENCES, null));

            Constraints constraints = new Constraints();
            constraints.add(new EqConstraint(RecordModelData.NAME_FIELD, name));
            sequence.refreshOrCreateWithParams(null, constraints);

            sequences.put(name + "-" + projectId + "-" + parentId, sequence);
        }
        return sequence;
    }

    /**
     * @param name
     *            Name of the constituent.
     * @param category
     *            ID of the project linked to the approval type.
     * @return Category corresponding to parameters (create it if it does not exist in services).
     */
    public static ConstituentModelData getConstituent(String name, CategoryModelData category)
    {
        ConstituentModelData constituent = constituents.get(name + "-" + category.getId());
        if (constituent == null)
        {
            constituent = new ConstituentModelData(name, category);

            Constraints constraints = new Constraints();
            constraints.add(new EqConstraint(ConstituentModelData.CONSTITUENT_LABEL, name));
            constituent.refreshOrCreateWithParams(null, constraints);

            constituents.put(name + "-" + category.getId(), constituent);
        }
        return constituent;
    }

    public static ShotModelData getShot(String name, SequenceModelData sequence)
    {
        ShotModelData shot = shots.get(name + "-" + sequence.getId());
        if (shot == null)
        {
            shot = new ShotModelData(name, sequence);

            Constraints constraints = new Constraints();
            constraints.add(new EqConstraint(ShotModelData.SHOT_LABEL, name));
            shot.refreshOrCreateWithParams(null, constraints);

            shots.put(name + "-" + sequence.getId(), shot);
        }
        return shot;
    }

    /**
     * @param date
     *            Date of the approval.
     * @param constituent
     *            Constituent to set as work object for approval.
     * @param approvalType
     *            Type of the approval.
     * @return Approval note corresponding to parameters (create it if it does not exist in services).
     */
    public static ApprovalNoteModelData getApprovalNote(Date date, ConstituentModelData constituent,
            ApprovalNoteTypeModelData approvalType)
    {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString = dateFormatter.format(date) + ".0";
        ApprovalNoteModelData approval = approvalNotes.get(dateString + "-" + constituent.getId() + "-"
                + approvalType.getId());
        if (approval == null)
        {
            approval = new ApprovalNoteModelData(date, constituent.getId(), approvalType.getId());
            approval.setStatus("OK");

            Constraints constraints = new Constraints();
            constraints.add(new EqConstraint(ApprovalNoteModelData.DATE_FIELD, dateString));
            constraints.add(new EqConstraint(ApprovalNoteModelData.BOUND_ENTITY_ID, constituent.getId()));
            constraints.add(new EqConstraint(ApprovalNoteModelData.APPROVAL_TYPE_ID, approvalType.getId()));
            approval.refreshOrCreateWithParams(null, constraints);

            approvalNotes.put(dateString + "-" + constituent.getId() + "-" + approvalType.getId(), approval);
        }
        return approval;
    }

    /**
     * @param name
     * @return Project corresponding to name of which status is equal to OPEN. If project does not exists, null is
     *         returned.
     */
    public static ProjectModelData getOpenProject(String name)
    {
        ProjectModelData project = new ProjectModelData(name, EProjectStatus.OPEN.toString(), name);
        project.refreshOrCreateWithParams(null, new EqConstraint(RecordModelData.NAME_FIELD, name));

        return project;
    }

    /**
     * @param person
     *            Worker of task to retrieve.
     * @param taskType
     *            Task type of task to retrieve.
     * @param constituent
     *            Constituent linked to task type (must not be null).
     * @param status
     *            Status of task to retrieve.
     * @return Task assigned to person of task type equals to taskType, linked to constituent constituent and status
     *         equals to status. If task does not exist it is created. Constituent must not be null.
     */
    public static TaskModelData getTask(PersonModelData person, TaskTypeModelData taskType,
            final ConstituentModelData constituent, ETaskStatus status)
    {
        String key = "";
        if (person != null)
            key += person.getId();
        key += "-";

        key += taskType.getId() + "-" + constituent.getId();
        key += "-" + status.toString();

        final TaskModelData task = tasks.get(key);

        if (task == null)
        {
            final TaskModelData newTask = new TaskModelData();

            Constraints constraints = new Constraints();
            constraints.add(new EqConstraint("taskType.id", taskType.getId()));
            constraints.add(new EqConstraint("boundEntityTaskLinks.boundEntity", constituent.getId()));
            constraints.add(new EqConstraint("boundEntityTaskLinks.boundEntityName",
                    ConstituentModelData.SIMPLE_CLASS_NAME));
            if (person != null)
                constraints.add(new EqConstraint("worker.id", person.getId()));
            if (status != null)
                constraints.add(new EqConstraint(TaskModelData.STATUS_FIELD, status.toString()));

            newTask.refreshWithParams(null, constraints);

            if (newTask.getId() == null)
            {
                if (person != null)
                    newTask.setWorkerID(person.getId());
                newTask.setTaskTypeId(taskType.getId());
                newTask.setStatus(status.toString());
                newTask.setName(constituent.getLabel() + "_" + taskType.getName());
                newTask.setProjectID(getDefaultProject().getId());
                newTask.save(new PostModelDataCallback(newTask) {
                    @Override
                    protected void onSuccess(Request request, Response response)
                    {
                        this.setNewId(response);
                        newTask.setWorkObjectId(constituent.getId());

                        final EntityTaskLinkModelData taskLink = new EntityTaskLinkModelData();
                        taskLink.refreshByTask(newTask, new GetModelDataCallback(taskLink) {
                            @Override
                            protected void onNoRecordReturned()
                            {
                                if (taskLink.getBoundEntity() == null)
                                {
                                    taskLink.setBoundEntity(constituent.getId());
                                    taskLink.setBoundEntityName(ConstituentModelData.SIMPLE_CLASS_NAME);
                                    taskLink.setTaskId(newTask.getId());
                                    taskLink.save();
                                }
                            }
                        });
                    }
                });
            }

            tasks.put(key, newTask);
            return newTask;
        }
        else
        {
            return task;
        }
    }

    public static PersonDayModelData getDefaultDay()
    {
        PersonDayModelData personDay = new PersonDayModelData();
        personDay.setPersonId(TestUtils.getRootPerson().getId());
        personDay.setDate(DatetimeUtil.today());
        personDay.refreshOrCreate(null);

        Date startDate1 = DatetimeUtil.getTodayWithTime(9, 30);
        Date startDate2 = DatetimeUtil.getTodayWithTime(14, 00);
        Date endDate1 = DatetimeUtil.getTodayWithTime(13, 00);
        Date endDate2 = DatetimeUtil.getTodayWithTime(18, 30);
        personDay.setStartDate1(startDate1);
        personDay.setStartDate2(startDate2);
        personDay.setEndDate1(endDate1);
        personDay.setEndDate2(endDate2);

        return personDay;
    }

    private static Hd3dModelData getRootPerson()
    {
        if (personRootTest.getId() == null)
        {
            personRootTest.setLastName("root");

            personRootTest.refreshByLogin(CommonEvents.MODEL_DATA_REFRESH_SUCCESS);
        }

        return personRootTest;
    }

    public static SimpleActivityModelData getSimpleActivity(PersonDayModelData currentDay, ProjectModelData project,
            ESimpleActivityType type)
    {
        SimpleActivityModelData simpleActivity = new SimpleActivityModelData();
        simpleActivity.setType(type.toString());
        simpleActivity.setProject(project);
        simpleActivity.setDayId(currentDay.getId());
        simpleActivity.setWorkerId(currentDay.getPersonId());
        simpleActivity.setFilledById(currentDay.getPersonId());
        simpleActivity.setDuration(Long.valueOf(DatetimeUtil.HOUR_SECONDS));
        simpleActivity.setFilledDate(DatetimeUtil.today());

        Constraints constraints = new Constraints();
        constraints.add(new EqConstraint(SimpleActivityModelData.TYPE_FIELD, type.toString()));
        constraints.add(new EqConstraint("project.id", project.getId()));
        constraints.add(new EqConstraint("day.id", currentDay.getId()));

        simpleActivity.refreshOrCreateWithParams(null, constraints);

        return simpleActivity;
    }

    public static TaskActivityModelData getTaskActivity(TaskModelData task, PersonDayModelData currentDay)
    {
        TaskActivityModelData taskActivity = new TaskActivityModelData();
        taskActivity.setTaskId(task.getId());
        taskActivity.setProjectId(task.getProjectID());
        taskActivity.setDayId(currentDay.getId());
        taskActivity.setWorkerId(currentDay.getPersonId());
        taskActivity.setFilledById(currentDay.getPersonId());
        taskActivity.setDuration(Long.valueOf(DatetimeUtil.HOUR_SECONDS));
        taskActivity.setFilledDate(DatetimeUtil.today());

        Constraints constraints = new Constraints();
        // constraints.add(new EqConstraint(SimpleActivityModelData.TYPE_FIELD, type));
        constraints.add(new EqConstraint("day.id", currentDay.getId()));
        constraints.add(new EqConstraint("task.id", task.getId()));

        taskActivity.refreshOrCreateWithParams(null, constraints);

        return taskActivity;
    }

    public static TaskModelData getDefaultTask()
    {
        ConstituentModelData constituent = TestUtils.getDefaultConstituent();
        TaskTypeModelData taskType = TestUtils.getTaskType("Test-Modeling");
        TaskModelData task = TestUtils.getTask(MainModel.currentUser, taskType, constituent, ETaskStatus.STAND_BY);
        return task;
    }

    public static void deleteTasksForTaskTypes(List<TaskTypeModelData> taskTypes)
    {
        ServiceStore<TaskModelData> tasks = new ServiceStore<TaskModelData>(ReaderFactory.getTaskReader());
        tasks.addInConstraint("taskType.id", CollectionUtils.getIds(taskTypes));
        tasks.reload();
        for (TaskModelData task : tasks.getModels())
            task.delete();
    }

    public static void deleteApprovalForConstiuent(ConstituentModelData defaultConstituent)
    {
        ServiceStore<ApprovalNoteModelData> approvals = new ServiceStore<ApprovalNoteModelData>(
                ReaderFactory.getApprovalNoteReader());
        approvals.setPath(TestUtils.getDefaultConstituent().getDefaultPath() + "/" + ServicesPath.APPROVAL_NOTES);
        approvals.reload();

        for (ApprovalNoteModelData approval : approvals.getModels())
            approval.delete();

    }
}
