package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.task.TaskTypeModelData;


public class TaskTypeReaderMock extends Hd3dJsonReaderMock<TaskTypeModelData>
{
    public TaskTypeReaderMock()
    {
        super(TaskTypeModelData.getModelType());
    }

    @Override
    public TaskTypeModelData newModelInstance()
    {
        return new TaskTypeModelData();
    }
}
