package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.playlist.PlayListEditModelData;


public class PlayListEditReaderMock extends Hd3dJsonReaderMock<PlayListEditModelData>
{

    public PlayListEditReaderMock()
    {
        super(PlayListEditModelData.getModelType());
    }

    @Override
    public PlayListEditModelData newModelInstance()
    {
        return new PlayListEditModelData();
    }

}
