package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteTypeModelData;


public class ApprovalNoteTypeReaderMock extends Hd3dJsonReaderMock<ApprovalNoteTypeModelData>
{
    public ApprovalNoteTypeReaderMock()
    {
        super(ApprovalNoteTypeModelData.getModelType());
    }

    @Override
    public ApprovalNoteTypeModelData newModelInstance()
    {
        return new ApprovalNoteTypeModelData();
    }
}
