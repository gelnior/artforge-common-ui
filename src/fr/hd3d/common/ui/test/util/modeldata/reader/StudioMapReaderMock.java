package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.inventory.StudioMapModelData;


public class StudioMapReaderMock extends Hd3dJsonReaderMock<StudioMapModelData>
{
    public StudioMapReaderMock()
    {
        super(StudioMapModelData.getModelType());
    }

    @Override
    public StudioMapModelData newModelInstance()
    {
        return new StudioMapModelData();
    }
}
