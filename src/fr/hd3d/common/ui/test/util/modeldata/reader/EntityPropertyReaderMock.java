package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata.EntityPropertyModelData;


public class EntityPropertyReaderMock extends Hd3dJsonReaderMock<EntityPropertyModelData>
{
    public EntityPropertyReaderMock()
    {
        super(EntityPropertyModelData.getModelType());
    }

    @Override
    public EntityPropertyModelData newModelInstance()
    {
        return new EntityPropertyModelData();
    }
}
