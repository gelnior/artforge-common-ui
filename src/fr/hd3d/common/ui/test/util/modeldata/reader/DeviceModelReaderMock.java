package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.inventory.DeviceModelModelData;


public class DeviceModelReaderMock extends Hd3dJsonReaderMock<DeviceModelModelData>
{
    public DeviceModelReaderMock()
    {
        super(DeviceModelModelData.getModelType());
    }

    @Override
    public DeviceModelModelData newModelInstance()
    {
        return new DeviceModelModelData();
    }
}
