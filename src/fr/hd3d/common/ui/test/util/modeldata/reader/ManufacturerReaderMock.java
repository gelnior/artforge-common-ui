package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.inventory.ManufacturerModelData;


public class ManufacturerReaderMock extends Hd3dJsonReaderMock<ManufacturerModelData>
{
    public ManufacturerReaderMock()
    {
        super(ManufacturerModelData.getModelType());
    }

    @Override
    public ManufacturerModelData newModelInstance()
    {
        return new ManufacturerModelData();
    }
}
