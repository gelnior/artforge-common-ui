package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.task.TaskActivityModelData;


public class TaskActivityReaderMock extends Hd3dJsonReaderMock<TaskActivityModelData>
{
    public TaskActivityReaderMock()
    {
        super(PersonModelData.getModelType());
    }

    @Override
    public TaskActivityModelData newModelInstance()
    {
        return new TaskActivityModelData();
    }
}
