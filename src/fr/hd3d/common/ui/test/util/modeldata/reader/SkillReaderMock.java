package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.resource.SkillModelData;


public class SkillReaderMock extends Hd3dJsonReaderMock<SkillModelData>
{
    public SkillReaderMock()
    {
        super(SkillModelData.getModelType());
    }

    @Override
    public SkillModelData newModelInstance()
    {
        return new SkillModelData();
    }
}
