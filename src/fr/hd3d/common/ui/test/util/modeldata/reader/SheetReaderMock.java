package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;


public class SheetReaderMock extends Hd3dJsonReaderMock<SheetModelData>
{
    public SheetReaderMock()
    {
        super(SheetModelData.getModelType());
    }

    @Override
    public SheetModelData newModelInstance()
    {
        return new SheetModelData();
    }
}
