package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.inventory.ScreenModelData;


public class ScreenReaderMock extends Hd3dJsonReaderMock<ScreenModelData>
{
    public ScreenReaderMock()
    {
        super(ScreenModelData.getModelType());
    }

    @Override
    public ScreenModelData newModelInstance()
    {
        return new ScreenModelData();
    }
}
