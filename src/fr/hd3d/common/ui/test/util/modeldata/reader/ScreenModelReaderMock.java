package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.inventory.ScreenModelModelData;


public class ScreenModelReaderMock extends Hd3dJsonReaderMock<ScreenModelModelData>
{
    public ScreenModelReaderMock()
    {
        super(ScreenModelModelData.getModelType());
    }

    @Override
    public ScreenModelModelData newModelInstance()
    {
        return new ScreenModelModelData();
    }
}
