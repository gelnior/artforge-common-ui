package fr.hd3d.common.ui.test.util.modeldata.writer;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Stack;

import fr.hd3d.common.ui.client.modeldata.writer.IJsonWriter;


/**
 * Stringtree JSON writer updated for HD3D requirements.
 * 
 * @author Stringtree
 */
public class JSONWriterMock implements IJsonWriter
{
    protected final StringBuilder buf = new StringBuilder();
    protected final Stack<Object> calls = new Stack<Object>();

    // boolean emitClassName = true;

    public JSONWriterMock(boolean emitClassName)
    {
        // this.emitClassName = emitClassName;
    }

    public JSONWriterMock()
    {
        this(true);
    }

    public String write(Object object)
    {
        buf.setLength(0);
        value(object);
        return buf.toString();
    }

    public String write(long n)
    {
        return String.valueOf(n);
    }

    public String write(double d)
    {
        return String.valueOf(d);
    }

    public String write(char c)
    {
        return "\"" + c + "\"";
    }

    public String write(boolean b)
    {
        return String.valueOf(b);
    }

    private void value(Object object)
    {
        if (object == null || cyclic(object))
        {
            add("null");
        }
        else
        {
            calls.push(object);
            if (object instanceof Class<?>)
                string(object);
            else if (object instanceof Boolean)
                bool(((Boolean) object).booleanValue());
            else if (object instanceof Number)
                add(object);
            else if (object instanceof String)
                string(object);
            else if (object instanceof Character)
                string(object);
            else if (object instanceof Map<?, ?>)
                map((Map<?, ?>) object);
            else if (object.getClass().isArray())
                array(object);
            else if (object instanceof Iterator<?>)
                array((Iterator<?>) object);
            else if (object instanceof Collection<?>)
                array(((Collection<?>) object).iterator());
            else if (object instanceof Date)
                date((Date) object);
            else
                bean(object);
            calls.pop();
        }
    }

    private void date(Date value)
    {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        add('"');
        add(dateFormatter.format(value) + ".0");
        add('"');
    }

    private boolean cyclic(Object object)
    {
        Iterator<Object> it = calls.iterator();
        while (it.hasNext())
        {
            Object called = it.next();
            if (object == called)
                return true;
        }
        return false;
    }

    private void bean(Object object)
    {
        add("{");
        add("bean null");
        add("}");
    }

    protected void add(String name, Object value)
    {
        add('"');
        add(name);
        add("\":");
        value(value);
    }

    private void map(Map<?, ?> map)
    {
        add("{");
        Iterator<?> it = map.entrySet().iterator();
        while (it.hasNext())
        {
            Map.Entry<?, ?> e = (Map.Entry<?, ?>) it.next();
            value(e.getKey());
            add(":");
            value(e.getValue());
            if (it.hasNext())
                add(',');
        }
        add("}");
    }

    private void array(Iterator<?> it)
    {
        add("[");
        while (it.hasNext())
        {
            value(it.next());
            if (it.hasNext())
                add(",");
        }
        add("]");
    }

    private void array(Object object)
    {
        add("[");
        // Nothing...
        add("]");
    }

    private void bool(boolean b)
    {
        add(b ? "true" : "false");
    }

    private void string(Object obj)
    {
        add('"');
        for (char c : obj.toString().toCharArray())
        {
            if (c == '"')
                add("\\\"");
            else if (c == '\\')
                add("\\\\");
            else if (c == '/')
                add("\\/");
            else if (c == '\b')
                add("\\b");
            else if (c == '\f')
                add("\\f");
            else if (c == '\n')
                add("\\n");
            else if (c == '\r')
                add("\\r");
            else if (c == '\t')
                add("\\t");
            else
            {
                add(c);
            }
        }
        add('"');
    }

    protected void add(Object obj)
    {
        buf.append(obj);
    }

    private void add(char c)
    {
        buf.append(c);
    }

    static char[] hex = "0123456789ABCDEF".toCharArray();
}
