package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.resource.ContractModelData;


public class ContractReaderMock extends Hd3dJsonReaderMock<ContractModelData>
{
    public ContractReaderMock()
    {
        super(ContractModelData.getModelType());
    }

    @Override
    public ContractModelData newModelInstance()
    {
        return new ContractModelData();
    }
}
