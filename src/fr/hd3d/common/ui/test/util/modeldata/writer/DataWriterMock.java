package fr.hd3d.common.ui.test.util.modeldata.writer;

import java.util.Date;

import fr.hd3d.common.ui.client.modeldata.writer.IDateWriter;


public class DataWriterMock implements IDateWriter
{

    public String write(String dateTimeString, Date date)
    {
        java.text.SimpleDateFormat dateFormatter = new java.text.SimpleDateFormat(dateTimeString);
        return dateFormatter.format(date);
    }

}
