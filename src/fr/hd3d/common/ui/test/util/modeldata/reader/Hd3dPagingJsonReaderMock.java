package fr.hd3d.common.ui.test.util.modeldata.reader;

import com.extjs.gxt.ui.client.data.BasePagingLoadResult;
import com.extjs.gxt.ui.client.data.ModelType;
import com.extjs.gxt.ui.client.data.PagingLoadResult;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.reader.IPagingReader;


public abstract class Hd3dPagingJsonReaderMock<C extends Hd3dModelData> extends Hd3dJsonReaderMock<C> implements
        IPagingReader<C>
{

    public Hd3dPagingJsonReaderMock(ModelType modelType)
    {
        super(modelType);
    }

    @Override
    public PagingLoadResult<C> read(String data)
    {
        return this.read(null, data);
    }

    @Override
    public PagingLoadResult<C> read(C loadConfig, Object data)
    {
        this.readResult((String) data);

        BasePagingLoadResult<C> results = new BasePagingLoadResult<C>(models);
        results.setTotalLength(totalCount);

        return results;
    }

    @SuppressWarnings("unchecked")
    public PagingLoadResult<C> read(Object loadConfig, Object data)
    {
        return read((C) loadConfig, data);
    }

}
