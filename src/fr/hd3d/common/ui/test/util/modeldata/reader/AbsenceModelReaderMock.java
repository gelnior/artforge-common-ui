package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.resource.AbsenceModelData;


public class AbsenceModelReaderMock extends Hd3dJsonReaderMock<AbsenceModelData>
{
    public AbsenceModelReaderMock()
    {
        super(AbsenceModelData.getModelType());
    }

    @Override
    public AbsenceModelData newModelInstance()
    {
        return new AbsenceModelData();
    }
}
