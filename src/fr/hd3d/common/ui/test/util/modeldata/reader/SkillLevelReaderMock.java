package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.resource.SkillLevelModelData;


public class SkillLevelReaderMock extends Hd3dJsonReaderMock<SkillLevelModelData>
{
    public SkillLevelReaderMock()
    {
        super(SkillLevelModelData.getModelType());
    }

    @Override
    public SkillLevelModelData newModelInstance()
    {
        return new SkillLevelModelData();
    }
}
