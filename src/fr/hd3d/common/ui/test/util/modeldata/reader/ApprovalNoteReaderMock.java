package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.production.ApprovalNoteModelData;


public class ApprovalNoteReaderMock extends Hd3dJsonReaderMock<ApprovalNoteModelData>
{
    public ApprovalNoteReaderMock()
    {
        super(ApprovalNoteModelData.getModelType());
    }

    @Override
    public ApprovalNoteModelData newModelInstance()
    {
        return new ApprovalNoteModelData();
    }
}
