package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.resource.ResourceGroupModelData;


public class ResourceGroupReaderMock extends Hd3dJsonReaderMock<ResourceGroupModelData>
{
    public ResourceGroupReaderMock()
    {
        super(ResourceGroupModelData.getModelType());
    }

    @Override
    public ResourceGroupModelData newModelInstance()
    {
        return new ResourceGroupModelData();
    }
}
