package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.inventory.PoolModelData;


public class PoolReaderMock extends Hd3dJsonReaderMock<PoolModelData>
{
    public PoolReaderMock()
    {
        super(PoolModelData.getModelType());
    }

    @Override
    public PoolModelData newModelInstance()
    {
        return new PoolModelData();
    }
}
