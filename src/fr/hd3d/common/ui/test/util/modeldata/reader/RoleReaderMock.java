package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.technical.RoleModelData;


public class RoleReaderMock extends Hd3dJsonReaderMock<RoleModelData>
{
    public RoleReaderMock()
    {
        super(RoleModelData.getModelType());
    }

    @Override
    public RoleModelData newModelInstance()
    {
        return new RoleModelData();
    }
}
