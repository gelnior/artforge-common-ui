package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;


public class PersonReaderMock extends Hd3dJsonReaderMock<PersonModelData>
{
    public PersonReaderMock()
    {
        super(PersonModelData.getModelType());
    }

    @Override
    public PersonModelData newModelInstance()
    {
        return new PersonModelData();
    }
}
