package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.playlist.PlayListRevisionModelData;


public class PlayListRevisionModelDataReaderMock extends Hd3dJsonReaderMock<PlayListRevisionModelData>
{

    public PlayListRevisionModelDataReaderMock()
    {
        super(PlayListRevisionModelData.getModelType());
    }

    @Override
    public PlayListRevisionModelData newModelInstance()
    {
        return new PlayListRevisionModelData();
    }

}
