package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.production.CategoryModelData;


public class CategoryReaderMock extends Hd3dListJsonReaderMock<CategoryModelData>
{
    public CategoryReaderMock()
    {
        super();
    }

    @Override
    public CategoryModelData newModelInstance()
    {
        return new CategoryModelData();
    }
}
