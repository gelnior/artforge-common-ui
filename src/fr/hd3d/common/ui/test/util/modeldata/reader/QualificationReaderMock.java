package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.resource.QualificationModelData;


public class QualificationReaderMock extends Hd3dJsonReaderMock<QualificationModelData>
{
    public QualificationReaderMock()
    {
        super(QualificationModelData.getModelType());
    }

    @Override
    public QualificationModelData newModelInstance()
    {
        return new QualificationModelData();
    }
}
