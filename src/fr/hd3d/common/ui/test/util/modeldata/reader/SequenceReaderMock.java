package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.production.SequenceModelData;


public class SequenceReaderMock extends Hd3dListJsonReaderMock<SequenceModelData>
{
    public SequenceReaderMock()
    {
        super();
    }

    @Override
    public SequenceModelData newModelInstance()
    {
        return new SequenceModelData();
    }
}
