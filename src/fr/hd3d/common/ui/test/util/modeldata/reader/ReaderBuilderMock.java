package fr.hd3d.common.ui.test.util.modeldata.reader;

import com.extjs.gxt.ui.client.data.ModelType;

import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.modeldata.reader.mock.IReaderBuilder;


public class ReaderBuilderMock implements IReaderBuilder
{
    public <T extends Hd3dModelData> IReader<T> makeBuilder(final Class<T> classLiteral, ModelType modelType)
    {
        return new Hd3dJsonReaderMock<T>(modelType) {
            @Override
            public T newModelInstance()
            {
                try
                {
                    return classLiteral.newInstance();
                }
                catch (InstantiationException e)
                {
                    Logger.log("Class instantiation error for reader occurs.", e);
                    return null;
                }
                catch (IllegalAccessException e)
                {
                    Logger.log("Class instantiation error for reader occurs.", e);
                    return null;
                }
            }
        };
    }
}
