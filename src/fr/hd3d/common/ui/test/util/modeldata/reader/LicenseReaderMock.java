package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.inventory.LicenseModelData;


public class LicenseReaderMock extends Hd3dJsonReaderMock<LicenseModelData>
{
    public LicenseReaderMock()
    {
        super(LicenseModelData.getModelType());
    }

    @Override
    public LicenseModelData newModelInstance()
    {
        return new LicenseModelData();
    }
}
