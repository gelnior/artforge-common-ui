package fr.hd3d.common.ui.test.util.modeldata.writer;

import java.util.Map;
import java.util.Map.Entry;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.writer.IModelDataJsonWriter;


/**
 * ModelDataJsonWriter translates a ModelData object in the JSON format expected by web services for PUT and POST
 * operations.
 * 
 * @author HD3D
 */
public class ModelDataJsonWriterMock extends JSONWriterMock implements IModelDataJsonWriter
{

    private static final String BEGIN_OBJECT = "{";
    private static final String END_OBJECT = "}";

    /**
     * Default constructor.
     */
    public ModelDataJsonWriterMock()
    {
        super(false);
    }

    /**
     * @param object
     *            The object that need to be transcripted at JSON format.
     * @param httpMethod
     *            the method type for which the JSON format is expected.
     * 
     * @return Model data object at JSON Format.
     */
    public final String write(Hd3dModelData object, Method httpMethod)
    {
        this.add(BEGIN_OBJECT);
        for (Map.Entry<String, ?> e : object.getProperties().entrySet())
        {
            this.addEntry(e, httpMethod);
        }
        this.add(END_OBJECT);

        return buf.toString();
    }

    /**
     * Add entry to the string buffer.
     * 
     * @param entry
     *            The entry (field name and value) to add to final JSON object.
     * @param httpMethod
     *            The method chosen to build the final JSON object.
     */
    private void addEntry(Entry<String, ?> entry, Method httpMethod)
    {
        String key = entry.getKey();

        if (this.isAddableField(key, httpMethod))
        {
            if (buf.length() > 1)
            {
                this.add(",");
            }

            this.add(key, entry.getValue());
        }
    }

    /**
     * Tests if the field can be added to the JSON. Version and id fields are not expected in a POST request. Version
     * field is not expected in a PUT request.
     * 
     * @param key
     *            The name field to test.
     * @param httpMethod
     *            The chosen method for json writing.
     * @return True if the field can be added, false either.
     */
    public boolean isAddableField(String key, Method httpMethod)
    {
        boolean addField = true;

        addField = httpMethod == Method.POST && 0 == key.compareTo(Hd3dModelData.ID_FIELD);
        addField = addField || 0 == key.compareTo(Hd3dModelData.VERSION_FIELD);
        addField = !addField;

        return addField;
    }

    public void clear()
    {
        this.buf.setLength(0);
    }
}
