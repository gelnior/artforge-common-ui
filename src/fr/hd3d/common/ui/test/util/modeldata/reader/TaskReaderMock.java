package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.task.TaskModelData;


public class TaskReaderMock extends Hd3dJsonReaderMock<TaskModelData>
{
    public TaskReaderMock()
    {
        super(TaskModelData.getModelType());
    }

    @Override
    public TaskModelData newModelInstance()
    {
        return new TaskModelData();
    }
}
