package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.inventory.SoftwareModelData;


public class SoftwareReaderMock extends Hd3dJsonReaderMock<SoftwareModelData>
{
    public SoftwareReaderMock()
    {
        super(SoftwareModelData.getModelType());
    }

    @Override
    public SoftwareModelData newModelInstance()
    {
        return new SoftwareModelData();
    }
}
