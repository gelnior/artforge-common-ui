package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.technical.SettingModelData;

public class SettingReaderMock extends Hd3dJsonReaderMock<SettingModelData>
{
    public SettingReaderMock()
    {
        super(SettingModelData.getModelType());
    }

    @Override
    public SettingModelData newModelInstance()
    {
        return new SettingModelData();
    }
}
