package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.inventory.DeviceModelData;


public class DeviceReaderMock extends Hd3dJsonReaderMock<DeviceModelData>
{
    public DeviceReaderMock()
    {
        super(DeviceModelData.getModelType());
    }

    @Override
    public DeviceModelData newModelInstance()
    {
        return new DeviceModelData();
    }
}
