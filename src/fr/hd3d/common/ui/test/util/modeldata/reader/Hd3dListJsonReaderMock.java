package fr.hd3d.common.ui.test.util.modeldata.reader;

import com.extjs.gxt.ui.client.data.DataReader;
import com.extjs.gxt.ui.client.data.ListLoadResult;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;


public abstract class Hd3dListJsonReaderMock<C extends Hd3dModelData> extends Hd3dJsonReaderMock<C> implements
        DataReader<ListLoadResult<C>>
{

    public ListLoadResult<C> read(Object loadConfig, Object data)
    {
        return super.read(null, data);
    }

}
