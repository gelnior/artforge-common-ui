package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.inventory.ComputerTypeModelData;


public class ComputerTypeReaderMock extends Hd3dJsonReaderMock<ComputerTypeModelData>
{
    public ComputerTypeReaderMock()
    {
        super(ComputerTypeModelData.getModelType());
    }

    @Override
    public ComputerTypeModelData newModelInstance()
    {
        return new ComputerTypeModelData();
    }
}
