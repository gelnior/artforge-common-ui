package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;


public class ProjectReaderMock extends Hd3dJsonReaderMock<ProjectModelData>
{
    public ProjectReaderMock()
    {
        super(ProjectModelData.getModelType());
    }

    @Override
    public ProjectModelData newModelInstance()
    {
        return new ProjectModelData();
    }
}
