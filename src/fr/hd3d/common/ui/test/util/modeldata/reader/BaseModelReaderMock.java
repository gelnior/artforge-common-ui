package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;


public class BaseModelReaderMock extends Hd3dJsonReaderMock<Hd3dModelData>
{
    public BaseModelReaderMock()
    {
        super(Hd3dModelData.getModelType());
    }

    @Override
    public Hd3dModelData newModelInstance()
    {
        return new Hd3dModelData();
    }
}
