package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.inventory.RoomModelData;


public class RoomReaderMock extends Hd3dJsonReaderMock<RoomModelData>
{
    public RoomReaderMock()
    {
        super(RoomModelData.getModelType());
    }

    @Override
    public RoomModelData newModelInstance()
    {
        return new RoomModelData();
    }
}
