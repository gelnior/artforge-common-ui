package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelData;


public class ComputerReaderMock extends Hd3dJsonReaderMock<ComputerModelData>
{
    public ComputerReaderMock()
    {
        super(ComputerModelData.getModelType());
    }

    @Override
    public ComputerModelData newModelInstance()
    {
        return new ComputerModelData();
    }
}
