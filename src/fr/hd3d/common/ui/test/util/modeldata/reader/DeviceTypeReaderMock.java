package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.inventory.DeviceTypeModelData;


public class DeviceTypeReaderMock extends Hd3dJsonReaderMock<DeviceTypeModelData>
{
    public DeviceTypeReaderMock()
    {
        super(DeviceTypeModelData.getModelType());
    }

    @Override
    public DeviceTypeModelData newModelInstance()
    {
        return new DeviceTypeModelData();
    }
}
