package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelModelData;


public class ComputerModelReaderMock extends Hd3dJsonReaderMock<ComputerModelModelData>
{
    public ComputerModelReaderMock()
    {
        super(ComputerModelModelData.getModelType());
    }

    @Override
    public ComputerModelModelData newModelInstance()
    {
        return new ComputerModelModelData();
    }
}
