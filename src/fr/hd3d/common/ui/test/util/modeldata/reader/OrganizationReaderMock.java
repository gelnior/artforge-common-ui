package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.resource.OrganizationModelData;


public class OrganizationReaderMock extends Hd3dJsonReaderMock<OrganizationModelData>
{
    public OrganizationReaderMock()
    {
        super(OrganizationModelData.getModelType());
    }

    @Override
    public OrganizationModelData newModelInstance()
    {
        return new OrganizationModelData();
    }
}
