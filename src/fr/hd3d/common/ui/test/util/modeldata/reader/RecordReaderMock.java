package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.RecordModelData;


public class RecordReaderMock extends Hd3dJsonReaderMock<RecordModelData>
{
    public RecordReaderMock()
    {
        super(RecordModelData.getModelType());
    }

    @Override
    public RecordModelData newModelInstance()
    {
        return new RecordModelData();
    }
}
