package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.inventory.ProcessorModelData;


public class ProcessorReaderMock extends Hd3dJsonReaderMock<ProcessorModelData>
{
    public ProcessorReaderMock()
    {
        super(ProcessorModelData.getModelType());
    }

    @Override
    public ProcessorModelData newModelInstance()
    {
        return new ProcessorModelData();
    }
}
