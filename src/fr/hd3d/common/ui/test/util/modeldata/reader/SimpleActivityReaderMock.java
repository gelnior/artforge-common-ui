package fr.hd3d.common.ui.test.util.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.task.SimpleActivityModelData;


public class SimpleActivityReaderMock extends Hd3dJsonReaderMock<SimpleActivityModelData>
{
    public SimpleActivityReaderMock()
    {
        super(SimpleActivityModelData.getModelType());
    }

    @Override
    public SimpleActivityModelData newModelInstance()
    {
        return new SimpleActivityModelData();
    }
}
