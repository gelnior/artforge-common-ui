package fr.hd3d.common.ui.test.explorer;

import junit.framework.TestCase;

import org.junit.Test;

import com.extjs.gxt.ui.client.widget.grid.CheckColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;

import fr.hd3d.common.client.Editor;
import fr.hd3d.common.client.Renderer;
import fr.hd3d.common.ui.client.modeldata.technical.ItemModelData;
import fr.hd3d.common.ui.client.widget.explorer.model.BaseColumnInfoProvider;


public class BaseColumnInfoProviderTest extends TestCase
{

    @Test
    public void testBooleanRenderer()
    {
        ItemModelData column = new ItemModelData();

        column.setRenderer(Renderer.BOOLEAN);
        column.setEditor(Editor.BOOLEAN);
        column.setItemType("static");
        column.setType("Boolean");
        column.setId(124L);
        BaseColumnInfoProvider provider = new BaseColumnInfoProvider();

        ColumnConfig cc = provider.getColumnConfig(column, 1L);

        assertNull(cc.getEditor());
        assertTrue(cc instanceof CheckColumnConfig);
    }
}
