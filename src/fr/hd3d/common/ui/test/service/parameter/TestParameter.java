package fr.hd3d.common.ui.test.service.parameter;

import junit.framework.TestCase;

import org.junit.Test;

import fr.hd3d.common.client.enums.EConstraintLogicalOperator;
import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.parameter.LogicConstraint;
import fr.hd3d.common.ui.client.service.parameter.Pagination;


public class TestParameter extends TestCase
{

    @Test
    public void testPagination()
    {
        Pagination pagination = new Pagination(10L, 20L);
        String paginationString = "{\"quantity\":20,\"first\":10}";
        assertEquals(paginationString, pagination.toJson());

        paginationString = "pagination={\"quantity\":20,\"first\":10}";
        assertEquals(paginationString, pagination.toString());
    }

    @Test
    public void testEqualConstraint()
    {
        Constraint equal = new Constraint(EConstraintOperator.eq, "name", "test", null);

        String constraintString = "{\"value\":\"test\",\"column\":\"name\",\"type\":\"eq\"}";
        assertEquals(constraintString, equal.toJson());

        constraintString = "constraint=[{\"value\":\"test\",\"column\":\"name\",\"type\":\"eq\"}]";
        assertEquals(constraintString, equal.toString());
    }

    @Test
    public void testLogicConstraint()
    {
        Constraint equal = new Constraint(EConstraintOperator.eq, "name", "test", null);
        Constraint notEqual = new Constraint(EConstraintOperator.neq, "name", "test_not", null);

        LogicConstraint constraint = new LogicConstraint(EConstraintLogicalOperator.AND, equal, notEqual);

        String constraintString = "{";
        constraintString += "\"filter\":[{\"value\":\"test\",\"column\":\"name\",\"type\":\"eq\"},";
        constraintString += "{\"value\":\"test_not\",\"column\":\"name\",\"type\":\"neq\"}]";
        constraintString += ",\"logic\":\"AND\"}";
        assertEquals(constraintString, constraint.toJson());

        constraintString = "constraint=[" + constraintString + "]";
        assertEquals(constraintString, constraint.toString());
    }
}
