package fr.hd3d.common.ui.test;

import junit.framework.TestCase;

import org.junit.Test;

import fr.hd3d.common.ui.test.util.modeldata.reader.ProjectReaderMock;


public class JSONReaderTest extends TestCase
{
    @Test
    public void testRead()
    {
        ProjectReaderMock reader = new ProjectReaderMock();

        System.out.println(reader.readString("{\"toto\":\"({[1,"));
        System.out.println(reader.readString("{\"toto\":\""));
        System.out.println(reader.readString("{\"toto\":t"));
        System.out.println(reader.readString("{\"toto\":f"));
        System.out.println(reader.readString("{{toto\":f"));
        System.out.println(reader.readString("{{toto\":f"));
        System.out.println(reader.readString("[toto\":f"));
        System.out.println(reader.readString("Azerezrerz\""));
        System.out.println(reader.readString("{\"toto:f}"));
    }
}
