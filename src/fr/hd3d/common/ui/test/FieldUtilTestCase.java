package fr.hd3d.common.ui.test;

import org.junit.Test;

import fr.hd3d.common.ui.client.util.FieldUtils;


public class FieldUtilTestCase extends UITestCase
{

    @Test
    public void testGetDigitNumber()
    {
        String number = FieldUtils.getDigitNumber(2, 3);
        assertEquals("002", number);
        assertNotSame("0002", number);
        assertNotSame("02", number);

        number = FieldUtils.getDigitNumber(2, 2);
        assertEquals("02", number);

        number = FieldUtils.getDigitNumber(12, 2);
        assertEquals("12", number);
    }
}
