package fr.hd3d.common.ui.test;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import org.junit.Test;

import fr.hd3d.common.ui.client.clipboard.ClipBoard;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.production.ProjectModelData;


public class ClipBoardTest extends TestCase
{

    @Test
    public void testCopy()
    {
        ProjectModelData project = new ProjectModelData("Test Project", "OPEN");
        ClipBoard.get().copy(project);
        Hd3dModelData projectCopy = (Hd3dModelData) ClipBoard.get().paste();

        assertEquals("Test Project", projectCopy.get(ProjectModelData.NAME_FIELD));
        projectCopy.set(ProjectModelData.NAME_FIELD, "Change");
        assertEquals("Test Project", project.getName());
        assertEquals("Change", projectCopy.get(ProjectModelData.NAME_FIELD));
    }

    @Test
    public void testCopyList()
    {
        ProjectModelData project1 = new ProjectModelData("Test Project 1", "OPEN");
        ProjectModelData project2 = new ProjectModelData("Test Project 2", "OPEN");
        List<ProjectModelData> projects = new ArrayList<ProjectModelData>();
        projects.add(project1);
        projects.add(project2);

        ClipBoard.get().copy(projects);
        List<Hd3dModelData> projectCopies = (List<Hd3dModelData>) ClipBoard.get().paste();

        assertEquals(2, projectCopies.size());
        Hd3dModelData projectCopy1 = projectCopies.get(0);
        Hd3dModelData projectCopy2 = projectCopies.get(1);

        assertEquals("Test Project 1", projectCopy1.get(ProjectModelData.NAME_FIELD));
        projectCopy1.set(ProjectModelData.NAME_FIELD, "Change 1");
        assertEquals("Test Project 1", project1.getName());
        assertEquals("Change 1", projectCopy1.get(ProjectModelData.NAME_FIELD));

        assertEquals("Test Project 2", projectCopy2.get(ProjectModelData.NAME_FIELD));
        projectCopy2.set(ProjectModelData.NAME_FIELD, "Change 2");
        assertEquals("Test Project 2", project2.getName());
        assertEquals("Change 2", projectCopy2.get(ProjectModelData.NAME_FIELD));
    }
}
